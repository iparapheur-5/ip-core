#
# iparapheur Core
# Copyright (C) 2018-2023 Libriciel-SCOP
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

FROM hubdocker.libriciel.fr/eclipse-temurin:17.0.7_7-jre-jammy

HEALTHCHECK --interval=5s --timeout=2s --retries=60 \
  CMD curl --fail --silent http://localhost:8080/api/actuator/health | grep UP || exit 1

# Open Containers Initiative parameters
ARG CURRENT_BUILD_VERSION=""
ARG CURRENT_BUILD_DATE=""
ARG CURRENT_BUILD_COMMIT_REVISION=""
# Non-standard and/or deprecated variables, that are still widely used.
# If it is already set in the FROM image, it has to be overridden.
MAINTAINER Libriciel SCOP
LABEL maintainer="Libriciel SCOP"
LABEL org.label-schema.name="ip-core"
LABEL org.label-schema.vendor="Libriciel SCOP"
LABEL org.label-schema.build-date="$CURRENT_BUILD_DATE"
LABEL org.label-schema.schema-version="$CURRENT_BUILD_VERSION"
# Open Containers Initiative's image specifications
LABEL org.opencontainers.image.created="$CURRENT_BUILD_DATE"
LABEL org.opencontainers.image.version="$CURRENT_BUILD_VERSION"
LABEL org.opencontainers.image.revision="$CURRENT_BUILD_COMMIT_REVISION"
LABEL org.opencontainers.image.vendor="Libriciel SCOP"
LABEL org.opencontainers.image.title="ip-core"
LABEL org.opencontainers.image.description="iparapheur main entrypoint, used to link every other elements"
LABEL org.opencontainers.image.authors="Libriciel SCOP"
LABEL org.opencontainers.image.licenses="GNU Affero GPL v3"

# The docker image does not contain any other language than English by default.
# Additional compatible languages shall be installed at build-time.
RUN locale-gen en_US.UTF-8
RUN locale-gen fr_FR.UTF-8
# Then, languages can be set at runtime, with a regular environment variable.
# Timezones aren't affected by the build, and any timezone would work.
ENV LANG="fr_FR.UTF-8"
ENV LANGUAGE="fr_FR:en_US:en"
ENV LC_ALL="fr_FR.UTF-8"
ENV TZ="Europe/Paris"

ENV JAVA_OPTS=""

VOLUME /tmp

ADD ip-core/ip-core-*.jar core.jar

COPY docker-entrypoint.sh /usr/local/bin/
RUN chmod +x /usr/local/bin/docker-entrypoint.sh
RUN ln -s /usr/local/bin/docker-entrypoint.sh /

ENTRYPOINT [ "docker-entrypoint.sh" ]
