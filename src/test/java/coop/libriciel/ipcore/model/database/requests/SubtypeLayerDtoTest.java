/*
 * iparapheur Core
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.ipcore.model.database.requests;

import coop.libriciel.ipcore.model.database.Subtype;
import coop.libriciel.ipcore.model.database.SubtypeLayer;
import coop.libriciel.ipcore.model.pdfstamp.Layer;
import org.junit.jupiter.api.Test;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Optional;
import java.util.UUID;

import static coop.libriciel.ipcore.model.database.SubtypeLayerAssociation.ANNEXE;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;


@SpringBootTest
class SubtypeLayerDtoTest {


    private @Autowired ModelMapper modelMapper;


    private static void assertDeepEquals(SubtypeLayer entity, SubtypeLayerDto dto) {
        assertEquals(Optional.ofNullable(entity.getSubtype()).map(Subtype::getId).orElse(null), dto.getSubtypeId());
        assertEquals(Optional.ofNullable(entity.getId()).map(SubtypeLayer.CompositeId::getSubtypeId).orElse(null), dto.getSubtypeId());
        assertEquals(Optional.ofNullable(entity.getId()).map(SubtypeLayer.CompositeId::getLayerId).orElse(null), dto.getLayerId());
        // assertEquals(Optional.ofNullable(entity.getLayer()).map(Layer::getId).orElse(null), dto.getLayerId());
        // assertEquals(Optional.ofNullable(entity.getLayer()).map(Layer::getName).orElse(null), dto.getLayer().getName());
        assertEquals(entity.getAssociation(), dto.getAssociation());
    }


    @Test
    public void convertEntityToDto_full() {

        String subtypeId = UUID.randomUUID().toString();
        String layerId = UUID.randomUUID().toString();

        SubtypeLayer entity = new SubtypeLayer();
        entity.setId(new SubtypeLayer.CompositeId(subtypeId, layerId));
        entity.setSubtype(new Subtype(subtypeId));
        entity.setLayer(new Layer(layerId));
        entity.setAssociation(ANNEXE);

        SubtypeLayerDto dto = modelMapper.map(entity, SubtypeLayerDto.class);
        assertNotNull(dto);
        assertDeepEquals(entity, dto);
    }


    @Test
    public void convertEntityToDto_empty() {

        SubtypeLayer entity = new SubtypeLayer();

        SubtypeLayerDto dto = modelMapper.map(entity, SubtypeLayerDto.class);
        assertNotNull(dto);
        assertDeepEquals(entity, dto);
    }


    @Test
    public void convertDtoToEntity_full() {

        SubtypeLayerDto dto = new SubtypeLayerDto();
        dto.setLayerId(UUID.randomUUID().toString());
        dto.setAssociation(ANNEXE);

        SubtypeLayer entity = modelMapper.map(dto, SubtypeLayer.class);
        assertNotNull(entity);
        assertDeepEquals(entity, dto);
    }


    @Test
    public void convertDtoToEntity_empty() {

        SubtypeLayerDto dto = new SubtypeLayerDto();

        SubtypeLayer entity = modelMapper.map(dto, SubtypeLayer.class);
        assertNotNull(entity);
        assertDeepEquals(entity, dto);
    }


}