/*
 * iparapheur Core
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.ipcore.model.database.requests;

import coop.libriciel.ipcore.model.database.Tenant;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.Test;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;


@SpringBootTest
class TenantDtoTest {


    private @Autowired ModelMapper modelMapper;


    private static void assertDeepEquals(Tenant entity, TenantDto dto) {
        assertEquals(entity.getId(), dto.getId());
        assertEquals(entity.getName(), dto.getName());
    }


    @Test
    public void convertEntityToDto_full() {

        Tenant entity = new Tenant();
        entity.setId(UUID.randomUUID().toString());
        entity.setName(RandomStringUtils.random(30));

        TenantDto dto = modelMapper.map(entity, TenantDto.class);
        assertNotNull(dto);
        assertDeepEquals(entity, dto);
    }


    @Test
    public void convertEntityToDto_empty() {

        Tenant entity = new Tenant();

        TenantDto dto = modelMapper.map(entity, TenantDto.class);
        assertNotNull(dto);
        assertDeepEquals(entity, dto);
    }


    @Test
    public void convertDtoToEntity_full() {

        TenantDto dto = new TenantDto();
        dto.setId(UUID.randomUUID().toString());
        dto.setName(RandomStringUtils.random(30));

        Tenant entity = modelMapper.map(dto, Tenant.class);
        assertNotNull(entity);
        assertDeepEquals(entity, dto);
    }


    @Test
    public void convertDtoToEntity_empty() {

        TenantDto dto = new TenantDto();

        Tenant entity = modelMapper.map(dto, Tenant.class);
        assertNotNull(entity);
        assertDeepEquals(entity, dto);
    }


}