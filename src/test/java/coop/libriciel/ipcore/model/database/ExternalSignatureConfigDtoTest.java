/*
 * iparapheur Core
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.ipcore.model.database;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.Test;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static coop.libriciel.ipcore.model.database.ExternalSignatureProvider.YOUSIGN;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;


@SpringBootTest
class ExternalSignatureConfigDtoTest {


    private @Autowired ModelMapper modelMapper;


    private static void assertDeepEquals(ExternalSignatureConfig entity, ExternalSignatureConfigDto dto) {
        assertEquals(entity.getName(), dto.getName());
        assertEquals(entity.getServiceName(), dto.getServiceName());
        assertEquals(entity.getUrl(), dto.getUrl());
        assertEquals(entity.getToken(), dto.getToken());
        assertEquals(entity.getPassword(), dto.getPassword());
        assertEquals(entity.getLogin(), dto.getLogin());
    }


    @Test
    void convertEntityToDto_full() {

        ExternalSignatureConfig entity = new ExternalSignatureConfig();
        entity.setName(RandomStringUtils.random(30));
        entity.setServiceName(YOUSIGN);
        entity.setUrl(RandomStringUtils.random(30));
        entity.setToken(RandomStringUtils.random(30));
        entity.setPassword(RandomStringUtils.random(30));
        entity.setLogin(RandomStringUtils.random(30));

        ExternalSignatureConfigDto dto = modelMapper.map(entity, ExternalSignatureConfigDto.class);
        assertNotNull(dto);
        assertDeepEquals(entity, dto);
    }


    @Test
    public void convertEntityToDto_empty() {

        ExternalSignatureConfig entity = new ExternalSignatureConfig();

        ExternalSignatureConfigDto dto = modelMapper.map(entity, ExternalSignatureConfigDto.class);
        assertNotNull(dto);
        assertDeepEquals(entity, dto);
    }


    @Test
    void convertDtoToEntity_full() {

        ExternalSignatureConfigDto dto = new ExternalSignatureConfigDto();
        dto.setName(RandomStringUtils.random(30));
        dto.setServiceName(YOUSIGN);
        dto.setUrl(RandomStringUtils.random(30));
        dto.setToken(RandomStringUtils.random(30));
        dto.setPassword(RandomStringUtils.random(30));
        dto.setLogin(RandomStringUtils.random(30));

        ExternalSignatureConfig entity = modelMapper.map(dto, ExternalSignatureConfig.class);
        assertNotNull(entity);
        assertDeepEquals(entity, dto);
    }


    @Test
    public void convertDtoToEntity_empty() {

        ExternalSignatureConfigDto dto = new ExternalSignatureConfigDto();

        ExternalSignatureConfig entity = modelMapper.map(dto, ExternalSignatureConfig.class);
        assertNotNull(entity);
        assertDeepEquals(entity, dto);
    }


}
