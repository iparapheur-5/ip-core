/*
 * iparapheur Core
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.ipcore.model.database.requests;

import coop.libriciel.ipcore.model.database.*;
import coop.libriciel.ipcore.model.database.SubtypeMetadata.CompositeId;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.Test;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;


@SpringBootTest
class SubtypeMetadataDtoTest {


    private @Autowired ModelMapper modelMapper;


    private static void assertDeepEquals(SubtypeMetadata entity, SubtypeMetadataDto dto) {
        assertEquals(Optional.ofNullable(entity.getSubtype()).map(Subtype::getId).orElse(null), dto.getSubtypeId());
        assertEquals(Optional.ofNullable(entity.getId()).map(CompositeId::getSubtypeId).orElse(null), dto.getSubtypeId());
        assertEquals(Optional.ofNullable(entity.getId()).map(CompositeId::getMetadataId).orElse(null), dto.getMetadataId());
        // assertEquals(Optional.ofNullable(entity.getMetadata()).map(Metadata::getId).orElse(null), dto.getMetadataId());
        // assertEquals(Optional.ofNullable(entity.getMetadata()).map(Metadata::getName).orElse(null), dto.getMetadata().getName());
        assertEquals(entity.getDefaultValue(), dto.getDefaultValue());
        assertEquals(entity.isMandatory(), dto.isMandatory());
        assertEquals(entity.isEditable(), dto.isEditable());
    }


    @Test
    public void convertEntityToDto_full() {

        String subtypeId = UUID.randomUUID().toString();
        String metadataId = UUID.randomUUID().toString();

        SubtypeMetadata entity = new SubtypeMetadata();
        entity.setId(new CompositeId(subtypeId, metadataId));
        entity.setSubtype(new Subtype(subtypeId));
        entity.setMetadata(Metadata.builder().id(metadataId).name(RandomStringUtils.random(36)).build());
        entity.setDefaultValue(RandomStringUtils.random(30));
        entity.setMandatory(true);
        entity.setEditable(true);

        SubtypeMetadataDto dto = modelMapper.map(entity, SubtypeMetadataDto.class);
        assertNotNull(dto);
        assertDeepEquals(entity, dto);
    }


    @Test
    public void convertEntityToDto_empty() {

        SubtypeMetadata entity = new SubtypeMetadata();

        SubtypeMetadataDto dto = modelMapper.map(entity, SubtypeMetadataDto.class);
        assertNotNull(dto);
        assertDeepEquals(entity, dto);
    }


    @Test
    public void convertDtoToEntity_full() {

        String metadataId = UUID.randomUUID().toString();

        SubtypeMetadataDto dto = new SubtypeMetadataDto();
        dto.setMetadataId(metadataId);
        dto.setSubtypeId(UUID.randomUUID().toString());
        dto.setMetadata(new MetadataDto());
        dto.setDefaultValue(RandomStringUtils.random(30));
        dto.setMandatory(true);
        dto.setEditable(true);

        SubtypeMetadata entity = modelMapper.map(dto, SubtypeMetadata.class);
        assertNotNull(entity);
        assertDeepEquals(entity, dto);
    }


    @Test
    public void convertDtoToEntity_empty() {

        SubtypeMetadataDto dto = new SubtypeMetadataDto();

        SubtypeMetadata entity = modelMapper.map(dto, SubtypeMetadata.class);
        assertNotNull(entity);
        assertDeepEquals(entity, dto);
    }


}
