/*
 * iparapheur Core
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.ipcore.model.auth.requests;

import coop.libriciel.ipcore.model.auth.Desk;
import coop.libriciel.ipcore.model.auth.DeskRepresentation;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.Test;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;


@SpringBootTest
class DeskDtoTest {


    private @Autowired ModelMapper modelMapper;


    private static void assertDeepEquals(Desk entity, DeskDto dto) {
        assertEquals(entity.getId(), dto.getId());
        assertEquals(entity.getShortName(), dto.getShortName());
        assertEquals(entity.getDescription(), dto.getDescription());
        assertEquals(entity.isFolderCreationAllowed(), dto.isFolderCreationAllowed());
        assertEquals(entity.isActionAllowed(), dto.isActionAllowed());
        assertEquals(entity.isArchivingAllowed(), dto.isArchivingAllowed());
        assertEquals(entity.isChainAllowed(), dto.isChainAllowed());
        assertEquals(Optional.ofNullable(entity.getParentDesk()).map(DeskRepresentation::getId).orElse(null), dto.getParentDeskId());
    }


    @Test
    void convertEntityToDto_full() {

        Desk entity = new Desk();
        entity.setId(UUID.randomUUID().toString());
        entity.setName(RandomStringUtils.random(30));
        entity.setShortName(RandomStringUtils.random(30));
        entity.setDescription(RandomStringUtils.random(30));
        entity.setFolderCreationAllowed(true);
        entity.setActionAllowed(true);
        entity.setArchivingAllowed(true);
        entity.setChainAllowed(true);
        entity.setParentDesk(new DeskRepresentation(UUID.randomUUID().toString(), RandomStringUtils.random(30)));

        DeskDto dto = modelMapper.map(entity, DeskDto.class);
        assertNotNull(dto);
        assertDeepEquals(entity, dto);
    }


    @Test
    void convertEntityToDto_empty_just_name() {

        Desk entity = new Desk();
        entity.setName(RandomStringUtils.random(30));
        DeskDto dto = modelMapper.map(entity, DeskDto.class);
        assertNotNull(dto);
        assertDeepEquals(entity, dto);
    }


    @Test
    void convertDtoToEntity_full() {

        DeskDto dto = new DeskDto();
        dto.setId(UUID.randomUUID().toString());
        dto.setShortName(RandomStringUtils.random(30));
        dto.setDescription(RandomStringUtils.random(30));
        dto.setFolderCreationAllowed(true);
        dto.setActionAllowed(true);
        dto.setArchivingAllowed(true);
        dto.setChainAllowed(true);
        dto.setParentDeskId(UUID.randomUUID().toString());

        Desk entity = modelMapper.map(dto, Desk.class);
        assertNotNull(entity);
        assertDeepEquals(entity, dto);
    }


    @Test
    void convertDtoToEntity_empty() {

        DeskDto dto = new DeskDto();

        Desk entity = modelMapper.map(dto, Desk.class);
        assertNotNull(entity);
        assertDeepEquals(entity, dto);
    }


}
