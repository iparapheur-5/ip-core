/*
 * iparapheur Core
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.ipcore.model.auth.requests;

import coop.libriciel.ipcore.model.auth.DeskRepresentation;
import coop.libriciel.ipcore.model.auth.User;
import coop.libriciel.ipcore.model.database.TenantRepresentation;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.Test;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.UUID;

import static coop.libriciel.ipcore.model.auth.UserPrivilege.FUNCTIONAL_ADMIN;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;


@SpringBootTest
class UserDtoTest {


    private @Autowired ModelMapper modelMapper;


    private static void assertDeepEquals(User entity, UserDto dto) {
        assertEquals(entity.getUserName(), dto.getUserName());
        assertEquals(entity.getFirstName(), dto.getFirstName());
        assertEquals(entity.getLastName(), dto.getLastName());
        assertEquals(entity.getEmail(), dto.getEmail());
        assertEquals(entity.getPrivilege(), dto.getPrivilege());
        assertEquals(entity.getAdministeredTenants(), dto.getAdministeredTenants());
        assertEquals(entity.getAdministeredDesks(), dto.getAdministeredDesks());
        assertEquals(entity.getSupervisedDesks(), dto.getSupervisedDesks());
        assertEquals(entity.getComplementaryField(), dto.getComplementaryField());
    }


    @Test
    void convertEntityToDto_full() {

        User entity = new User();
        entity.setId(UUID.randomUUID().toString());
        entity.setUserName(RandomStringUtils.random(30));
        entity.setFirstName(RandomStringUtils.random(30));
        entity.setLastName(RandomStringUtils.random(30));
        entity.setEmail(RandomStringUtils.random(30, true, true) + "@dom.local");
        entity.setPrivilege(FUNCTIONAL_ADMIN);
        entity.setNotificationsCronFrequency(RandomStringUtils.random(30));
        entity.setNotificationsRedirectionMail(RandomStringUtils.random(30));
        entity.setComplementaryField(RandomStringUtils.random(30));
        entity.setAdministeredTenants(List.of(
                new TenantRepresentation(UUID.randomUUID().toString(), RandomStringUtils.random(30)),
                new TenantRepresentation(UUID.randomUUID().toString(), RandomStringUtils.random(30))
        ));
        entity.setAdministeredDesks(List.of(
                new DeskRepresentation(UUID.randomUUID().toString(), RandomStringUtils.random(30)),
                new DeskRepresentation(UUID.randomUUID().toString(), RandomStringUtils.random(30))
        ));
        entity.setSupervisedDesks(List.of(
                new DeskRepresentation(UUID.randomUUID().toString(), RandomStringUtils.random(30)),
                new DeskRepresentation(UUID.randomUUID().toString(), RandomStringUtils.random(30))
        ));

        UserDto dto = modelMapper.map(entity, UserDto.class);
        assertNotNull(dto);
        assertDeepEquals(entity, dto);
    }


    @Test
    void convertEntityToDto_empty() {

        User entity = new User();

        UserDto dto = modelMapper.map(entity, UserDto.class);
        assertNotNull(dto);
        assertDeepEquals(entity, dto);
    }


    @Test
    void convertDtoToEntity_full() {

        UserDto dto = new UserDto();
        dto.setUserName(RandomStringUtils.random(30));
        dto.setFirstName(RandomStringUtils.random(30));
        dto.setLastName(RandomStringUtils.random(30));
        dto.setEmail(RandomStringUtils.random(30, true, true) + "@dom.local");
        dto.setPrivilege(FUNCTIONAL_ADMIN);
        dto.setComplementaryField(RandomStringUtils.random(30));

        dto.setAdministeredTenants(List.of(
                new TenantRepresentation(UUID.randomUUID().toString(), RandomStringUtils.random(30)),
                new TenantRepresentation(UUID.randomUUID().toString(), RandomStringUtils.random(30))
        ));
        dto.setAdministeredDesks(List.of(
                new DeskRepresentation(UUID.randomUUID().toString(), RandomStringUtils.random(30)),
                new DeskRepresentation(UUID.randomUUID().toString(), RandomStringUtils.random(30))
        ));
        dto.setSupervisedDesks(List.of(
                new DeskRepresentation(UUID.randomUUID().toString(), RandomStringUtils.random(30)),
                new DeskRepresentation(UUID.randomUUID().toString(), RandomStringUtils.random(30))
        ));

        User entity = modelMapper.map(dto, User.class);
        assertNotNull(entity);
        assertDeepEquals(entity, dto);
    }


    @Test
    void convertDtoToEntity_empty() {

        UserDto dto = new UserDto();

        User entity = modelMapper.map(dto, User.class);
        assertNotNull(entity);
        assertDeepEquals(entity, dto);
    }


}