/*
 * iparapheur Core
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.ipcore.model.workflow;

import org.junit.jupiter.api.Test;

import static coop.libriciel.ipcore.model.workflow.Action.VISA;
import static coop.libriciel.ipcore.model.workflow.State.PENDING;
import static coop.libriciel.ipcore.model.workflow.State.UPCOMING;
import static org.junit.jupiter.api.Assertions.assertEquals;


class TaskTest {


    @Test
    void noArgsConstructor() {
        Task task = new Task();
        assertEquals(VISA, task.getAction());
        assertEquals(UPCOMING, task.getState());
    }


    @Test
    void builder() {
        Task task = Task.builder().build();
        assertEquals(VISA, task.getAction());
        assertEquals(PENDING, task.getState());
    }


}
