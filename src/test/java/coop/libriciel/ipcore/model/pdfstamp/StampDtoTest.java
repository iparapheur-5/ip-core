/*
 * iparapheur Core
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.ipcore.model.pdfstamp;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.Test;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Random;
import java.util.UUID;

import static coop.libriciel.ipcore.model.pdfstamp.StampTextColor.RED;
import static coop.libriciel.ipcore.model.pdfstamp.StampType.TEXT;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;


@SpringBootTest
class StampDtoTest {


    private @Autowired ModelMapper modelMapper;


    private static void assertDeepEquals(Stamp entity, StampDto dto) {
        assertEquals(entity.getId(), dto.getId());
        assertEquals(entity.getPage(), dto.getPage());
        assertEquals(entity.getX(), dto.getX());
        assertEquals(entity.getY(), dto.getY());
        assertEquals(entity.getSignatureRank(), dto.getSignatureRank());
        assertEquals(entity.isAfterSignature(), dto.isAfterSignature());
        assertEquals(entity.getType(), dto.getType());
        assertEquals(entity.getValue(), dto.getValue());
        assertEquals(entity.getFontSize(), dto.getFontSize());
        assertEquals(entity.getTextColor(), dto.getTextColor());
    }


    @Test
    public void convertEntityToDto_full() {

        Stamp entity = new Stamp();
        entity.setId(UUID.randomUUID().toString());
        entity.setParentLayer(new Layer(UUID.randomUUID().toString()));
        entity.setId(UUID.randomUUID().toString());
        entity.setPage(new Random().nextInt());
        entity.setX(new Random().nextInt());
        entity.setY(new Random().nextInt());
        entity.setSignatureRank(new Random().nextInt());
        entity.setAfterSignature(true);
        entity.setType(TEXT);
        entity.setValue(RandomStringUtils.random(250));
        entity.setFontSize(new Random().nextInt());
        entity.setTextColor(RED);

        StampDto dto = modelMapper.map(entity, StampDto.class);
        assertNotNull(dto);
        assertDeepEquals(entity, dto);
    }


    @Test
    public void convertEntityToDto_empty() {

        Stamp entity = new Stamp();

        StampDto dto = modelMapper.map(entity, StampDto.class);
        assertNotNull(dto);
        assertDeepEquals(entity, dto);
    }


    @Test
    public void convertDtoToEntity_full() {

        StampDto dto = new StampDto();
        dto.setId(UUID.randomUUID().toString());
        dto.setPage(new Random().nextInt());
        dto.setX(new Random().nextInt());
        dto.setY(new Random().nextInt());
        dto.setSignatureRank(new Random().nextInt());
        dto.setAfterSignature(true);
        dto.setType(TEXT);
        dto.setValue(RandomStringUtils.random(250));
        dto.setFontSize(new Random().nextInt());
        dto.setTextColor(RED);

        Stamp entity = modelMapper.map(dto, Stamp.class);
        assertNotNull(entity);
        assertDeepEquals(entity, dto);
    }


    @Test
    public void convertDtoToEntity_empty() {

        StampDto dto = new StampDto();

        Stamp entity = modelMapper.map(dto, Stamp.class);
        assertNotNull(entity);
        assertDeepEquals(entity, dto);
    }


}