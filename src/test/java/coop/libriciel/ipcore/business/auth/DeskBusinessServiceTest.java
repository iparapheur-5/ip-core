/*
 * iparapheur Core
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.ipcore.business.auth;

import coop.libriciel.ipcore.model.auth.User;
import coop.libriciel.ipcore.model.auth.requests.DeskDto;
import coop.libriciel.ipcore.services.auth.AuthServiceInterface;
import coop.libriciel.ipcore.services.auth.KeycloakService;
import coop.libriciel.ipcore.services.database.SubtypeRepository;
import coop.libriciel.ipcore.services.database.TypeRepository;
import coop.libriciel.ipcore.services.permission.KeycloakResourceService;
import coop.libriciel.ipcore.services.permission.PermissionServiceInterface;
import coop.libriciel.ipcore.services.workflow.IpWorkflowService;
import coop.libriciel.ipcore.services.workflow.WorkflowServiceInterface;
import coop.libriciel.ipcore.utils.LocalizedStatusException;
import coop.libriciel.ipcore.utils.PaginatedList;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.modelmapper.ModelMapper;

import java.util.List;

import static coop.libriciel.ipcore.utils.ApiUtils.MAX_USER_PER_DESKS_COUNT;
import static coop.libriciel.ipcore.utils.PaginatedList.emptyPaginatedList;
import static java.util.Collections.emptyList;
import static java.util.UUID.randomUUID;
import static java.util.stream.IntStream.range;
import static org.junit.Assert.assertThrows;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;


public class DeskBusinessServiceTest {

    private final String tenantId = "fakeTenantId";
    private final String deskId = "fakeDeskId";

    private DeskBusinessService deskBusinessService;

    private @Mock AuthServiceInterface authServiceMock = mock(KeycloakService.class);
    private @Mock SubtypeRepository subtypeRepositoryMock = mock(SubtypeRepository.class);
    private @Mock TypeRepository typeRepositoryMock = mock(TypeRepository.class);
    private @Mock PermissionServiceInterface permissionServiceMock = mock(KeycloakResourceService.class);
    private @Mock WorkflowServiceInterface workflowServiceMock = mock(IpWorkflowService.class);


    @BeforeEach
    void setup() {
        deskBusinessService = new DeskBusinessService(
                authServiceMock,
                new ModelMapper(),
                permissionServiceMock,
                subtypeRepositoryMock,
                typeRepositoryMock,
                workflowServiceMock
        );
    }


    // <editor-fold desc="updateDeskUsers">


    @Test
    void updateDeskUsersTestEmpty() {
        DeskDto request = new DeskDto();
        request.setOwnerIds(emptyList());

        when(authServiceMock.listUsersFromDesk(eq(tenantId), eq(deskId), anyInt(), anyInt()))
                .thenReturn(emptyPaginatedList());

        deskBusinessService.updateDeskUsers(request, tenantId, deskId);

        verify(authServiceMock, times(1)).addUsersToDesk(eq(deskId), eq(emptyList()));
        verify(authServiceMock, times(1)).removeUsersFromDesk(eq(deskId), eq(emptyList()));
    }


    @Test
    void updateDeskUsersTestAddOne() {
        DeskDto request = new DeskDto();
        request.setOwnerIds(List.of("1"));

        when(authServiceMock.listUsersFromDesk(eq(tenantId), eq(deskId), anyInt(), anyInt()))
                .thenReturn(emptyPaginatedList());

        deskBusinessService.updateDeskUsers(request, tenantId, deskId);

        verify(authServiceMock, times(1)).addUsersToDesk(eq(deskId), eq(List.of("1")));
        verify(authServiceMock, times(1)).removeUsersFromDesk(eq(deskId), eq(emptyList()));
    }


    @Test
    void updateDeskUsersTestRemoveOne() {
        DeskDto request = new DeskDto();
        request.setOwnerIds(List.of("1"));

        when(authServiceMock.listUsersFromDesk(eq(tenantId), eq(deskId), anyInt(), anyInt()))
                .thenReturn(PaginatedList.of(new User("1"), new User("2")));

        deskBusinessService.updateDeskUsers(request, tenantId, deskId);

        verify(authServiceMock, times(1)).addUsersToDesk(eq(deskId), eq(emptyList()));
        verify(authServiceMock, times(1)).removeUsersFromDesk(eq(deskId), eq(List.of("2")));
    }


    @Test
    void updateDeskUsersTestAddAndRemove() {
        DeskDto request = new DeskDto();
        request.setOwnerIds(List.of("1", "2", "3", "4"));

        when(authServiceMock.listUsersFromDesk(eq(tenantId), eq(deskId), anyInt(), anyInt()))
                .thenReturn(PaginatedList.of(new User("1"), new User("3"), new User("5")));

        deskBusinessService.updateDeskUsers(request, tenantId, deskId);

        verify(authServiceMock, times(1)).addUsersToDesk(eq(deskId), eq(List.of("2", "4")));
        verify(authServiceMock, times(1)).removeUsersFromDesk(eq(deskId), eq(List.of("5")));
    }


    @Test
    void updateDeskUsersTestThrows() {
        DeskDto request = new DeskDto();
        request.setOwnerIds(range(0, MAX_USER_PER_DESKS_COUNT + 1).mapToObj(i -> randomUUID().toString()).toList());

        when(authServiceMock.listUsersFromDesk(eq(tenantId), eq(deskId), anyInt(), anyInt()))
                .thenReturn(emptyPaginatedList());

        assertThrows(LocalizedStatusException.class, () -> deskBusinessService.updateDeskUsers(request, tenantId, deskId));
    }


    // </editor-fold desc="updateDeskUsers">


    // <editor-fold desc="updateDeskAssociatedDesks">


    @Test
    void updateDeskAssociatedDesksTestEmpty() {
        DeskDto request = new DeskDto();
        request.setAssociatedDeskIds(emptyList());

        when(permissionServiceMock.getAssociatedDesks(eq(tenantId), eq(deskId)))
                .thenReturn(emptyList());

        deskBusinessService.updateDeskAssociatedDesks(request, tenantId, deskId);

        verify(permissionServiceMock, times(1)).associateDesks(eq(tenantId), eq(deskId), eq(emptyList()));
        verify(permissionServiceMock, times(1)).removeAssociatedDesks(eq(tenantId), eq(deskId), eq(emptyList()));
    }


    @Test
    void updateDeskAssociatedDesksTestAddOne() {
        DeskDto request = new DeskDto();
        request.setAssociatedDeskIds(List.of("1"));

        when(permissionServiceMock.getAssociatedDesks(eq(tenantId), eq(deskId)))
                .thenReturn(emptyList());

        deskBusinessService.updateDeskAssociatedDesks(request, tenantId, deskId);

        verify(permissionServiceMock, times(1)).associateDesks(eq(tenantId), eq(deskId), eq(List.of("1")));
        verify(permissionServiceMock, times(1)).removeAssociatedDesks(eq(tenantId), eq(deskId), eq(emptyList()));
    }


    @Test
    void updateDeskAssociatedDesksTestRemoveOne() {
        DeskDto request = new DeskDto();
        request.setAssociatedDeskIds(List.of("1"));

        when(permissionServiceMock.getAssociatedDesks(eq(tenantId), eq(deskId)))
                .thenReturn(List.of("1", "2"));

        deskBusinessService.updateDeskAssociatedDesks(request, tenantId, deskId);

        verify(permissionServiceMock, times(1)).associateDesks(eq(tenantId), eq(deskId), eq(emptyList()));
        verify(permissionServiceMock, times(1)).removeAssociatedDesks(eq(tenantId), eq(deskId), eq(List.of("2")));
    }


    @Test
    void updateDeskAssociatedDesksTestAddAndRemove() {
        DeskDto request = new DeskDto();
        request.setAssociatedDeskIds(List.of("1", "2", "3", "4"));

        when(permissionServiceMock.getAssociatedDesks(eq(tenantId), eq(deskId)))
                .thenReturn(List.of("1", "3", "5"));

        deskBusinessService.updateDeskAssociatedDesks(request, tenantId, deskId);

        verify(permissionServiceMock, times(1)).associateDesks(eq(tenantId), eq(deskId), eq(List.of("2", "4")));
        verify(permissionServiceMock, times(1)).removeAssociatedDesks(eq(tenantId), eq(deskId), eq(List.of("5")));
    }


    @Test
    void updateDeskAssociatedDesksTestThrows() {
        DeskDto request = new DeskDto();
        request.setAssociatedDeskIds(range(0, MAX_USER_PER_DESKS_COUNT + 1).mapToObj(i -> randomUUID().toString()).toList());

        when(permissionServiceMock.getAssociatedDesks(eq(tenantId), eq(deskId)))
                .thenReturn(emptyList());

        assertThrows(LocalizedStatusException.class, () -> deskBusinessService.updateDeskAssociatedDesks(request, tenantId, deskId));
    }


    // </editor-fold desc="updateDeskAssociatedDesks">


    // <editor-fold desc="updateDeskFilterableMetadata">


    @Test
    void updateDeskFilterableMetadataTestEmpty() {
        DeskDto request = new DeskDto();
        request.setFilterableMetadataIds(emptyList());

        when(permissionServiceMock.getFilterableMetadataIds(eq(tenantId), eq(deskId)))
                .thenReturn(emptyList());

        deskBusinessService.updateDeskFilterableMetadata(request, tenantId, deskId);

        verify(permissionServiceMock, times(1)).addFilterableMetadata(eq(tenantId), eq(deskId), eq(emptyList()));
        verify(permissionServiceMock, times(1)).removeFilterableMetadata(eq(tenantId), eq(deskId), eq(emptyList()));
    }


    @Test
    void updateDeskFilterableMetadataTestAddOne() {
        DeskDto request = new DeskDto();
        request.setFilterableMetadataIds(List.of("1"));

        when(permissionServiceMock.getFilterableMetadataIds(eq(tenantId), eq(deskId)))
                .thenReturn(emptyList());

        deskBusinessService.updateDeskFilterableMetadata(request, tenantId, deskId);

        verify(permissionServiceMock, times(1)).addFilterableMetadata(eq(tenantId), eq(deskId), eq(List.of("1")));
        verify(permissionServiceMock, times(1)).removeFilterableMetadata(eq(tenantId), eq(deskId), eq(emptyList()));
    }


    @Test
    void updateDeskFilterableMetadataTestRemoveOne() {
        DeskDto request = new DeskDto();
        request.setFilterableMetadataIds(List.of("1"));

        when(permissionServiceMock.getFilterableMetadataIds(eq(tenantId), eq(deskId)))
                .thenReturn(List.of("1", "2"));

        deskBusinessService.updateDeskFilterableMetadata(request, tenantId, deskId);

        verify(permissionServiceMock, times(1)).addFilterableMetadata(eq(tenantId), eq(deskId), eq(emptyList()));
        verify(permissionServiceMock, times(1)).removeFilterableMetadata(eq(tenantId), eq(deskId), eq(List.of("2")));
    }


    @Test
    void updateDeskFilterableMetadataTestAddAndRemove() {
        DeskDto request = new DeskDto();
        request.setFilterableMetadataIds(List.of("1", "2", "3", "4"));

        when(permissionServiceMock.getFilterableMetadataIds(eq(tenantId), eq(deskId)))
                .thenReturn(List.of("1", "3", "5"));

        deskBusinessService.updateDeskFilterableMetadata(request, tenantId, deskId);

        verify(permissionServiceMock, times(1)).addFilterableMetadata(eq(tenantId), eq(deskId), eq(List.of("2", "4")));
        verify(permissionServiceMock, times(1)).removeFilterableMetadata(eq(tenantId), eq(deskId), eq(List.of("5")));
    }


    @Test
    void updateDeskFilterableMetadataTestThrows() {
        DeskDto request = new DeskDto();
        request.setFilterableMetadataIds(range(0, MAX_USER_PER_DESKS_COUNT + 1).mapToObj(i -> randomUUID().toString()).toList());

        when(permissionServiceMock.getFilterableMetadataIds(eq(tenantId), eq(deskId)))
                .thenReturn(emptyList());

        assertThrows(LocalizedStatusException.class, () -> deskBusinessService.updateDeskFilterableMetadata(request, tenantId, deskId));
    }


    // </editor-fold desc="updateDeskFilterableMetadata">


    // <editor-fold desc="updateDeskSupervisors">


    @Test
    void updateDeskSupervisorsTestEmpty() {
        DeskDto request = new DeskDto();
        request.setSupervisorIds(emptyList());

        when(permissionServiceMock.getSupervisorsFromDesk(eq(tenantId), eq(deskId)))
                .thenReturn(emptyList());

        deskBusinessService.updateDeskSupervisors(request, tenantId, deskId);

        verify(permissionServiceMock, times(1)).addSupervisorsToDesk(eq(tenantId), eq(deskId), eq(emptyList()));
        verify(permissionServiceMock, times(1)).removeSupervisorsFromDesk(eq(tenantId), eq(deskId), eq(emptyList()));
    }


    @Test
    void updateDeskSupervisorsTestAddOne() {
        DeskDto request = new DeskDto();
        request.setSupervisorIds(List.of("1"));

        when(permissionServiceMock.getSupervisorsFromDesk(eq(tenantId), eq(deskId)))
                .thenReturn(emptyList());

        deskBusinessService.updateDeskSupervisors(request, tenantId, deskId);

        verify(permissionServiceMock, times(1)).addSupervisorsToDesk(eq(tenantId), eq(deskId), eq(List.of("1")));
        verify(permissionServiceMock, times(1)).removeSupervisorsFromDesk(eq(tenantId), eq(deskId), eq(emptyList()));
    }


    @Test
    void updateDeskSupervisorsTestRemoveOne() {
        DeskDto request = new DeskDto();
        request.setSupervisorIds(List.of("1"));

        when(permissionServiceMock.getSupervisorsFromDesk(eq(tenantId), eq(deskId)))
                .thenReturn(List.of("1", "2"));

        deskBusinessService.updateDeskSupervisors(request, tenantId, deskId);

        verify(permissionServiceMock, times(1)).addSupervisorsToDesk(eq(tenantId), eq(deskId), eq(emptyList()));
        verify(permissionServiceMock, times(1)).removeSupervisorsFromDesk(eq(tenantId), eq(deskId), eq(List.of("2")));
    }


    @Test
    void updateDeskSupervisorsTestAddAndRemove() {
        DeskDto request = new DeskDto();
        request.setSupervisorIds(List.of("1", "2", "3", "4"));

        when(permissionServiceMock.getSupervisorsFromDesk(eq(tenantId), eq(deskId)))
                .thenReturn(List.of("1", "3", "5"));

        deskBusinessService.updateDeskSupervisors(request, tenantId, deskId);

        verify(permissionServiceMock, times(1)).addSupervisorsToDesk(eq(tenantId), eq(deskId), eq(List.of("2", "4")));
        verify(permissionServiceMock, times(1)).removeSupervisorsFromDesk(eq(tenantId), eq(deskId), eq(List.of("5")));
    }


    @Test
    void updateDeskSupervisorsTestThrows() {
        DeskDto request = new DeskDto();
        request.setSupervisorIds(range(0, MAX_USER_PER_DESKS_COUNT + 1).mapToObj(i -> randomUUID().toString()).toList());

        when(permissionServiceMock.getSupervisorsFromDesk(eq(tenantId), eq(deskId)))
                .thenReturn(emptyList());

        assertThrows(LocalizedStatusException.class, () -> deskBusinessService.updateDeskSupervisors(request, tenantId, deskId));
    }


    // </editor-fold desc="updateDeskSupervisors">


    // <editor-fold desc="updateDeskDelegations">


    @Test
    void updateDeskDelegationsTestEmpty() {
        DeskDto request = new DeskDto();
        request.setDelegationManagerIds(emptyList());

        when(permissionServiceMock.getDelegationManagerOfDesk(eq(tenantId), eq(deskId)))
                .thenReturn(emptyList());

        deskBusinessService.updateDeskDelegations(request, tenantId, deskId);

        verify(permissionServiceMock, times(1)).addDelegationManagersToDesk(eq(tenantId), eq(deskId), eq(emptyList()));
        verify(permissionServiceMock, times(1)).removeDelegationManagersFromDesk(eq(tenantId), eq(deskId), eq(emptyList()));
    }


    @Test
    void updateDeskDelegationsTestAddOne() {
        DeskDto request = new DeskDto();
        request.setDelegationManagerIds(List.of("1"));

        when(permissionServiceMock.getDelegationManagerOfDesk(eq(tenantId), eq(deskId)))
                .thenReturn(emptyList());

        deskBusinessService.updateDeskDelegations(request, tenantId, deskId);

        verify(permissionServiceMock, times(1)).addDelegationManagersToDesk(eq(tenantId), eq(deskId), eq(List.of("1")));
        verify(permissionServiceMock, times(1)).removeDelegationManagersFromDesk(eq(tenantId), eq(deskId), eq(emptyList()));
    }


    @Test
    void updateDeskDelegationsTestRemoveOne() {
        DeskDto request = new DeskDto();
        request.setDelegationManagerIds(List.of("1"));

        when(permissionServiceMock.getDelegationManagerOfDesk(eq(tenantId), eq(deskId)))
                .thenReturn(List.of("1", "2"));

        deskBusinessService.updateDeskDelegations(request, tenantId, deskId);

        verify(permissionServiceMock, times(1)).addDelegationManagersToDesk(eq(tenantId), eq(deskId), eq(emptyList()));
        verify(permissionServiceMock, times(1)).removeDelegationManagersFromDesk(eq(tenantId), eq(deskId), eq(List.of("2")));
    }


    @Test
    void updateDeskDelegationsTestAddAndRemove() {
        DeskDto request = new DeskDto();
        request.setDelegationManagerIds(List.of("1", "2", "3", "4"));

        when(permissionServiceMock.getDelegationManagerOfDesk(eq(tenantId), eq(deskId)))
                .thenReturn(List.of("1", "3", "5"));

        deskBusinessService.updateDeskDelegations(request, tenantId, deskId);

        verify(permissionServiceMock, times(1)).addDelegationManagersToDesk(eq(tenantId), eq(deskId), eq(List.of("2", "4")));
        verify(permissionServiceMock, times(1)).removeDelegationManagersFromDesk(eq(tenantId), eq(deskId), eq(List.of("5")));
    }


    @Test
    void updateDeskDelegationsTestThrows() {
        DeskDto request = new DeskDto();
        request.setDelegationManagerIds(range(0, MAX_USER_PER_DESKS_COUNT + 1).mapToObj(i -> randomUUID().toString()).toList());

        when(permissionServiceMock.getDelegationManagerOfDesk(eq(tenantId), eq(deskId)))
                .thenReturn(emptyList());

        assertThrows(LocalizedStatusException.class, () -> deskBusinessService.updateDeskDelegations(request, tenantId, deskId));
    }


    // </editor-fold desc="updateDeskDelegations">

}
