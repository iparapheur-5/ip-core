/*
 * iparapheur Core
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package coop.libriciel.ipcore.services.workflow;

import coop.libriciel.ipcore.model.auth.DeskRepresentation;
import coop.libriciel.ipcore.model.database.TenantRepresentation;
import coop.libriciel.ipcore.model.database.userPreferences.FolderFilter;
import coop.libriciel.ipcore.model.ipng.IpngProof;
import coop.libriciel.ipcore.model.ipng.IpngProofWrap;
import coop.libriciel.ipcore.model.ipng.PendingIpngFolder;
import coop.libriciel.ipcore.model.workflow.*;
import coop.libriciel.ipcore.model.workflow.requests.SimpleTaskParams;
import coop.libriciel.ipcore.utils.PaginatedList;
import lombok.extern.log4j.Log4j2;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import static coop.libriciel.ipcore.utils.PaginatedList.emptyPaginatedList;
import static coop.libriciel.ipcore.utils.TextUtilsTest.DUMMY_SERVICE;
import static java.util.Collections.emptyList;
import static java.util.Collections.emptyMap;


@Log4j2
@Service(WorkflowServiceInterface.BEAN_NAME)
@ConditionalOnProperty(name = WorkflowServiceInterface.PREFERENCES_PROVIDER_KEY, havingValue = DUMMY_SERVICE)
public class DummyWorkflowService implements WorkflowServiceInterface {


    // <editor-fold desc="Workflow definition CRUDL">


    @Override
    public void createWorkflowDefinition(@NotNull String tenantKey, @NotNull String workflowDefinition, @NotNull String workflowName) {}


    @Override
    public void createWorkflowDefinition(@NotNull String tenantKey, @NotNull WorkflowDefinition workflowDefinition) {}


    @Override
    public @Nullable WorkflowDefinition getWorkflowDefinitionById(@NotNull String tenantKey, @NotNull String id) {
        return null;
    }


    @Override
    public @NotNull Optional<WorkflowDefinition> getWorkflowDefinitionByKey(@NotNull String tenantId,
                                                                            @NotNull String key,
                                                                            @Nullable String originDeskId) {
        return Optional.empty();
    }


    @Override
    public void updateWorkflowDefinition(@NotNull String tenantKey, @NotNull String fullId, @NotNull WorkflowDefinition workflowDefinition) {}


    @Override
    public void deleteWorkflowDefinition(@NotNull String workflowId) {}


    @Override
    public @NotNull Page<WorkflowDefinitionRepresentation> listWorkflowDefinitions(@NotNull String tenantKey,
                                                                                   @NotNull Pageable pageable,
                                                                                   @Nullable String searchTerm) {
        return Page.empty();
    }


    // </editor-fold desc="Workflow definition CRUDL">


    // <editor-fold desc="Lists">


    @Override
    public @NotNull PaginatedList<? extends Folder> listFolders(@NotNull String tenantId, int page, int pageSize, FolderSortBy sortBy,
                                                                @Nullable FolderFilter folderFilter, boolean asc, @Nullable String deskId,
                                                                @Nullable String searchTerm, @Nullable Long emitBeforeTime,
                                                                @Nullable Long stillSinceTime, @Nullable State state) {
        return emptyPaginatedList();
    }


    @Override
    public @NotNull PaginatedList<? extends Folder> listFoldersByState(@NotNull String role,
                                                                       @NotNull State state,
                                                                       @NotNull List<DelegationRule> delegationRules,
                                                                       @Nullable FolderSortBy sortBy,
                                                                       @Nullable FolderFilter folderFilter,
                                                                       boolean asc,
                                                                       int page,
                                                                       int pageSize) {
        return emptyPaginatedList();
    }


    @Override
    public @NotNull PaginatedList<? extends Folder> listFoldersForDesks(@NotNull String tenantId,
                                                                        int page,
                                                                        int pageSize,
                                                                        FolderSortBy sortBy,
                                                                        @Nullable FolderFilter folderFilter,
                                                                        boolean asc,
                                                                        @NotNull List<String> deskIds,
                                                                        @Nullable String searchTerm,
                                                                        @Nullable Long emitBeforeTime,
                                                                        @Nullable Long stillSinceTime,
                                                                        @Nullable State state) {
        return emptyPaginatedList();
    }


    @Override
    public @NotNull Map<DelegationRule, Integer> countFolders(@NotNull Set<String> roles, @NotNull List<DelegationRule> delegationRules) {return emptyMap();}


    @Override
    public @NotNull Integer countFolders(@NotNull String role, @NotNull State state) {
        return -1;
    }


    // </editor-fold desc="Lists">


    @Override
    public @NotNull Folder createDraftWorkflow(@NotNull String tenantId, @NotNull Folder folder, Map<Integer, String> variableDesksIds) {return new Folder();}


    @Override
    public @Nullable Folder getFolder(@NotNull String id, @NotNull String tenantId) {return null;}


    @Override
    public @Nullable Folder getFolder(@NotNull String id, @NotNull String tenantId, boolean withHistory) {
        return null;
    }


    @Override
    public void editFolder(@NotNull String folderId, @NotNull FolderDto request) {}


    @Override
    public void editFolderValidationWorkflow(@NotNull String folderId, @NotNull WorkflowDefinitionDto newValidationWorkflow) {}


    @Override
    public @NotNull List<Task> getHistoricTasks(@NotNull String folderId) {return emptyList();}


    @Override
    public @NotNull List<Task> getReadTasks(@NotNull Folder folder) {return emptyList();}


    @Override
    public @NotNull Task getTask(@NotNull String taskId) {return new Task();}


    @Override
    public void performTask(@NotNull Task task,
                            @NotNull Action action,
                            @NotNull String userId,
                            @NotNull Folder folder,
                            @Nullable String deskId,
                            @Nullable String overriddenCreationWorkflowDefinitionId,
                            @Nullable String overriddenValidationWorkflowDefinitionId,
                            @Nullable String overriddenOriginDeskId,
                            @Nullable String overriddenFinalDeskId,
                            @Nullable String transactionId,
                            @Nullable String pastellDocumentId,
                            @Nullable String ipngBusinessId,
                            @Nullable SimpleTaskParams simpleTaskParams,
                            @Nullable String targetDeskId,
                            @Nullable String certBase64) {}


    @Override
    public void deleteWorkflow(@NotNull String folderId) {}


    // <editor-fold desc="Archives CRUDL">


    @Override
    public void deleteArchive(@NotNull String folderId) {}


    @Override
    public @NotNull PaginatedList<? extends Folder> getArchives(@NotNull String tenantId,
                                                                @NotNull Pageable pageable,
                                                                @Nullable Long stillSinceDate) {
        return emptyPaginatedList();
    }


    // </editor-fold desc="Archives CRUDL">


    @Override
    public void setFolderWaitingForIpngResponse(@NotNull IpngProof sentProof, @NotNull String tenantId, @NotNull Folder folder) {}


    @Override
    public void receivedIpngResponse(String tenantId, String deskId, Folder folder, PendingIpngFolder pendingFolder, IpngProofWrap proofData) {}


    @Override
    public void ipngProofReceiptWasReceived(String tenantId, String deskId, PendingIpngFolder pendingFolder, IpngProofWrap proofData) {}


    @Override
    public void ipngProofWasSent(String tenantId, String deskId, PendingIpngFolder pendingFolder, IpngProofWrap proofData) {}


    @Override
    public Map<String, String> computePlaceholderDesksConcreteValues(@NotNull WorkflowDefinition definition,
                                                                     @NotNull String originDeskId,
                                                                     @NotNull Map<Integer, String> variableDesksIdsMap) {
        return null;
    }


    @Override
    public void mapIndexedPlaceholdersToActualDesk(@NotNull DeskRepresentation finalDesk,
                                                   @NotNull String originDeskId,
                                                   @NotNull Map<String, String> additionalMetadata) {}


    @Override
    public void substitutePlaceholdersToIndexedPlaceholders(@NotNull WorkflowDefinition definition) {}

}
