/*
 * iparapheur Core
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.ipcore.services.workflow;

import org.junit.jupiter.api.Test;

import java.util.regex.Matcher;

import static coop.libriciel.ipcore.services.workflow.WorkflowServiceInterface.META_BOSSOF_PATTERN;
import static coop.libriciel.ipcore.services.workflow.WorkflowServiceInterface.META_VARIABLEDESK_PATTERN;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;


public class IpWorkflowServiceTest {


    @Test
    void matchBossofMetadataPatterns() {

        String bossOfMetadataKey = "i_Parapheur_internal_validation_boss_of_desk_id_1";
        String bossOfIdPlaceHolder = String.format("${%s}", bossOfMetadataKey);
        Matcher matcher = META_BOSSOF_PATTERN.matcher(bossOfIdPlaceHolder);
        assertTrue(matcher.matches());
        assertEquals(bossOfMetadataKey, matcher.group(1));
    }


    @Test
    void matchVariableDeskMetadataPatterns() {

        String variableDeskMetadataKey = "i_Parapheur_internal_validation_variable_desk_id_1";
        String variableDeskIdPlaceHolder = String.format("${%s}", variableDeskMetadataKey);
        Matcher matcher = META_VARIABLEDESK_PATTERN.matcher(variableDeskIdPlaceHolder);
        assertTrue(matcher.matches());
        assertEquals(variableDeskMetadataKey, matcher.group(1));

    }


}
