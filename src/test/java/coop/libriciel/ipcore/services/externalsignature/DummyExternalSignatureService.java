/*
 * iparapheur Core
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package coop.libriciel.ipcore.services.externalsignature;

import coop.libriciel.ipcore.model.auth.User;
import coop.libriciel.ipcore.model.content.Document;
import coop.libriciel.ipcore.model.content.DocumentBuffer;
import coop.libriciel.ipcore.model.database.ExternalSignatureConfig;
import coop.libriciel.ipcore.model.externalsignature.ExternalSignatureParams;
import coop.libriciel.ipcore.model.externalsignature.Status;
import coop.libriciel.ipcore.model.externalsignature.response.ExternalSignatureProcedure;
import coop.libriciel.ipcore.model.workflow.Folder;
import coop.libriciel.ipcore.model.workflow.Task;
import coop.libriciel.ipcore.services.database.ExternalSignatureConfigRepository;
import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.ClientResponse;

import java.util.List;

import static coop.libriciel.ipcore.utils.TextUtilsTest.DUMMY_SERVICE;
import static java.util.Collections.emptyList;
import static org.apache.commons.lang3.StringUtils.EMPTY;


@Service(ExternalSignatureInterface.BEAN_NAME)
@ConditionalOnProperty(name = ExternalSignatureInterface.PREFERENCES_PROVIDER_KEY, havingValue = DUMMY_SERVICE)
public class DummyExternalSignatureService implements ExternalSignatureInterface {


    @Override
    public void checkIfFilesAreSigned(Logger log, ExternalSignatureConfigRepository externalSignatureConfigRepository) {
        ExternalSignatureInterface.super.checkIfFilesAreSigned(log, externalSignatureConfigRepository);
    }


    @Override
    public @NotNull String createProcedure(@NotNull Folder folder,
                                           @NotNull ExternalSignatureParams externalSignatureParams,
                                           @NotNull List<Document> documents,
                                           @NotNull String subtypeId) {
        return EMPTY;
    }


    @Override
    public void revokeExternalProcedure(@NotNull ExternalSignatureConfig externalSignatureConfig, @NotNull String procedureId) {
    }


    @Override
    public @NotNull Status getProcedureStatus(@NotNull ExternalSignatureConfig externalSignatureConfig, @NotNull String procedureId) {
        return Status.ACTIVE;
    }


    @Override
    public ExternalSignatureProcedure getProcedureData(@NotNull ExternalSignatureConfig externalSignatureConfig, @NotNull String procedureId) {
        return null;
    }


    @Override
    public List<DocumentBuffer> getSignedDocumentList(@NotNull List<String> fileIds,
                                                      @NotNull ExternalSignatureConfig externalSignatureConfig,
                                                      @NotNull String procedureId) {
        return emptyList();
    }


    @Override
    public void updateFilesAndPerformTask(@NotNull ExternalSignatureConfig externalSignatureConfig, @NotNull User user, @NotNull String procedureId) {

    }


    @Override
    public void rejectFolder(@NotNull ExternalSignatureConfig externalSignatureConfig, @NotNull User user, @NotNull String procedureId) {

    }


    @Override
    public @NotNull String getProcedureRejectionReason(@NotNull ExternalSignatureConfig externalSignatureConfig, @NotNull String procedureId) {
        return EMPTY;
    }


    @Override
    public void rejectExternalProcedure(@NotNull Folder folder, @NotNull Task task) {

    }


    @Override
    public void testService(@NotNull ExternalSignatureConfig externalSignatureConfig) {}


}
