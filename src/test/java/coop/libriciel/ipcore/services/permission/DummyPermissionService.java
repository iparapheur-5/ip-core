/*
 * iparapheur Core
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.ipcore.services.permission;

import coop.libriciel.ipcore.model.auth.Desk;
import coop.libriciel.ipcore.model.auth.DeskRepresentation;
import coop.libriciel.ipcore.model.permission.DelegationDto;
import coop.libriciel.ipcore.model.workflow.DelegationRule;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import java.util.*;

import static coop.libriciel.ipcore.utils.TextUtilsTest.DUMMY_SERVICE;
import static java.util.Collections.emptyList;
import static java.util.Collections.emptyMap;


@Service(PermissionServiceInterface.BEAN_NAME)
@ConditionalOnProperty(name = PermissionServiceInterface.PREFERENCES_PROVIDER_KEY, havingValue = DUMMY_SERVICE)
public class DummyPermissionService implements PermissionServiceInterface {


    @Override
    public void deletePermission(@NotNull String resourceId) {}


    // <editor-fold desc="Desk resource CRUDL">


    @Override
    public void createDeskResource(@NotNull String tenantId, @NotNull Desk desk) {}


    @Override
    public void retrieveAndRefreshDeskPermissions(@NotNull String tenantId, @NotNull Desk desk) {}


    @Override
    public void editDeskPermission(@NotNull String tenantId, @NotNull Desk desk) {}


    @Override
    public boolean currentUserHasDirectViewingRightOnSomeDeskIn(@NotNull String tenantId,
                                                                @NotNull Set<String> deskIds,
                                                                @Nullable String typeId,
                                                                @Nullable String subtypeId) {
        return false;
    }


    @Override
    public boolean currentUserHasViewingRightOnSomeDeskIn(@NotNull String tenantId,
                                                          @NotNull Set<String> deskIds,
                                                          @Nullable String typeId,
                                                          @Nullable String subtypeId) {
        return false;
    }


    @Override
    public boolean currentUserHasArchivingRightOnSomeDeskIn(@NotNull String tenantId,
                                                            @NotNull Set<String> deskIds,
                                                            @Nullable String typeId,
                                                            @Nullable String subtypeId) {
        return false;
    }


    @Override
    public boolean currentUserHasChainingRightOnSomeDeskIn(@NotNull String tenantId,
                                                           @NotNull Set<String> deskIds,
                                                           @Nullable String typeId,
                                                           @Nullable String subtypeId) {
        return false;
    }


    @Override
    public boolean currentUserHasAdminRightOnDesk(@NotNull String tenantId, @NotNull String deskIds) {
        return false;
    }


    @Override
    public boolean currentUserHasAdminRightsOnTenant(@NotNull String tenantId) {
        return false;
    }


    @Override
    public boolean currentUserHasFolderActionRightOnSomeDeskIn(@NotNull String tenantId,
                                                               @NotNull Set<String> deskIds,
                                                               @Nullable String typeId,
                                                               @Nullable String subtypeId) {
        return false;
    }


    @Override
    public boolean currentUserHasDelegationOnSomeDeskIn(@NotNull String tenantId,
                                                        @NotNull Set<String> deskIds,
                                                        @Nullable String typeId,
                                                        @Nullable String subtypeId) {
        return false;
    }


    @Override
    public @NotNull Map<String, Set<DelegationRule>> getActiveDelegations(@NotNull Set<String> deskIds) {return emptyMap();}


    // </editor-fold desc="Desk resource CRUDL">


    // <editor-fold desc="Metadata resource CRUDL">


    @Override
    public void createMetadataResource(@NotNull String tenantId, @NotNull String metadataId) {}


    @Override
    public @NotNull List<String> getCurrentUserViewableDeskIds() {return emptyList();}


    @Override
    public void createSubtypeResource(@NotNull String tenantId, @NotNull String subtypeId) {}


    // </editor-fold desc="Metadata resource CRUDL">


    // <editor-fold desc="Delegations CRUDL">


    @Override
    public void addDelegation(@NotNull String tenantId, @NotNull String substituteDeskId, @NotNull String delegatingDeskId,
                              @Nullable String typeId, @Nullable String subtypeId, @Nullable Date beginDate, @Nullable Date endDate) {}


    @Override
    public void deleteDelegation(@NotNull String delegationId,
                                 @NotNull String tenantId,
                                 @NotNull String substituteDeskId,
                                 @NotNull String delegatingDeskId,
                                 @Nullable String typeId,
                                 @Nullable String subtypeId,
                                 @Nullable Date beginDate,
                                 @Nullable Date endDate) {}


    @Override
    public @NotNull List<DelegationDto> getDelegations(@NotNull String tenantId, @NotNull String delegatingDeskId) {return emptyList();}


    // </editor-fold desc="Delegations CRUDL">


    @Override
    public @NotNull List<DeskRepresentation> getDelegatingDesks(@NotNull String targetDesk) {return emptyList();}


    @Override
    public void setMetadataDelegations(@NotNull String deskId, @NotNull Collection<DelegationRule> metadataDelegations) {}


    // <editor-fold desc="Supervisor CRUDL">


    @Override
    public void addSupervisorsToDesk(@NotNull String tenantId, @NotNull String targetDeskId, @NotNull Collection<String> userIds) {}


    @Override
    public void removeSupervisorsFromDesk(@NotNull String tenantId, @NotNull String sourceDeskId, @NotNull Collection<String> userIds) {}


    @Override
    public void removeAllSupervisedDesks(@NotNull String tenantId, @NotNull String userId) {

    }


    @Override
    public @NotNull List<String> getSupervisorsFromDesk(@NotNull String tenantId, @NotNull String deskId) {return emptyList();}


    @Override
    public @NotNull List<DeskRepresentation> getSupervisedDesks(@NotNull String userId, @Nullable String tenantId) {return emptyList();}


    // </editor-fold desc="Supervisor CRUDL">


    // <editor-fold desc="Delegation manager CRUDL">


    @Override
    public void addDelegationManagersToDesk(@NotNull String tenantId, @NotNull String targetDeskId, @NotNull Collection<String> userIds) {}


    @Override
    public void removeDelegationManagersFromDesk(@NotNull String tenantId, @NotNull String sourceDeskId, @NotNull Collection<String> userIds) {}


    @Override
    public void removeAllDelegationManagedDesks(@NotNull String tenantId, @NotNull String userId) {

    }


    @Override
    public @NotNull List<String> getDelegationManagerOfDesk(@NotNull String tenantId, @NotNull String deskId) {return emptyList();}


    @Override
    public @NotNull List<DeskRepresentation> getDelegationManagedDesks(@NotNull String userId, @Nullable String tenantId) {return emptyList();}


    // </editor-fold desc="Delegation manager CRUDL">


    // <editor-fold desc="Functional admin CRUDL">


    @Override
    public void setFunctionalAdmin(@NotNull String tenantId, @NotNull String targetDeskId, @NotNull String userId) {}


    @Override
    public void removeFunctionalAdmin(@NotNull String tenantId, @NotNull String sourceDeskId, @NotNull String userId) {}


    @Override
    public void removeAllAdministeredDesks(@NotNull String tenantId, @NotNull String userId) {

    }


    @Override
    public @NotNull List<DeskRepresentation> getAdministeredDesks(@NotNull String userId, @Nullable String tenantId) {
        return emptyList();
    }


    // </editor-fold desc="Functional admin CRUDL">


    // <editor-fold desc="Associates CRUDL">


    @Override
    public void associateDesks(@NotNull String tenantId, @NotNull String sourceDeskId, @NotNull Collection<String> associateDeskIds) {}


    @Override
    public void removeAssociatedDesks(@NotNull String tenantId, @NotNull String sourceDeskId, @NotNull Collection<String> associateDeskIds) {}


    @Override
    public @NotNull List<String> getAssociatedDesks(@NotNull String tenantId, @NotNull String sourceDeskId) {
        return emptyList();
    }


    // </editor-fold desc="Associates CRUDL">


    // <editor-fold desc="Filterable metadata CRUDL">


    @Override
    public void addFilterableMetadata(@NotNull String tenantId, @NotNull String sourceDeskId, @NotNull Collection<String> metadataIds) {}


    @Override
    public void removeFilterableMetadata(@NotNull String tenantId, @NotNull String sourceDeskId, @NotNull Collection<String> metadataIds) {}


    @Override
    public @NotNull List<String> getFilterableMetadataIds(@NotNull String tenantId, @NotNull String sourceDeskId) {
        return emptyList();
    }


    // </editor-fold desc="Filterable metadata CRUDL">


    // <editor-fold desc="Subtype usage allowed CRUDL">


    @Override
    public void setSubtypeCreationPermittedDeskIds(@NotNull String tenantId, @NotNull String subtypeId, @NotNull Collection<String> deskIds) {}


    @Override
    public @Nullable List<String> getSubtypePermittedDeskIds(@NotNull String tenantId, @NotNull String subtypeId) {
        return null;
    }


    @Override
    public @NotNull List<String> getAllowedSubtypeIds(@NotNull String tenantId, @NotNull String sourceDeskId) {
        return emptyList();
    }


    // </editor-fold desc="Subtype usage allowed CRUDL">


    // <editor-fold desc="Filterable subtype CRUDL">


    @Override
    public void setSubtypeFilterableByDeskIds(@NotNull String tenantId, @NotNull String subtypeId, @NotNull Collection<String> deskIds) {}


    @Override
    public @Nullable List<String> getSubtypeFilterableByDeskIds(@NotNull String tenantId, @NotNull String subtypeId) {return null;}


    @Override
    public @NotNull List<String> getFilterableSubtypeIds(@NotNull String tenantId, @NotNull String sourceDeskId) {
        return emptyList();
    }


    // </editor-fold desc="Filterable subtype CRUDL">


}
