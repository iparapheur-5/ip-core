/*
 * iparapheur Core
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.ipcore.services.crypto;

import coop.libriciel.crypto.model.PdfSignatureStamp;
import coop.libriciel.crypto.model.XadesParameters;
import coop.libriciel.ipcore.model.auth.Desk;
import coop.libriciel.ipcore.model.auth.User;
import coop.libriciel.ipcore.model.database.Type;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Test;

import static coop.libriciel.crypto.model.SignatureStampElementType.IMAGE;
import static java.util.Collections.emptyMap;
import static java.util.Collections.singletonList;
import static org.junit.jupiter.api.Assertions.*;


public class CryptoServiceInterfaceTest {


    CryptoServiceInterface cryptoService = new DummyCryptoService();


    @Test
    void getPdfSignatureStamp() {

        PdfSignatureStamp stamp = cryptoService.getPdfSignatureStamp(emptyMap());

        assertEquals(70, stamp.getHeight(), 1F);
        assertEquals(200, stamp.getWidth(), 1F);

        assertEquals(4, stamp.getElements().size());

        stamp.getElements().stream()
                .filter(e -> e.getType() == IMAGE)
                .forEach(e -> {
                    assertTrue(StringUtils.isNotEmpty(e.getValue()));
                    assertFalse(e.getValue().contains(" "));
                    assertFalse(e.getValue().contains("\n"));
                });
    }


    @Test
    void overrideParamsWithComplementaryInfo_allParams_success() {

        String complementaryInfo = "TITRE=\"Overridden title\", VILLE=\"Overridden City\",CODEPOSTAL=\"56789\"";
        User user = User.builder()
                .userName("user")
                .complementaryField(complementaryInfo)
                .build();

        XadesParameters signatureParams = new XadesParameters();
        signatureParams.setClaimedRoles(singletonList("baseRole"));
        signatureParams.setCity("baseCity");
        signatureParams.setZipCode("12345");

        cryptoService.overrideParamsWithComplementaryInfo(
                user,
                new Desk(),
                new Type(),
                signatureParams::setClaimedRoles,
                signatureParams::setCountry,
                signatureParams::setCity,
                signatureParams::setZipCode
        );

        assertTrue(CollectionUtils.isNotEmpty(signatureParams.getClaimedRoles()));
        assertEquals("Overridden title", signatureParams.getClaimedRoles().get(0));
        assertEquals("Overridden City", signatureParams.getCity());
        assertEquals("56789", signatureParams.getZipCode());
    }


}
