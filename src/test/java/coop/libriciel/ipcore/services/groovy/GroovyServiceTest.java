/*
 * iparapheur Core
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.ipcore.services.groovy;

import coop.libriciel.ipcore.model.auth.DeskRepresentation;
import coop.libriciel.ipcore.model.workflow.StepDefinition;
import coop.libriciel.ipcore.model.workflow.WorkflowDefinition;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import static coop.libriciel.ipcore.model.workflow.Action.VISA;
import static coop.libriciel.ipcore.model.workflow.StepDefinitionParallelType.OR;
import static coop.libriciel.ipcore.utils.TextUtils.INTERNAL_PREFIX;
import static java.util.Collections.emptyList;
import static java.util.Collections.emptyMap;
import static org.junit.jupiter.api.Assertions.*;


@SpringBootTest
@ExtendWith(SpringExtension.class)
class GroovyServiceTest {


    @Autowired GroovyService groovyService;


    @Test
    void executeSelectionScript_simpleScript() {

        String script =
                """
                circuit "test1234";
                """;

        GroovyResultCatcher result = groovyService.executeSelectionScript("tenant01", script, emptyMap());

        assertNotNull(result);
        assertEquals("test1234", result.getWorkflowDefinitionId());
    }


    @Test
    void executeSelectionScript_wrapper() {

        String script =
                """
                selectionCircuit {
                  circuit "test1234";
                }
                """;

        GroovyResultCatcher result = groovyService.executeSelectionScript("tenant01", script, emptyMap());

        assertNotNull(result);
        assertEquals("test1234", result.getWorkflowDefinitionId());
    }


    @Test
    void executeSelectionScript_metadataInput() {

        String script =
                """
                if (meta01 == '001') {
                  circuit "entered001";
                }
                                
                if (meta01 == '002') {
                  circuit "entered002";
                }
                                
                circuit "didNotEnterIfs";
                """;

        GroovyResultCatcher result = groovyService.executeSelectionScript("tenant01", script, Map.of("meta01", "002"));

        assertNotNull(result);
        assertEquals("entered002", result.getWorkflowDefinitionId());
    }


    @Test
    void executeSelectionScript_deskSelector() {

        String script =
                """
                circuit "signature_helios", ["Bleu"];
                """;

        GroovyResultCatcher result = groovyService.executeSelectionScript("tenant01", script, Map.of("meta01", "002"));

        assertNotNull(result);
        assertEquals("signature_helios", result.getWorkflowDefinitionId());
        assertEquals(1, result.getVariableDesks().size());
        assertEquals("Bleu", result.getVariableDesks().get(0));
    }


    @Test
    void updateVariableDeskMapFromScriptResult() {

        // Prepare variables

        String tenantId = "tenantId01";
        String workflowDefinitionId = "workflowDefinitionId01";
        List<String> variableDesks = List.of("desk01", "desk02", "desk03");

        GroovyResultCatcher groovyResultCatcher = new GroovyResultCatcher();
        groovyResultCatcher.setResult(workflowDefinitionId, variableDesks);

        WorkflowDefinition workflowDefinition = new WorkflowDefinition();
        workflowDefinition.setSteps(List.of(
                new StepDefinition(
                        null, VISA,
                        List.of(new DeskRepresentation(UUID.randomUUID().toString())),
                        emptyList(), emptyList(), emptyList(), OR
                ),
                new StepDefinition(
                        null, VISA,
                        List.of(new DeskRepresentation("${" + INTERNAL_PREFIX + "validation_variable_desk_id_1}")),
                        emptyList(), emptyList(), emptyList(), OR
                ),
                new StepDefinition(
                        null, VISA,
                        List.of(new DeskRepresentation(UUID.randomUUID().toString())),
                        emptyList(), emptyList(), emptyList(), OR
                ),
                new StepDefinition(
                        null, VISA,
                        List.of(new DeskRepresentation("${" + INTERNAL_PREFIX + "validation_variable_desk_id_3}")),
                        emptyList(), emptyList(), emptyList(), OR
                )
        ));

        // Actual tests

        HashMap<Integer, String> result = new HashMap<>();
        groovyService.updateVariableDeskMapFromScriptResult(tenantId, groovyResultCatcher, workflowDefinition, result);

        assertEquals(2, result.size());
        assertTrue(StringUtils.isNotEmpty(result.get(1)));
        assertTrue(StringUtils.isNotEmpty(result.get(3)));
    }


}
