/*
 * iparapheur Core
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.ipcore.services.auth;

import com.google.common.collect.ImmutableMap;
import coop.libriciel.ipcore.model.auth.Desk;
import coop.libriciel.ipcore.model.database.Tenant;
import coop.libriciel.ipcore.services.permission.KeycloakResourceService;
import org.assertj.core.util.Lists;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.resource.*;
import org.keycloak.authorization.client.AuthzClient;
import org.keycloak.representations.idm.RoleRepresentation;
import org.keycloak.representations.idm.UserRepresentation;
import org.keycloak.representations.idm.authorization.ResourceRepresentation;
import org.keycloak.representations.idm.authorization.RolePolicyRepresentation;
import org.keycloak.representations.idm.authorization.ScopePermissionRepresentation;
import org.mockito.Mockito;
import org.modelmapper.ModelMapper;

import java.util.Collections;
import java.util.UUID;

import static coop.libriciel.ipcore.model.auth.UserPrivilege.SUPER_ADMIN;
import static coop.libriciel.ipcore.model.auth.requests.UserSortBy.USERNAME;
import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static org.apache.commons.lang3.StringUtils.EMPTY;
import static org.jooq.conf.ParamType.INLINED;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;


public class KeycloakServiceTest {


    private KeycloakService keycloakService;
    private KeycloakResourceService keycloakResourceServerService;
    private Keycloak keycloakMock;
    private RealmResource keycloakRealmMock;
    private AuthzClient authzClient;
    private ClientResource clientResource;
    private ResourcesResource resources;
    private ModelMapper modelMapper;


    @BeforeEach
    void setUp() {

        authzClient = Mockito.mock(AuthzClient.class);
        keycloakRealmMock = Mockito.mock(RealmResource.class);
        keycloakMock = Mockito.mock(Keycloak.class);
        clientResource = Mockito.mock(ClientResource.class);
        resources = Mockito.mock(ResourcesResource.class);
        modelMapper = Mockito.mock(ModelMapper.class);

        AuthorizationResource authorizationResource = Mockito.mock(AuthorizationResource.class);
        Mockito.doReturn(authorizationResource).when(clientResource).authorization();
        Mockito.doReturn(new UserRepresentation()).when(clientResource).getServiceAccountUser();
        Mockito.doReturn(resources).when(authorizationResource).resources();
        Mockito.doReturn(keycloakRealmMock).when(keycloakMock).realm(anyString());

        AuthServiceProperties properties = new AuthServiceProperties(
                "keycloak", "localhost", 8080,
                60000, "i-Parapheur", "user", "pass",
                new AuthServiceProperties.DataBase("localhost", 4567, "test", "user", "pass")
        );

        keycloakService = new KeycloakService(properties, modelMapper);
        keycloakService.keycloak = keycloakMock;
        keycloakService.realmResource = keycloakRealmMock;
        keycloakService.clientResource = clientResource;

        keycloakResourceServerService = new KeycloakResourceService(authzClient, properties);
        keycloakResourceServerService.setClientResource(clientResource);
    }


    @AfterEach
    void after() {
    }


    @Test
    void getAttribute() {

        assertEquals("0", KeycloakService.getAttribute(ImmutableMap.of("key", asList("0", "1", "2")), "key"));
        assertEquals("0", KeycloakService.getAttribute(ImmutableMap.of("key", singletonList("0")), "key"));

        assertNull(KeycloakService.getAttribute(ImmutableMap.of("key", asList("0", "1", "2")), "badKey"));
        assertNull(KeycloakService.getAttribute(ImmutableMap.of("key", emptyList()), "key"));
        assertNull(KeycloakService.getAttribute(null, "key"));
    }


    @Test
    void createListDesksSqlRequest() {

        String expected =
                """
                WITH RECURSIVE rel_tree AS (
                     SELECT
                       kr.id AS id,
                       0 AS level,
                       CAST(ARRAY[((kr.description || ' :: ') || kr.id)] AS TEXT[]) AS path_info_names,
                       CAST(ARRAY[kr.id] AS TEXT[]) AS path_info_ids,
                       kr.description AS description
                     FROM keycloak_role AS kr
                     LEFT OUTER JOIN role_attribute AS ra
                       ON ( kr.id = ra.role_id
                      AND ra.name = 'i_Parapheur_internal_parent_desk_id' )
                     WHERE ( ra.value IS NULL
                       AND CAST(kr.name AS varchar) LIKE 'tenant_tenant01_desk_%' )
                   UNION ALL
                     SELECT
                       ra.role_id AS id,
                       (p.level + 1) AS level,
                       array_append( p.path_info_names, CAST(((kr.description || ' :: ') || ra.role_id) AS TEXT) ) AS path_info_names,
                       array_append( p.path_info_ids, CAST(ra.role_id AS TEXT) ) AS path_info_ids,
                       kr.description AS description
                     FROM role_attribute AS ra
                     JOIN rel_tree AS p
                       ON p.id = ra.value
                     JOIN keycloak_role AS kr
                       ON kr.id = ra.role_id
                     WHERE ( ra.name = 'i_Parapheur_internal_parent_desk_id'
                       AND ra.value NOT IN ('1234')
                       AND CAST(kr.name AS varchar) LIKE 'tenant_tenant01_desk_%' )
                )
                SELECT
                  rt.id AS id,
                  rt.description AS description,
                  rt.level AS level,
                  count(ra.id) AS direct_children_count,
                  rt.path_info_names AS path_info_names,
                  rt.path_info_ids AS path_info_ids
                FROM rel_tree AS rt
                LEFT OUTER JOIN role_attribute AS ra
                  ON ( ra.value = rt.id
                 AND ra.name = 'i_Parapheur_internal_parent_desk_id' )
                WHERE (
                  description ILIKE '%namelike%'
                  AND 1 = 1
                )
                GROUP BY rt.id, description, level, path_info_names, path_info_ids
                ORDER BY path_info_names
                OFFSET 100 ROWS
                FETCH NEXT 50 ROWS ONLY
                """.indent(1);

        String result = keycloakService
                .buildListDesksSqlRequest("tenant01", 2, 50,
                        false, singletonList("1234"), "namelike", null)
                .getSQL(INLINED);

        assertEquals(
                expected.trim().replaceAll("\\s+", " "),
                result.trim().replaceAll("\\s+", " ")
        );
    }


    @Test
    void buildGetUserAdminPrivileges() {

        String expected =
                """
                SELECT
                  urm.user_id AS id
                FROM keycloak_role AS kr
                JOIN user_role_mapping AS urm
                  ON kr.id = urm.role_id
                JOIN realm AS r
                  ON (
                   kr.realm_id = r.id
                   AND r.name = 'i-Parapheur'
                  )
                WHERE (
                  kr.name LIKE 'admin'
                  AND urm.user_id IN ( 'userId01', 'userId02' )
                )
                """.indent(1);

        String result = keycloakService
                .buildFilterUsersWithPrivilege(asList("userId01", "userId02"), SUPER_ADMIN, null)
                .getSQL(INLINED);

        assertEquals(
                expected.trim().replaceAll("\\s+", " "),
                result.trim().replaceAll("\\s+", " ")
        );
    }


    @Test
    void buildGetDeskNames() {
        String expected =
                """
                SELECT
                  kr.id AS id,
                  kr.description AS description
                FROM keycloak_role AS kr
                JOIN realm AS r
                  ON kr.realm_id = r.id
                WHERE (
                  r.name = 'i-Parapheur'
                  AND kr.id IN ( 'id01', 'id02' )
                )
                """.indent(1);

        String result = keycloakService
                .buildGetDeskNames(asList("id01", "id02"))
                .getSQL(INLINED);

        assertEquals(
                expected.trim().replaceAll("\\s+", " "),
                result.trim().replaceAll("\\s+", " ")
        );
    }


    @Test
    void buildListUsersByIds() {
        String expected =
                """
                SELECT
                  id AS id,
                  username AS userName,
                  first_name AS firstName,
                  last_name AS lastName,
                  email AS email,   federation_link AS federation
                FROM
                  user_entity
                WHERE (
                  realm_id = 'i-Parapheur'   AND id IN ( 'id01', 'id02' )
                )
                """.indent(1);

        String result = keycloakService
                .buildListUsersByIds(asList("id01", "id02"))
                .getSQL(INLINED);

        assertEquals(
                expected.trim().replaceAll("\\s+", " "),
                result.trim().replaceAll("\\s+", " ")
        );
    }


    @Test
    void buildListUsersRequest() {
        String expected =
                """
                SELECT
                  ue.id AS id,
                  ue.username AS userName,
                  ue.first_name AS firstName,
                  ue.last_name AS lastName,
                  ue.email AS email,
                  ue.federation_link AS federation
                FROM user_entity AS ue
                WHERE (
                  upper(ue.username) LIKE upper('%searchTerm%')
                  OR upper(ue.first_name) LIKE upper('%searchTerm%')
                  OR upper(ue.last_name) LIKE upper('%searchTerm%')
                  OR upper(ue.email) LIKE upper('%searchTerm%')
                )
                ORDER BY username ASC
                OFFSET 150 ROWS
                FETCH NEXT 50 ROWS ONLY
                """.indent(1);

        String result = keycloakService
                .buildListUsersRequest(USERNAME, true, "searchTerm", 3, 50)
                .getSQL(INLINED);

        assertEquals(
                expected.trim().replaceAll("\\s+", " "),
                result.trim().replaceAll("\\s+", " ")
        );
    }


    @Test
    void buildSearchUserWithinGroupRequest() {
        String expected =
                """
                SELECT
                  ue.id AS id,
                  ue.username AS userName,
                  ue.first_name AS firstName,
                  ue.last_name AS lastName,
                  ue.email AS email,
                  ue.federation_link AS federation,
                  max(CASE
                      WHEN ( kr.name = 'tenant_tenant01_admin' OR cf.value = 'tenant_tenant01_admin' ) THEN 3
                      WHEN ( kr.name = 'tenant_tenant01_functional_admin' OR cf.value = 'tenant_tenant01_functional_admin' ) THEN 2
                      WHEN ( kr.name = 'tenant_tenant01' OR cf.value = 'tenant_tenant01' ) THEN 1
                      ELSE 0
                    END) AS privilegeIndex
                FROM user_entity AS ue
                LEFT OUTER JOIN (
                           user_role_mapping AS urm
                           JOIN keycloak_role AS kr
                           ON (
                             urm.role_id = kr.id
                             AND (
                               kr.name = 'tenant_tenant01'
                               OR kr.name = 'tenant_tenant01_admin'
                               OR kr.name = 'tenant_tenant01_functional_admin'
                             )
                           )
                  )
                  ON urm.user_id = ue.id
                LEFT OUTER JOIN (
                           component AS c
                           JOIN component_config AS cf
                           ON (
                             c.id = cf.component_id
                             AND cf.name = 'role'
                             AND cf.value = 'tenant_tenant01'
                           )
                  )
                  ON c.parent_id = ue.federation_link
                WHERE (
                  upper(ue.username) LIKE upper('%searchTerm%')
                  OR upper(ue.first_name) LIKE upper('%searchTerm%')
                  OR upper(ue.last_name) LIKE upper('%searchTerm%')
                  OR upper(ue.email) LIKE upper('%searchTerm%')
                )
                GROUP BY ue.id, ue.username, ue.first_name, ue.last_name, ue.email, ue.federation_link
                HAVING (
                  'tenant_tenant01' = ANY ( array_agg(kr.name) )
                  OR 'tenant_tenant01' = ANY ( array_agg(cf.value) )
                  OR 'tenant_tenant01_admin' = ANY ( array_agg(kr.name) )
                  OR 'tenant_tenant01_admin' = ANY ( array_agg(cf.value) )
                  OR 'tenant_tenant01_functional_admin' = ANY ( array_agg(kr.name) )
                  OR 'tenant_tenant01_functional_admin' = ANY ( array_agg(cf.value) )
                )
                ORDER BY username ASC
                OFFSET 150 ROWS
                FETCH NEXT 50 ROWS ONLY
                """.indent(1);

        String result = keycloakService
                .buildSearchUserWithinGroupRequest("tenant01", USERNAME, true, "searchTerm", 3, 50)
                .getSQL(INLINED);

        assertEquals(
                expected.trim().replaceAll("\\s+", " "),
                result.trim().replaceAll("\\s+", " ")
        );
    }


    @Test
    void buildListUsersTotalRequest() {
        String expected =
                """
                SELECT
                  count(*)
                FROM user_entity AS ue
                WHERE (
                  upper(ue.username) LIKE upper('%searchTerm%')
                  OR upper(ue.first_name) LIKE upper('%searchTerm%')
                  OR upper(ue.last_name) LIKE upper('%searchTerm%')
                  OR upper(ue.email) LIKE upper('%searchTerm%')
                )
                """.indent(1);

        String result = keycloakService
                .buildListUsersTotalRequest("searchTerm")
                .getSQL(INLINED);

        assertEquals(
                expected.trim().replaceAll("\\s+", " "),
                result.trim().replaceAll("\\s+", " ")
        );
    }


    @Test
    void buildSearchUserWithinGroupTotalRequest() {
        String expected =
                """
                SELECT
                  count(DISTINCT ue.id)
                FROM user_entity AS ue
                LEFT OUTER JOIN (
                           user_role_mapping AS urm
                           JOIN keycloak_role AS kr
                           ON (
                             urm.role_id = kr.id
                             AND (
                               kr.name = 'tenant_tenant01'
                               OR kr.name = 'tenant_tenant01_admin'
                               OR kr.name = 'tenant_tenant01_functional_admin'
                             )
                           )
                  )
                  ON urm.user_id = ue.id
                LEFT OUTER JOIN (
                           component AS c
                           JOIN component_config AS cf
                           ON (
                             c.id = cf.component_id
                             AND cf.name = 'role'
                             AND cf.value = 'tenant_tenant01'
                           )
                  )
                  ON c.parent_id = ue.federation_link
                WHERE (
                  (
                    upper(ue.username) LIKE upper('%searchTerm%')
                    OR upper(ue.first_name) LIKE upper('%searchTerm%')
                    OR upper(ue.last_name) LIKE upper('%searchTerm%')
                    OR upper(ue.email) LIKE upper('%searchTerm%')
                  )
                  AND (
                    'tenant_tenant01' = kr.name
                    OR 'tenant_tenant01' = cf.value
                    OR 'tenant_tenant01_admin' = kr.name
                    OR 'tenant_tenant01_admin' = cf.value
                    OR 'tenant_tenant01_functional_admin' = kr.name
                    OR 'tenant_tenant01_functional_admin' = cf.value
                  )
                )
                """.indent(1);

        String result = keycloakService
                .buildSearchUserWithinGroupTotalRequest("tenant01", "searchTerm")
                .getSQL(INLINED);

        assertEquals(
                expected.trim().replaceAll("\\s+", " "),
                result.trim().replaceAll("\\s+", " ")
        );
    }


    @Test
    void buildTotalDesksSqlRequest() {
        String expected =
                """
                WITH RECURSIVE rel_tree AS (
                     SELECT
                       kr.id AS id,
                       kr.description AS description
                     FROM keycloak_role AS kr
                     LEFT OUTER JOIN role_attribute AS ra
                       ON ( kr.id = ra.role_id
                      AND ra.name = 'i_Parapheur_internal_parent_desk_id' )
                     WHERE ( ra.value IS NULL
                       AND CAST(kr.name AS varchar) LIKE 'tenant_tenant01_desk_%' )
                   UNION ALL
                     SELECT
                       ra.role_id AS id,
                       kr.description AS description
                     FROM role_attribute AS ra
                     JOIN rel_tree AS p
                       ON p.id = ra.value
                     JOIN keycloak_role AS kr
                       ON kr.id = ra.role_id
                     WHERE ( ra.name = 'i_Parapheur_internal_parent_desk_id'
                       AND ra.value NOT IN ('1234')
                       AND CAST(kr.name AS varchar) LIKE 'tenant_tenant01_desk_%' )
                )
                SELECT
                  count(*)
                FROM rel_tree AS rt
                WHERE description ILIKE '%nameLike%'
                """.indent(1);

        String result = keycloakService
                .buildTotalDesksSqlRequest("tenant01", false, singletonList("1234"), "nameLike")
                .getSQL(INLINED);

        assertEquals(
                expected.trim().replaceAll("\\s+", " "),
                result.trim().replaceAll("\\s+", " ")
        );
    }


    @Test
    void buildListDeskAndDelegates() {

        String expected =
                """
                SELECT
                  keycloak_role_parent.id AS id,
                  keycloak_role_parent.description AS description,
                  keycloak_role_parent.name AS name,
                  keycloak_role_child.id AS delegating_id,
                  role_attribute_parent.value AS parent_delegation_by_metadata,
                  substring( keycloak_role_parent.name, 7, 36 ) AS tenantId
                FROM user_role_mapping
                LEFT OUTER JOIN composite_role
                  ON composite_role.composite = user_role_mapping.role_id
                JOIN keycloak_role AS keycloak_role_parent
                  ON user_role_mapping.role_id = keycloak_role_parent.id
                LEFT OUTER JOIN keycloak_role AS keycloak_role_child
                  ON composite_role.child_role = keycloak_role_child.id
                LEFT OUTER JOIN role_attribute AS role_attribute_parent
                  ON ( keycloak_role_parent.id = role_attribute_parent.role_id
                 AND role_attribute_parent.name = 'i_Parapheur_internal_typology_delegations' )
                WHERE (
                  user_role_mapping.user_id = 'user1234'
                  AND keycloak_role_parent.realm_id = 'i-Parapheur'
                  AND keycloak_role_parent.name LIKE 'tenant_%_desk_%'
                  AND 1 = 1
                )
                ORDER BY tenantId
                OFFSET 150 ROWS
                FETCH NEXT 50 ROWS ONLY
                """.indent(1);

        String result = keycloakService
                .buildListDeskAndDelegates("user1234", 3, 50, null)
                .getSQL(INLINED);

        assertEquals(
                expected.trim().replaceAll("\\s+", " "),
                result.trim().replaceAll("\\s+", " ")
        );
    }


    @Test
    void findDeskByIdTest_withParentDesk() {
        String testParentId = "testParentId";
        String testParentName = "testParentName";

        RoleRepresentation roleMock = Mockito.mock(RoleRepresentation.class);
        RoleByIdResource roleByIdResourceMock = Mockito.mock(RoleByIdResource.class);
        Mockito.when(roleMock.getAttributes()).thenReturn(Collections.singletonMap(Desk.ATTRIBUTE_PARENT_DESK_ID, Lists.list(testParentId)));
        Mockito.when(roleMock.getName()).thenReturn("testRoleName");
        Mockito.when(roleMock.getId()).thenReturn("testRoleId");
        Mockito.when(keycloakRealmMock.rolesById()).thenReturn(roleByIdResourceMock);
        Mockito.when(roleByIdResourceMock.getRole(anyString())).thenReturn(roleMock);

        KeycloakService keycloakServiceSpy = Mockito.spy(keycloakService);
        Mockito.doReturn(Collections.singletonMap(testParentId, testParentName)).when(keycloakServiceSpy).getDeskNames(anySet());
        Mockito.doReturn("testTenantId").when(keycloakServiceSpy).getTenantIdForRoleName(anyString(), anyString());

        Desk d = keycloakServiceSpy.findDeskById(null, "cc9f4c57-173a-4c89-b53f-e465a023ae62");
        assertNotNull(d);
        assertNotNull(d.getParentDesk());
        assertEquals(d.getParentDesk().getId(), testParentId);
        assertEquals(d.getParentDesk().getName(), testParentName);

    }


    @Test
    void saveDeskWithShortName() {
        String testId = "testId";
        String testName = "testName";
        String testShortName = "testShortName";

        Tenant tenant = Tenant.builder().id("testTenantId").name("testTenantName").build();

        KeycloakService keycloakServiceSpy = Mockito.spy(keycloakService);
        KeycloakResourceService keycloakResourceServerServiceSpy = Mockito.spy(keycloakResourceServerService);

        RoleRepresentation roleMock = Mockito.mock(RoleRepresentation.class);
        Mockito.when(roleMock.getId()).thenReturn(testId);

        RoleResource roleResource = Mockito.mock(RoleResource.class);
        Mockito.when(roleResource.toRepresentation()).thenReturn(roleMock);

        RolesResource rolesResourceListMock = Mockito.mock(RolesResource.class);
        Mockito.when(rolesResourceListMock.get(Mockito.any())).thenReturn(roleResource);
        Mockito.doNothing().when(rolesResourceListMock).create(any());

        RoleByIdResource roleByIdResourceMock = Mockito.mock(RoleByIdResource.class);
        Mockito.when(keycloakRealmMock.rolesById()).thenReturn(roleByIdResourceMock);
        Mockito.when(keycloakRealmMock.roles()).thenReturn(rolesResourceListMock);
        keycloakServiceSpy.rolesResourceClient = rolesResourceListMock;

        ResourceRepresentation mockResourceRepresentation = new ResourceRepresentation();
        RolePolicyRepresentation mockRolePolicyRepresentation = new RolePolicyRepresentation();
        ScopePermissionRepresentation scopePermissionRepresentation = new ScopePermissionRepresentation();
        mockResourceRepresentation.setId(UUID.randomUUID().toString());
        mockRolePolicyRepresentation.setId(UUID.randomUUID().toString());
        scopePermissionRepresentation.setId(UUID.randomUUID().toString());
        Mockito.doNothing().when(keycloakServiceSpy).addUsersToDesk(anyString(), anyList());
        Mockito.doReturn(EMPTY).when(keycloakResourceServerServiceSpy).getServiceAccountAccessToken(anyString());
        Mockito.doReturn(scopePermissionRepresentation).when(keycloakResourceServerServiceSpy).createDeskPermission(
                anyString(), anyString(), anySet(), eq(mockResourceRepresentation), eq(mockRolePolicyRepresentation)
        );

        keycloakServiceSpy.createDesk(tenant.getId(), testName, testShortName, null, null);

        Mockito.verify(rolesResourceListMock, Mockito.times(1)).create(Mockito.any());
        Mockito.verify(roleMock, Mockito.times(1)).singleAttribute(Desk.ATTRIBUTE_SHORT_NAME, testShortName);
        Mockito.verify(roleByIdResourceMock, Mockito.times(1)).updateRole(Mockito.any(), eq(roleMock));
    }


    @Test
    void buildListDesksByShortNameSqlRequest() {

        String expected =
                """
                SELECT
                  kr.id AS id,
                  kr.description AS description
                FROM keycloak_role AS kr
                LEFT OUTER JOIN role_attribute AS ra
                  ON ( kr.id = ra.role_id AND ra.name = 'i_Parapheur_internal_short_name' )
                WHERE (
                  CAST(kr.name AS varchar) LIKE 'tenant_tenant01_desk_%'
                  AND ra.value = 'shortName01'
                )
                """.indent(1);

        String result = keycloakService
                .buildListDesksByShortNameSqlRequest("tenant01", "shortName01")
                .getSQL(INLINED);

        assertEquals(
                expected.trim().replaceAll("\\s+", " "),
                result.trim().replaceAll("\\s+", " ")
        );
    }


}
