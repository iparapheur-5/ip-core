/*
 * iparapheur Core
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package coop.libriciel.ipcore.services.database;

import coop.libriciel.ipcore.model.database.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.UUID;
import java.util.stream.StreamSupport;

import static coop.libriciel.ipcore.model.crypto.SignatureFormat.PES_V2;
import static coop.libriciel.ipcore.model.database.MetadataType.TEXT;
import static java.util.Collections.emptyList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace.NONE;


@SpringBootTest
@ExtendWith(SpringExtension.class)
@AutoConfigureTestDatabase(replace = NONE)
public class SubtypeMetadataRepositoryTest {


    private @Autowired TypeRepository typeRepository;
    private @Autowired TenantRepository tenantRepository;
    private @Autowired SubtypeRepository subtypeRepository;
    private @Autowired MetadataRepository metadataRepository;
    private @Autowired SubtypeMetadataRepository subtypeMetadataRepository;


    @BeforeEach
    void setup() {

        this.cleanup();

        Tenant tenant = tenantRepository.save(
                Tenant.builder()
                        .id("tenantId")
                        .name("Tenant name")
                        .index(1L)
                        .contentId("content1234")
                        .statsId("stats1234")
                        .build()
        );

        Type type = typeRepository.save(Type.builder()
                .id("type_01").name("Type 01").description("First type")
                .subtypes(emptyList()).signatureFormat(PES_V2).tenant(tenant)
                .build());

        subtypeRepository.save(Subtype.builder().id("st01").tenant(tenant).parentType(type).build());
    }


    @AfterEach
    void cleanup() {
        subtypeMetadataRepository.deleteAll();
        subtypeRepository.deleteAll();
        typeRepository.deleteAll();
        metadataRepository.deleteAll();
        tenantRepository.deleteAll();
    }


    @Test
    void deleteCascade_fromSubtype() {

        Tenant tenant = StreamSupport.stream(tenantRepository.findAll().spliterator(), false).findFirst().orElse(null);
        assertNotNull(tenant);
        Type type = StreamSupport.stream(typeRepository.findAll().spliterator(), false).findFirst().orElse(null);
        assertNotNull(type);
        Subtype subtype = StreamSupport.stream(subtypeRepository.findAll().spliterator(), false).findFirst().orElse(null);
        assertNotNull(subtype);

        Metadata metadata01 = metadataRepository.save(new Metadata(UUID.randomUUID().toString(), "m01", "m1", 1, TEXT, null, tenant, emptyList()));
        metadata01 = metadataRepository.findById(metadata01.getId()).orElse(null);
        assertNotNull(metadata01);
        Metadata metadata02 = metadataRepository.save(new Metadata(UUID.randomUUID().toString(), "m02", "m2", 2, TEXT, null, tenant, emptyList()));
        metadata02 = metadataRepository.findById(metadata02.getId()).orElse(null);
        assertNotNull(metadata02);

        subtypeMetadataRepository.save(new SubtypeMetadata(subtype, metadata01, null, true, true));
        subtypeMetadataRepository.save(new SubtypeMetadata(subtype, metadata02, null, true, false));

        subtype = subtypeRepository.findById(subtype.getId()).orElse(null);
        assertNotNull(subtype);
        metadata01 = metadataRepository.findById(metadata01.getId()).orElse(null);
        assertNotNull(metadata01);
        metadata02 = metadataRepository.findById(metadata02.getId()).orElse(null);
        assertNotNull(metadata02);

        // Delete cascade

        assertEquals(1, StreamSupport.stream(subtypeRepository.findAll().spliterator(), false).count());
        assertEquals(2, StreamSupport.stream(metadataRepository.findAll().spliterator(), false).count());
        assertEquals(2, StreamSupport.stream(subtypeMetadataRepository.findAll().spliterator(), false).count());
        subtypeRepository.delete(subtype);
        assertEquals(0, StreamSupport.stream(subtypeRepository.findAll().spliterator(), false).count());
        assertEquals(2, StreamSupport.stream(metadataRepository.findAll().spliterator(), false).count());
        assertEquals(0, StreamSupport.stream(subtypeMetadataRepository.findAll().spliterator(), false).count());
    }


    @Test
    void deleteCascade_fromMetadata() {

        Tenant tenant = StreamSupport.stream(tenantRepository.findAll().spliterator(), false).findFirst().orElse(null);
        assertNotNull(tenant);
        Type type = StreamSupport.stream(typeRepository.findAll().spliterator(), false).findFirst().orElse(null);
        assertNotNull(type);
        Subtype subtype = StreamSupport.stream(subtypeRepository.findAll().spliterator(), false).findFirst().orElse(null);
        assertNotNull(subtype);

        Metadata metadata01 = metadataRepository.save(new Metadata(UUID.randomUUID().toString(), "m01", "m1", 1, TEXT, null, tenant, emptyList()));
        metadata01 = metadataRepository.findById(metadata01.getId()).orElse(null);
        assertNotNull(metadata01);
        Metadata metadata02 = metadataRepository.save(new Metadata(UUID.randomUUID().toString(), "m02", "m2", 2, TEXT, null, tenant, emptyList()));
        metadata02 = metadataRepository.findById(metadata02.getId()).orElse(null);
        assertNotNull(metadata02);

        subtypeMetadataRepository.save(new SubtypeMetadata(subtype, metadata01, null, true, true));
        subtypeMetadataRepository.save(new SubtypeMetadata(subtype, metadata02, null, true, false));

        subtype = subtypeRepository.findById(subtype.getId()).orElse(null);
        assertNotNull(subtype);
        metadata01 = metadataRepository.findById(metadata01.getId()).orElse(null);
        assertNotNull(metadata01);
        metadata02 = metadataRepository.findById(metadata02.getId()).orElse(null);
        assertNotNull(metadata02);

        // Delete cascade

        assertEquals(1, StreamSupport.stream(subtypeRepository.findAll().spliterator(), false).count());
        assertEquals(2, StreamSupport.stream(metadataRepository.findAll().spliterator(), false).count());
        assertEquals(2, StreamSupport.stream(subtypeMetadataRepository.findAll().spliterator(), false).count());
        metadataRepository.delete(metadata01);
        assertEquals(1, StreamSupport.stream(subtypeRepository.findAll().spliterator(), false).count());
        assertEquals(1, StreamSupport.stream(metadataRepository.findAll().spliterator(), false).count());
        assertEquals(1, StreamSupport.stream(subtypeMetadataRepository.findAll().spliterator(), false).count());
        metadataRepository.delete(metadata02);
        assertEquals(1, StreamSupport.stream(subtypeRepository.findAll().spliterator(), false).count());
        assertEquals(0, StreamSupport.stream(metadataRepository.findAll().spliterator(), false).count());
        assertEquals(0, StreamSupport.stream(subtypeMetadataRepository.findAll().spliterator(), false).count());
    }


}