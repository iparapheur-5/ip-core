/*
 * iparapheur Core
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.ipcore.services.database;

import coop.libriciel.ipcore.model.auth.Desk;
import coop.libriciel.ipcore.model.database.Tenant;
import org.junit.jupiter.api.Test;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.Set;

import static coop.libriciel.ipcore.model.workflow.FolderSortBy.TYPE_NAME;
import static coop.libriciel.ipcore.model.workflow.State.PENDING;
import static org.jooq.conf.ParamType.INLINED;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.data.domain.Sort.Direction.DESC;


public class PostgresServiceTest {


    @Test
    void buildTenantIndexAvailableRequest() {
        String expected =
                """
                SELECT
                  possibleIndex
                FROM generate_series(0, 10000) AS possibleIndex
                WHERE
                  possibleIndex NOT IN (
                    SELECT index FROM tenant
                  )
                FETCH NEXT 1 ROWS ONLY
                """.indent(1);

        String result = PostgresService
                .buildTenantIndexAvailableRequest()
                .getSQL(INLINED);

        assertEquals(
                expected.trim().replaceAll("\\s+", " "),
                result.trim().replaceAll("\\s+", " ")
        );
    }


    @Test
    void buildUserDataGroupIndexAvailableRequest() {
        String expected =
                """
                SELECT
                  possibleIndex
                FROM generate_series(0, 500000) AS possibleIndex
                WHERE
                  possibleIndex NOT IN (
                    SELECT CAST(value AS int)
                    FROM user_attribute
                    WHERE name = 'i_Parapheur_internal_content_group_index'
                  )
                FETCH NEXT 1 ROWS ONLY
                """.indent(1);

        String result = PostgresService
                .buildUserDataGroupIndexAvailableRequest()
                .getSQL(INLINED);

        assertEquals(
                expected.trim().replaceAll("\\s+", " "),
                result.trim().replaceAll("\\s+", " ")
        );
    }


    @Test
    void buildListTenantsForUser() {

        String expected =
                """
                SELECT
                  t.id AS id,
                  t.name AS name,
                  urmu.role_id AS user_link,
                  urmu.user_id AS user_id
                FROM
                  tenant AS t
                JOIN
                  keycloak_role AS kru
                  ON
                    kru.name = ('tenant_' || t.id)
                LEFT OUTER JOIN
                  user_role_mapping AS urmu
                  ON (
                    urmu.role_id = kru.id
                    AND urmu.user_id = 'user1234'
                  )
                WHERE (
                  upper(t.name) LIKE upper('%search%')
                  AND urmu.role_id IS NULL
                ) ORDER BY t.id DESC
                OFFSET 150 ROWS
                FETCH NEXT 50 ROWS ONLY
                """.indent(1);

        String result = PostgresService
                .buildListTenantsForUserRequest("user1234", PageRequest.of(3, 50, DESC, Tenant.COLUMN_ID), "search", true)
                .getSQL(INLINED);

        assertEquals(
                expected.trim().replaceAll("\\s+", " "),
                result.trim().replaceAll("\\s+", " ")
        );
    }


    @Test
    void buildCountTenantsForUser() {

        String expected =
                """
                SELECT
                  count(*)
                FROM
                  tenant AS t
                JOIN
                  keycloak_role AS kru
                  ON
                    kru.name = ('tenant_' || t.id)
                LEFT OUTER JOIN
                  user_role_mapping AS urmu
                  ON (
                    urmu.role_id = kru.id
                    AND urmu.user_id = 'user1234'
                  )
                WHERE (
                  t.name LIKE '%search%'
                  AND urmu.role_id IS NOT NULL
                )
                """.indent(1);

        String result = PostgresService
                .buildCountTenantsForUser("user1234", "search", false)
                .getSQL(INLINED);

        assertEquals(
                expected.trim().replaceAll("\\s+", " "),
                result.trim().replaceAll("\\s+", " ")
        );
    }


    @Test
    void buildListAdminUsersIdForTenant() {

        String expected =
                """
                                
                SELECT
                  urm.user_id AS user_id,
                  krta.name
                FROM user_role_mapping AS urm
                JOIN
                  keycloak_role AS krta
                  ON (
                    krta.name = 'tenant_tenant01_admin'
                    AND urm.role_id = krta.id
                  )
                                
                UNION ALL
                                
                SELECT
                  urma.user_id AS user_id,
                  kru.name
                FROM user_role_mapping AS urma
                JOIN
                  keycloak_role AS krsa
                  ON (
                    krsa.name = 'admin'
                    AND urma.role_id = krsa.id
                  )
                JOIN user_role_mapping AS urmta
                ON urma.user_id = urmta.user_id
                JOIN
                  keycloak_role AS kru
                  ON (
                    kru.name = 'tenant_tenant01'
                    AND urmta.role_id = kru.id
                  )
                """.indent(1);

        String result = PostgresService
                .buildListAdminUsersIdForTenant("tenant01")
                .getSQL(INLINED);

        assertEquals(
                expected.trim().replaceAll("\\s+", " "),
                result.trim().replaceAll("\\s+", " ")
        );
    }


    @Test
    void buildTypologyEntityListSqlRequest() {

        String expected =
                """
                SELECT
                    t.id AS id,
                    t.name AS name,
                    t.description AS description,
                    t.name AS order1_name,
                    NULL AS order2_name,
                    NULL AS parent_id,
                    count(st.id) AS children_count
                FROM type AS t
                LEFT OUTER JOIN subtype AS st
                  ON st.parent_id = t.id
                WHERE (
                  t.tenant_id = 'tenant01'
                  AND (
                    t.name ILIKE '%search_word%'
                    OR st.name ILIKE '%search_word%'
                  )
                )
                GROUP BY t.id, t.name, t.description, parent_id, order1_name, order2_name
                UNION ALL
                SELECT
                    st.id AS id,
                    st.name AS name,
                    st.description AS description,
                    t.name AS order1_name,
                    st.name AS order2_name,
                    t.id AS parent_id,
                    0 AS children_count
                FROM subtype AS st
                JOIN type AS t
                  ON st.parent_id = t.id
                WHERE (
                  st.tenant_id = 'tenant01'
                  AND t.id NOT IN ('type_15870')
                  AND st.name ILIKE '%search_word%'
                )
                ORDER BY order1_name ASC, order2_name ASC NULLS FIRST
                OFFSET 150 ROWS
                FETCH NEXT 50 ROWS ONLY
                """.indent(1);

        Pageable pageable = PageRequest.of(3, 50);
        String result = PostgresService
                .buildTypologyEntityListSqlRequest("tenant01", pageable, false, Set.of("type_15870"), "search_word")
                .getSQL(INLINED);

        assertEquals(
                expected.trim().replaceAll("\\s+", " "),
                result.trim().replaceAll("\\s+", " ")
        );
    }


    @Test
    void buildTypologyEntityListSqlRequest_collapseAll() {

        String expected =
                """
                SELECT
                    t.id AS id,
                    t.name AS name,
                    t.description AS description,
                    t.name AS order1_name,
                    NULL AS order2_name,
                    NULL AS parent_id,
                    count(st.id) AS children_count
                FROM type AS t
                LEFT OUTER JOIN subtype AS st
                  ON st.parent_id = t.id
                WHERE (   t.tenant_id = 'tenant01'
                  AND 1 = 1
                )
                GROUP BY t.id, t.name, t.description, parent_id, order1_name, order2_name
                UNION ALL
                SELECT
                    st.id AS id,
                    st.name AS name,
                    st.description AS description,
                    t.name AS order1_name,
                    st.name AS order2_name,
                    t.id AS parent_id,
                    0 AS children_count
                FROM subtype AS st
                JOIN type AS t
                  ON st.parent_id = t.id
                WHERE (
                  st.tenant_id = 'tenant01'
                  AND t.id IN ('type_15870')
                  AND 1 = 1
                )
                ORDER BY order1_name ASC, order2_name ASC NULLS FIRST
                OFFSET 150 ROWS
                FETCH NEXT 50 ROWS ONLY
                """.indent(1);

        Pageable pageable = PageRequest.of(3, 50);
        String result = PostgresService
                .buildTypologyEntityListSqlRequest("tenant01", pageable, true, Set.of("type_15870"), null)
                .getSQL(INLINED);

        assertEquals(
                expected.trim().replaceAll("\\s+", " "),
                result.trim().replaceAll("\\s+", " ")
        );
    }


    @Test
    void buildTypologyEntityTotalSqlRequest() {

        String expected =
                """
                SELECT count(*)
                FROM (
                  SELECT    t.id AS id
                  FROM type AS t
                  LEFT OUTER JOIN subtype AS st
                    ON st.parent_id = t.id
                  WHERE (
                    t.tenant_id = 'tenant01'
                    AND 1 = 1
                  )
                  UNION ALL
                  SELECT
                    st.id AS id
                  FROM subtype AS st
                  JOIN type AS t
                    ON st.parent_id = t.id
                  WHERE (
                    st.tenant_id = 'tenant01'
                    AND t.id NOT IN ('type_15870')
                    AND 1 = 1
                  )
                )
                """;

        String result = PostgresService
                .buildTypologyEntityTotalSqlRequest("tenant01", false, Set.of("type_15870"), null)
                .getSQL(INLINED)
                .replaceAll(" AS alias_.*?$", "");

        assertEquals(
                expected.trim().replaceAll("\\s+", " "),
                result.trim().replaceAll("\\s+", " ")
        );
    }


    @Test
    void buildTypologyEntityTotalSqlRequest_collapseAll() {

        String expected =
                """
                SELECT count(*)
                FROM (
                  SELECT
                    t.id AS id
                  FROM type AS t
                  LEFT OUTER JOIN subtype AS st
                    ON st.parent_id = t.id
                  WHERE (
                    t.tenant_id = 'tenant01'
                    AND 1 = 1
                  )
                  UNION ALL
                  SELECT
                    st.id AS id
                  FROM subtype AS st
                  JOIN type AS t
                    ON st.parent_id = t.id
                  WHERE (
                    st.tenant_id = 'tenant01'
                    AND t.id IN ('type_15870')
                    AND 1 = 1
                  )
                )
                """;

        String result = PostgresService
                .buildTypologyEntityTotalSqlRequest("tenant01", true, Set.of("type_15870"), null)
                .getSQL(INLINED)
                .replaceAll(" AS alias_.*?$", "");

        assertEquals(
                expected.trim().replaceAll("\\s+", " "),
                result.trim().replaceAll("\\s+", " ")
        );
    }


    @Test
    void buildTasksSqlRequest() {
        String expected =
                """
                SELECT
                  ACT_RU_EXECUTION.root_proc_inst_id_ AS instance_id,
                  act_ru_exec_root.name_ AS instance_name,
                  ACT_RU_IDENTITYLINK.group_id_ AS candidate_group,
                  ACT_RU_TASK.id_ AS task_id,
                  ACT_RU_TASK.name_ AS task_name,
                  ACT_RU_TASK.due_date_ AS due_date,
                  ACT_RU_EXECUTION.start_time_ AS begin_date,
                  NULL AS end_date,
                  array_agg(DISTINCT ARRAY[
                    act_ru_variable_md.name_,
                    act_ru_variable_md.text_
                  ]) AS variables,
                  typology.name AS sort_by
                FROM ACT_RU_IDENTITYLINK
                  JOIN ACT_RU_TASK
                    ON ACT_RU_IDENTITYLINK.task_id_ = ACT_RU_TASK.id_
                  JOIN ACT_RU_EXECUTION
                    ON ACT_RU_EXECUTION.id_ = ACT_RU_TASK.proc_inst_id_
                  JOIN ACT_RU_EXECUTION AS act_ru_exec_root
                    ON act_ru_exec_root.id_ = ACT_RU_EXECUTION.root_proc_inst_id_
                  JOIN act_ru_variable AS act_ru_variable_md
                    ON ACT_RU_EXECUTION.root_proc_inst_id_ = act_ru_variable_md.proc_inst_id_
                  LEFT OUTER JOIN act_ru_variable AS act_ru_variables_sb
                    ON (
                      ACT_RU_EXECUTION.root_proc_inst_id_ = act_ru_variables_sb.proc_inst_id_
                      AND act_ru_variables_sb.name_ = 'i_Parapheur_internal_type_id'
                    )
                  LEFT OUTER JOIN type AS typology
                    ON act_ru_variables_sb.text_ = typology.id
                  LEFT OUTER JOIN act_ru_variable AS act_ru_variables_db
                    ON ACT_RU_EXECUTION.root_proc_inst_id_ = act_ru_variables_db.proc_inst_id_
                WHERE (
                  ACT_RU_IDENTITYLINK.type_ = 'candidate'
                  AND ACT_RU_IDENTITYLINK.group_id_ IN ('27551ec8-38b3-4cb9-9d57-6ccb615a3f26')
                  AND ACT_RU_TASK.name_ <> 'undo'
                  AND ACT_RU_TASK.name_ <> 'workflow_internal_draft'
                  AND ACT_RU_TASK.name_ <> 'workflow_internal_rejected'  AND ACT_RU_TASK.name_ <> 'workflow_internal_finished'
                )
                GROUP BY
                  instance_id,
                  instance_name,
                  candidate_group,
                  task_id,
                  task_name,
                  due_date,
                  begin_date,
                  sort_by
                ORDER BY
                  sort_by ASC,
                  begin_date ASC
                OFFSET 50 ROWS
                FETCH NEXT 50 ROWS ONLY
                """;

        Desk testDesk = Desk.builder().id("27551ec8-38b3-4cb9-9d57-6ccb615a3f26").build();
        String result = PostgresService
                .buildTasksSqlRequest(testDesk, PENDING, TYPE_NAME, true, 1, 50)
                .getSQL(INLINED);

        assertEquals(
                expected.trim().replaceAll("\\s+", " "),
                result.trim().replaceAll("\\s+", " ")
        );
    }


}
