/*
 * iparapheur Core
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package coop.libriciel.ipcore.services.content;

import coop.libriciel.ipcore.model.database.Tenant;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;


@SpringBootTest
public class AlfrescoServiceTest {

//    @Mock
//    private RestTemplate restTemplate = mock(RestTemplate.class);
//
//    @InjectMocks
//    private AlfrescoService alfrescoService = new AlfrescoService("localhost", 9000, "admin", "admin");
//
//    private ObjectMapper mapper = new ObjectMapper();


    @Test
    void computePathPlaceholders() {

        String pathTenant00 = AlfrescoService.computePathPlaceholders(Tenant.builder().id("tenant0").name("Tenant 0").index(0L).build());
        assertNotNull(pathTenant00);
        assertTrue(pathTenant00.matches("authorities/0/tenant0/folders/\\d+/\\d+-\\d+/\\d+-\\d+"));

        String pathTenant1K = AlfrescoService.computePathPlaceholders(Tenant.builder().id("tenant1001").name("Tenant 1001").index(1001L).build());
        assertNotNull(pathTenant1K);
        assertTrue(pathTenant1K.matches("authorities/1/tenant1001/folders/\\d+/\\d+-\\d+/\\d+-\\d+"));
    }


//    @Before public void generateHeaders() throws JsonProcessingException {
//        AlfrescoTicketModel ticketModel = new AlfrescoTicketModel();
//        HashMap<String, String> entry = new HashMap<>();
//        entry.put("id", "bearer_token");
//        ticketModel.setEntry(entry);
//
//        ResponseEntity<String> ticket = new ResponseEntity<>(mapper.writeValueAsString(ticketModel), HttpStatus.OK);
//
//        Mockito.when(restTemplate.exchange(
//                ArgumentMatchers.any(),
//                ArgumentMatchers.eq(HttpMethod.POST),
//                ArgumentMatchers.any(HttpEntity.class),
//                ArgumentMatchers.eq(String.class)))
//                .thenReturn(ticket);
//
//        alfrescoService.generateHeaders();
//    }
//
//
//    @Test public void test4xxErrorCode() {
//        Mockito.when(restTemplate.exchange(
//                ArgumentMatchers.any(),
//                ArgumentMatchers.eq(HttpMethod.POST),
//                ArgumentMatchers.any(HttpEntity.class),
//                ArgumentMatchers.eq(String.class)))
//                .thenThrow(new HttpClientErrorException(HttpStatus.UNAUTHORIZED));
//
//        alfrescoService.generateHeaders();
//    }
//
//
//    @Test public void testPopulateInfos() {
//        // Empty infos
//        alfrescoService.populateInfos(new String[]{});
//        // Only one info
//        alfrescoService.populateInfos(new String[]{"admin"});
//        // Double info
//        alfrescoService.populateInfos(new String[]{"admin", "admin"});
//    }
//
//
//    @Test public void testCreateContent() throws JsonProcessingException {
//        MockMultipartFile file = new MockMultipartFile("file", "filename.txt", "text/plain", "some text".getBytes());
//
//        AlfrescoEntryModel mockedResponse = new AlfrescoEntryModel();
//        HashMap<String, Object> entry = new HashMap<>();
//        entry.put("id", UUID.randomUUID().toString());
//        mockedResponse.setEntry(entry);
//
//        ResponseEntity<String> nodeCreatedResponse = new ResponseEntity<>(mapper.writeValueAsString(mockedResponse), HttpStatus.OK);
//
//        Mockito.when(restTemplate.exchange(
//                ArgumentMatchers.any(),
//                ArgumentMatchers.any(),
//                ArgumentMatchers.any(HttpEntity.class),
//                ArgumentMatchers.eq(String.class)))
//                .thenReturn(nodeCreatedResponse);
//
//        alfrescoService.createContent(file);
//    }

}
