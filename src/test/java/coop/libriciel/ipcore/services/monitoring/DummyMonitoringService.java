/*
 * iparapheur Core
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package coop.libriciel.ipcore.services.monitoring;

import coop.libriciel.ipcore.model.monitoring.JobDataResponse;
import coop.libriciel.ipcore.model.monitoring.JobEnum;
import org.jetbrains.annotations.NotNull;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import java.util.List;

import static coop.libriciel.ipcore.utils.TextUtilsTest.DUMMY_SERVICE;


@Service(MonitoringServiceInterface.BEAN_NAME)
@ConditionalOnProperty(name = MonitoringServiceInterface.PREFERENCES_PROVIDER_KEY, havingValue = DUMMY_SERVICE)
public class DummyMonitoringService implements MonitoringServiceInterface {

    @Override
    public JobDataResponse getJobData(@NotNull JobEnum job) {
        return null;
    }


    @Override
    public List<JobDataResponse> getJobsData() {
        return null;
    }


    @Override
    public Long getLocalSpaceUsage() {
        return null;
    }
}
