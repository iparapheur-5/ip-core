///*
// * iparapheur Core
// * Copyright (C) 2018-2020 Libriciel-SCOP
// *
// * This program is free software: you can redistribute it and/or modify
// * it under the terms of the GNU Affero General Public License as
// * published by the Free Software Foundation, either version 3 of the
// * License, or (at your option) any later version.
// *
// * This program is distributed in the hope that it will be useful,
// * but WITHOUT ANY WARRANTY; without even the implied warranty of
// * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// * GNU Affero General Public License for more details.
// *
// * You should have received a copy of the GNU Affero General Public License
// * along with this program.  If not, see <https://www.gnu.org/licenses/>.
// */
//package coop.libriciel.ipcore.controller;
//
//import com.fasterxml.jackson.databind.ObjectMapper;
//import coop.libriciel.ipcore.model.content.CreateFolderResponse;
//import coop.libriciel.ipcore.model.crypto.DataToSign;
//import coop.libriciel.ipcore.model.workflow.Folder;
//import coop.libriciel.ipcore.services.redis.FolderRepository;
//import org.junit.*;
//import org.junit.jupiter.api.extension.ExtendWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.mock.web.MockMultipartFile;
//import org.springframework.security.test.context.support.WithMockUser;
//import org.springframework.test.context.junit.jupiter.SpringExtension;
//import org.springframework.test.web.servlet.MockMvc;
//import redis.embedded.RedisServer;
//
//import java.io.IOException;
//
//import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
//
//
//@ExtendWith(SpringExtension.class)
//@SpringBootTest
//@AutoConfigureMockMvc
//@WithMockUser
//public class FolderControllerTest {
//
//
//    private static final String KEYSTORE_JKS_PUBLIC_KEY = "" +
//            "MIIEvDCCA6SgAwIBAgIJAMXb2wwwEe7bMA0GCSqGSIb3DQEBBQUAMIGYMQswCQYD" +
//            "VQQGEwJGUjEQMA4GA1UECBMHSGVyYXVsdDEYMBYGA1UEChMPQURVTExBQ1QtUHJv" +
//            "amV0MRgwFgYDVQQLEw9BRFVMTEFDVC1Qcm9qZXQxHjAcBgNVBAMTFUFDIEFEVUxM" +
//            "QUNUIFByb2pldCBnMjEjMCEGCSqGSIb3DQEJARYUc3lzdGVtZUBhZHVsbGFjdC5v" +
//            "cmcwHhcNMTAxMjAzMTcyNDAwWhcNMjAwNzAzMTcyNDAwWjCBzzELMAkGA1UEBhMC" +
//            "RlIxEDAOBgNVBAgTB0hlcmF1bHQxFDASBgNVBAcTC01vbnRwZWxsaWVyMREwDwYD" +
//            "VQQKEwhBRFVMTEFDVDEhMB8GA1UECxMYaVBhcmFwaGV1ci1EZW1vbnN0cmF0aW9u" +
//            "MS8wLQYDVQQDEyZpcGFyYXBoZXVyLmRlbW9uc3RyYXRpb25zLmFkdWxsYWN0Lm9y" +
//            "ZzExMC8GCSqGSIb3DQEJARYic3RlcGhhbmUudmFzdEBhZHVsbGFjdC1wcm9qZXQu" +
//            "Y29vcDCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAOOBtmbcik0+fpPx" +
//            "wYboKuxgelLsK2RM0ZZliUADjYK1cE2MlipU8BNLgCOYViyM54cGruNXOKQDH8HR" +
//            "v0YtzvY2oqeLxCRLVXkZwLBF+g4W68h7BJj8iFg56iOb2DW4AFqDloPpEuv5fPGT" +
//            "qEipAZ4VBio3kvsgAhugMEcYP4SX38GcZCTyey5C+0dhwg5s+EVNQ1no/DCIusT5" +
//            "ZIhEAUwNL1c8PeZt+6GeSHeu2y4caZrewX5xFMPbbS9t+cNUMamdgrB9PTbWzxu/" +
//            "0AxanBO/niCd4pycyorqgBjxCz62gozt0uPFQ1A2EHjye8KbIEmnX3hT2QdcOp7H" +
//            "/ReQdHMCAwEAAaOBzzCBzDAJBgNVHRMEAjAAMBEGCWCGSAGG+EIBAQQEAwIE8DAL" +
//            "BgNVHQ8EBAMCBPAwLAYJYIZIAYb4QgENBB8WHU9wZW5TU0wgR2VuZXJhdGVkIENl" +
//            "cnRpZmljYXRlMB0GA1UdDgQWBBQnKfd6xieeSFOVJJLw2RjQ4y0tpzAfBgNVHSME" +
//            "GDAWgBTMw0TiHX0eWOdzkNHBq8KnjELPGTAxBglghkgBhvhCAQQEJBYiaHR0cDov" +
//            "L2NybC5hZHVsbGFjdC5vcmcvQ1JMX2cyLnBlbTANBgkqhkiG9w0BAQUFAAOCAQEA" +
//            "G4vhv+haqbaLes5Vx0XS9S6M+TM9ImYCXqa3NZ2QQxqyZyfKuD6uP/YyEDl2JLjM" +
//            "U1hlQONbA5xja+P0Gtupvgx7TucOopnxUMPDMf5nkNuJt3aaCL19FdL2DkdhzPvl" +
//            "fiUTVyHuXuwGCda987kG62T4MCttjz6mv1wfSa2ZEOPizZo665Ftu+esd0d2tJaE" +
//            "0RDsJmSA/UYjHDNOzaD3xAGCcbqbpbxiOnCwhqLt9foTYiWO0xVTmHKDOMZ3Fcz0" +
//            "aI4sMpE2XfSFX2lfQgT4zJMqLYj21aUDFY5GC/6+y/Q0DvrTnG6Zga2YT+k+ADX2" +
//            "zQTAPlHkAUt5Kwtlp9xk7w==";
//
//
//    @Autowired private MockMvc mvc;
//    @Autowired private FolderRepository folderRepository;
//    private static RedisServer mockupRedisServer;
//
//    private ObjectMapper mapper = new ObjectMapper();
//
//
//    // <editor-fold desc="Global rules">
//
//
//    @BeforeAll public static void startMockRedis() throws IOException {
//        mockupRedisServer = new RedisServer(6379);
//        mockupRedisServer.start();
//    }
//
//
//    @Before @After public void cleanup() {
//        folderRepository.deleteAll();
//    }
//
//
//    @AfterClass public static void stopMockRedis() {
//        mockupRedisServer.stop();
//    }
//
//
//    // </editor-fold desc="Global rules">
//
//
//    @Test public void generateDataToSign() throws Exception {
//
//        // Prepare
//
//        Folder testFolder = new Folder("content_id_1234", "content_id_1234", null);
//        folderRepository.save(testFolder);
//
//        // Test
//
//        String result = mvc.perform(
//                get("/folder/dataToSign")
//                        .param("folderId", "content_id_1234")
//                        .param("publicKeyBase64", KEYSTORE_JKS_PUBLIC_KEY)
//                        .with(csrf()))
//                .andExpect(status().isOk())
//                .andReturn().getResponse().getContentAsString();
//
//        // Check
//
//        DataToSign dataToSignResponse = mapper.readValue(result, DataToSign.class);
//        Assert.assertNotNull(dataToSignResponse);
//        Assert.assertFalse(dataToSignResponse.getDataToSignBase64List().isEmpty());
//        Assert.assertNotNull(dataToSignResponse.getSignatureDateTime());
//    }
//
//
//    @Test public void whenNoFile_thenStatus400() throws Exception {
//
//        mvc.perform(
//                multipart("/folder")
//                        .with(csrf()))
//                .andExpect(status().isBadRequest());
//    }
//
//
//    @Test public void whenNoAuth_thenStatus403() throws Exception {
//
//        mvc.perform(multipart("/folder"))
//                .andExpect(status().isForbidden());
//    }
//
//
//    @Test public void whenFile_thenStatus201() throws Exception {
//
//        MockMultipartFile file = new MockMultipartFile("file", "filename.txt", "text/plain", "some text".getBytes());
//
//        String result = mvc.perform(
//                multipart("/folder")
//                        .file(file)
//                        .with(csrf()))
//                .andExpect(status().isCreated())
//                .andReturn().getResponse().getContentAsString();
//
//        CreateFolderResponse createFolderResponse = mapper.readValue(result, CreateFolderResponse.class);
//        Assert.assertNotNull(createFolderResponse.getId());
//        Assert.assertEquals(36, createFolderResponse.getId().length());
//    }
//
//}
