/*
 * iparapheur Core
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.ipcore.controller.admin;

import coop.libriciel.ipcore.model.database.Tenant;
import coop.libriciel.ipcore.model.database.TenantRepresentation;
import coop.libriciel.ipcore.model.database.requests.TenantDto;
import coop.libriciel.ipcore.services.database.DatabaseServiceInterface;
import coop.libriciel.ipcore.services.database.TenantRepository;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.UUID;
import java.util.stream.StreamSupport;

import static coop.libriciel.ipcore.model.database.TenantSortBy.Constants.NAME_VALUE;
import static coop.libriciel.ipcore.utils.TextUtils.MESSAGE_BUNDLE;
import static java.lang.Integer.MAX_VALUE;
import static org.junit.jupiter.api.Assertions.*;


@Log4j2
@SpringBootTest
@ExtendWith(SpringExtension.class)
@WithMockUser(
        username = "Jean",
        roles = {"admin"}
)
public class AdminTenantControllerTest {


    @Autowired DatabaseServiceInterface databaseService;
    @Autowired AdminTenantController adminTenantController;
    @Autowired TenantRepository tenantRepository;


    @BeforeEach
    @AfterEach
    void cleanup() {

        StreamSupport.stream(tenantRepository.findAll().spliterator(), false)
                .forEach(tenantRepository::delete);

        ResourceBundle messageResourceBundle = ResourceBundle.getBundle(MESSAGE_BUNDLE, Locale.getDefault());
        String defaultTenantName = messageResourceBundle.getString("message.initial_tenant");
        tenantRepository.save(
                Tenant.builder()
                        .id(UUID.randomUUID().toString())
                        .name(defaultTenantName)
                        .index(0L)
                        .contentId(UUID.randomUUID().toString())
                        .statsId(UUID.randomUUID().toString())
                        .build()
        );
    }


    @Test
    void initTest() {

        String defaultTenantId = StreamSupport.stream(tenantRepository.findAll().spliterator(), false).findFirst()
                .map(Tenant::getId)
                .orElseThrow(() -> new AssertionError("No default tenant"));

        PageRequest pageRequest = PageRequest.of(0, MAX_VALUE, Sort.by(NAME_VALUE));
        List<TenantRepresentation> tenantList = adminTenantController.listTenantsAsAdmin(pageRequest, null).getContent();
        assertEquals(1, tenantList.size());

        TenantRepresentation defaultTenant = tenantList.get(0);
        assertNotNull(defaultTenant);
        assertEquals(defaultTenantId, defaultTenant.getId());
    }


    @Test
    void createTenant() {
        TenantDto tenantDto01 = new TenantDto("Tenant 01");
        TenantDto tenantDto02 = new TenantDto("Tenant 02");

        TenantDto tenant01 = adminTenantController.createTenant(tenantDto01);
        TenantDto tenant02 = adminTenantController.createTenant(tenantDto02);

        assertNotNull(tenant01);
        assertNotNull(tenant02);
        assertFalse(StringUtils.isEmpty(tenant01.getId()));
        assertFalse(StringUtils.isEmpty(tenant02.getId()));
        assertEquals("Tenant 01", tenant01.getName());
        assertEquals("Tenant 02", tenant02.getName());
    }


    @Test
    void editTenant() {

        TenantDto tenantDto = new TenantDto("Tenant 01");
        tenantDto = adminTenantController.createTenant(tenantDto);

        Tenant tenant = tenantRepository.findById(tenantDto.getId())
                .orElseGet(Assertions::fail);

        assertEquals("Tenant 01", tenant.getName());

        TenantDto modifiedDto = new TenantDto("Tenant 01 with new name");
        modifiedDto.setId(tenant.getId());
        adminTenantController.updateTenant(tenant.getId(), tenant, modifiedDto);

        tenant = tenantRepository.findById(tenantDto.getId())
                .orElseGet(Assertions::fail);

        assertEquals("Tenant 01 with new name", tenant.getName());
    }


}