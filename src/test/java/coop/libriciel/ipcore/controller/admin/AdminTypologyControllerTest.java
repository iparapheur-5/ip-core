/*
 * iparapheur Core
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.ipcore.controller.admin;

import coop.libriciel.ipcore.model.database.Tenant;
import coop.libriciel.ipcore.model.database.Type;
import coop.libriciel.ipcore.model.database.TypologyRepresentation;
import coop.libriciel.ipcore.model.database.requests.SubtypeDto;
import coop.libriciel.ipcore.model.database.requests.TypeDto;
import coop.libriciel.ipcore.services.database.SubtypeRepository;
import coop.libriciel.ipcore.services.database.TenantRepository;
import coop.libriciel.ipcore.services.database.TypeRepository;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.stream.IntStream;
import java.util.stream.StreamSupport;

import static coop.libriciel.ipcore.model.crypto.SignatureFormat.PES_V2;
import static coop.libriciel.ipcore.services.auth.KeycloakService.SUPER_ADMIN_ROLE_NAME;
import static java.lang.Integer.MAX_VALUE;
import static java.util.Collections.emptyList;
import static java.util.Collections.emptySet;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;


@Log4j2
@SpringBootTest
@AutoConfigureMockMvc
@ExtendWith(SpringExtension.class)
@WithMockUser(username = "admin", roles = {SUPER_ADMIN_ROLE_NAME})
public class AdminTypologyControllerTest {


    @Autowired AdminTypologyController adminTypologyController;
    @Autowired TypeRepository typeRepository;
    @Autowired TenantRepository tenantRepository;
    @Autowired SubtypeRepository subtypeRepository;
    @Autowired ModelMapper modelMapper;


    // <editor-fold desc="Utils">


    private static TypeDto dummyType(int id) {
        return new TypeDto() {{
            setName("type_" + id);
            setSignatureFormat(PES_V2);
        }};
    }


    private static SubtypeDto dummySubtype(Pair<Type, Integer> index) {
        return new SubtypeDto() {{
            setName(index.getKey().getName() + ".subtype_" + index.getValue());
            setDescription("Description subtype " + index.getKey().getId() + "." + index.getValue());
            setCreationPermittedDeskIds(emptyList());
            setCreationPermittedDesks(emptyList());
            setFilterableByDeskIds(emptyList());
            setFilterableByDesks(emptyList());
            setSubtypeMetadataList(emptyList());
            setSubtypeLayers(emptyList());
        }};
    }


    // </editor-fold desc="Utils">


    @BeforeEach
    void setup() {

        cleanup();

        Tenant tenant = tenantRepository.save(Tenant.builder().id("tenant01").build());

        // Creating 10 types and 10 subtypes for each
        IntStream.range(0, 10)
                .mapToObj(i -> adminTypologyController.createType(tenant.getId(), tenant, dummyType(i)))
                .map(typeDto -> modelMapper.map(typeDto, Type.class))
                .peek(type -> type.setTenant(tenant))
                .flatMap(t -> IntStream.range(0, 10).mapToObj(i -> Pair.of(t, i)))
                .forEach(p -> adminTypologyController.createSubtype(tenant.getId(), p.getKey().getId(), p.getKey(), dummySubtype(p)));

        assertEquals(10, typeRepository.count());
        assertEquals(100, subtypeRepository.count());
    }


    @AfterEach
    void cleanup() {
        subtypeRepository.deleteAll();
        typeRepository.deleteAll();
        tenantRepository.deleteAll();
    }


    @Test
    void createType() {

        Tenant defaultTenant = StreamSupport.stream(tenantRepository.findAll().spliterator(), false)
                .findFirst()
                .orElseThrow(() -> new AssertionError("No default tenant"));

        adminTypologyController.createType(defaultTenant.getId(), defaultTenant, dummyType(9999));
    }


    @Test
    void list() {

        Tenant defaultTenant = StreamSupport.stream(tenantRepository.findAll().spliterator(), false)
                .findFirst()
                .orElseThrow(() -> new AssertionError("No default tenant"));

        Page<TypologyRepresentation> list = adminTypologyController.getTypologyHierarchy(
                defaultTenant.getId(),
                defaultTenant,
                false,
                emptySet(),
                PageRequest.of(0, MAX_VALUE),
                null
        );

        assertEquals(110, list.getContent().size());
        assertEquals(110, list.getTotalElements());

        assertTrue(list.getContent().stream().map(TypologyRepresentation::getId).allMatch(StringUtils::isNotEmpty));
        assertTrue(list.getContent().stream().map(TypologyRepresentation::getName).allMatch(StringUtils::isNotEmpty));

        assertEquals("type_0", list.getContent().get(0).getName());
        assertTrue(list.getContent().stream()
                .skip(1).limit(10)
                .map(TypologyRepresentation::getName)
                .allMatch(s -> StringUtils.startsWith(s, "type_0.subtype_"))
        );
        assertEquals("type_9", list.getContent().get(99).getName());
        assertTrue(list.getContent().stream()
                .skip(100).limit(10)
                .map(TypologyRepresentation::getName)
                .allMatch(s -> StringUtils.startsWith(s, "type_9.subtype_"))
        );
    }


}