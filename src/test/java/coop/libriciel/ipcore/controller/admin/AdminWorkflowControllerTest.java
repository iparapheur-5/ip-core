/*
 * iparapheur Core
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.ipcore.controller.admin;

import coop.libriciel.ipcore.model.auth.DeskRepresentation;
import coop.libriciel.ipcore.model.workflow.StepDefinition;
import coop.libriciel.ipcore.model.workflow.WorkflowDefinition;
import coop.libriciel.ipcore.utils.LocalizedStatusException;
import lombok.extern.log4j.Log4j2;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.io.InputStream;
import java.util.List;
import java.util.UUID;

import static coop.libriciel.ipcore.model.workflow.Action.VISA;
import static coop.libriciel.ipcore.model.workflow.StepDefinitionParallelType.OR;
import static coop.libriciel.ipcore.model.workflow.WorkflowDefinition.BOSS_OF_ID_PLACEHOLDER;
import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static org.junit.jupiter.api.Assertions.*;


@Log4j2
@SpringBootTest
@ExtendWith(SpringExtension.class)
public class AdminWorkflowControllerTest {


    @Test
    void patchBpmn_bpmnIo() throws Exception {

        InputStream simpleWorkflowInputStream = getClass()
                .getClassLoader()
                .getResourceAsStream("bpmn/http.bpmn.io_visaDRH_visaDGS_signPresident.bpmn20.xml");

        String result = AdminWorkflowDefinitionController.patchBpmn(getClass().getClassLoader(), simpleWorkflowInputStream);
        String expected =
                """
                <bpmn:definitions xmlns:bpmn="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:flowable="http://flowable.org/bpmn" \
                exporter="bpmn-js (https://demo.bpmn.io)" exporterVersion="3.2.1" id="Definitions_1osu5pz" targetNamespace="http://bpmn.io/schema/bpmn">
                  <bpmn:process id="Process_1lzk2p2" isExecutable="true" name="Process_1lzk2p2">
                    <bpmn:startEvent id="StartEvent_0gowkhg">
                      <bpmn:outgoing>SequenceFlow_0jia701</bpmn:outgoing>
                    </bpmn:startEvent>
                    <bpmn:sequenceFlow id="SequenceFlow_0jia701" sourceRef="StartEvent_0gowkhg" targetRef="Task_0tu55t0"></bpmn:sequenceFlow>
                    <bpmn:sequenceFlow id="SequenceFlow_0889tye" sourceRef="Task_0tu55t0" targetRef="Task_0ii92vr"></bpmn:sequenceFlow>
                    <bpmn:sequenceFlow id="SequenceFlow_0sgwjie" sourceRef="Task_0ii92vr" targetRef="Task_08nvn6g"></bpmn:sequenceFlow>
                    <bpmn:endEvent id="EndEvent_0cokryq">
                      <bpmn:incoming>SequenceFlow_0iuof41</bpmn:incoming>
                    </bpmn:endEvent>
                    <bpmn:sequenceFlow id="SequenceFlow_0iuof41" sourceRef="Task_08nvn6g" targetRef="EndEvent_0cokryq"></bpmn:sequenceFlow>
                    <bpmn:subProcess id="Task_0tu55t0" name="visa DRH">
                      <bpmn:incoming>SequenceFlow_0jia701</bpmn:incoming>
                      <bpmn:outgoing>SequenceFlow_0889tye</bpmn:outgoing>
                    </bpmn:subProcess>
                    <bpmn:subProcess id="Task_0ii92vr" name="visa DGS">
                      <bpmn:incoming>SequenceFlow_0889tye</bpmn:incoming>
                      <bpmn:outgoing>SequenceFlow_0sgwjie</bpmn:outgoing>
                    </bpmn:subProcess>
                    <bpmn:subProcess id="Task_08nvn6g" name="signature Président">
                      <bpmn:incoming>SequenceFlow_0sgwjie</bpmn:incoming>
                      <bpmn:outgoing>SequenceFlow_0iuof41</bpmn:outgoing>
                    </bpmn:subProcess>
                  </bpmn:process>
                </bpmn:definitions>
                """;

        assertEquals(expected.trim(), result.trim());
    }


    @Test
    void patchBpmn_flowableModeler() throws Exception {

        InputStream simpleWorkflowInputStream = getClass().getClassLoader().getResourceAsStream("bpmn/flowableModeler_simple_workflow.bpmn20.xml");
        String result = AdminWorkflowDefinitionController.patchBpmn(getClass().getClassLoader(), simpleWorkflowInputStream);
        String expected =
                """
                <bpmn:definitions xmlns:bpmn="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:flowable="http://flowable.org/bpmn" expressionLanguage="http://www.w3.org/1999/XPath" targetNamespace="http://www.flowable.org/processdef" typeLanguage="http://www.w3.org/2001/XMLSchema">
                  <process xmlns="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI" xmlns:omgdc="http://www.omg.org/spec/DD/20100524/DC" xmlns:omgdi="http://www.omg.org/spec/DD/20100524/DI" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" id="simple_workflow" isExecutable="true" name="Simple workflow">
                    <documentation>Straighforward visa-signature workflow</documentation>
                    <startEvent id="startEvent1"></startEvent>
                    <callActivity calledElement="visa" id="visa_DRH" name="visa DRH" flowable:inheritBusinessKey="true" flowable:inheritVariables="false">
                      <bpmn:extensionElements>
                        <flowable:in sourceExpression="DRH" target="current_candidate_groups"></flowable:in>
                        <flowable:in sourceExpression="admins" target="previous_candidate_groups"></flowable:in>
                        <flowable:in sourceExpression="unknown" target="previous_action"></flowable:in>
                      </bpmn:extensionElements>
                    </callActivity>
                    <callActivity calledElement="signature" id="signature_President" name="signature Président" flowable:inheritBusinessKey="true" flowable:inheritVariables="false">
                      <bpmn:extensionElements>
                        <flowable:in sourceExpression="Président" target="current_candidate_groups"></flowable:in>
                        <flowable:in sourceExpression="DRH" target="previous_candidate_groups"></flowable:in>
                        <flowable:in sourceExpression="visa" target="previous_action"></flowable:in>
                      </bpmn:extensionElements>
                    </callActivity>
                    <bpmn:boundaryEvent attachedToRef="signature_President" id="signature_President_undo_catch">
                      <bpmn:errorEventDefinition errorRef="undo"></bpmn:errorEventDefinition>
                    </bpmn:boundaryEvent>
                    <bpmn:sequenceFlow id="signature_President_undo_catch_back_sequenceflow" sourceRef="signature_President_undo_catch" targetRef="visa_DRH"></bpmn:sequenceFlow>
                    <endEvent id="sid-60A14468-0498-4BAA-9FCE-03AE43CF47F7"></endEvent>
                    <sequenceFlow id="sid-24850A4C-275C-4A43-9608-CF356255317F" sourceRef="signature_President" targetRef="sid-60A14468-0498-4BAA-9FCE-03AE43CF47F7"></sequenceFlow>
                    <sequenceFlow id="sid-E81E772C-1D9E-433F-8AAE-7F5F3B5C5925" sourceRef="startEvent1" targetRef="visa_DRH"></sequenceFlow>
                    <sequenceFlow id="sid-D05D909D-956E-4A7F-BA5D-6008F4DC6541" sourceRef="visa_DRH" targetRef="signature_President"></sequenceFlow>
                  </process>
                </bpmn:definitions>
                """;

        assertEquals(expected.trim(), result.trim());
    }


    @Test
    void checkIntegrity_nullityError() {

        StepDefinition stepDefinition = new StepDefinition(UUID.randomUUID().toString(), VISA, emptyList(), emptyList(), emptyList(), emptyList(), OR);
        WorkflowDefinition workflowDefinition = new WorkflowDefinition();
        workflowDefinition.setId(UUID.randomUUID().toString());

        // Null steps

        workflowDefinition.setSteps(null);
        assertThrows(LocalizedStatusException.class, () -> AdminWorkflowDefinitionController.checkDefinitionIntegrity(workflowDefinition));

        workflowDefinition.setSteps(singletonList(null));
        assertThrows(LocalizedStatusException.class, () -> AdminWorkflowDefinitionController.checkDefinitionIntegrity(workflowDefinition));

        // Null validators

        workflowDefinition.setSteps(List.of(stepDefinition));

        stepDefinition.setValidatingDesks(null);
        assertThrows(LocalizedStatusException.class, () -> AdminWorkflowDefinitionController.checkDefinitionIntegrity(workflowDefinition));

        stepDefinition.setValidatingDesks(singletonList(null));
        assertThrows(LocalizedStatusException.class, () -> AdminWorkflowDefinitionController.checkDefinitionIntegrity(workflowDefinition));

        stepDefinition.setValidatingDesks(singletonList(new DeskRepresentation()));
        assertThrows(LocalizedStatusException.class, () -> AdminWorkflowDefinitionController.checkDefinitionIntegrity(workflowDefinition));

        // Valid

        stepDefinition.setValidatingDesks(singletonList(new DeskRepresentation(UUID.randomUUID().toString())));
        assertDoesNotThrow(() -> AdminWorkflowDefinitionController.checkDefinitionIntegrity(workflowDefinition));
    }


    @Test
    void checkIntegrity_parallelStepGenericDeskError() {

        WorkflowDefinition workflowDefinition = new WorkflowDefinition();
        workflowDefinition.setId(UUID.randomUUID().toString());
        workflowDefinition.setSteps(List.of(
                new StepDefinition(
                        UUID.randomUUID().toString(), VISA,
                        asList(new DeskRepresentation(UUID.randomUUID().toString()), new DeskRepresentation(BOSS_OF_ID_PLACEHOLDER)),
                        emptyList(), emptyList(), emptyList(), OR
                )
        ));

        assertThrows(LocalizedStatusException.class, () -> AdminWorkflowDefinitionController.checkDefinitionIntegrity(workflowDefinition));
    }


    @Test
    void checkIntegrity_bossOfAfterParallelStepError() {

        WorkflowDefinition workflowDefinition = new WorkflowDefinition();
        workflowDefinition.setId(UUID.randomUUID().toString());
        workflowDefinition.setSteps(List.of(
                new StepDefinition(
                        UUID.randomUUID().toString(), VISA,
                        asList(new DeskRepresentation(UUID.randomUUID().toString()), new DeskRepresentation(UUID.randomUUID().toString())),
                        emptyList(), emptyList(), emptyList(), OR
                ),
                new StepDefinition(
                        UUID.randomUUID().toString(), VISA,
                        singletonList(new DeskRepresentation(BOSS_OF_ID_PLACEHOLDER)),
                        emptyList(), emptyList(), emptyList(), OR
                )
        ));

        assertThrows(LocalizedStatusException.class, () -> AdminWorkflowDefinitionController.checkDefinitionIntegrity(workflowDefinition));
    }


}