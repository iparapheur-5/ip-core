/*
 * iparapheur Core
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package coop.libriciel.ipcore.utils;

import coop.libriciel.ipcore.model.auth.DeskRepresentation;
import coop.libriciel.ipcore.model.auth.User;
import coop.libriciel.ipcore.model.content.DetachedSignature;
import coop.libriciel.ipcore.model.content.Document;
import coop.libriciel.ipcore.model.database.Subtype;
import coop.libriciel.ipcore.model.database.Type;
import coop.libriciel.ipcore.model.workflow.Folder;
import coop.libriciel.ipcore.model.workflow.Task;
import org.apache.commons.lang3.StringUtils;
import org.bouncycastle.jcajce.provider.digest.SHA256;
import org.junit.jupiter.api.Test;

import javax.xml.stream.XMLStreamException;
import javax.xml.transform.TransformerException;
import java.io.ByteArrayInputStream;
import java.util.Date;
import java.util.UUID;

import static coop.libriciel.ipcore.model.crypto.SignatureFormat.PADES;
import static coop.libriciel.ipcore.model.crypto.SignatureFormat.XADES_DETACHED;
import static coop.libriciel.ipcore.model.workflow.Action.SIGNATURE;
import static coop.libriciel.ipcore.model.workflow.Action.VISA;
import static coop.libriciel.ipcore.model.workflow.State.VALIDATED;
import static java.nio.charset.StandardCharsets.UTF_8;
import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.http.MediaType.APPLICATION_PDF;
import static org.springframework.http.MediaType.TEXT_XML;


class XmlUtilsTest {


    @Test
    void canonicalize() throws Exception {
        String poorXml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>   <definitions ></definitions   >   ";
        String canonicalizedXml = XmlUtils.canonicalize(poorXml.getBytes());

        assertEquals("<definitions></definitions>", canonicalizedXml);
    }


    @Test
    void canonicalize_error() {
        String badXml = "<<<<<";
        assertThrows(TransformerException.class, () -> XmlUtils.canonicalize(badXml.getBytes()));
    }


    @Test
    void folderToPremisXml() throws XMLStreamException {

        String expected =
                """
                <?xml version="1.0" encoding="UTF-8"?>
                <premis xmlns="http://www.loc.gov/premis/v3" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="3.0" \
                xsi:schemaLocation="http://www.loc.gov/premis/v3 http://www.loc.gov/standards/premis/v3/premis.xsd">
                  <!-- Folder -->
                  <object xsi:type="intellectualEntity">
                    <objectIdentifier>
                      <objectIdentifierType>folderId</objectIdentifierType>
                      <objectIdentifierValue>12536f34-ce6c-444a-aa28-b9318e0b94f5</objectIdentifierValue>
                    </objectIdentifier>
                    <significantProperties>
                      <significantPropertiesType>type</significantPropertiesType>
                      <significantPropertiesExtension>
                        <significantPropertiesType>typeId</significantPropertiesType>
                        <significantPropertiesValue>8ee47ea0-13b4-444f-a2a8-8c62dd53ba7c</significantPropertiesValue>
                      </significantPropertiesExtension>
                      <significantPropertiesExtension>
                        <significantPropertiesType>typeName</significantPropertiesType>
                        <significantPropertiesValue>PES</significantPropertiesValue>
                      </significantPropertiesExtension>
                    </significantProperties>
                    <significantProperties>
                      <significantPropertiesType>subtype</significantPropertiesType>
                      <significantPropertiesExtension>
                        <significantPropertiesType>subtypeId</significantPropertiesType>
                        <significantPropertiesValue>a023cbf0-6605-4913-92c7-5b75bcc6af9b</significantPropertiesValue>
                      </significantPropertiesExtension>
                      <significantPropertiesExtension>
                        <significantPropertiesType>subtypeName</significantPropertiesType>
                        <significantPropertiesValue>Default</significantPropertiesValue>
                      </significantPropertiesExtension>
                    </significantProperties>
                    <originalName>Folder 001</originalName>
                  </object>
                  <!-- Files -->
                  <object xsi:type="file">
                    <objectIdentifier>
                      <objectIdentifierType>documentId</objectIdentifierType>
                      <objectIdentifierValue>77bd7ea0-29c3-447e-bdfd-b6bb30d77e9d</objectIdentifierValue>
                    </objectIdentifier>
                    <preservationLevel>
                      <preservationLevelValue>mainDocument</preservationLevelValue>
                    </preservationLevel>
                    <objectCharacteristics>
                      <fixity>
                        <messageDigestAlgorithm>SHA-256</messageDigestAlgorithm>
                        <messageDigest>B03D0710D8648314A5FF7D69DBDF3217F9D7775132D2993EBF6C2C896D9B5A81</messageDigest>
                      </fixity>
                      <size>1234</size>
                      <format>
                        <formatDesignation>
                          <formatName>application/pdf</formatName>
                          <formatVersion>1.6</formatVersion>
                        </formatDesignation>
                      </format>
                    </objectCharacteristics>
                    <originalName>Document_foo.pdf</originalName>
                    <signatureInformation>
                      <signature>
                        <signatureEncoding>UTF-8</signatureEncoding>
                        <signatureMethod>XAdES_BASELINE_B</signatureMethod>
                        <signatureValue><![CDATA[signature_1_value]]></signatureValue>
                        <signatureValidationRules>XAdES-1.2.2</signatureValidationRules>
                      </signature>
                    </signatureInformation>
                    <signatureInformation>
                      <signature>
                        <signatureEncoding>UTF-8</signatureEncoding>
                        <signatureMethod>XAdES_BASELINE_B</signatureMethod>
                        <signatureValue><![CDATA[signature_2_value]]></signatureValue>
                        <signatureValidationRules>XAdES-1.2.2</signatureValidationRules>
                      </signature>
                    </signatureInformation>
                  </object>
                  <object xsi:type="file">
                    <objectIdentifier>
                      <objectIdentifierType>documentId</objectIdentifierType>
                      <objectIdentifierValue>2d6684cc-1c9b-42f3-8a91-fb76bb617598</objectIdentifierValue>
                    </objectIdentifier>
                    <preservationLevel>
                      <preservationLevelValue>annex</preservationLevelValue>
                    </preservationLevel>
                    <objectCharacteristics>
                      <fixity>
                        <messageDigestAlgorithm>SHA-256</messageDigestAlgorithm>
                        <messageDigest>2072C9B544878DE7DE0711441A8C61070B0872FA0A4F2A243B45BC11C43729F9</messageDigest>
                      </fixity>
                      <size>4321</size>
                      <format>
                        <formatDesignation>
                          <formatName>application/pdf</formatName>
                          <formatVersion>1.5</formatVersion>
                        </formatDesignation>
                      </format>
                    </objectCharacteristics>
                    <originalName>Annexe.pdf</originalName>
                  </object>
                  <!-- Events -->
                  <event>
                    <eventIdentifier>
                      <eventIdentifierType>taskId</eventIdentifierType>
                      <eventIdentifierValue>e7daa1a7-fcdd-4f7e-81b7-1440c7a941a4</eventIdentifierValue>
                    </eventIdentifier>
                    <eventType>VISA</eventType>
                    <eventDateTime>1970-01-01T01:00:00.000+0100</eventDateTime>
                    <eventOutcomeInformation>
                      <eventOutcomeDetail>
                        <eventOutcomeDetailNote>Annotation Publique</eventOutcomeDetailNote>
                      </eventOutcomeDetail>
                    </eventOutcomeInformation>
                    <linkingAgentIdentifier>
                      <linkingAgentIdentifierType>userId</linkingAgentIdentifierType>
                      <linkingAgentIdentifierValue>458b3c8e-af55-4e3a-bd4f-2705c4b5b741</linkingAgentIdentifierValue>
                      <linkingAgentRole>Bureau de Machin</linkingAgentRole>
                    </linkingAgentIdentifier>
                  </event>
                  <event>
                    <eventIdentifier>
                      <eventIdentifierType>taskId</eventIdentifierType>
                      <eventIdentifierValue>f8ed5dd9-910f-4c33-bab7-854de49f0ea3</eventIdentifierValue>
                    </eventIdentifier>
                    <eventType>SIGNATURE</eventType>
                    <eventDateTime>1970-01-01T01:00:09.999+0100</eventDateTime>
                    <linkingAgentIdentifier>
                      <linkingAgentIdentifierType>userId</linkingAgentIdentifierType>
                      <linkingAgentIdentifierValue>f31b53a2-ffd8-416d-b24e-778bc69d3b8c</linkingAgentIdentifierValue>
                      <linkingAgentRole>Bureau de Truc</linkingAgentRole>
                    </linkingAgentIdentifier>
                  </event>
                  <!-- Users -->
                  <agent>
                    <agentIdentifier>
                      <agentIdentifierType>userId</agentIdentifierType>
                      <agentIdentifierValue>458b3c8e-af55-4e3a-bd4f-2705c4b5b741</agentIdentifierValue>
                    </agentIdentifier>
                    <agentName>Gérard Plip</agentName>
                  </agent>
                  <agent>
                    <agentIdentifier>
                      <agentIdentifierType>userId</agentIdentifierType>
                      <agentIdentifierValue>f31b53a2-ffd8-416d-b24e-778bc69d3b8c</agentIdentifierValue>
                    </agentIdentifier>
                    <agentName>Robert Plop</agentName>
                  </agent>
                </premis>
                """;

        // Building example

        DeskRepresentation desk1 = new DeskRepresentation("deskId_01", "Bureau de Machin");
        DeskRepresentation desk2 = new DeskRepresentation("deskId_02", "Bureau de Truc");

        User user1 = User.builder().id("458b3c8e-af55-4e3a-bd4f-2705c4b5b741").firstName("Gérard").lastName("Plip").build();
        User user2 = User.builder().id("f31b53a2-ffd8-416d-b24e-778bc69d3b8c").firstName("Robert").lastName("Plop").build();

        Task task1 = Task.builder()
                .action(VISA)
                .state(VALIDATED)
                .date(new Date(0L))
                .desks(singletonList(desk1))
                .user(user1)
                .publicAnnotation("Annotation Publique")
                .build();
        Task task2 = Task.builder().action(SIGNATURE).state(VALIDATED).date(new Date(9999L)).desks(singletonList(desk2)).user(user2).build();
        task1.setId("e7daa1a7-fcdd-4f7e-81b7-1440c7a941a4");
        task2.setId("f8ed5dd9-910f-4c33-bab7-854de49f0ea3");

        Document document1 = Document.builder()
                .id("77bd7ea0-29c3-447e-bdfd-b6bb30d77e9d")
                .name("Document_foo.pdf")
                .contentLength(1234)
                .mediaType(APPLICATION_PDF)
                .mediaVersion(1.6F)
                .checksumAlgorithm(new SHA256.Digest().getAlgorithm())
                .checksumValue("B03D0710D8648314A5FF7D69DBDF3217F9D7775132D2993EBF6C2C896D9B5A81")
                .isMainDocument(true)
                .detachedSignatures(asList(
                        new DetachedSignature("detachedSig_1",
                                "myFile1.p7s",
                                1234L,
                                TEXT_XML,
                                "77bd7ea0-29c3-447e-bdfd-b6bb30d77e9d",
                                UUID.randomUUID().toString()),
                        new DetachedSignature("detachedSig_2",
                                "myFile2.p7s",
                                1234L,
                                TEXT_XML,
                                "77bd7ea0-29c3-447e-bdfd-b6bb30d77e9d",
                                UUID.randomUUID().toString())
                ))
                .build();

        Document document2 = Document.builder()
                .id("2d6684cc-1c9b-42f3-8a91-fb76bb617598")
                .name("Annexe.pdf").contentLength(4321)
                .mediaType(APPLICATION_PDF)
                .mediaVersion(1.5F)
                .checksumAlgorithm(new SHA256.Digest().getAlgorithm())
                .checksumValue("2072C9B544878DE7DE0711441A8C61070B0872FA0A4F2A243B45BC11C43729F9")
                .isMainDocument(false)
                .detachedSignatures(emptyList())
                .build();

        Folder folder = new Folder();
        folder.setId("12536f34-ce6c-444a-aa28-b9318e0b94f5");
        folder.setName("Folder 001");
        folder.setType(Type.builder().id("8ee47ea0-13b4-444f-a2a8-8c62dd53ba7c").signatureFormat(XADES_DETACHED).name("PES").build());
        folder.setSubtype(Subtype.builder().id("a023cbf0-6605-4913-92c7-5b75bcc6af9b").name("Default").build());
        folder.setStepList(asList(task1, task2));
        folder.setDocumentList(asList(document1, document2));

        // Actual tests

        String result = XmlUtils.folderToPremisXml(
                folder,
                PADES,
                s -> {
                    if (StringUtils.equals(s, "detachedSig_1")) {return new ByteArrayInputStream("signature_1_value".getBytes(UTF_8));}
                    if (StringUtils.equals(s, "detachedSig_2")) {return new ByteArrayInputStream("signature_2_value".getBytes(UTF_8));}
                    return null;
                }
        ).toString(UTF_8);

        assertNotNull(result);

        // TODO : fix this on CI
        // assertEquals(
        //        expected.trim(),
        //        XmlUtils.prettyPrint(result).trim()
        // );
    }


}
