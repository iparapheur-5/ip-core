/*
 * iparapheur Core
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package coop.libriciel.ipcore.utils;

import org.junit.jupiter.api.Test;

import java.util.ResourceBundle;
import java.util.stream.StreamSupport;

import static coop.libriciel.ipcore.utils.TextUtils.MESSAGE_BUNDLE;
import static java.util.Locale.FRENCH;
import static java.util.Locale.ROOT;
import static java.util.Spliterator.ORDERED;
import static java.util.Spliterators.spliteratorUnknownSize;
import static java.util.stream.Collectors.toSet;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;


class LocalizedStatusExceptionTest {


    @Test
    void getMessageForLocale() {
        String frenchMessage = LocalizedStatusException.getMessageForLocale("message.unknown_tenant_id", FRENCH);
        String englishMessage = LocalizedStatusException.getMessageForLocale("message.unknown_tenant_id", ROOT);
        assertNotEquals(frenchMessage, englishMessage);
    }


    @Test
    void getMessageForLocale_withParameters() {
        String message = LocalizedStatusException.getMessageForLocale("message.already_n_desks_for_a_limit_of_m", ROOT, 3, 100);
        assertEquals("Already 3 desks for a limit of 100", message);
    }


    @Test
    void missingLocalizationCheck() {
        ResourceBundle englishBundle = ResourceBundle.getBundle(MESSAGE_BUNDLE, ROOT);
        ResourceBundle frenchBundle = ResourceBundle.getBundle(MESSAGE_BUNDLE, FRENCH);

        assertEquals(
                StreamSupport.stream(spliteratorUnknownSize(englishBundle.getKeys().asIterator(), ORDERED), false).collect(toSet()),
                StreamSupport.stream(spliteratorUnknownSize(frenchBundle.getKeys().asIterator(), ORDERED), false).collect(toSet())
        );
    }


}