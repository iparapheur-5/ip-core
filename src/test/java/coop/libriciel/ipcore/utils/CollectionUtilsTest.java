/*
 * iparapheur Core
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package coop.libriciel.ipcore.utils;

import org.junit.jupiter.api.Test;

import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;


class CollectionUtilsTest {


    @Test
    void parseMetadata() {
        String[][] dataTest = {{"testKey1", "testValue1"}, {"testKey2", "testValue2"}};
        Map<String, String> parsed = CollectionUtils.parseMetadata(dataTest);

        assertEquals("testValue1", parsed.get("testKey1"));
        assertEquals("testValue2", parsed.get("testKey2"));
        assertNull(parsed.get("testKey99"));

        assertNotNull(CollectionUtils.parseMetadata(null));
    }


}