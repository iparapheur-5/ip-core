/*
 * iparapheur Core
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.ipcore.utils;

import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.cert.jcajce.JcaX509CertificateHolder;
import org.junit.jupiter.api.Test;

import static coop.libriciel.ipcore.model.crypto.SignatureFormat.*;
import static coop.libriciel.ipcore.utils.CryptoUtils.EXAMPLE_PUBLIC_CERTIFICATE_BASE64;
import static org.junit.jupiter.api.Assertions.*;


class CryptoUtilsTest {


    @Test
    void parseX509Certificate() {

        JcaX509CertificateHolder jcaX509CertificateHolder = CryptoUtils.parseX509Certificate(EXAMPLE_PUBLIC_CERTIFICATE_BASE64);
        assertNotNull(jcaX509CertificateHolder);

        assertEquals(1689501897000L, jcaX509CertificateHolder.getNotAfter().getTime());
        assertEquals(1626429897000L, jcaX509CertificateHolder.getNotBefore().getTime());
    }


    @Test
    void getCnStringValue() {

        JcaX509CertificateHolder jcaX509CertificateHolder = CryptoUtils.parseX509Certificate(EXAMPLE_PUBLIC_CERTIFICATE_BASE64);
        assertNotNull(jcaX509CertificateHolder);

        X500Name issuerX500Name = jcaX509CertificateHolder.getIssuer();
        assertNotNull(issuerX500Name);
        assertEquals("AC_ADULLACT_Utilisateurs_G3", CryptoUtils.getName(issuerX500Name));

        X500Name subjectX500Name = jcaX509CertificateHolder.getSubject();
        assertNotNull(subjectX500Name);
        assertEquals("Test_Tilo", CryptoUtils.getName(subjectX500Name));
    }


    @Test
    void checkDetachedSignatures() {
        assertDoesNotThrow(() -> CryptoUtils.checkIfDetachedSignatureAllowed(PKCS7, true));

        assertDoesNotThrow(() -> CryptoUtils.checkIfDetachedSignatureAllowed(XADES_DETACHED, true));

        assertDoesNotThrow(() -> CryptoUtils.checkIfDetachedSignatureAllowed(PES_V2, false));

        assertThrows(
                RuntimeException.class,
                () -> CryptoUtils.checkIfDetachedSignatureAllowed(AUTO, true)
        );

        assertThrows(
                LocalizedStatusException.class,
                () -> CryptoUtils.checkIfDetachedSignatureAllowed(PES_V2, true)
        );

        assertThrows(
                LocalizedStatusException.class,
                () -> CryptoUtils.checkIfDetachedSignatureAllowed(PADES, true)
        );
    }

}
