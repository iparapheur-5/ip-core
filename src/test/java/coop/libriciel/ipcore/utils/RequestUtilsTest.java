/*
 * iparapheur Core
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.ipcore.utils;

import coop.libriciel.ipcore.model.auth.DeskRepresentation;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;


class RequestUtilsTest {


    @Test
    void hasChangesBetween() {
        DeskRepresentation o1 = new DeskRepresentation("id01", "same_name");
        DeskRepresentation o2 = new DeskRepresentation("id02", "same_name");
        assertTrue(RequestUtils.hasChangesBetween(o1, o2, DeskRepresentation::getId));
        assertFalse(RequestUtils.hasChangesBetween(o1, o2, DeskRepresentation::getName));
    }


}