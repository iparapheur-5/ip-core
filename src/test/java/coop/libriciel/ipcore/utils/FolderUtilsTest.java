/*
 * iparapheur Core
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.ipcore.utils;

import org.junit.Assert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.MissingResourceException;

import static coop.libriciel.ipcore.model.workflow.Task.ExternalState.*;


class FolderUtilsTest {


    @Test
    void testMapTaskStatusBadArgument() {
        Assert.assertThrows(NullPointerException.class, () -> FolderUtils.mapTaskStatus(null));
        Assert.assertThrows(MissingResourceException.class, () -> FolderUtils.mapTaskStatus("toto"));
    }


    @Test
    void testMapTaskStatusOK() {
        Assertions.assertEquals(ACTIVE, FolderUtils.mapTaskStatus("ACTIVE"));
        Assertions.assertEquals(SIGNED, FolderUtils.mapTaskStatus("SIGNED"));
        Assertions.assertEquals(REFUSED, FolderUtils.mapTaskStatus("REFUSED"));
        Assertions.assertEquals(EXPIRED, FolderUtils.mapTaskStatus("EXPIRED"));
        Assertions.assertEquals(CREATED, FolderUtils.mapTaskStatus("creation"));
        Assertions.assertEquals(IN_REDACTION, FolderUtils.mapTaskStatus("modification"));
        Assertions.assertEquals(DELETED, FolderUtils.mapTaskStatus("supression"));
        Assertions.assertEquals(SENT, FolderUtils.mapTaskStatus("envoi"));
        Assertions.assertEquals(SENT_AGAIN, FolderUtils.mapTaskStatus("renvoi"));
        Assertions.assertEquals(RECEIVED_PARTIALLY, FolderUtils.mapTaskStatus("reception-partielle"));
        Assertions.assertEquals(RECEIVED, FolderUtils.mapTaskStatus("reception"));
        Assertions.assertEquals(NOT_RECEIVED, FolderUtils.mapTaskStatus("non-recu"));
        Assertions.assertEquals(FORM, FolderUtils.mapTaskStatus("FORM"));
        Assertions.assertEquals(ERROR, FolderUtils.mapTaskStatus("ERROR"));
    }


//    @Test
//    void testSetStateLateAllValues() {
//        Date before = Date.from(Instant.now().minus(30, ChronoUnit.DAYS));
//        Date now = Date.from(Instant.now());
//        Date after = Date.from(Instant.now().plus(30, ChronoUnit.DAYS));
//
//        Assertions.assertEquals(PENDING, StateUtils.computeLateState(PENDING, null));
//        Assertions.assertEquals(LATE, StateUtils.computeLateState(PENDING, before));
//        Assertions.assertEquals(LATE, StateUtils.computeLateState(PENDING, now));
//        Assertions.assertEquals(PENDING, StateUtils.computeLateState(PENDING, after));
//
//        Assertions.assertEquals(CURRENT, StateUtils.computeLateState(CURRENT, null));
//        Assertions.assertEquals(LATE, StateUtils.computeLateState(CURRENT, before));
//        Assertions.assertEquals(LATE, StateUtils.computeLateState(CURRENT, now));
//        Assertions.assertEquals(CURRENT, StateUtils.computeLateState(CURRENT, after));
//    }


}
