/*
 * iparapheur Core
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.ipcore.utils;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.http.MediaType.*;


class FileUtilsTest {


    @Test
    void cleanFileSystemForbiddenCharacters() {
        assertEquals("test_test", FileUtils.cleanFileSystemForbiddenCharacters("test?test"));
        assertEquals("test_test", FileUtils.cleanFileSystemForbiddenCharacters("test|test"));
        assertEquals("test_test", FileUtils.cleanFileSystemForbiddenCharacters("test\\test"));
        assertEquals("test_test", FileUtils.cleanFileSystemForbiddenCharacters("test*test"));
        assertEquals("test_test", FileUtils.cleanFileSystemForbiddenCharacters("test/test"));
        assertEquals("test-test", FileUtils.cleanFileSystemForbiddenCharacters("test:test"));
        assertEquals("_test_.pdf", FileUtils.cleanFileSystemForbiddenCharacters("<test>.pdf"));
    }


    @Test
    void parseFileNameFromAttachmentTest() {
        assertEquals("test 1.pdf", FileUtils.parseFileNameFromAttachment("attachment; filename=\"test 1.pdf\"; filename*=UTF-8''..."));
        assertEquals("(2).pdf", FileUtils.parseFileNameFromAttachment("attachment; filename=\"(2).pdf\"; filename*=UTF-8''..."));
        assertNull(FileUtils.parseFileNameFromAttachment("not_a_real_header"));
        assertNull(FileUtils.parseFileNameFromAttachment(null));
    }


    @Test
    void isXml() {
        assertTrue(FileUtils.isXml(TEXT_XML));
        assertTrue(FileUtils.isXml(APPLICATION_XML));
        assertFalse(FileUtils.isXml(TEXT_PLAIN));
        assertFalse(FileUtils.isXml(null));
    }


}