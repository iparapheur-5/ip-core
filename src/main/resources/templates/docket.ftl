<!DOCTYPE html>

<!--
  iparapheur
  Copyright (C) 2018-2023 Libriciel. All rights reserved.

  This work is licensed under the terms of the MIT license.
  For a copy, see <https://opensource.org/licenses/MIT>.
-->

<#setting datetime_format="short">

<#-- @ftlvariable name="i_Parapheur_internal_folder" type="coop.libriciel.ipcore.model.workflow.Folder" -->
<#-- @ftlvariable name="i_Parapheur_internal_resource_bundle" type="java.util.ResourceBundle" -->
<#-- @ftlvariable name="i_Parapheur_internal_crypto_utils" type="coop.libriciel.ipcore.utils.CryptoUtils" -->

<html lang="fr">
<head>
    <meta http-equiv="content-type" content="text/html" charset="UTF-8"/>
    <title></title>
    <meta name="created" content="0;0"/>
    <meta name="changed" content="20080421;21344800"/>
    <style>
        @page {
            size: 21cm 29.7cm
        }

        * {
            font-family: Helvetica, Verdana, Arial, sans-serif;
            color: #262626;
        }

        div {
            display: inline-block;
        }

        .title {
            width: 100%;
        }

        h1 {
            text-align: center;
        }

        h2 {
            text-align: center;
            font-weight: 400;
        }

        table {
            width: 100%;
            border-collapse: collapse;
        }

        table.table td {
            padding: 10px 10px 10px 15px;
            height: 30px;
            border: 1px solid #dcdcdc;
            font-size: 16px;
        }

        p {
            font-size: 12px;
            font-weight: 500;
        }

        .table-head {
            background-color: #61c6ea;
            font-weight: bold;
            letter-spacing: 1px;
        }

        .focus {
            font-weight: bold;
        }
    </style>

</head>
<body lang="fr-FR" dir="ltr">

<div class="title">
    <h1>Bordereau</h1>
    <h2>${i_Parapheur_internal_folder.getName()}</h2>
</div>

<table class="table">
    <tr class="table-head">
        <td>Signataire</td>
        <td>Date</td>
        <td>Annotation</td>
    </tr>
    <#list i_Parapheur_internal_folder.getStepList() as task>
        <tr>
            <td>
                <#if task.getUser()??>
                    <#if task.getUser().getFirstName()?has_content>${task.getUser().getFirstName()}</#if>
                    <#if task.getUser().getLastName()?has_content>${task.getUser().getLastName()},</#if>
                </#if>
                <#list task.getDesks() as desk>
                    <#if desk.getName()?has_content>${desk.getName()}</#if>
                </#list>
                <#if task.getDelegatedByDesk()?? && task.getDelegatedByDesk().getName()?has_content>
                    en l'absence de ${task.getDelegatedByDesk().getName()}
                </#if>
            </td>
            <#if task.getDate()??>
                <td>${task.getDate()?datetime}</td>
            <#else>
                <td class="table-head"></td>
            </#if>
            <td>
                <div class="focus">
                    <#if task.getAction()??>
                        Action : ${i_Parapheur_internal_resource_bundle.getString(task.getAction().getMessageKey())}
                        <#if task.getState().toString() == "REJECTED" && task.getAction()??>
                            <br/>
                            <span style="color: #d9534f">Dossier Rejeté</span>
                        </#if>
                    </#if>
                </div>
                <#if task.getPublicAnnotation()?? && task.getPublicAnnotation()?has_content>
                    <p>${task.getPublicAnnotation()?replace('\n','<br/>')}</p>
                </#if>
                <#if ((task.getAction().toString() == "SEAL") || (task.getAction().toString() == "SIGNATURE")) && (task.getPublicCertificateBase64()??)>
                    <div>
                        <#--noinspection CheckImageSize-->
                        <img src="/static/docket-certificate.png" alt="certificat" width="32" height="32"/>
                        <#assign x509_certificate_holder=i_Parapheur_internal_crypto_utils.parseX509Certificate(task.getPublicCertificateBase64())/>
                        <#if x509_certificate_holder??>
                            <#assign x500_subject=x509_certificate_holder.getSubject()/>
                            <#assign cert_organization=i_Parapheur_internal_crypto_utils.getOrganization(x500_subject)/>
                            <#assign cert_subject=i_Parapheur_internal_crypto_utils.getTitle(x500_subject)/>
                            <#assign cert_name=i_Parapheur_internal_crypto_utils.getName(x500_subject)/>
                            Certificat au nom de <u>${cert_name}</u>
                            <#if cert_organization?has_content && cert_organization!="_inconnu_">
                                (<#if cert_subject?has_content && cert_subject!="_inconnu_"><u>${cert_subject}</u>,
                            </#if><u>${cert_organization}</u>)</#if>,
                            &eacute;mis par <u>${i_Parapheur_internal_crypto_utils.getName(x509_certificate_holder.getIssuer())}</u>,
                            valide du ${x509_certificate_holder.getNotBefore()?string("dd/MM/yyyy HH:mm:ss")} au ${x509_certificate_holder.getNotAfter()?string("dd/MM/yyyy HH:mm:ss")}.
                            <br/>
                        </#if>
                    </div>
                </#if>
            </td>
        </tr>
    </#list>
</table>


<p class="mentions">
    Dossier de type
    : <#if i_Parapheur_internal_folder.getType()?has_content && i_Parapheur_internal_folder.getType().getName()?has_content>${i_Parapheur_internal_folder.getType().getName()}</#if><#if !i_Parapheur_internal_folder.getType()?has_content || !i_Parapheur_internal_folder.getType().getName()?has_content>${i_Parapheur_internal_resource_bundle.getString("message.deleted_type")}</#if>
    // <#if i_Parapheur_internal_folder.getSubtype()?has_content && i_Parapheur_internal_folder.getSubtype().getName()?has_content>${i_Parapheur_internal_folder.getSubtype().getName()}</#if><#if !i_Parapheur_internal_folder.getSubtype()?has_content || !i_Parapheur_internal_folder.getSubtype().getName()?has_content>${i_Parapheur_internal_resource_bundle.getString("message.deleted_subtype")}</#if>
</p>

<#assign metadataMap = i_Parapheur_internal_folder.getMetadata()>
<#if metadataMap?has_content><br>
    <table>
        <tr>
            <td>
                Propri&eacute;t&eacute;s sp&eacute;cifiques :
            </td>
            <td>
                <ul>
                    <#list metadataMap?keys as key>
                        <li>${key} : ${metadataMap[key]}</li>
                    </#list>
                </ul>
            </td>
        </tr>
    </table>
</#if>

</body>
</html>
