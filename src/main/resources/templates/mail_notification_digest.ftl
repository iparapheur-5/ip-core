<!DOCTYPE html>

<!--
  iparapheur
  Copyright (C) 2018-2023 Libriciel. All rights reserved.

  This work is licensed under the terms of the MIT license.
  For a copy, see <https://opensource.org/licenses/MIT>.
-->

<#setting datetime_format="short">

<#-- @ftlvariable name="i_Parapheur_internal_task_list" type="java.util.List<coop.libriciel.ipcore.model.mail.MailContent>" -->
<#-- @ftlvariable name="i_Parapheur_internal_main_url" type="java.lang.String" -->
<#-- @ftlvariable name="i_Parapheur_internal_footer" type="java.lang.String" -->

<html lang="fr">

<head>
    <title>e-mail r&eacute;capitulatif</title>
    <style>

        body {
            margin: 0;
            display: block;
            position: relative;
            font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
            font-size: 14px;
            line-height: 1.42857143;
            color: #333;
            background-color: #fff;
            word-wrap: break-word;
        }

        h1, h2, h3 {
            margin-top: 20px;
            margin-bottom: 10px;
            font-family: inherit;
            font-weight: 500;
            line-height: 1.1;
            color: inherit;
        }

        h1 {
            font-size: 20px;
        }

        h2 {
            font-size: 18px;
        }

        h1 small, h2 small, h3 small {
            font-size: 65%;
            font-weight: 400;
            line-height: 1;
            color: #999;
        }

        div {
            display: block;
        }

        p {
            margin: 0 0 10px;
        }

        a {
            text-decoration: none;
            color: black;
        }

        .container {
            width: 920px;
            margin-right: auto;
            margin-left: auto;
            padding-left: 15px;
            padding-right: 15px;
        }

        .bg-primary {
            background-color: #428bca;
        }

        .table {
            width: 100%;
            margin-bottom: 20px;
            border-collapse: collapse;
            border-spacing: 0;
            display: table;
        }

        .table-bordered {
            border: 1px solid #ddd;
        }

        .table-striped > tbody > tr:nth-child(odd) > td, .table-striped > tbody > tr:nth-child(odd) > th {
            background-color: #f9f9f9;
        }

    </style>
</head>

<body>

<div class="container">

    <h1 class="bg-primary" style="color:white; padding-left:15px">
        iparapheur | e-mail r&eacute;capitulatif
    </h1>

    <table class="table table-bordered table-striped">
        <thead>
        <tr style="text-align:left;">
            <th>Nom</th>
            <th>Lien</th>
            <th>Date</th>
            <th>Acteur</th>
            <th>Action</th>
            <th>Annotation</th>
        </tr>
        </thead>

        <tbody>
        <#list i_Parapheur_internal_task_list as i_Parapheur_internal_task>
            <tr>
                <td>${i_Parapheur_internal_task.folderName ! ""}</td>
                <td>
                    <#if i_Parapheur_internal_task.tenantId?? && i_Parapheur_internal_task.deskId?? && i_Parapheur_internal_task.folderId??>
                        <a href="${i_Parapheur_internal_main_url}/tenant/${i_Parapheur_internal_task.tenantId}/desk/${i_Parapheur_internal_task.deskId}/folder/${i_Parapheur_internal_task.folderId}%3FasDeskId%3D${i_Parapheur_internal_task.deskId}"
                           target="_blank">
                            Acc&eacute;der au dossier
                        </a>
                    </#if>
                </td>
                <td>
                    <#if i_Parapheur_internal_task.date??>
                        ${i_Parapheur_internal_task.date?datetime ! ""}
                    </#if>
                </td>
                <td>
                    <#if i_Parapheur_internal_task.deskId?? && i_Parapheur_internal_task.username??>
                        ${i_Parapheur_internal_task.deskId ! ""} ${i_Parapheur_internal_task.username ! ""}
                    </#if>
                </td>
                <td>${i_Parapheur_internal_task.taskActionString ! ""}</td>
                <td>${i_Parapheur_internal_task.publicAnnotation ! ""}</td>
            </tr>
        </#list>
        </tbody>
    </table>

</div>

<p><small>${i_Parapheur_internal_footer}</small></p>

</body>

</html>
