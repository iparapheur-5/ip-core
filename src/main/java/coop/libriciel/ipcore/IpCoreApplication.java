/*
 * iparapheur Core
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.ipcore;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;


@EnableScheduling
@EnableJpaRepositories
@SpringBootApplication
@ConfigurationPropertiesScan({"coop.libriciel.ipcore.services", "coop.libriciel.ipcore.business", "coop.libriciel.ipcore.configuration"})
public class IpCoreApplication {


    public static final String API_V1 = "v1";
    public static final String API_INTERNAL = "internal";
    public static final String API_PROVISIONING_V1 = "provisioning/v1";
    public static final boolean HIDE_UNSTABLE_API = true;


    public static void main(String[] args) {
        // This is a necessary workaround to avoid a deadlock in the reactive stack,
        // when the number of CPU is low (eg 4). There is probably a problem on our side
        // that is triggering this deadlock, we will investigate it,
        // but raising the number of worker threads also prevents the problem.
        System.setProperty("reactor.netty.ioWorkerCount", "16");
        SpringApplication.run(IpCoreApplication.class, args);
    }


}
