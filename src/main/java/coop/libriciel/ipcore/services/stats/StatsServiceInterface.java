/*
 * iparapheur Core
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package coop.libriciel.ipcore.services.stats;

import coop.libriciel.ipcore.model.auth.Desk;
import coop.libriciel.ipcore.model.auth.DeskRepresentation;
import coop.libriciel.ipcore.model.database.Tenant;
import coop.libriciel.ipcore.model.stats.StatsCategory;
import coop.libriciel.ipcore.model.workflow.Action;
import coop.libriciel.ipcore.model.workflow.Folder;
import coop.libriciel.ipcore.model.workflow.Task;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Date;
import java.util.Optional;

import static java.util.concurrent.TimeUnit.MILLISECONDS;


public interface StatsServiceInterface {

    String BEAN_NAME = "statsService";
    String PREFERENCES_PREFIX = "services.stats";
    String PREFERENCES_PROVIDER_KEY = PREFERENCES_PREFIX + ".provider";


    enum GraphType {LINES, VERTICAL_BARS}


    enum GraphPeriod {DAY, WEEK, MONTH, YEAR}


    default @Nullable Long computeTimeToCompleteInHours(@Nullable Task task) {

        return Optional.ofNullable(task)
                .map(Task::getBeginDate)
                .map(beginDate -> MILLISECONDS.toHours(new Date().getTime() - beginDate.getTime()))
                .orElse(null);
    }


    // <editor-fold desc="Administration">


    String createTenant(@NotNull Tenant tenant);


    void renameTenant(@NotNull Tenant tenant);


    void deleteTenant(@NotNull Tenant tenant);


    // </editor-fold desc="Administration">


    // <editor-fold desc="Registering actions">


    default void registerFolderAction(@NotNull Tenant tenant, @NotNull Action action, @NotNull Folder folder, @NotNull Desk desk,
                                      @Nullable Long timeToCompleteInHours) {

        DeskRepresentation dummyEntity = new DeskRepresentation(desk.getId(), desk.getName());
        registerFolderAction(tenant, action, folder, dummyEntity, timeToCompleteInHours);
    }


    void registerFolderAction(@NotNull Tenant tenant, @NotNull Action action, @NotNull Folder folder, @NotNull DeskRepresentation deskEntity,
                              @Nullable Long timeToCompleteInHours);


    void registerAdminAction(@Nullable Tenant tenant, @NotNull StatsCategory category, @NotNull Action action, @NotNull String target);


    // </editor-fold desc="Registering actions">


    // <editor-fold desc="Charts">


    String getCount(@NotNull Tenant tenant, @NotNull Action action);


    String getCount(@NotNull Tenant tenant, @NotNull String desk);


    String getActionChartUrl(@NotNull Tenant tenant, @NotNull String deskId, @NotNull String startDate, @NotNull String endDate, int width, int height,
                             @NotNull GraphType graphType, @NotNull GraphPeriod periodicity);


    // </editor-fold desc="Charts">


}
