/*
 * iparapheur Core
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package coop.libriciel.ipcore.services.stats;

import coop.libriciel.ipcore.model.auth.DeskRepresentation;
import coop.libriciel.ipcore.model.database.Tenant;
import coop.libriciel.ipcore.model.stats.StatsCategory;
import coop.libriciel.ipcore.model.stats.matomo.CreateSiteResult;
import coop.libriciel.ipcore.model.workflow.Action;
import coop.libriciel.ipcore.model.workflow.Folder;
import coop.libriciel.ipcore.utils.ForcedExceptionsConnector;
import coop.libriciel.ipcore.utils.KeycloakSecurityUtils;
import coop.libriciel.ipcore.utils.LocalizedStatusException;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.text.MessageFormat;
import java.util.*;
import java.util.stream.Stream;

import static coop.libriciel.ipcore.model.stats.StatsCategory.FOLDER;
import static coop.libriciel.ipcore.utils.RequestUtils.SUBSERVICE_REQUEST_SHORT_TIMEOUT;
import static coop.libriciel.ipcore.utils.TextUtils.MESSAGE_BUNDLE;
import static java.util.Locale.ROOT;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;


@Log4j2
@Service(StatsServiceInterface.BEAN_NAME)
@ConditionalOnProperty(name = StatsServiceInterface.PREFERENCES_PROVIDER_KEY, havingValue = "internal")
public class InternalMatomoService implements StatsServiceInterface {

    private static final String REPORTING_PATH = "matomo.php";
    private static final String ADMIN_OPERATIONS_PATH = "index.php";


    // <editor-fold desc="Beans">


    private final StatsServiceProperties properties;


    public InternalMatomoService(StatsServiceProperties properties) {
        this.properties = properties;
    }


    // </editor-fold desc="Beans">


    // <editor-fold desc="Administration">


    @Override
    public String createTenant(@NotNull Tenant tenant) {

        String tenantNameWrapper = ResourceBundle
                .getBundle(MESSAGE_BUNDLE, Locale.getDefault())
                .getString("message.stats_tenant_name");

        String tenantName = MessageFormat.format(tenantNameWrapper, tenant.getName());

        URI requestUri = UriComponentsBuilder.newInstance()
                .scheme(properties.getScheme()).host(properties.getUrl()).path(ADMIN_OPERATIONS_PATH)
                .queryParam("module", "API")
                .queryParam("method", "SitesManager.addSite")
                .queryParam("format", "JSON")
                .queryParam("token_auth", properties.getToken())
                .queryParam("siteName", tenantName)
                .build().normalize().toUri();

        String statsId = WebClient.builder().clientConnector(new ForcedExceptionsConnector()).build()
                .post().uri(requestUri)
                .retrieve()
                .bodyToMono(CreateSiteResult.class)
                .blockOptional(SUBSERVICE_REQUEST_SHORT_TIMEOUT)
                .map(CreateSiteResult::getValue)
                .map(String::valueOf)
                .orElseThrow(() -> new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.cannot_parse_stats_service_response"));

        // Sending back result

        log.debug("Matomo created site id:{}", statsId);
        return statsId;
    }


    @Override
    public void renameTenant(@NotNull Tenant tenant) {
        log.debug("Matomo renaming site id:{}", tenant.getStatsId());

        if (StringUtils.isEmpty(tenant.getStatsId())) {return;}

        String tenantNameWrapper = ResourceBundle
                .getBundle(MESSAGE_BUNDLE, Locale.getDefault())
                .getString("message.stats_tenant_name");

        String tenantName = MessageFormat.format(tenantNameWrapper, tenant.getName());

        URI requestUri = UriComponentsBuilder.newInstance()
                .scheme(properties.getScheme()).host(properties.getUrl()).path(ADMIN_OPERATIONS_PATH)
                .queryParam("module", "API")
                .queryParam("method", "SitesManager.updateSite")
                .queryParam("format", "JSON")
                .queryParam("token_auth", properties.getToken())
                .queryParam("idSite", tenant.getStatsId())
                .queryParam("siteName", tenantName)
                .build().normalize().toUri();

        sendRequest(requestUri);
    }


    @Override
    public void deleteTenant(@NotNull Tenant tenant) {
        log.debug("Matomo deleting site id:{}", tenant.getStatsId());

        if (StringUtils.isEmpty(tenant.getStatsId())) {return;}

        URI requestUri = UriComponentsBuilder.newInstance()
                .scheme(properties.getScheme()).host(properties.getUrl()).path(ADMIN_OPERATIONS_PATH)
                .queryParam("module", "API")
                .queryParam("method", "SitesManager.deleteSite")
                .queryParam("format", "JSON")
                .queryParam("token_auth", properties.getToken())
                .queryParam("idSite", tenant.getStatsId())
                .build().normalize().toUri();

        sendRequest(requestUri);
    }


    // </editor-fold desc="Administration">


    // <editor-fold desc="Registering actions">


    @Override
    public void registerFolderAction(@NotNull Tenant tenant, @NotNull Action action, @NotNull Folder folder, @NotNull DeskRepresentation deskEntity,
                                     @Nullable Long timeToCompleteInHours) {

        registerFolderAction(String.valueOf(properties.getGlobalSiteId()), tenant.getId(), FOLDER.name(), action.name(), folder, timeToCompleteInHours);

        if (StringUtils.isNotEmpty(tenant.getStatsId())) {
            registerFolderAction(tenant.getStatsId(), deskEntity.getId(), FOLDER.name(), action.name(), folder, timeToCompleteInHours);
        }
    }


    private void registerFolderAction(@NotNull String siteId, @NotNull String uid, @NotNull String categoryName, @NotNull String actionName,
                                      @NotNull Folder folder, @Nullable Long value) {

        UriComponentsBuilder builder = UriComponentsBuilder.newInstance()
                .scheme(properties.getScheme()).host(properties.getUrl()).path(REPORTING_PATH)
                .queryParam("idsite", siteId)
                .queryParam("apiv", 1)
                .queryParam("rec", 1)
                .queryParam("e_c", categoryName)
                .queryParam("e_a", actionName)
                .queryParam("e_n", folder.getId())
                .queryParam("uid", uid)
                .queryParam("rand", new Random().nextInt(1000000));
        Optional.ofNullable(value).ifPresent(v -> builder.queryParam("e_v", v));

        URI requestUri = builder.build().normalize().toUri();
        log.debug("Matomo registered action : {}", requestUri);
        sendRequest(requestUri);
    }


    @Override
    public void registerAdminAction(@Nullable Tenant tenant, @NotNull StatsCategory category, @NotNull Action action, @NotNull String target) {

        Integer tenantSiteId = Optional.ofNullable(tenant)
                .map(Tenant::getStatsId)
                .filter(NumberUtils::isCreatable)
                .map(NumberUtils::createInteger)
                .orElse(null);

        String currentUserId = KeycloakSecurityUtils.getCurrentSessionUserId();
        String categoryName = ResourceBundle.getBundle(MESSAGE_BUNDLE, Locale.getDefault()).getString(category.getMessageKey());
        String actionName = ResourceBundle.getBundle(MESSAGE_BUNDLE, Locale.getDefault()).getString(action.getMessageKey());

        Stream.of(tenantSiteId, properties.getGlobalSiteId())
                // Filtering out un-parameterized sites
                .distinct()
                .filter(Objects::nonNull)
                // Actual request(s)
                .forEach(siteId -> sendRequest(UriComponentsBuilder.newInstance()
                        .scheme(properties.getScheme()).host(properties.getUrl()).path(REPORTING_PATH)
                        .queryParam("idsite", siteId)
                        .queryParam("apiv", 1)
                        .queryParam("rec", 1)
                        .queryParam("e_c", categoryName)
                        .queryParam("e_a", actionName)
                        .queryParam("e_n", target)
                        .queryParam("uid", currentUserId)
                        .queryParam("rand", new Random().nextInt(1000000))
                        .build().normalize().toUri())
                );
    }


    // </editor-fold desc="Registering actions">


    private void sendRequest(URI requestUri) {
        WebClient.builder().clientConnector(new ForcedExceptionsConnector()).build()
                .post().uri(requestUri)
                .exchange()
                .blockOptional(SUBSERVICE_REQUEST_SHORT_TIMEOUT)
                .map(ClientResponse::statusCode)
                .filter(HttpStatus::is2xxSuccessful)
                .orElseGet(() -> {
                    log.warn(ResourceBundle.getBundle(MESSAGE_BUNDLE, ROOT).getString("message.cannot_reach_stats_service"));
                    return null;
                });
    }


    // <editor-fold desc="Charts">


    @Override
    public String getCount(@NotNull Tenant tenant, @NotNull Action action) {

        if (StringUtils.isEmpty(tenant.getStatsId())) {return null;}

        URI requestUri = UriComponentsBuilder.newInstance()
                .scheme(properties.getScheme()).host(properties.getUrl())
                .queryParam("module", "API")
                .queryParam("method", "Events.getAction")
                .queryParam("format", "JSON")
                .queryParam("idSite", tenant.getStatsId())
                .queryParam("token_auth", properties.getToken())
                .queryParam("period", "month")
                .queryParam("date", "today")
                .build().normalize().toUri();

        log.debug("Matomo request : {}", requestUri);

        return WebClient.builder().clientConnector(new ForcedExceptionsConnector()).build()
                .get().uri(requestUri)
                .retrieve()
                .bodyToMono(String.class)
                .block(SUBSERVICE_REQUEST_SHORT_TIMEOUT);
    }


    @Override
    public String getCount(@NotNull Tenant tenant, @NotNull String desk) {

        if (StringUtils.isEmpty(tenant.getStatsId())) {return null;}

        URI requestUri = UriComponentsBuilder.newInstance()
                .scheme(properties.getScheme()).host(properties.getUrl())
                .queryParam("module", "API")
                .queryParam("method", "Events.getActionFromCategoryId")
                .queryParam("format", "JSON")
                .queryParam("idSite", tenant.getStatsId())
                .queryParam("token_auth", properties.getToken())
                .queryParam("idSubtable", 2)
                .queryParam("period", "month")
                .queryParam("date", "today")
                .build().normalize().toUri();

        log.debug("Matomo request : {}", requestUri);

        return WebClient.builder().clientConnector(new ForcedExceptionsConnector()).build()
                .get().uri(requestUri)
                .retrieve()
                .bodyToMono(String.class)
                .block(SUBSERVICE_REQUEST_SHORT_TIMEOUT);
    }


    @Override
    public String getActionChartUrl(@NotNull Tenant tenant, @NotNull String deskId, @NotNull String startDate, @NotNull String endDate, int width, int height,
                                    @NotNull GraphType graphType, @NotNull GraphPeriod periodicity) {

        if (StringUtils.isEmpty(tenant.getStatsId())) {return null;}
        Action action = Action.VISA;

        // http://iparapheur.dom.local/matomo/?module=API&apiModule=Events&method=ImageGraph.get&idSite=2&token_auth=token
        // &apiAction=getAction&graphType=evolution&period=day&date=last30&width=1080&height=720&segment=userId==f383fd2d-7658-4175-a367-f62814ab93fb

        String periodString = switch (periodicity) {
            case YEAR -> "year";
            case MONTH -> "month";
            case WEEK -> "week";
            default -> "day";
        };

        String graphTypeString = switch (graphType) {
            case VERTICAL_BARS -> "verticalBar";
            default -> "evolution";
        };

        return UriComponentsBuilder.newInstance()
                .scheme(properties.getScheme()).host(properties.getUrl())
                .queryParam("module", "API")
                .queryParam("apiModule", "Events")
                .queryParam("apiAction", "getAction")
                .queryParam("method", "ImageGraph.get")
                .queryParam("idSite", tenant.getStatsId())
                .queryParam("token_auth", properties.getToken())
                .queryParam("graphType", graphTypeString)
                .queryParam("period", periodString)
                .queryParam("date", String.format("%s,%s", startDate, endDate))
                .queryParam("width", width)
                .queryParam("height", height)
                .queryParam("segment", String.format("userId==%s", deskId))
                .build().toUri().toString();
    }


    // </editor-fold desc="Charts">


}
