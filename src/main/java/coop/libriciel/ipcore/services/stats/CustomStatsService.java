/*
 * iparapheur Core
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package coop.libriciel.ipcore.services.stats;

import coop.libriciel.ipcore.model.auth.DeskRepresentation;
import coop.libriciel.ipcore.model.database.Tenant;
import coop.libriciel.ipcore.model.stats.StatsCategory;
import coop.libriciel.ipcore.model.workflow.Action;
import coop.libriciel.ipcore.model.workflow.Folder;
import coop.libriciel.ipcore.utils.ForcedExceptionsConnector;
import lombok.Data;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.text.StringSubstitutor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.*;

import static coop.libriciel.ipcore.model.stats.StatsCategory.FOLDER;
import static coop.libriciel.ipcore.utils.RequestUtils.SUBSERVICE_REQUEST_SHORT_TIMEOUT;
import static coop.libriciel.ipcore.utils.TextUtils.INTERNAL_PREFIX;
import static coop.libriciel.ipcore.utils.TextUtils.MESSAGE_BUNDLE;
import static java.util.Comparator.naturalOrder;
import static java.util.Comparator.nullsFirst;
import static java.util.Locale.ROOT;
import static java.util.Map.Entry.comparingByKey;
import static javax.ws.rs.HttpMethod.POST;
import static javax.ws.rs.HttpMethod.PUT;


@Data
@Log4j2
@Service(StatsServiceInterface.BEAN_NAME)
@ConditionalOnProperty(name = StatsServiceInterface.PREFERENCES_PROVIDER_KEY, havingValue = "custom")
public class CustomStatsService implements StatsServiceInterface {

    // Spring/SpringBoot doesn't allow to escaping the "$" sign in the `application.properties`.
    // Ugly hacks do exists, but it feels more elegant to simply override the default character.
    private static final String PLACEHOLDER_PREFIX = "@{";
    private static final String PLACEHOLDER_SUFFIX = "}";

    private static final String PLACEHOLDER_RANDOM_NUMBER = INTERNAL_PREFIX + "random_number";
    private static final String PLACEHOLDER_CATEGORY = INTERNAL_PREFIX + "category";
    private static final String PLACEHOLDER_ACTION_NAME = INTERNAL_PREFIX + "action_name";
    private static final String PLACEHOLDER_TARGET = INTERNAL_PREFIX + "target";
    private static final String PLACEHOLDER_TIME_TO_COMPLETE_IN_HOURS = INTERNAL_PREFIX + "time_to_complete_in_hours";
    private static final String PLACEHOLDER_TENANT_ID = INTERNAL_PREFIX + "tenant_id";
    private static final String PLACEHOLDER_TENANT_NAME = INTERNAL_PREFIX + "tenant_name";
    private static final String PLACEHOLDER_DESK_ID = INTERNAL_PREFIX + "desk_id";
    private static final String PLACEHOLDER_DESK_NAME = INTERNAL_PREFIX + "desk_name";
    private static final String PLACEHOLDER_FOLDER_ID = INTERNAL_PREFIX + "folder_id";
    private static final String PLACEHOLDER_FOLDER_NAME = INTERNAL_PREFIX + "folder_name";
    private static final String PLACEHOLDER_TYPE_ID = INTERNAL_PREFIX + "type_id";
    private static final String PLACEHOLDER_TYPE_NAME = INTERNAL_PREFIX + "type_name";
    private static final String PLACEHOLDER_SUBTYPE_ID = INTERNAL_PREFIX + "subtype_id";
    private static final String PLACEHOLDER_SUBTYPE_NAME = INTERNAL_PREFIX + "subtype_name";


    // <editor-fold desc="Beans">


    private final StatsServiceProperties properties;


    public CustomStatsService(StatsServiceProperties properties) {
        this.properties = properties;
    }


    // </editor-fold desc="Beans">


    // <editor-fold desc="Administration">


    @Override
    public String createTenant(@NotNull Tenant tenant) {return null;}


    @Override
    public void renameTenant(@NotNull Tenant tenant) {}


    @Override
    public void deleteTenant(@NotNull Tenant tenant) {}


    // </editor-fold desc="Administration">


    // <editor-fold desc="Registering actions">


    @Override
    public void registerFolderAction(@NotNull Tenant tenant, @NotNull Action action, @NotNull Folder folder, @NotNull DeskRepresentation deskEntity,
                                     @Nullable Long timeToCompleteInHours) {

        String folderCategoryName = ResourceBundle.getBundle(MESSAGE_BUNDLE, Locale.getDefault()).getString(FOLDER.getMessageKey());
        String actionName = ResourceBundle.getBundle(MESSAGE_BUNDLE, Locale.getDefault()).getString(action.getMessageKey());

        // Building the substitutes map

        Map<String, String> placeholderSubstitutes = new HashMap<>(folder.getMetadata());

        placeholderSubstitutes.put(PLACEHOLDER_RANDOM_NUMBER, String.valueOf(new Random().nextInt(1000000)));
        placeholderSubstitutes.put(PLACEHOLDER_CATEGORY, folderCategoryName);
        placeholderSubstitutes.put(PLACEHOLDER_ACTION_NAME, actionName);
        placeholderSubstitutes.put(PLACEHOLDER_TARGET, folder.getId());
        placeholderSubstitutes.put(PLACEHOLDER_TENANT_ID, tenant.getId());
        placeholderSubstitutes.put(PLACEHOLDER_TENANT_NAME, tenant.getName());

        placeholderSubstitutes.put(PLACEHOLDER_TIME_TO_COMPLETE_IN_HOURS, Optional.ofNullable(timeToCompleteInHours).map(String::valueOf).orElse(null));
        placeholderSubstitutes.put(PLACEHOLDER_DESK_ID, deskEntity.getId());
        placeholderSubstitutes.put(PLACEHOLDER_DESK_NAME, deskEntity.getName());
        placeholderSubstitutes.put(PLACEHOLDER_FOLDER_ID, folder.getId());
        placeholderSubstitutes.put(PLACEHOLDER_FOLDER_NAME, folder.getName());
        placeholderSubstitutes.put(PLACEHOLDER_TYPE_ID, folder.getId());
        placeholderSubstitutes.put(PLACEHOLDER_TYPE_NAME, folder.getName());
        placeholderSubstitutes.put(PLACEHOLDER_SUBTYPE_ID, deskEntity.getId());
        placeholderSubstitutes.put(PLACEHOLDER_SUBTYPE_NAME, deskEntity.getName());

        placeholderSubstitutes.entrySet()
                .stream()
                .filter(entry -> properties.getValuesSubstitutions().containsKey(entry.getValue()))
                .forEach(entry -> placeholderSubstitutes.put(entry.getKey(), properties.getValuesSubstitutions().get(entry.getValue())));

        StringSubstitutor substitutor = new StringSubstitutor(placeholderSubstitutes, PLACEHOLDER_PREFIX, PLACEHOLDER_SUFFIX);

        // Building the URL

        UriComponentsBuilder requestUriBuilder = UriComponentsBuilder.newInstance()
                .scheme(properties.getScheme())
                .host(properties.getUrl())
                .port(properties.getPort())
                .path(properties.getFolderAction().getPath());

        properties.getFolderAction()
                .getQueryParams()
                .entrySet()
                .stream()
                .sorted(comparingByKey(nullsFirst(naturalOrder())))  // Sorting alphabetically to ease unit-tests
                .forEach(entry -> requestUriBuilder.queryParam(
                        substitutor.replace(entry.getKey()),
                        substitutor.replace(entry.getValue())
                ));

        // Sending the result

        sendRequest(requestUriBuilder.build().normalize().toUri());
    }


    @Override
    public void registerAdminAction(@Nullable Tenant tenant, @NotNull StatsCategory category, @NotNull Action action, @NotNull String target) {

        // Building the substitutes map

        Map<String, String> placeholderSubstitutes = new HashMap<>();
        placeholderSubstitutes.put(PLACEHOLDER_RANDOM_NUMBER, String.valueOf(new Random().nextInt(1000000)));
        placeholderSubstitutes.put(PLACEHOLDER_CATEGORY, category.name());
        placeholderSubstitutes.put(PLACEHOLDER_ACTION_NAME, action.name());
        placeholderSubstitutes.put(PLACEHOLDER_TARGET, target);

        if (tenant != null) {
            placeholderSubstitutes.put(PLACEHOLDER_TENANT_ID, tenant.getId());
            placeholderSubstitutes.put(PLACEHOLDER_TENANT_NAME, tenant.getName());
        }

        placeholderSubstitutes.entrySet()
                .stream()
                .filter(entry -> properties.getValuesSubstitutions().containsKey(entry.getValue()))
                .forEach(entry -> placeholderSubstitutes.put(entry.getKey(), properties.getValuesSubstitutions().get(entry.getValue())));

        StringSubstitutor substitutor = new StringSubstitutor(placeholderSubstitutes, PLACEHOLDER_PREFIX, PLACEHOLDER_SUFFIX);

        // Building the URL

        UriComponentsBuilder requestUriBuilder = UriComponentsBuilder.newInstance()
                .scheme(properties.getScheme())
                .host(properties.getUrl())
                .port(properties.getPort())
                .path(properties.getAdminAction().getPath());

        properties.getAdminAction()
                .getQueryParams()
                .entrySet()
                .stream()
                .sorted(comparingByKey(nullsFirst(naturalOrder())))  // Sorting alphabetically to ease unit-tests
                .forEach(entry -> requestUriBuilder.queryParam(
                        substitutor.replace(entry.getKey()),
                        substitutor.replace(entry.getValue())
                ));

        // Sending the result

        sendRequest(requestUriBuilder.build().normalize().toUri());
    }


    private void sendRequest(URI requestUri) {
        log.debug("Custom stat event, action:{} on URI:{}", properties.getHttpMethod(), requestUri);

        WebClient webClient = WebClient.builder().clientConnector(new ForcedExceptionsConnector()).build();
        WebClient.RequestHeadersUriSpec<?> request = switch (properties.getHttpMethod().toUpperCase(ROOT)) {
            case POST -> webClient.post();
            case PUT -> webClient.put();
            default -> webClient.get();
        };

        request.uri(requestUri)
                .exchange()
                .blockOptional(SUBSERVICE_REQUEST_SHORT_TIMEOUT)
                .map(ClientResponse::statusCode)
                .filter(HttpStatus::is2xxSuccessful)
                .orElseGet(() -> {
                    log.warn(ResourceBundle.getBundle(MESSAGE_BUNDLE, ROOT).getString("message.cannot_reach_stats_service"));
                    return null;
                });
    }


    // </editor-fold desc="Registering actions">


    // <editor-fold desc="Charts">


    @Override
    public String getCount(@NotNull Tenant tenant, @NotNull Action action) {
        return null;
    }


    @Override
    public String getCount(@NotNull Tenant tenant, @NotNull String desk) {
        return null;
    }


    @Override
    public String getActionChartUrl(@NotNull Tenant tenant, @NotNull String deskId, @NotNull String startDate, @NotNull String endDate, int width, int height,
                                    @NotNull GraphType graphType, @NotNull GraphPeriod periodicity) {
        return null;
    }


    // </editor-fold desc="Charts">


}
