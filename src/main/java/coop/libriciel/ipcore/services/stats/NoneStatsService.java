/*
 * iparapheur Core
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package coop.libriciel.ipcore.services.stats;

import coop.libriciel.ipcore.model.auth.DeskRepresentation;
import coop.libriciel.ipcore.model.database.Tenant;
import coop.libriciel.ipcore.model.stats.StatsCategory;
import coop.libriciel.ipcore.model.workflow.Action;
import coop.libriciel.ipcore.model.workflow.Folder;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import java.util.UUID;


@Service(StatsServiceInterface.BEAN_NAME)
@ConditionalOnProperty(name = StatsServiceInterface.PREFERENCES_PROVIDER_KEY, havingValue = "none")
public class NoneStatsService implements StatsServiceInterface {


    // <editor-fold desc="Administration">


    @Override
    public String createTenant(@NotNull Tenant tenant) {return UUID.randomUUID().toString();}


    @Override
    public void renameTenant(@NotNull Tenant tenant) {}


    @Override
    public void deleteTenant(@NotNull Tenant tenant) {}


    // </editor-fold desc="Administration">


    // <editor-fold desc="Registering actions">


    @Override
    public void registerFolderAction(@NotNull Tenant tenant, @NotNull Action action, @NotNull Folder folder, @NotNull DeskRepresentation deskEntity,
                                     @Nullable Long timeToCompleteInHours) {}


    @Override
    public void registerAdminAction(@Nullable Tenant tenant, @NotNull StatsCategory category, @NotNull Action action, @NotNull String target) {}


    // </editor-fold desc="Registering actions">


    // <editor-fold desc="Charts">


    @Override
    public String getCount(@NotNull Tenant tenant, @NotNull Action action) {
        return null;
    }


    @Override
    public String getCount(@NotNull Tenant tenant, @NotNull String desk) {
        return null;
    }


    @Override
    public String getActionChartUrl(@NotNull Tenant tenant, @NotNull String deskId, @NotNull String startDate, @NotNull String endDate, int width, int height,
                                    @NotNull GraphType graphType, @NotNull GraphPeriod periodicity) {
        return null;
    }


    // </editor-fold desc="Charts">


}