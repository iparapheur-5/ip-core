/*
 * iparapheur Core
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.ipcore.services.database;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import coop.libriciel.ipcore.model.auth.Desk;
import coop.libriciel.ipcore.model.database.*;
import coop.libriciel.ipcore.model.workflow.Folder;
import coop.libriciel.ipcore.model.workflow.FolderSortBy;
import coop.libriciel.ipcore.model.workflow.State;
import coop.libriciel.ipcore.model.workflow.ipWorkflow.IpWorkflowInstance;
import coop.libriciel.ipcore.model.workflow.ipWorkflow.IpWorkflowTask;
import coop.libriciel.ipcore.utils.LocalizedStatusException;
import coop.libriciel.ipcore.utils.TextUtils;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.StringSubstitutor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.jooq.*;
import org.jooq.impl.DSL;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Primary;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.security.InvalidParameterException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.*;

import static coop.libriciel.ipcore.controller.admin.AdminTenantController.MAX_TENANTS_COUNT;
import static coop.libriciel.ipcore.controller.admin.AdminTenantUserController.MAX_USERS_COUNT;
import static coop.libriciel.ipcore.model.auth.User.ATTRIBUTE_CONTENT_GROUP_INDEX;
import static coop.libriciel.ipcore.model.database.TypologyEntity.COLUMN_NAME;
import static coop.libriciel.ipcore.model.workflow.FolderSortBy.SUBTYPE_NAME;
import static coop.libriciel.ipcore.model.workflow.FolderSortBy.TYPE_NAME;
import static coop.libriciel.ipcore.model.workflow.State.*;
import static coop.libriciel.ipcore.services.auth.KeycloakService.*;
import static coop.libriciel.ipcore.utils.TextUtils.PSQL_RENDER_SETTINGS;
import static java.lang.Integer.MAX_VALUE;
import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static java.util.Collections.emptySet;
import static java.util.Comparator.naturalOrder;
import static java.util.stream.Collectors.toList;
import static org.apache.commons.lang3.ObjectUtils.firstNonNull;
import static org.jooq.SQLDialect.POSTGRES;
import static org.jooq.conf.ParamType.INLINED;
import static org.jooq.impl.DSL.*;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;


@Log4j2
@Primary
@Service(DatabaseServiceInterface.BEAN_NAME)
@ConditionalOnProperty(name = DatabaseServiceInterface.PREFERENCES_PROVIDER_KEY, havingValue = "org.postgresql.Driver")
public class PostgresService implements DatabaseServiceInterface {


    public static final String SQL_TASK_ID = "task_id";
    public static final String SQL_TASK_NAME = "task_name";
    public static final String SQL_INSTANCE_ID = "instance_id";
    public static final String SQL_BEGIN_DATE = "begin_date";
    public static final String SQL_INSTANCE_NAME = "instance_name";
    public static final String SQL_CREATE_TIME = "createTime";
    public static final String SQL_DUE_DATE = "due_date";
    public static final String SQL_END_DATE = "end_date";
    public static final String SQL_EXPECTED_ACTION = "expectedAction";
    public static final String SQL_PERFORMED_ACTION = "performedAction";
    public static final String SQL_PENDING = "pending";
    public static final String SQL_VARIABLES = "variables";
    public static final String SQL_CANDIDATE_GROUP = "candidate_group";
    public static final String SQL_ASSIGNEE = "assignee";
    public static final String SQL_ORIGIN_GROUP_ID = "origin_group_id";
    public static final String SQL_FINAL_GROUP_ID = "final_group_id";
    public static final String SQL_SORT_BY = "sort_by";

    public static final Condition TRUE_CONDITION = val(1, Integer.class).eq(val(1, Integer.class));
    private static final Condition FALSE_CONDITION = val(1, Integer.class).eq(val(0, Integer.class));

    private static final String TABLE_RU_EXECUTION = "ACT_RU_EXECUTION";
    private static final String TABLE_RU_IDENTITYLINK = "ACT_RU_IDENTITYLINK";
    private static final String TABLE_RU_TASK = "ACT_RU_TASK";


    private static HikariDataSource dataSource;

    private @Value("${spring.datasource.url}") String dbUrl;
    private @Value("${spring.datasource.username}") String dbUsername;
    private @Value("${spring.datasource.password}") String dbPassword;


    // <editor-fold desc="LifeCycle">


    @Autowired
    public PostgresService() {}


    @PostConstruct
    public void init() {
        HikariConfig config = new HikariConfig();
        config.setJdbcUrl(dbUrl);
        config.setUsername(dbUsername);
        config.setPassword(dbPassword);
        config.setMaxLifetime(5 * 60 * 1000);
        dataSource = new HikariDataSource(config);
    }


    // </editor-fold desc="LifeCycle">


    /**
     * Tenants have an index, to ease grouping in Alfresco.
     * <p>
     * Those indexes could be a regular number field, auto-incremented with an SQL primitive function,
     * but this method leave gaps, on every tenant deletion.
     * <p>
     * To get the smaller index available, in one request, this do the trick.
     *
     * @return a long
     */
    @Override
    public long getTenantIndexAvailable() {
        try (Connection connection = dataSource.getConnection()) {
            return DSL.using(connection, PSQL_RENDER_SETTINGS)
                    .fetchOptional(buildTenantIndexAvailableRequest())
                    .map(Record1::value1)
                    .orElseThrow(() -> new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.cannot_read_database_response"));
        } catch (SQLException e) {
            throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.cannot_reach_database", e);
        }
    }


    static @NotNull Select<Record1<Integer>> buildTenantIndexAvailableRequest() {

        String possibleIndex = "possibleIndex";

        return DSL.using(POSTGRES, PSQL_RENDER_SETTINGS)
                .select(field(possibleIndex, Integer.class))
                .from(generateSeries(0, MAX_TENANTS_COUNT).as(possibleIndex))
                .where(field(possibleIndex)
                        .notIn(
                                select(field(Tenant.COLUMN_INDEX)).from(Tenant.TABLE_NAME)
                        ))
                .limit(1);
    }


    @Override
    public int getUserDataGroupIndexAvailable() {
        try (Connection connection = dataSource.getConnection()) {
            return DSL.using(connection, PSQL_RENDER_SETTINGS)
                    .fetchOptional(buildUserDataGroupIndexAvailableRequest())
                    .map(Record1::value1)
                    .orElseThrow(() -> new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.cannot_read_database_response"));
        } catch (SQLException e) {
            throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.cannot_reach_database", e);
        }
    }


    static @NotNull Select<Record1<Integer>> buildUserDataGroupIndexAvailableRequest() {

        String possibleIndex = "possibleIndex";

        return DSL.using(POSTGRES, PSQL_RENDER_SETTINGS)
                .select(field(possibleIndex, Integer.class))
                .from(generateSeries(0, MAX_USERS_COUNT).as(possibleIndex))
                .where(field(possibleIndex)
                        .notIn(
                                select(field("value").cast(Integer.class))
                                        .from("user_attribute")
                                        .where(field("name", String.class).equal(ATTRIBUTE_CONTENT_GROUP_INDEX))
                        ))
                .limit(1);
    }


    @Override
    public @NotNull Page<TenantRepresentation> listTenantsForUser(@NotNull String userId,
                                                                  @NotNull Pageable pageable,
                                                                  @Nullable String searchTerm,
                                                                  boolean reverse,
                                                                  boolean adminOnly,
                                                                  boolean includeFunctionalAdminTenants) {

        log.debug("listTenantsForUser searchTerm:{} page:{} pageSize:{}, reverse:{}",
                searchTerm,
                pageable.isPaged() ? pageable.getPageNumber() : "NA",
                pageable.isPaged() ? pageable.getPageSize() : "INF",
                reverse);

        try (Connection connection = dataSource.getConnection()) {

            DSLContext dsl = DSL.using(connection, PSQL_RENDER_SETTINGS);
            List<TenantRepresentation> result = dsl
                    .fetch(adminOnly
                           ? buildListAdministeredTenantsForUserRequest(userId, pageable, searchTerm, includeFunctionalAdminTenants)
                           : buildListTenantsForUserRequest(userId, pageable, searchTerm, reverse)
                    )
                    .stream()
                    .map(TenantRepresentation::new)
                    .collect(toList());

            // Sending back result

            log.info("listTenantsForUser resultSize:{}", result.size());

            long total = dsl
                    .fetchOptional(adminOnly ? buildCountAdministeredTenantsForUser(userId, searchTerm, includeFunctionalAdminTenants)
                                             : buildCountTenantsForUser(userId, searchTerm, reverse))
                    .map(Record1::value1)
                    .orElse(-1);

            return new PageImpl<>(result, pageable, total);

        } catch (SQLException e) {
            throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.cannot_reach_database", e);
        }
    }


    static @NotNull Select<Record4<String, String, String, String>>
    buildListAdministeredTenantsForUserRequest(@NotNull String userId,
                                               @NotNull Pageable pageable,
                                               @Nullable String searchTerm,
                                               boolean withFunctionalAdmins) {

        long offset = (pageable != Pageable.unpaged()) ? pageable.getOffset() : 0L;
        long limit = (pageable != Pageable.unpaged()) ? pageable.getPageSize() : MAX_VALUE;

        String sortBy = pageable.getSort().stream().findFirst()
                .map(Sort.Order::getProperty)
                .orElse(Tenant.COLUMN_NAME);

        boolean asc = pageable.getSort().stream().findFirst()
                .map(Sort.Order::getDirection)
                .map(Sort.Direction::isAscending)
                .orElse(true);

        Condition searchCondition = TextUtils.getAsSqlLikeWrapped(searchTerm)
                .map(DSL::upper)
                .map(wrappedSearch -> (Condition) upper(field("t." + Tenant.COLUMN_NAME, String.class)).like(wrappedSearch))
                .orElse(TRUE_CONDITION);

        Select<Record4<String, String, String, String>> result;
        SelectOnConditionStep<Record4<String, String, String, String>> partialQuery = DSL.using(POSTGRES, PSQL_RENDER_SETTINGS)
                .select(
                        field("t." + Tenant.COLUMN_ID, String.class).as(Tenant.COLUMN_ID),
                        field("t." + Tenant.COLUMN_NAME, String.class).as(Tenant.COLUMN_NAME),
                        field("urma.role_id", String.class).as("admin_link"),
                        field("urma.user_id", String.class).as("user_id")
                )
                .from(table(Tenant.TABLE_NAME).as("t"))

                .innerJoin(table("keycloak_role").as("kra"))
                .on(field("kra.name").equal(concat(
                        val("tenant_", String.class),
                        field("t." + Tenant.COLUMN_ID, String.class),
                        val("_admin", String.class)
                )));

        if (withFunctionalAdmins) {
            partialQuery = partialQuery.innerJoin(table("keycloak_role").as("krf"))
                    .on(field("krf.name").equal(concat(
                            val("tenant_", String.class),
                            field("t." + Tenant.COLUMN_ID, String.class),
                            val("_functional_admin", String.class)
                    )))
                    .leftOuterJoin(table("user_role_mapping").as("urmf"))
                    .on(and(
                            field("urmf.role_id").equal(field("krf.id")),
                            field("urmf.user_id").equal(userId)
                    ));
        }

        result = partialQuery.leftOuterJoin(table("user_role_mapping").as("urma"))
                .on(and(
                        field("urma.role_id").equal(field("kra.id")),
                        field("urma.user_id").equal(userId)
                ))

                .where(and(searchCondition, or(
                        field("urma.role_id").isNotNull(),
                        withFunctionalAdmins ? field("urmf.role_id").isNotNull() : FALSE_CONDITION)
                ))

                .orderBy(asc ? field("t." + sortBy).asc() : field("t." + sortBy).desc())
                .limit(limit)
                .offset(offset);

        log.trace("buildListAdministeredTenantsForUserRequest SQL:{}", result.getSQL(INLINED));
        return result;
    }


    static @NotNull Select<Record4<String, String, String, String>>
    buildListTenantsForUserRequest(@NotNull String userId,
                                   @NotNull Pageable pageable,
                                   @Nullable String searchTerm,
                                   boolean reverse) {

        long offset = (pageable != Pageable.unpaged()) ? pageable.getOffset() : 0L;
        long limit = (pageable != Pageable.unpaged()) ? pageable.getPageSize() : MAX_VALUE;

        String sortBy = pageable.getSort().stream().findFirst()
                .map(Sort.Order::getProperty)
                .orElse(Tenant.COLUMN_NAME);

        boolean asc = pageable.getSort().stream().findFirst()
                .map(Sort.Order::getDirection)
                .map(Sort.Direction::isAscending)
                .orElse(true);

        Condition searchCondition = TextUtils.getAsSqlLikeWrapped(searchTerm)
                .map(DSL::upper)
                .map(wrappedSearch -> (Condition) upper(field("t." + Tenant.COLUMN_NAME, String.class)).like(wrappedSearch))
                .orElse(TRUE_CONDITION);

        Select<Record4<String, String, String, String>> result = DSL.using(POSTGRES, PSQL_RENDER_SETTINGS)
                .select(
                        field("t." + Tenant.COLUMN_ID, String.class).as(Tenant.COLUMN_ID),
                        field("t." + Tenant.COLUMN_NAME, String.class).as(Tenant.COLUMN_NAME),
                        field("urmu.role_id", String.class).as("user_link"),
                        field("urmu.user_id", String.class).as("user_id")
                )
                .from(table(Tenant.TABLE_NAME).as("t"))

                .innerJoin(table("keycloak_role").as("kru"))
                .on(field("kru.name").equal(concat(
                        val("tenant_", String.class),
                        field("t." + Tenant.COLUMN_ID, String.class)
                )))


                .leftOuterJoin(table("user_role_mapping").as("urmu"))
                .on(and(
                        field("urmu.role_id").equal(field("kru.id")),
                        field("urmu.user_id").equal(userId)
                ))

                .where(and(
                        searchCondition,
                        reverse
                        ? field("urmu.role_id").isNull()
                        : field("urmu.role_id").isNotNull()
                ))

                .orderBy(asc ? field("t." + sortBy).asc() : field("t." + sortBy).desc())
                .limit(limit)
                .offset(offset);

        log.trace("buildListTenantsForUserRequest SQL:{}", result.getSQL(INLINED));
        return result;
    }


    static @NotNull Select<Record1<Integer>>
    buildCountAdministeredTenantsForUser(@NotNull String userId, @Nullable String searchTerm, boolean withFunctionalAdmins) {

        Condition searchCondition = TextUtils.getAsSqlLikeWrapped(searchTerm)
                .map(wrappedSearch -> (Condition) field("t." + Tenant.COLUMN_NAME, String.class).like(wrappedSearch))
                .orElse(TRUE_CONDITION);

        Select<Record1<Integer>> result;
        SelectOnConditionStep<Record1<Integer>> partialQuery = DSL.using(POSTGRES, PSQL_RENDER_SETTINGS)
                .selectCount()
                .from(table(Tenant.TABLE_NAME).as("t"))

                .innerJoin(table("keycloak_role").as("kra"))
                .on(field("kra.name").equal(concat(
                        val("tenant_", String.class),
                        field("t." + Tenant.COLUMN_ID, String.class),
                        val("_admin", String.class)
                )));

        if (withFunctionalAdmins) {
            partialQuery = partialQuery.innerJoin(table("keycloak_role").as("krf"))
                    .on(field("krf.name").equal(concat(
                            val("tenant_", String.class),
                            field("t." + Tenant.COLUMN_ID, String.class),
                            val("_functional_admin", String.class)
                    )))
                    .leftOuterJoin(table("user_role_mapping").as("urmf"))
                    .on(and(
                            field("urmf.role_id").equal(field("krf.id")),
                            field("urmf.user_id").equal(userId)
                    ));
        }

        result = partialQuery.leftOuterJoin(table("user_role_mapping").as("urma"))
                .on(and(
                        field("urma.role_id").equal(field("kra.id")),
                        field("urma.user_id").equal(userId)
                ))

                .where(and(searchCondition, or(
                        field("urma.role_id").isNotNull(),
                        withFunctionalAdmins ? field("urmf.role_id").isNotNull() : FALSE_CONDITION)
                ));

        return result;
    }


    static @NotNull Select<Record1<Integer>>
    buildCountTenantsForUser(@NotNull String userId, @Nullable String searchTerm, boolean reverse) {

        Condition searchCondition = TextUtils.getAsSqlLikeWrapped(searchTerm)
                .map(wrappedSearch -> (Condition) field("t." + Tenant.COLUMN_NAME, String.class).like(wrappedSearch))
                .orElse(TRUE_CONDITION);

        return DSL.using(POSTGRES, PSQL_RENDER_SETTINGS)
                .selectCount()
                .from(table(Tenant.TABLE_NAME).as("t"))

                .innerJoin(table("keycloak_role").as("kru"))
                .on(field("kru.name").equal(concat(
                        val("tenant_", String.class),
                        field("t." + Tenant.COLUMN_ID, String.class)
                )))


                .leftOuterJoin(table("user_role_mapping").as("urmu"))
                .on(and(
                        field("urmu.role_id").equal(field("kru.id")),
                        field("urmu.user_id").equal(userId)
                ))

                .where(and(
                        searchCondition,
                        reverse
                        ? field("urmu.role_id").isNull()
                        : field("urmu.role_id").isNotNull()
                ));
    }


    @Override
    public @NotNull List<String> listAdminUsersIdForTenant(@NotNull TenantRepresentation tenant) {
        log.debug("listAdminUsersForTenant tenant:{}", tenant.getId());
        try (Connection connection = dataSource.getConnection()) {

            DSLContext dsl = DSL.using(connection, PSQL_RENDER_SETTINGS);
            List<String> result = dsl
                    .fetch(buildListAdminUsersIdForTenant(tenant.getId()))
                    .stream()
                    .map(r -> r.get(0, String.class))
                    .toList();

            log.info("listTenantsForUser resultSize:{}", result.size());
            return result;

        } catch (SQLException e) {
            throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.cannot_reach_database", e);
        }
    }


    /**
     * Explain : We want to fetch the somehow-tenant-admins here.
     * <p>
     * They are the union between that have the roles :
     * - "tenant_id01_admin" : the native tenant-admin ones
     * - "admin" + "tenant_id01" : the super-admins that are somehow linked to a tenant
     * <p>
     * By design, those two groups cannot overlap.
     * We can do 2 separate requests, merged in an union-all.
     *
     * @param tenantId
     * @return A Record2 selector user_id/role_name
     */
    static @NotNull Select<Record2<String, String>>
    buildListAdminUsersIdForTenant(@NotNull String tenantId) {

        Map<String, String> tenantIdMapping = Map.of(TENANT_PLACEHOLDER, tenantId);
        String tenantAdminRoleName = StringSubstitutor.replace(TENANT_ADMIN_INTERNAL_NAME, tenantIdMapping);
        String tenantUserRoleName = StringSubstitutor.replace(TENANT_INTERNAL_NAME, tenantIdMapping);

        String tenantAdminUserRoleMappingAlias = "urm";
        String tenantAdminRoleAlias = "krta";
        String superAdminUserRoleMappingAlias = "urma";
        String superAdminRoleAlias = "krsa";
        String simpleUserRoleRoleMappingAlias = "urmta";
        String simpleUserRoleAlias = "kru";

        Select<Record2<String, String>> result = DSL.using(POSTGRES, PSQL_RENDER_SETTINGS)
                .select(
                        field(tenantAdminUserRoleMappingAlias + ".user_id", String.class).as("user_id"),
                        field(tenantAdminRoleAlias + ".name", String.class)
                )
                .from(table("user_role_mapping").as(tenantAdminUserRoleMappingAlias))

                .innerJoin(table("keycloak_role").as(tenantAdminRoleAlias))
                .on(and(
                        field(tenantAdminRoleAlias + ".name", String.class).equal(val(tenantAdminRoleName, String.class)),
                        field(tenantAdminUserRoleMappingAlias + ".role_id", String.class).equal(field(tenantAdminRoleAlias + ".id", String.class))
                ))

                .unionAll(
                        select(
                                field(superAdminUserRoleMappingAlias + ".user_id", String.class).as("user_id"),
                                field(simpleUserRoleAlias + ".name", String.class)
                        )
                                .from(table("user_role_mapping").as(superAdminUserRoleMappingAlias))

                                .innerJoin(table("keycloak_role").as(superAdminRoleAlias))
                                .on(and(
                                        field(superAdminRoleAlias + ".name", String.class).equal(val(SUPER_ADMIN_ROLE_NAME, String.class)),
                                        field(superAdminUserRoleMappingAlias + ".role_id", String.class).equal(field(superAdminRoleAlias + ".id", String.class))
                                ))

                                .innerJoin(table("user_role_mapping").as(simpleUserRoleRoleMappingAlias))
                                .on(field(superAdminUserRoleMappingAlias + ".user_id", String.class).equal(field(simpleUserRoleRoleMappingAlias + ".user_id",
                                        String.class)))

                                .innerJoin(table("keycloak_role").as(simpleUserRoleAlias))
                                .on(and(
                                        field(simpleUserRoleAlias + ".name", String.class).equal(val(tenantUserRoleName, String.class)),
                                        field(simpleUserRoleRoleMappingAlias + ".role_id", String.class).equal(field(simpleUserRoleAlias + ".id", String.class))
                                ))
                );

        log.trace("buildListTenantsForUserRequest SQL:{}", result.getSQL(INLINED));
        return result;
    }


    @Override
    public @NotNull List<TypologyRepresentation> getTypologyHierarchy(@NotNull String tenantId,
                                                                      @NotNull Pageable pageable,
                                                                      boolean collapseAll,
                                                                      @Nullable Set<String> reverseIdList,
                                                                      @Nullable String searchTerm) {

        try (Connection connection = dataSource.getConnection()) {
            return DSL.using(connection, PSQL_RENDER_SETTINGS)
                    .fetch(buildTypologyEntityListSqlRequest(tenantId, pageable, collapseAll, firstNonNull(reverseIdList, emptySet()), searchTerm))
                    .stream()
                    .map(TypologyRepresentation::new)
                    .toList();
        } catch (SQLException e) {
            throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.cannot_reach_database", e);
        }
    }


    /**
     * The request is split in 2 sub-requests, merged with an UNION ALL :
     * - {@link Type} that matches the search request, or containing a {@link Subtype} that matches the search request
     * - {@link Subtype} that matches the search request
     *
     * @param tenantId
     * @param pageable
     * @param collapseAll
     * @param reversedIdList
     * @param searchTerm
     * @return
     */
    static @NotNull Select<Record7<String, String, String, String, String, String, Integer>>
    buildTypologyEntityListSqlRequest(@NotNull String tenantId, @NotNull Pageable pageable, boolean collapseAll, @NotNull Set<String> reversedIdList,
                                      @Nullable String searchTerm) {

        if (pageable.getSort().stream()
                .map(Sort.Order::getProperty)
                .anyMatch(property -> !StringUtils.equals(property, TypologySortBy.NAME.getColumnName()))) {
            throw new LocalizedStatusException(BAD_REQUEST, "message.typology_hierarchy_can_only_be_sort_by_name");
        }

        boolean sortAscending = pageable.getSort().stream().findFirst()
                .map(Sort.Order::getDirection)
                .map(Sort.Direction::isAscending)
                .orElse(true);

        String order1 = "order1_name";
        String order2 = "order2_name";

        int limit = pageable.isPaged() ? pageable.getPageSize() : MAX_VALUE;
        long offset = pageable.isPaged() ? pageable.getOffset() : 0;

        return DSL.using(POSTGRES, PSQL_RENDER_SETTINGS)
                .select(
                        field("t." + Type.COLUMN_ID, String.class).as(TypologyEntity.COLUMN_ID),
                        field("t." + Type.COLUMN_NAME, String.class).as(TypologyEntity.COLUMN_NAME),
                        field("t." + Type.COLUMN_DESCRIPTION, String.class).as(TypologyEntity.COLUMN_DESCRIPTION),
                        field("t." + Type.COLUMN_NAME, String.class).as(order1),
                        val(null, String.class).as(order2),
                        val(null, String.class).as(TypologyRepresentation.COLUMN_PARENT_ID),
                        count(field("st." + Subtype.COLUMN_ID)).as(TypologyRepresentation.COLUMN_CHILDREN_COUNT)
                )

                .from(table(Type.TABLE_NAME).as("t"))

                .leftJoin(table(Subtype.TABLE_NAME).as("st"))
                .on(field("st." + Subtype.COLUMN_PARENT).eq(field("t." + Type.COLUMN_ID)))
                .where(and(
                        field("t." + Type.COLUMN_TENANT_ID).eq(tenantId),
                        TextUtils.getAsSqlLikeWrapped(searchTerm)
                                .map(s -> or(
                                        field("t." + COLUMN_NAME, String.class).likeIgnoreCase(s),
                                        field("st." + COLUMN_NAME, String.class).likeIgnoreCase(s)
                                ))
                                .orElse(val(1).eq(1))
                ))

                .groupBy(field("t." + TypologyEntity.COLUMN_ID),
                        field("t." + TypologyEntity.COLUMN_NAME),
                        field("t." + TypologyEntity.COLUMN_DESCRIPTION),
                        field(TypologyRepresentation.COLUMN_PARENT_ID),
                        field(order1),
                        field(order2)
                )

                .unionAll(
                        select(
                                field("st." + Subtype.COLUMN_ID, String.class).as(TypologyEntity.COLUMN_ID),
                                field("st." + Subtype.COLUMN_NAME, String.class).as(TypologyEntity.COLUMN_NAME),
                                field("st." + Subtype.COLUMN_DESCRIPTION, String.class).as(TypologyEntity.COLUMN_DESCRIPTION),
                                field("t." + Type.COLUMN_NAME, String.class).as(order1),
                                field("st." + Subtype.COLUMN_NAME, String.class).as(order2),
                                field("t." + Type.COLUMN_ID, String.class).as(TypologyRepresentation.COLUMN_PARENT_ID),
                                val(0, Integer.class).as(TypologyRepresentation.COLUMN_CHILDREN_COUNT)
                        ).from(table(Subtype.TABLE_NAME).as("st"))
                                .innerJoin(table(Type.TABLE_NAME).as("t"))
                                .on(field("st." + Subtype.COLUMN_PARENT).equal(field("t." + Type.COLUMN_ID)))
                                .where(and(
                                        field("st." + Subtype.COLUMN_TENANT_ID).equal(tenantId),
                                        collapseAll
                                        ? field("t." + Type.COLUMN_ID).in(reversedIdList)
                                        : field("t." + Type.COLUMN_ID).notIn(reversedIdList),
                                        TextUtils.getAsSqlLikeWrapped(searchTerm)
                                                .map(s -> (Condition) field("st." + COLUMN_NAME, String.class).likeIgnoreCase(s))
                                                .orElse(val(1).eq(1))
                                ))
                )
                .orderBy(
                        sortAscending ? field(order1).asc() : field(order1).desc(),
                        sortAscending ? field(order2).asc().nullsFirst() : field(order2).desc().nullsFirst()
                )
                .limit(limit)
                .offset(offset);
    }


    static @NotNull Select<Record1<Integer>>
    buildTypologyEntityTotalSqlRequest(@NotNull String tenantId, boolean collapseAll, @NotNull Set<String> reversedIdList, @Nullable String searchTerm) {

        Select<Record1<Integer>> result = DSL.using(POSTGRES, PSQL_RENDER_SETTINGS)
                .selectCount()
                .from(
                        select(field("t." + Type.COLUMN_ID, String.class).as(Type.COLUMN_ID))
                                .from(table(Type.TABLE_NAME).as("t"))
                                .leftJoin(table(Subtype.TABLE_NAME).as("st"))
                                .on(field("st." + Subtype.COLUMN_PARENT).eq(field("t." + Type.COLUMN_ID)))
                                .where(and(
                                        field("t." + Type.COLUMN_TENANT_ID, String.class).eq(tenantId),
                                        TextUtils.getAsSqlLikeWrapped(searchTerm)
                                                .map(s -> or(
                                                        field("t." + COLUMN_NAME, String.class).likeIgnoreCase(s),
                                                        field("st." + COLUMN_NAME, String.class).likeIgnoreCase(s)
                                                ))
                                                .orElse(val(1).eq(1)))
                                )
                                .unionAll(
                                        select(field("st." + Subtype.COLUMN_ID, String.class).as(Subtype.COLUMN_ID))
                                                .from(table(Subtype.TABLE_NAME).as("st"))
                                                .innerJoin(table(Type.TABLE_NAME).as("t"))
                                                .on(field("st." + Subtype.COLUMN_PARENT).equal(field("t." + Type.COLUMN_ID)))
                                                .where(and(
                                                        field("st." + Subtype.COLUMN_TENANT_ID, String.class).eq(tenantId),
                                                        collapseAll
                                                        ? field("t." + Type.COLUMN_ID).in(reversedIdList)
                                                        : field("t." + Type.COLUMN_ID).notIn(reversedIdList),
                                                        TextUtils.getAsSqlLikeWrapped(searchTerm)
                                                                .map(s -> (Condition) field("st." + COLUMN_NAME, String.class).likeIgnoreCase(s))
                                                                .orElse(val(1).eq(1)))
                                                )
                                ));

        log.trace("buildTypologyEntityTotalSqlRequest:\n{}", result.getSQL(INLINED));
        return result;
    }


    @Override
    public Integer getTypologyHierarchyListTotal(@NotNull String tenantId,
                                                 boolean collapseAll,
                                                 @Nullable Set<String> reverseIdList,
                                                 @Nullable String searchTerm) {

        try (Connection connection = dataSource.getConnection()) {
            return DSL.using(connection, PSQL_RENDER_SETTINGS)
                    .fetchOptional(buildTypologyEntityTotalSqlRequest(tenantId, collapseAll, firstNonNull(reverseIdList, emptySet()), searchTerm))
                    .map(Record1::value1)
                    .orElse(-1);
        } catch (SQLException e) {
            throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.cannot_reach_database", e);
        }
    }


    static @NotNull Select<Record10<String, String, String, String, String, Date, Date, String, String[][], String>>
    buildTasksSqlRequest(@NotNull Desk desk, @NotNull State state, @Nullable FolderSortBy sortBy, boolean asc, int page, int pageSize) {

        Condition stateCondition = switch (state) {
            case DRAFT -> field(TABLE_RU_TASK + ".name_").equal("workflow_internal_" + DRAFT.toString().toLowerCase());
            case REJECTED -> field(TABLE_RU_TASK + ".name_").equal("workflow_internal_" + REJECTED.toString().toLowerCase());
            case FINISHED -> field(TABLE_RU_TASK + ".name_").equal("workflow_internal_" + FINISHED.toString().toLowerCase());
            default -> and(
                    field(TABLE_RU_TASK + ".name_").notEqual("workflow_internal_" + DRAFT.toString().toLowerCase()),
                    field(TABLE_RU_TASK + ".name_").notEqual("workflow_internal_" + REJECTED.toString().toLowerCase()),
                    field(TABLE_RU_TASK + ".name_").notEqual("workflow_internal_" + FINISHED.toString().toLowerCase())
            );
        };

        // Build up the SQL request

        return DSL.using(POSTGRES, PSQL_RENDER_SETTINGS)
                .select(
                        field(TABLE_RU_EXECUTION + ".root_proc_inst_id_", String.class).as(SQL_INSTANCE_ID),
                        field("act_ru_exec_root" + ".name_", String.class).as(SQL_INSTANCE_NAME),
                        field(TABLE_RU_IDENTITYLINK + ".group_id_", String.class).as(SQL_CANDIDATE_GROUP),
                        field(TABLE_RU_TASK + ".id_", String.class).as(SQL_TASK_ID),
                        field(TABLE_RU_TASK + ".name_", String.class).as(SQL_TASK_NAME),
                        field(TABLE_RU_TASK + ".due_date_", Date.class).as(SQL_DUE_DATE),
                        field(TABLE_RU_EXECUTION + ".start_time_", Date.class).as(SQL_BEGIN_DATE),
                        val(null, String.class).as(SQL_END_DATE),
                        field(arrayAggDistinct(array(
                                field("act_ru_variable_md.name_", String.class),
                                field("act_ru_variable_md.text_", String.class)))
                        ).as(SQL_VARIABLES),
                        field("typology.name", String.class).as(SQL_SORT_BY))

                .from(table(TABLE_RU_IDENTITYLINK))

                .innerJoin(table(TABLE_RU_TASK))
                .on(field(TABLE_RU_IDENTITYLINK + ".task_id_").equal(field(TABLE_RU_TASK + ".id_")))

                .innerJoin(table(TABLE_RU_EXECUTION))
                .on(field(TABLE_RU_EXECUTION + ".id_").equal(field(TABLE_RU_TASK + ".proc_inst_id_")))

                .innerJoin(table(TABLE_RU_EXECUTION).as("act_ru_exec_root"))
                .on(field("act_ru_exec_root" + ".id_").equal(field(TABLE_RU_EXECUTION + ".root_proc_inst_id_")))

                .innerJoin(table("act_ru_variable").as("act_ru_variable_md"))
                .on(field(TABLE_RU_EXECUTION + ".root_proc_inst_id_").equal(field("act_ru_variable_md.proc_inst_id_")))

                .leftJoin(table("act_ru_variable").as("act_ru_variables_sb"))
                .on(field(TABLE_RU_EXECUTION + ".root_proc_inst_id_").equal(field("act_ru_variables_sb.proc_inst_id_")))
                .and(field("act_ru_variables_sb" + ".name_").equal(sortBy == TYPE_NAME ? "i_Parapheur_internal_type_id" : "i_Parapheur_internal_subtype_id"))

                .leftJoin(table(sortBy == TYPE_NAME ? Type.TABLE_NAME : Subtype.TABLE_NAME).as("typology"))
                .on(field("act_ru_variables_sb" + ".text_").equal(field("typology.id")))

                .leftJoin(table("act_ru_variable").as("act_ru_variables_db"))
                .on(field(TABLE_RU_EXECUTION + ".root_proc_inst_id_").equal(field("act_ru_variables_db.proc_inst_id_")))

                .where(field(TABLE_RU_IDENTITYLINK + ".type_").equal("candidate"))
                .and(buildPermissionCondition(desk))
                .and(field(TABLE_RU_TASK + ".name_").notEqual("undo"))
                .and(stateCondition)

                .groupBy(
                        field(SQL_INSTANCE_ID),
                        field(SQL_INSTANCE_NAME),
                        field(SQL_CANDIDATE_GROUP),
                        field(SQL_TASK_ID),
                        field(SQL_TASK_NAME),
                        field(SQL_DUE_DATE),
                        field(SQL_BEGIN_DATE),
                        field(SQL_SORT_BY)
                )

                .orderBy(asc ? field(SQL_SORT_BY).asc() : field(SQL_SORT_BY).desc(), field(SQL_BEGIN_DATE).asc())
                .limit(pageSize)
                .offset(pageSize * page);
    }


    static Condition buildPermissionCondition(@NotNull Desk desk) {

        ArrayList<String> deskIds = new ArrayList<>();
        deskIds.add(desk.getId());
        deskIds.addAll(Optional.ofNullable(desk.getDelegatingDesks())
                .orElse(emptyList())
                .stream()
                .map(Desk::getId)
                .toList());
        deskIds.sort(naturalOrder());

        // Building SQL request.

        List<Condition> delegatingConditionList = new ArrayList<>();
        delegatingConditionList.add(field(TABLE_RU_IDENTITYLINK + ".group_id_").in(deskIds));

        Optional.ofNullable(desk.getDelegationRules())
                .stream()
                .flatMap(Collection::stream)
                .map(c -> and(
                        field(TABLE_RU_IDENTITYLINK + ".group_id_").equal(c.getDelegatingGroup()),
                        field("act_ru_variables_db.name_").equal(c.getMetadataKey()),
                        field("act_ru_variables_db.text_").equal(c.getMetadataValue())))
                .forEach(delegatingConditionList::add);

        return or(delegatingConditionList);
    }


    @Override
    public @NotNull List<? extends Folder> getFoldersSortedByTypologyName(@NotNull Desk desk, @NotNull State state,
                                                                          @NotNull FolderSortBy sortBy, boolean asc, int page, int pageSize) {

        if (!asList(TYPE_NAME, SUBTYPE_NAME).contains(sortBy)) {
            throw new InvalidParameterException("This method only accept sorting by Type/Subtype name");
        }

        try (Connection connection = dataSource.getConnection()) {
            return DSL.using(connection, PSQL_RENDER_SETTINGS)
                    .fetch(buildTasksSqlRequest(desk, state, sortBy, asc, page, pageSize))
                    .stream()
                    .map(IpWorkflowTask::new)
                    .map(IpWorkflowInstance::new)
                    .toList();
        } catch (SQLException e) {
            throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.cannot_reach_database", e);
        }
    }


}
