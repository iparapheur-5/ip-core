/*
 * iparapheur Core
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.ipcore.services.database;

import coop.libriciel.ipcore.model.database.Subtype;
import coop.libriciel.ipcore.model.database.SubtypeMetadata;
import coop.libriciel.ipcore.model.database.Type;
import coop.libriciel.ipcore.model.database.TypologyEntity;
import coop.libriciel.ipcore.model.workflow.Folder;
import coop.libriciel.ipcore.services.content.ContentServiceProperties;
import coop.libriciel.ipcore.utils.LocalizedStatusException;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.StreamSupport;

import static coop.libriciel.ipcore.controller.DocumentController.MAX_MAIN_DOCUMENTS;
import static coop.libriciel.ipcore.model.database.SubtypeMetadata.SUBTYPE_METADATA_COMPARATOR;
import static org.springframework.http.HttpStatus.NOT_ACCEPTABLE;
import static org.springframework.http.HttpStatus.NOT_FOUND;


/**
 * Utility class to factorize typology-related logic, using repositories.
 */
@Service
@Log4j2
public class TypologyService {


    // <editor-fold desc="Beans">


    private final SubtypeRepository subtypeRepository;
    private final ContentServiceProperties contentServiceProperties;


    @Autowired
    public TypologyService(SubtypeRepository subtypeRepository, ContentServiceProperties contentServiceProperties) {
        this.subtypeRepository = subtypeRepository;
        this.contentServiceProperties = contentServiceProperties;
    }


    // </editor-fold desc="Beans">


    public void updateTypology(@NotNull List<? extends Folder> elements) {

        Map<String, Type> typesMap = new HashMap<>();
        elements.stream()
                .map(Folder::getType)
                .filter(Objects::nonNull)
                .filter(t -> StringUtils.isEmpty(t.getName()))
                .map(TypologyEntity::getId)
                .filter(StringUtils::isNotEmpty)
                .forEach(i -> typesMap.put(i, null));

        Map<String, Subtype> subtypesMap = new HashMap<>();
        elements.stream()
                .map(Folder::getSubtype)
                .filter(Objects::nonNull)
                .filter(t -> StringUtils.isEmpty(t.getName()))
                .map(TypologyEntity::getId)
                .filter(StringUtils::isNotEmpty)
                .forEach(i -> subtypesMap.put(i, null));

        StreamSupport
                .stream(subtypeRepository.findAllById(subtypesMap.keySet()).spliterator(), false)
                .peek(st -> typesMap.put(st.getParentType().getId(), st.getParentType()))
                .forEach(st -> subtypesMap.put(st.getId(), st));

        elements.stream()
                .filter(f -> f.getType() != null)
                .filter(f -> StringUtils.isEmpty(f.getType().getName()))
                .forEach(f -> f.setType(typesMap.get(f.getType().getId())));

        elements.stream()
                .filter(f -> f.getSubtype() != null)
                .filter(f -> StringUtils.isEmpty(f.getSubtype().getName()))
                .forEach(f -> f.setSubtype(subtypesMap.get(f.getSubtype().getId())));

        // Integrity check, at last.

        if (elements.stream()
                .filter(e -> e.getType() != null)
                .filter(e -> e.getSubtype() != null)
                .anyMatch(e -> !StringUtils.equals(e.getType().getId(), e.getSubtype().getParentType().getId()))) {
            throw new LocalizedStatusException(NOT_FOUND, "message.unknown_subtype_id");
        }

        // Hardcoded parameter,
        // This won't be necessary anymore in 5.1, when this value will be stored in database

        int maxMainDoc = Math.min(contentServiceProperties.getMaxMainFiles(), MAX_MAIN_DOCUMENTS);

        elements.stream()
                .map(Folder::getSubtype)
                .filter(Objects::nonNull)
                .forEach(s -> s.setMaxMainDocuments(s.isMultiDocuments() ? maxMainDoc : 1));

        // Metadata sorts

        elements.stream().map(Folder::getSubtype)
                .filter(Objects::nonNull)
                .map(Subtype::getSubtypeMetadataList)
                .filter(CollectionUtils::isNotEmpty)
                .forEach(subtypeMetadataList -> subtypeMetadataList.sort(SUBTYPE_METADATA_COMPARATOR));
    }


    public void verifyFolderMetadataForValidationWorkflow(@NotNull String tenantId, @NotNull Folder folder) {
        Subtype fullSubtype = subtypeRepository.findByIdAndTenant_Id(folder.getSubtype().getId(), tenantId)
                .orElseThrow(() -> new LocalizedStatusException(NOT_FOUND, "message.unknown_subtype_id"));

        boolean everyMandatoryMetadataIsFilled = isEveryMandatoryMetadataValued(folder.getMetadata(), fullSubtype.getSubtypeMetadataList(), false);

        if (!everyMandatoryMetadataIsFilled) {
            log.debug("verifyFolderMetadata - A mandatory metadata was not filled, abort");
            throw new LocalizedStatusException(NOT_ACCEPTABLE, "message.missing_mandatory_metadata");
        }
    }


    public void verifyMetadataForDraftOrCreation(Map<String, String> inputMetadata, List<SubtypeMetadata> subtypeMetadataList) {
        boolean everyMandatoryMetadataIsFilled = isEveryMandatoryMetadataValued(inputMetadata, subtypeMetadataList, true);

        if (!everyMandatoryMetadataIsFilled) {
            log.debug("createDraftFolder - A mandatory metadata was not filled, abort");
            throw new LocalizedStatusException(NOT_ACCEPTABLE, "message.missing_mandatory_metadata");
        }
    }


    private boolean isEveryMandatoryMetadataValued(Map<String, String> inputMetadata, List<SubtypeMetadata> subtypeMetadataList, boolean allowAllEditable) {
        boolean everyMandatoryMetadataIsFilled = subtypeMetadataList.stream()
                .filter(SubtypeMetadata::isMandatory)
                .filter(meta -> StringUtils.isEmpty(meta.getDefaultValue()))
                .filter(meta -> !(meta.isEditable() && allowAllEditable))
                .map(subtypeMetadata -> subtypeMetadata.getMetadata().getKey())
                .allMatch(inputMetadata::containsKey);
        return everyMandatoryMetadataIsFilled;
    }


}
