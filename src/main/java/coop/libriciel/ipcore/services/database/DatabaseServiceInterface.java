/*
 * iparapheur Core
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.ipcore.services.database;

import coop.libriciel.ipcore.model.auth.Desk;
import coop.libriciel.ipcore.model.auth.User;
import coop.libriciel.ipcore.model.database.TenantRepresentation;
import coop.libriciel.ipcore.model.database.TypologyRepresentation;
import coop.libriciel.ipcore.model.workflow.Folder;
import coop.libriciel.ipcore.model.workflow.FolderSortBy;
import coop.libriciel.ipcore.model.workflow.State;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;


@Service
public interface DatabaseServiceInterface {


    String BEAN_NAME = "databaseService";
    String PREFERENCES_PROVIDER_KEY = "spring.datasource.driver-class-name";

    int DEFAULT_MAX_WORKFLOW_SELECTION_SCRIPT_SIZE = 8192;


    long getTenantIndexAvailable();


    int getUserDataGroupIndexAvailable();


    @NotNull Page<TenantRepresentation> listTenantsForUser(@NotNull String userId,
                                                           @NotNull Pageable pageable,
                                                           @Nullable String searchTerm,
                                                           boolean reverse,
                                                           boolean adminOnly,
                                                           boolean addFunctionalAdministeredTenants);


    @NotNull List<String> listAdminUsersIdForTenant(@NotNull TenantRepresentation tenant);


    @NotNull List<TypologyRepresentation> getTypologyHierarchy(@NotNull String tenantId, @NotNull Pageable pageable, boolean collapseAll,
                                                               @Nullable Set<String> reverseIdList, @Nullable String searchTerm);


    Integer getTypologyHierarchyListTotal(@NotNull String tenantId, boolean collapseAll, @Nullable Set<String> reverseIdList, @Nullable String searchTerm);


    @NotNull List<? extends Folder> getFoldersSortedByTypologyName(@NotNull Desk desk, @NotNull State state, @NotNull FolderSortBy sortBy,
                                                                   boolean asc, int page, int pageSize);


}
