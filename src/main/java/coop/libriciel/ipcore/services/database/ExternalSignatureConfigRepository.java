/*
 * iparapheur Core
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.ipcore.services.database;

import coop.libriciel.ipcore.model.database.ExternalSignatureConfig;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;


@Repository
public interface ExternalSignatureConfigRepository extends PagingAndSortingRepository<ExternalSignatureConfig, String>, JpaSpecificationExecutor<ExternalSignatureConfig> {

    @Query(value = "SELECT e FROM ExternalSignatureConfig e WHERE UPPER(e.name) LIKE UPPER(:searchTerm) AND e.tenant.id = :tenantId",
            countQuery = "SELECT COUNT(e) FROM ExternalSignatureConfig e WHERE UPPER(e.name) LIKE UPPER(:searchTerm) AND e.tenant.id = :tenantId")
    Page<ExternalSignatureConfig> findAllWithSearchTermAndTenant(@Param("tenantId") String tenantId,
                                                                 @Param("searchTerm") String wrappedSearchTerm,
                                                                 Pageable pageable);

    /* Auto-generated methods */

    long countAllByTenantId(String tenantId);

    Page<ExternalSignatureConfig> findAllByTenantId(String tenantId, Pageable pageable);

    Optional<ExternalSignatureConfig> findByTenant_IdAndId(String tenantId, String configId);
}
