/*
 * iparapheur Core
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package coop.libriciel.ipcore.services.database;

import coop.libriciel.ipcore.model.database.Metadata;
import coop.libriciel.ipcore.model.database.MetadataRepresentation;
import org.jetbrains.annotations.NotNull;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

import static org.apache.commons.lang3.StringUtils.isNotEmpty;


@Repository
public interface MetadataRepository extends PagingAndSortingRepository<Metadata, String> {


    /* Auto-generated methods */


    Optional<Metadata> findByIdAndTenant_Id(@NotNull String metadataId, String tenantId);


    @Query(value = "SELECT m FROM Metadata m WHERE m.id = :metadataId AND (m.tenant IS NULL OR m.tenant.id = :tenantId)")
    Optional<Metadata> findByIdAndTenant_IdOrTenantLess(@Param("metadataId") @NotNull String metadataId,
                                                        @Param("tenantId") String tenantId);


    @Query(value = "SELECT m FROM Metadata m WHERE m.id IN :metadataIds AND (m.tenant IS NULL OR m.tenant.id = :tenantId)")
    List<Metadata> findAllByIdAndTenant_IdOrTenantLess(@Param("metadataIds") @NotNull Collection<String> metadataIds,
                                                       @Param("tenantId") String tenantId);


    long countAllByTenant_Id(@NotNull String tenantId);


    default Page<MetadataRepresentation> findAll(String tenantId, String searchTerm, Pageable pageable, boolean addInternalMetadata) {
        if (isNotEmpty(searchTerm)) {
            return addInternalMetadata
                   ? findAllWithSearchTermWithInternalMetadata(tenantId, searchTerm, pageable)
                   : findAllWithSearchTermWithoutInternalMetadata(tenantId, searchTerm, pageable);
        } else {
            return addInternalMetadata
                   ? findAllByTenant_IdOrTenantIsNull(tenantId, pageable)
                   : findByTenant_Id(tenantId, pageable);
        }
    }

    Page<MetadataRepresentation> findByTenant_Id(@NotNull String tenantId, Pageable pageable);


    Page<MetadataRepresentation> findAllByTenant_IdOrTenantIsNull(@NotNull String tenantId, Pageable pageable);

    @Query(
            value =
                    """
                    SELECT m FROM Metadata m
                    WHERE (
                        UPPER(m.name) LIKE CONCAT('%', UPPER(:searchTerm), '%')
                        OR UPPER(m.key) LIKE CONCAT('%', UPPER(:searchTerm), '%')
                    )
                    AND m.tenant.id = :tenantId
                    """,
            countQuery =
                    """
                    SELECT COUNT(m) FROM Metadata m
                    WHERE (
                        UPPER(m.name) LIKE CONCAT('%', UPPER(:searchTerm), '%')
                        OR UPPER(m.key) LIKE CONCAT('%', UPPER(:searchTerm), '%')
                    )
                    AND m.tenant.id = :tenantId
                    """
    )
    Page<MetadataRepresentation> findAllWithSearchTermWithoutInternalMetadata(@Param("tenantId") String tenantId,
                                                                              @Param("searchTerm") String wrappedSearchTerm,
                                                                              Pageable pageable);

    @Query(
            value =
                    """
                    SELECT m FROM Metadata m
                    WHERE (
                        UPPER(m.name) LIKE CONCAT('%', UPPER(:searchTerm), '%')
                        OR UPPER(m.key) LIKE CONCAT('%', UPPER(:searchTerm), '%')
                    )
                    AND (m.tenant IS NULL OR m.tenant.id = :tenantId)
                    """,
            countQuery =
                    """
                    SELECT COUNT(m) FROM Metadata m
                    WHERE (
                        UPPER(m.name) LIKE CONCAT('%', UPPER(:searchTerm), '%')
                        OR UPPER(m.key) LIKE CONCAT('%', UPPER(:searchTerm), '%')
                    )
                    AND (m.tenant IS NULL OR m.tenant.id = :tenantId)
                    """
    )
    Page<MetadataRepresentation> findAllWithSearchTermWithInternalMetadata(@Param("tenantId") String tenantId,
                                                                           @Param("searchTerm") String wrappedSearchTerm,
                                                                           Pageable pageable);

    Page<MetadataRepresentation> findByKeyIn(@NotNull Collection<String> keys, Pageable pageable);


    Page<MetadataRepresentation> findAllByTenant_IdAndIdIn(@NotNull String tenantId, @NotNull Collection<String> ids, Pageable pageable);


    Page<MetadataRepresentation> findAllByTenantIdAndKeyIn(@NotNull String tenantId, Collection<String> keyList, Pageable pageable);


}
