/*
 * iparapheur Core
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.ipcore.services.database;

import coop.libriciel.ipcore.model.database.IdCount;
import coop.libriciel.ipcore.model.database.Subtype;
import coop.libriciel.ipcore.model.database.Tenant;
import org.jetbrains.annotations.NotNull;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.Set;


@Repository
public interface SubtypeRepository extends PagingAndSortingRepository<Subtype, String>, JpaSpecificationExecutor<Subtype> {


    /* Auto-generated methods */


    Optional<Subtype> findByIdAndTenant_Id(@NotNull String id, @NotNull String tenantId);


    Optional<Subtype> findByIdAndParentType_IdAndTenant_Id(@NotNull String id, @NotNull String typeId, @NotNull String tenantId);


    Page<Subtype> findAllByIdIn(@NotNull Collection<String> idList, @NotNull Pageable pageable);


    Page<Subtype> findAllByTenant_IdAndIdIn(@NotNull String tenantId, @NotNull Collection<String> idList, @NotNull Pageable pageable);


    Page<Subtype> findAllByTenant_IdAndParentType_Id(@NotNull String tenantId, @NotNull String typeId, @NotNull Pageable pageable);


    Page<Subtype> findAllByTenant_Id(@NotNull String tenantId, @NotNull Pageable pageable);


    Page<Subtype> findByTenant_IdAndSealCertificateId(@NotNull String tenantId, String certificateId, Pageable pageable);


    Page<Subtype> findAllByTenant_IdAndParentTypeName(@NotNull String tenantId, String parentName, Pageable pageable);


    Optional<Subtype> findAllByTenant_IdAndParentTypeNameAndName(@NotNull String tenantId, @NotNull String parentName, @NotNull String name);

    List<Subtype> findAllByTenant_IdAndValidationWorkflowId(@NotNull String tenantId, @NotNull String validationWorkflowId);
    List<Subtype> findAllByTenant_IdAndCreationWorkflowId(@NotNull String tenantId, @NotNull String creationWorkflowId);


    Page<Subtype> findAllByTenant_IdAndParentType_IdAndIdIn(@NotNull String tenantId, @NotNull String typeId, @NotNull Collection<String> subtypeIds,
                                                            @NotNull Pageable pageable);


    @Query(value = """
                   SELECT NEW coop.libriciel.ipcore.model.database.IdCount(st.sealCertificateId, COUNT(st))\s
                   FROM Subtype st\s
                   WHERE st.sealCertificateId IN :sealCertificateIdList\s
                   GROUP BY st.sealCertificateId
                   """
    )
    List<IdCount> fetchCountBySealCertificateIdIn(@Param("sealCertificateIdList") Set<String> sealCertificateIdList);


    default List<Subtype> getSubtypesReferencingWorkflowKey(@NotNull Tenant tenant, @NotNull String key) {
        List<Subtype> result = findAllByTenant_IdAndCreationWorkflowId(tenant.getId(), key);
        result.addAll(findAllByTenant_IdAndValidationWorkflowId(tenant.getId(), key));
        return result;
    }

}
