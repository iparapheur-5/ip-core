/*
 * iparapheur Core
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.ipcore.services.resolvers;

import coop.libriciel.ipcore.configuration.WebConfig;
import coop.libriciel.ipcore.model.auth.User;
import coop.libriciel.ipcore.model.database.Tenant;
import coop.libriciel.ipcore.services.auth.AuthServiceInterface;
import coop.libriciel.ipcore.utils.LocalizedStatusException;
import io.swagger.v3.oas.annotations.Parameter;
import lombok.extern.log4j.Log4j2;
import org.jetbrains.annotations.NotNull;
import org.springframework.core.MethodParameter;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

import javax.servlet.http.HttpServletRequest;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.Map;
import java.util.Optional;

import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;
import static org.springframework.http.HttpStatus.NOT_FOUND;
import static org.springframework.web.servlet.HandlerMapping.URI_TEMPLATE_VARIABLES_ATTRIBUTE;


/**
 * This is an utility class,
 * registering it in {@link WebConfig} will factorize the {@link User} retrieving in controller classes.
 */
@Log4j2
public class CurrentUserResolver implements HandlerMethodArgumentResolver {


    @Parameter(hidden = true)
    @Retention(RetentionPolicy.RUNTIME)
    @Target(ElementType.PARAMETER)
    public @interface CurrentUserResolved {}


    // <editor-fold desc="Beans">


    private final AuthServiceInterface authService;


    public CurrentUserResolver(@NotNull AuthServiceInterface authService) {
        this.authService = authService;
    }


    // </editor-fold desc="Beans">


    @Override
    public boolean supportsParameter(MethodParameter methodParameter) {
        return methodParameter.getParameterAnnotation(CurrentUserResolved.class) != null;
    }


    @Override
    public User resolveArgument(@NotNull MethodParameter methodParameter, ModelAndViewContainer modelAndViewContainer,
                                @NotNull NativeWebRequest nativeWebRequest, WebDataBinderFactory webDataBinderFactory) {
        // noinspection unchecked
        Map<String, String> variables = (Map<String, String>) Optional.of(nativeWebRequest)
                .map(r -> r.getNativeRequest(HttpServletRequest.class))
                .map(r -> r.getAttribute(URI_TEMPLATE_VARIABLES_ATTRIBUTE))
                .orElseThrow(() -> new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.cannot_read_request"));

        String tenantId = variables.get(Tenant.API_PATH);
        String userId = Optional.ofNullable(SecurityContextHolder.getContext().getAuthentication().getPrincipal())
                .map(Object::toString)
                .orElseThrow(() -> new LocalizedStatusException(NOT_FOUND, "message.unknown_user_id"));

        log.debug("resolveArgument tenantId:{} userId:{}", tenantId, userId);

        return Optional.ofNullable(authService.findUserById(userId))
                .orElseThrow(() -> new LocalizedStatusException(NOT_FOUND, "message.unknown_user_id"));
    }


}
