/*
 * iparapheur Core
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package coop.libriciel.ipcore.services.externalsignature;

import coop.libriciel.ipcore.model.auth.User;
import coop.libriciel.ipcore.model.content.Document;
import coop.libriciel.ipcore.model.content.DocumentBuffer;
import coop.libriciel.ipcore.model.database.ExternalSignatureConfig;
import coop.libriciel.ipcore.model.externalsignature.ExternalSignatureParams;
import coop.libriciel.ipcore.model.externalsignature.Status;
import coop.libriciel.ipcore.model.externalsignature.response.ExternalSignatureProcedure;
import coop.libriciel.ipcore.model.workflow.Folder;
import coop.libriciel.ipcore.model.workflow.Task;
import coop.libriciel.ipcore.services.database.ExternalSignatureConfigRepository;
import coop.libriciel.ipcore.utils.UserUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.NotNull;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static coop.libriciel.ipcore.model.externalsignature.Status.*;


public interface ExternalSignatureInterface {


    String BEAN_NAME = "externalSignature";
    String PREFERENCES_PROVIDER_KEY = "services.externalsignature.provider";

    default void checkIfFilesAreSigned(Logger log, ExternalSignatureConfigRepository externalSignatureConfigRepository) {

        log.debug("checkIfFilesAreSigned");
        User automaticUser = UserUtils.getAutomaticUser();

        StreamSupport.stream(externalSignatureConfigRepository.findAll().spliterator(), false)
                .filter(config -> CollectionUtils.isNotEmpty(config.getTransactionIds()))
                .forEach(config ->
                        Optional.ofNullable(config.getTransactionIds())
                                .orElse(Collections.emptySet())
                                .forEach(id -> {

                                    Status status = getProcedureStatus(config, id);
                                    if (status.equals(REFUSED)) {
                                        log.debug("Transaction refused : " + id);
                                        rejectFolder(config, automaticUser, id);
                                    } else if (status.equals(SIGNED)) {
                                        log.debug("Transaction signed : " + id);
                                        try {
                                            updateFilesAndPerformTask(config, automaticUser, id);
                                        } catch (Exception e) {
                                            log.debug(e);
                                        }
                                    }
                                    if (status.equals(SIGNED) || status.equals(ERROR) || status.equals(REFUSED)) {
                                        config.getTransactionIds().remove(id);
                                        externalSignatureConfigRepository.save(config);
                                    }
                                })
                );
    }

    @NotNull String createProcedure(@NotNull Folder folder,
                                    @NotNull ExternalSignatureParams externalSignatureParams,
                                    @NotNull List<Document> documents,
                                    @NotNull String subtypeId);

    void revokeExternalProcedure(@NotNull ExternalSignatureConfig externalSignatureConfig,
                                 @NotNull String procedureId);

    @NotNull Status getProcedureStatus(@NotNull ExternalSignatureConfig externalSignatureConfig,
                                       @NotNull String procedureId);

    ExternalSignatureProcedure getProcedureData(@NotNull ExternalSignatureConfig externalSignatureConfig,
                                                @NotNull String procedureId);

    List<DocumentBuffer> getSignedDocumentList(@NotNull List<String> fileIds,
                                               @NotNull ExternalSignatureConfig externalSignatureConfig,
                                               @NotNull String procedureId);

    void updateFilesAndPerformTask(@NotNull ExternalSignatureConfig externalSignatureConfig,
                                   @NotNull User user,
                                   @NotNull String procedureId) throws Exception;

    void rejectFolder(@NotNull ExternalSignatureConfig externalSignatureConfig,
                      @NotNull User user,
                      @NotNull String procedureId);

    @NotNull String getProcedureRejectionReason(@NotNull ExternalSignatureConfig externalSignatureConfig,
                                                @NotNull String procedureId);

    void rejectExternalProcedure(@NotNull Folder folder,
                                 @NotNull Task task);

    void testService(@NotNull ExternalSignatureConfig externalSignatureConfig);


}
