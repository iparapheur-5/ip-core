/*
 * iparapheur Core
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.ipcore.services.workflow;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import coop.libriciel.ipcore.model.auth.Desk;
import coop.libriciel.ipcore.model.auth.DeskRepresentation;
import coop.libriciel.ipcore.model.database.TypologyEntity;
import coop.libriciel.ipcore.model.database.userPreferences.FolderFilter;
import coop.libriciel.ipcore.model.database.userPreferences.FolderFilterParam;
import coop.libriciel.ipcore.model.ipng.IpngProof;
import coop.libriciel.ipcore.model.ipng.IpngProofWrap;
import coop.libriciel.ipcore.model.ipng.PendingIpngFolder;
import coop.libriciel.ipcore.model.workflow.*;
import coop.libriciel.ipcore.model.workflow.ipWorkflow.IpWorkflowEditFolder;
import coop.libriciel.ipcore.model.workflow.ipWorkflow.IpWorkflowInstance;
import coop.libriciel.ipcore.model.workflow.ipWorkflow.IpWorkflowTask;
import coop.libriciel.ipcore.model.workflow.requests.FilteredRequest;
import coop.libriciel.ipcore.model.workflow.requests.IntResponse;
import coop.libriciel.ipcore.model.workflow.requests.SimpleTaskParams;
import coop.libriciel.ipcore.services.auth.AuthServiceInterface;
import coop.libriciel.ipcore.services.ipng.PendingIpngFolderRepository;
import coop.libriciel.ipcore.utils.*;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.boot.configurationprocessor.json.JSONStringer;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientRequestException;
import org.springframework.web.util.UriComponentsBuilder;
import reactor.core.publisher.Mono;

import javax.annotation.PostConstruct;
import java.net.URI;
import java.net.URLEncoder;
import java.security.InvalidParameterException;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Consumer;
import java.util.function.Supplier;
import java.util.regex.Matcher;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static coop.libriciel.ipcore.model.workflow.Action.*;
import static coop.libriciel.ipcore.model.workflow.FolderSortBy.CREATION_DATE;
import static coop.libriciel.ipcore.model.workflow.FolderSortBy.LATE_DATE;
import static coop.libriciel.ipcore.model.workflow.State.DELEGATED;
import static coop.libriciel.ipcore.model.workflow.State.PENDING;
import static coop.libriciel.ipcore.model.workflow.WorkflowDefinition.*;
import static coop.libriciel.ipcore.utils.CollectionUtils.popValue;
import static coop.libriciel.ipcore.utils.CollectionUtils.safeAddKeyValue;
import static coop.libriciel.ipcore.utils.RequestUtils.SUBSERVICE_REQUEST_LONG_TIMEOUT;
import static coop.libriciel.ipcore.utils.RequestUtils.SUBSERVICE_REQUEST_SHORT_TIMEOUT;
import static coop.libriciel.ipcore.utils.TextUtils.INTERNAL_PREFIX;
import static java.lang.Integer.MAX_VALUE;
import static java.nio.charset.StandardCharsets.UTF_8;
import static java.util.Collections.emptyMap;
import static java.util.Comparator.*;
import static org.apache.commons.lang3.ObjectUtils.firstNonNull;
import static org.apache.commons.lang3.StringUtils.EMPTY;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.security.config.Elements.HTTP;


@Log4j2
@Service(WorkflowServiceInterface.BEAN_NAME)
@ConditionalOnProperty(name = WorkflowServiceInterface.PREFERENCES_PROVIDER_KEY, havingValue = "workflow")
public class IpWorkflowService implements WorkflowServiceInterface {


    public static final String METADATA_TRANSACTION_ID = INTERNAL_PREFIX + "transaction_id";
    public static final String METADATA_PASTELL_DOCUMENT_ID = INTERNAL_PREFIX + "pastell_document_id";
    public static final String METADATA_KEY_IPNG_RETURN_DESKBOX_ID = INTERNAL_PREFIX + "ipng_deskbox_profile_id";
    public static final String METADATA_KEY_IPNG_SOURCE_DESKBOX_ID = INTERNAL_PREFIX + "ipng_source_deskbox_id";

    public static final String METADATA_KEY_IPNG_RETURN_BUSINESS_ID = INTERNAL_PREFIX + "ipng_proof_id";
    public static final String METADATA_KEY_IPNG_RETURN_TYPE_ID = INTERNAL_PREFIX + "ipng_type_id";
    public static final String METADATA_KEY_IPNG_RETURN_METADATA_IDS = INTERNAL_PREFIX + "ipng_metadata_ids";
    public static final String IPNG_METADATA_KEYS_SEPARATOR = "|";
    public static final String METADATA_IPNG_BUSINESS_ID = INTERNAL_PREFIX + "ipng_business_id";
    public static final String METADATA_STATUS = INTERNAL_PREFIX + "status";
    public static final String METADATA_PUBLIC_ANNOTATION = INTERNAL_PREFIX + "public_annotation";
    public static final String METADATA_PRIVATE_ANNOTATION = INTERNAL_PREFIX + "private_annotation";
    public static final String METADATA_DRAFT_CREATION_DATE = INTERNAL_PREFIX + "creation_date";
    public static final String METADATA_TARGET_DELEGATE = "workflow_internal_delegate_candidate_groups";
    public static final String METADATA_PUBLIC_CERTIFICATE_BASE64 = INTERNAL_PREFIX + "public_certificate_base64";
    public static final String METADATA_ORIGIN_GROUP_ID = "workflow_internal_origin_group_id";
    public static final String METADATA_FINAL_GROUP_ID = "workflow_internal_final_group_id";

    private static final String DEFINITIONS_URL = "workflow/definitions";
    private static final String DEFINITIONS_BY_KEY_SUBPATH = "byKey";
    private static final String GROUP_URL = "workflow/group";
    private static final String ARCHIVE_URL = "workflow/archive";
    private static final String INSTANCE_URL = "workflow/instance";
    private static final String TASK_URL = "workflow/task";
    private static final String HISTORY_TASKS = "historyTasks";
    private static final String READ_TASKS = "readTasks";
    private static final String COUNT_PATH = "count";
    private static final String WORKFLOW_FOLDER_KEY = INTERNAL_PREFIX + "folder";

    /**
     * Workflow cannot handle real UUIDs on its internal model.
     * NCNames should start with a letter, which is not guaranteed on UUID.
     */
    private static final Supplier<String> NC_NAME_UUID_GENERATOR = () -> "uuid-%s".formatted(UUID.randomUUID().toString());


    // <editor-fold desc="Beans">


    private final AuthServiceInterface authService;
    private final ObjectMapper objectMapper;
    private final ModelMapper modelMapper;
    private final PendingIpngFolderRepository pendingIpngFolderRepository;
    private final WorkflowServiceProperties properties;


    @Autowired
    public IpWorkflowService(AuthServiceInterface authService,
                             ObjectMapper objectMapper,
                             ModelMapper modelMapper,
                             PendingIpngFolderRepository pendingIpngFolderRepository,
                             WorkflowServiceProperties properties) {
        this.authService = authService;
        this.objectMapper = objectMapper;
        this.modelMapper = modelMapper;
        this.pendingIpngFolderRepository = pendingIpngFolderRepository;
        this.properties = properties;
    }


    // </editor-fold desc="Beans">


    private static String toIpWorkflowValue(@Nullable FolderSortBy sortBy) {

        return switch (firstNonNull(sortBy, CREATION_DATE)) {
            case FOLDER_NAME -> "INSTANCE_NAME";
            case FOLDER_ID -> "INSTANCE_ID";
            case TYPE_ID -> META_TYPE_ID;
            case SUBTYPE_ID -> META_SUBTYPE_ID;
            case TYPE_NAME, SUBTYPE_NAME -> throw new InvalidParameterException("IpWorkflow can't sort by type/subtype name");
            case LATE_DATE -> LATE_DATE.toString();
            default -> CREATION_DATE.toString();
        };
    }


    @PostConstruct
    public void init() {

        URI requestUri = UriComponentsBuilder.newInstance()
                .scheme(HTTP).host(properties.getHost()).port(properties.getPort())
                .path(DEFINITIONS_URL).pathSegment("i-parapheur")
                .build().normalize().toUri();

        WebClient.builder().clientConnector(new ForcedExceptionsConnector()).build()
                .post().uri(requestUri)
                .retrieve();
    }


    // <editor-fold desc="Workflow definition CRUDL">


    @Override
    public void createWorkflowDefinition(@NotNull String tenantId, @NotNull String workflowDefinition, @NotNull String workflowName) {

        URI requestUri = UriComponentsBuilder.newInstance()
                .scheme(HTTP).host(properties.getHost()).port(properties.getPort())
                .path(DEFINITIONS_URL)
                .queryParam("tenantId", tenantId)
                .build().normalize().toUri();

        JSONStringer stringer;
        try {
            stringer = new JSONStringer()
                    .object()
                    /**/.key("workflowDefinitionBase64").value(Base64.getEncoder().encodeToString(workflowDefinition.getBytes()))
                    /**/.key("workflowName").value(workflowName + (workflowName.endsWith(".bpmn20.xml") ? EMPTY : ".bpmn20.xml"))
                    /**/.key("isBpmnPatchNeeded").value(true)
                    .endObject();
        } catch (JSONException e) {
            throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, e, "message.error_writing_request");
        }

        WorkflowDefinition result = WebClient.builder().clientConnector(new ForcedExceptionsConnector()).build()
                .post().uri(requestUri)
                .contentType(APPLICATION_JSON).accept(APPLICATION_JSON)
                .bodyValue(stringer.toString())
                .retrieve()
                // Result
                .bodyToMono(WorkflowDefinition.class)
                .blockOptional(SUBSERVICE_REQUEST_SHORT_TIMEOUT)
                .orElseThrow(() -> new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.cannot_reach_workflow_service"));

        log.info("Created definition response:" + result);
    }


    @Override
    public void createWorkflowDefinition(@NotNull String tenantId, @NotNull WorkflowDefinition workflowDefinition) {

        substitutePlaceholdersToIndexedPlaceholders(workflowDefinition);

        WorkflowDefinitionDto ipWorkflowDefinition = modelMapper.map(workflowDefinition, WorkflowDefinitionDto.class);
        ipWorkflowDefinition.getSteps().forEach(step -> step.setId(NC_NAME_UUID_GENERATOR.get()));

        URI requestUri = UriComponentsBuilder.newInstance()
                .scheme(HTTP).host(properties.getHost()).port(properties.getPort())
                .path(DEFINITIONS_URL)
                .queryParam("tenantId", tenantId)
                .build().normalize().toUri();

        WorkflowDefinition result = WebClient.builder().clientConnector(new ForcedExceptionsConnector()).build()
                .post().uri(requestUri)
                .contentType(APPLICATION_JSON).accept(APPLICATION_JSON)
                .bodyValue(ipWorkflowDefinition)
                .retrieve()
                // Result
                .bodyToMono(WorkflowDefinition.class)
                .blockOptional(SUBSERVICE_REQUEST_SHORT_TIMEOUT)
                .orElseThrow(() -> new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.cannot_reach_workflow_service"));

        log.info("Created definition response:" + result);
    }


    @Override
    public @Nullable WorkflowDefinition getWorkflowDefinitionById(@NotNull String tenantId, @NotNull String id) {

        UriComponentsBuilder requestUriBuilder = UriComponentsBuilder.newInstance()
                .scheme(HTTP).host(properties.getHost()).port(properties.getPort())
                .path(DEFINITIONS_URL)
                .pathSegment(id)
                .queryParam("tenantId", tenantId);

        WorkflowDefinition result = WebClient.builder().clientConnector(new ForcedExceptionsConnector()).build()
                .get().uri(requestUriBuilder.build().encode().normalize().toUri())
                .accept(APPLICATION_JSON)
                .retrieve()
                .bodyToMono(WorkflowDefinitionDto.class)
                .blockOptional(SUBSERVICE_REQUEST_SHORT_TIMEOUT)
                .map(dto -> modelMapper.map(dto, WorkflowDefinition.class))
                .orElseThrow(() -> new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.cannot_reach_workflow_service"));

        log.debug("getWorkflowDefinitionById {}", result);
        return result;
    }


    @Override
    public @NotNull Optional<WorkflowDefinition> getWorkflowDefinitionByKey(@NotNull String tenantId,
                                                                            @NotNull String key,
                                                                            @Nullable String originDeskId) {

        UriComponentsBuilder requestUriBuilder = UriComponentsBuilder.newInstance()
                .scheme(HTTP).host(properties.getHost()).port(properties.getPort())
                .path(DEFINITIONS_URL)
                .pathSegment(DEFINITIONS_BY_KEY_SUBPATH)
                .pathSegment(key)
                .queryParam("tenantId", tenantId);

        Optional<WorkflowDefinition> result = WebClient.builder().clientConnector(new ForcedExceptionsConnector()).build()
                .get().uri(requestUriBuilder.build().encode().normalize().toUri())
                .accept(APPLICATION_JSON)
                .retrieve()
                .bodyToMono(WorkflowDefinitionDto.class)
                .onErrorResume(WebClientRequestException.class, e -> Mono.empty())
                // .onErrorMap(Error.class, e -> new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.cannot_reach_workflow_service", e))
                .blockOptional(SUBSERVICE_REQUEST_SHORT_TIMEOUT)
                .map(dto -> modelMapper.map(dto, WorkflowDefinition.class));

        log.debug("getWorkflowDefinitionByKey result:{}", result);
        // FIXME : In any other non-404 error, we should re-throw an error instead of returning an empty object
        if (result.isEmpty()) {
            return result;
        }

        // Computing placeholders, if possible
        if (StringUtils.isNotEmpty(originDeskId)) {
            substitutePlaceholdersToIndexedPlaceholders(result.get());
            Map<String, String> placeholdersMapping = computePlaceholderDesksConcreteValues(result.get(), originDeskId, emptyMap());
            result.get().getSteps().stream()
                    .flatMap(s -> s.getValidatingDesks().stream())
                    .forEach(d -> mapIndexedPlaceholdersToActualDesk(d, originDeskId, placeholdersMapping));

            mapIndexedPlaceholdersToActualDesk(result.get().getFinalDesk(), originDeskId, placeholdersMapping);
        }

        log.debug("getWorkflowDefinitionByKey {}", result);
        return result;
    }


    @Override
    public void updateWorkflowDefinition(@NotNull String tenantId, @NotNull String fullId, @NotNull WorkflowDefinition workflowDefinition) {
        log.debug("updateWorkflowDefinition id:{} def:{}", fullId, workflowDefinition);

        substitutePlaceholdersToIndexedPlaceholders(workflowDefinition);

        WorkflowDefinitionDto ipWorkflowDefinition = modelMapper.map(workflowDefinition, WorkflowDefinitionDto.class);
        ipWorkflowDefinition.getSteps().forEach(step -> step.setId(NC_NAME_UUID_GENERATOR.get()));

        URI requestUri = UriComponentsBuilder.newInstance()
                .scheme(HTTP).host(properties.getHost()).port(properties.getPort())
                .path(DEFINITIONS_URL)
                .pathSegment(fullId)
                .queryParam("tenantId", tenantId)
                .build().encode().normalize().toUri();

        WorkflowDefinition result = WebClient.builder().clientConnector(new ForcedExceptionsConnector()).build()
                .put().uri(requestUri)
                .contentType(APPLICATION_JSON).accept(APPLICATION_JSON)
                .bodyValue(ipWorkflowDefinition)
                .retrieve()
                // Result
                .bodyToMono(WorkflowDefinition.class)
                .blockOptional(SUBSERVICE_REQUEST_SHORT_TIMEOUT)
                .orElseThrow(() -> new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.cannot_reach_workflow_service"));

        log.info("Update definition response:" + result);
    }


    @Override
    public @NotNull Page<WorkflowDefinitionRepresentation> listWorkflowDefinitions(@NotNull String tenantId,
                                                                                   @NotNull Pageable pageable,
                                                                                   @Nullable String searchTerm) {

        log.trace("getWorkflowDefinitions");
        String urlEncodedSearchTerm = Optional.ofNullable(searchTerm)
                .filter(StringUtils::isNotEmpty)
                .map(value -> URLEncoder.encode(value, UTF_8))
                .orElse(null);

        long page = (pageable != Pageable.unpaged()) ? pageable.getPageNumber() : 0L;
        long pageSize = (pageable != Pageable.unpaged()) ? pageable.getPageSize() : MAX_VALUE;

        String sortBy = pageable.getSort().stream().findFirst()
                .map(Sort.Order::getProperty)
                .orElse(WorkflowDefinitionSortBy.NAME.name());

        boolean asc = pageable.getSort().stream().findFirst()
                .map(Sort.Order::getDirection)
                .map(Sort.Direction::isAscending)
                .orElse(true);

        log.debug("getWorkflowDefinitions - Url encoded searchTerm : {}", urlEncodedSearchTerm);

        UriComponentsBuilder requestUriBuilder = UriComponentsBuilder.newInstance()
                .scheme(HTTP).host(properties.getHost()).port(properties.getPort())
                .path(DEFINITIONS_URL)
                .queryParam("page", page)
                .queryParam("pageSize", pageSize)
                .queryParam("sortBy", sortBy)
                .queryParam("asc", asc)
                .queryParam("tenantId", tenantId)
                .queryParam("searchTerm", urlEncodedSearchTerm);

        PaginatedList<WorkflowDefinitionRepresentation> result = WebClient.builder().clientConnector(new ForcedExceptionsConnector()).build()
                .get().uri(requestUriBuilder.build().normalize().toUri())
                .accept(APPLICATION_JSON)
                .retrieve()
                .bodyToMono(new ParameterizedTypeReference<PaginatedList<WorkflowDefinitionRepresentation>>() {})
                .blockOptional(SUBSERVICE_REQUEST_SHORT_TIMEOUT)
                .orElseThrow(() -> new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.cannot_reach_workflow_service"));

        log.debug("getWorkflowDefinitions {}", result);

        // Filtering internal workflows

        return new PageImpl<>(result.getData(), pageable, result.getTotal());
    }


    // </editor-fold desc="Workflow definition CRUDL">


    // <editor-fold desc="Folder CRUDL">


    @Override
    public Folder getFolder(@NotNull String id, @NotNull String tenantId) {
        return getFolder(id, tenantId, true);
    }


    // TODO add @NotNull to getFolder function
    @Override
    public Folder getFolder(@NotNull String id, @NotNull String tenantId, boolean withHistory) {
        URI requestUri = UriComponentsBuilder.newInstance()
                .scheme(HTTP).host(properties.getHost()).port(properties.getPort())
                .path(INSTANCE_URL).pathSegment(id)
                .queryParam("withHistory", withHistory)
                .queryParam("tenantId", tenantId)
                .build().normalize().toUri();

        WebClient webClient = RequestUtils.getWebClientWithBufferSize(16, true);

        Folder folder = webClient
                .get().uri(requestUri)
                .accept(APPLICATION_JSON)
                .retrieve()
                // Result
                .bodyToMono(IpWorkflowInstance.class)
                .blockOptional(SUBSERVICE_REQUEST_SHORT_TIMEOUT)
                .orElseThrow(() -> new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.cannot_reach_workflow_service"));

        log.debug("getFolder metadata {}", folder.getMetadata());

        folder.getStepList().stream()
                .filter(t -> t.getAction() != START)
                .flatMap(t -> t.getDesks().stream())
                .forEach(d -> mapIndexedPlaceholdersToActualDesk(d, folder.getOriginDesk().getId(), folder.getMetadata()));

        return folder;
    }


    @Override
    public void deleteWorkflowDefinition(@NotNull String workflowId) {

        URI requestUri = UriComponentsBuilder.newInstance()
                .scheme(HTTP).host(properties.getHost()).port(properties.getPort())
                .path(DEFINITIONS_URL).pathSegment(workflowId)
                .build().encode().normalize().toUri();

        WebClient.builder().clientConnector(new ForcedExceptionsConnector()).build()
                .delete().uri(requestUri)
                .exchange()
                .doOnError(e -> {throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, e, "message.cannot_reach_workflow_service");})
                .block(SUBSERVICE_REQUEST_SHORT_TIMEOUT);
    }


    @Override
    public @NotNull PaginatedList<? extends Folder> listFoldersByState(@NotNull String role,
                                                                       @NotNull State state,
                                                                       @NotNull List<DelegationRule> delegationRules,
                                                                       @Nullable FolderSortBy sortBy,
                                                                       @Nullable FolderFilter folderFilter,
                                                                       boolean asc,
                                                                       int page,
                                                                       int pageSize) {

        log.debug("listFoldersByState role:{} state:{}", role, state);

        // The original CURRENT state use to concatenate CURRENT and DELEGATED folders.
        // Due to a specs re-planification, we don't want the DELEGATED folders anymore,
        // and we had to create a specific DELEGATED state, that does not match the CURRENT.
        // TODO : Find a proper way to request Workflow.
        //  The current state of the art is kinda weird anyway: we have some data in a path, and some in the body.
        //  Maybe we should manage everything through the body object, that would contain (or not) the current desk.
        String finalRole = (state == DELEGATED) ? "non-existing-id" : role;

        // Workflow is a permission-free zone, it does not know anything.
        // DELEGATED and PENDING are equal up there.
        State finalState = (state == DELEGATED) ? PENDING : state;

        URI requestUri = UriComponentsBuilder.newInstance()
                .scheme(HTTP).host(properties.getHost()).port(properties.getPort())
                .path(GROUP_URL).pathSegment(finalRole).pathSegment(finalState.toString().toLowerCase())
                .queryParam("sortBy", toIpWorkflowValue(sortBy))
                .queryParam("asc", asc)
                .queryParam("page", page)
                .queryParam("pageSize", pageSize)
                .build().normalize().toUri();

        PaginatedList<IpWorkflowTask> result;

        FilteredRequest body = new FilteredRequest();
        if (state == DELEGATED) {
            body.setFilteringParameters(delegationRules
                    .stream()
                    // DELEGATED is actually a PENDING state in Workflow,
                    .map(d -> new FilteringParameterResultCount(d.getDelegatingGroup(), PENDING, d.getMetadataKey(), d.getMetadataValue(), null))
                    .toList());
        }

        if (Objects.nonNull(folderFilter)) {
            FolderFilterParam folderFilterParam = modelMapper.map(folderFilter, FolderFilterParam.class);
            body.setFolderFilter(folderFilterParam);
        }

        WebClient webclient = RequestUtils.getWebClientWithBufferSize(16, true);

        result = webclient
                .post().uri(requestUri)
                .contentType(APPLICATION_JSON).accept(APPLICATION_JSON)
                .bodyValue(body)
                .retrieve()
                // Result
                .bodyToMono(new ParameterizedTypeReference<PaginatedList<IpWorkflowTask>>() {})
                .blockOptional(SUBSERVICE_REQUEST_SHORT_TIMEOUT)
                .orElseThrow(() -> new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.cannot_reach_workflow_service"));

        return new PaginatedList<>(
                result.getData().stream().map(IpWorkflowInstance::new).toList(),
                page,
                pageSize,
                result.getTotal()
        );
    }


    @Override
    public @NotNull Map<DelegationRule, Integer> countFolders(@NotNull Set<String> roles, @NotNull List<DelegationRule> delegationRules) {
        log.trace("countFolders roles:{} delegationRules:{}", roles, delegationRules);

        // Building body

        List<FilteringParameterResultCount> groupCounts = new ArrayList<>();
        roles.stream()
                .map(r -> new FilteringParameterResultCount(r, null, null, null, null))
                .forEach(groupCounts::add);

        delegationRules.stream()
                .map(d -> new FilteringParameterResultCount(d.getDelegatingGroup(), PENDING, d.getMetadataKey(), d.getMetadataValue(), null))
                .forEach(groupCounts::add);

        FilteredRequest body = FilteredRequest
                .builder()
                .filteringParameters(groupCounts)
                .build();

        // Request

        URI requestUri = UriComponentsBuilder.newInstance()
                .scheme(HTTP).host(properties.getHost()).port(properties.getPort())
                .path(GROUP_URL)
                .build().normalize().toUri();

        List<FilteringParameterResultCount> requestResult = WebClient.builder().clientConnector(new ForcedExceptionsConnector()).build()
                .post().uri(requestUri)
                .contentType(APPLICATION_JSON).accept(APPLICATION_JSON)
                .bodyValue(body)
                .retrieve()
                // Result
                .bodyToMono(new ParameterizedTypeReference<List<FilteringParameterResultCount>>() {})
                .blockOptional(SUBSERVICE_REQUEST_SHORT_TIMEOUT)
                .orElseThrow(() -> new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.cannot_reach_workflow_service"));

        Map<DelegationRule, Integer> resultMap = new HashMap<>();
        requestResult.forEach(f -> resultMap.put(
                new DelegationRule(f.getGroupId(), f.getState(), f.getFilterMetadataKey(), f.getFilterMetadataValue()),
                f.getCount())
        );

        log.trace("countFolders call result {}", resultMap);
        return resultMap;
    }


    @Override
    public @NotNull Integer countFolders(@NotNull String role, @NotNull State state) {

        URI requestUri = UriComponentsBuilder.newInstance()
                .scheme(HTTP).host(properties.getHost()).port(properties.getPort())
                .path(GROUP_URL).pathSegment(role).pathSegment(COUNT_PATH).path(state.toString())
                .build().normalize().toUri();

        log.debug("countFolders URI:{}", requestUri);

        IntResponse result = WebClient.builder().clientConnector(new ForcedExceptionsConnector()).build()
                .get().uri(requestUri)
                .accept(APPLICATION_JSON)
                .retrieve()
                // Result
                .bodyToMono(IntResponse.class)
                .blockOptional(SUBSERVICE_REQUEST_SHORT_TIMEOUT)
                .orElseThrow(() -> new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.cannot_reach_workflow_service"));

        log.debug("countFolders role:{} state:{} result:{}", role, state, result);
        return result.getResult();
    }


    @Override
    public @NotNull PaginatedList<? extends Folder> listFoldersForDesks(@NotNull String tenantId,
                                                                        int page,
                                                                        int pageSize,
                                                                        FolderSortBy sortBy,
                                                                        @Nullable FolderFilter folderFilter,
                                                                        boolean asc,
                                                                        @NotNull List<String> deskIds,
                                                                        @Nullable String searchTerm,
                                                                        @Nullable Long emitBeforeTime,
                                                                        @Nullable Long stillSinceTime,
                                                                        @Nullable State state) {

        log.debug("listFoldersForDesks deskIds:{}, searchTerm : {}", deskIds, searchTerm);
        String urlEncodedSearchTerm = Optional.ofNullable(searchTerm)
                .filter(StringUtils::isNotEmpty)
                .map(value -> URLEncoder.encode(value, UTF_8))
                .orElse(null);

        UriComponentsBuilder builder = UriComponentsBuilder.newInstance()
                .scheme(HTTP).host(properties.getHost()).port(properties.getPort())
                .path(INSTANCE_URL).pathSegment("search")
                .queryParam("tenantId", tenantId)
                .queryParam("page", page)
                .queryParam("pageSize", pageSize)
                .queryParam("sortBy", toIpWorkflowValue(sortBy))
                .queryParam("asc", asc);

        JSONStringer stringer;
        try {
            stringer = new JSONStringer()
                    .object()
                    /**/.key("searchTerm").value(urlEncodedSearchTerm)
                    /**/.key("emitBeforeTime").value(emitBeforeTime)
                    /**/.key("stillSinceTime").value(stillSinceTime)
                    /**/.key("state").value(Optional.ofNullable(state).map(State::name).orElse(null))
                    /**/.key("groupIds").array();
            for (String id : deskIds) {
                stringer.value(id);
            }
            stringer.endArray();

            if (Objects.nonNull(folderFilter)) {
                stringer
                        /**/.key("typeId").value(Optional.ofNullable(folderFilter.getType()).map(TypologyEntity::getId).orElse(null))
                        /**/.key("subtypeId").value(Optional.ofNullable(folderFilter.getSubtype()).map(TypologyEntity::getId).orElse(null))
                        /**/.key("legacyId").value(folderFilter.getLegacyId());
            }
            stringer.endObject();

        } catch (JSONException e) {
            throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, e, "message.error_writing_request");
        }


        URI requestUri = builder.build().normalize().toUri();

        WebClient webclient = RequestUtils.getWebClientWithBufferSize(16, true);

        PaginatedList<IpWorkflowTask> taskPaginatedList = webclient
                .post().uri(requestUri)
                .contentType(APPLICATION_JSON)
                .accept(APPLICATION_JSON)
                .bodyValue(stringer.toString())
                .retrieve()
                // Result
                .bodyToMono(new ParameterizedTypeReference<PaginatedList<IpWorkflowTask>>() {})
                .blockOptional(SUBSERVICE_REQUEST_SHORT_TIMEOUT)
                .orElseThrow(() -> new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.cannot_reach_workflow_service"));

        log.debug("listFoldersForDesks taskPaginatedList:{}", taskPaginatedList);

        // Re-wrap retrieved tasks into Instances.
        return new PaginatedList<>(
                taskPaginatedList.getData().stream().map(IpWorkflowInstance::new).toList(),
                page,
                pageSize,
                taskPaginatedList.getTotal()
        );
    }


    @Override
    public @NotNull PaginatedList<? extends Folder> listFolders(@NotNull String tenantId, int page, int pageSize, FolderSortBy sortBy,
                                                                @Nullable FolderFilter folderFilter, boolean asc,
                                                                @Nullable String deskId, @Nullable String searchTerm, @Nullable Long emitBeforeTime,
                                                                @Nullable Long stillSinceTime, @Nullable State state) {

        String urlEncodedSearchTerm = Optional.ofNullable(searchTerm)
                .filter(StringUtils::isNotEmpty)
                .map(value -> URLEncoder.encode(value, UTF_8))
                .orElse(null);

        UriComponentsBuilder builder = UriComponentsBuilder.newInstance()
                .scheme(HTTP).host(properties.getHost()).port(properties.getPort())
                .path(INSTANCE_URL)
                .queryParam("tenantId", tenantId)
                .queryParam("page", page)
                .queryParam("pageSize", pageSize)
                .queryParam("sortBy", toIpWorkflowValue(sortBy))
                .queryParam("asc", asc)
                .queryParam("groupId", Optional.ofNullable(deskId).filter(StringUtils::isNotEmpty).orElse(null))
                .queryParam("searchTerm", urlEncodedSearchTerm)
                .queryParam("emitBeforeTime", emitBeforeTime)
                .queryParam("stillSinceTime", stillSinceTime)
                .queryParam("state", Optional.ofNullable(state).map(State::name).orElse(null));

        if (Objects.nonNull(folderFilter)) {
            builder.queryParam("typeId", Optional.ofNullable(folderFilter.getType()).map(TypologyEntity::getId).orElse(null))
                    .queryParam("subtypeId", Optional.ofNullable(folderFilter.getSubtype()).map(TypologyEntity::getId).orElse(null))
                    .queryParam("legacyId", folderFilter.getLegacyId());
        }

        URI requestUri = builder.build().normalize().toUri();

        WebClient webclient = RequestUtils.getWebClientWithBufferSize(16, true);

        PaginatedList<IpWorkflowTask> taskPaginatedList = webclient
                .get().uri(requestUri)
                .accept(APPLICATION_JSON)
                .retrieve()
                // Result
                .bodyToMono(new ParameterizedTypeReference<PaginatedList<IpWorkflowTask>>() {})
                .blockOptional(SUBSERVICE_REQUEST_SHORT_TIMEOUT)
                .orElseThrow(() -> new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.cannot_reach_workflow_service"));

        log.debug("listFolders taskPaginatedList:{}", taskPaginatedList);

        // Re-wrap retrieved tasks into Instances.
        return new PaginatedList<>(
                taskPaginatedList.getData().stream().map(IpWorkflowInstance::new).toList(),
                page,
                pageSize,
                taskPaginatedList.getTotal()
        );
    }


    @Override
    public @NotNull Folder createDraftWorkflow(@NotNull String tenantId, @NotNull Folder folder, Map<Integer, String> variableDesksIds) {

        if (folder.getMetadata().entrySet().stream().anyMatch(meta -> meta.getKey().startsWith(INTERNAL_PREFIX))) {
            throw new LocalizedStatusException(BAD_REQUEST, "message.metadata_starting_with_p_are_reserved", INTERNAL_PREFIX);
        }

        URI requestUri = UriComponentsBuilder.newInstance()
                .scheme(HTTP).host(properties.getHost()).port(properties.getPort())
                .path(INSTANCE_URL)
                .build().normalize().toUri();

        WorkflowDefinition validationWorkflowDefinition = getWorkflowDefinitionByKey(tenantId, folder.getValidationWorkflowDefinitionKey(), null)
                .orElseThrow(() -> new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.unknown_workflow_definition_id"));

        Map<String, String> additionalMetadata = computePlaceholderDesksConcreteValues(
                validationWorkflowDefinition,
                folder.getOriginDesk().getId(),
                variableDesksIds
        );
        folder.getMetadata().putAll(additionalMetadata);

        folder.setFinalDesk(Optional.ofNullable(validationWorkflowDefinition.getFinalDesk())
                .orElse(folder.getOriginDesk())
        );
        mapIndexedPlaceholdersToActualDesk(folder.getFinalDesk(), folder.getOriginDesk().getId(), additionalMetadata);

        JSONStringer stringer;
        try {
            stringer = new JSONStringer()
                    .object()
                    /**/.key("tenantId").value(tenantId)
                    /**/.key("businessKey").value(folder.getContentId())
                    /**/.key("dueDate").value(Optional.ofNullable(folder.getDueDate()).map(Date::getTime).orElse(null))
                    /**/.key("deploymentKey").value(WORKFLOW_FOLDER_KEY)
                    /**/.key("name").value(folder.getName())
                    /**/.key("visibility").value(folder.getVisibility())
                    /**/.key("originGroup").value(folder.getOriginDesk().getId())
                    /**/.key("finalGroup").value(folder.getFinalDesk().getId())
                    /**/.key("creationWorkflowId").value(folder.getCreationWorkflowDefinitionKey())
                    /**/.key("validationWorkflowId").value(folder.getValidationWorkflowDefinitionKey())
                    /**/.key("variables").object();
            folder.getMetadata()
                    /**//**/.forEach((key, value) -> safeAddKeyValue(stringer, key, value));
            stringer/**//**/.key(META_TYPE_ID).value(Optional.ofNullable(folder.getType()).map(TypologyEntity::getId).orElse(null))
                    /**//**/.key(META_SUBTYPE_ID).value(Optional.ofNullable(folder.getSubtype()).map(TypologyEntity::getId).orElse(null))
                    /**//**/.key(META_LEGACY_ID).value(folder.getLegacyId())
                    /**/.endObject()
                    .endObject();

        } catch (JSONException e) {
            throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, e, "message.error_writing_request");
        }

        log.debug("instantiate workflow : {}", stringer.toString());

        Folder folderResult = WebClient.builder().clientConnector(new ForcedExceptionsConnector()).build()
                .post().uri(requestUri)
                .contentType(APPLICATION_JSON).accept(APPLICATION_JSON)
                .bodyValue(stringer.toString())
                .retrieve()
                // Result
                .bodyToMono(IpWorkflowInstance.class)
                .blockOptional(SUBSERVICE_REQUEST_SHORT_TIMEOUT)
                .orElseThrow(() -> new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.cannot_reach_workflow_service"));

        log.debug("createDraftWorkflow call result {}", folderResult);
        folder.getStepList().forEach(task -> {
            task.setPublicAnnotation(popValue(task.getMetadata(), METADATA_PUBLIC_ANNOTATION));
            task.setPrivateAnnotation(popValue(task.getMetadata(), METADATA_PRIVATE_ANNOTATION));
        });

        return folderResult;
    }


    // </editor-fold desc="Folder CRUDL">


    public void substitutePlaceholdersToIndexedPlaceholders(@NotNull WorkflowDefinition workflowDefinition) {

        // The real list, in streamed elements,
        // including an atomic-getter, to keep trace of the current step index.
        // We want to keep this streamed.

        AtomicInteger stepIdx = new AtomicInteger(-1);
        Stream<DeskRepresentation> concernedDesksStream = workflowDefinition.getSteps().stream()
                .peek(s -> stepIdx.getAndIncrement())
                .map(StepDefinition::getValidatingDesks)
                .flatMap(Collection::stream);

        concernedDesksStream = Stream.concat(
                concernedDesksStream,
                Stream.of(workflowDefinition.getFinalDesk())
                        .peek(s -> stepIdx.getAndIncrement())
        );

        // BOSS_OF/VARIABLE_DESKS are not permitted in the notified desks.
        // we can simply loop through those, and simply append these to the global stream...
        // It may seems useless, but we still want to evaluate a notified emitter.

        List<DeskRepresentation> notifiedDesks = workflowDefinition.getSteps().stream()
                .map(StepDefinition::getNotifiedDesks)
                .flatMap(Collection::stream)
                .toList();

        concernedDesksStream = Stream.concat(
                concernedDesksStream,
                notifiedDesks.stream()
        );

        concernedDesksStream = Stream.concat(
                concernedDesksStream,
                workflowDefinition.getFinalNotifiedDesks().stream()
        );

        // Loop through everything, and compute values.
        // This is where the index-counter will start to increase

        concernedDesksStream.forEach(entity -> {
            switch (entity.getId()) {
                case EMITTER_ID_PLACEHOLDER -> entity.setId(String.format("${%s}", META_EMITTER_ID));
                case VARIABLE_DESK_ID_PLACEHOLDER -> {
                    String varDeskMetadataKey = String.format(META_VARIABLE_DESK_DESKID_X_FORMAT, stepIdx.get());
                    entity.setId(String.format("${%s}", varDeskMetadataKey));
                }
                case BOSS_OF_ID_PLACEHOLDER -> {
                    String bossOfMetadataKey = String.format(META_BOSS_OF_DESKID_X_FORMAT, stepIdx.get());
                    entity.setId(String.format("${%s}", bossOfMetadataKey));
                }
            }
        });
    }


    public @NotNull Map<String, String> computePlaceholderDesksConcreteValues(@NotNull WorkflowDefinition validationWorkflowDefinition,
                                                                              @NotNull String originDeskId,
                                                                              @NotNull Map<Integer, String> variableDesksValues) {

        Map<String, String> result = new HashMap<>();
        result.put(META_EMITTER_ID, originDeskId);

        // We probably need an additional mechanic to handle the creation workflow,
        // As for now the metadata will be the same for both workflow
        // whereas the variable desks metadata may differ

//        if (folder.getCreationWorkflowDefinitionKey() != null) {
//            try {
//                WorkflowDefinition creationWorkflowDefinition = this.getWorkflowDefinitionByKey(tenantId, folder.getCreationWorkflowDefinitionKey());
//                Map<String, String> additionalMetadata = getMetadataForPlaceholders(
//                        folder,
//                        creationWorkflowDefinition,
//                        ...,
//                        ...
//                );
//                folder.getMetadata().putAll(additionalMetadata);
//            } catch (Exception e) {
//                log.error("Didn't find the creation workflow");
//            }
//        }

        AtomicInteger stepIdx = new AtomicInteger(-1);
        AtomicReference<String> previousValidatorDeskId = new AtomicReference<>(originDeskId);

        Consumer<String> processDeskIdFn = (String deskId) -> {

            if (META_EMITTER_PATTERN.matcher(deskId).matches()) {
                deskId = originDeskId;
            }

            Matcher bossDeskIdMatcher = META_BOSSOF_PATTERN.matcher(deskId);
            if (bossDeskIdMatcher.matches()) {
                Desk previousDesk = Optional.ofNullable(previousValidatorDeskId.get())
                        .map(i -> authService.findDeskById(null, i))
                        .orElseThrow(() -> new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.unknown_desk_id"));

                deskId = Optional.ofNullable(previousDesk.getParentDesk())
                        .map(DeskRepresentation::getId)
                        .orElse(previousDesk.getId());

                String metadataKey = bossDeskIdMatcher.group(1);
                result.put(metadataKey, deskId);
            }

            Matcher variableDeskIdMatcher = META_VARIABLEDESK_PATTERN.matcher(deskId);
            if (variableDeskIdMatcher.matches()) {
                String idValue = variableDesksValues.get(stepIdx.get());
                if (idValue != null) {
                    // Watch out - this means that a 'BossOf' step cannot follow a 'VariableDesk' step,
                    // If this variable desk's value is not provided at creation (but eg in selection script)
                    deskId = idValue;
                    String metadataKey = variableDeskIdMatcher.group(1);
                    result.put(metadataKey, deskId);
                } else {
                    log.warn("No ID provided for variable desk n° {}", stepIdx.get());
                }
            }
            previousValidatorDeskId.set(deskId);
        };


        validationWorkflowDefinition.getSteps().stream()
                .peek(s -> stepIdx.getAndIncrement())
                .forEach(step -> step.getValidatingDesks().stream()
                        .map(DeskRepresentation::getId)
                        .forEach(processDeskIdFn));

        stepIdx.getAndIncrement();
        Optional.ofNullable(validationWorkflowDefinition.getFinalDesk())
                .map(DeskRepresentation::getId)
                .ifPresent(processDeskIdFn);

        return result;
    }


    public void mapIndexedPlaceholdersToActualDesk(@NotNull DeskRepresentation desk,
                                                   @NotNull String originDeskId,
                                                   @NotNull Map<String, String> placeholdersMapping) {

        log.debug("mapVariablePlaceholdersToActualDesk desk : {}", desk);
        Matcher matcher = META_EMITTER_PATTERN.matcher(desk.getId());
        if (matcher.matches()) {
            desk.setId(originDeskId);
            return;
        }

        matcher = META_BOSSOF_PATTERN.matcher(desk.getId());
        if (matcher.matches()) {
            String metaKey = matcher.group(1);
            String actualDeskId = placeholdersMapping.get(metaKey);
            if (actualDeskId == null) {
                log.error("Could not find ID for 'bossOf' placeholder : {}", desk.getId());
            }
            desk.setId(actualDeskId);
            return;
        }

        matcher = META_VARIABLEDESK_PATTERN.matcher(desk.getId());
        if (matcher.matches()) {
            String metaKey = matcher.group(1);
            String actualDeskId = placeholdersMapping.get(metaKey);
            if (actualDeskId == null) {
                log.error("Could not find ID for 'variableDesk' placeholder : {}", desk.getId());
                return;
            }
            desk.setId(actualDeskId);
        }
    }


    @Override
    public void editFolder(@NotNull String folderId, @NotNull FolderDto folderDto) {

        log.info("editFolder folderId:{} folderParams:{}", folderId, folderDto);

        URI requestUri = UriComponentsBuilder.newInstance()
                .scheme(HTTP).host(properties.getHost()).port(properties.getPort())
                .path(INSTANCE_URL).pathSegment(folderId)
                .build().normalize().toUri();

        IpWorkflowEditFolder request = new IpWorkflowEditFolder(folderDto);
        String requestJson;
        try {
            requestJson = objectMapper.writeValueAsString(request);
        } catch (JsonProcessingException e) {
            throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, "messages.error_writing_request");
        }

        WebClient.builder().clientConnector(new ForcedExceptionsConnector()).build()
                .put().uri(requestUri)
                .contentType(APPLICATION_JSON)
                .bodyValue(requestJson)
                .exchange()
                .doOnError(e -> {throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, e, "message.cannot_reach_workflow_service");})
                .block(SUBSERVICE_REQUEST_SHORT_TIMEOUT);
    }


    @Override
    public void editFolderValidationWorkflow(@NotNull String folderId, @NotNull WorkflowDefinitionDto newValidationWorkflow) {
        JSONStringer stringer;
        try {
            stringer = new JSONStringer()
                    .object()
                    /**/.key("finalGroup").value(newValidationWorkflow.getFinalDesk().getId())
                    /**/.key("validationWorkflowId").value(newValidationWorkflow.getKey())
                    .endObject();

        } catch (JSONException e) {
            throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, e, "message.error_writing_request");
        }

        URI requestUri = UriComponentsBuilder.newInstance()
                .scheme(HTTP).host(properties.getHost()).port(properties.getPort())
                .path(INSTANCE_URL + "/" + folderId + "/changeValidationWorkflow")
                .build()
                .normalize()
                .toUri();

        WebClient.builder()
                .clientConnector(new ForcedExceptionsConnector())
                .build()
                .put()
                .uri(requestUri)
                .contentType(APPLICATION_JSON)
                .bodyValue(stringer.toString())
                .exchangeToMono(clientResponse -> clientResponse.bodyToMono(String.class))
                .block(SUBSERVICE_REQUEST_SHORT_TIMEOUT);
    }


    @Override
    public @NotNull List<Task> getHistoricTasks(@NotNull String folderId) {

        URI requestUri = UriComponentsBuilder.newInstance()
                .scheme(HTTP).host(properties.getHost()).port(properties.getPort())
                .path(INSTANCE_URL).pathSegment(folderId).pathSegment(HISTORY_TASKS)
                .build().normalize().toUri();

        List<Task> result = WebClient.builder().clientConnector(new ForcedExceptionsConnector()).build()
                .get().uri(requestUri)
                .accept(APPLICATION_JSON)
                .retrieve()
                // Result
                .bodyToMono(new ParameterizedTypeReference<List<IpWorkflowTask>>() {})
                .blockOptional(SUBSERVICE_REQUEST_SHORT_TIMEOUT)
                .orElseThrow(() -> new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.cannot_reach_workflow_service"))
                .stream()
                .map(t -> (Task) t)
                // Filtering internal pending tasks
                .filter(t -> !((t.getAction() == UNDO) && (t.getUser() == null)))
                .filter(t -> !((t.getAction() == READ) && (t.getUser() == null)))
                // Sorting chronologically
                .sorted(comparing(Task::getDate, nullsLast(naturalOrder())))
                .toList();

        log.debug("getTask result {}", result);
        return result;
    }


    @Override
    public @NotNull List<Task> getReadTasks(Folder folder) {

        URI requestUri = UriComponentsBuilder.newInstance()
                .scheme(HTTP).host(properties.getHost()).port(properties.getPort())
                .path(INSTANCE_URL).pathSegment(folder.getId()).pathSegment(READ_TASKS)
                .build().normalize().toUri();

        // TODO in the long run, the cleaner solution would be to stream each task back from workflow, one at a time
        WebClient webClient = RequestUtils.getWebClientWithBufferSize(16, true);

        List<Task> result = webClient
                .get().uri(requestUri)
                .accept(APPLICATION_JSON)
                .retrieve()
                // Result
                .bodyToMono(new ParameterizedTypeReference<List<IpWorkflowTask>>() {})
                .blockOptional(SUBSERVICE_REQUEST_SHORT_TIMEOUT)
                .orElseThrow(() -> new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.cannot_reach_workflow_service"))
                .stream()
                .map(t -> (Task) t)
                // Sorting chronologically
                .sorted(comparing(Task::getDate, nullsLast(naturalOrder())))
                .toList();

        log.debug("getTask result {}", result);
        return result;
    }


    @Override
    public @NotNull Task getTask(@NotNull String taskId) {

        URI requestUri = UriComponentsBuilder.newInstance()
                .scheme(HTTP).host(properties.getHost()).port(properties.getPort())
                .path(TASK_URL).pathSegment(taskId)
                .build().normalize().toUri();

        Task result = WebClient.builder().clientConnector(new ForcedExceptionsConnector()).build()
                .get().uri(requestUri)
                .accept(APPLICATION_JSON)
                .retrieve()
                // Result
                .bodyToMono(IpWorkflowTask.class)
                .blockOptional(SUBSERVICE_REQUEST_SHORT_TIMEOUT)
                .orElseThrow(() -> new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.cannot_reach_workflow_service"));

        log.debug("getTask result {}", result);
        return result;
    }


    @Override
    public void performTask(@NotNull Task task,
                            @NotNull Action action,
                            @NotNull String userId,
                            @NotNull Folder folder,
                            @Nullable String deskId,
                            @Nullable String overriddenCreationWorkflowDefinitionId,
                            @Nullable String overriddenValidationWorkflowDefinitionId,
                            @Nullable String overriddenOriginDeskId,
                            @Nullable String overriddenFinalDeskId,
                            @Nullable String transactionId,
                            @Nullable String pastellDocumentId,
                            @Nullable String ipngBusinessId,
                            @Nullable SimpleTaskParams simpleTaskParams,
                            @Nullable String targetDeskId,
                            @Nullable String publicCertBase64) {

        // In 2 cases, we want to re-write the current candidate group :
        // - If we're in a OR parallel task, and we want to remove unused ones
        // - If we're in a delegation, and we want to rewrite everything to get the delegating & delegate candidates

        boolean isOrParallelTask = task.getDesks().size() > 1;
        boolean isSomeDelegation = task.getDesks().stream()
                .map(DeskRepresentation::getId)
                .noneMatch(id -> StringUtils.equals(id, deskId));
        String randomOriginalCandidateDeskId = task.getDesks().stream()
                .findFirst()
                .map(DeskRepresentation::getId)
                .orElse(null);

        // Variable building

        Map<String, String> variablesMap = new HashMap<>();

        variablesMap.put(META_VALIDATION_WORKFLOW_ID, overriddenValidationWorkflowDefinitionId);
        variablesMap.put(META_CREATION_WORKFLOW_ID, overriddenCreationWorkflowDefinitionId);
        variablesMap.put(METADATA_ORIGIN_GROUP_ID, overriddenOriginDeskId);
        variablesMap.put(METADATA_FINAL_GROUP_ID, overriddenFinalDeskId);
        variablesMap.put(METADATA_TRANSACTION_ID, transactionId);
        variablesMap.put(METADATA_PASTELL_DOCUMENT_ID, pastellDocumentId);
        variablesMap.put(METADATA_IPNG_BUSINESS_ID, ipngBusinessId);
        variablesMap.put(METADATA_TARGET_DELEGATE, targetDeskId);
        variablesMap.put(METADATA_PUBLIC_CERTIFICATE_BASE64, publicCertBase64);

        String publicAnnotation = Objects.nonNull(simpleTaskParams) ? simpleTaskParams.getPublicAnnotation() : null;
        String privateAnnotation = Objects.nonNull(simpleTaskParams) ? simpleTaskParams.getPrivateAnnotation() : null;
        variablesMap.put(METADATA_PUBLIC_ANNOTATION, publicAnnotation);
        variablesMap.put(METADATA_PRIVATE_ANNOTATION, privateAnnotation);

        JSONStringer stringer;
        try {
            stringer = new JSONStringer()
                    .object()
                    /**/.key("action").value(action.toString())
                    /**/.key("userId").value(userId);

            if ((isSomeDelegation || isOrParallelTask) && StringUtils.isNotEmpty(deskId)) {
                stringer.key("groupId").value(deskId);
            }

            if (isSomeDelegation && StringUtils.isNotEmpty(randomOriginalCandidateDeskId)) {
                stringer.key("delegatedByGroupId").value(randomOriginalCandidateDeskId);
            }

            stringer/**/.key("variables").object();

            variablesMap.entrySet().stream()
                    .filter(e -> StringUtils.isNotEmpty(e.getKey()))
                    .filter(e -> StringUtils.isNotEmpty(e.getValue()))
                    .forEach(e -> safeAddKeyValue(stringer, e.getKey(), e.getValue()));

            stringer/**/.endObject()
                    .endObject();
        } catch (JSONException e) {
            throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, e, "message.error_writing_request");
        }

        if (Objects.nonNull(simpleTaskParams) && MapUtils.isNotEmpty(simpleTaskParams.getMetadata())) {
            FolderDto folderDto = new FolderDto();
            Map<String, String> filteredMetadata = folder.getMetadata()
                    .entrySet()
                    .stream()
                    .filter(entry -> !entry.getKey().contains("workflow_internal_"))
                    .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

            Map<String, String> metadata = new HashMap<>(filteredMetadata);

            metadata.putAll(simpleTaskParams.getMetadata());
            folderDto.setTypeId(folder.getType().getId());
            folderDto.setSubtypeId(folder.getSubtype().getId());
            folderDto.setName(folder.getName());
            folderDto.setMetadata(metadata);
            this.editFolder(folder.getId(), folderDto);
        }

        URI requestUri = UriComponentsBuilder.newInstance()
                .scheme(HTTP).host(properties.getHost()).port(properties.getPort())
                .path(TASK_URL).pathSegment(task.getId())
                .build().normalize().toUri();

        WebClient.builder().clientConnector(new ForcedExceptionsConnector()).build()
                .post().uri(requestUri)
                .contentType(APPLICATION_JSON).accept(APPLICATION_JSON)
                .bodyValue(stringer.toString())
                .retrieve()
                // Result
                .bodyToMono(IpWorkflowInstance.class)
                .block(SUBSERVICE_REQUEST_SHORT_TIMEOUT);
    }


    @Override
    public void deleteWorkflow(@NotNull String folderId) {
        deleteInstance(folderId, false);
    }


    // <editor-fold desc="Archives CRUDL">


    @Override
    public @NotNull PaginatedList<? extends Folder> getArchives(@NotNull String tenantId,
                                                                @NotNull Pageable pageable,
                                                                @Nullable Long stillSinceDate) {

        long page = (pageable != Pageable.unpaged()) ? pageable.getPageNumber() : 0L;
        long pageSize = (pageable != Pageable.unpaged()) ? pageable.getPageSize() : MAX_VALUE;

        FolderSortBy sortBy = pageable.getSort().stream().findFirst()
                .map(Sort.Order::getProperty)
                .map(FolderSortBy::valueOf)
                .orElse(FolderSortBy.FOLDER_NAME);

        boolean asc = pageable.getSort().stream().findFirst()
                .map(Sort.Order::getDirection)
                .map(Sort.Direction::isAscending)
                .orElse(true);

        URI requestUri = UriComponentsBuilder.newInstance()
                .scheme(HTTP).host(properties.getHost()).port(properties.getPort())
                .path(ARCHIVE_URL)
                .queryParam("page", page)
                .queryParam("tenantId", tenantId)
                .queryParam("pageSize", pageSize)
                .queryParam("sortBy", toIpWorkflowValue(sortBy))
                .queryParam("asc", asc)
                .queryParam("stillSinceDate", stillSinceDate)
                .build().normalize().toUri();

        return WebClient.builder().clientConnector(new ForcedExceptionsConnector()).build()
                .get().uri(requestUri)
                .accept(APPLICATION_JSON)
                .retrieve()
                // Result
                .bodyToMono(new ParameterizedTypeReference<PaginatedList<IpWorkflowInstance>>() {})
                // TODO this probably could be a short timeout, to be tested
                .blockOptional(SUBSERVICE_REQUEST_LONG_TIMEOUT)
                .orElseThrow(() -> new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.cannot_reach_workflow_service"));
    }


    @Override
    public void deleteArchive(@NotNull String folderId) {
        deleteInstance(folderId, true);
    }


    // </editor-fold desc="Archives CRUDL">


    private void deleteInstance(@NotNull String folderId, boolean archive) {

        URI requestUri = UriComponentsBuilder.newInstance()
                .scheme(HTTP).host(properties.getHost()).port(properties.getPort())
                .path(archive ? ARCHIVE_URL : INSTANCE_URL).pathSegment(folderId)
                .build().normalize().toUri();

        log.debug("deleteWorkflow folderId:{} url:{}", folderId, requestUri.toString());

        WebClient.builder().clientConnector(new ForcedExceptionsConnector()).build()
                .delete().uri(requestUri)
                .exchange()
                .doOnError(e -> {throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, e, "message.cannot_reach_workflow_service");})
                .block(SUBSERVICE_REQUEST_SHORT_TIMEOUT);
    }


    // <editor-fold desc="IPNG lifecycle">


    @Override
    public void setFolderWaitingForIpngResponse(@NotNull IpngProof sentProof, @NotNull String tenantId, @NotNull Folder folder) {
        log.info("Proof sent successfully into IPNG network : {}. Referring folder id : {}", sentProof, folder.getId());
        PendingIpngFolder pendingFolder = new PendingIpngFolder(sentProof.getBusinessId(),
                tenantId,
                folder.getId(),
                sentProof.getReceiverDeskboxId(),
                PendingIpngFolder.State.SENT);
        this.pendingIpngFolderRepository.save(pendingFolder);
    }


    @Override
    public void receivedIpngResponse(String tenantId, String deskId, Folder folder, PendingIpngFolder pendingFolder, IpngProofWrap proofData) {
        log.info("receivedIpngResponse");

        Map<String, String> metadataToAdd = Map.of(META_IPNG_RESPONSE_ID, proofData.getId());
        FolderDto editRequest = new FolderDto();
        editRequest.setMetadata(metadataToAdd);
        this.editFolder(pendingFolder.getFolderId(), editRequest);

        Optional<Task> matchingTask = folder.getStepList().stream()
                .filter(t -> t.getAction() == IPNG || t.getAction() == IPNG_RETURN)
                .filter(t -> FolderUtils.isActive(t.getState()))
                .filter(t -> t.getExternalState() == Task.ExternalState.ACTIVE)
                .findFirst();

        log.info("matching tasks : {}", matchingTask.orElse(null));

        if (matchingTask.isPresent()) {
            this.performTask(matchingTask.get(), IPNG, folder, null, null, null, null);
            this.pendingIpngFolderRepository.delete(pendingFolder);
        } else {
            log.error("received a response from IPNG, with a matching folder but no active IPNG task awaiting.");
        }
    }


    @Override
    public void ipngProofReceiptWasReceived(String tenantId, String deskId, PendingIpngFolder pendingFolder, IpngProofWrap proofData) {
        log.info("ipngProofReceiptWasReceived");

        Map<String, String> metadataToAdd = Map.of(META_IPNG_PROOF_RECEIPT_ID, proofData.getId());
        FolderDto editRequest = new FolderDto();
        editRequest.setMetadata(metadataToAdd);
        this.editFolder(pendingFolder.getFolderId(), editRequest);

        Folder folder = Optional.ofNullable(getFolder(pendingFolder.getFolderId(), tenantId))
                .orElseThrow(() -> new IpInternalException(String.format("Folder not found : %s", pendingFolder.getFolderId())));

        Optional<Task> matchingTask = folder.getStepList().stream()
                .filter(t -> t.getAction() == IPNG || t.getAction() == IPNG_RETURN)
                .filter(t -> FolderUtils.isActive(t.getState()))
                // TODO once we've updated externalState mechanics on workflow side
//                .filter(t -> t.getExternalState() == Task.ExternalState.ACTIVE)
                .findFirst();

        if (matchingTask.isPresent()) {
            log.info("Ok, proof was indeed sent");
            pendingFolder.setState(PendingIpngFolder.State.RECEIVED);
            this.pendingIpngFolderRepository.save(pendingFolder);

            // TODO once we've updated externalState mechanics on workflow side
//            this.performTask(matchingTask.get(), IPNG, null, null, deskId);
        } else {
            log.error("received a receipt from IPNG, with a matching folder but no active IPNG task awaiting.");
        }
    }


    @Override
    public void ipngProofWasSent(String tenantId, String deskId, PendingIpngFolder pendingFolder, IpngProofWrap proofData) {
        log.info("ipngProofWasSent");

        Map<String, String> metadataToAdd = new HashMap<>();
        metadataToAdd.put(META_IPNG_PROOF_SENT_ID, proofData.getId());
        metadataToAdd.put(META_IPNG_RECIPIENT_ENTITY_ID, proofData.getReceiverEntityId());
        metadataToAdd.put(META_IPNG_RECIPIENT_DBX_ID, proofData.getFolderExchange().getReceiverDeskboxId());
        FolderDto editRequest = new FolderDto();
        editRequest.setMetadata(metadataToAdd);
        this.editFolder(pendingFolder.getFolderId(), editRequest);

        Folder folder = Optional.ofNullable(getFolder(pendingFolder.getFolderId(), tenantId))
                .orElseThrow(() -> new IpInternalException(String.format("Folder not found : %s", pendingFolder.getFolderId())));

        Optional<Task> matchingTask = folder.getStepList().stream()
                .filter(t -> t.getAction() == IPNG || t.getAction() == IPNG_RETURN)
                .filter(t -> FolderUtils.isActive(t.getState()))
                // TODO once we've updated externalState mechanics on workflow side
//                .filter(t -> t.getExternalState() == Task.ExternalState.ACTIVE)
                .findFirst();

        if (matchingTask.isPresent()) {
            log.info("Ok, proof was indeed sent");
            pendingFolder.setState(PendingIpngFolder.State.IN_NETWORK);
            this.pendingIpngFolderRepository.save(pendingFolder);
            // TODO once we've updated externalState mechanics on workflow side
//            this.performTask(matchingTask.get(), IPNG, null, null, deskId);
        } else {
            log.error("received a proof-sent notification from IPNG, with a matching folder but no active IPNG task awaiting.");
        }

    }


    // </editor-fold desc="IPNG lifecycle">


}
