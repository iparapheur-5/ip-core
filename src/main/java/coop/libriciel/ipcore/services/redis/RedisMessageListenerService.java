/*
 * iparapheur Core
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package coop.libriciel.ipcore.services.redis;

import com.fasterxml.jackson.databind.ObjectMapper;
import coop.libriciel.ipcore.controller.WorkflowController;
import coop.libriciel.ipcore.model.auth.Desk;
import coop.libriciel.ipcore.model.auth.DeskRepresentation;
import coop.libriciel.ipcore.model.auth.User;
import coop.libriciel.ipcore.model.database.Subtype;
import coop.libriciel.ipcore.model.database.Tenant;
import coop.libriciel.ipcore.model.externalsignature.ExternalSignatureParams;
import coop.libriciel.ipcore.model.externalsignature.SignRequestMember;
import coop.libriciel.ipcore.model.mail.MailNotification;
import coop.libriciel.ipcore.model.workflow.Folder;
import coop.libriciel.ipcore.model.workflow.Task;
import coop.libriciel.ipcore.model.workflow.ipWorkflow.IpWorkflowInstance;
import coop.libriciel.ipcore.model.workflow.ipWorkflow.NotifiedIpWorkflowTask;
import coop.libriciel.ipcore.model.workflow.requests.SimpleTaskParams;
import coop.libriciel.ipcore.services.auth.AuthServiceInterface;
import coop.libriciel.ipcore.services.database.SubtypeRepository;
import coop.libriciel.ipcore.services.mail.NotificationServiceInterface;
import coop.libriciel.ipcore.services.workflow.WorkflowServiceInterface;
import coop.libriciel.ipcore.utils.LocalizedStatusException;
import coop.libriciel.ipcore.utils.UserUtils;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.connection.MessageListener;
import org.springframework.stereotype.Service;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.transaction.support.TransactionTemplate;

import java.io.IOException;
import java.util.*;

import static coop.libriciel.ipcore.model.externalsignature.ExternalSignatureParams.*;
import static coop.libriciel.ipcore.model.workflow.Action.EXTERNAL_SIGNATURE;
import static coop.libriciel.ipcore.model.workflow.Action.SEAL;
import static coop.libriciel.ipcore.model.workflow.State.VALIDATED;
import static coop.libriciel.ipcore.utils.UserUtils.AUTOMATIC_TASK_USER_ID;
import static java.nio.charset.StandardCharsets.UTF_8;
import static java.util.stream.Collectors.toCollection;
import static org.springframework.http.HttpStatus.NOT_FOUND;


@Log4j2
@Service
public class RedisMessageListenerService implements MessageListener {


    // <editor-fold desc="Beans">


    private final TransactionTemplate transactionTemplate;
    private final NotificationServiceInterface notificationService;
    private final WorkflowController workflowController;
    private final WorkflowServiceInterface workflowService;
    private final SubtypeRepository subtypeRepository;
    private final AuthServiceInterface authService;


    public RedisMessageListenerService(@NotNull NotificationServiceInterface notificationController,
                                       @NotNull SubtypeRepository subtypeRepository,
                                       @NotNull WorkflowServiceInterface workflowService,
                                       @NotNull WorkflowController workflowController,
                                       PlatformTransactionManager platformTransactionManager,
                                       AuthServiceInterface authService) {
        this.notificationService = notificationController;
        this.workflowService = workflowService;
        this.subtypeRepository = subtypeRepository;
        this.workflowController = workflowController;
        this.transactionTemplate = new TransactionTemplate(platformTransactionManager);
        this.authService = authService;
    }


    // </editor-fold desc="Beans">


    @Override
    public void onMessage(Message message, byte[] pattern) {

        Folder folder;
        NotifiedIpWorkflowTask task;

        try {
            String messageString = new String(message.getBody(), UTF_8);
            NotifiedIpWorkflowTask ipWorkflowTask = new ObjectMapper().readValue(messageString, NotifiedIpWorkflowTask.class);
            folder = new IpWorkflowInstance(ipWorkflowTask);
            task = ipWorkflowTask;
        } catch (IOException e) {
            log.warn("Something went wrong parsing mail notification", e);
            return;
        }

        // We need to give a little delay to Workflow to build up the task.
        // TODO : Find a post-delayed-method on Redis, that would be more elegant.
        try {Thread.sleep(500);} catch (InterruptedException ie) { /* Not Used */ }

        log.debug("Message received folder:{}", folder);
        
        Subtype subtype = subtypeRepository
                .findById(folder.getSubtype().getId())
                .orElseThrow(() -> new LocalizedStatusException(NOT_FOUND, "message.unknown_subtype_id"));

        Tenant tenant = subtype.getTenant();


        // Tests for auto SEAL or EXTERNAL_SIGNATURE

        boolean isExternalSignature = task.getAction() == EXTERNAL_SIGNATURE;
        boolean canPerformExternalSignature = subtype.isExternalSignatureAutomatic() && doesTaskHaveExternalSignatureMetadata(task);
        boolean doPerformAutoExternalSignature = isExternalSignature && canPerformExternalSignature;

        boolean isSeal = task.getAction() == SEAL;
        boolean isNotValidated = task.getState() != VALIDATED;
        boolean isSealAutomatic = subtype.isSealAutomatic();
        boolean doPerformAutoSeal = isSeal && isNotValidated && isSealAutomatic;

        // Content id is absent from the redis message, we have to fetch it

        if (doPerformAutoExternalSignature || doPerformAutoSeal) {
            Folder populatedFolder = this.workflowService.getFolder(folder.getId(), tenant.getId());
            if (populatedFolder != null) {
                folder.setContentId(populatedFolder.getContentId());
            }
        }

        // Auto seal, maybe

        if (doPerformAutoSeal) {

            Desk validatorDesk = task.getDesks().stream()
                    .findFirst()
                    .map(DeskRepresentation::getId)
                    .map(id -> authService.findDeskByIdNoException(tenant.getId(), id))
                    .orElseThrow(() -> new LocalizedStatusException(NOT_FOUND, "message.unknown_desk_id"));

            log.debug("Will perform automatic seal, on tenant '{}',  in the name of desk '{}', for folder '{}'",
                    tenant.getName(), validatorDesk.getName(), folder.getName()
            );

            // The transaction prevent a LazyLoadingException (required since we are not in the context of a request session)
            transactionTemplate.execute((TransactionCallback<Void>) transactionStatus -> {
                workflowController.seal(
                        tenant.getId(),
                        tenant,
                        validatorDesk.getId(),
                        validatorDesk,
                        folder.getId(),
                        folder,
                        task.getId(),
                        task,
                        UserUtils.getAutomaticUser(),
                        new SimpleTaskParams()
                );
                return null;
            });

            // We don't want to notify anyone here,
            // since the action was already done.
            return;
        }

        // Auto external signature, maybe

        if (doPerformAutoExternalSignature) {

            Desk validatorDesk = task.getDesks().stream()
                    .findFirst()
                    .map(DeskRepresentation::getId)
                    .map(id -> authService.findDeskByIdNoException(tenant.getId(), id))
                    .orElseThrow(() -> new LocalizedStatusException(NOT_FOUND, "message.unknown_desk_id"));

            workflowController.requestExternalSignatureProcedure(
                    tenant.getId(),
                    tenant,
                    validatorDesk.getId(),
                    validatorDesk,
                    folder.getId(),
                    folder,
                    task.getId(),
                    task,
                    UserUtils.getAutomaticUser(),
                    getExternalSignatureParams(folder, task));

            // We don't want to notify anyone here,
            // since the action was already done.
            return;
        }

        notify(tenant, folder, task);

    }


    private void notify(Tenant tenant, Folder folder, NotifiedIpWorkflowTask task) {

        Task populatedCurrentTask = this.getPopulatedCurrentTask(tenant.getId(), folder.getId());

        User sender = Optional.ofNullable(populatedCurrentTask)
                .map(Task::getUser)
                .map(u -> StringUtils.equals(u.getId(), AUTOMATIC_TASK_USER_ID)
                          ? UserUtils.getAutomaticUser()
                          : authService.findUserById(u.getId())
                )
                .orElse(null);

        MailNotification mailNotification = new MailNotification();
        mailNotification.setInstanceId(folder.getId());
        mailNotification.setInstanceName(folder.getName());
        mailNotification.setTenantId(tenant.getId());
        mailNotification.setTaskAction(task.getAction());
        mailNotification.setNotificationType(task.getNotificationType());
        mailNotification.setTaskId(
                Optional.ofNullable(task.getId())
                        .orElse(Optional.ofNullable(populatedCurrentTask)
                                .map(Task::getId)
                                .orElse(null)
                        )
        );

        HashSet<String> desksToNotify = task
                .getDesks()
                .stream()
                .map(DeskRepresentation::getId)
                .collect(toCollection(HashSet::new));
        mailNotification.setDeskIdsToNotify(desksToNotify);

        HashSet<String> additionalDesksToNotify = task
                .getNotifiedDesks()
                .stream()
                .map(DeskRepresentation::getId)
                .collect(toCollection(HashSet::new));
        mailNotification.setAdditionalDeskIdsToNotify(additionalDesksToNotify);

        notificationService.notify(mailNotification, sender);
    }


    private Task getPopulatedCurrentTask(String tenantId, String folderId) {
        Task task = null;
        Folder populatedFolder = this.workflowService.getFolder(folderId, tenantId);
        if (populatedFolder != null) {
            List<Task> validatedTasks = populatedFolder.getStepList()
                    .stream()
                    .filter(t -> t.getState().equals(VALIDATED))
                    .toList();

            if (!validatedTasks.isEmpty()) {
                task = validatedTasks
                        .stream()
                        .skip(validatedTasks.size() - 1)
                        .findFirst()
                        .orElse(null);
            }
        }
        return task;
    }


    private static ExternalSignatureParams getExternalSignatureParams(Folder folder, Task task) {
        SignRequestMember signRequestMember = new SignRequestMember(
                task.getMetadata().get(METADATA_EXTERNAL_SIGNATURE_KEY_FIRSTNAME),
                task.getMetadata().get(METADATA_EXTERNAL_SIGNATURE_KEY_LASTNAME),
                task.getMetadata().get(METADATA_EXTERNAL_SIGNATURE_KEY_MAIL),
                task.getMetadata().get(METADATA_EXTERNAL_SIGNATURE_KEY_PHONE)
        );

        return new ExternalSignatureParams(
                folder.getName(),
                task.getId(),
                Collections.singletonList(signRequestMember),
                new HashMap<>(),
                new ArrayList<>()
        );
    }


    private static boolean doesTaskHaveExternalSignatureMetadata(Task task) {
        return StringUtils.isNoneEmpty(
                task.getMetadata().get(METADATA_EXTERNAL_SIGNATURE_KEY_MAIL),
                task.getMetadata().get(METADATA_EXTERNAL_SIGNATURE_KEY_PHONE),
                task.getMetadata().get(METADATA_EXTERNAL_SIGNATURE_KEY_FIRSTNAME),
                task.getMetadata().get(METADATA_EXTERNAL_SIGNATURE_KEY_LASTNAME)
        );
    }


}
