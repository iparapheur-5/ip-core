/*
 * iparapheur Core
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package coop.libriciel.ipcore.services.scheduler;

import org.jetbrains.annotations.NotNull;

import java.util.Date;


public interface SchedulerServiceInterface {

    String BEAN_NAME = "schedulerService";
    String PREFERENCES_PROVIDER_KEY = "services.scheduler.provider";


    boolean hasScheduledNotifications(@NotNull String userId);


    void deleteExistingNotificationScheduler(@NotNull String userId);

    void addNotificationScheduler(@NotNull String userId, @NotNull Date nextStartDate);


}
