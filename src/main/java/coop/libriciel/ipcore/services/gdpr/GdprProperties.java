/*
 * iparapheur Core
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.ipcore.services.gdpr;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.ConstructorBinding;

import java.util.List;


@Getter
@Setter
@AllArgsConstructor
@ConstructorBinding
@ConfigurationProperties(prefix = "gdpr")
public class GdprProperties {


    @Getter
    @Setter
    @NoArgsConstructor
    @AllArgsConstructor
    public static class GdprEntity {

        private String name;
        private String address;
        private String siret;

    }


    @Getter
    @Setter
    @NoArgsConstructor
    public static class GdprDeclaringEntity extends GdprEntity {

        @Getter
        @Setter
        @NoArgsConstructor
        public static class GdprDeclaringEntityDpo {

            private String name;
            private String mail;

        }


        @Getter
        @Setter
        @NoArgsConstructor
        public static class GdprDeclaringEntityResponsible {

            private String name;
            private String title;

        }


        private String apeCode;
        private String phoneNumber;
        private String mail;
        private GdprDeclaringEntityDpo dpo;
        private GdprDeclaringEntityResponsible responsible;

    }


    @Getter
    @Setter
    @NoArgsConstructor
    public static class GdprApplication {


        @Getter
        @Setter
        @NoArgsConstructor
        @AllArgsConstructor
        public static class GdprDataElement {

            private String name;
            private List<String> elements;

        }


        @Getter
        @Setter
        @NoArgsConstructor
        @AllArgsConstructor
        public static class GdprCookie extends GdprDataElement {

            private String description;

        }


        @Getter
        @Setter
        @NoArgsConstructor
        @AllArgsConstructor
        public static class GdprDataSet {

            private String name;
            private List<String> mandatoryElements;
            private List<String> optionalElements;

        }


        private String name;
        private String cookieSessionDuration;
        private List<String> mandatoryCookies;
        private List<String> preservedDataAfterDeletion;
        private List<GdprCookie> optionalCookies;
        private boolean noCookies;
        private GdprEntity editor;
        private boolean noDataProcessed;
        private boolean noDataCollected;
        private List<GdprDataElement> dataProcesses;
        private List<GdprDataSet> collectedDataSet;

    }


    private GdprDeclaringEntity declaringEntity;
    private String hostingEntityComments;
    private GdprEntity hostingEntity;
    private GdprEntity maintenanceEntity;
    private GdprApplication application;


}
