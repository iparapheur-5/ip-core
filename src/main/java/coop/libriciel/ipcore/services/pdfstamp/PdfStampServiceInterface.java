/*
 * iparapheur Core
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package coop.libriciel.ipcore.services.pdfstamp;

import coop.libriciel.ipcore.model.content.DocumentBuffer;
import coop.libriciel.ipcore.model.pdfstamp.Annotation;
import coop.libriciel.ipcore.model.pdfstamp.SignaturePlacement;
import coop.libriciel.ipcore.model.pdfstamp.Stamp;
import coop.libriciel.ipcore.model.pdfstamp.StickyNote;
import coop.libriciel.pdfstamp.model.Comment;
import coop.libriciel.pdfstamp.model.Position;
import coop.libriciel.pdfstamp.model.PositionOrigin;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;
import java.util.function.Function;

import static coop.libriciel.pdfstamp.model.CommentIcon.SIGNATURE_PLACEMENT;
import static coop.libriciel.pdfstamp.model.CommentIcon.STICKY_NOTE;
import static org.apache.commons.lang3.ObjectUtils.firstNonNull;


@Service
public interface PdfStampServiceInterface {


    String BEAN_NAME = "pdfStampService";
    String PREFERENCES_PROVIDER_KEY = "services.pdfstamp.provider";


    @NotNull DocumentBuffer createStamp(@NotNull DocumentBuffer documentBuffer,
                                        @NotNull List<Stamp> stampList,
                                        @NotNull Function<String, DocumentBuffer> imageSupplier);


    default @NotNull DocumentBuffer createAnnotation(@NotNull DocumentBuffer documentBuffer, @NotNull StickyNote stickyNote) {

        if (StringUtils.isNotEmpty(stickyNote.getId())) {
            documentBuffer = this.deleteComment(documentBuffer, stickyNote.getId());
        }

        // TODO : new CommentApi().addComment(abstractResource, comment);

        Position position = new Position();
        position.setOrigin(stickyNote.getRectangleOrigin() == Annotation.Origin.TOP_RIGHT ? PositionOrigin.TOP_RIGHT : PositionOrigin.BOTTOM_LEFT);
        position.setWidth(stickyNote.getWidth());
        position.setHeight(stickyNote.getHeight());
        position.setX(stickyNote.getX() + (stickyNote.getWidth() / 2));
        position.setY(stickyNote.getY() + (stickyNote.getHeight() / 2));
        position.setMarginLeft(0);
        position.setCentered(true);

        Comment comment = new Comment();
        comment.setPage(stickyNote.getPage());
        comment.setOpacity(1f);
        comment.setName(UUID.randomUUID().toString());
        comment.setTitle(SecurityContextHolder.getContext().getAuthentication().getName());
        comment.setContents(stickyNote.getContent());
        comment.setPosition(position);
        comment.setIcon(STICKY_NOTE);
        // .key("rotation").value(stickyNote.getPageRotation())

        return addComment(documentBuffer, comment);
    }


    default @NotNull DocumentBuffer createSignaturePlacement(@NotNull DocumentBuffer documentBuffer, @NotNull SignaturePlacement signaturePlacement) {

        Position position = new Position();
        position.setOrigin(signaturePlacement.getRectangleOrigin() == Annotation.Origin.TOP_RIGHT ? PositionOrigin.TOP_RIGHT : PositionOrigin.BOTTOM_LEFT);
        position.setWidth(signaturePlacement.getWidth());
        position.setHeight(signaturePlacement.getHeight());
        position.setX(signaturePlacement.getX() + (signaturePlacement.getWidth() / 2));
        position.setY(signaturePlacement.getY() + (signaturePlacement.getHeight() / 2));
        position.setMarginLeft(0);
        position.setCentered(true);

        Comment comment = new Comment();
        comment.setPage(signaturePlacement.getPage());
        comment.setOpacity(1f);
        comment.setName(firstNonNull(signaturePlacement.getId(), UUID.randomUUID().toString()));
        comment.setTitle(SecurityContextHolder.getContext().getAuthentication().getName());
        comment.setContents(String.valueOf(signaturePlacement.getSignatureNumber()));
        comment.setPosition(position);
        comment.setIcon(SIGNATURE_PLACEMENT);
        // .key("rotation").value(stickyNote.getPageRotation())

        return addComment(documentBuffer, comment);
    }


    @NotNull DocumentBuffer addComment(@NotNull DocumentBuffer documentBuffer, @NotNull Comment comment);


    @NotNull DocumentBuffer deleteComment(@NotNull DocumentBuffer documentBuffer, @NotNull String annotationId);


}
