/*
 * iparapheur Core
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.ipcore.services.pdfstamp;

import coop.libriciel.ipcore.model.content.DocumentBuffer;
import coop.libriciel.ipcore.model.pdfstamp.Stamp;
import coop.libriciel.ipcore.utils.LocalizedStatusException;
import coop.libriciel.pdfstamp.model.Comment;
import org.jetbrains.annotations.NotNull;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.function.Function;

import static coop.libriciel.ipcore.utils.TextUtils.NONE_SERVICE;
import static org.springframework.http.HttpStatus.SERVICE_UNAVAILABLE;


@Service(PdfStampServiceInterface.BEAN_NAME)
@ConditionalOnProperty(name = PdfStampServiceInterface.PREFERENCES_PROVIDER_KEY, havingValue = NONE_SERVICE)
public class NonePdfStampService implements PdfStampServiceInterface {


    @Override
    public @NotNull DocumentBuffer createStamp(@NotNull DocumentBuffer documentBuffer,
                                               @NotNull List<Stamp> stampList,
                                               @NotNull Function<String, DocumentBuffer> imageSupplier) {
        throw new LocalizedStatusException(SERVICE_UNAVAILABLE, "message.pdf_stamp_service_not_available");
    }


    @Override
    public @NotNull DocumentBuffer addComment(@NotNull DocumentBuffer documentBuffer, @NotNull Comment comment) {
        throw new LocalizedStatusException(SERVICE_UNAVAILABLE, "message.pdf_stamp_service_not_available");
    }


    @Override
    public @NotNull DocumentBuffer deleteComment(@NotNull DocumentBuffer documentBuffer, @NotNull String annotationId) {
        throw new LocalizedStatusException(SERVICE_UNAVAILABLE, "message.pdf_stamp_service_not_available");
    }


}
