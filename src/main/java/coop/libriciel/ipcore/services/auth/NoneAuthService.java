/*
 * iparapheur Core
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.ipcore.services.auth;

import coop.libriciel.ipcore.model.auth.Desk;
import coop.libriciel.ipcore.model.auth.DeskRepresentation;
import coop.libriciel.ipcore.model.auth.User;
import coop.libriciel.ipcore.model.auth.UserPrivilege;
import coop.libriciel.ipcore.model.auth.requests.DeskDto;
import coop.libriciel.ipcore.model.auth.requests.UserDto;
import coop.libriciel.ipcore.model.database.Tenant;
import coop.libriciel.ipcore.utils.LocalizedStatusException;
import coop.libriciel.ipcore.utils.PaginatedList;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static coop.libriciel.ipcore.utils.TextUtils.NONE_SERVICE;
import static org.springframework.http.HttpStatus.SERVICE_UNAVAILABLE;


@Service(AuthServiceInterface.BEAN_NAME)
@ConditionalOnProperty(name = AuthServiceInterface.PREFERENCES_PROVIDER_KEY, havingValue = NONE_SERVICE)
public class NoneAuthService implements AuthServiceInterface {


    @Override
    public boolean isInternalUser(@NotNull User user) {
        throw new LocalizedStatusException(SERVICE_UNAVAILABLE, "message.auth_service_not_available");
    }


    @Override
    public @NotNull String createTenant(@NotNull String tenantName) {
        throw new LocalizedStatusException(SERVICE_UNAVAILABLE, "message.auth_service_not_available");
    }


    @Override
    public void deleteTenant(@NotNull String tenantId) {
        throw new LocalizedStatusException(SERVICE_UNAVAILABLE, "message.auth_service_not_available");
    }


    @Override
    public @NotNull String createUser(@NotNull Tenant tenant, @NotNull UserDto request) {
        throw new LocalizedStatusException(SERVICE_UNAVAILABLE, "message.auth_service_not_available");
    }


    @Override
    public @Nullable User findUserById(@NotNull String id) {
        throw new LocalizedStatusException(SERVICE_UNAVAILABLE, "message.auth_service_not_available");
    }


    @Override
    public void updateUser(@NotNull String userId,
                           @Nullable String firstName,
                           @Nullable String lastName,
                           @Nullable String email,
                           @Nullable String complementaryField) {
        throw new LocalizedStatusException(SERVICE_UNAVAILABLE, "message.auth_service_not_available");
    }


    @Override
    public void updateUserPreferences(@NotNull String userId,
                                      @Nullable String notificationsRedirectionMail,
                                      @Nullable String isNotifiedOnConfidentialFolders,
                                      @Nullable String isNotifiedOnFollowedFolders,
                                      @Nullable String isNotifiedOnLateFolders,
                                      @Nullable String notificationsCronFrequency) {
        throw new LocalizedStatusException(SERVICE_UNAVAILABLE, "message.auth_service_not_available");
    }


    @Override
    public void updateUserInternalMetadata(@NotNull String userId,
                                           @Nullable Integer contentGroupIndex,
                                           @Nullable String contentNodeId,
                                           @Nullable String signatureImageContentId) {
        throw new LocalizedStatusException(SERVICE_UNAVAILABLE, "message.auth_service_not_available");
    }


    @Override
    public void updateUserGlobalPrivileges(@NotNull User user, @NotNull UserDto updatedUser, String specificTenantId) {
        throw new LocalizedStatusException(SERVICE_UNAVAILABLE, "message.auth_service_not_available");
    }


    @Override
    public void resetUserPassword(@NotNull String userId, @NotNull String password) {
        throw new LocalizedStatusException(SERVICE_UNAVAILABLE, "message.auth_service_not_available");
    }


    @Override
    public void deleteUser(@NotNull String userId) {
        throw new LocalizedStatusException(SERVICE_UNAVAILABLE, "message.auth_service_not_available");
    }


    @Override
    public int countLoggedInUsers() {
        throw new LocalizedStatusException(SERVICE_UNAVAILABLE, "message.auth_service_not_available");
    }


    @Override
    public @NotNull Page<User> listUsers(@NotNull Pageable pageable, @Nullable String searchTerm) {
        throw new LocalizedStatusException(SERVICE_UNAVAILABLE, "message.auth_service_not_available");
    }


    @Override
    public @NotNull List<User> getUsersByIds(@NotNull Set<String> userIds) {
        throw new LocalizedStatusException(SERVICE_UNAVAILABLE, "message.auth_service_not_available");
    }


    @Override
    public @NotNull Set<String> filterUsersWithPrivilege(@NotNull Set<String> userIds, @NotNull UserPrivilege privilege, @Nullable String targetTenantId) {
        throw new LocalizedStatusException(SERVICE_UNAVAILABLE, "message.auth_service_not_available");
    }


    @Override
    public @NotNull List<User> getSuperAdminsList(int maxResults) {
        throw new LocalizedStatusException(SERVICE_UNAVAILABLE, "message.auth_service_not_available");
    }


    @Override
    public void addUserToTenant(@NotNull User user, @NotNull String tenantId, @NotNull UserPrivilege privilege) {
        throw new LocalizedStatusException(SERVICE_UNAVAILABLE, "message.auth_service_not_available");
    }


    @Override
    public void addDefaultUserOnTenant(Tenant tenant) {
        throw new LocalizedStatusException(SERVICE_UNAVAILABLE, "message.auth_service_not_available");
    }


    @Override
    public @Nullable User findTenantUserById(@Nullable String tenantId, @NotNull String id) {
        throw new LocalizedStatusException(SERVICE_UNAVAILABLE, "message.auth_service_not_available");
    }


    @Override
    public void updateUserPrivileges(@NotNull String tenantId, @NotNull User user, @NotNull UserPrivilege newPrivilege) {
        throw new LocalizedStatusException(SERVICE_UNAVAILABLE, "message.auth_service_not_available");
    }


    @Override
    public void removeUserFromTenant(@NotNull String userId, @NotNull String tenantId) {
        throw new LocalizedStatusException(SERVICE_UNAVAILABLE, "message.auth_service_not_available");
    }


    @Override
    public @NotNull Page<User> listTenantUsers(@NotNull String tenantId,
                                               @NotNull Pageable pageable,
                                               @Nullable String searchTerm) {
        throw new LocalizedStatusException(SERVICE_UNAVAILABLE, "message.auth_service_not_available");
    }


    @Override
    public @NotNull PaginatedList<User> listTenantAdminUsers(@NotNull String tenantId, int page, int pageSize) {
        throw new LocalizedStatusException(SERVICE_UNAVAILABLE, "message.auth_service_not_available");
    }


    @Override
    public @NotNull String createDesk(@NotNull String tenantId,
                                      @NotNull String name,
                                      @NotNull String shortName,
                                      @Nullable String description,
                                      @Nullable String parentDeskId) {
        throw new LocalizedStatusException(SERVICE_UNAVAILABLE, "message.auth_service_not_available");
    }


    @Override
    public @Nullable Desk findDeskById(@Nullable String tenantId, @NotNull String deskId) {
        throw new LocalizedStatusException(SERVICE_UNAVAILABLE, "message.auth_service_not_available");
    }


    @Override
    public @Nullable Desk findDeskByIdNoException(@Nullable String tenantId, @NotNull String deskId) {
        throw new LocalizedStatusException(SERVICE_UNAVAILABLE, "message.auth_service_not_available");
    }


    @Override
    public void editDesk(@NotNull String tenantId,
                         @NotNull Desk previousDeskVersion,
                         @NotNull String name,
                         @NotNull String shortName,
                         @Nullable String description,
                         @Nullable String directParentId) {
        throw new LocalizedStatusException(SERVICE_UNAVAILABLE, "message.auth_service_not_available");
    }


    @Override
    public void deleteDesk(@NotNull String deskId) {
        throw new LocalizedStatusException(SERVICE_UNAVAILABLE, "message.auth_service_not_available");
    }


    @Override
    public @NotNull Page<Desk> listDesks(@NotNull String tenantKey, @NotNull Pageable pageable, @NotNull List<String> reverseIdList, boolean collapseAll) {
        throw new LocalizedStatusException(SERVICE_UNAVAILABLE, "message.auth_service_not_available");
    }


    @Override
    public @NotNull Page<Desk> searchDesks(@NotNull String tenantKey, @NotNull Pageable pageable, @NotNull List<String> reverseIdList,
                                           @Nullable String searchTerm, boolean collapseAll) {
        throw new LocalizedStatusException(SERVICE_UNAVAILABLE, "message.auth_service_not_available");
    }


    @Override
    public @NotNull Page<DeskRepresentation> findDeskByShortName(@NotNull String tenantId, @NotNull String shortName) {
        throw new LocalizedStatusException(SERVICE_UNAVAILABLE, "message.auth_service_not_available");
    }


    @Override
    public @NotNull Map<String, String> getDeskNames(@NotNull Set<String> deskIds) {
        throw new LocalizedStatusException(SERVICE_UNAVAILABLE, "message.auth_service_not_available");
    }


    @Override
    public void addUsersToDesk(@NotNull String deskId, @NotNull Collection<String> userIdList) {
        throw new LocalizedStatusException(SERVICE_UNAVAILABLE, "message.auth_service_not_available");
    }


    @Override
    public @NotNull Page<DeskRepresentation> getDesksFromUser(@NotNull String userId, Pageable pageable, @Nullable String searchTerm) {
        throw new LocalizedStatusException(SERVICE_UNAVAILABLE, "message.auth_service_not_available");
    }


    @Override
    public void removeUsersFromDesk(@NotNull String deskId, @NotNull Collection<String> userIds) {
        throw new LocalizedStatusException(SERVICE_UNAVAILABLE, "message.auth_service_not_available");
    }


    @Override
    public @NotNull PaginatedList<User> listUsersFromDesk(String tenantId, @NotNull String deskId, int page, int pageSize) {
        throw new LocalizedStatusException(SERVICE_UNAVAILABLE, "message.auth_service_not_available");
    }


    @Override
    public void refreshUsersDeskStatus(@NotNull List<User> userList, @NotNull String deskId) {
        throw new LocalizedStatusException(SERVICE_UNAVAILABLE, "message.auth_service_not_available");
    }


    @Override
    public void populateDeskNamesAndTenantIds(@NotNull Map<String, Desk> deskMap) {
        throw new LocalizedStatusException(SERVICE_UNAVAILABLE, "message.auth_service_not_available");
    }


    @Override
    public void createDeskIntegrityChecks(@NotNull DeskDto request, @NotNull String tenantId) {
        throw new LocalizedStatusException(SERVICE_UNAVAILABLE, "message.auth_service_not_available");
    }


}
