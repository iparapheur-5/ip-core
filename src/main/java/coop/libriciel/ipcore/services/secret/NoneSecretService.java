/*
 * iparapheur Core
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.ipcore.services.secret;

import coop.libriciel.ipcore.model.crypto.SealCertificate;
import coop.libriciel.ipcore.model.crypto.SealCertificateRepresentation;
import coop.libriciel.ipcore.utils.LocalizedStatusException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import static coop.libriciel.ipcore.utils.TextUtils.NONE_SERVICE;
import static org.springframework.http.HttpStatus.SERVICE_UNAVAILABLE;


@Service(SecretServiceInterface.BEAN_NAME)
@ConditionalOnProperty(name = SecretServiceInterface.PREFERENCES_PROVIDER_KEY, havingValue = NONE_SERVICE)
public class NoneSecretService implements SecretServiceInterface {


    @Override
    public void storeSealCertificate(@NotNull String tenantId, @NotNull SealCertificate sealCertificate) {
        throw new LocalizedStatusException(SERVICE_UNAVAILABLE, "message.secret_service_service_not_available");
    }


    @Override
    public @Nullable SealCertificate getSealCertificate(@NotNull String tenantId, @NotNull String sealCertificateId) {
        throw new LocalizedStatusException(SERVICE_UNAVAILABLE, "message.secret_service_service_not_available");
    }


    @Override
    public void deleteSealCertificate(@NotNull String tenantId, @NotNull String sealCertificateId) {
        throw new LocalizedStatusException(SERVICE_UNAVAILABLE, "message.secret_service_service_not_available");
    }


    @Override
    public @NotNull Page<SealCertificateRepresentation> getSealCertificateList(@NotNull String tenantId, @NotNull Pageable pageable) {
        throw new LocalizedStatusException(SERVICE_UNAVAILABLE, "message.secret_service_service_not_available");
    }


    @Override
    public void deleteTenant(@NotNull String tenantId) {
        throw new LocalizedStatusException(SERVICE_UNAVAILABLE, "message.secret_service_service_not_available");
    }


}
