/*
 * Workflow
 * Copyright (C) 2019-2020 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package coop.libriciel.ipcore.services.groovy;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.script.ScriptEngine;
import java.util.List;
import java.util.Optional;

import static java.util.Collections.emptyList;


/**
 * An instance of this will be passed to a Groovy's {@link ScriptEngine}.
 * A convenient way to catch and retrieve the script's results.
 */
@Data
@NoArgsConstructor
public class GroovyResultCatcher {


    private String workflowDefinitionId;
    private List<String> variableDesks;


    /**
     * This will be called reflexively from the engine.
     */
    public void setResult(@NotNull String catchWorkflowDefinitionId, @Nullable List<String> catchVariableDesks) {

        // We only want a single result.
        // Only the first one will count, we'll ignore the following ones
        if (StringUtils.isNotEmpty(workflowDefinitionId)) {
            return;
        }

        workflowDefinitionId = catchWorkflowDefinitionId;
        variableDesks = Optional.ofNullable(catchVariableDesks).orElse(emptyList());
    }


}