/*
 * Workflow
 * Copyright (C) 2019-2020 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package coop.libriciel.ipcore.services.groovy;

import coop.libriciel.ipcore.model.auth.DeskRepresentation;
import coop.libriciel.ipcore.services.auth.AuthServiceInterface;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.Nullable;

import javax.script.ScriptEngine;
import java.util.Objects;
import java.util.Optional;


/**
 * An instance of this will be passed to a Groovy's {@link ScriptEngine}.
 * This allows us to reduce Groovy's access
 */
@Log4j2
@AllArgsConstructor
public class GroovyInjectableMethods {


    private final AuthServiceInterface authService;
    private final String tenantId;


    /**
     * This will be called reflexively from the engine.
     */
    @SuppressWarnings("unused")
    public boolean isDesk(@Nullable String deskId) {

        if (StringUtils.isEmpty(deskId)) {
            return false;
        }

        return Optional.of(deskId)
                .map(i -> authService.findDeskByIdNoException(tenantId, i))
                // Maybe this is not a deskId, but a shortName.
                // This was the v4 way. Let's check...
                .or(() -> authService.findDeskByShortName(tenantId, deskId)
                        .stream()
                        .map(DeskRepresentation::getId)
                        .map(id -> authService.findDeskByIdNoException(tenantId, id))
                        .filter(Objects::nonNull)
                        .filter(desk -> StringUtils.equals(desk.getShortName(), deskId))
                        .findFirst()
                )
                .isPresent();
    }


    /**
     * This will be called reflexively from the engine.
     */
    @SuppressWarnings("unused")
    public void log(@Nullable String message) {
        log.warn("Selection script tenantId:{} log:{}", tenantId, message);
    }


}