/*
 * iparapheur Core
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.ipcore.services.content;

import com.alfresco.client.api.core.ApiClient;
import com.alfresco.client.api.core.client.NodesApi;
import com.alfresco.client.api.core.model.*;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import coop.libriciel.ipcore.model.content.DetachedSignature;
import coop.libriciel.ipcore.model.content.Document;
import coop.libriciel.ipcore.model.content.DocumentBuffer;
import coop.libriciel.ipcore.model.content.SignatureProof;
import coop.libriciel.ipcore.model.content.alfresco.AlfrescoEntry;
import coop.libriciel.ipcore.model.content.alfresco.AlfrescoNode;
import coop.libriciel.ipcore.model.content.alfresco.AlfrescoTicketRequest;
import coop.libriciel.ipcore.model.database.TemplateType;
import coop.libriciel.ipcore.model.database.Tenant;
import coop.libriciel.ipcore.model.pdfstamp.Layer;
import coop.libriciel.ipcore.model.pdfstamp.SignaturePlacement;
import coop.libriciel.ipcore.model.workflow.Folder;
import coop.libriciel.ipcore.model.workflow.Task;
import coop.libriciel.ipcore.utils.*;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.StringSubstitutor;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.core.io.buffer.DataBufferUtils;
import org.springframework.core.io.buffer.DefaultDataBufferFactory;
import org.springframework.http.client.MultipartBodyBuilder;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientException;
import org.springframework.web.util.UriComponentsBuilder;
import reactor.core.publisher.Flux;

import javax.annotation.PostConstruct;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.util.*;

import static com.google.common.base.MoreObjects.firstNonNull;
import static coop.libriciel.ipcore.model.content.alfresco.AlfrescoNode.*;
import static coop.libriciel.ipcore.model.content.alfresco.AlfrescoProperties.*;
import static coop.libriciel.ipcore.model.workflow.Action.START;
import static coop.libriciel.ipcore.utils.RequestUtils.SUBSERVICE_REQUEST_LONG_TIMEOUT;
import static coop.libriciel.ipcore.utils.RequestUtils.SUBSERVICE_REQUEST_SHORT_TIMEOUT;
import static coop.libriciel.ipcore.utils.TextUtils.INTERNAL_PREFIX;
import static java.lang.Integer.MAX_VALUE;
import static java.nio.charset.StandardCharsets.UTF_8;
import static java.util.Calendar.*;
import static java.util.Collections.emptyList;
import static java.util.Collections.emptyMap;
import static java.util.Comparator.*;
import static java.util.Locale.ROOT;
import static org.apache.commons.io.FilenameUtils.removeExtension;
import static org.apache.pdfbox.io.MemoryUsageSetting.setupTempFileOnly;
import static org.springframework.http.HttpHeaders.CONTENT_DISPOSITION;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;
import static org.springframework.http.HttpStatus.NOT_FOUND;
import static org.springframework.http.MediaType.*;
import static org.springframework.security.config.Elements.HTTP;


@Log4j2
@Service(ContentServiceInterface.BEAN_NAME)
@ConditionalOnProperty(name = ContentServiceInterface.PREFERENCES_PROVIDER_KEY, havingValue = AlfrescoService.PROVIDER_NAME)
public class AlfrescoService implements ContentServiceInterface {

    public static final String PROVIDER_NAME = "alfresco";

    private static final String AUTHORITY_GROUP_PLACEHOLDER = "authorityGroup";
    private static final String AUTHORITY_ID_PLACEHOLDER = "authorityId";
    private static final String YEAR_PLACEHOLDER = "year";
    private static final String MONTH_PLACEHOLDER = "month";
    private static final String DAY_PLACEHOLDER = "day";
    private static final String HOUR_PLACEHOLDER = "hour";
    private static final String MINUTE_PLACEHOLDER = "minute";
    private static final String USER_GROUP_PLACEHOLDER = "userGroup";
    private static final String USER_ID_PLACEHOLDER = "userId";
    private static final String DATA_PATH_AUTHORITIES_GROUP = String.format("authorities/${%s}", AUTHORITY_GROUP_PLACEHOLDER);
    private static final String DATA_PATH_TENANT = String.format("%s/${%s}", DATA_PATH_AUTHORITIES_GROUP, AUTHORITY_ID_PLACEHOLDER);
    private static final String USER_DATA_PATH = String.format("user_data/${%s}/${%s}", USER_GROUP_PLACEHOLDER, USER_ID_PLACEHOLDER);
    private static final String FOLDER_DATA_PATH = String.format("%s/folders/${%s}/${%s}-${%s}/${%s}-${%s}",
            DATA_PATH_TENANT, YEAR_PLACEHOLDER, MONTH_PLACEHOLDER, DAY_PLACEHOLDER, HOUR_PLACEHOLDER, MINUTE_PLACEHOLDER);

    private static final String TICKET_URL = "alfresco/api/-default-/public/authentication/versions/1/tickets";
    private static final String NODES_URL = "alfresco/api/-default-/public/alfresco/versions/1/nodes";
    private static final String ROOT_NODE = "-root-";
    private static final String NODE_CHILDREN = "children";
    private static final String NODE_CONTENT = "content";
    private static final String PROPERTIES_KEY = "properties";

    public static final String PREMIS_NODE_FILE_NAME = INTERNAL_PREFIX + "premis.xml";

    private static final Comparator<DetachedSignature> DETACHED_SIGNATURE_COMPARATOR = comparing(DetachedSignature::getTargetTaskId, nullsLast(naturalOrder()))
            .thenComparing(DetachedSignature::getName, nullsLast(naturalOrder()))
            .thenComparing(DetachedSignature::getId, nullsLast(naturalOrder()));


    private String authHeader = null;
    private ApiClient apiClient;


    // <editor-fold desc="Beans">


    private final ContentServiceProperties properties;
    private final ObjectMapper objectMapper;
    private final WebClient.Builder sharedWebClientBuilder;


    @Autowired
    public AlfrescoService(ContentServiceProperties properties,
                           ObjectMapper objectMapper,
                           WebClient.Builder sharedWebClientBuilder) {
        this.properties = properties;
        this.objectMapper = objectMapper;
        this.sharedWebClientBuilder = sharedWebClientBuilder;
    }


    // </editor-fold desc="Beans">


    @PostConstruct
    public void setup() {

        // Proper way...

        URI alfrescoUri = UriComponentsBuilder.newInstance()
                .scheme(HTTP)
                .host(properties.getHost())
                .port(properties.getPort())
                .path("alfresco/api/-default-/public/alfresco/versions/1")
                .build().normalize().toUri();

        apiClient = new ApiClient();
        apiClient.setBasePath(alfrescoUri.toString());
        apiClient.setUsername(properties.getUsername());
        apiClient.setPassword(properties.getPassword());

        // Old way...

        try {
            generateHeaders();
        } catch (WebClientException | IpInternalException e) {
            log.warn("Alfresco is not ready yet...");
        }
    }


    /**
     * This is only used for the few remaining un-library-ised methods.
     * It should be deleted once the OpenApiGenerator bug will be fixed.
     * <p>
     * TODO: Maybe we could library-ized its inner method... ?
     */
    @Scheduled(fixedDelayString = "${services.content.ticket-delay}")
    protected void generateHeaders() {

        log.debug("Requesting Alfresco ticket ...");

        URI requestUri = UriComponentsBuilder.newInstance()
                .scheme(HTTP).host(properties.getHost()).port(properties.getPort())
                .path(TICKET_URL)
                .build().normalize().toUri();

        try {
            sharedWebClientBuilder.build()
                    .post().uri(requestUri)
                    .contentType(APPLICATION_JSON).accept(APPLICATION_JSON)
                    .bodyValue(new AlfrescoTicketRequest(properties.getUsername(), properties.getPassword()))
                    .retrieve()
                    // Result
                    .bodyToMono(new ParameterizedTypeReference<AlfrescoEntry<AlfrescoNode>>() {})
                    .blockOptional(SUBSERVICE_REQUEST_SHORT_TIMEOUT)
                    .map(AlfrescoEntry::getEntry)
                    .map(AlfrescoNode::getId)
                    .map(i -> Base64.getEncoder().encodeToString(i.getBytes()))
                    // https://docs.alfresco.com/6.1/concepts/dev-api-by-language-alf-rest-auth-with-repo.html
                    .ifPresent(b -> authHeader = b);
        } catch (IllegalStateException e) {
            // catching timeout and rethrow an exception with a friendlier message
            throw new IpInternalException(
                    "The content service (alfresco) did not respond to header request after 5 seconds, it is probably down or malfunctionning");
        }
    }


    // <editor-fold desc="Utils">


    private @NotNull String getAlfrescoHeader() {

        if (StringUtils.isNotEmpty(authHeader)) {
            return authHeader;
        }

        generateHeaders();
        return authHeader;
    }


    static @NotNull String computePathPlaceholders(@NotNull Tenant tenant) {
        return StringSubstitutor.replace(
                FOLDER_DATA_PATH,
                Map.of(
                        AUTHORITY_GROUP_PLACEHOLDER, String.valueOf(tenant.getIndex() / 1000),
                        AUTHORITY_ID_PLACEHOLDER, tenant.getId(),
                        YEAR_PLACEHOLDER, Integer.toString(Calendar.getInstance().get(YEAR)),
                        MONTH_PLACEHOLDER, Integer.toString(Calendar.getInstance().get(MONTH)),
                        DAY_PLACEHOLDER, Integer.toString(Calendar.getInstance().get(DAY_OF_MONTH)),
                        HOUR_PLACEHOLDER, Integer.toString(Calendar.getInstance().get(HOUR)),
                        MINUTE_PLACEHOLDER, Integer.toString(Calendar.getInstance().get(MINUTE))
                ));
    }


    private @NotNull String createFolder(@NotNull NodeBodyCreate nodeBodyCreate) {
        return new NodesApi(apiClient)
                .createNode(ROOT_NODE, nodeBodyCreate, true, null, null, null, null)
                .doOnError(e -> {throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, e, "message.cannot_reach_content_service");})
                .blockOptional(SUBSERVICE_REQUEST_LONG_TIMEOUT)
                .map(nodeEntry -> nodeEntry.getEntry().getId())
                .orElseThrow(() -> new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.cannot_reach_content_service"));
    }


    private @NotNull String createContent(@NotNull String contentId, @NotNull MultipartBodyBuilder builder) {

        // TODO : This could be library-ised, but we'll have to refactor the MultipartBodyBuilder into a NodeCreateRequest.
        //  A little too big for the moment, it will deserve a dedicated MR

        URI createContentRequestUri = UriComponentsBuilder.newInstance()
                .scheme(HTTP).host(properties.getHost()).port(properties.getPort())
                .path(NODES_URL).pathSegment(contentId).pathSegment(NODE_CHILDREN)
                .build().normalize().toUri();

        return sharedWebClientBuilder.build()
                .post().uri(createContentRequestUri)
                .headers(headers -> headers.setBasicAuth(getAlfrescoHeader()))
                .contentType(MULTIPART_FORM_DATA).accept(APPLICATION_JSON)
                .body(BodyInserters.fromMultipartData(builder.build()))
                .exchange()
                // Result
                .doOnError(e -> {throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, e, "message.cannot_reach_content_service");})
                .blockOptional(SUBSERVICE_REQUEST_LONG_TIMEOUT)
                .orElseThrow(() -> new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.cannot_write_on_content_service"))
                .bodyToMono(new ParameterizedTypeReference<AlfrescoEntry<AlfrescoNode>>() {})
                .blockOptional(SUBSERVICE_REQUEST_LONG_TIMEOUT)
                .orElseThrow(() -> new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.cannot_parse_content_service_response"))
                .getEntry()
                .getId();
    }


    private @NotNull String updateContent(@NotNull String nodeId, @NotNull DocumentBuffer documentBuffer, boolean updateFileName) {
        String updatedName = updateFileName ? documentBuffer.getName() : null;

        try (InputStream inputStream = RequestUtils.bufferToInputStream(documentBuffer)) {
            InputStreamResource resource = new InputStreamResource(inputStream);

            return new NodesApi(apiClient)
                    .updateNodeContent(nodeId, resource, true, null, updatedName, emptyList(), emptyList())
                    .doOnError(e -> {throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, e, "message.cannot_reach_content_service");})
                    .blockOptional(SUBSERVICE_REQUEST_LONG_TIMEOUT)
                    .map(NodeEntry::getEntry)
                    .map(Node::getId)
                    .orElseThrow(() -> new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.cannot_reach_content_service"));

        } catch (IOException e) {
            throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.cannot_reach_content_service");
        }
    }


    private @NotNull Node getNode(@NotNull String nodeId) {
        return new NodesApi(apiClient)
                .getNode(nodeId, null, null, null)
                .doOnError(e -> {throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, e, "message.cannot_reach_content_service");})
                .blockOptional(SUBSERVICE_REQUEST_SHORT_TIMEOUT)
                .map(NodeEntry::getEntry)
                .orElseThrow(() -> new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.cannot_reach_content_service"));
    }


    /**
     * This method cannot be library-ised yet.
     * The standard OpenApiGenerator downloads everything in a temp file, and returns the {@link File} object.
     * This is specific to the WebClient generation, it works well on an older RestTemplate lib.
     *
     * @param nodeId
     * @return
     * @see <a href="https://stackoverflow.com/questions/72142403/useabstractionforfiles-openapi-webclient-not-working">The StackOverflow associate issue</a>
     */
    private @NotNull DocumentBuffer getContent(@NotNull String nodeId) {

        URI requestUri = UriComponentsBuilder.newInstance()
                .scheme(HTTP).host(properties.getHost()).port(properties.getPort())
                .path(NODES_URL).pathSegment(nodeId).pathSegment(NODE_CONTENT)
                .queryParam("attachment", true)
                .build().normalize().toUri();

        DocumentBuffer result = new DocumentBuffer();

        Flux<DataBuffer> dataFlux = sharedWebClientBuilder.build()
                .get().uri(requestUri)
                .headers(headers -> headers.setBasicAuth(getAlfrescoHeader()))
                .accept(APPLICATION_OCTET_STREAM)
                .exchangeToFlux(response -> RequestUtils.clientResponseToDataBufferFlux(response, result, nodeId))
                .doOnError(e -> {throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, e, "message.cannot_reach_content_service");});

        result.setContentFlux(dataFlux);

        return result;
    }


    private void deleteContent(@NotNull String nodeId) {
        log.debug("AlfrescoService deleteContent:{}", nodeId);
        new NodesApi(apiClient)
                .deleteNode(nodeId, false)
                .doOnError(e -> {throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, e, "message.cannot_reach_content_service");})
                .blockOptional(SUBSERVICE_REQUEST_SHORT_TIMEOUT);
    }


    // </editor-fold desc="Utils">


    @Override
    public @NotNull String updateDocumentAlfrescoProperties(@NotNull Document document) {

        NodeBodyUpdate updateRequest = new NodeBodyUpdate();
        try {
            updateRequest.setProperties(Map.of(
                    PROPERTY_DELETABLE, objectMapper.writeValueAsString(document.isDeletable()),
                    PROPERTY_ANNOTATIONS, objectMapper.writeValueAsString(document.getSignaturePlacementAnnotations()),
                    PROPERTY_SIGNATURE_TAGS, objectMapper.writeValueAsString(document.getSignatureTags()),
                    PROPERTY_SEAL_TAGS, objectMapper.writeValueAsString(document.getSealTags())
            ));
        } catch (JsonProcessingException e) {
            throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.error_writing_request", e);
        }

        return new NodesApi(apiClient)
                .updateNode(document.getId(), updateRequest, emptyList(), emptyList())
                .doOnError(e -> {throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, e, "message.cannot_reach_content_service");})
                .blockOptional(SUBSERVICE_REQUEST_LONG_TIMEOUT)
                .map(NodeEntry::getEntry)
                .map(Node::getId)
                .orElseThrow(() -> new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.cannot_reach_content_service"));
    }


    @Override
    public void removeFirstSignaturePlacementAnnotation(List<Document> mainDocumentList) {
        mainDocumentList
                .stream()
                .filter(doc -> !doc.getSignaturePlacementAnnotations().isEmpty())
                .forEach(doc -> {
                    doc.getSignaturePlacementAnnotations().sort(Comparator.comparingInt(SignaturePlacement::getSignatureNumber));
                    doc.getSignaturePlacementAnnotations().remove(0);
                    updateDocumentAlfrescoProperties(doc);
                });
    }


    @Override
    public @NotNull String createTenant(@NotNull Tenant tenant) {

        String tenantPath = StringSubstitutor.replace(
                DATA_PATH_AUTHORITIES_GROUP,
                Map.of(AUTHORITY_GROUP_PLACEHOLDER, String.valueOf(tenant.getIndex() / 1000))
        );

        NodeBodyCreate request = new NodeBodyCreate();
        request.setName(tenant.getId());
        request.setNodeType(NODE_TYPE_NATIVE_FOLDER);
        request.setRelativePath(tenantPath);
        request.setProperties(emptyMap());

        String newNodeId = createFolder(request);
        log.debug("createTenant response create:{}", newNodeId);
        return newNodeId;
    }


    @Override
    public void deleteTenant(@NotNull Tenant tenant) {
        log.debug("deleteTenant id:{} contentId:{}", tenant.getId(), tenant.getContentId());
        deleteContent(tenant.getContentId());
    }


    @Override
    public @NotNull String createUserData(@NotNull String userId, int userDataGroupIndex) {

        String userDataPath = StringSubstitutor.replace(
                USER_DATA_PATH,
                Map.of(
                        USER_GROUP_PLACEHOLDER, String.valueOf(userDataGroupIndex / 1000),
                        USER_ID_PLACEHOLDER, userId
                ));

        NodeBodyCreate request = new NodeBodyCreate();
        request.setName(userId);
        request.setNodeType(NODE_TYPE_NATIVE_FOLDER);
        request.setRelativePath(userDataPath);
        request.setProperties(emptyMap());

        String newNodeId = createFolder(request);
        log.debug("createUserData response create:{}", newNodeId);
        return newNodeId;
    }


    @Override
    public void deleteUserData(@NotNull String contentId) {
        deleteContent(contentId);
    }


    // <editor-fold desc="Signature image CRUD">


    @Override
    public @NotNull String createUserSignatureImage(@NotNull String userDataContentId, @NotNull DocumentBuffer file) {

        log.debug("createSignatureImage userDataContentId:{} docLength:{}", userDataContentId, file.getContentLength());

        String fileName = StringUtils.firstNonEmpty(file.getName(), "(No name)");

        MultipartBodyBuilder builder = new MultipartBodyBuilder();
        builder.part("name", file.getName());
        builder.part("nodeType", NODE_TYPE_NATIVE_CONTENT);
        builder.part("autoRename", true);

        // For some reason, header seems mandatory for the asyncPart, and not for the other ones.
        // Cheers to this article : https://dev.to/shavz/sending-multipart-form-data-using-spring-webtestclient-2gb7
        builder.asyncPart("filedata", file.getContentFlux(), DataBuffer.class)
                .header(CONTENT_DISPOSITION, "form-data; name=filedata; filename=" + fileName);

        // Upload content

        return createContent(userDataContentId, builder);
    }


    @Override
    public @NotNull String createSealCertificateSignatureImage(@NotNull Tenant tenant, @NotNull String sealCertificateId, @NotNull DocumentBuffer file) {

        log.debug("createSealCertificateSignatureImage certificateId:{} docLength:{}", sealCertificateId, file.getContentLength());

        String fileName = StringUtils.firstNonEmpty(file.getName(), "(No name)");

        MultipartBodyBuilder builder = new MultipartBodyBuilder();
        builder.part("name", file.getName());
        builder.part("nodeType", NODE_TYPE_NATIVE_CONTENT);
        builder.part("relativePath", String.format("seal_certificate_data/%s/signatureImage", sealCertificateId));
        builder.part("autoRename", false);

        // For some reason, header seems mandatory for the asyncPart, and not for the other ones.
        // Cheers to this article : https://dev.to/shavz/sending-multipart-form-data-using-spring-webtestclient-2gb7
        builder.asyncPart("filedata", file.getContentFlux(), DataBuffer.class)
                .header(CONTENT_DISPOSITION, "form-data; name=filedata; filename=" + fileName);

        // Upload content

        return createContent(tenant.getContentId(), builder);
    }


    @Override
    public void updateSignatureImage(@NotNull String imageNodeId, @NotNull DocumentBuffer file) {
        log.debug("updateSignatureImage imageNodeId:{} docLength:{}", imageNodeId, file.getContentLength());
        String nodeId = updateContent(imageNodeId, file, true);
        log.debug("updateDocument done nodeId:{}", nodeId);
    }


    @Override
    public @NotNull DocumentBuffer getSignatureImage(@NotNull String signatureNodeId) {
        log.debug("getSignatureImage id:{}", signatureNodeId);
        DocumentBuffer result = getContent(signatureNodeId);
        log.debug("getSignatureImage id:{} length:{}", signatureNodeId, result.getContentLength());
        return result;
    }


    @Override
    public void deleteSignatureImage(@NotNull String signatureNodeId) {
        deleteContent(signatureNodeId);
    }


    // </editor-fold desc="Signature image CRUD">


    @Override
    public @NotNull String createFolder(@NotNull Tenant tenant) {

        NodeBodyCreate request = new NodeBodyCreate();
        request.setName("draft");
        request.setNodeType(NODE_TYPE_IP_FOLDER);
        request.setRelativePath(computePathPlaceholders(tenant));
        request.setProperties(emptyMap());

        log.debug("createFolder request:{}", request);
        String newNodeId = createFolder(request);
        log.debug("createFolder response create:{}", newNodeId);
        return newNodeId;
    }


    @Override
    public void updateFolderMetadata(@NotNull String folderContentId, @NotNull String name) {

        NodeBodyUpdate updateRequest = new NodeBodyUpdate();
        updateRequest.setName(name);

        new NodesApi(apiClient)
                .updateNode(folderContentId, updateRequest, emptyList(), emptyList())
                .doOnError(e -> {throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, e, "message.cannot_reach_content_service");})
                .blockOptional(SUBSERVICE_REQUEST_SHORT_TIMEOUT)
                .orElseThrow(() -> new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.cannot_reach_content_service"));
    }


    @Override
    public @NotNull String createDocument(@NotNull String tenantId, @NotNull Folder folder, @NotNull DocumentBuffer documentBuffer) {

        log.debug("createDocument folderContentId:{} docLength:{}", folder.getContentId(), documentBuffer.getContentLength());

        String fileName = firstNonNull(documentBuffer.getName(), "(No name)");

        MultipartBodyBuilder builder = new MultipartBodyBuilder();
        builder.part("name", fileName);
        builder.part("nodeType", NODE_TYPE_IP_DOCUMENT);
        builder.part("autoRename", true);
        builder.part(PROPERTY_INDEX, documentBuffer.getIndex());
        builder.part(PROPERTY_DELETABLE, true);
        builder.part(PROPERTY_IS_MAIN_DOCUMENT, documentBuffer.isMainDocument());

        // For some reason, header seems mandatory for the asyncPart, and not for the other ones.
        // Cheers to this article : https://dev.to/shavz/sending-multipart-form-data-using-spring-webtestclient-2gb7
        builder.asyncPart("filedata", documentBuffer.getContentFlux(), DataBuffer.class)
                .header(CONTENT_DISPOSITION, "form-data; name=filedata; filename=" + fileName);

        // Upload content

        String documentId = createContent(folder.getContentId(), builder);
        documentBuffer.setId(documentId);

        // If we're in a main document, we have to fetch the signature positioning tag.

        if (documentBuffer.isMainDocument() && (APPLICATION_PDF.equalsTypeAndSubtype(documentBuffer.getMediaType()))) {

            try (InputStream documentInputStream = retrievePipedDocument(documentBuffer.getId());
                 PDDocument pdDocument = PDDocument.load(documentInputStream, setupTempFileOnly())) {

                String signatureTag = properties.getTag().getSignature().getDefaultValue();
                if (Objects.nonNull(properties.getTag().getSignature().getTenants())
                        && properties.getTag().getSignature().getTenants().containsKey(tenantId)) {
                    signatureTag = properties.getTag().getSignature().getTenants().get(tenantId);
                }

                String sealTag = properties.getTag().getSeal().getDefaultValue();
                if (Objects.nonNull(properties.getTag().getSeal().getTenants())
                        && properties.getTag().getSeal().getTenants().containsKey(tenantId)) {
                    sealTag = properties.getTag().getSeal().getTenants().get(tenantId);
                }

                PdfTextLocationStripper stripper = new PdfTextLocationStripper(signatureTag, sealTag);
                stripper.search(pdDocument);
                documentBuffer.setSignatureTags(stripper.getSignatureTagsFound());
                documentBuffer.setSealTags(stripper.getSealTagsFound());
                log.debug("PdfTextLocationStripper results signPosition:{}", stripper.getSignatureTagsFound());
                log.debug("PdfTextLocationStripper results sealPosition:{}", stripper.getSealTagsFound());

                this.updateDocumentAlfrescoProperties(documentBuffer);
            } catch (IOException e) {
                log.error("Cannot determine signature-position tag location", e);
            }
        }

        return documentId;
    }


    @Override
    public @NotNull String createDetachedSignature(@NotNull String folderContentId,
                                                   @NotNull Document targetDocument,
                                                   @NotNull DocumentBuffer documentBuffer,
                                                   @Nullable Task task) {
        log.debug(
                "createSignature folderContentId:{} taskId:{} taskIndex:{} docLength:{}",
                folderContentId,
                Optional.ofNullable(task).map(Task::getId).orElse(null),
                Optional.ofNullable(task).map(Task::getStepIndex).orElse(null),
                documentBuffer.getContentLength());

        String fileName = Optional
                .ofNullable(task)
                .map(t -> String.format(
                        "%s-%d-%s.%s",
                        removeExtension(targetDocument.getName()),
                        (t.getAction() == START) ? 0 : t.getStepIndex() + 1,
                        (t.getAction() == START) ? "signature_externe" : t.getUser().getFirstName() + " " + t.getUser().getLastName(),
                        FileUtils.isXml(documentBuffer.getMediaType()) ? "xml" : "p7s"
                ))
                .orElse(documentBuffer.getName());

        MultipartBodyBuilder builder = new MultipartBodyBuilder();
        builder.part("name", fileName);
        builder.part("nodeType", NODE_TYPE_IP_DETACHED_SIGNATURE);
        builder.part("autoRename", true);
        builder.part(PROPERTY_TARGET_DOCUMENT_ID, targetDocument.getId());

        Optional.ofNullable(task).map(t -> builder.part(PROPERTY_TARGET_TASK_ID, t.getId()));

        // For some reason, header seems mandatory for the asyncPart, and not for the other ones.
        // Cheers to this article : https://dev.to/shavz/sending-multipart-form-data-using-spring-webtestclient-2gb7
        builder.asyncPart("filedata", documentBuffer.getContentFlux(), DataBuffer.class)
                .header(CONTENT_DISPOSITION, "form-data; name=filedata; filename=" + fileName);

        // Upload content

        return createContent(folderContentId, builder);
    }


    @Override
    public @NotNull String createSignatureProof(@NotNull String folderContentId,
                                                @NotNull String taskId,
                                                @NotNull DocumentBuffer documentBuffer) {
        log.debug(
                "createSignatureProof folderContentId:{}, docLength:{}",
                folderContentId,
                documentBuffer.getContentLength()
        );

        MultipartBodyBuilder builder = new MultipartBodyBuilder();
        builder.part("name", documentBuffer.getName());
        builder.part("nodeType", NODE_TYPE_IP_SIGNATURE_PROOF);
        builder.part("autoRename", true);
        builder.part(PROPERTY_TASK_ID, taskId);
        builder.part(PROPERTY_IS_MAIN_DOCUMENT, false);

        builder.asyncPart("filedata", documentBuffer.getContentFlux(), DataBuffer.class)
                .header(CONTENT_DISPOSITION, "form-data; name=filedata; filename=" + documentBuffer.getName());


        return createContent(folderContentId, builder);
    }


    @Override
    public @NotNull String createPremisDocument(@NotNull Folder folder, @NotNull String premis) {
        log.debug("createPremisDocument folderContentId:{} docLength:{}", folder.getContentId(), premis.length());

        // Cleanup

        String existingPremisNodeId = folder.getPremisNodeId();
        log.debug("createPremisDocument existingPremisNode:{}", existingPremisNodeId);

        // Create or update content

        if (StringUtils.isEmpty(existingPremisNodeId)) {

            MultipartBodyBuilder builder = new MultipartBodyBuilder();
            builder.part("name", PREMIS_NODE_FILE_NAME);
            builder.part("nodeType", NODE_TYPE_NATIVE_CONTENT);
            builder.part("autoRename", true);

            // For some reason, header seems mandatory for the asyncPart, and not for the other ones.
            // Cheers to this article : https://dev.to/shavz/sending-multipart-form-data-using-spring-webtestclient-2gb7
            builder.part("filedata", premis)
                    .header(CONTENT_DISPOSITION, "form-data; name=filedata; filename=" + PREMIS_NODE_FILE_NAME);

            String premisNodeId = createContent(folder.getContentId(), builder);
            log.debug("createPremisDocument nodeId:{}", premisNodeId);
            return premisNodeId;

        } else {

            // For some reason, creating and updating nodes doesn't have the same pipes.
            // We have to go through a multipart request for a create, and an octet-stream for an update.
            // That's why we initiate the documentBuffer here.

            // Note, the DataBufferUtils closes the given stream
            InputStream body = new ByteArrayInputStream(premis.getBytes(UTF_8));
            DocumentBuffer premisDocument = new DocumentBuffer();
            premisDocument.setName(PREMIS_NODE_FILE_NAME);
            premisDocument.setContentFlux(DataBufferUtils.readInputStream(() -> body, new DefaultDataBufferFactory(), FILE_TRANSFER_BUFFER_SIZE));
            updateContent(existingPremisNodeId, premisDocument, true);

            return existingPremisNodeId;
        }
    }


    // <editor-fold desc="Document CRUDL">


    /**
     * TODO : the 3 requests in a single one ?
     *
     * @param folderContentId
     * @return
     */
    @Override
    public @NotNull List<Document> getDocumentList(@NotNull String folderContentId) {

        List<Document> documentList = new NodesApi(apiClient)
                .listNodeChildren(
                        folderContentId,
                        0,
                        MAX_VALUE,
                        null,
                        String.format("(nodeType='%s')", NODE_TYPE_IP_DOCUMENT),
                        List.of(PROPERTIES_KEY),
                        null,
                        false,
                        List.of("nodeType", "name", "id", "content")
                )
                .doOnError(e -> {throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, e, "message.cannot_reach_content_service");})
                .blockOptional(SUBSERVICE_REQUEST_SHORT_TIMEOUT)
                .map(NodeChildAssociationPaging::getList)
                .map(NodeChildAssociationPagingList::getEntries)
                .orElseThrow(() -> new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.cannot_reach_content_service"))
                .stream()
                .map(NodeChildAssociationEntry::getEntry)
                .map(Document::new)
                .toList();

        List<SignatureProof> proofFiles = new NodesApi(apiClient)
                .listNodeChildren(
                        folderContentId,
                        0,
                        MAX_VALUE,
                        null,
                        String.format("(nodeType='%s')", AlfrescoNode.NODE_TYPE_IP_SIGNATURE_PROOF),
                        List.of(PROPERTIES_KEY),
                        null,
                        false,
                        List.of("nodeType", "name", "id", "content")
                )
                .doOnError(e -> {throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, e, "message.cannot_reach_content_service");})
                .blockOptional(SUBSERVICE_REQUEST_SHORT_TIMEOUT)
                .map(NodeChildAssociationPaging::getList)
                .map(NodeChildAssociationPagingList::getEntries)
                .orElseThrow(() -> new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.cannot_reach_content_service"))
                .stream()
                .map(NodeChildAssociationEntry::getEntry)
                .map(SignatureProof::new)
                .toList();

        List<DetachedSignature> signatureList = new NodesApi(apiClient)
                .listNodeChildren(
                        folderContentId,
                        0,
                        MAX_VALUE,
                        null,
                        String.format("(nodeType='%s')", NODE_TYPE_IP_DETACHED_SIGNATURE),
                        List.of(PROPERTIES_KEY),
                        null,
                        false,
                        List.of("nodeType", "name", "id", "content")
                )
                .doOnError(e -> {throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, e, "message.cannot_reach_content_service");})
                .blockOptional(SUBSERVICE_REQUEST_SHORT_TIMEOUT)
                .map(NodeChildAssociationPaging::getList)
                .map(NodeChildAssociationPagingList::getEntries)
                .orElseThrow(() -> new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.cannot_reach_content_service"))
                .stream()
                .map(NodeChildAssociationEntry::getEntry)
                .map(DetachedSignature::new)
                .toList();

        List<NodeChildAssociation> visualPdfList = new NodesApi(apiClient)
                .listNodeChildren(
                        folderContentId,
                        0,
                        MAX_VALUE,
                        null,
                        String.format("(nodeType='%s')", NODE_TYPE_IP_PDF_VISUAL),
                        List.of(PROPERTIES_KEY),
                        null,
                        false,
                        List.of("nodeType", "name", "id", "content")
                )
                .doOnError(e -> {throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, e, "message.cannot_reach_content_service");})
                .blockOptional(SUBSERVICE_REQUEST_SHORT_TIMEOUT)
                .map(NodeChildAssociationPaging::getList)
                .map(NodeChildAssociationPagingList::getEntries)
                .orElseThrow(() -> new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.cannot_reach_content_service"))
                .stream()
                .map(NodeChildAssociationEntry::getEntry)
                .toList();

        // Merge results

        documentList = new ArrayList<>(documentList); // Mutable copy
        documentList.addAll(proofFiles);
        documentList.sort(comparingInt(Document::getIndex));

        documentList.forEach(d -> d.setDetachedSignatures(
                signatureList.stream()
                        .filter(n -> StringUtils.equals(n.getTargetDocumentId(), d.getId()))
                        .sorted(DETACHED_SIGNATURE_COMPARATOR)
                        .toList()
        ));

        documentList.forEach(d -> d.setPdfVisualId(
                visualPdfList.stream()
                        .filter(v -> {
                            //noinspection unchecked
                            HashMap<String, Object> properties = (HashMap<String, Object>) v.getProperties();
                            String sourceDocumentId = MapUtils.getString(properties, PROPERTY_SOURCE_DOCUMENT_ID, null);
                            return StringUtils.equals(sourceDocumentId, d.getId());
                        })
                        .map(NodeChildAssociation::getId)
                        .findFirst()
                        .orElse(null))
        );

        return documentList;
    }


    @Override
    public @NotNull Document getDocumentInfo(@NotNull String documentId) {
        Node node = getNode(documentId);
        return new Document(node);
    }


    /**
     * @param documentId
     * @param documentBuffer
     * @return
     * @deprecated Replaced by {@link #updateDocument(String, DocumentBuffer, boolean)}}
     */
    @Deprecated
    @Override
    public void updateDocument(@NotNull String documentId, @NotNull DocumentBuffer documentBuffer) {
        updateContent(documentId, documentBuffer, false);
    }


    /**
     * TODO : Remove the updateFileName parameter, it should always be true.
     *  The document buffer should contain a proper document name at any time.
     *  If not, we shall fix the document chain, fix it.
     *
     * @param documentId
     * @param documentBuffer
     * @param updateFileName
     * @return
     */
    @Override
    public void updateDocument(@NotNull String documentId, @NotNull DocumentBuffer documentBuffer, boolean updateFileName) {
        log.debug("updateDocument id:{} length:{}", documentId, documentBuffer.getContentLength());
        String nodeId = updateContent(documentId, documentBuffer, updateFileName);
        log.debug("updateDocument done nodeId:{}", nodeId);
    }


    // </editor-fold desc="Document CRUDL">


    @Override
    public @NotNull DocumentBuffer retrieveContent(@NotNull String contentId) {
        log.debug("retrieveContent id:{}", contentId);
        DocumentBuffer result = getContent(contentId);
        log.debug("retrieveContent id:{} length:{}", contentId, result.getContentLength());
        return result;
    }


    /**
     * This method cannot be library-ised yet.
     * The standard OpenApiGenerator downloads everything in a temp file, and returns the {@link File} object.
     * This is specific to the WebClient generation, it works well on an older RestTemplate lib.
     *
     * @param documentId
     * @return
     * @see <a href="https://stackoverflow.com/questions/72142403/useabstractionforfiles-openapi-webclient-not-working">The StackOverflow associate issue</a>
     */
    @Override
    public @NotNull InputStream retrievePipedDocument(@NotNull String documentId) {

        URI requestUri = UriComponentsBuilder.newInstance()
                .scheme(HTTP).host(properties.getHost()).port(properties.getPort())
                .path(NODES_URL).pathSegment(documentId).pathSegment(NODE_CONTENT)
                .queryParam("attachment", false)
                .build().normalize().toUri();

        Flux<DataBuffer> bodyDataFlux = sharedWebClientBuilder.build()
                .get().uri(requestUri)
                .headers(headers -> headers.setBasicAuth(getAlfrescoHeader()))
                .accept(APPLICATION_OCTET_STREAM)
                .exchangeToFlux(RequestUtils::clientResponseToRawDataBufferFlux);

        try {
            return RequestUtils.pipeFluxAsInputStream(bodyDataFlux);
        } catch (IOException e) {
            throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, e, "message.cannot_reach_content_service");
        }
    }


    @Override
    public void deleteFolder(@NotNull Folder folder) {
        log.debug("deleteFolder id:{} contentId:{}", folder.getId(), folder.getContentId());

        Optional.ofNullable(folder.getContentId())
                .ifPresent(this::deleteContent);
    }


    @Override
    public void deleteDocument(@NotNull String documentId) {
        deleteContent(documentId);
    }


    @Override
    public @NotNull String createPdfVisual(@NotNull Folder folder, @NotNull String targetDocument, @NotNull DocumentBuffer documentBuffer) {

        log.debug("createPdfVisual folderContentId:{} docLength:{}", folder.getContentId(), documentBuffer.getContentLength());

        String fileName = firstNonNull(documentBuffer.getName(), "(No name)");

        MultipartBodyBuilder builder = new MultipartBodyBuilder();
        builder.part("name", fileName);
        builder.part("nodeType", NODE_TYPE_IP_PDF_VISUAL);
        builder.part("autoRename", true);
        builder.part(PROPERTY_SOURCE_DOCUMENT_ID, targetDocument);

        // For some reason, header seems mandatory for the asyncPart, and not for the other ones.
        // Cheers to this article : https://dev.to/shavz/sending-multipart-form-data-using-spring-webtestclient-2gb7
        builder.asyncPart("filedata", documentBuffer.getContentFlux(), DataBuffer.class)
                .header(CONTENT_DISPOSITION, "form-data; name=filedata; filename=" + fileName);

        // Upload content

        return createContent(folder.getContentId(), builder);
    }


    // <editor-fold desc="Templates CRUD">


    @Override
    public @NotNull String createCustomTemplate(@NotNull Tenant tenant, @NotNull TemplateType templateType, @NotNull String templateValue) {

        // This file name is not used, but Alfresco demands a real one, with a proper extension...
        String fileName = templateType.name().toLowerCase(ROOT) + ".ftl";

        MultipartBodyBuilder builder = new MultipartBodyBuilder();
        builder.part("name", fileName);
        builder.part("nodeType", NODE_TYPE_NATIVE_CONTENT);
        builder.part("relativePath", "templates");
        builder.part("autoRename", false);
        builder.part("filedata", templateValue).header(CONTENT_DISPOSITION, "form-data; name=filedata; filename=" + fileName);

        String newNodeId = createContent(tenant.getContentId(), builder);
        log.debug("createCustomTemplate response create:{}", newNodeId);
        return newNodeId;
    }


    @Override
    public void editCustomTemplate(@NotNull Tenant tenant, @NotNull TemplateType templateType, @NotNull String templateValue) {

        String nodeId = Optional.ofNullable(tenant.getCustomTemplates().get(templateType))
                .orElseThrow(() -> new LocalizedStatusException(NOT_FOUND, "message.missing_template"));

        // Note, the DataBufferUtils closes the given stream
        ByteArrayInputStream bais = new ByteArrayInputStream(templateValue.getBytes(UTF_8));
        DocumentBuffer documentBuffer = new DocumentBuffer();
        documentBuffer.setName("%s.ftl".formatted(templateType));
        documentBuffer.setContentFlux(DataBufferUtils.readInputStream(() -> bais, new DefaultDataBufferFactory(), FILE_TRANSFER_BUFFER_SIZE));

        updateContent(nodeId, documentBuffer, true);
    }


    @Override
    public @NotNull DocumentBuffer getCustomTemplate(@NotNull Tenant tenant, @NotNull TemplateType templateType) {

        String nodeId = Optional.ofNullable(tenant.getCustomTemplates().get(templateType))
                .orElseThrow(() -> new LocalizedStatusException(NOT_FOUND, "message.missing_template"));

        return getContent(nodeId);
    }


    @Override
    public void deleteCustomTemplate(@NotNull Tenant tenant, @NotNull TemplateType templateType) {

        String nodeId = Optional.ofNullable(tenant.getCustomTemplates().get(templateType))
                .orElseThrow(() -> new LocalizedStatusException(NOT_FOUND, "message.missing_template"));

        deleteContent(nodeId);
    }


    // </editor-fold desc="Templates CRUD">


    // <editor-fold desc="Layer CRUD">


    @Override
    public @NotNull String createLayerImage(@NotNull Tenant tenant, @NotNull Layer layer, @NotNull DocumentBuffer file) {

        log.debug("createLayerImage certificateId:{} docLength:{}", layer.getId(), file.getContentLength());

        String fileName = StringUtils.firstNonEmpty(file.getName(), "(No name)");

        MultipartBodyBuilder builder = new MultipartBodyBuilder();
        builder.part("name", file.getName());
        builder.part("nodeType", NODE_TYPE_NATIVE_CONTENT);
        builder.part("relativePath", String.format("layer_data/%s/", layer.getId()));
        builder.part("autoRename", true);

        // For some reason, header seems mandatory for the asyncPart, and not for the other ones.
        // Cheers to this article : https://dev.to/shavz/sending-multipart-form-data-using-spring-webtestclient-2gb7
        builder.asyncPart("filedata", file.getContentFlux(), DataBuffer.class)
                .header(CONTENT_DISPOSITION, "form-data; name=filedata; filename=" + fileName);

        // Upload content

        return createContent(tenant.getContentId(), builder);
    }


    @Override
    public void deleteLayerImage(@NotNull String layerImageId) {
        deleteContent(layerImageId);
    }


    // </editor-fold desc="Layer CRUD">


}
