/*
 * iparapheur Core
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.ipcore.services.content;

import coop.libriciel.ipcore.model.content.Document;
import coop.libriciel.ipcore.model.content.DocumentBuffer;
import coop.libriciel.ipcore.model.database.TemplateType;
import coop.libriciel.ipcore.model.database.Tenant;
import coop.libriciel.ipcore.model.pdfstamp.Layer;
import coop.libriciel.ipcore.model.workflow.Folder;
import coop.libriciel.ipcore.model.workflow.Task;
import coop.libriciel.ipcore.utils.LocalizedStatusException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import java.io.InputStream;
import java.util.List;

import static coop.libriciel.ipcore.utils.TextUtils.NONE_SERVICE;
import static org.springframework.http.HttpStatus.SERVICE_UNAVAILABLE;


@Service(ContentServiceInterface.BEAN_NAME)
@ConditionalOnProperty(name = ContentServiceInterface.PREFERENCES_PROVIDER_KEY, havingValue = NONE_SERVICE)
public class NoneContentService implements ContentServiceInterface {


    @Override
    public @NotNull String createTenant(@NotNull Tenant tenant) {
        throw new LocalizedStatusException(SERVICE_UNAVAILABLE, "message.content_service_not_available");
    }


    @Override
    public void deleteTenant(@NotNull Tenant tenant) {
        throw new LocalizedStatusException(SERVICE_UNAVAILABLE, "message.content_service_not_available");
    }


    @Override
    public @NotNull String createUserData(@NotNull String userId, int userDataGroupIndex) {
        throw new LocalizedStatusException(SERVICE_UNAVAILABLE, "message.content_service_not_available");
    }


    @Override
    public void deleteUserData(@NotNull String dataGroupNodeId) {
        throw new LocalizedStatusException(SERVICE_UNAVAILABLE, "message.content_service_not_available");
    }


    @Override
    public @NotNull String createUserSignatureImage(@NotNull String userDataContentId, @NotNull DocumentBuffer file) {
        throw new LocalizedStatusException(SERVICE_UNAVAILABLE, "message.content_service_not_available");
    }


    @Override
    public @NotNull String createSealCertificateSignatureImage(@NotNull Tenant tenant, @NotNull String sealCertificateId, @NotNull DocumentBuffer file) {
        throw new LocalizedStatusException(SERVICE_UNAVAILABLE, "message.content_service_not_available");
    }


    @Override
    public @NotNull DocumentBuffer getSignatureImage(@NotNull String signatureNodeId) {
        throw new LocalizedStatusException(SERVICE_UNAVAILABLE, "message.content_service_not_available");
    }


    @Override
    public void updateSignatureImage(@NotNull String imageNodeId, @NotNull DocumentBuffer file) {
        throw new LocalizedStatusException(SERVICE_UNAVAILABLE, "message.content_service_not_available");
    }


    @Override
    public void deleteSignatureImage(@NotNull String signatureNodeId) {
        throw new LocalizedStatusException(SERVICE_UNAVAILABLE, "message.content_service_not_available");
    }


    @Override
    public @NotNull String createFolder(@NotNull Tenant tenant) {
        throw new LocalizedStatusException(SERVICE_UNAVAILABLE, "message.content_service_not_available");
    }


    @Override
    public void deleteFolder(@NotNull Folder folder) {
        throw new LocalizedStatusException(SERVICE_UNAVAILABLE, "message.content_service_not_available");
    }


    @Override
    public void updateFolderMetadata(@NotNull String folderContentId, @NotNull String name) {
        throw new LocalizedStatusException(SERVICE_UNAVAILABLE, "message.content_service_not_available");
    }


    @Override
    public @NotNull String createPremisDocument(@NotNull Folder folder, @NotNull String premis) {
        throw new LocalizedStatusException(SERVICE_UNAVAILABLE, "message.content_service_not_available");
    }


    @Override
    public String updateDocumentAlfrescoProperties(@NotNull Document document) {
        throw new LocalizedStatusException(SERVICE_UNAVAILABLE, "message.content_service_not_available");
    }


    @Override
    public void removeFirstSignaturePlacementAnnotation(List<Document> mainDocumentList) {
        throw new LocalizedStatusException(SERVICE_UNAVAILABLE, "message.content_service_not_available");
    }


    @Override
    public @NotNull String createDocument(@NotNull String tenantId, @NotNull Folder folder, @NotNull DocumentBuffer file) {
        throw new LocalizedStatusException(SERVICE_UNAVAILABLE, "message.content_service_not_available");
    }


    @Override
    public @NotNull Document getDocumentInfo(@NotNull String documentId) {
        throw new LocalizedStatusException(SERVICE_UNAVAILABLE, "message.content_service_not_available");
    }


    @Override
    public void updateDocument(@NotNull String documentId, @NotNull DocumentBuffer documentBuffer) {
        throw new LocalizedStatusException(SERVICE_UNAVAILABLE, "message.content_service_not_available");
    }


    @Override
    public void updateDocument(@NotNull String documentId, @NotNull DocumentBuffer documentBuffer, boolean updateFileName) {
        throw new LocalizedStatusException(SERVICE_UNAVAILABLE, "message.content_service_not_available");
    }


    @Override
    public void deleteDocument(@NotNull String documentId) {
        throw new LocalizedStatusException(SERVICE_UNAVAILABLE, "message.content_service_not_available");
    }


    @Override
    public @NotNull List<Document> getDocumentList(@NotNull String folderContentId) {
        throw new LocalizedStatusException(SERVICE_UNAVAILABLE, "message.content_service_not_available");
    }


    @Override
    public @NotNull DocumentBuffer retrieveContent(@NotNull String documentId) {
        throw new LocalizedStatusException(SERVICE_UNAVAILABLE, "message.content_service_not_available");
    }


    @Override
    public @NotNull InputStream retrievePipedDocument(@NotNull String documentId) {
        throw new LocalizedStatusException(SERVICE_UNAVAILABLE, "message.content_service_not_available");
    }


    @Override
    public @NotNull String createDetachedSignature(@NotNull String folderContentId,
                                                   @NotNull Document targetDocument,
                                                   @NotNull DocumentBuffer file,
                                                   @Nullable Task task) {
        throw new LocalizedStatusException(SERVICE_UNAVAILABLE, "message.content_service_not_available");
    }


    @Override
    public @NotNull String createSignatureProof(@NotNull String folderContentId,
                                                @NotNull String taskId,
                                                @NotNull DocumentBuffer documentBuffer) {
        throw new LocalizedStatusException(SERVICE_UNAVAILABLE, "message.content_service_not_available");
    }


    @Override
    public @NotNull String createPdfVisual(@NotNull Folder folder, @NotNull String targetDocument, @NotNull DocumentBuffer documentBuffer) {
        throw new LocalizedStatusException(SERVICE_UNAVAILABLE, "message.content_service_not_available");
    }


    @Override
    public @NotNull String createCustomTemplate(@NotNull Tenant tenant, @NotNull TemplateType templateType, @NotNull String templateValue) {
        throw new LocalizedStatusException(SERVICE_UNAVAILABLE, "message.content_service_not_available");
    }


    @Override
    public void editCustomTemplate(@NotNull Tenant tenant, @NotNull TemplateType templateType, @NotNull String templateValue) {
        throw new LocalizedStatusException(SERVICE_UNAVAILABLE, "message.content_service_not_available");
    }


    @Override
    public @NotNull DocumentBuffer getCustomTemplate(@NotNull Tenant tenant, @NotNull TemplateType templateType) {
        throw new LocalizedStatusException(SERVICE_UNAVAILABLE, "message.content_service_not_available");
    }


    @Override
    public void deleteCustomTemplate(@NotNull Tenant tenant, @NotNull TemplateType templateType) {
        throw new LocalizedStatusException(SERVICE_UNAVAILABLE, "message.content_service_not_available");
    }


    @Override
    public @NotNull String createLayerImage(@NotNull Tenant tenant, @NotNull Layer layer, @NotNull DocumentBuffer file) {
        throw new LocalizedStatusException(SERVICE_UNAVAILABLE, "message.content_service_not_available");
    }


    @Override
    public void deleteLayerImage(@NotNull String layerImageId) {
        throw new LocalizedStatusException(SERVICE_UNAVAILABLE, "message.content_service_not_available");
    }


}
