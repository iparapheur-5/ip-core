/*
 * iparapheur Core
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.ipcore.services.content;

import coop.libriciel.ipcore.model.content.Document;
import coop.libriciel.ipcore.model.content.DocumentBuffer;
import coop.libriciel.ipcore.model.database.TemplateType;
import coop.libriciel.ipcore.model.database.Tenant;
import coop.libriciel.ipcore.model.pdfstamp.Layer;
import coop.libriciel.ipcore.model.workflow.Folder;
import coop.libriciel.ipcore.model.workflow.Task;
import coop.libriciel.ipcore.utils.FileUtils;
import coop.libriciel.ipcore.utils.LocalizedStatusException;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.core.io.buffer.DefaultDataBufferFactory;
import org.springframework.http.MediaType;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.util.Base64;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.IntStream;

import static coop.libriciel.ipcore.model.crypto.SignatureFormat.PADES;
import static org.springframework.core.io.buffer.DataBufferUtils.readInputStream;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;
import static org.springframework.http.MediaType.APPLICATION_PDF;


public interface ContentServiceInterface {

    Logger log = LogManager.getLogger(ContentServiceInterface.class);

    String BEAN_NAME = "contentService";
    String PREFERENCES_PROVIDER_KEY = "services.content.provider";

    int FILE_TRANSFER_BUFFER_SIZE = 8192;


    // <editor-fold desc="Utils">


    /**
     * We want to factorize this behavior, since it's the same between the Document and the Desk and/or Folder controllers.
     * Yet, we don't want the Controllers to call themselves, nor the Services to call themselves.
     * <p>
     * A lambda function for the transformation seemed to be the way to go.
     * (at some point, we could even throw an error, if the transformation service is not available...)
     *
     * @param tenantId
     * @param folder
     * @param files
     * @param isMainDocument
     * @param pdfTransformer a lambda that wraps the transformation service method
     */
    default void storeMultipartList(@NotNull String tenantId,
                                    @NotNull Folder folder,
                                    @Nullable List<MultipartFile> files,
                                    boolean isMainDocument,
                                    @Nullable List<String> replaceExistingDocumentIds,
                                    @NotNull Function<DocumentBuffer, DocumentBuffer> pdfTransformer) {

        if (CollectionUtils.isEmpty(files)) {
            return;
        }

        boolean isInEditMode = replaceExistingDocumentIds != null;
        boolean areEditListsSameSize = CollectionUtils.size(files) == CollectionUtils.size(replaceExistingDocumentIds);
        if (isInEditMode && !areEditListsSameSize) {
            throw new RuntimeException("Document update ids and files do not match");
        }

        IntStream.range(0, files.size())
                .mapToObj(i -> {
                    DocumentBuffer document = new DocumentBuffer(files.get(i), isMainDocument, i);
                    try (InputStream body = files.get(i).getInputStream()) {
                        document.setContentFlux(readInputStream(() -> body, new DefaultDataBufferFactory(), FILE_TRANSFER_BUFFER_SIZE));

                        // Special case : if we're in a PAdES case, we transform the document before storing it.
                        // It will become the main one.

                        boolean isPades = folder.getType().getSignatureFormat() == PADES;
                        boolean isPdf = Optional.ofNullable(files.get(i))
                                .map(MultipartFile::getContentType)
                                .map(MediaType::parseMediaType)
                                .map(APPLICATION_PDF::equalsTypeAndSubtype)
                                .orElse(false);

                        log.debug("Transformation ? isMainDoc:{} isPades:{} isPdf:{}", isMainDocument, isPades, isPdf);
                        if (isMainDocument && isPades && !isPdf) {

                            DocumentBuffer transformedDocument = pdfTransformer.apply(document);
                            transformedDocument.setName(FilenameUtils.removeExtension(document.getName()) + ".pdf");
                            transformedDocument.setIndex(document.getIndex());
                            transformedDocument.setMediaType(APPLICATION_PDF);
                            transformedDocument.setMainDocument(true);

                            document = transformedDocument;
                        }

                        // Store the document in the Content service

                        String documentId;
                        if (CollectionUtils.isEmpty(replaceExistingDocumentIds)) {
                            documentId = createDocument(tenantId, folder, document);
                        } else {
                            documentId = replaceExistingDocumentIds.get(i);
                            updateDocument(documentId, document, true);
                        }

                        document.setId(documentId);
                        return document;
                    } catch (IOException e) {
                        throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, e, "message.error_reading_file");
                    }
                })
                .peek(d -> {
                    // We may need a PDF-visualisation document.
                    // We'll transform the original one, and store that into a secondary child node.
                    // TODO : We have to transform every document file into a PDF, only to display something in the UI.
                    //  In 5.1+, we'll have some non-PDF viewer. We shall remove this behaviour, and use the original file directly.
                    boolean isNotXmlFile = !FileUtils.isXml(d.getMediaType());
                    boolean isNotPdf = !APPLICATION_PDF.equalsTypeAndSubtype(d.getMediaType());
                    boolean isNotZip = !d.getMediaType().toString().equals("application/zip");

                    if (isNotXmlFile && isNotPdf && isNotZip) {

                        DocumentBuffer transformedDocumentBuffer = retrieveContent(d.getId());
                        if (transformedDocumentBuffer.getMediaType() == null) {
                            transformedDocumentBuffer.setMediaType(d.getMediaType());
                        }
                        transformedDocumentBuffer = pdfTransformer.apply(transformedDocumentBuffer);
                        String transformedContentId = createPdfVisual(folder, d.getId(), transformedDocumentBuffer);
                        transformedDocumentBuffer.setId(transformedContentId);
                        folder.getDocumentList().add(transformedDocumentBuffer);
                    }
                })
                .forEach(d -> folder.getDocumentList().add(d));
    }


    // <editor-fold desc="Utils">


    @NotNull String createTenant(@NotNull Tenant tenant);


    void deleteTenant(@NotNull Tenant tenant);


    @NotNull String createUserData(@NotNull String userId, int userDataGroupIndex);


    void deleteUserData(@NotNull String dataGroupNodeId);


    // <editor-fold desc="Signature image CRUD">


    @NotNull String createUserSignatureImage(@NotNull String userDataContentId, @NotNull DocumentBuffer file);


    @NotNull String createSealCertificateSignatureImage(@NotNull Tenant tenant, @NotNull String sealCertificateId, @NotNull DocumentBuffer file);


    @NotNull DocumentBuffer getSignatureImage(@NotNull String signatureNodeId);


    void updateSignatureImage(@NotNull String imageNodeId, @NotNull DocumentBuffer file);


    void deleteSignatureImage(@NotNull String signatureNodeId);


    // </editor-fold desc="Signature image CRUD">


    @NotNull String createFolder(@NotNull Tenant tenant);


    void deleteFolder(@NotNull Folder folder);


    void updateFolderMetadata(@NotNull String folderContentId, @NotNull String name);


    @NotNull String createPremisDocument(@NotNull Folder folder, @NotNull String premis);


    String updateDocumentAlfrescoProperties(@NotNull Document document);


    void removeFirstSignaturePlacementAnnotation(List<Document> mainDocumentList);


    // <editor-fold desc="Document CRUDL">


    @NotNull String createDocument(@NotNull String tenantId, @NotNull Folder folder, @NotNull DocumentBuffer file);


    @NotNull Document getDocumentInfo(@NotNull String documentId);


    @Deprecated
    void updateDocument(@NotNull String documentId, @NotNull DocumentBuffer documentBuffer);


    void updateDocument(@NotNull String documentId, @NotNull DocumentBuffer documentBuffer, boolean updateFileName);


    void deleteDocument(@NotNull String documentId);


    @NotNull List<Document> getDocumentList(@NotNull String folderContentId);


    // </editor-fold desc="Document CRUDL">


    default @Nullable String retrieveContentAsBase64(@NotNull String contentId) {
        try (InputStream inputStream = retrievePipedDocument(contentId)) {
            return Base64.getEncoder().encodeToString(inputStream.readAllBytes());
        } catch (IOException e) {
            return null;
        }
    }


    @NotNull DocumentBuffer retrieveContent(@NotNull String documentId);


    /**
     * The regular {@link #retrieveContent(String)} returns an {@link OutputStream}.
     * In some cases, we'll want an regular {@link InputStream} for the document.
     * <p>
     * {@link PipedInputStream}/{@link PipedOutputStream} allows this,
     * but has to be set closer to the request. Here.
     *
     * @param documentId the node retrieved
     * @return an inputStream
     * @throws IOException if anything went wrong with the network
     */
    @NotNull InputStream retrievePipedDocument(@NotNull String documentId);


    @NotNull String createDetachedSignature(@NotNull String folderContentId,
                                            @NotNull Document targetDocument,
                                            @NotNull DocumentBuffer file,
                                            @Nullable Task task);


    @NotNull String createSignatureProof(@NotNull String folderContentId,
                                         @NotNull String taskId,
                                         @NotNull DocumentBuffer documentBuffer);


    @NotNull String createPdfVisual(@NotNull Folder folder, @NotNull String targetDocument, @NotNull DocumentBuffer documentBuffer);


    // <editor-fold desc="Templates CRUD">


    @NotNull String createCustomTemplate(@NotNull Tenant tenant, @NotNull TemplateType templateType, @NotNull String templateValue);


    void editCustomTemplate(@NotNull Tenant tenant, @NotNull TemplateType templateType, @NotNull String templateValue);


    @NotNull DocumentBuffer getCustomTemplate(@NotNull Tenant tenant, @NotNull TemplateType templateType);


    void deleteCustomTemplate(@NotNull Tenant tenant, @NotNull TemplateType templateType);


    // </editor-fold desc="Templates CRUD">


    // <editor-fold desc="Layer CRUD">


    @NotNull String createLayerImage(@NotNull Tenant tenant, @NotNull Layer layer, @NotNull DocumentBuffer file);


    void deleteLayerImage(@NotNull String layerImageId);


    // </editor-fold desc="Layer CRUD">


}
