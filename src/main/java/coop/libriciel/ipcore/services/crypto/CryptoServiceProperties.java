/*
 * iparapheur Core
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.ipcore.services.crypto;

import coop.libriciel.ipcore.model.crypto.SignatureFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.ConstructorBinding;


@Data
@AllArgsConstructor
@ConstructorBinding
@ConfigurationProperties(prefix = "services.crypto")
public class CryptoServiceProperties {


    @Data
    @AllArgsConstructor
    public static class AutoFormatDefaultValue {

        private SignatureFormat doc;

    }


    @Data
    @AllArgsConstructor
    public static class SignatureLocation {

        private String country;
        private String city;
        private String zipCode;

    }


    private String provider;
    private String host;
    private int port;
    private AutoFormatDefaultValue autoFormatDefaultValue;
    private SignatureLocation defaultLocation;


}
