/*
 * iparapheur Core
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.ipcore.services.permission;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import coop.libriciel.ipcore.model.auth.Desk;
import coop.libriciel.ipcore.model.auth.DeskRepresentation;
import coop.libriciel.ipcore.model.permission.DelegationDto;
import coop.libriciel.ipcore.model.permission.TargetResourceRepresentation;
import coop.libriciel.ipcore.model.permission.request.*;
import coop.libriciel.ipcore.model.workflow.DelegationRule;
import coop.libriciel.ipcore.services.auth.AuthServiceProperties;
import coop.libriciel.ipcore.utils.*;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.text.StringSubstitutor;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.jooq.Record3;
import org.jooq.Select;
import org.jooq.impl.DSL;
import org.keycloak.KeycloakPrincipal;
import org.keycloak.KeycloakSecurityContext;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.KeycloakBuilder;
import org.keycloak.admin.client.resource.*;
import org.keycloak.authorization.client.AuthorizationDeniedException;
import org.keycloak.authorization.client.AuthzClient;
import org.keycloak.representations.AccessToken;
import org.keycloak.representations.AccessTokenResponse;
import org.keycloak.representations.idm.authorization.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.util.UriComponentsBuilder;

import javax.annotation.PostConstruct;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import static coop.libriciel.ipcore.model.permission.request.KeycloakResourcePolicyEvaluation.PERMIT;
import static coop.libriciel.ipcore.model.workflow.State.PENDING;
import static coop.libriciel.ipcore.services.auth.KeycloakService.*;
import static coop.libriciel.ipcore.services.workflow.WorkflowServiceInterface.META_SUBTYPE_ID;
import static coop.libriciel.ipcore.services.workflow.WorkflowServiceInterface.META_TYPE_ID;
import static coop.libriciel.ipcore.utils.RequestUtils.*;
import static coop.libriciel.ipcore.utils.TextUtils.*;
import static java.lang.Integer.MAX_VALUE;
import static java.net.HttpURLConnection.HTTP_CREATED;
import static java.util.Collections.*;
import static java.util.Set.of;
import static java.util.stream.Collectors.*;
import static org.apache.commons.lang3.ObjectUtils.firstNonNull;
import static org.apache.commons.lang3.StringUtils.EMPTY;
import static org.jooq.SQLDialect.POSTGRES;
import static org.jooq.conf.ParamType.INLINED;
import static org.jooq.impl.DSL.*;
import static org.keycloak.representations.idm.authorization.DecisionStrategy.AFFIRMATIVE;
import static org.keycloak.representations.idm.authorization.DecisionStrategy.UNANIMOUS;
import static org.springframework.http.HttpStatus.*;
import static org.springframework.http.MediaType.APPLICATION_FORM_URLENCODED;
import static org.springframework.http.MediaType.APPLICATION_JSON;


@Log4j2
@Service(PermissionServiceInterface.BEAN_NAME)
@ConditionalOnProperty(name = PermissionServiceInterface.PREFERENCES_PROVIDER_KEY, havingValue = "keycloak")
public class KeycloakResourceService implements PermissionServiceInterface {


    /**
     * Policies are the inner Keycloak tests to be performed :
     * - Does the user have the appropriate role?
     * - Is this the right user?
     * - Is this the right time?
     * <p>
     * Those can be considered as simple internal tests, and can be be reused.
     */
    private static final String POLICY_DESK_OWNER_INTERNAL_NAME = "tenant_${%s}_desk_${%s}_owner"
            .formatted(TENANT_PLACEHOLDER, DESK_PLACEHOLDER);
    private static final String POLICY_USER_INTERNAL_NAME = "user_${%s}"
            .formatted(USER_PLACEHOLDER);
    private static final String POLICY_TIME_INTERNAL_NAME = "time_${%s}_to_${%s}"
            .formatted(START_DATE_PLACEHOLDER, END_DATE_PLACEHOLDER);

    /**
     * Policies that are group of policies. Those are more specific, and hardly reusable.
     * Calendar-driven delegations, for instance, are a collection of simple time-policies.
     */
    private static final String POLICY_DESK_TARGET_CALENDAR_INTERNAL_NAME = "tenant_${%s}_desk_${%s}_owner_${%s}_target_calendar"
            .formatted(TENANT_PLACEHOLDER, DESK_PLACEHOLDER, TARGET_DESK_PLACEHOLDER);
    private static final String POLICY_DESK_TARGET_TYPE_CALENDAR_INTERNAL_NAME = "tenant_${%s}_desk_${%s}_owner_${%s}_type_${%s}_target_calendar"
            .formatted(TENANT_PLACEHOLDER, DESK_PLACEHOLDER, TARGET_DESK_PLACEHOLDER, TARGET_TYPE_PLACEHOLDER);
    private static final String POLICY_DESK_TARGET_SUBTYPE_CALENDAR_INTERNAL_NAME = "tenant_${%s}_desk_${%s}_owner_${%s}_subtype_${%s}_target_calendar"
            .formatted(TENANT_PLACEHOLDER, DESK_PLACEHOLDER, TARGET_DESK_PLACEHOLDER, TARGET_SUBTYPE_PLACEHOLDER);

    private static final String POLICY_TARGETING_OWNER_PARTIAL_NAME_FORMAT = "_owner_%s_";
    private static final String POLICY_DESK_OWNER_PARTIAL_NAME_FORMAT = "_desk_%s_owner_";
    /**
     * Permissions define the access to a specific thing.
     * It may contains :
     * - A target Resource, or a more general resource type (a Desk, a Subtype, etc).
     * - A Scope (action-like filter, access, read, delegating, administrate...)
     * - A collection of policies.
     * <p>
     * On runtime, Keycloak will evaluate every associated policies.
     * If resolved successfully, the permission will grant an access to the target/action.
     * <p>
     * Weirdly enough, permissions and policies are the same thing.
     * But those 2 concepts are properly separated in the Keycloak UI.
     */
    private static final String PERMISSION_DESK_DELEGATION_INTERNAL_NAME = "tenant_${%s}_desk_${%s}_delegation"
            .formatted(TENANT_PLACEHOLDER, DESK_PLACEHOLDER);
    private static final String PERMISSION_DESK_TARGET_DELEGATION_INTERNAL_NAME = "tenant_${%s}_desk_${%s}_delegation_${%s}_owner"
            .formatted(TENANT_PLACEHOLDER, DESK_PLACEHOLDER, TARGET_DESK_PLACEHOLDER);
    private static final String PERMISSION_DESK_TARGET_DELEGATION_TYPE_INTERNAL_NAME = "tenant_${%s}_desk_${%s}_delegation_${%s}_owner_type_${%s}"
            .formatted(TENANT_PLACEHOLDER, DESK_PLACEHOLDER, TARGET_DESK_PLACEHOLDER, TARGET_TYPE_PLACEHOLDER);
    private static final String PERMISSION_DESK_TARGET_DELEGATION_SUBTYPE_INTERNAL_NAME = "tenant_${%s}_desk_${%s}_delegation_${%s}_owner_subtype_${%s}"
            .formatted(TENANT_PLACEHOLDER, DESK_PLACEHOLDER, TARGET_DESK_PLACEHOLDER, TARGET_SUBTYPE_PLACEHOLDER);
    private static final String PERMISSION_DESK_ACCESS_INTERNAL_NAME = "tenant_${%s}_desk_${%s}_access"
            .formatted(TENANT_PLACEHOLDER, DESK_PLACEHOLDER);
    private static final String PERMISSION_DESK_ASSOCIATION_INTERNAL_NAME = "tenant_${%s}_desk_${%s}_association"
            .formatted(TENANT_PLACEHOLDER, DESK_PLACEHOLDER);
    private static final String PERMISSION_DESK_FUNCTIONAL_ADMIN_INTERNAL_NAME = "tenant_${%s}_desk_${%s}_functional_admin"
            .formatted(TENANT_PLACEHOLDER, DESK_PLACEHOLDER);
    private static final String PERMISSION_DESK_SUPERVISOR_INTERNAL_NAME = "tenant_${%s}_desk_${%s}_supervisor"
            .formatted(TENANT_PLACEHOLDER, DESK_PLACEHOLDER);
    private static final String PERMISSION_DESK_DELEGATION_MANAGER_INTERNAL_NAME = "tenant_${%s}_desk_${%s}_delegation_manager"
            .formatted(TENANT_PLACEHOLDER, DESK_PLACEHOLDER);
    private static final String PERMISSION_FILTERABLE_METADATA_INTERNAL_NAME = "tenant_${%s}_metadata_${%s}_filterable"
            .formatted(TENANT_PLACEHOLDER, METADATA_PLACEHOLDER);
    private static final String PERMISSION_USABLE_SUBTYPE_INTERNAL_NAME = "tenant_${%s}_subtype_${%s}_usable"
            .formatted(TENANT_PLACEHOLDER, SUBTYPE_PLACEHOLDER);
    private static final String PERMISSION_FILTERABLE_SUBTYPE_INTERNAL_NAME = "tenant_${%s}_subtype_${%s}_filterable"
            .formatted(TENANT_PLACEHOLDER, SUBTYPE_PLACEHOLDER);

    private static final String CLIENT_ID_ADMIN_CLI = "admin-cli";
    private static final String SQL_FIELD_TARGET_RESOURCE_ID = "targetResourceId";
    private static final String SQL_FIELD_TARGET_RESOURCE_NAME = "targetResourceName";
    private static final String SQL_FIELD_SOURCE_DESK_NAME = "sourceDeskName";
    public static final String TARGET_CALENDAR_POLICY_DISCRIMINATING_NAME_PART = "target_calendar";


    private static HikariDataSource dataSource;

    private @Value("${keycloak.auth-server-url}") String authServerUrl;
    private @Value("${keycloak.resource}") String client;
    private @Value("${keycloak.credentials.secret}") String clientSecret;

    Keycloak keycloak;

    ScheduledExecutorService executorService;

    private @Setter ClientResource clientResource;
    private ResourceScopesResource scopesClient;
    private PoliciesResource policiesClient;
    private AggregatePoliciesResource aggregatePoliciesClient;
    private RolePoliciesResource rolePoliciesClient;
    private TimePoliciesResource timePoliciesClient;
    private UserPoliciesResource userPoliciesClient;
    private ScopePermissionsResource scopePermissionsClient;
    private ResourcesResource resourcesClient;


    // <editor-fold desc="LifeCycle">


    final AuthzClient authzClient;
    private final AuthServiceProperties authServiceProperties;


    @Autowired
    public KeycloakResourceService(AuthzClient authzClient, AuthServiceProperties authServiceProperties) {
        this.authzClient = authzClient;
        this.authServiceProperties = authServiceProperties;
    }


    @PostConstruct
    void setup() {

        AuthServiceProperties.DataBase db = authServiceProperties.getDb();
        String url = String.format("jdbc:postgresql://%s:%d/%s", db.getHost(), db.getPort(), db.getName());

        HikariConfig config = new HikariConfig();
        config.setJdbcUrl(url);
        config.setUsername(db.getName());
        config.setPassword(db.getPassword());
        config.setMaximumPoolSize(64);
        config.setMaxLifetime(5 * 60 * 1000);
        dataSource = new HikariDataSource(config);

        executorService = Executors.newSingleThreadScheduledExecutor();

        generateHeaders();
    }


    @Scheduled(fixedDelayString = "${services.auth.ticket-delay}")
    protected void generateHeaders() {
        if (keycloak != null && !keycloak.isClosed()) {
            Keycloak kcClientToClose = keycloak;
            executorService.schedule(kcClientToClose::close, WEBCLIENT_CLOSE_DELAY_SEC, TimeUnit.SECONDS);
        }

        keycloak = KeycloakBuilder.builder()
                .serverUrl(String.format("http://%s:%d/auth", authServiceProperties.getHost(), authServiceProperties.getPort()))
                .realm(authServiceProperties.getRealm())
                .username(authServiceProperties.getUsername())
                .password(authServiceProperties.getPassword())
                .clientId(CLIENT_ID_ADMIN_CLI)
                .resteasyClient(((ResteasyClientBuilder) ResteasyClientBuilder.newBuilder()).connectionPoolSize(64).build())
                .build();

        generateClients();
    }


    /**
     * We usually have the Client ID only, and not the Client's Resource ID.
     * This is why we have to go for it here.
     * Yep, the loop is kinda ugly, but that's how they do it in their own tests.
     *
     * @return a properly fetched interface
     * @see <a href="https://github.com/keycloak/keycloak/blob/master/testsuite/integration-arquillian/tests/base/src/test/java/org/keycloak/testsuite/authz/RolePolicyTest.java>Keycloak's tests</a></a>
     */
    private void generateClients() {

        ClientsResource clients = keycloak.realm(authServiceProperties.getRealm()).clients();
        clientResource = clients.findByClientId(client)
                .stream()
                .map(representation -> clients.get(representation.getId()))
                .findFirst()
                .orElseThrow(() -> new RuntimeException("Expected client [resource-server-test]"));

        policiesClient = clientResource.authorization().policies();
        scopesClient = clientResource.authorization().scopes();
        aggregatePoliciesClient = clientResource.authorization().policies().aggregate();
        rolePoliciesClient = clientResource.authorization().policies().role();
        timePoliciesClient = clientResource.authorization().policies().time();
        userPoliciesClient = clientResource.authorization().policies().user();

        scopePermissionsClient = clientResource.authorization().permissions().scope();
        resourcesClient = clientResource.authorization().resources();
    }


    // </editor-fold desc="LifeCycle">


    /**
     * The service account is an internal user, that can be used by the Core.
     * It has every role, and we can request any permissions with it, with any scope.
     * <p>
     * In our case, any user can have multiple roles, and multiple permissions associated with each.
     * Asking every resource-permissions will bring unusable results :
     * We'll get SOME resource-permissions, but we won't know which role granted what.
     * That's where the client-scope is useful : we can ask for a partial accessToken, with a single role.
     * Granted permissions will be role-specific, and that's what we want.
     * <p>
     * We have a generic Client-Scope protocol mapper on the Keycloak params.
     * It ease the Keycloak ClientScope settings, we only need a single one for every desk.
     * It helps a lot on performances too. Around 5k Client-Scopes, Keycloak starts to be slow.
     * <p>
     * Sending parameter to a Script Mapping is not yet implemented, but there is some feature requests about it.
     * We don't have much ways to pass data, the custom header was the only one that did work immediately.
     * Not very elegant... But it works.
     * Yet, we have to do the request manually, since the lib logically does not allow to send any additional header.
     * <p>
     * Maybe we could get rid of this service account. That would be simpler.
     *
     * @param clientScope a desk internal name "tenant_xxx_desk_yyy", that will be mapped to a role
     * @return an accessToken
     */
    public @NotNull String getServiceAccountAccessToken(@Nullable String clientScope) {

        URI requestUri = UriComponentsBuilder
                .fromHttpUrl(authServerUrl)
                .path("/realms/api/protocol/openid-connect/token")
                .build().normalize().toUri();

        MultiValueMap<String, String> bodyValues = new LinkedMultiValueMap<>();
        bodyValues.add("grant_type", "client_credentials");
        bodyValues.add("client_id", client);
        bodyValues.add("client_secret", clientSecret);
        bodyValues.add("scope", "generic_role_mapper"); // We have to namely call the generic scope, otherwise it won't trigger

        log.trace("getServiceAccountAccessToken url:{}", requestUri);

        long start = System.currentTimeMillis();
        String accessToken = WebClient.builder().clientConnector(new ForcedExceptionsConnector()).build()
                .post().uri(requestUri)
                .header("X-Scope-Mapper-Filtering", clientScope)
                .contentType(APPLICATION_FORM_URLENCODED).accept(APPLICATION_JSON)
                .body(BodyInserters.fromFormData(bodyValues))
                .retrieve()
                // Result
                .bodyToMono(new ParameterizedTypeReference<AccessTokenResponse>() {})
                .blockOptional(SUBSERVICE_REQUEST_SHORT_TIMEOUT)
                .map(AccessTokenResponse::getToken)
                .orElseThrow(() -> new ResponseStatusException(INTERNAL_SERVER_ERROR, "cannot get the internal access token"));

        log.debug("oidc accessToken   time:{}", System.currentTimeMillis() - start);
        log.trace("oidc accessToken       :{}", accessToken);
        return accessToken;
    }


    /**
     * Here's the ugly one.
     * <p>
     * Keycloak resource-server is actually pretty good on evaluating permissions on a given resource.
     * But there is a way less glorious performance on listing *any* resources.
     * <p>
     * So here it is, we perform a prior (and naive) SQL request to narrow the target resources to fetch.
     * Once we have this list, with actual resource ids, we can request the real permission engine, and let Keycloak roll its policies.
     *
     * @param roleIds ids of the roles targeted by permission. Uses low-level ids (raw uuid), not role name (eg tenant_...)
     * @return a narrowed list of resource ids matching the request
     */
    @NotNull
    Set<TargetResourceRepresentation> preFetchTargetResources(@Nullable Set<String> roleIds,
                                                              @Nullable String userId,
                                                              @NotNull String resourceType,
                                                              @NotNull String scopeLike) {

        Select<Record3<String, String, String>> sqlRequest = buildPrefetchSqlRequest(roleIds, userId, resourceType, scopeLike);
        log.trace("preFetchTargetResources sqlRequest:\n{}", sqlRequest.getSQL(INLINED));

        try (Connection connection = dataSource.getConnection()) {
            return DSL.using(connection, PSQL_RENDER_SETTINGS)
                    .fetch(sqlRequest)
                    .stream()
                    .map(r -> new TargetResourceRepresentation(
                            r.get(SQL_FIELD_TARGET_RESOURCE_ID, String.class),
                            r.get(SQL_FIELD_TARGET_RESOURCE_NAME, String.class)
                    ))
                    .collect(toSet());
        } catch (SQLException e) {
            throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.cannot_reach_database", e);
        }
    }


    /**
     * The Keycloak DB is pretty straightforward.
     * We can join permissions with policies, scopes, and resources...
     * Then filtering with appropriate request params.
     * <p>
     * Note, permissions and policies are actually stored in the same table.
     *
     * @param roles
     * @param resourceType
     * @param scopeLike
     * @return
     */
    static @NotNull Select<Record3<String, String, String>> buildPrefetchSqlRequest(@Nullable Set<String> roles, @Nullable String userId,
                                                                                    @NotNull String resourceType, @NotNull String scopeLike) {

        Select<Record3<String, String, String>> result = DSL.using(POSTGRES, PSQL_RENDER_SETTINGS)
                .select(
                        field("resource.id", String.class).as(SQL_FIELD_TARGET_RESOURCE_ID),
                        field("resource.name", String.class).as(SQL_FIELD_TARGET_RESOURCE_NAME),
                        field("policy.name", String.class).as(SQL_FIELD_SOURCE_DESK_NAME)
                )

                .from(table("resource_server_policy").as("permission"))

                .innerJoin(table("associated_policy"))
                .on(field("associated_policy.policy_id", String.class).eq(field("permission.id", String.class)))

                .innerJoin(table("resource_server_policy").as("policy"))
                .on(field("policy.id", String.class).eq(field("associated_policy.associated_policy_id", String.class)))

                .innerJoin(table("policy_config").as("policy_config_role"))
                .on(and(
                                field("policy_config_role.policy_id", String.class).eq(field("policy.id", String.class)),
                                or(
                                        Optional.ofNullable(roles)
                                                .filter(CollectionUtils::isNotEmpty)
                                                .map(l -> and(
                                                        field("policy_config_role.name", String.class).eq("roles"),
                                                        field("policy_config_role.value", String.class).in(
                                                                l.stream().map(r -> String.format("[{\"id\":\"%s\",\"required\":true}]", r)).collect(toSet())
                                                        )
                                                ))
                                                .orElse(falseCondition()),

                                        Optional.ofNullable(userId)
                                                .filter(StringUtils::isNotEmpty)
                                                .map(r -> and(
                                                        field("policy_config_role.name", String.class).eq("users"),
                                                        field("policy_config_role.value", String.class).eq(String.format("[\"%s\"]", userId))
                                                ))
                                                .orElse(falseCondition())
                                )
                        )
                )

                .innerJoin(table("scope_policy"))
                .on(field("scope_policy.policy_id", String.class).eq(field("permission.id", String.class)))

                .innerJoin(table("resource_server_scope").as("scope"))
                .on(field("scope.id", String.class).eq(field("scope_policy.scope_id", String.class)))

                .innerJoin(table("resource_policy"))
                .on(field("resource_policy.policy_id", String.class).eq(field("permission.id", String.class)))

                .innerJoin(table("resource_server_resource").as("resource"))
                .on(field("resource.id", String.class).eq(field("resource_policy.resource_id", String.class)))

                .where(and(
                        field("permission.type", String.class).eq("scope"),
                        field("scope.name", String.class).like(scopeLike),
                        field("resource.type", String.class).eq(resourceType)
                ));

        log.trace("buildPrefetchSqlRequest :\n{}", result.getSQL(INLINED));
        return result;
    }


    /**
     * OIDC permissions request,
     * <p>
     * We're using the service-account with a limited Client Scope to fetch the authorizations.
     * The User authorization may be available too, but it may be polluted with different Resources/Scopes/Policies...
     *
     * @param accessToken  the user one or the service-account one
     * @param clientScope  usually a "tenant_xxx_desk_yyy" string
     * @param scopeRequest a requestId/[scopes] request
     */
    private @NotNull List<Permission> getPermissions(@NotNull String accessToken, @Nullable String clientScope,
                                                     @NotNull Map<String, List<String>> scopeRequest) {

        // Access token

        AuthorizationRequest.Metadata authorizationMetadata = new AuthorizationRequest.Metadata();
        authorizationMetadata.setIncludeResourceName(false);
        authorizationMetadata.setResponseMode("permissions"); // "decision" | "permissions" | "result"

        // PAT request

        AuthorizationRequest authorizationRequest = new AuthorizationRequest();
        authorizationRequest.setScope(clientScope);
        authorizationRequest.setMetadata(authorizationMetadata);
        authorizationRequest.setPermissions(new PermissionTicketToken());
        scopeRequest.forEach(authorizationRequest::addPermission);

        long start = System.currentTimeMillis();

        List<Permission> result = emptyList();

        try {
            List<Permission> res = authzClient.authorization(accessToken).getPermissions(authorizationRequest);

            // Due to this bug : https://github.com/keycloak/keycloak/issues/16520,
            // the actual dynamic type of the object `res` does not match.
            // TODO remove this bunch of ugly cast as soon as the issue is fixed in the keycloak-authz-client

            List<Object> castedToObj = (List<Object>) ((Object) res);
            List<Map<String, Object>> castedToMaps = castedToObj.stream().map(o -> (Map<String, Object>) o).toList();
            List<Permission> castedResult = castedToMaps
                    .stream()
                    .map(permObjectMap -> {
                        String resourceId = (String) permObjectMap.get("rsid");
                        List<String> scopes = (List<String>) permObjectMap.get("scopes");
                        return new Permission(resourceId, new HashSet<>(scopes));
                    })
                    .toList();

            result = castedResult;
        } catch (AuthorizationDeniedException e) {
            // This is kinda annoying, Keycloak throws an error if no auth was granted.
            // An empty token would have been easier to manage...
        } catch (RuntimeException e) {
            // If some user has no associated desks, a runtime exception is raised - we should still log it, it could be something else
            log.warn("Caught an unexpected exception when testing permission : {}, caused by : {}", e.getMessage(), e.getCause());
            log.trace("Details : ", e);
        }

        log.debug("oidc permissions : {}", result);
        log.debug("oidc authorization time : {}", System.currentTimeMillis() - start);

        return result;
    }


    /**
     * Removing everything from Keycloak.
     * <p>
     * Comparing to creation, we have a little less to do here. Some elements are linked together.
     * Dependant {@link ScopePermissionResource} and other {@link PolicyResource} will be automatically deleted by Keycloak.
     * <p>
     * Every deletion (but the {@link RoleResource}) is optional.
     * In some cases (stopping a container in a middle of a stress-test), we can have some partially created Desks.
     *
     * @param resourceId typically a deskId, or a type/subtypeId
     */
    @Override
    public void deletePermission(@NotNull String resourceId) {
        log.debug("deletePermission - resource id : {}", resourceId);
        resourcesClient.findByName(resourceId)
                .forEach(resourceRep -> {
                    ResourceResource resourcesProxy = resourcesClient.resource(resourceRep.getId());
                    Matcher m = DESK_INTERNAL_NAME_PATTERN.matcher(resourceRep.getName());
                    if (!m.matches()) {
                        log.info("deletePermission - resourceId does not map to a desk, just delete it");
                        // TODO check if we have some extra work to perform for other resource types
                        resourcesProxy.remove();
                        return;
                    }

                    String tenantId = m.group(MATCH_GROUP_TENANT_ID);
                    String deskId = m.group(MATCH_GROUP_DESK_ID);

                    String targetCalendarPartialName = String.format(POLICY_TARGETING_OWNER_PARTIAL_NAME_FORMAT, resourceId);
                    List<PolicyRepresentation> policiesTargetingResourceOwner = policiesClient.policies(
                            null,
                            targetCalendarPartialName,
                            null,
                            null,
                            null,
                            false,
                            null,
                            null,
                            0,
                            250
                    );

                    String deskOwnerPartialName = String.format(POLICY_DESK_OWNER_PARTIAL_NAME_FORMAT, resourceId);
                    log.debug("deskOwnerPartialName : {}", deskOwnerPartialName);
                    List<PolicyRepresentation> deskOwnerPolicies = policiesClient.policies(
                            null,
                            deskOwnerPartialName,
                            null,
                            null,
                            null,
                            false,
                            null,
                            null,
                            0,
                            250
                    );

                    policiesTargetingResourceOwner.addAll(deskOwnerPolicies);
                    policiesTargetingResourceOwner.stream()
                            // This should not be necessary at time of writing, but it is a precaution,
                            // in case other policies using this name formatting appear in the future
                            .filter(p -> p.getName().contains(TARGET_CALENDAR_POLICY_DISCRIMINATING_NAME_PART))
                            .forEach(policyRep -> {
                                PolicyResource policyProxy = policiesClient.policy(policyRep.getId());
                                List<PolicyRepresentation> associatedPolicies = policyProxy.associatedPolicies();
                                policyProxy.remove();

                                associatedPolicies.forEach(timePolicyRep -> {
                                    TimePolicyResource timePolicyProxy = timePoliciesClient.findById(timePolicyRep.getId());
                                    if (timePolicyProxy.dependentPolicies().size() == 0) {
                                        timePolicyProxy.remove();
                                    }
                                });
                            });


                    Map<String, String> namePlaceholders = Map.of(
                            TENANT_PLACEHOLDER, tenantId,
                            DESK_PLACEHOLDER, deskId
                    );

                    String deskOwnerPolicyName = StringSubstitutor.replace(POLICY_DESK_OWNER_INTERNAL_NAME, namePlaceholders);
                    PolicyRepresentation policyRep = policiesClient.findByName(deskOwnerPolicyName);
                    if (policyRep != null) {
                        PolicyResource policyProxy = policiesClient.policy(policyRep.getId());
                        policyProxy.remove();
                    }

                    resourcesProxy.remove();
                });


    }


    @Nullable
    private UserPolicyResource getUserPolicyWithName(String userPolicyName) {
        UserPolicyRepresentation userPolicyRep = userPoliciesClient.findByName(userPolicyName);

        if (userPolicyRep == null) {
            log.debug("User policy  was not found : {}", userPolicyName);
            return null;
        }

        return userPoliciesClient.findById(userPolicyRep.getId());
    }


    private String getUserPolicyName(@NotNull String userId) {
        Map<String, String> targetUserSubstitution = Map.of(USER_PLACEHOLDER, userId);
        return StringSubstitutor.replace(POLICY_USER_INTERNAL_NAME, targetUserSubstitution);
    }


    private void removePolicySDependentPermissionsMatchingPattern(String userPolicyName, UserPolicyResource userPolicy, Pattern permissionNamePattern) {

        userPolicy.dependentPolicies().stream()
                .map(AbstractPolicyRepresentation::getName)
                .filter(permissionName -> permissionNamePattern.matcher(permissionName).matches())
                .map(scopePermissionsClient::findByName)
                .filter(Objects::nonNull)
                .forEach(permissionRep -> {
                    // this permission matches the pattern, we remove the target user policy from it's associated policies
                    ScopePermissionResource permission = scopePermissionsClient.findById(permissionRep.getId());
                    permissionRep.setPolicies(
                            permission.associatedPolicies()
                                    .stream()
                                    .map(AbstractPolicyRepresentation::getName)
                                    // keep all existing policies that are not the target user policy
                                    .filter(policyName -> !StringUtils.equals(policyName, userPolicyName))
                                    .collect(toSet())
                    );

                    permission.update(permissionRep);
                    if (permissionRep.getPolicies().size() == 0) {
                        permission.remove();
                    }
                });
    }


    // <editor-fold desc="Desk resource CRUDL">


    /**
     * Creating the {@link ResourceResource} object,
     * that will be the target of every {@link ScopePermissionResource}, {@link PoliciesResource}, etc.
     *
     * @param tenantId
     * @param desk     a {@link Desk} with appropriate id and permissions set
     * @see <a href="https://issues.redhat.com/browse/KEYCLOAK-6621>the bug marked as "fixed"</a>, yet it is not.
     */
    @Override
    public void createDeskResource(@NotNull String tenantId, @NotNull Desk desk) {

        Map<String, String> namePlaceholders = Map.of(
                TENANT_PLACEHOLDER, tenantId,
                DESK_PLACEHOLDER, desk.getId()
        );

        Set<ScopeRepresentation> scopes = Stream.concat(
                        generateScopeUrnSet(desk).stream(),
                        DESK_OPTIONAL_SCOPES.stream()
                )
                .map(ScopeRepresentation::new)
                .collect(toSet());

        ResourceRepresentation resourceRepresentation = new ResourceRepresentation();
        resourceRepresentation.setId(desk.getId());
        resourceRepresentation.setName(StringSubstitutor.replace(DESK_INTERNAL_NAME, namePlaceholders));
        resourceRepresentation.setScopes(scopes);
        resourceRepresentation.setOwnerManagedAccess(true);
        resourceRepresentation.setType(RESOURCE_TYPE_DESK);
        // We won't use it for now, but it makes GUI searches easier
        resourceRepresentation.setUris(of(String.format("/tenant/%s/desk/%s", tenantId, desk.getId())));

        try (Response resourceResponse = resourcesClient.create(resourceRepresentation)) {
            if (resourceResponse.getStatus() != HTTP_CREATED) {
                // Building a dummy exception here, to pass the HTTP error message to the LocalizedStatusException.
                Exception exception = new Exception(resourceResponse.getStatusInfo().getReasonPhrase());
                throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, exception, "message.cannot_create_oidc_resource");
            }
        }
    }


    @Override
    public void retrieveAndRefreshDeskPermissions(@NotNull String tenantId, @NotNull Desk desk) {

        // Desk permissions are directly accessible through the resource itself,
        // no need for an Oauth2/PAT request here.
        Set<String> selfScope = authzClient.protection().resource().findById(desk.getId())
                .getScopes()
                .stream()
                .map(ScopeRepresentation::getName)
                .collect(toSet());

        desk.setFolderCreationAllowed(selfScope.contains(SCOPE_FOLDER_CREATION));
        desk.setActionAllowed(selfScope.contains(SCOPE_FOLDER_ACTION));
        desk.setArchivingAllowed(selfScope.contains(SCOPE_FOLDER_ARCHIVE));
        desk.setChainAllowed(selfScope.contains(SCOPE_FOLDER_CHAIN));
    }


    @Override
    public void editDeskPermission(@NotNull String tenantId, @NotNull Desk desk) {

        ResourceRepresentation resourceRepresentation = authzClient.protection().resource().findById(desk.getId());
        Set<ScopeRepresentation> newScopes = new HashSet<>();

        // Adding the new proper owner-specific scopes
        generateScopeUrnSet(desk).stream()
                .map(ScopeRepresentation::new)
                .forEach(newScopes::add);

        // Adding the other existing scopes
        resourceRepresentation.getScopes().stream()
                .filter(s -> !DESK_OWNER_SCOPES.contains(s.getName()))
                .forEach(newScopes::add);

        resourceRepresentation.setScopes(newScopes);
        authzClient.protection().resource().update(resourceRepresentation);
    }


    @Override
    public @NotNull Map<String, Set<DelegationRule>> getActiveDelegations(@NotNull Set<String> deskIds) {

        KeycloakPrincipal<KeycloakSecurityContext> principal = KeycloakSecurityUtils.getLoggedInPrincipal();
        KeycloakSecurityContext session = principal.getKeycloakSecurityContext();
        AccessToken accessToken = session.getToken();

        Set<String> rolesInternalNames = accessToken.getRealmAccess().getRoles()
                .stream()
                .map(DESK_INTERNAL_NAME_PATTERN::matcher)
                .filter(Matcher::matches)
                .filter(matcher -> deskIds.stream().anyMatch(d -> StringUtils.equals(d, matcher.group(MATCH_GROUP_DESK_ID))))
                .map(matcher -> matcher.group(0))
                .collect(toSet());

        return getActiveDelegations(principal.getName(), rolesInternalNames);
    }


    // </editor-fold desc="Desk resource CRUDL">


    // <editor-fold desc="Metadata resource CRUDL">


    /**
     * Creating the {@link ResourceResource} object,
     * that will be the target of every {@link ScopePermissionResource}, {@link PoliciesResource}, etc.
     *
     * @param tenantId
     * @param metadataId
     * @see <a href="https://issues.redhat.com/browse/KEYCLOAK-6621>the bug marked as "fixed"</a>, yet it is not.
     */
    @Override
    public void createMetadataResource(@NotNull String tenantId, @NotNull String metadataId) {

        Map<String, String> namePlaceholders = Map.of(
                TENANT_PLACEHOLDER, tenantId,
                METADATA_PLACEHOLDER, metadataId
        );

        Set<ScopeRepresentation> scopes = METADATA_SCOPES.stream()
                .map(ScopeRepresentation::new)
                .collect(toSet());

        ResourceRepresentation resourceRepresentation = new ResourceRepresentation();
        resourceRepresentation.setId(metadataId);
        resourceRepresentation.setName(StringSubstitutor.replace(METADATA_INTERNAL_NAME, namePlaceholders));
        resourceRepresentation.setScopes(scopes);
        resourceRepresentation.setOwnerManagedAccess(true);
        resourceRepresentation.setType(RESOURCE_TYPE_METADATA);
        // We won't use it for now, but it makes GUI searches easier
        resourceRepresentation.setUris(of(String.format("/tenant/%s/metadata/%s", tenantId, metadataId)));

        try (Response resourceResponse = resourcesClient.create(resourceRepresentation)) {
            if (resourceResponse.getStatus() != HTTP_CREATED) {
                // Building a dummy exception here, to pass the HTTP error message to the LocalizedStatusException.
                Exception exception = new Exception(resourceResponse.getStatusInfo().getReasonPhrase());
                throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, exception, "message.cannot_create_oidc_resource");
            }
        }
    }


    // </editor-fold desc="Metadata resource CRUDL">


    // <editor-fold desc="Evaluation API">


    private List<String> generatePartialScopes(@NotNull String scope,
                                               @Nullable String tenantId,
                                               @Nullable String typeId,
                                               @Nullable String subtypeId) {

        List<String> scopes = new ArrayList<>();
        scopes.add(scope);

        Optional.ofNullable(typeId)
                .filter(StringUtils::isNotEmpty)
                .filter(id -> StringUtils.isNotEmpty(tenantId))
                .map(id -> generateScopeType(tenantId, id))
                .ifPresent(scopes::add);

        Optional.ofNullable(subtypeId)
                .filter(StringUtils::isNotEmpty)
                .filter(id -> StringUtils.isNotEmpty(tenantId))
                .map(id -> generateScopeSubtype(tenantId, id))
                .ifPresent(scopes::add);

        return scopes;
    }


    private boolean permissionIsADelegationForTypeAndSubtype(@NotNull String tenantId,
                                                             @Nullable String typeId,
                                                             @Nullable String subtypeId,
                                                             @NotNull Permission perm) {
        if (perm.getScopes().contains(SCOPE_DESK_DELEGATION)) {
            return true;
        }

        if (subtypeId == null && typeId == null) {
            if (perm.getScopes().stream().anyMatch(scopeName -> scopeName.startsWith(SCOPE_DESK_DELEGATION))) {
                return true;
            }
        } else {
            String subtypeScope = generateScopeSubtype(tenantId, subtypeId);
            if (perm.getScopes().contains(subtypeScope)) {
                return true;
            }

            String typeScope = generateScopeType(tenantId, typeId);
            if (perm.getScopes().contains(typeScope)) {
                return true;
            }
        }

        return false;
    }


    @Override
    public boolean currentUserHasDirectViewingRightOnSomeDeskIn(@NotNull String tenantId,
                                                                @NotNull Set<String> deskIds,
                                                                @Nullable String typeId,
                                                                @Nullable String subtypeId) {

        List<String> scopes = generatePartialScopes(SCOPE_VIEW, tenantId, typeId, subtypeId);
        return currentUserHasRightOnSomeDeskIn(tenantId, deskIds, scopes);
    }


    @Override
    public boolean currentUserHasViewingRightOnSomeDeskIn(@NotNull String tenantId,
                                                          @NotNull Set<String> deskIds,
                                                          @Nullable String typeId,
                                                          @Nullable String subtypeId) {

        List<String> scopes = generatePartialScopes(SCOPE_VIEW, tenantId, typeId, subtypeId);
        scopes.addAll(generatePartialScopes(SCOPE_DESK_DELEGATION, tenantId, typeId, subtypeId));
        return currentUserHasRightOnSomeDeskIn(tenantId, deskIds, scopes);
    }


    @Override
    public boolean currentUserHasArchivingRightOnSomeDeskIn(@NotNull String tenantId,
                                                            @NotNull Set<String> deskIds,
                                                            @Nullable String typeId,
                                                            @Nullable String subtypeId) {

        List<String> scopes = generatePartialScopes(SCOPE_FOLDER_ARCHIVE, tenantId, typeId, subtypeId);
        return currentUserHasRightOnSomeDeskIn(tenantId, deskIds, scopes);
    }


    @Override
    public boolean currentUserHasChainingRightOnSomeDeskIn(@NotNull String tenantId,
                                                           @NotNull Set<String> deskIds,
                                                           @Nullable String typeId,
                                                           @Nullable String subtypeId) {

        List<String> scopes = generatePartialScopes(SCOPE_FOLDER_CHAIN, tenantId, typeId, subtypeId);
        return currentUserHasRightOnSomeDeskIn(tenantId, deskIds, scopes);
    }


    @Override
    public boolean currentUserHasAdminRightOnDesk(@NotNull String tenantId, @NotNull String deskId) {

        boolean isSuperAdmin = KeycloakSecurityUtils.isSuperAdmin();

        boolean isTenantAdmin = KeycloakSecurityUtils.currentUserIsTenantAdmin(tenantId);

        boolean isFunctionalAdmin = currentUserHasRightOnSomeDeskIn(tenantId, singleton(deskId), singletonList(SCOPE_DESK_FUNCTIONAL_ADMIN));

        return isSuperAdmin || isTenantAdmin || isFunctionalAdmin;
    }


    @Override
    public boolean currentUserHasAdminRightsOnTenant(@NotNull String tenantId) {

        boolean isSuperAdmin = KeycloakSecurityUtils.isSuperAdmin();

        boolean isTenantAdmin = KeycloakSecurityUtils.currentUserIsTenantAdmin(tenantId);


        return isSuperAdmin || isTenantAdmin;
    }


    @Override
    public boolean currentUserHasFolderActionRightOnSomeDeskIn(@NotNull String tenantId,
                                                               @NotNull Set<String> deskIds,
                                                               @Nullable String typeId,
                                                               @Nullable String subtypeId) {

        List<String> scopes = generatePartialScopes(SCOPE_FOLDER_ACTION, tenantId, typeId, subtypeId);
        scopes.add(SCOPE_DESK_DELEGATION);

        List<Permission> permissions = getCurrentUserPermissionsOnDesksForScopes(deskIds, scopes);

        // regular owner, and desk has folder action enabled
        if (permissions.stream().anyMatch(perm -> perm.getScopes().contains(SCOPE_FOLDER_ACTION))) {
            return true;
        }

        List<Permission> delegationPerms = permissions.stream()
                .filter(perm -> perm.getScopes()
                        .stream()
                        .anyMatch(scopeName -> scopeName.startsWith(SCOPE_DESK_DELEGATION))
                )
                .toList();

        // user has a delegation, we must check with admin API if the desk has folder action enabled
        // TODO we should find a way for this to be evaluated in a single call
        if (delegationPerms.size() > 0) {
            Optional<Permission> matchingPerm = delegationPerms
                    .stream()
                    .filter(perm -> permissionIsADelegationForTypeAndSubtype(tenantId, typeId, subtypeId, perm))
                    .findAny();

            return matchingPerm.map(perm -> {
                        ResourceRepresentation resourceRepresentation = authzClient.protection().resource().findById(perm.getResourceId());

                        return resourceRepresentation.getScopes()
                                .stream()
                                .map(ScopeRepresentation::getName)
                                .anyMatch(SCOPE_FOLDER_ACTION::equals);
                    }
            ).orElse(false);
        }

        return false;
    }


    @Override
    public boolean currentUserHasDelegationOnSomeDeskIn(@NotNull String tenantId,
                                                        @NotNull Set<String> deskIds,
                                                        @Nullable String typeId,
                                                        @Nullable String subtypeId) {

        List<String> scopes = singletonList(SCOPE_DESK_DELEGATION);
        return currentUserHasRightOnSomeDeskIn(tenantId, deskIds, scopes);
    }


    public boolean currentUserHasRightOnSomeDeskIn(@NotNull String tenantId, @NotNull Set<String> deskIds, List<String> rightScopes) {
        log.debug("currentUserHasRightOnSomeDeskIn - tenantId : {}, desksIds : {}, scopes : {}", tenantId, deskIds, rightScopes);

        List<Permission> permissions = getCurrentUserPermissionsOnDesksForScopes(deskIds, rightScopes);

        return permissions.size() > 0;
    }


    @NotNull
    private List<Permission> getCurrentUserPermissionsOnDesksForScopes(@NotNull Set<String> deskIds, List<String> rightScopes) {
        KeycloakPrincipal<KeycloakSecurityContext> principal = KeycloakSecurityUtils.getLoggedInPrincipal();
        KeycloakSecurityContext session = principal.getKeycloakSecurityContext();
        String accessTokenString = session.getTokenString();

        Map<String, List<String>> requestMap = deskIds.stream().collect(toMap(s -> s, s -> rightScopes));
        List<Permission> permissions = getPermissions(accessTokenString, null, requestMap);
        return permissions;
    }


    @Override
    public @NotNull List<String> getCurrentUserViewableDeskIds() {

        List<String> result = KeycloakSecurityUtils.getCurrentUserDeskIds();
        KeycloakPrincipal<KeycloakSecurityContext> principal = KeycloakSecurityUtils.getLoggedInPrincipal();

        // supervised desks
        getViewableDesksForUser(principal.getName())
                .stream()
                .map(DeskRepresentation::getId)
                .forEach(result::add);

        // desk with active delegation
        Set<String> potentiallyViewableDeskIds = preFetchTargetResources(new HashSet<>(result), null, RESOURCE_TYPE_DESK, SCOPE_VIEW)
                .stream()
                .map(TargetResourceRepresentation::getId)
                .collect(toSet());

        Map<String, List<String>> requestMap = potentiallyViewableDeskIds
                .stream()
                .collect(toMap(s -> s, s -> singletonList(SCOPE_VIEW)));

        getPermissions(principal.getKeycloakSecurityContext().getTokenString(), null, requestMap)
                .stream()
                .map(Permission::getResourceId)
                .forEach(result::add);

        return result;
    }


    private @NotNull List<DeskRepresentation> getViewableDesksForUser(@NotNull String userId) {
        return getScopeLinkedResourceIds(userId, RESOURCE_TYPE_DESK, SCOPE_VIEW).stream()
                .map(DeskRepresentation::new)
                .toList();
    }


    private @NotNull List<DeskRepresentation> getAllDesksHavingDelegationTo(@NotNull Set<String> deskIds) {
        return getScopeLinkedResourceIdsForRoles(deskIds, RESOURCE_TYPE_DESK, SCOPE_DESK_DELEGATION).stream()
                .map(DeskRepresentation::new)
                .toList();
    }


    /**
     * This is a fine-grained permission request.
     * Anywhere else, we would simply want to know what a user can do.
     * In those cases, the default permission party token is fine, but not here.
     * <p>
     * Here, we want to know what every role gives us.
     * If a given desk has a delegation, we want this specific information.
     * <p>
     * The admin API offers this information : it evaluates everything, like any party token.
     * But it gives us a nice summary of "Which policy granted which permission".
     * That's what we have to parse to have every needed information.
     *
     * @param userId    The current userId
     * @param roleNames The roles to tests. This array should be narrowed (when we have pagination) to improve performances.
     * @return A map of desk id -> A list of active delegating desks id
     */
    public @NotNull Map<String, Set<DelegationRule>> getActiveDelegations(@Nullable String userId, @NotNull Set<String> roleNames) {

        log.debug("evaluatePermissions userId:{} roles:{}", userId, roleNames);

        // Prefetch results

        // TODO : the Keycloak evaluator actually ignores the specified target resources if no scope is set.
        //  Tested in KC 16.1.1, directly on the Admin's evaluation form page.
        //  Since we want that, the prefetch is useless.
        //  Yet, KC may fix it in the future, and we may want to have this back.
        //  Or we can pre-fetch the (partial) scopes, that will do too.

//        Set<String> roleIds = roleNames.stream()
//                .map(DESK_INTERNAL_NAME_PATTERN::matcher)
//                .filter(Matcher::matches)
//                .map(matcher -> matcher.group(MATCH_GROUP_DESK_ID))
//                .collect(toSet());
//
//        Set<String> targetResourceNames = preFetchTargetResources(roleIds, userId, RESOURCE_TYPE_DESK, SCOPE_DESK_DELEGATION + "%").stream()
//                .map(TargetResourceRepresentation::getInternalName)
//                .collect(toSet());
//
//        log.debug("getActiveDelegations prefetch request roleIds:{}", roleIds);
//        log.debug("                              result targetResourceNames:{}", targetResourceNames);

        // Prepare request

        KeycloakPermissionRequest request = new KeycloakPermissionRequest();
        request.setRoleNames(roleNames);
        request.setUserId(userId);


        // API request to evaluate permissions
        // Note : This is only accessible through the API for now.
        // Using the keycloak lib would be cleaner, in the future

        URI evaluationApiUri = UriComponentsBuilder
                .fromHttpUrl(authServerUrl)
                .pathSegment("admin/realms/api/clients", clientResource.toRepresentation().getId(), "authz/resource-server/policy/evaluate")
                .build().normalize().toUri();

        String adminToken = keycloak.tokenManager().getAccessTokenString();
        log.trace("evaluatePermissions adminToken : " + adminToken);

        WebClient webClient = RequestUtils.getWebClientWithBufferSize(16, true);

        KeycloakPermissionResult<KeycloakResourcePoliciesEvaluation> result = webClient
                .post().uri(evaluationApiUri)
                .headers(headers -> headers.setBearerAuth(adminToken))
                .contentType(APPLICATION_JSON).accept(APPLICATION_JSON)
                .bodyValue(request)
                .retrieve()
                // Result
                .bodyToMono(new ParameterizedTypeReference<KeycloakPermissionResult<KeycloakResourcePoliciesEvaluation>>() {})
                .blockOptional(SUBSERVICE_REQUEST_LONG_TIMEOUT)
                .orElseThrow(() -> new ResponseStatusException(INTERNAL_SERVER_ERROR, "message.cannot_parse_permission_service_response"));

        // Parsing result
        // Note : The Keycloak GUI's "Admin -> Client -> Authorization -> Evaluate" web page
        // shows a nice visual representation of this exact result.

        Map<String, Set<DelegationRule>> activeDelegations = new HashMap<>();
        result.getResults()
                // Resource evaluations loop
                .stream()
                .filter(e -> StringUtils.equals(e.getStatus(), PERMIT))
                .forEach(e -> {
                    String targetResourceId = e.getResource().getId();
                    log.trace("evaluatePermissions evaluation result resourceId:{}", targetResourceId);
                    e.getPolicies().stream()
                            // Permission evaluations loop
                            .filter(p -> StringUtils.equals(p.getStatus(), PERMIT))
                            // The following ones aren't interesting, skipping...
                            .filter(p -> !StringUtils.equals(p.getPolicy().getName(), "Desk access"))
                            .filter(p -> !DESK_ASSOCIATION_INTERNAL_NAME_PATTERN.matcher(p.getPolicy().getName()).matches())
                            // Policy evaluations loop
                            .forEach(evalResult -> {

                                log.trace("evaluatePermissions evaluation result permission:{}", evalResult.getPolicy().getName());
                                log.trace("                               evalResult scopes:{}", evalResult.getScopes());
                                log.trace("                               e scopes:{}", e.getScopes());

                                String substituteDeskId = evalResult.getAssociatedPolicies().stream()
                                        .filter(p -> StringUtils.equals(p.getStatus(), PERMIT))
                                        .map(KeycloakResourcePolicyEvaluation::getPolicy)
                                        .map(KeycloakResourcePolicy::getName)
                                        .map(DESK_OWNER_INTERNAL_NAME_PATTERN::matcher)
                                        .filter(Matcher::matches)
                                        .map(m -> m.group(MATCH_GROUP_DESK_ID))
                                        .filter(id -> !StringUtils.equals(id, targetResourceId))
                                        .findFirst()
                                        .orElse(null);

                                if (StringUtils.isEmpty(substituteDeskId)) {
                                    log.warn("Unknown permission, skipping : {}", evalResult.getPolicy().getName());
                                    return;
                                }

                                String targetTypeId = evalResult.getAssociatedPolicies().stream()
                                        .filter(p -> StringUtils.equals(p.getStatus(), PERMIT))
                                        .map(KeycloakResourcePolicyEvaluation::getPolicy)
                                        .map(KeycloakResourcePolicy::getName)
                                        .map(PARTIAL_TYPE_DELEGATION_POLICY_INTERNAL_NAME_PATTERN::matcher)
                                        .filter(Matcher::matches)
                                        .map(m -> m.group(MATCH_GROUP_TYPE_ID))
                                        .findFirst()
                                        .orElse(null);

                                String targetSubtypeId = evalResult.getAssociatedPolicies().stream()
                                        .filter(p -> StringUtils.equals(p.getStatus(), PERMIT))
                                        .map(KeycloakResourcePolicyEvaluation::getPolicy)
                                        .map(KeycloakResourcePolicy::getName)
                                        .map(PARTIAL_SUBTYPE_DELEGATION_POLICY_INTERNAL_NAME_PATTERN::matcher)
                                        .filter(Matcher::matches)
                                        .map(m -> m.group(MATCH_GROUP_SUBTYPE_ID))
                                        .findFirst()
                                        .orElse(null);

                                DelegationRule grantedDelegation;
                                if (targetSubtypeId != null) {
                                    grantedDelegation = new DelegationRule(targetResourceId, PENDING, META_SUBTYPE_ID, targetSubtypeId);
                                } else if (targetTypeId != null) {
                                    grantedDelegation = new DelegationRule(targetResourceId, PENDING, META_TYPE_ID, targetTypeId);
                                } else {
                                    grantedDelegation = new DelegationRule(targetResourceId, PENDING, null, null);
                                }

                                activeDelegations.computeIfAbsent(substituteDeskId, k -> new HashSet<>());
                                activeDelegations.get(substituteDeskId).add(grantedDelegation);
                            });
                });

        // Return result

        log.debug("evaluatePermissions delegations:{}", activeDelegations);
        return activeDelegations;
    }


    // </editor-fold desc="Evaluation API">


    // <editor-fold desc="Subtype resource CRUDL">


    /**
     * Creating the {@link ResourceResource} object,
     * that will be the target of every {@link ScopePermissionResource}, {@link PoliciesResource}, etc.
     *
     * @param tenantId
     * @param subtypeId
     * @see <a href="https://issues.redhat.com/browse/KEYCLOAK-6621>the bug marked as "fixed"</a>, yet it is not.
     */
    @Override
    public void createSubtypeResource(@NotNull String tenantId, @NotNull String subtypeId) {

        Map<String, String> namePlaceholders = Map.of(TENANT_PLACEHOLDER, tenantId, SUBTYPE_PLACEHOLDER, subtypeId);

        Set<ScopeRepresentation> scopes = Stream
                .of(SCOPE_PUBLIC_USE, SCOPE_PUBLIC_FILTER)
                .map(ScopeRepresentation::new)
                .collect(toSet());

        ResourceRepresentation resourceRepresentation = new ResourceRepresentation();
        resourceRepresentation.setId(subtypeId);
        resourceRepresentation.setName(StringSubstitutor.replace(SUBTYPE_INTERNAL_NAME, namePlaceholders));
        resourceRepresentation.setScopes(scopes);
        resourceRepresentation.setOwnerManagedAccess(true);
        resourceRepresentation.setType(RESOURCE_TYPE_SUBTYPE);
        // We won't use it for now, but it makes GUI searches easier
        resourceRepresentation.setUris(of(String.format("/tenant/%s/subtype/%s", tenantId, subtypeId)));

        try (Response resourceResponse = resourcesClient.create(resourceRepresentation)) {
            if (resourceResponse.getStatus() != HTTP_CREATED) {
                log.warn("An error occurred during subtype's permission resource creation : {}", resourceResponse.getStatusInfo().getReasonPhrase());

                // FIXME this method does not work to pass the message along, front-side we only get "Internal Server Error"
                // Building a dummy exception here, to pass the HTTP error message to the LocalizedStatusException.
                Exception exception = new Exception(resourceResponse.getStatusInfo().getReasonPhrase());
                throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, exception, "message.cannot_create_oidc_resource");
            }
        }
    }


    // </editor-fold desc="Subtype resource CRUDL">


    /**
     * Wrapping up the Keycloak create call response, to factorize a little bit of code.
     *
     * @param response The created policy result
     * @param clazz    The policy .class
     * @param <T>      Generic parameter, should be a subclass of {@link PolicyRepresentation}
     * @return The created instance
     */
    private static <T extends AbstractPolicyRepresentation> T parsePolicyResult(@NotNull Response response, @NotNull Class<T> clazz) {

        if (response.getStatus() != CREATED.value()) {
            // Building a dummy exception here, to pass the HTTP error message to the LocalizedStatusException.
            Exception exception = new Exception(response.getStatusInfo().getReasonPhrase());
            throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, exception, "message.cannot_create_oidc_policy");
        }

        return response.readEntity(clazz);
    }


    private ScopeRepresentation getOrCreatePartialScope(@NotNull String partialScopeName) {

        ScopeRepresentation result = scopesClient.findByName(partialScopeName);

        if (result == null) {
            ScopeRepresentation scopeRepresentation = new ScopeRepresentation();
            scopeRepresentation.setName(partialScopeName);

            try (Response policyResponse = scopesClient.create(scopeRepresentation)) {

                if (policyResponse.getStatus() != CREATED.value()) {
                    // Building a dummy exception here, to pass the HTTP error message to the LocalizedStatusException.
                    Exception exception = new Exception(policyResponse.getStatusInfo().getReasonPhrase());
                    log.error("getOrCreatePartialScope error ", exception);
                    throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, exception, "message.cannot_create_oidc_policy");
                }

                return policyResponse.readEntity(ScopeRepresentation.class);
            }
        }

        return result;
    }


    private RolePolicyRepresentation getOrCreateDeskOwnerPolicy(@NotNull String tenantId, @NotNull String deskId) {

        Map<String, String> substitutions = Map.of(TENANT_PLACEHOLDER, tenantId, DESK_PLACEHOLDER, deskId);
        String deskPolicyInternalName = StringSubstitutor.replace(POLICY_DESK_OWNER_INTERNAL_NAME, substitutions);
        RolePolicyRepresentation result = rolePoliciesClient.findByName(deskPolicyInternalName);

        if (result == null) {
            RolePolicyRepresentation policyRepresentation = new RolePolicyRepresentation();
            policyRepresentation.setName(deskPolicyInternalName);
            policyRepresentation.setDescription(deskId);
            policyRepresentation.setDecisionStrategy(AFFIRMATIVE);
            policyRepresentation.addScope(SCOPE_VIEW);
            policyRepresentation.setOwner(client);
            policyRepresentation.setRoles(of(new RolePolicyRepresentation.RoleDefinition(deskId, true)));

            try (Response policyResponse = rolePoliciesClient.create(policyRepresentation)) {
                result = parsePolicyResult(policyResponse, RolePolicyRepresentation.class);
            }
        }

        return result;
    }


    private AggregatePolicyRepresentation getOrPrepareDeskTimeGroupPolicy(@NotNull String tenantId,
                                                                          @NotNull String deskId,
                                                                          @NotNull String targetId,
                                                                          @Nullable String typeId,
                                                                          @Nullable String subtypeId) {

        Map<String, String> substitutions = Map.of(
                TENANT_PLACEHOLDER, tenantId,
                DESK_PLACEHOLDER, deskId,
                TARGET_DESK_PLACEHOLDER, targetId,
                TARGET_TYPE_PLACEHOLDER, firstNonNull(typeId, EMPTY),
                TARGET_SUBTYPE_PLACEHOLDER, firstNonNull(subtypeId, EMPTY)
        );

        String deskPolicyInternalNameTemplate;
        if (subtypeId != null) {
            deskPolicyInternalNameTemplate = POLICY_DESK_TARGET_SUBTYPE_CALENDAR_INTERNAL_NAME;
        } else if (typeId != null) {
            deskPolicyInternalNameTemplate = POLICY_DESK_TARGET_TYPE_CALENDAR_INTERNAL_NAME;
        } else {
            deskPolicyInternalNameTemplate = POLICY_DESK_TARGET_CALENDAR_INTERNAL_NAME;
        }

        String deskPolicyInternalName = StringSubstitutor.replace(deskPolicyInternalNameTemplate, substitutions);
        AggregatePolicyRepresentation aggregatePolicyRepresentation = aggregatePoliciesClient.findByName(deskPolicyInternalName);
        log.debug("getOrPrepareDeskTimeGroupPolicy findByName:{} result:{}", deskPolicyInternalName, aggregatePolicyRepresentation);

        if (aggregatePolicyRepresentation == null) {

            aggregatePolicyRepresentation = new AggregatePolicyRepresentation();
            aggregatePolicyRepresentation.setName(deskPolicyInternalName);
            aggregatePolicyRepresentation.setDescription(deskId);
            aggregatePolicyRepresentation.setDecisionStrategy(AFFIRMATIVE);
            aggregatePolicyRepresentation.setOwner(client);

            try (Response policyResponse = aggregatePoliciesClient.create(aggregatePolicyRepresentation)) {
                aggregatePolicyRepresentation = parsePolicyResult(policyResponse, AggregatePolicyRepresentation.class);
            }
        }

        return aggregatePolicyRepresentation;
    }


    private TimePolicyRepresentation getOrCreateTimePolicy(@Nullable Date start, @Nullable Date end, boolean createIfAbsent) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(ISO8601_DATE_FORMAT);

        String startString = Optional.ofNullable(start)
                .map(dateFormat::format)
                .orElse(null);

        String endString = Optional.ofNullable(end)
                .map(dateFormat::format)
                .orElse(null);

        Map<String, String> substitutions = Map.of(START_DATE_PLACEHOLDER, String.valueOf(startString), END_DATE_PLACEHOLDER, String.valueOf(endString));
        String policyInternalName = StringSubstitutor.replace(POLICY_TIME_INTERNAL_NAME, substitutions);
        TimePolicyRepresentation result = timePoliciesClient.findByName(policyInternalName);

        if (result == null && createIfAbsent) {
            TimePolicyRepresentation policyRepresentation = new TimePolicyRepresentation();
            policyRepresentation.setName(policyInternalName);
            policyRepresentation.setDecisionStrategy(AFFIRMATIVE);
            policyRepresentation.setOwner(client);
            policyRepresentation.setNotBefore(startString);
            policyRepresentation.setNotOnOrAfter(endString);

            try (Response policyResponse = timePoliciesClient.create(policyRepresentation)) {
                result = parsePolicyResult(policyResponse, TimePolicyRepresentation.class);
            }
        }

        return result;
    }


    private UserPolicyRepresentation getOrCreateUserPolicy(@NotNull String userId) {

        Map<String, String> substitutions = Map.of(USER_PLACEHOLDER, userId);
        String userPolicyInternalName = StringSubstitutor.replace(POLICY_USER_INTERNAL_NAME, substitutions);

        UserPolicyRepresentation result = userPoliciesClient.findByName(userPolicyInternalName);

        if (result == null) {
            UserPolicyRepresentation policyRepresentation = new UserPolicyRepresentation();
            policyRepresentation.setName(userPolicyInternalName);
            policyRepresentation.setDescription(userId);
            policyRepresentation.setDecisionStrategy(AFFIRMATIVE);
            policyRepresentation.addScope(SCOPE_VIEW);
            policyRepresentation.setOwner(client);
            policyRepresentation.setUsers(of(userId));

            try (Response policyResponse = userPoliciesClient.create(policyRepresentation)) {
                result = parsePolicyResult(policyResponse, UserPolicyRepresentation.class);
            }
        }

        return result;
    }


    private ScopePermissionRepresentation getOrCreateDeskScopePermission(@NotNull String tenantId, @NotNull String permissionName,
                                                                         @NotNull Set<String> scopeSet, @NotNull String targetDeskId) {

        Map<String, String> targetSubstitutions = Map.of(TENANT_PLACEHOLDER, tenantId, DESK_PLACEHOLDER, targetDeskId);
        String targetDeskInternalName = StringSubstitutor.replace(DESK_INTERNAL_NAME, targetSubstitutions);
        return getOrCreateScopePermission(permissionName, scopeSet, targetDeskInternalName);
    }


    private ScopePermissionRepresentation getOrCreateSubtypeScopePermission(@NotNull String tenantId, @NotNull String permissionName,
                                                                            @NotNull Set<String> scopes, @NotNull String subtypeId) {

        Map<String, String> targetSubstitutions = Map.of(TENANT_PLACEHOLDER, tenantId, SUBTYPE_PLACEHOLDER, subtypeId);
        String targetSubtypeInternalName = StringSubstitutor.replace(SUBTYPE_INTERNAL_NAME, targetSubstitutions);
        return getOrCreateScopePermission(permissionName, scopes, targetSubtypeInternalName);
    }


    private ScopePermissionRepresentation getOrCreateScopePermission(@NotNull String permissionName, @NotNull Set<String> scopeSet,
                                                                     @NotNull String resourceInternalName) {

        ScopePermissionRepresentation permissionRepresentation = scopePermissionsClient.findByName(permissionName);

        if (permissionRepresentation == null) {
            log.debug("getOrCreateScopePermission create permissionName:{}", permissionName);

            // Fetching the target resource

            ResourceRepresentation targetResourceRepresentation = resourcesClient.findByName(resourceInternalName).stream()
                    .findFirst()
                    .orElseThrow(() -> new ResponseStatusException(INTERNAL_SERVER_ERROR, "Unknown resource id"));

            // Creating the permission

            permissionRepresentation = new ScopePermissionRepresentation();
            permissionRepresentation.setName(permissionName);
            permissionRepresentation.addResource(targetResourceRepresentation.getId());
            scopeSet.forEach(permissionRepresentation::addScope);
            permissionRepresentation.setDecisionStrategy(AFFIRMATIVE);

            try (Response policyResponse = scopePermissionsClient.create(permissionRepresentation)) {
                Response.StatusType responseStatus = policyResponse.getStatusInfo();
                log.trace("getOrCreateScopePermission responseCode:{} message:{}", responseStatus.getStatusCode(), responseStatus.getReasonPhrase());
                if (policyResponse.getStatus() != CREATED.value()) {
                    // Building a dummy exception here, to pass the HTTP error message to the LocalizedStatusException.
                    Exception exception = new Exception(responseStatus.getReasonPhrase());
                    log.error("getOrCreateScopePermission error:{}", responseStatus.getReasonPhrase());
                    throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, exception, "message.cannot_create_oidc_policy");
                }
            }

            // For some reason, KC doesn't send back the representation here, like in any other similar request.
            // We have to fetch it, again, to get the full object, with the id set.
            permissionRepresentation = scopePermissionsClient.findByName(permissionName);
        } else {
            log.debug("getOrCreateScopePermission get permissionName:{}", permissionName);
        }

        return permissionRepresentation;
    }


    @SuppressWarnings("UnusedReturnValue")
    public ScopePermissionRepresentation createDeskPermission(@NotNull String tenantId, @NotNull String deskId, @NotNull Set<String> deskActiveScopes,
                                                              @NotNull ResourceRepresentation resourceRepresentation,
                                                              @NotNull RolePolicyRepresentation policyRepresentation) {

        Map<String, String> namePlaceholders = Map.of(
                TENANT_PLACEHOLDER, tenantId,
                DESK_PLACEHOLDER, deskId
        );

        ScopePermissionRepresentation permissionRepresentation = new ScopePermissionRepresentation();
        permissionRepresentation.setName(StringSubstitutor.replace(PERMISSION_DESK_ACCESS_INTERNAL_NAME, namePlaceholders));
        permissionRepresentation.setDescription(StringSubstitutor.replace(DESK_INTERNAL_NAME, namePlaceholders));
        permissionRepresentation.addResource(resourceRepresentation.getId());
        permissionRepresentation.addScope(deskActiveScopes.toArray(new String[0]));
        permissionRepresentation.addPolicy(policyRepresentation.getId());
        permissionRepresentation.setDecisionStrategy(AFFIRMATIVE);

        try (Response permissionResponse = scopePermissionsClient.create(permissionRepresentation)) {
            if (permissionResponse.getStatus() != CREATED.value()) {
                // Building a dummy exception here, to pass the HTTP error message to the LocalizedStatusException.
                Exception exception = new Exception(permissionResponse.getStatusInfo().getReasonPhrase());
                throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, exception, "message.cannot_create_oidc_permission");
            }
            return permissionResponse.readEntity(ScopePermissionRepresentation.class);
        }
    }


    // <editor-fold desc="Delegation CRUDL">


    @Override
    public void addDelegation(@NotNull String tenantId, @NotNull String substituteDeskId, @NotNull String delegatingDeskId,
                              @Nullable String typeId, @Nullable String subtypeId, @Nullable Date beginDate, @Nullable Date endDate) {

        Map<String, String> delegatingSubstitutions = Map.of(
                TENANT_PLACEHOLDER, tenantId,
                DESK_PLACEHOLDER, delegatingDeskId,
                TARGET_DESK_PLACEHOLDER, substituteDeskId,
                TARGET_TYPE_PLACEHOLDER, firstNonNull(typeId, EMPTY),
                TARGET_SUBTYPE_PLACEHOLDER, firstNonNull(subtypeId, EMPTY)
        );

        boolean isTimedDelegation = ObjectUtils.anyNotNull(beginDate, endDate);

        String internalNameTemplate;
        if (StringUtils.isNotEmpty(subtypeId)) {
            internalNameTemplate = PERMISSION_DESK_TARGET_DELEGATION_SUBTYPE_INTERNAL_NAME;
        } else if (StringUtils.isNotEmpty(typeId)) {
            internalNameTemplate = PERMISSION_DESK_TARGET_DELEGATION_TYPE_INTERNAL_NAME;
        } else if (isTimedDelegation) {
            internalNameTemplate = PERMISSION_DESK_TARGET_DELEGATION_INTERNAL_NAME;
        } else {
            internalNameTemplate = PERMISSION_DESK_DELEGATION_INTERNAL_NAME;
        }

        String permissionName = StringSubstitutor.replace(internalNameTemplate, delegatingSubstitutions);

        // Evaluate the appropriate Scope name, in that specific order :
        // From the more specific to the more general.

        Set<String> scopesToAllow = new HashSet<>();
        ScopeRepresentation partialDelegationScope = null;
        if (StringUtils.isNotEmpty(subtypeId)) {
            partialDelegationScope = getOrCreatePartialScope(generateScopeSubtype(tenantId, subtypeId));
            scopesToAllow.add(partialDelegationScope.getName());
        } else if (StringUtils.isNotEmpty(typeId)) {
            partialDelegationScope = getOrCreatePartialScope(generateScopeType(tenantId, typeId));
            scopesToAllow.add(partialDelegationScope.getName());
        } else {
            scopesToAllow.add(SCOPE_DESK_DELEGATION);
        }

        // Only default scopes are allowed on desk resources.
        // On partial delegations, we have to create a new scope. So we have to allow it on target.

        if (partialDelegationScope != null) {

            Map<String, String> targetSubstitutions = Map.of(TENANT_PLACEHOLDER, tenantId, DESK_PLACEHOLDER, delegatingDeskId);
            String delegatingDeskInternalName = StringSubstitutor.replace(DESK_INTERNAL_NAME, targetSubstitutions);

            ResourceRepresentation targetDeskRepresentation = resourcesClient.findByName(delegatingDeskInternalName).stream()
                    .findFirst()
                    .orElseThrow(() -> new ResponseStatusException(INTERNAL_SERVER_ERROR, "Unknown resource id"));

            ScopeRepresentation finalPartialDelegationScope = partialDelegationScope;
            if (targetDeskRepresentation.getScopes().stream()
                    .noneMatch(scope -> StringUtils.equals(scope.getId(), finalPartialDelegationScope.getId()))) {
                targetDeskRepresentation.addScope(partialDelegationScope);
                resourcesClient.resource(targetDeskRepresentation.getId())
                        .update(targetDeskRepresentation);
            }
        }

        // Create/link the actual delegation

        ScopePermissionRepresentation permissionRepresentation = getOrCreateDeskScopePermission(tenantId, permissionName, scopesToAllow, delegatingDeskId);
        if (CollectionUtils.size(permissionRepresentation.getPolicies()) >= MAX_DELEGATION_PER_DESK) {
            throw new LocalizedStatusException(INSUFFICIENT_STORAGE, "message.already_n_delegations_maximum_reached", MAX_DELEGATION_PER_DESK);
        }

        RolePolicyRepresentation sourcePolicy = getOrCreateDeskOwnerPolicy(tenantId, substituteDeskId);
        permissionRepresentation.addPolicy(sourcePolicy.getId());

        if (isTimedDelegation) {
            AggregatePolicyRepresentation timeGroupPolicyRepresentation = getOrPrepareDeskTimeGroupPolicy(
                    tenantId,
                    delegatingDeskId,
                    substituteDeskId,
                    typeId,
                    subtypeId
            );
            AggregatePolicyResource timeGroupPolicyResource = aggregatePoliciesClient.findById(timeGroupPolicyRepresentation.getId());

            timeGroupPolicyResource.associatedPolicies()
                    .stream()
                    .map(AbstractPolicyRepresentation::getId)
                    .forEach(timeGroupPolicyRepresentation::addPolicy);

            TimePolicyRepresentation timePolicyRepresentation = getOrCreateTimePolicy(beginDate, endDate, true);
            timeGroupPolicyRepresentation.addPolicy(timePolicyRepresentation.getId());
            timeGroupPolicyResource.update(timeGroupPolicyRepresentation);

            permissionRepresentation.addPolicy(timeGroupPolicyRepresentation.getId());
        }

        permissionRepresentation.setDecisionStrategy(isTimedDelegation ? UNANIMOUS : AFFIRMATIVE);
        scopePermissionsClient.findById(permissionRepresentation.getId())
                .update(permissionRepresentation);
    }


    @Override
    public void deleteDelegation(@NotNull String delegationId,
                                 @NotNull String tenantId,
                                 @Nullable String substituteDeskId,
                                 @NotNull String delegatingDeskId,
                                 @Nullable String typeId,
                                 @Nullable String subtypeId,
                                 @Nullable Date beginDate,
                                 @Nullable Date endDate) {

        log.debug("deleteDelegation id:{}", delegationId);

        ScopePermissionResource permissionResource = scopePermissionsClient.findById(delegationId);
        ScopePermissionRepresentation permissionRepresentation = permissionResource.toRepresentation();
        Set<String> associatedPoliciesNames = permissionResource.associatedPolicies().stream()
                .map(AbstractPolicyRepresentation::getName)
                .collect(toSet());


        if (substituteDeskId == null) {
            // probably a delegation to a deleted desk, we just double-check that only target calendar policies remain
            if (associatedPoliciesNames.stream().map(DELEGATION_CALENDAR_POLICY_INTERNAL_NAME_PATTERN::matcher).allMatch(Matcher::matches)) {
                associatedPoliciesNames.forEach(aggregateTimePolicyInternalName -> {
                    deleteSpecificTimePolicyAndOwnerFromAggregate(aggregateTimePolicyInternalName, beginDate, endDate, "NULL", associatedPoliciesNames);
                });
            }
        } else {

            boolean isTimedDelegation = ObjectUtils.anyNotNull(beginDate, endDate);

            Map<String, String> substituteOwnerPolicySubstitutions = Map.of(TENANT_PLACEHOLDER, tenantId, DESK_PLACEHOLDER, substituteDeskId);
            String substituteOwnerPolicyName = StringSubstitutor.replace(POLICY_DESK_OWNER_INTERNAL_NAME, substituteOwnerPolicySubstitutions);


            if (isTimedDelegation) {


                Map<String, String> substitutions = Map.of(
                        TENANT_PLACEHOLDER, tenantId,
                        DESK_PLACEHOLDER, delegatingDeskId,
                        TARGET_DESK_PLACEHOLDER, substituteDeskId,
                        TARGET_TYPE_PLACEHOLDER, firstNonNull(typeId, EMPTY),
                        TARGET_SUBTYPE_PLACEHOLDER, firstNonNull(subtypeId, EMPTY)
                );

                String aggregateTimePolicyInternalNameTemplate;
                if (subtypeId != null) {
                    aggregateTimePolicyInternalNameTemplate = POLICY_DESK_TARGET_SUBTYPE_CALENDAR_INTERNAL_NAME;
                } else if (typeId != null) {
                    aggregateTimePolicyInternalNameTemplate = POLICY_DESK_TARGET_TYPE_CALENDAR_INTERNAL_NAME;
                } else {
                    aggregateTimePolicyInternalNameTemplate = POLICY_DESK_TARGET_CALENDAR_INTERNAL_NAME;
                }

                String aggregateTimePolicyInternalName = StringSubstitutor.replace(aggregateTimePolicyInternalNameTemplate, substitutions);

                if (!associatedPoliciesNames.contains(aggregateTimePolicyInternalName)) {
                    throw new ResponseStatusException(INTERNAL_SERVER_ERROR, "could not find aggregate time policy on delegation permission, abort");
                }

                deleteSpecificTimePolicyAndOwnerFromAggregate(aggregateTimePolicyInternalName,
                        beginDate,
                        endDate,
                        substituteOwnerPolicyName,
                        associatedPoliciesNames);
            } else {
                associatedPoliciesNames.removeIf(p -> StringUtils.equals(p, substituteOwnerPolicyName));
            }
        }

        permissionRepresentation.setPolicies(associatedPoliciesNames);

        if (CollectionUtils.isNotEmpty(permissionRepresentation.getPolicies())) {
            permissionResource.update(permissionRepresentation);
        } else {
            permissionResource.remove();
        }
    }


    private void deleteSpecificTimePolicyAndOwnerFromAggregate(String aggregateTimePolicyInternalName,
                                                               @Nullable Date beginDate,
                                                               @Nullable Date endDate,
                                                               String substituteOwnerPolicyName,
                                                               Set<String> associatedPoliciesNames) {
        AggregatePolicyRepresentation aggregatePolicyRepresentation = aggregatePoliciesClient.findByName(aggregateTimePolicyInternalName);
        AggregatePolicyResource aggregatePolicy = aggregatePoliciesClient.findById(aggregatePolicyRepresentation.getId());

        TimePolicyRepresentation timePolicyRepresentation = getOrCreateTimePolicy(beginDate, endDate, false);
        if (timePolicyRepresentation == null) {
            throw new ResponseStatusException(INTERNAL_SERVER_ERROR, "time policy not found");
        }


        Set<String> linkedPolicies = aggregatePolicy.associatedPolicies()
                .stream()
                .map(AbstractPolicyRepresentation::getName)
                .collect(toSet());


        if (!linkedPolicies.contains(timePolicyRepresentation.getName())) {
            log.error("Could not found time policy {} on aggregated policy", timePolicyRepresentation.getName());
        }

        linkedPolicies.removeIf(policyName -> StringUtils.equals(policyName, timePolicyRepresentation.getName()));

        if (linkedPolicies.size() > 0) {
            aggregatePolicyRepresentation.setPolicies(linkedPolicies);
            aggregatePolicy.update(aggregatePolicyRepresentation);
        } else {
            aggregatePolicy.remove();
            associatedPoliciesNames.removeIf(name -> StringUtils.equals(name, aggregatePolicyRepresentation.getName()));
            associatedPoliciesNames.removeIf(p -> StringUtils.equals(p, substituteOwnerPolicyName));
        }

        PolicyResource timePolicyResource = policiesClient.policy(timePolicyRepresentation.getId());
        if (timePolicyResource.dependentPolicies().size() == 0) {
            timePolicyResource.remove();
        }

    }


    @Override
    public @NotNull List<DelegationDto> getDelegations(@NotNull String tenantId, @NotNull String delegatingDeskId) {
        log.debug("getDelegations tenantId:{} delegatingDeskId:{}", tenantId, delegatingDeskId);

        SimpleDateFormat iso8601DateFormat = new SimpleDateFormat(ISO8601_DATE_FORMAT);

        Map<String, String> deskSubstitutions = Map.of(TENANT_PLACEHOLDER, tenantId, DESK_PLACEHOLDER, delegatingDeskId);
        String deskInternalName = StringSubstitutor.replace(DESK_INTERNAL_NAME, deskSubstitutions);
        ResourceResource resourceResource = resourcesClient.findByName(deskInternalName).stream()
                .findFirst()
                .map(ResourceRepresentation::getId)
                .map(resourcesClient::resource)
                .orElseThrow(() -> new ResponseStatusException(NOT_FOUND, "message.unknown_desk_id"));

        String delegationNameTemplate = StringSubstitutor.replace(PERMISSION_DESK_DELEGATION_INTERNAL_NAME, deskSubstitutions);
        String delegationManagerNameTemplate = StringSubstitutor.replace(PERMISSION_DESK_DELEGATION_MANAGER_INTERNAL_NAME, deskSubstitutions);
        List<DelegationDto> result = resourceResource.permissions().stream()
                .filter(p -> StringUtils.contains(p.getName(), delegationNameTemplate))
                .filter(p -> !StringUtils.equals(p.getName(), delegationManagerNameTemplate))
                .map(p -> {
                    PolicyResource policy = policiesClient.policy(p.getId());
                    List<PolicyRepresentation> associatedPolicies = policy.associatedPolicies();

                    String substituteDeskId = associatedPolicies.stream()
                            .map(AbstractPolicyRepresentation::getName)
                            .map(DESK_OWNER_INTERNAL_NAME_PATTERN::matcher)
                            .filter(Matcher::matches)
                            .map(m -> m.group(MATCH_GROUP_DESK_ID))
                            .findFirst()
                            .orElse(null);

                    List<Pair<Date, Date>> intervals = associatedPolicies.stream()
                            .filter(pp -> StringUtils.contains(pp.getName(), "_target_calendar"))
                            .map(AbstractPolicyRepresentation::getId)
                            .map(policiesClient::policy)
                            .map(PolicyResource::associatedPolicies)
                            .flatMap(Collection::stream)
                            .map(AbstractPolicyRepresentation::getName)
                            .map(TIME_PERMISSION_INTERNAL_NAME_PATTERN::matcher)
                            .filter(Matcher::matches)
                            .map(m -> Pair.of(
                                    Optional.ofNullable(m.group(MATCH_GROUP_DATE_START))
                                            .filter(s -> !StringUtils.equals(s, "null")) // Preventing a useless common parse here
                                            .map(s -> TextUtils.deserializeDate(s, iso8601DateFormat))
                                            .orElse(null),
                                    Optional.ofNullable(m.group(MATCH_GROUP_DATE_END))
                                            .filter(s -> !StringUtils.equals(s, "null")) // Preventing a useless common parse here
                                            .map(s -> TextUtils.deserializeDate(s, iso8601DateFormat))
                                            .orElse(null)
                            ))
                            .toList();

                    Pair<String, String> typeScope = policy.scopes().stream()
                            .map(ScopeRepresentation::getName)
                            .filter(StringUtils::isNotEmpty)
                            .map(PARTIAL_TYPE_SCOPE_INTERNAL_NAME_PATTERN::matcher)
                            .filter(Matcher::matches)
                            .map(m -> Pair.of(
                                    m.group(MATCH_GROUP_TENANT_ID),
                                    m.group(MATCH_GROUP_TYPE_ID))
                            )
                            .findFirst()
                            .orElse(Pair.of(null, null));

                    Pair<String, String> subtypeScope = policy.scopes().stream()
                            .map(ScopeRepresentation::getName)
                            .filter(StringUtils::isNotEmpty)
                            .map(PARTIAL_SUBTYPE_SCOPE_INTERNAL_NAME_PATTERN::matcher)
                            .filter(Matcher::matches)
                            .map(m -> Pair.of(
                                    m.group(MATCH_GROUP_TENANT_ID),
                                    m.group(MATCH_GROUP_SUBTYPE_ID))
                            )
                            .findFirst()
                            .orElse(Pair.of(null, null));

                    if (CollectionUtils.isEmpty(intervals)) {
                        return singletonList(
                                DelegationDto.builder()
                                        .id(p.getId())
                                        .substituteDeskId(substituteDeskId)
                                        .typeId(typeScope.getRight())
                                        .subtypeId(subtypeScope.getRight())
                                        .build()
                        );
                    } else {
                        return intervals
                                .stream()
                                .map(i -> DelegationDto.builder()
                                        .id(p.getId())
                                        .substituteDeskId(substituteDeskId)
                                        .start(i.getLeft())
                                        .end(i.getRight())
                                        .typeId(typeScope.getRight())
                                        .subtypeId(subtypeScope.getRight())
                                        .build())
                                .toList();
                    }
                })
                .flatMap(Collection::stream)
                .toList();

        log.debug("getDelegations resultSize:{}", result.size());
        return result;
    }


    @Override
    public @NotNull List<DeskRepresentation> getDelegatingDesks(@NotNull String substituteDeskId) {
        // FIXME Adrien:
        /*
        RoleRepresentation substituteRole = keycloak.realm(realm).rolesById().getRole(substituteDeskId);
        log.info("List delegating roles for substitute desk:{}", substituteRole);

        return keycloak.realm(realm)
                .roles()
                .get(substituteRole.getName())
                .getRoleComposites()
                .stream()
                .map(Desk::new)
                .collect(toSet());
         */
        return emptyList();
    }


    // </editor-fold desc="Delegation CRUDL">


    @Override
    public void setMetadataDelegations(@NotNull String deskId, @NotNull Collection<DelegationRule> metadataDelegations) {
        // FIXME : Adrien
        /*
        // TODO : Limit delegations
        // FIXME : Cleanup this prefix
        RoleRepresentation role = keycloak.realm(realm).roles().get(DESK_ROLE_PREFIX + deskId).toRepresentation();

        if (metadataDelegations.isEmpty()) {
            role.getAttributes().remove(ATTRIBUTE_TYPOLOGY_DELEGATIONS);
        } else {
            try {
                String metadataDelegationsSerialized = new ObjectMapper().writeValueAsString(metadataDelegations);
                log.debug("Serializing for deskId:{} metadataDelegation:{}", deskId, metadataDelegations);
                role.singleAttribute(ATTRIBUTE_TYPOLOGY_DELEGATIONS, metadataDelegationsSerialized);
            } catch (JsonProcessingException e) {
                throw new LocalizedStatusException(INTERNAL_ERROR_STATUS, "cannot serialize delegation", e);
            }
        }

        keycloak.realm(realm).rolesById().updateRole(role.getId(), role);
         */
    }


    /**
     * @param roleIds      ids of the roles targeted by permission. Uses low-level ids (raw uuid), not role name (eg tenant_...)
     * @param resourceType eg urn:ipcore:resources:desk
     * @param scope        eg urn:ipcore:scopes:delegation
     * @return The list of raw resource ids matching the criteria
     */
    private @NotNull List<String> getScopeLinkedResourceIdsForRoles(@NotNull Set<String> roleIds, @NotNull String resourceType, @NotNull String scope) {

        Set<String> prefetchResourceIds = preFetchTargetResources(roleIds, null, resourceType, scope).stream()
                .map(TargetResourceRepresentation::getId)
                .collect(toSet());

        return prefetchResourceIds.stream().toList();
    }


    private @NotNull List<String> getScopeLinkedResourceIds(@NotNull String userId, @NotNull String resourceType, @NotNull String scope) {

        Set<String> prefetchResourceIds = preFetchTargetResources(null, userId, resourceType, scope).stream()
                .map(TargetResourceRepresentation::getId)
                .collect(toSet());

        return prefetchResourceIds.stream().toList();
    }


    private @NotNull List<String> getScopeLinkedResourceIdsForTenant(@NotNull String userId,
                                                                     @NotNull String resourceType,
                                                                     @NotNull String scope,
                                                                     @NotNull String tenantId) {

        Set<String> prefetchResourceIds = preFetchTargetResources(null, userId, resourceType, scope).stream()
                .map(TargetResourceRepresentation::getId)
                .collect(toSet());

        Map<String, String> deskSubstitutions = Map.of(TENANT_PLACEHOLDER, tenantId, DESK_PLACEHOLDER, ID_REGEX);
        String anyDeskForTenantRegex = StringSubstitutor.replace(DESK_INTERNAL_NAME, deskSubstitutions);

        Pattern tenantNamePattern = Pattern.compile(anyDeskForTenantRegex);
        List<String> result = prefetchResourceIds.stream()
                .map(resourcesClient::resource)
                .map(ResourceResource::toRepresentation)
                .filter(resourceRep -> tenantNamePattern.matcher(resourceRep.getName()).matches())
                .map(ResourceRepresentation::getId)
                .toList();
        return result;
    }


    private void removeUserPoliciesFromPermission(@NotNull String tenantId,
                                                  @NotNull Collection<String> userIds,
                                                  @NotNull ScopePermissionResource scopePermissionResource,
                                                  @NotNull ScopePermissionRepresentation scopePermissionRepresentation) {

        Set<String> userPoliciesIds = userIds.stream()
                .map(i -> Map.of(TENANT_PLACEHOLDER, tenantId, USER_PLACEHOLDER, i))
                .map(s -> StringSubstitutor.replace(POLICY_USER_INTERNAL_NAME, s))
                .map(n -> policiesClient.findByName(n))
                .map(PolicyRepresentation::getId)
                .collect(toSet());

        Set<String> remainingPolicies = scopePermissionResource.associatedPolicies().stream()
                .map(AbstractPolicyRepresentation::getId)
                .filter(i -> !userPoliciesIds.contains(i))
                .collect(toSet());

        if (remainingPolicies.isEmpty()) {
            scopePermissionResource.remove();
        } else {
            scopePermissionRepresentation.setPolicies(remainingPolicies);
            scopePermissionResource.update(scopePermissionRepresentation);
        }

        userPoliciesIds.stream().map(userPolicyId -> userPoliciesClient.findById(userPolicyId))
                .filter(userPolicyResource -> userPolicyResource.dependentPolicies().size() == 0)
                .forEach(UserPolicyResource::remove);
    }


    private void linkDeskToResource(@NotNull Collection<String> targetResourceIds,
                                    @NotNull AbstractPolicyRepresentation sourcePolicyRepresentation,
                                    @NotNull Function<String, ScopePermissionRepresentation> permissionRepresentationSupplier,
                                    int maxAssociations,
                                    @NotNull String maxErrorMessage) {

        if (CollectionUtils.isEmpty(targetResourceIds)) {
            return;
        }

        targetResourceIds.forEach(targetResourceId -> {

            ScopePermissionRepresentation permissionRepresentation = permissionRepresentationSupplier.apply(targetResourceId);
            ScopePermissionResource permissionResource = scopePermissionsClient.findById(permissionRepresentation.getId());
            Set<String> alreadyAssociatedPoliciesIds = permissionResource.associatedPolicies().stream()
                    .map(PolicyRepresentation::getId)
                    .collect(toSet());

            if (CollectionUtils.size(permissionRepresentation.getPolicies()) >= maxAssociations) {
                throw new LocalizedStatusException(INSUFFICIENT_STORAGE, maxErrorMessage, maxAssociations);
            }

            permissionRepresentation.setPolicies(alreadyAssociatedPoliciesIds);
            permissionRepresentation.addPolicy(sourcePolicyRepresentation.getId());
            permissionResource.update(permissionRepresentation);
        });
    }


    // <editor-fold desc="Supervisor CRUDL">


    // TODO it would be simpler to have "set" method, as it is more natural with the keycloak API *and* our desk api
    @Override
    public void addSupervisorsToDesk(@NotNull String tenantId, @NotNull String deskId, @NotNull Collection<String> userIds) {

        if (CollectionUtils.isEmpty(userIds)) {
            return;
        }

        Map<String, String> targetSubstitutions = Map.of(TENANT_PLACEHOLDER, tenantId, DESK_PLACEHOLDER, deskId);
        String permissionName = StringSubstitutor.replace(PERMISSION_DESK_SUPERVISOR_INTERNAL_NAME, targetSubstitutions);
        log.trace("addSupervisorsToDesk permissionName:{}", permissionName);

        Set<String> scopesToAllow = of(SCOPE_DESK_SUPERVISOR, SCOPE_VIEW);

        ScopePermissionRepresentation permissionRepresentation = getOrCreateDeskScopePermission(tenantId, permissionName, scopesToAllow, deskId);
        if (CollectionUtils.size(permissionRepresentation.getPolicies()) + userIds.size() >= MAX_SUPERVISOR_PER_DESK) {
            throw new LocalizedStatusException(INSUFFICIENT_STORAGE, "message.already_n_supervisors_maximum_reached", MAX_SUPERVISOR_PER_DESK);
        }

        ScopePermissionResource permResource = scopePermissionsClient.findById(permissionRepresentation.getId());

        permResource.associatedPolicies().stream()
                .map(AbstractPolicyRepresentation::getName)
                .forEach(permissionRepresentation::addPolicy);


        userIds.forEach(i -> {
            UserPolicyRepresentation sourcePolicyRepresentation = getOrCreateUserPolicy(i);
            permissionRepresentation.addPolicy(sourcePolicyRepresentation.getId());
        });


        permResource.update(permissionRepresentation);
    }


    @Override
    public void removeSupervisorsFromDesk(@NotNull String tenantId, @NotNull String sourceDeskId, @NotNull Collection<String> userIds) {

        if (CollectionUtils.isEmpty(userIds)) {
            return;
        }

        Map<String, String> targetDeskSubstitutions = Map.of(TENANT_PLACEHOLDER, tenantId, DESK_PLACEHOLDER, sourceDeskId);
        String associationName = StringSubstitutor.replace(PERMISSION_DESK_SUPERVISOR_INTERNAL_NAME, targetDeskSubstitutions);

        ScopePermissionRepresentation permissionRepresentation = scopePermissionsClient.findByName(associationName);
        ScopePermissionResource permissionResource = scopePermissionsClient.findById(permissionRepresentation.getId());

        removeUserPoliciesFromPermission(tenantId, userIds, permissionResource, permissionRepresentation);
    }


    @Override
    public void removeAllSupervisedDesks(@NotNull String tenantId, @NotNull String userId) {

        String userPolicyName = getUserPolicyName(userId);

        UserPolicyResource userPolicy = getUserPolicyWithName(userPolicyName);
        if (userPolicy == null) {
            log.warn("Could not retrieve user policy with name '{}' by id", userPolicyName);
            return;
        }

        Map<String, String> targetRegexSubstitutions = Map.of(TENANT_PLACEHOLDER, tenantId, DESK_PLACEHOLDER, ID_REGEX);
        String supervisorForTenantRegex = StringSubstitutor.replace(PERMISSION_DESK_SUPERVISOR_INTERNAL_NAME, targetRegexSubstitutions);
        Pattern supervisorForTenantPattern = Pattern.compile(supervisorForTenantRegex);

        log.debug("userPolicy.dependentPolicies() : {}", userPolicy.dependentPolicies());

        removePolicySDependentPermissionsMatchingPattern(userPolicyName, userPolicy, supervisorForTenantPattern);
    }


    @Override
    public @NotNull List<String> getSupervisorsFromDesk(@NotNull String tenantId, @NotNull String deskId) {

        // Desk permissions are directly accessible through the permission itself,
        // no need for an Oauth2/PAT request here.
        Map<String, String> targetSubstitutions = Map.of(TENANT_PLACEHOLDER, tenantId, DESK_PLACEHOLDER, deskId);
        String permissionName = StringSubstitutor.replace(PERMISSION_DESK_SUPERVISOR_INTERNAL_NAME, targetSubstitutions);
        ScopePermissionResource permissionResource = Optional.ofNullable(scopePermissionsClient.findByName(permissionName))
                .map(AbstractPolicyRepresentation::getId)
                .map(scopePermissionsClient::findById)
                .orElse(null);

        if (permissionResource == null) {
            return emptyList();
        }

        return permissionResource.associatedPolicies().stream()
                .map(AbstractPolicyRepresentation::getName)
                .filter(StringUtils::isNotEmpty)
                .map(USER_POLICY_INTERNAL_NAME_PATTERN::matcher)
                .filter(Matcher::matches)
                .map(m -> m.group(MATCH_GROUP_USER_ID))
                .toList();
    }


    @Override
    public @NotNull List<DeskRepresentation> getSupervisedDesks(@NotNull String userId, @Nullable String tenantId) {
        List<String> deskIds;
        if (tenantId != null) {
            deskIds = getScopeLinkedResourceIdsForTenant(userId, RESOURCE_TYPE_DESK, SCOPE_DESK_SUPERVISOR, tenantId);
        } else {
            deskIds = getScopeLinkedResourceIds(userId, RESOURCE_TYPE_DESK, SCOPE_DESK_SUPERVISOR);
        }

        return deskIds.stream().map(DeskRepresentation::new).collect(toList());
    }


    // </editor-fold desc="Supervisor CRUDL">


    // <editor-fold desc="Delegation manager CRUDL">


    // TODO it would be simpler to have "set" method, as it is more natural with the keycloak API *and* our desk api
    @Override
    public void addDelegationManagersToDesk(@NotNull String tenantId, @NotNull String deskId, @NotNull Collection<String> userIds) {

        if (CollectionUtils.isEmpty(userIds)) {
            return;
        }

        Map<String, String> targetSubstitutions = Map.of(TENANT_PLACEHOLDER, tenantId, DESK_PLACEHOLDER, deskId);
        String permissionName = StringSubstitutor.replace(PERMISSION_DESK_DELEGATION_MANAGER_INTERNAL_NAME, targetSubstitutions);
        log.trace("addDelegationManagersToDesk permissionName:{}", permissionName);

        Set<String> scopesToAllow = of(SCOPE_DESK_DELEGATION_MANAGER, SCOPE_VIEW);

        ScopePermissionRepresentation permissionRepresentation = getOrCreateDeskScopePermission(tenantId, permissionName, scopesToAllow, deskId);
        if (CollectionUtils.size(permissionRepresentation.getPolicies()) + userIds.size() >= MAX_DELEGATION_MANAGER_PER_DESK) {
            throw new LocalizedStatusException(INSUFFICIENT_STORAGE, "message.already_n_delegations_managers_maximum_reached", MAX_SUPERVISOR_PER_DESK);
        }

        ScopePermissionResource permResource = scopePermissionsClient.findById(permissionRepresentation.getId());

        permResource.associatedPolicies().stream()
                .map(AbstractPolicyRepresentation::getName)
                .forEach(permissionRepresentation::addPolicy);


        userIds.forEach(i -> {
            UserPolicyRepresentation sourcePolicyRepresentation = getOrCreateUserPolicy(i);
            permissionRepresentation.addPolicy(sourcePolicyRepresentation.getId());
        });

        permResource.update(permissionRepresentation);
    }


    @Override
    public void removeDelegationManagersFromDesk(@NotNull String tenantId, @NotNull String sourceDeskId, @NotNull Collection<String> userIds) {

        if (CollectionUtils.isEmpty(userIds)) {
            return;
        }

        Map<String, String> targetDeskSubstitutions = Map.of(TENANT_PLACEHOLDER, tenantId, DESK_PLACEHOLDER, sourceDeskId);
        String associationName = StringSubstitutor.replace(PERMISSION_DESK_DELEGATION_MANAGER_INTERNAL_NAME, targetDeskSubstitutions);

        ScopePermissionRepresentation permissionRepresentation = scopePermissionsClient.findByName(associationName);
        ScopePermissionResource permissionResource = scopePermissionsClient.findById(permissionRepresentation.getId());

        removeUserPoliciesFromPermission(tenantId, userIds, permissionResource, permissionRepresentation);
    }


    @Override
    public void removeAllDelegationManagedDesks(@NotNull String tenantId, @NotNull String userId) {

        String userPolicyName = getUserPolicyName(userId);

        UserPolicyResource userPolicy = getUserPolicyWithName(userPolicyName);
        if (userPolicy == null) {
            log.warn("Could not retrieve user policy with name '{}' by id", userPolicyName);
            return;
        }

        Map<String, String> targetRegexSubstitutions = Map.of(TENANT_PLACEHOLDER, tenantId, DESK_PLACEHOLDER, ID_REGEX);
        String delegationManagerForTenantRegex = StringSubstitutor.replace(PERMISSION_DESK_DELEGATION_MANAGER_INTERNAL_NAME, targetRegexSubstitutions);
        Pattern delegationManagerForTenantPattern = Pattern.compile(delegationManagerForTenantRegex);

        log.debug("userPolicy.dependentPolicies() : {}", userPolicy.dependentPolicies());

        removePolicySDependentPermissionsMatchingPattern(userPolicyName, userPolicy, delegationManagerForTenantPattern);
    }


    @Override
    public @NotNull List<String> getDelegationManagerOfDesk(@NotNull String tenantId, @NotNull String deskId) {

        // Desk permissions are directly accessible through the permission itself,
        // no need for an Oauth2/PAT request here.
        Map<String, String> targetSubstitutions = Map.of(TENANT_PLACEHOLDER, tenantId, DESK_PLACEHOLDER, deskId);
        String permissionName = StringSubstitutor.replace(PERMISSION_DESK_DELEGATION_MANAGER_INTERNAL_NAME, targetSubstitutions);
        ScopePermissionResource permissionResource = Optional.ofNullable(scopePermissionsClient.findByName(permissionName))
                .map(AbstractPolicyRepresentation::getId)
                .map(scopePermissionsClient::findById)
                .orElse(null);

        if (permissionResource == null) {
            return emptyList();
        }

        return permissionResource.associatedPolicies().stream()
                .map(AbstractPolicyRepresentation::getName)
                .filter(StringUtils::isNotEmpty)
                .map(USER_POLICY_INTERNAL_NAME_PATTERN::matcher)
                .filter(Matcher::matches)
                .map(m -> m.group(MATCH_GROUP_USER_ID))
                .toList();
    }


    @Override
    public @NotNull List<DeskRepresentation> getDelegationManagedDesks(@NotNull String userId, @Nullable String tenantId) {
        List<String> deskIds;
        if (tenantId != null) {
            deskIds = getScopeLinkedResourceIdsForTenant(userId, RESOURCE_TYPE_DESK, SCOPE_DESK_DELEGATION_MANAGER, tenantId);
        } else {
            deskIds = getScopeLinkedResourceIds(userId, RESOURCE_TYPE_DESK, SCOPE_DESK_DELEGATION_MANAGER);
        }

        return deskIds.stream().map(DeskRepresentation::new).collect(toList());
    }


    // </editor-fold desc="Delegation manager CRUDL">


    // <editor-fold desc="Functional admin CRUDL">


    @Override
    public void setFunctionalAdmin(@NotNull String tenantId, @NotNull String targetDeskId, @NotNull String userId) {

        Map<String, String> targetSubstitutions = Map.of(TENANT_PLACEHOLDER, tenantId, DESK_PLACEHOLDER, targetDeskId);
        String policyName = StringSubstitutor.replace(PERMISSION_DESK_FUNCTIONAL_ADMIN_INTERNAL_NAME, targetSubstitutions);

        ScopePermissionRepresentation permissionRepresentation = getOrCreateDeskScopePermission(tenantId,
                policyName,
                of(SCOPE_DESK_FUNCTIONAL_ADMIN),
                targetDeskId);
        if (CollectionUtils.size(permissionRepresentation.getPolicies()) >= MAX_FUNCTIONAL_ADMIN_PER_DESK) {
            throw new LocalizedStatusException(INSUFFICIENT_STORAGE, "message.already_n_functional_admin_maximum_reached", MAX_FUNCTIONAL_ADMIN_PER_DESK);
        }

        UserPolicyRepresentation sourcePolicyRepresentation = getOrCreateUserPolicy(userId);
        permissionRepresentation.addPolicy(sourcePolicyRepresentation.getId());
        scopePermissionsClient.findById(permissionRepresentation.getId())
                .update(permissionRepresentation);
    }


    @Override
    public void removeFunctionalAdmin(@NotNull String tenantId, @NotNull String sourceDeskId, @NotNull String userId) {

        Map<String, String> targetDeskSubstitutions = Map.of(TENANT_PLACEHOLDER, tenantId, DESK_PLACEHOLDER, sourceDeskId);
        String associationName = StringSubstitutor.replace(PERMISSION_DESK_FUNCTIONAL_ADMIN_INTERNAL_NAME, targetDeskSubstitutions);

        ScopePermissionRepresentation permissionRepresentation = scopePermissionsClient.findByName(associationName);
        ScopePermissionResource permissionResource = scopePermissionsClient.findById(permissionRepresentation.getId());

        removeUserPoliciesFromPermission(tenantId, singletonList(userId), permissionResource, permissionRepresentation);
    }


    @Override
    public void removeAllAdministeredDesks(@NotNull String tenantId, @NotNull String userId) {

        String userPolicyName = getUserPolicyName(userId);

        UserPolicyResource userPolicy = getUserPolicyWithName(userPolicyName);
        if (userPolicy == null) {
            log.warn("Could not retrieve user policy with name '{}' by id", userPolicyName);
            return;
        }

        Map<String, String> targetRegexSubstitutions = Map.of(TENANT_PLACEHOLDER, tenantId, DESK_PLACEHOLDER, ID_REGEX);
        String functionalAdminForTenantRegex = StringSubstitutor.replace(PERMISSION_DESK_FUNCTIONAL_ADMIN_INTERNAL_NAME, targetRegexSubstitutions);
        Pattern functionalAdminForTenantPattern = Pattern.compile(functionalAdminForTenantRegex);

        log.debug("userPolicy.dependentPolicies() : {}", userPolicy.dependentPolicies());

        removePolicySDependentPermissionsMatchingPattern(userPolicyName, userPolicy, functionalAdminForTenantPattern);
    }


    @Override
    public @NotNull List<DeskRepresentation> getAdministeredDesks(@NotNull String userId, @Nullable String tenantId) {
        List<String> deskIds;
        if (tenantId != null) {
            deskIds = getScopeLinkedResourceIdsForTenant(userId, RESOURCE_TYPE_DESK, SCOPE_DESK_FUNCTIONAL_ADMIN, tenantId);
        } else {
            deskIds = getScopeLinkedResourceIds(userId, RESOURCE_TYPE_DESK, SCOPE_DESK_FUNCTIONAL_ADMIN);
        }

        return deskIds.stream().map(DeskRepresentation::new).collect(toList());
    }


    // </editor-fold desc="Functional admin CRUDL">


    // <editor-fold desc="Associated desks CRUDL">


    @Override
    public void associateDesks(@NotNull String tenantId, @NotNull String sourceDeskId, @NotNull Collection<String> associateDeskIds) {

        if (CollectionUtils.isEmpty(associateDeskIds)) {
            return;
        }

        linkDeskToResource(
                associateDeskIds,
                getOrCreateDeskOwnerPolicy(tenantId, sourceDeskId),
                i -> {
                    Map<String, String> targetSubstitutions = Map.of(TENANT_PLACEHOLDER, tenantId, DESK_PLACEHOLDER, i);
                    String policyName = StringSubstitutor.replace(PERMISSION_DESK_ASSOCIATION_INTERNAL_NAME, targetSubstitutions);
                    return getOrCreateDeskScopePermission(tenantId, policyName, of(SCOPE_DESK_ASSOCIATION), i);
                },
                MAX_ASSOCIATED_DESK_PER_DESK,
                "message.already_n_associated_desks_maximum_reached"
        );
    }


    @Override
    public void removeAssociatedDesks(@NotNull String tenantId, @NotNull String sourceDeskId, @NotNull Collection<String> associateDeskIds) {

        if (CollectionUtils.isEmpty(associateDeskIds)) {
            return;
        }

        RolePolicyRepresentation deskOwnerPolicyRepresentation = getOrCreateDeskOwnerPolicy(tenantId, sourceDeskId);

        associateDeskIds.forEach(id -> {
            Map<String, String> targetSubstitutions = Map.of(TENANT_PLACEHOLDER, tenantId, DESK_PLACEHOLDER, id);
            String associationName = StringSubstitutor.replace(PERMISSION_DESK_ASSOCIATION_INTERNAL_NAME, targetSubstitutions);
            ScopePermissionRepresentation permissionRepresentation = scopePermissionsClient.findByName(associationName);
            ScopePermissionResource resource = scopePermissionsClient.findById(permissionRepresentation.getId());

            Set<String> remainingPolicies = resource.associatedPolicies()
                    .stream()
                    .map(AbstractPolicyRepresentation::getId)
                    .filter(i -> !StringUtils.equals(deskOwnerPolicyRepresentation.getId(), i))
                    .collect(toSet());

            if (remainingPolicies.isEmpty()) {
                resource.remove();
            } else {
                permissionRepresentation.setPolicies(remainingPolicies);
                resource.update(permissionRepresentation);
            }
        });

        RolePolicyResource deskOwnerPolicyResource = rolePoliciesClient.findById(deskOwnerPolicyRepresentation.getId());
        if (deskOwnerPolicyResource.dependentPolicies().size() == 0) {
            deskOwnerPolicyResource.remove();
        }
    }


    @Override
    public @NotNull List<String> getAssociatedDesks(@NotNull String tenantId, @NotNull String sourceDeskId) {

        Set<String> prefetchResourceIds = preFetchTargetResources(of(sourceDeskId), null, RESOURCE_TYPE_DESK, SCOPE_DESK_ASSOCIATION).stream()
                .map(TargetResourceRepresentation::getId)
                .collect(toSet());

        Map<String, List<String>> requestMap = prefetchResourceIds.stream().collect(toMap(s -> s, s -> singletonList(SCOPE_DESK_ASSOCIATION)));

        // Special case : we don't bother to ask Keycloak
        log.debug("getAssociatedDesks prefetch:{}", requestMap);
        if (requestMap.isEmpty()) {
            return emptyList();
        }

        Map<String, String> substitutions = Map.of(TENANT_PLACEHOLDER, tenantId, DESK_PLACEHOLDER, sourceDeskId);
        String roleInternalName = StringSubstitutor.replace(DESK_INTERNAL_NAME, substitutions);
        String accessToken = getServiceAccountAccessToken(roleInternalName);
        return getPermissions(accessToken, roleInternalName, requestMap)
                .stream()
                .map(Permission::getResourceId)
                .toList();
    }


    // </editor-fold desc="Associated desks CRUDL">


    // <editor-fold desc="Filterable metadata CRUDL">


    @Override
    public void addFilterableMetadata(@NotNull String tenantId, @NotNull String sourceDeskId, @NotNull Collection<String> metadataIds) {

        if (CollectionUtils.isEmpty(metadataIds)) {
            return;
        }

        linkDeskToResource(
                metadataIds,
                getOrCreateDeskOwnerPolicy(tenantId, sourceDeskId),
                metadataId -> {
                    Map<String, String> metadataSubstitutions = Map.of(TENANT_PLACEHOLDER, tenantId, METADATA_PLACEHOLDER, metadataId);
                    String metadataInternalName = StringSubstitutor.replace(METADATA_INTERNAL_NAME, metadataSubstitutions);
                    String permissionInternalName = StringSubstitutor.replace(PERMISSION_FILTERABLE_METADATA_INTERNAL_NAME, metadataSubstitutions);
                    return getOrCreateScopePermission(permissionInternalName, METADATA_SCOPES, metadataInternalName);
                },
                MAX_FILTERABLE_METADATA_PER_DESK,
                "message.already_n_filterable_metadata_maximum_reached"
        );
    }


    @Override
    public void removeFilterableMetadata(@NotNull String tenantId, @NotNull String sourceDeskId, @NotNull Collection<String> metadataIds) {

        if (CollectionUtils.isEmpty(metadataIds)) {
            return;
        }

        Map<String, String> sourceSubstitutions = Map.of(TENANT_PLACEHOLDER, tenantId, DESK_PLACEHOLDER, sourceDeskId);
        String sourceOwnerPolicyName = StringSubstitutor.replace(POLICY_DESK_OWNER_INTERNAL_NAME, sourceSubstitutions);
        PolicyRepresentation deskOwnerPolicyRepresentation = policiesClient.findByName(sourceOwnerPolicyName);

        metadataIds.forEach(id -> {
            Map<String, String> targetSubstitutions = Map.of(TENANT_PLACEHOLDER, tenantId, METADATA_PLACEHOLDER, id);
            String associationName = StringSubstitutor.replace(PERMISSION_FILTERABLE_METADATA_INTERNAL_NAME, targetSubstitutions);
            ScopePermissionRepresentation permissionRepresentation = scopePermissionsClient.findByName(associationName);
            ScopePermissionResource resource = scopePermissionsClient.findById(permissionRepresentation.getId());

            Set<String> remainingPolicies = resource.associatedPolicies()
                    .stream()
                    .map(AbstractPolicyRepresentation::getId)
                    .filter(i -> !StringUtils.equals(deskOwnerPolicyRepresentation.getId(), i))
                    .collect(toSet());

            if (remainingPolicies.isEmpty()) {
                resource.remove();
            } else {
                permissionRepresentation.setPolicies(remainingPolicies);
                resource.update(permissionRepresentation);
            }
        });

        RolePolicyResource deskOwnerPolicyResource = rolePoliciesClient.findById(deskOwnerPolicyRepresentation.getId());
        if (deskOwnerPolicyResource.dependentPolicies().size() == 0) {
            deskOwnerPolicyResource.remove();
        }
    }


    @Override
    public @NotNull List<String> getFilterableMetadataIds(@NotNull String tenantId, @NotNull String sourceDeskId) {

        Set<String> prefetchResourceIds = preFetchTargetResources(of(sourceDeskId), null, RESOURCE_TYPE_METADATA, SCOPE_FILTER).stream()
                .map(TargetResourceRepresentation::getId)
                .collect(toSet());

        Map<String, List<String>> requestMap = prefetchResourceIds.stream().collect(toMap(s -> s, s -> singletonList(SCOPE_FILTER)));

        // Special case : we don't bother to ask Keycloak
        log.debug("getFilterableMetadataIds prefetch:{}", requestMap);
        if (requestMap.isEmpty()) {
            return emptyList();
        }

        Map<String, String> substitutions = Map.of(TENANT_PLACEHOLDER, tenantId, DESK_PLACEHOLDER, sourceDeskId);
        String roleInternalName = StringSubstitutor.replace(DESK_INTERNAL_NAME, substitutions);
        String accessToken = getServiceAccountAccessToken(roleInternalName);
        return getPermissions(accessToken, roleInternalName, requestMap)
                .stream()
                .map(Permission::getResourceId)
                .toList();
    }


    // </editor-fold desc="Filterable metadata CRUDL">


    // <editor-fold desc="Subtype usage allowed CRUDL">


    @Override
    public void setSubtypeCreationPermittedDeskIds(@NotNull String tenantId, @NotNull String subtypeId, @Nullable Collection<String> deskIds) {
        log.debug("setSubtypeAllowedDeskIds deskIds:{}", deskIds);

        Map<String, String> targetSubstitutions = Map.of(TENANT_PLACEHOLDER, tenantId, SUBTYPE_PLACEHOLDER, subtypeId);
        String usableSubtypePermissionName = StringSubstitutor.replace(PERMISSION_USABLE_SUBTYPE_INTERNAL_NAME, targetSubstitutions);

        // Update Resource scopes, if needed

        ResourceRepresentation resourceRepresentation = authzClient.protection().resource().findById(subtypeId);
        Set<ScopeRepresentation> scopeRepresentations = new HashSet<>(resourceRepresentation.getScopes());

        boolean isPublic = scopeRepresentations.stream().noneMatch(s -> StringUtils.equals(s.getName(), SCOPE_USE));
        boolean isRestricted = scopeRepresentations.stream().noneMatch(s -> StringUtils.equals(s.getName(), SCOPE_PUBLIC_USE));

        if (isRestricted && (deskIds == null)) {
            scopeRepresentations.removeIf(s -> StringUtils.equals(s.getName(), SCOPE_USE));
            scopeRepresentations.add(new ScopeRepresentation(SCOPE_PUBLIC_USE));
            resourceRepresentation.setScopes(scopeRepresentations);
            authzClient.protection().resource().update(resourceRepresentation);
        }

        if (isPublic && (deskIds != null)) {
            scopeRepresentations.removeIf(s -> StringUtils.equals(s.getName(), SCOPE_PUBLIC_USE));
            scopeRepresentations.add(new ScopeRepresentation(SCOPE_USE));
            resourceRepresentation.setScopes(scopeRepresentations);
            authzClient.protection().resource().update(resourceRepresentation);
        }

        // Special case : if deskIds are null, we go for the public scope
        // and clean things if needed.
        if (deskIds == null) {
            Optional.ofNullable(scopePermissionsClient.findByName(usableSubtypePermissionName))
                    .map(ScopePermissionRepresentation::getId)
                    .ifPresent(i -> clientResource.authorization().permissions().resource().findById(i).remove());
        }
        // Any other case, we update the policies with the given ones.
        else {

            ScopePermissionRepresentation usableSubtypePermissionRepresentation = getOrCreateSubtypeScopePermission(
                    tenantId, usableSubtypePermissionName, of(SCOPE_USE), subtypeId
            );

            deskIds.stream()
                    .map(i -> getOrCreateDeskOwnerPolicy(tenantId, i))
                    .peek(r -> {
                        if (CollectionUtils.size(r.getPolicies()) >= MAX_USABLE_SUBTYPE_PER_DESK) {
                            throw new LocalizedStatusException(
                                    INSUFFICIENT_STORAGE, "message.already_n_usable_subtypes_maximum_reached", MAX_USABLE_SUBTYPE_PER_DESK
                            );
                        }
                    })
                    .map(AbstractPolicyRepresentation::getId)
                    .forEach(usableSubtypePermissionRepresentation::addPolicy);

            scopePermissionsClient.findById(usableSubtypePermissionRepresentation.getId())
                    .update(usableSubtypePermissionRepresentation);
        }
    }


    @Override
    public @Nullable List<String> getSubtypePermittedDeskIds(@NotNull String tenantId, @NotNull String subtypeId) {

        Map<String, String> substitutions = Map.of(TENANT_PLACEHOLDER, tenantId, SUBTYPE_PLACEHOLDER, subtypeId);
        String subtypeInternalName = StringSubstitutor.replace(SUBTYPE_INTERNAL_NAME, substitutions);
        boolean isPublic = resourcesClient.findByName(subtypeInternalName)
                .stream()
                .map(ResourceRepresentation::getScopes)
                .flatMap(Collection::stream)
                .map(ScopeRepresentation::getName)
                .anyMatch(n -> StringUtils.equals(SCOPE_PUBLIC_USE, n));

        log.debug("getSubtypePermittedDeskIds isPublic:{}", isPublic);
        if (isPublic) {
            return null;
        }

        String subtypePermissionInternalName = StringSubstitutor.replace(PERMISSION_USABLE_SUBTYPE_INTERNAL_NAME, substitutions);
        log.debug("getSubtypePermittedDeskIds subtypePermissionInternalName:{}", subtypePermissionInternalName);
        ScopePermissionRepresentation scopePermissionRep = scopePermissionsClient.findByName(subtypePermissionInternalName);
        // if no specific resource is found, the permission falls back to public
        if (scopePermissionRep == null) {
            log.debug("getSubtypePermittedDeskIds - no specific perm resource found, fall back to public");
            return null;
        }
        ScopePermissionResource scopePermission = scopePermissionsClient.findById(scopePermissionRep.getId());

        return scopePermission.associatedPolicies().stream()
                .map(PolicyRepresentation::getDescription)
                .toList();
    }


    @Override
    public @NotNull List<String> getAllowedSubtypeIds(@NotNull String tenantId, @NotNull String sourceDeskId) {

        List<String> result = new ArrayList<>();

        // Public permissions

        resourcesClient.find(null, null, null, RESOURCE_TYPE_SUBTYPE, SCOPE_PUBLIC_USE, 0, MAX_VALUE)
                .stream()
                .map(ResourceRepresentation::getId)
                .forEach(result::add);

        // Specific permissions

        // We are pre-fetching things here to speed up the poor performances on the Keycloak query request
        Set<String> specificPrefetchResourceIds = preFetchTargetResources(of(sourceDeskId), null, RESOURCE_TYPE_SUBTYPE, SCOPE_USE).stream()
                .map(TargetResourceRepresentation::getId)
                .collect(toSet());

        Map<String, List<String>> requestMap = specificPrefetchResourceIds.stream().collect(toMap(s -> s, s -> List.of(SCOPE_USE)));
        log.debug("getAllowedSubtypeIds prefetch:{}", requestMap);

        if (CollectionUtils.isNotEmpty(specificPrefetchResourceIds)) {
            Map<String, String> substitutions = Map.of(TENANT_PLACEHOLDER, tenantId, DESK_PLACEHOLDER, sourceDeskId);
            String roleInternalName = StringSubstitutor.replace(DESK_INTERNAL_NAME, substitutions);
            String accessToken = getServiceAccountAccessToken(roleInternalName);
            getPermissions(accessToken, roleInternalName, requestMap)
                    .stream()
                    .map(Permission::getResourceId)
                    .forEach(result::add);
        }

        log.debug("getAllowedSubtypeIds result:{}", result);
        return result;
    }


    // </editor-fold desc="Subtype usage allowed CRUDL">


    // <editor-fold desc="Filterable subtype CRUDL">


    @Override
    public void setSubtypeFilterableByDeskIds(@NotNull String tenantId, @NotNull String subtypeId, @Nullable Collection<String> deskIds) {
        log.debug("setSubtypeFilterableByDeskIds deskIds:{}", deskIds);

        Map<String, String> targetSubstitutions = Map.of(TENANT_PLACEHOLDER, tenantId, SUBTYPE_PLACEHOLDER, subtypeId);
        String filterableSubtypePermissionName = StringSubstitutor.replace(PERMISSION_FILTERABLE_SUBTYPE_INTERNAL_NAME, targetSubstitutions);

        // Update Resource scopes, if needed

        ResourceRepresentation resourceRepresentation = authzClient.protection().resource().findById(subtypeId);
        Set<ScopeRepresentation> scopeRepresentations = new HashSet<>(resourceRepresentation.getScopes());

        boolean isPublic = scopeRepresentations.stream().noneMatch(s -> StringUtils.equals(s.getName(), SCOPE_FILTER));
        boolean isRestricted = scopeRepresentations.stream().noneMatch(s -> StringUtils.equals(s.getName(), SCOPE_PUBLIC_FILTER));

        if (isRestricted && (deskIds == null)) {
            scopeRepresentations.removeIf(s -> StringUtils.equals(s.getName(), SCOPE_FILTER));
            scopeRepresentations.add(new ScopeRepresentation(SCOPE_PUBLIC_FILTER));
            resourceRepresentation.setScopes(scopeRepresentations);
            authzClient.protection().resource().update(resourceRepresentation);
        }

        if (isPublic && (deskIds != null)) {
            scopeRepresentations.removeIf(s -> StringUtils.equals(s.getName(), SCOPE_PUBLIC_FILTER));
            scopeRepresentations.add(new ScopeRepresentation(SCOPE_FILTER));
            resourceRepresentation.setScopes(scopeRepresentations);
            authzClient.protection().resource().update(resourceRepresentation);
        }

        // Update policies

        // Special case : if deskIds are null, we go for the public scope
        // and clean things if needed.
        if (deskIds == null) {
            Optional.ofNullable(scopePermissionsClient.findByName(filterableSubtypePermissionName))
                    .map(ScopePermissionRepresentation::getId)
                    .ifPresent(i -> clientResource.authorization().permissions().resource().findById(i).remove());
        }
        // Any other case, we update the policies with the given ones.
        else {
            ScopePermissionRepresentation usableSubtypePermissionRepresentation = getOrCreateSubtypeScopePermission(
                    tenantId, filterableSubtypePermissionName, of(SCOPE_FILTER), subtypeId
            );

            deskIds.stream()
                    .map(i -> getOrCreateDeskOwnerPolicy(tenantId, i))
                    .peek(r -> {
                        if (CollectionUtils.size(r.getPolicies()) >= MAX_FILTERABLE_SUBTYPE_PER_DESK) {
                            throw new LocalizedStatusException(
                                    INSUFFICIENT_STORAGE, "message.already_n_filterable_subtypes_maximum_reached", MAX_FILTERABLE_SUBTYPE_PER_DESK
                            );
                        }
                    })
                    .map(AbstractPolicyRepresentation::getId)
                    .forEach(usableSubtypePermissionRepresentation::addPolicy);

            scopePermissionsClient.findById(usableSubtypePermissionRepresentation.getId())
                    .update(usableSubtypePermissionRepresentation);
        }
    }


    @Override
    public @Nullable List<String> getSubtypeFilterableByDeskIds(@NotNull String tenantId, @NotNull String subtypeId) {

        Map<String, String> substitutions = Map.of(TENANT_PLACEHOLDER, tenantId, SUBTYPE_PLACEHOLDER, subtypeId);
        String subtypeInternalName = StringSubstitutor.replace(SUBTYPE_INTERNAL_NAME, substitutions);
        boolean isPublic = resourcesClient.findByName(subtypeInternalName)
                .stream()
                .map(ResourceRepresentation::getScopes)
                .flatMap(Collection::stream)
                .map(ScopeRepresentation::getName)
                .anyMatch(n -> StringUtils.equals(SCOPE_PUBLIC_FILTER, n));

        log.debug("getSubtypeFilterableByDeskIds isPublic:{}", isPublic);
        if (isPublic) {
            return null;
        }

        String subtypePermissionInternalName = StringSubstitutor.replace(PERMISSION_FILTERABLE_SUBTYPE_INTERNAL_NAME, substitutions);
        log.debug("getSubtypeFilterableByDeskIds subtypePermissionInternalName:{}", subtypePermissionInternalName);
        ScopePermissionRepresentation scopePermissionRep = scopePermissionsClient.findByName(subtypePermissionInternalName);
        // if no specific resource is found, the permission falls back to public
        if (scopePermissionRep == null) {
            log.debug("getSubtypeFilterableByDeskIds - no specific perm resource found, fall back to public");
            return null;
        }
        ScopePermissionResource scopePermission = scopePermissionsClient.findById(scopePermissionRep.getId());

        return scopePermission.associatedPolicies().stream()
                .map(PolicyRepresentation::getDescription)
                .toList();
    }


    @Override
    public @NotNull List<String> getFilterableSubtypeIds(@NotNull String tenantId, @NotNull String sourceDeskId) {

        List<String> result = new ArrayList<>();

        // Public permissions

        resourcesClient.find(null, null, null, RESOURCE_TYPE_SUBTYPE, SCOPE_PUBLIC_FILTER, 0, MAX_VALUE)
                .stream()
                .map(ResourceRepresentation::getId)
                .forEach(result::add);

        // Specific permissions

        // We are pre-fetching things here to speed up the poor performances on the Keycloak query request
        Set<String> specificPrefetchResourceIds = preFetchTargetResources(of(sourceDeskId), null, RESOURCE_TYPE_SUBTYPE, SCOPE_FILTER).stream()
                .map(TargetResourceRepresentation::getId)
                .collect(toSet());

        Map<String, List<String>> requestMap = specificPrefetchResourceIds.stream().collect(toMap(s -> s, s -> List.of(SCOPE_FILTER)));
        log.debug("getFilterableSubtypeIds prefetch:{}", requestMap);

        if (CollectionUtils.isNotEmpty(specificPrefetchResourceIds)) {
            Map<String, String> substitutions = Map.of(TENANT_PLACEHOLDER, tenantId, DESK_PLACEHOLDER, sourceDeskId);
            String roleInternalName = StringSubstitutor.replace(DESK_INTERNAL_NAME, substitutions);
            String accessToken = getServiceAccountAccessToken(roleInternalName);
            getPermissions(accessToken, roleInternalName, requestMap)
                    .stream()
                    .map(Permission::getResourceId)
                    .forEach(result::add);
        }

        log.debug("getFilterableSubtypeIds result:{}", result);
        return result;
    }


    // </editor-fold desc="Filterable subtype CRUDL">


}
