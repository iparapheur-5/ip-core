/*
 * Workflow
 * Copyright (C) 2019-2020 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.ipcore.services.mail;

import coop.libriciel.ipcore.model.auth.Desk;
import coop.libriciel.ipcore.model.auth.DeskRepresentation;
import coop.libriciel.ipcore.model.auth.User;
import coop.libriciel.ipcore.model.crypto.SealCertificateRepresentation;
import coop.libriciel.ipcore.model.database.Tenant;
import coop.libriciel.ipcore.model.database.TenantRepresentation;
import coop.libriciel.ipcore.model.mail.MailContent;
import coop.libriciel.ipcore.model.mail.MailNotification;
import coop.libriciel.ipcore.model.mail.MailTarget;
import coop.libriciel.ipcore.model.mail.PostponedNotification;
import coop.libriciel.ipcore.model.workflow.Folder;
import coop.libriciel.ipcore.model.workflow.State;
import coop.libriciel.ipcore.model.workflow.Task;
import coop.libriciel.ipcore.services.auth.AuthServiceInterface;
import coop.libriciel.ipcore.services.database.TenantRepository;
import coop.libriciel.ipcore.services.scheduler.SchedulerServiceInterface;
import coop.libriciel.ipcore.services.workflow.WorkflowServiceInterface;
import coop.libriciel.ipcore.utils.TextUtils;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.scheduling.support.CronExpression;
import org.springframework.stereotype.Service;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;

import javax.activation.DataHandler;
import javax.annotation.PostConstruct;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import static coop.libriciel.ipcore.controller.admin.AdminTemplateController.TEMPLATE_PATH;
import static coop.libriciel.ipcore.model.auth.User.USER_PREFERENCE_SINGLE_NOTIFICATIONS;
import static coop.libriciel.ipcore.model.database.TemplateType.MAIL_NOTIFICATION_DIGEST;
import static coop.libriciel.ipcore.model.database.TemplateType.MAIL_NOTIFICATION_SINGLE;
import static coop.libriciel.ipcore.model.workflow.FolderSortBy.FOLDER_NAME;
import static coop.libriciel.ipcore.model.workflow.NotificationType.*;
import static coop.libriciel.ipcore.model.workflow.State.PENDING;
import static coop.libriciel.ipcore.utils.TextUtils.INTERNAL_PREFIX;
import static coop.libriciel.ipcore.utils.TextUtils.MESSAGE_BUNDLE;
import static java.lang.Integer.MAX_VALUE;
import static java.nio.charset.StandardCharsets.UTF_8;
import static java.util.Collections.singletonList;
import static java.util.stream.Collectors.*;
import static javax.mail.Message.RecipientType.*;
import static org.apache.commons.lang3.StringUtils.EMPTY;
import static org.springframework.http.MediaType.APPLICATION_PDF_VALUE;
import static org.springframework.http.MediaType.TEXT_HTML_VALUE;


@Log4j2
@Service(NotificationServiceInterface.BEAN_NAME)
@ConditionalOnProperty(name = NotificationServiceInterface.PREFERENCES_PROVIDER_KEY, havingValue = "spring")
public class NotificationService implements NotificationServiceInterface {


    private static final String MAIL_FIELD_TASK_LIST = INTERNAL_PREFIX + "task_list";
    private static final String MAIL_FIELD_FROM_USER_ID = INTERNAL_PREFIX + "from_user_id";
    private static final String MAIL_FIELD_FROM_USERNAME = INTERNAL_PREFIX + "from_username";
    private static final String MAIL_FIELD_FROM_FIRST_NAME = INTERNAL_PREFIX + "from_first_name";
    private static final String MAIL_FIELD_FROM_LAST_NAME = INTERNAL_PREFIX + "from_last_name";
    private static final String MAIL_FIELD_MESSAGE = INTERNAL_PREFIX + "message";
    private static final String MAIL_FIELD_MAIN_URL = INTERNAL_PREFIX + "main_url";
    private static final String MAIL_FIELD_FOOTER = INTERNAL_PREFIX + "footer";
    private static final String MAIL_FIELD_EXPIRED_SEAL_CERTIFICATE_LIST = INTERNAL_PREFIX + "expired_seal_certificate_list";
    private static final String MAIL_FIELD_TENANT = INTERNAL_PREFIX + "tenant";

    private @Value(TEMPLATE_PATH + "mail_notification_single.ftl") Resource mailNotificationSingleTemplateResource;
    private @Value(TEMPLATE_PATH + "mail_notification_digest.ftl") Resource mailNotificationDigestTemplateResource;
    private @Value(TEMPLATE_PATH + "mail_notification_expired_certificate.ftl") Resource mailNotificationExpiredCertificateResource;


    // <editor-fold desc="Beans">


    private final AuthServiceInterface authService;
    private final Configuration configuration;
    private final JavaMailSender javaMailSender;
    private final ModelMapper modelMapper;
    private final NotificationServiceProperties properties;
    private final PostponedNotificationCacheRepository mailNotificationCacheRepository;
    private final SchedulerServiceInterface schedulerService;
    private final TenantRepository tenantRepository;
    private final WorkflowServiceInterface workflowService;


    @Autowired
    public NotificationService(AuthServiceInterface authService,
                               Configuration configuration,
                               JavaMailSender javaMailSender,
                               ModelMapper modelMapper,
                               NotificationServiceProperties properties,
                               PostponedNotificationCacheRepository mailNotificationCacheRepository,
                               SchedulerServiceInterface schedulerService,
                               TenantRepository tenantRepository,
                               WorkflowServiceInterface workflowService) {
        this.authService = authService;
        this.configuration = configuration;
        this.javaMailSender = javaMailSender;
        this.modelMapper = modelMapper;
        this.properties = properties;
        this.mailNotificationCacheRepository = mailNotificationCacheRepository;
        this.schedulerService = schedulerService;
        this.tenantRepository = tenantRepository;
        this.workflowService = workflowService;
    }


    @PostConstruct
    public void init() {
        configuration.setClassLoaderForTemplateLoading(getClass().getClassLoader(), "/");
    }


    // </editor-fold desc="Beans">


    @Scheduled(cron = "${services.mail.late-folder-notification-cron:-}")
    public void notifyLateFolders() {
        this.tenantRepository.findAll()
                .forEach(tenant -> {
                            List<MailNotification> mailNotifications = new ArrayList<>();

                            this.workflowService
                                    .listFolders(
                                            tenant.getId(),
                                            0,
                                            MAX_VALUE,
                                            FOLDER_NAME,
                                            null,
                                            true,
                                            null,
                                            null,
                                            null,
                                            null,
                                            State.LATE
                                    )
                                    .getData()
                                    .stream()
                                    .map(folder -> this.workflowService.getFolder(folder.getId(), tenant.getId()))
                                    .filter(Objects::nonNull)
                                    .filter(folder -> folder.getStepList() != null)
                                    .forEach(folder -> mailNotifications.add(createLateMailNotification(tenant.getId(), folder)));

                            log.info("Sending late folder notifications for tenant : {}", tenant.getId());

                            mailNotifications.forEach(mailNotification -> this.notify(mailNotification, null));
                        }
                );
    }


    private MailNotification createLateMailNotification(String tenantId, Folder folder) {
        Task currentTask = folder
                .getStepList()
                .stream()
                .filter(step -> step.getState().equals(PENDING))
                .findFirst()
                .orElse(null);

        MailNotification mailNotification = new MailNotification();
        mailNotification.setNotificationType(LATE);
        mailNotification.setTenantId(tenantId);
        mailNotification.setInstanceName(folder.getName());

        if (currentTask != null) {
            mailNotification.setDeskIdsToNotify(currentTask
                    .getDesks()
                    .stream()
                    .map(DeskRepresentation::getId)
                    .collect(toCollection(HashSet::new))
            );

            mailNotification.setAdditionalDeskIdsToNotify(currentTask
                    .getNotifiedDesks()
                    .stream()
                    .map(DeskRepresentation::getId)
                    .collect(toCollection(HashSet::new))
            );

            mailNotification.setInstanceId(currentTask.getId());
        }
        return mailNotification;
    }


    @Override
    public void notify(@NotNull MailNotification notification, @Nullable User sender) {
        log.debug("notify - Notification {}", notification);

        Set<String> deskIds = Stream
                .concat(
                        notification.getDeskIdsToNotify().stream(),
                        notification.getAdditionalDeskIdsToNotify().stream()
                )
                .collect(toSet());

        log.trace("notify - deskIds :  {}", deskIds);
        Map<String, DeskRepresentation> deskMap = authService.getDeskNames(deskIds)
                .entrySet()
                .stream()
                .collect(toMap(
                        Map.Entry::getKey,
                        entry -> new DeskRepresentation(entry.getKey(), entry.getValue())
                ));

        // Sending mails

        Map<String, User> usersToNotifyMap = deskIds
                .stream()
                .map(deskMap::get)
                .filter(Objects::nonNull)
                // TODO : This, in a single request ?
                .map(d -> authService.listUsersFromDesk(notification.getTenantId(), d.getId(), 0, MAX_VALUE))
                .flatMap(p -> p.getData().stream())
                .distinct()
                .collect(toMap(User::getId, u -> u));

        log.trace("notify - usersToNotify ids :  {}", usersToNotifyMap.keySet());

        // Direct sending

        List<User> directlyNotifiedUsers = usersToNotifyMap
                .values()
                .stream()
                .filter(u -> StringUtils.equals(u.getNotificationsCronFrequency(), USER_PREFERENCE_SINGLE_NOTIFICATIONS))
                .filter(u -> userHasActivatedNotification(u, notification))
                .toList();

        Resource mailResource = Optional.ofNullable(notification.getTenantId())
                .flatMap(tenantRepository::findById)
                .map(Tenant::getCustomTemplates)
                .map(m -> m.get(MAIL_NOTIFICATION_SINGLE))
                .map(s -> (Resource) new ByteArrayResource(s.getBytes(UTF_8)))
                .orElse(mailNotificationSingleTemplateResource);

        if (CollectionUtils.isNotEmpty(directlyNotifiedUsers)) {
            prepareAndSendMails(mailResource, directlyNotifiedUsers, singletonList(notification), sender);
        }

        // Postponed sending

        usersToNotifyMap.values().stream()
                .filter(u -> StringUtils.isNotEmpty(u.getNotificationsCronFrequency()))
                .filter(u -> CronExpression.isValidExpression(u.getNotificationsCronFrequency()))
                .filter(u -> userHasActivatedNotification(u, notification))
                .forEach(u -> {
                    log.trace("notify - user {} has cron and notification is active, preparing postponed notification", u.getId());
                    PostponedNotification postponedNotification = new PostponedNotification();
                    postponedNotification.setUserId(u.getId());
                    postponedNotification.setMailNotification(notification);
                    mailNotificationCacheRepository.save(postponedNotification);

                    if (!schedulerService.hasScheduledNotifications(u.getId())) {
                        CronExpression cronTrigger = CronExpression.parse(u.getNotificationsCronFrequency());
                        LocalDateTime nextExecution = cronTrigger.next(LocalDateTime.now());

                        if (nextExecution != null) {
                            schedulerService.addNotificationScheduler(u.getId(), Date.from(nextExecution.atZone(ZoneId.systemDefault()).toInstant()));
                        } else {
                            log.error(
                                    "An error occurred while parsing the next cron trigger. Tenant : {}, User : {},  Task {}",
                                    notification.getTenantId(),
                                    u.getId(),
                                    notification.getTaskId()
                            );
                        }

                    } else {
                        log.trace("notify - user {}  already has an active notification scheduler", u.getId());
                    }
                });
    }


    /**
     * Triggered by a User's cron expression,
     * we should retrieve every pending mails from Redis cache, and send the actual digested mail.
     */
    @Override
    public void computePendingMails(@NotNull String userId) {
        log.debug("computePendingMails userId:{}", userId);

        User user = Optional.ofNullable(authService.findUserById(userId))
                .orElseThrow(() -> new RuntimeException("computePendingMails called with an unknown user Id"));

        // FIXME mailNotificationCacheRepository.findAllByUserId() is returning an empty list, fix it then use it
        List<PostponedNotification> postponedNotifications = StreamSupport
                .stream(mailNotificationCacheRepository.findAll().spliterator(), false)
                .filter(notification -> notification.getUserId().equals(userId))
                .toList();

        log.debug("computePendingMails userId:{} found:{}", userId, postponedNotifications.size());

        List<MailNotification> mailNotificationList = postponedNotifications.stream()
                .map(PostponedNotification::getMailNotification)
                .toList();

        if (mailNotificationList.isEmpty()) {
            // Nothing to do here
            return;
        }

        // Retrieving the appropriate template.
        // The custom template is applicable only on single-targeted tenant.
        // If the current user should receive notifications from multiple tenants, we go for the default template.

        Resource mailResource = mailNotificationDigestTemplateResource;
        List<String> tenantList = mailNotificationList.stream()
                .map(MailNotification::getTenantId)
                .filter(StringUtils::isNotEmpty)
                .distinct()
                .toList();

        if (tenantList.size() == 1) {
            mailResource = tenantList.stream()
                    .findFirst()
                    .flatMap(tenantRepository::findById)
                    .map(Tenant::getCustomTemplates)
                    .map(m -> m.get(MAIL_NOTIFICATION_DIGEST))
                    .map(s -> (Resource) new ByteArrayResource(s.getBytes(UTF_8)))
                    .orElse(mailNotificationDigestTemplateResource);
        }

        // Sending mail

        prepareAndSendMails(mailResource, singletonList(user), mailNotificationList, user);
        mailNotificationCacheRepository.deleteAll(postponedNotifications);
    }


    @Override
    public void resetNotificationScheduler(String userId, String newNotificationFrequency) {
        schedulerService.deleteExistingNotificationScheduler(userId);

        if (NotificationServiceInterface.isScheduledNotification(newNotificationFrequency)) {
            // TODO potentially creating one scheduler per user, this may be a little heavy on the Quartz engine... to be evaluated and maybe refactored
            try {
                CronExpression cronTrigger = CronExpression.parse(newNotificationFrequency);
                LocalDateTime nextExecution = cronTrigger.next(LocalDateTime.now());
                if (nextExecution != null) {
                    schedulerService.addNotificationScheduler(userId, Date.from(nextExecution.atZone(ZoneId.systemDefault()).toInstant()));
                }
            } catch (Exception e) {
                log.warn("error while rescheduling cron notification job for user {}, some notifications might still be on hold...", userId);
                log.debug("error details : ", e);
            }
        }
    }


    @Override
    public void mailFolder(@NotNull List<MailTarget> targetList,
                           @NotNull Resource mailTemplate,
                           @NotNull List<MailContent> contentList,
                           @Nullable User sender,
                           @Nullable String object,
                           @Nullable String message,
                           @Nullable InputStream mergedDocsInputStream,
                           @Nullable String attachementFilename) {
        log.debug("mailFolder ");
        String htmlMessage = prepareFolderMessage(sender, contentList, message, mailTemplate);
        log.debug("mailFolder - htmlMessage prepared");
        mail(targetList, htmlMessage, sender, object, mergedDocsInputStream, attachementFilename);
    }


    String prepareFolderMessage(@Nullable User sender,
                                @NotNull List<MailContent> contentList,
                                @Nullable String message,
                                @NotNull Resource mailTemplate) {

        // Fetch missing desk names

        Set<String> deskIds = contentList.stream()
                .filter(content -> StringUtils.isEmpty(content.getDeskName()))
                .map(MailContent::getDeskId)
                .collect(toSet());

        Map<String, String> deskIdNameMap = authService.getDeskNames(deskIds);
        String missingDeskName = ResourceBundle.getBundle(MESSAGE_BUNDLE, Locale.getDefault()).getString("message.deleted_desk");

        contentList.stream()
                .filter(mail -> StringUtils.isEmpty(mail.getDeskName()))
                .forEach(mail -> mail.setDeskName(deskIdNameMap.getOrDefault(mail.getDeskId(), missingDeskName)));

        // Sanitize values

        contentList.forEach(mail -> {
            mail.setDeskName(TextUtils.sanitizeTemplateMessage(mail.getDeskName()));
            mail.setPublicAnnotation(TextUtils.sanitizeTemplateMessage(mail.getPublicAnnotation()));
            mail.setTaskActionString(TextUtils.sanitizeTemplateMessage(mail.getTaskActionString()));
            mail.setTaskActionPastString(TextUtils.sanitizeTemplateMessage(mail.getTaskActionPastString()));
        });

        String sanitizedMessage = TextUtils.sanitizeTemplateMessage(message);
        String sanitizedFooter = TextUtils.sanitizeTemplateMessage(properties.getFooter());
        String sanitizedUserName = TextUtils.sanitizeTemplateMessage(Optional.ofNullable(sender).map(User::getUserName).orElse(null));
        String sanitizedFirstName = TextUtils.sanitizeTemplateMessage(Optional.ofNullable(sender).map(User::getFirstName).orElse(null));
        String sanitizedLastName = TextUtils.sanitizeTemplateMessage(Optional.ofNullable(sender).map(User::getLastName).orElse(null));

        // Building FreeTemplate map

        Map<String, Object> fields = new HashMap<>();
        fields.put(MAIL_FIELD_TASK_LIST, contentList);
        fields.put(MAIL_FIELD_MESSAGE, sanitizedMessage);
        fields.put(MAIL_FIELD_FOOTER, sanitizedFooter);
        fields.put(MAIL_FIELD_MAIN_URL, properties.getBaseUrl());
        fields.put(MAIL_FIELD_FROM_USER_ID, Optional.ofNullable(sender).map(User::getId).orElse(EMPTY));
        fields.put(MAIL_FIELD_FROM_USERNAME, sanitizedUserName);
        fields.put(MAIL_FIELD_FROM_FIRST_NAME, sanitizedFirstName);
        fields.put(MAIL_FIELD_FROM_LAST_NAME, sanitizedLastName);

        // Then build the message content

        return prepareAndProcessBasicFields(fields, mailTemplate);
    }


    @Override
    public void mailExpiredCertificates(@NotNull List<User> usersToNotify,
                                        @NotNull TenantRepresentation tenant,
                                        @NotNull List<SealCertificateRepresentation> certificateList) {

        if (CollectionUtils.isEmpty(usersToNotify)) {
            log.warn("A certificate is about to expire on the tenant {}, but the tenant has no admin to notify...", tenant.getName());
            return;
        }

        List<MailTarget> mailTargetList = MailTarget.fromUserList(usersToNotify, null);
        String htmlMessage = prepareExpiredCertificatesMessage(tenant, certificateList);

        ResourceBundle messages = ResourceBundle.getBundle(MESSAGE_BUNDLE, Locale.getDefault());
        String object = messages.getString("message.expiring_seal_certificate_alert");
        mail(mailTargetList, htmlMessage, null, object, null, null);
    }


    private String prepareExpiredCertificatesMessage(@NotNull TenantRepresentation tenant,
                                                     @NotNull List<SealCertificateRepresentation> certificates) {

        // Sanitize values

        TenantRepresentation sanitizedTenant = new TenantRepresentation(tenant.getId(), TextUtils.sanitizeTemplateMessage(tenant.getName()));
        String sanitizedFooter = TextUtils.sanitizeTemplateMessage(properties.getFooter());
        certificates.forEach(c -> c.setName(TextUtils.sanitizeTemplateMessage(c.getName())));

        // Building FreeTemplate map

        Map<String, Object> fields = new HashMap<>();

        fields.put(MAIL_FIELD_EXPIRED_SEAL_CERTIFICATE_LIST, certificates);
        fields.put(MAIL_FIELD_TENANT, sanitizedTenant);
        fields.put(MAIL_FIELD_FOOTER, sanitizedFooter);

        // Then build the message content

        return prepareAndProcessBasicFields(fields, mailNotificationExpiredCertificateResource);
    }


    private String prepareAndProcessBasicFields(Map<String, Object> fields, Resource template) {

        // Then build the message content

        try (InputStream resourceInputStream = template.getInputStream();
             InputStreamReader resourceInputStreamReader = new InputStreamReader(resourceInputStream)) {

            Template templateBody = new Template(template.getFilename(), resourceInputStreamReader, configuration);
            return FreeMarkerTemplateUtils.processTemplateIntoString(templateBody, fields);

        } catch (IOException | TemplateException e) {
            throw new RuntimeException("Error generating mail", e);
        }
    }


    private void mail(@NotNull List<MailTarget> targetList,
                      @NotNull String bodyString,
                      @Nullable User sender,
                      @Nullable String object,
                      @Nullable InputStream mergedDocsInputStream,
                      @Nullable String attachementFilename) {
        log.debug("mail - targetList length : {}", targetList);
        log.trace("mail - target addresses : {}", targetList.stream().map(MailTarget::getTo).toList());

        List<MimeMessage> mimeMessages = targetList.stream()
                .map(t -> {
                    try {
                        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
                        mimeMessage.setRecipients(TO, InternetAddress.parse(String.join(",", t.getTo())));

                        if (CollectionUtils.isNotEmpty(t.getCc())) {
                            mimeMessage.setRecipients(CC, InternetAddress.parse(String.join(",", t.getCc())));
                        }
                        if (CollectionUtils.isNotEmpty(t.getCc())) {
                            mimeMessage.setRecipients(BCC, InternetAddress.parse(String.join(",", t.getBcc())));
                        }

                        mimeMessage.setSubject(properties.getObjectPrefix().trim() + " " + object);
                        mimeMessage.setFrom(properties.getFrom());

                        if (Optional.ofNullable(sender)
                                .map(User::getEmail)
                                .filter(StringUtils::isNotEmpty)
                                .isPresent()) {
                            InternetAddress[] replyToAddresses = InternetAddress.parse(sender.getEmail());
                            mimeMessage.setReplyTo(replyToAddresses);
                        }

                        MimeBodyPart mimeBodyPart = new MimeBodyPart();
                        mimeBodyPart.setContent(bodyString, TEXT_HTML_VALUE);

                        Multipart multipart = new MimeMultipart();
                        multipart.addBodyPart(mimeBodyPart);
                        mimeMessage.setContent(multipart);

                        if (mergedDocsInputStream != null) {
                            String targetFilename = attachementFilename != null ? attachementFilename : "document.pdf";
                            MimeBodyPart attachmentPart = new MimeBodyPart();
                            attachmentPart.setDataHandler(new DataHandler(new ByteArrayDataSource(mergedDocsInputStream, APPLICATION_PDF_VALUE)));
                            attachmentPart.setHeader("Content-Type", APPLICATION_PDF_VALUE + "; " + targetFilename);
                            attachmentPart.setFileName(targetFilename);
                            multipart.addBodyPart(attachmentPart);
                        }

                        return mimeMessage;

                    } catch (MessagingException e) {
                        log.error("Error preparing message", e);
                        return null;
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    }
                })
                .filter(Objects::nonNull)
                .toList();

        javaMailSender.send(mimeMessages.toArray(MimeMessage[]::new));
    }


    /**
     * Computing every needed value, before sending the actual things.
     */
    private void prepareAndSendMails(@NotNull Resource mailTemplate,
                                     @NotNull List<User> usersToNotify,
                                     @NotNull List<MailNotification> notificationList,
                                     @Nullable User user) {

        // Pre-computing content
        log.debug(
                "prepareAndSendMails - usersToNotify : {}, nb notifications : {}",
                usersToNotify.stream().map(User::getId).toList(),
                notificationList.size()
        );
        List<MailContent> mailContentList = notificationList
                .stream()
                .map(n -> new MailContent(
                        Tenant.builder().id(n.getTenantId()).build(),
                        Folder.builder().id(n.getInstanceId()).name(n.getInstanceName()).build(),
                        Optional.ofNullable(n.getTaskId())
                                .map(taskId -> {
                                    try {
                                        return workflowService.getTask(taskId);
                                    } catch (Exception e) {
                                        // FIXME: Find why we have to get this task, and narrow the Exception
                                        log.warn("Flushing pending mail could not find the folder for task:{}, skipping...", taskId);
                                        log.trace("Flushing pending mail error...", e);
                                        return null;
                                    }
                                })
                                .orElse(null),
                        getDeskForNotification(n),
                        user,
                        n.getNotificationType()
                ))
                .toList();

        // Compute targets

        List<MailTarget> targetList = MailTarget.fromUserList(usersToNotify, Optional.ofNullable(user).map(User::getEmail).orElse(null));

        String unPrefixedObject;
        // TODO : extract those values in resource.messages
        if (notificationList.size() == 1) {
            // "Dossier en cours" -> vient d'arriver dans
            MailNotification notification = notificationList.get(0);
            unPrefixedObject = "Dossier à traiter : \"%s\"".formatted(notification.getInstanceName());
        } else {
            unPrefixedObject = "Notifications groupées";
        }

        mailFolder(targetList, mailTemplate, mailContentList, user, unPrefixedObject, null, null, null);
    }


    private DeskRepresentation getDeskForNotification(MailNotification notification) {
        String deskId = notification.getDeskIdsToNotify().stream().findFirst().orElse(null);

        if (deskId == null) {
            return null;
        }
        Desk desk = authService.findDeskByIdNoException(notification.getTenantId(), deskId);
        // FIXME remove modelMapper when all internal actions are mapped to Desk in controllers
        return modelMapper.map(desk, DeskRepresentation.class);
    }


    private boolean userHasActivatedNotification(User user, MailNotification notification) {
        return (notification.getNotificationType().equals(NEW_ON_DESK) && user.isNotifiedOnConfidentialFolders())
                || (notification.getNotificationType().equals(ACTION_PERFORMED) && user.isNotifiedOnFollowedFolders())
                || (notification.getNotificationType().equals(LATE) && user.isNotifiedOnLateFolders());
    }

}
