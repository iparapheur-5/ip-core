/*
 * iparapheur Core
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.ipcore.services.filetransfer;

import coop.libriciel.ipcore.model.filetransfer.FolderUnpackedContent;
import coop.libriciel.ipcore.model.workflow.Folder;
import coop.libriciel.ipcore.utils.LocalizedStatusException;
import org.jetbrains.annotations.NotNull;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import static coop.libriciel.ipcore.utils.TextUtils.NONE_SERVICE;
import static org.springframework.http.HttpStatus.SERVICE_UNAVAILABLE;


@Service(FileTransferServiceInterface.BEAN_NAME)
@ConditionalOnProperty(name = FileTransferServiceInterface.PREFERENCES_PROVIDER_KEY, havingValue = NONE_SERVICE)
public class NoneFileTransferService implements FileTransferServiceInterface {


    @Override
    public @NotNull String buildRetrievalUrl(String baseUrl, String fileName) {
        throw new LocalizedStatusException(SERVICE_UNAVAILABLE, "message.file_transfer_service_not_available");
    }


    @Override
    public String postFolder(@NotNull Folder folder, String targetName) {
        throw new LocalizedStatusException(SERVICE_UNAVAILABLE, "message.file_transfer_service_not_available");
    }


    @Override
    public FolderUnpackedContent getFolderFilesFromUrl(@NotNull String url) {
        throw new LocalizedStatusException(SERVICE_UNAVAILABLE, "message.file_transfer_service_not_available");
    }


}
