/*
 * iparapheur Core
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.ipcore.utils;

import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.StringSubstitutor;
import org.jetbrains.annotations.NotNull;
import org.keycloak.KeycloakPrincipal;
import org.keycloak.KeycloakSecurityContext;
import org.keycloak.representations.AccessToken;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.server.ResponseStatusException;

import java.util.*;
import java.util.regex.Matcher;

import static coop.libriciel.ipcore.services.auth.KeycloakService.*;
import static coop.libriciel.ipcore.utils.TextUtils.*;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;


@Log4j2
public class KeycloakSecurityUtils {


    public static KeycloakPrincipal<KeycloakSecurityContext> getLoggedInPrincipal() {
        Authentication token = SecurityContextHolder.getContext().getAuthentication();
        if (token == null) {
            throw new IpInternalException("No authenticated user");
        }
        //noinspection unchecked
        return (KeycloakPrincipal<KeycloakSecurityContext>) token.getPrincipal();
    }


    public static String getCurrentSessionUserId() {
        KeycloakPrincipal<KeycloakSecurityContext> principal = getLoggedInPrincipal();
        if (principal == null) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Aucun utilisateur connecté");
        }
        return principal.getName();
    }


    public static AccessToken getCurrentSessionAccessToken() {
        KeycloakPrincipal<KeycloakSecurityContext> principal = getLoggedInPrincipal();
        KeycloakSecurityContext session = principal.getKeycloakSecurityContext();
        return session.getToken();
    }


    public static @NotNull Set<String> getCurrentUserTenantIds() {

        AccessToken accessToken = KeycloakSecurityUtils.getCurrentSessionAccessToken();
        Collection<String> roles = accessToken.getRealmAccess().getRoles();
        log.trace("Token roles:{}", roles);

        Set<String> result = roles.stream()
                .map(role -> Optional.of(TENANT_ID_NAME_PATTERN.matcher(role))
                        .filter(Matcher::matches)
                        .or(() -> Optional.of(TENANT_ID_ADMIN_NAME_PATTERN.matcher(role)))
                        .filter(Matcher::matches)
                        .or(() -> Optional.of(TENANT_ID_FUNCTIONAL_ADMIN_NAME_PATTERN.matcher(role)))
                        .filter(Matcher::matches)
                )
                .filter(Optional::isPresent)
                .map(Optional::get)
                .map(m -> m.group(MATCH_GROUP_TENANT_ID))
                .collect(toSet());

        log.debug("getCurrentUserTenantIds result:{}", result);
        return result;
    }


    public static @NotNull List<String> getCurrentUserDeskIds() {

        AccessToken accessToken = KeycloakSecurityUtils.getCurrentSessionAccessToken();
        Collection<String> roles = accessToken.getRealmAccess().getRoles();
        log.trace("Token roles:{}", roles);

        List<String> result = roles.stream()
                .map(DESK_INTERNAL_NAME_PATTERN::matcher)
                .filter(Matcher::matches)
                .map(m -> m.group(MATCH_GROUP_DESK_ID))
                .collect(toList());

        log.debug("getCurrentUserDeskIds result:{}", result);
        return result;
    }


    public static boolean isSuperAdmin() {

        AccessToken accessToken = getCurrentSessionAccessToken();

        log.trace("Token roles:{}", accessToken.getRealmAccess().getRoles());
        return accessToken.getRealmAccess().getRoles().contains(SUPER_ADMIN_ROLE_NAME);
    }


    public static boolean currentUserIsTenantAdmin(@NotNull String tenantId) {
        Map<String, String> tenantIdMapping = Map.of(TENANT_PLACEHOLDER, tenantId);
        String tenantAdminRoleName = StringSubstitutor.replace(TENANT_ADMIN_INTERNAL_NAME, tenantIdMapping);

        AccessToken accessToken = getCurrentSessionAccessToken();
        return accessToken.getRealmAccess().getRoles()
                .stream()
                .anyMatch(roleName -> StringUtils.equals(tenantAdminRoleName, roleName));
    }


}
