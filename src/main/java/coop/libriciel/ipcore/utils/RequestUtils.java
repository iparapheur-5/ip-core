/*
 * iparapheur Core
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.ipcore.utils;

import coop.libriciel.ipcore.model.content.DocumentBuffer;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.EnumUtils;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.core.io.buffer.DataBufferUtils;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.ExchangeStrategies;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.time.Duration;
import java.util.HashMap;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Stream;

import static coop.libriciel.ipcore.utils.FileUtils.parseFileNameFromHeaders;
import static java.nio.charset.StandardCharsets.UTF_8;
import static org.springframework.core.io.buffer.DataBufferUtils.releaseConsumer;
import static org.springframework.http.HttpHeaders.CONTENT_DISPOSITION;
import static org.springframework.http.HttpStatus.*;
import static org.springframework.http.MediaType.APPLICATION_OCTET_STREAM;
import static reactor.core.scheduler.Schedulers.boundedElastic;


@Log4j2
public class RequestUtils {

    public static final int DEFAULT_PIPE_SIZE = 8192;
    private static HashMap<Integer, WebClient.Builder> webClientBuilderMap = new HashMap<>();

    public static final Duration SUBSERVICE_REQUEST_SHORT_TIMEOUT = Duration.ofSeconds(5);
    public static final Duration SUBSERVICE_REQUEST_LONG_TIMEOUT = Duration.ofSeconds(60);

    public static final long WEBCLIENT_CLOSE_DELAY_SEC = 10;


    /**
     * JPA database request asks for a column name.
     * Yet, we don't want those names to be listed anywhere publicly, in some kind of enum.
     * <p>
     * We want a standard enum, that we can control.
     * The inner value shall be translated.
     *
     * @param pageable        the pageable source
     * @param enumClass       the target enum definition
     * @param sortByConverter the method that transform the type into the inner column
     * @param <E>             a target SortBy enum
     * @return the same Pageable object, with inner (hidden) Sort values
     */
    public static <E extends Enum<E>> Pageable convertSortedPageable(@NotNull Pageable pageable,
                                                                     @NotNull Class<E> enumClass,
                                                                     @NotNull Function<E, String> sortByConverter) {

        if (pageable == Pageable.unpaged()) {
            return pageable;
        }

        Sort patchedSort = Sort.by(pageable.getSort().stream()
                .map(order -> new Sort.Order(
                        order.getDirection(),
                        // Here is the actual translation
                        Optional.ofNullable(EnumUtils.getEnum(enumClass, order.getProperty()))
                                .map(sortByConverter)
                                .filter(StringUtils::isNotEmpty)
                                .orElseThrow(() -> new LocalizedStatusException(BAD_REQUEST, "message.unknown_sort_by_enum_value_s", order.getProperty())),
                        order.getNullHandling()
                ))
                .toList()
        );

        return PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(), patchedSort);
    }


    public static String writeBufferToString(@NotNull DocumentBuffer documentBuffer) {

        try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {

            DataBufferUtils.write(documentBuffer.getContentFlux(), baos)
                    .map(DataBufferUtils::release)
                    .then()
                    .block(SUBSERVICE_REQUEST_LONG_TIMEOUT);

            baos.flush();
            return baos.toString(UTF_8);

        } catch (IOException e) {
            throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, e, "message.error_reading_file");
        }
    }


    public static InputStream bufferToInputStream(@NotNull DocumentBuffer documentBuffer) throws IOException {
        log.debug("bufferToInputStream");
        PipedOutputStream osPipe = new PipedOutputStream();
        // default size for IS is 1024, whereas experiment show that for databuffer flux it is 8192
        PipedInputStream isPipe = new PipedInputStream(osPipe, DEFAULT_PIPE_SIZE);

        DataBufferUtils.write(documentBuffer.getContentFlux(), osPipe)
                .timeout(SUBSERVICE_REQUEST_LONG_TIMEOUT.minusSeconds(5))
                .subscribeOn(boundedElastic())
                .doOnTerminate(() -> {
                    try {
                        osPipe.close();
                    } catch (IOException e) {
                        log.error("bufferToInputStream - Error closing input stream", e);
                    }
                })
                .subscribe(releaseConsumer());
        return isPipe;
    }


    public static void writeBufferToResponse(@NotNull DocumentBuffer documentBuffer, @NotNull HttpServletResponse response) {

        log.debug("writeBufferToResponse");
        try (OutputStream responseOutput = response.getOutputStream()) {

            DataBufferUtils.write(documentBuffer.getContentFlux(), responseOutput)
                    .map(DataBufferUtils::release)
                    .then()
                    .block(SUBSERVICE_REQUEST_LONG_TIMEOUT);

            response.setContentType(documentBuffer.getMediaType().toString());
            response.setContentLengthLong(documentBuffer.getContentLength());
            response.setHeader(CONTENT_DISPOSITION, "attachment;filename=test.pdf");

            log.debug(
                    "writeBufferToResponse fileName:{} mediaType:{} length:{}",
                    documentBuffer.getName(),
                    documentBuffer.getMediaType(),
                    documentBuffer.getContentLength()
            );

            response.flushBuffer();

        } catch (IOException e) {
            throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, e, "message.error_reading_file");
        }
    }


    public static @NotNull InputStream pipeFluxAsInputStream(@NotNull Flux<DataBuffer> inFlux) throws IOException {
        log.debug("pipeFluxAsInputStream");
        PipedOutputStream osPipe = new PipedOutputStream();
        PipedInputStream isPipe = new PipedInputStream(osPipe, DEFAULT_PIPE_SIZE);

        DataBufferUtils.write(inFlux, osPipe)
                .timeout(SUBSERVICE_REQUEST_LONG_TIMEOUT.minusSeconds(5))
                .subscribeOn(boundedElastic())
                .doOnTerminate(() -> {
                    try {
                        osPipe.close();
                    } catch (IOException e) {
                        log.error("pipeAsInputStream - Error closing input stream", e);
                    }
                })
                .subscribe(releaseConsumer());

        return isPipe;
    }


    @SafeVarargs
    public static <T, U extends Comparable<U>> boolean hasChangesBetween(T o1, T o2, Function<T, U>... getters) {
        return Stream.of(getters).anyMatch(g -> g.apply(o1) != g.apply(o2));
    }


    private static WebClient.Builder webClientBuilderForSize(Integer bufferSizeKey) {
        WebClient.Builder builder = webClientBuilderMap.get(bufferSizeKey);
        if (builder == null) {
            ExchangeStrategies exchangeStrategies = ExchangeStrategies.builder()
                    .codecs(configurer -> configurer.defaultCodecs().maxInMemorySize(1024 * 1024 * bufferSizeKey)).build();

            builder = WebClient.builder().exchangeStrategies(exchangeStrategies);
            webClientBuilderMap.put(bufferSizeKey, builder);
        }

        return builder;
    }


    @NotNull
    public static WebClient getWebClientWithBufferSize(int bufferSizeInMo, boolean withForcedExceptionConnector) {
        WebClient.Builder builder = webClientBuilderForSize(bufferSizeInMo);

        if (withForcedExceptionConnector) {
            builder.clientConnector(new ForcedExceptionsConnector());
        }

        return builder.build();
    }


    @NotNull
    public static Flux<DataBuffer> clientResponseToDataBufferFlux(ClientResponse response, DocumentBuffer result, String defaultDocName) {
        if (response.statusCode() == OK) {
            log.debug("clientResponseToDataBufferFlux - converting to flux");
            result.setMediaType(response.headers().contentType().orElse(APPLICATION_OCTET_STREAM));
            result.setName(parseFileNameFromHeaders(response.headers(), defaultDocName));
            result.setContentLength(response.headers().contentLength().orElse(-1L));
            log.debug("clientResponseToDataBufferFlux - converting to flux - result.getContentLength() : {}", result.getContentLength());
            return response.bodyToFlux(DataBuffer.class);
        } else {
            return response.createException().flatMapMany(Mono::error);
        }
    }


    @NotNull
    public static Flux<DataBuffer> clientResponseToRawDataBufferFlux(ClientResponse response) {
        if (response.statusCode() == OK) {
            log.debug("clientResponseToRawDataBufferFlux - converting to flux");
            return response.bodyToFlux(DataBuffer.class);
        } else {
            return response.createException().flatMapMany(Mono::error);
        }
    }

}
