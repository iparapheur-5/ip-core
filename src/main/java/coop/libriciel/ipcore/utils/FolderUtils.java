/*
 * iparapheur Core
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.ipcore.utils;

import coop.libriciel.ipcore.model.workflow.Action;
import coop.libriciel.ipcore.model.workflow.Folder;
import coop.libriciel.ipcore.model.workflow.State;
import coop.libriciel.ipcore.model.workflow.Task;
import coop.libriciel.ipcore.model.workflow.Task.ExternalState;
import coop.libriciel.ipcore.model.workflow.ipWorkflow.IpWorkflowTask;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.collections4.CollectionUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static coop.libriciel.ipcore.model.workflow.State.*;
import static coop.libriciel.ipcore.model.workflow.Task.ExternalState.*;
import static java.util.Collections.emptyList;
import static java.util.Comparator.comparing;
import static java.util.Comparator.naturalOrder;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;


@Log4j2
public class FolderUtils {


    private FolderUtils() {
        throw new IllegalStateException("Utility class");
    }


    public static ExternalState mapTaskStatus(String state) {
        return switch (state) {
            case "ACTIVE" -> ACTIVE;
            case "SIGNED" -> SIGNED;
            case "REFUSED" -> REFUSED;
            case "EXPIRED" -> EXPIRED;
            case "SENT" -> SENT;
            case "RECEIVED" -> RECEIVED;
            case "CREATED" -> CREATED;
            case "FORM" -> FORM;
            case "ERROR" -> ERROR;
            case "creation" -> CREATED;
            case "modification" -> IN_REDACTION;
            case "supression" -> DELETED;
            case "envoi" -> SENT;
            case "renvoi" -> SENT_AGAIN;
            case "reception-partielle" -> RECEIVED_PARTIALLY;
            case "reception" -> RECEIVED;
            case "non-recu" -> NOT_RECEIVED;
            default -> throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.state_not_found");
        };
    }


    public static boolean isActive(@Nullable State state) {

        if (state == null) {
            return false;
        }

        return Set.of(CURRENT, PENDING).contains(state);
    }


    public static State computeFolderStateFromTaskList(@NotNull List<IpWorkflowTask> tasks) {
        return tasks.stream()
                .filter(task -> isActive(task.getState()))
                .findFirst()
                .map(FolderUtils::computeFolderStateFromTask)
                .orElseGet(() -> {
                    log.warn("computeFolderStateFromTaskList cannot determine the folder state from tasks:{}", tasks);
                    return PENDING;
                });
    }


    public static State computeFolderStateFromTask(@NotNull IpWorkflowTask task) {
        return switch (task.getAction()) {
            case START -> DRAFT;
            case DELETE -> REJECTED;
            case ARCHIVE -> FINISHED;
            default -> Optional.ofNullable(task.getFolderDueDate())
                    .filter(dueDate -> dueDate.compareTo(new Date()) <= 0)
                    .map(d -> LATE)
                    .orElse(PENDING);
        };
    }


    /**
     * Workflow index is :
     * - 0 on draft/start
     * - 1 in the creation workflow
     * - 2 in the validation workflow
     * - n for any chained validation workflow
     * - MAX_INT at the end
     *
     * @param folder with a {@link Folder#getStepList} properly available
     * @return
     */
    public static boolean hasBeenStarted(@NotNull Folder folder) {

        if (CollectionUtils.isEmpty(folder.getStepList())) {
            throw new RuntimeException("hasBeenStarted should be called with an initialized folder's step list");
        }

        return folder.getStepList().stream()
                .filter(task -> isActive(task.getState()))
                .map(Task::getWorkflowIndex)
                .anyMatch(workflowIndex -> workflowIndex > 1);
    }


    public static @Nullable Task getLastPerformedTask(@Nullable Folder folder, @NotNull Set<Action> actionsFilter) {
        return Optional.ofNullable(folder)
                .map(Folder::getStepList)
                .orElse(emptyList())
                .stream()
                .filter(t -> actionsFilter.contains(t.getAction()))
                .filter(t -> t.getState() == VALIDATED)
                .filter(t -> t.getDate() != null)
                .max(comparing(Task::getDate, naturalOrder()))
                .orElse(null);
    }

}
