/*
 * iparapheur Core
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.ipcore.utils;

import lombok.extern.log4j.Log4j2;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.boot.configurationprocessor.json.JSONStringer;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.LongSupplier;
import java.util.function.Predicate;


@Log4j2
public class CollectionUtils {


    public static <T> @Nullable T popValue(@Nullable Map<String, T> map, @NotNull String key) {

        if (map == null) {
            return null;
        }

        T result = map.get(key);
        map.remove(key);
        return result;
    }


    public static <T> void pushValue(@NotNull Map<String, Set<T>> map, @NotNull String key, @NotNull T value) {
        map.computeIfAbsent(key, k -> new HashSet<>());
        map.get(key).add(value);
    }


    public static void safeStartObject(JSONStringer stringer) {
        try {
            stringer.object();
        } catch (JSONException e) {
            log.warn("safeStartObject error", e);
        }
    }


    public static void safeEndObject(JSONStringer stringer) {
        try {
            stringer.endObject();
        } catch (JSONException e) {
            log.warn("safeEndObject error", e);
        }
    }


    public static void safeAddKeyValue(JSONStringer stringer, String key, String string) {
        try {
            stringer.key(key).value(string);
        } catch (JSONException e) {
            log.warn("safeAddKeyValue error", e);
        }
    }


    public static @NotNull Map<String, String> parseMetadata(@Nullable String[][] data) {

        Map<String, String> metadata = new HashMap<>();

        if (data == null) {
            return metadata;
        }

        Arrays.stream(data)
                .filter(entry -> entry.length == 2)
                .forEach(entry -> metadata.put(entry[0], entry[1]));

        return metadata;
    }


    public static <T> Predicate<T> distinctByProperty(Function<? super T, ?> keyExtractor) {
        Set<Object> seen = ConcurrentHashMap.newKeySet();
        return t -> seen.add(keyExtractor.apply(t));
    }


    /**
     * Managing computable cases, avoiding unnecessary evaluations
     *
     * @param data          The final list, should not be modified afterwards
     * @param page          Starting at 0
     * @param pageSize      Page chunk size
     * @param totalSupplier The lazy evaluator
     * @return
     */
    public static long computeTotal(@NotNull List<?> data, long page, long pageSize, @NotNull LongSupplier totalSupplier) {

        // No data, nothing to compute
        if ((data.size() == 0) && (page == 0)) {
            return 0;
        }

        // If the current page is not half-set, we can compute the total
        if ((data.size() != 0) && (data.size() != pageSize)) {
            return page * pageSize + data.size();
        }

        // Default case, we need to compute the total
        return totalSupplier.getAsLong();
    }


}
