/*
 * iparapheur Core
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.ipcore.utils;

import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.http.ContentDisposition;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.client.ClientResponse;

import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import static org.springframework.http.HttpHeaders.CONTENT_DISPOSITION;
import static org.springframework.http.MediaType.*;
import static org.springframework.util.ResourceUtils.CLASSPATH_URL_PREFIX;
import static org.springframework.util.ResourceUtils.FILE_URL_PREFIX;


@Log4j2
public class FileUtils {


    /**
     * Sometimes, we cannot use the more appropriate {@link ContentDisposition#parse}.
     * Alfresco itself does not return an RFC 5987 compliant header when the file has parenthesis in its name.
     * We go for the manual regex.
     */
    private static final Pattern ATTACHMENT_FILENAME_PATTERN = Pattern.compile("\\sfilename=\"(?<filename>.*?)\";\\s");


    /**
     * Send back the most appropriate overridable resource in this order :
     * - The current file path (<code>/</code> in a Docker image)
     * - The classpath (the <code>src/main/resource</code> folder, before compilation)
     * - A {@link RuntimeException}, if nothing matches.
     * <p>
     * Weird that SpringBoot does not have such a feature for any resource.
     * It natively works for `application.properties|yml` only.
     *
     * @param resourceLoader Should be autowired
     * @param resourceName   If it includes a prefixed path, the overridden file should be prefixed too
     * @return an existing {@link Resource} file
     */
    public static @NotNull Resource getOverridableResource(@NotNull ResourceLoader resourceLoader, @NotNull String resourceName) {
        Resource result = resourceLoader.getResource(FILE_URL_PREFIX + resourceName);

        if (result.exists()) {
            log.debug("Overridden '{}' file found", resourceName);
            return result;
        }

        return Optional.of(resourceLoader.getResource(CLASSPATH_URL_PREFIX + resourceName))
                .filter(Resource::exists)
                .orElseThrow(() -> new RuntimeException("The resource " + resourceName + " should have a default value in the resource folder."));
    }


    public static @Nullable String parseFileNameFromHeaders(@NotNull ClientResponse.Headers headers, @Nullable String defaultValue) {
        Optional<String> resultOpt = headers.header(CONTENT_DISPOSITION)
                .stream()
                .map(FileUtils::parseFileNameFromAttachment)
                .filter(StringUtils::isNotEmpty)
                .findFirst();

        if (resultOpt.isEmpty()) {
            log.debug("parseFileNameFromHeaders - could not find filename in response headers, using default value : {}", defaultValue);
        }

        return resultOpt.orElse(defaultValue);
    }


    public static @Nullable String parseFileNameFromAttachment(@Nullable String attachmentHeader) {
        return Optional.ofNullable(attachmentHeader)
                .map(ATTACHMENT_FILENAME_PATTERN::matcher)
                .filter(Matcher::find)
                .map(m -> m.group("filename"))
                .orElse(null);
    }


    public static @NotNull String cleanFileSystemForbiddenCharacters(@NotNull String filename) {
        return filename
                // Linux/Unix
                .replaceAll("/", "_")
                // MacOS
                .replaceAll(":", "-")
                // Windows
                .replaceAll("<", "_")
                .replaceAll(">", "_")
                .replaceAll("\\\\", "_")
                .replaceAll("\\|", "_")
                .replaceAll("\\?", "_")
                .replaceAll("\\*", "_");
    }


    public static boolean isXml(@Nullable MediaType mediaType) {
        return (mediaType != null) && Stream
                .of(APPLICATION_XML, APPLICATION_XHTML_XML, TEXT_XML, TEXT_HTML)
                .anyMatch(s -> s.equalsTypeAndSubtype(mediaType));
    }


}
