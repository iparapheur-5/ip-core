/*
 * iparapheur Core
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.ipcore.utils;

import coop.libriciel.ipcore.model.content.Document;
import coop.libriciel.ipcore.model.crypto.PdfSignaturePosition;
import coop.libriciel.ipcore.model.crypto.SealCertificate;
import coop.libriciel.ipcore.model.crypto.SignatureFormat;
import coop.libriciel.ipcore.model.database.Subtype;
import coop.libriciel.ipcore.model.database.Type;
import coop.libriciel.ipcore.model.pdfstamp.SignaturePlacement;
import coop.libriciel.ipcore.model.workflow.Action;
import coop.libriciel.ipcore.model.workflow.Folder;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.collections4.CollectionUtils;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.asn1.x500.AttributeTypeAndValue;
import org.bouncycastle.asn1.x500.RDN;
import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.asn1.x500.style.IETFUtils;
import org.bouncycastle.cert.jcajce.JcaX509CertificateHolder;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.Signature;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.*;
import java.util.stream.Stream;

import static coop.libriciel.ipcore.model.crypto.PdfSignaturePosition.Origin.BOTTOM_LEFT;
import static coop.libriciel.ipcore.model.crypto.PdfSignaturePosition.Origin.CENTER;
import static coop.libriciel.ipcore.model.crypto.SignatureFormat.*;
import static coop.libriciel.ipcore.model.workflow.Action.EXTERNAL_SIGNATURE;
import static coop.libriciel.ipcore.model.workflow.Action.SIGNATURE;
import static coop.libriciel.ipcore.model.workflow.State.BYPASSED;
import static coop.libriciel.ipcore.model.workflow.State.VALIDATED;
import static coop.libriciel.ipcore.utils.TextUtils.INTERNAL_PREFIX;
import static java.util.Collections.emptyList;
import static java.util.Collections.emptyMap;
import static java.util.List.of;
import static org.apache.commons.lang3.StringUtils.EMPTY;
import static org.bouncycastle.asn1.x500.style.BCStyle.CN;
import static org.bouncycastle.asn1.x500.style.BCStyle.O;
import static org.bouncycastle.asn1.x500.style.BCStyle.T;
import static org.springframework.http.HttpStatus.*;


@Log4j2
public class CryptoUtils {

    @SuppressWarnings("SpellCheckingInspection")
    public static String EXAMPLE_PUBLIC_CERTIFICATE_BASE64 = EMPTY
            + "MIIG+jCCBOKgAwIBAgIUStb/+TKiIUj2TDZgq+uVGtUbZMkwDQYJKoZIhvcNAQELBQAwga8xCzAJBgNVBAYTAkZSMRAwDgYDVQQIDAdIZXJhdWx0MR0wGwYDVQQKDBRBc3NvY2lhdGl"
            + "vbiBBRFVMTEFDVDEkMCIGA1UECwwbQUNfQURVTExBQ1RfVXRpbGlzYXRldXJzX0czMSQwIgYDVQQDDBtBQ19BRFVMTEFDVF9VdGlsaXNhdGV1cnNfRzMxIzAhBgkqhkiG9w0BCQEWFH"
            + "N5c3RlbWVAYWR1bGxhY3Qub3JnMB4XDTIxMDcxNjEwMDQ1N1oXDTIzMDcxNjEwMDQ1N1owgZUxCzAJBgNVBAYTAkZSMRAwDgYDVQQIDAdIZXJhdWx0MRQwEgYDVQQHDAtNb250cGVsb"
            + "GllcjESMBAGA1UECgwJTGlicmljaWVsMRMwEQYDVQQLDApQcm9kdWN0aW9uMRIwEAYDVQQDDAlUZXN0X1RpbG8xITAfBgkqhkiG9w0BCQEWEnRpbG8udGVzdEBkb20udGVzdDCCASIw"
            + "DQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBANWdP3N5URDGUCf4qdx1krdDVHfs/KHN/7mGa/1C+pT1x5i8qX0ZF/DYI/zcSwMFhqJkr/G0sqLELOPgVjXgXKE/9YcsppwGcq4PwVI"
            + "Lr8LfiblmufREBJQaNKbUS+2XbMfFt+gYSa3twrKigkq1n4wo7I0S43nhioZPwBBfxdJUTWr4/Uc6VaOasWDrKqkznOtHUE5sD2edHqrapOfrIOSa1ccqdhgybPZQwCanA+Lk848Re/"
            + "KA5gxcx+X/3/8Qqww2wew27JdpIib2MJ3Ixq8ZlqYDaAmTpDhEiiWklJnEgNUlAZOQKz2Qmhs8QqAO+46R2JVf8rGSzkgCg6UK4ucCAwEAAaOCAiQwggIgMAkGA1UdEwQCMAAwQgYJY"
            + "IZIAYb4QgENBDUWM0NlcnRpZmljYXQgZ2VuZXJlIHBhciBsJ0FDX0FEVUxMQUNUX1VUSUxJU0FURVVSU19HMzAdBgNVHQ4EFgQU2sGKHc7e9oKl+i0yPq/88XEeW3cwgfUGA1UdIwSB"
            + "7TCB6oAUmbt/AgKYRZQErGAkE9Mq+9qjX0ChgbukgbgwgbUxCzAJBgNVBAYTAkZSMRAwDgYDVQQIDAdIZXJhdWx0MRQwEgYDVQQHDAtNb250cGVsbGllcjEdMBsGA1UECgwUQXNzb2N"
            + "pYXRpb24gQURVTExBQ1QxHDAaBgNVBAsME0FDX0FEVUxMQUNUX1JPT1RfRzMxHDAaBgNVBAMME0FDX0FEVUxMQUNUX1JPT1RfRzMxIzAhBgkqhkiG9w0BCQEWFHN5c3RlbWVAYWR1bG"
            + "xhY3Qub3JnghRvKXgHN8D4NbAw8kcWCR1fEjtQajAdBgNVHREEFjAUgRJ0aWxvLnRlc3RAZG9tLnRlc3QwHwYDVR0SBBgwFoEUc3lzdGVtZUBhZHVsbGFjdC5vcmcwOgYJYIZIAYb4Q"
            + "gEEBC0WK2h0dHA6Ly9jcmwuYWR1bGxhY3Qub3JnL0NBX1VTRVJTX0NSTF9nMy5wZW0wPAYDVR0fBDUwMzAxoC+gLYYraHR0cDovL2NybC5hZHVsbGFjdC5vcmcvQUNfVVNFUlNfQ1JM"
            + "X2czLnBlbTANBgkqhkiG9w0BAQsFAAOCAgEAcLKmNmkqXRMzTrMqLPB8BpGMWM2kNPA8OjIQyLhFD+WUXYJp+/4VfHPzJrv/cfeAC/9WPy9G1UV70PMIT67s6wsLdHCegxLOd5NUwJ6"
            + "89cBTtcCLvU6GUNubq+MDRHBTPzxprMPa1LzA4giPLIZZmVFslBYWtUKDgipn938+nS7X5buvVS1Razs2xwgsgRMuffGTbrtmp1XrKEnjhHOWzjTdSp43alK1s2i/7YUNkIUawZbPWK"
            + "QAYKxqlm5siEick5T5LhgIx+2BnrlWfrs0GPxTkplbolLFtMZucxAkazyaq29gLR6JtvJZCVIxWj2vXkWo9up7rW1d0w9pvuLO0zxmbYQZUmXmLn4lFZTYzF3esIxhRvKRKjmkpyOT/"
            + "7gXi6lrs/qVWg5MzGHOdg4uhKtnsYh6aD3PVjcxxPnltR4r27RQbj6Hu944BarXEH7veKFuvpjCUnUZuCqqptUA/1p99OYuhXuFwR7/jxHRzedlIPDJTmsDYv14bL4OApi0nlooldCZ"
            + "bprmw8+T0Z/n5qdu9neqeDvyp7WhRX+uQuIjyW5IP6RqHusuCOe3xJLD1XSDrZWhiF/GqvhEfogil3gx3U1RLfNeShiNdc3ZRnWHj+eC0UbOB8OVCkMMm56VrKjwY9vZWwl3k89AqZc"
            + "N0PXUEpXOExpS3XMW01wn1+U=";

    public static final String SIGNATURE_PLACEHOLDER_USER_NAME = INTERNAL_PREFIX + "user_name";
    public static final String SIGNATURE_PLACEHOLDER_DESK_NAME = INTERNAL_PREFIX + "desk_name";
    public static final String SIGNATURE_PLACEHOLDER_COUNTRY_NAME = INTERNAL_PREFIX + "country";
    public static final String SIGNATURE_PLACEHOLDER_CITY_NAME = INTERNAL_PREFIX + "city";
    public static final String SIGNATURE_PLACEHOLDER_ZIPCODE_NAME = INTERNAL_PREFIX + "zipcode";
    public static final String SIGNATURE_PLACEHOLDER_SIGNATURE_DATE = INTERNAL_PREFIX + "signature_date";
    public static final String SIGNATURE_PLACEHOLDER_SIGNATURE_TIME = INTERNAL_PREFIX + "signature_time";
    public static final String SIGNATURE_PLACEHOLDER_DELEGATION_FROM = INTERNAL_PREFIX + "delegation_from";
    public static final String SIGNATURE_PLACEHOLDER_CUSTOM_SIGNATURE_FIELD = INTERNAL_PREFIX + "custom_signature_field";
    public static final String SIGNATURE_PLACEHOLDER_SIGNATURE_IMAGE_BASE64 = INTERNAL_PREFIX + "signature_image_base64";

    private static final String CERTIFICATE_TYPE_X509 = "X.509";


    public CryptoUtils() {
        // This seems useless,
        // but this class is actually instantiated and fed to the FreeTemplate models
    }


    public static @NotNull PrivateKey getPrivateKey(@NotNull SealCertificate sealCertificate) {
        try {
            byte[] privateKeyBytes = Base64.getDecoder().decode(sealCertificate.getPrivateKeyBase64());
            KeyFactory kf = KeyFactory.getInstance("RSA");
            return kf.generatePrivate(new PKCS8EncodedKeySpec(privateKeyBytes));
        } catch (GeneralSecurityException e) {
            throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, e, "message.could_not_read_certificate_content");
        }
    }


    public static @Nullable String sha256Sign(byte[] bytesToSign, @NotNull PrivateKey privateKey) {

        try {
            Signature privateSignature = Signature.getInstance("SHA256withRSA");
            privateSignature.initSign(privateKey);
            privateSignature.update(bytesToSign);

            byte[] signature = privateSignature.sign();
            return Base64.getEncoder().encodeToString(signature);

        } catch (GeneralSecurityException e) {
            log.error("Signature failed", e);
            return null;
        }
    }


    private static @Nullable PdfSignaturePosition getHashtagPosition(@NotNull Folder folder, @NotNull Document document, @NotNull Action action) {
        List<Action> signatureTagsActions = of(SIGNATURE, EXTERNAL_SIGNATURE);

        final List<Action> countedActions = signatureTagsActions.contains(action)
                                            ? signatureTagsActions
                                            : of(action);

        long index = folder.getStepList().stream()
                .filter(s -> countedActions.contains(s.getAction()))
                .filter(s -> s.getState() == VALIDATED || s.getState() == BYPASSED)
                .count() + 1;

        log.debug("actionIndex : {}", action);

        Map<Integer, PdfSignaturePosition> hashtagMap = switch (action) {
            case SIGNATURE, EXTERNAL_SIGNATURE -> document.getSignatureTags();
            case SEAL -> document.getSealTags();
            default -> emptyMap();
        };

        PdfSignaturePosition result = Optional.ofNullable(hashtagMap.get((int) index))
                .orElseGet(() -> hashtagMap.get(0));

        Optional.ofNullable(result).ifPresent(r -> r.setOrigin(CENTER));

        log.debug("getHashtagPosition action:{} index:{} result:{}", action, index, result);
        return result;
    }


    public static PdfSignaturePosition getPosition(@NotNull Folder folder, @NotNull Document document, @NotNull Action action) {

        // Annotations
        // TODO : have a seal-specific-one, they should not be shared
        Optional<PdfSignaturePosition> documentAnnotationPosition = Optional.of(document)
                .map(Document::getSignaturePlacementAnnotations)
                .orElse(emptyList())
                .stream()
                .min(Comparator.comparingInt(SignaturePlacement::getSignatureNumber))
                .map(p -> new PdfSignaturePosition(p.getX(), p.getY(), p.getPage(), BOTTOM_LEFT));

        // HashTag
        Optional<PdfSignaturePosition> documentHashtagPosition = Optional.ofNullable(getHashtagPosition(folder, document, action));

        // Type
        // TODO : have a seal-specific-one, they should not be shared
        Optional<PdfSignaturePosition> typeDefinedPosition = Optional
                .ofNullable(folder.getType())
                .map(Type::getSignaturePosition)
                .map(p -> new PdfSignaturePosition(p.getX(), p.getY(), p.getPage(), BOTTOM_LEFT));

        log.debug("PdfSignaturePosition...");
        log.debug("    documentAnnotationPosition : {}", documentAnnotationPosition);
        log.debug("       documentHashtagPosition : {}", documentHashtagPosition);
        log.debug("           typeDefinedPosition : {}", typeDefinedPosition);

        return documentAnnotationPosition
                .or(() -> documentHashtagPosition)
                .or(() -> typeDefinedPosition)
                .orElse(new PdfSignaturePosition(50, 50, 0, BOTTOM_LEFT));
    }


    public static void checkTypeReplacementCompatibility(@NotNull Type oldType, @NotNull Type newType) {

        if ((oldType.getSignatureFormat() == AUTO) || (newType.getSignatureFormat()) == AUTO) {
            throw new RuntimeException("The signature format should have been evaluated before the checkTypeReplacementCompatibility call");
        }

        boolean isDifferentFormat = oldType.getSignatureFormat() != newType.getSignatureFormat();
        boolean isNewFormatEnveloped = newType.getSignatureFormat().isEnvelopedSignature();
        if (isDifferentFormat && isNewFormatEnveloped) {
            throw new LocalizedStatusException(NOT_ACCEPTABLE, "message.folder_incompatible_with_an_enveloped_signature_typology");
        }
    }


    public static void checkSubtypeReplacementCompatibility(@NotNull Subtype oldSubtype, @NotNull Subtype newSubType) {

        int updatedMaxDocuments = newSubType.getMaxMainDocuments();
        int existingMaxDocuments = oldSubtype.getMaxMainDocuments();
        if (updatedMaxDocuments < existingMaxDocuments) {
            throw new LocalizedStatusException(NOT_ACCEPTABLE, "message.folder_incompatible_with_a_single_document_typology");
        }
    }


    public static void checkIfDetachedSignatureAllowed(@NotNull SignatureFormat signatureFormat, boolean hasSomeDetachedSignature) {
        if (signatureFormat.isEnvelopedSignature() && hasSomeDetachedSignature) {
            throw new LocalizedStatusException(NOT_ACCEPTABLE, "message.detached_signature_are_incompatible_with_enveloped_signature_types");
        }
    }


    public static void checkSignatureFormat(@NotNull SignatureFormat signatureFormat,
                                            @NotNull List<Document> documentList,
                                            @Nullable List<MultipartFile> detachedSignatures) {
        boolean hasSomeDetachedSignature = documentList.stream()
                .filter(Document::isMainDocument)
                .map(Document::getDetachedSignatures)
                .mapToLong(CollectionUtils::size)
                .sum() > 1
                || CollectionUtils.isNotEmpty(detachedSignatures);

        checkIfDetachedSignatureAllowed(signatureFormat, hasSomeDetachedSignature);

        // Integrity checks

        boolean isEnvelopedXadesSignature = (signatureFormat == PES_V2);
        boolean isSingleFile = documentList.size() <= 1;
        boolean areAllXmlFile = documentList.stream()
                .filter(Document::isMainDocument)
                .map(Document::getMediaType)
                .allMatch(FileUtils::isXml);
        if (isEnvelopedXadesSignature && !(isSingleFile && areAllXmlFile)) {
            throw new LocalizedStatusException(NOT_ACCEPTABLE, "message.enveloped_xades_signature_should_have_a_single_xml_document");
        }
    }


    public static @Nullable JcaX509CertificateHolder parseX509Certificate(@NotNull String publicCertificateBase64) {

        byte[] publicCertificateByteArray = Base64.getDecoder().decode(publicCertificateBase64);
        try (ByteArrayInputStream pubKeyInputStream = new ByteArrayInputStream(publicCertificateByteArray)) {

            CertificateFactory certFactory = CertificateFactory.getInstance(CERTIFICATE_TYPE_X509, new BouncyCastleProvider());
            X509Certificate x509Certificate = (X509Certificate) certFactory.generateCertificate(pubKeyInputStream);
            return new JcaX509CertificateHolder(x509Certificate);

        } catch (IllegalArgumentException | CertificateException | IOException e) {
            log.error("Cannot read public certificate pem", e);
            log.trace("Unreadable PEM:%s".formatted(publicCertificateBase64));
            return null;
        }
    }


    public static @NotNull String getAsn1StringValue(@NotNull X500Name x500name, ASN1ObjectIdentifier asn1ObjectIdentifier) {
        return Optional.of(x500name.getRDNs(asn1ObjectIdentifier))
                .map(List::of)
                .map(Collection::stream)
                .flatMap(Stream::findFirst)
                .map(RDN::getFirst)
                .map(AttributeTypeAndValue::getValue)
                .map(IETFUtils::valueToString)
                .orElse(EMPTY);
    }


    public static @NotNull String getOrganization(@NotNull X500Name x500name) {
        return getAsn1StringValue(x500name, O);
    }


    public static @NotNull String getName(@NotNull X500Name x500name) {
        return getAsn1StringValue(x500name, CN);
    }


    public static @NotNull String getTitle(@NotNull X500Name x500name) {
        return getAsn1StringValue(x500name, T);
    }


}
