/*
 * iparapheur Core
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.ipcore.utils;

import coop.libriciel.ipcore.model.auth.User;
import org.jetbrains.annotations.NotNull;

import java.util.Locale;
import java.util.ResourceBundle;

import static coop.libriciel.ipcore.utils.TextUtils.INTERNAL_PREFIX;
import static coop.libriciel.ipcore.utils.TextUtils.MESSAGE_BUNDLE;


public class UserUtils {


    public static String AUTOMATIC_TASK_USER_ID = INTERNAL_PREFIX + "automatic_task";
    public static String AUTOMATIC_TASK_USER_NAME = "iparapheur";


    public static @NotNull User getAutomaticUser() {

        ResourceBundle resources = ResourceBundle.getBundle(MESSAGE_BUNDLE, Locale.getDefault());

        User automaticUser = new User();
        automaticUser.setId(AUTOMATIC_TASK_USER_ID);
        automaticUser.setUserName(AUTOMATIC_TASK_USER_NAME);
        automaticUser.setFirstName(resources.getString("message.automatic_user_firstname"));
        automaticUser.setLastName(resources.getString("message.automatic_user_lastname"));

        return automaticUser;
    }


}
