/*
 * iparapheur Core
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.ipcore.utils;

import lombok.Data;


public class ApiUtils {


    public static final int MAX_DESKS_COUNT = 250000;
    public static final int MAX_DESKS_PER_ACCOUNT_COUNT = 256;
    public static final int MAX_USER_PER_DESKS_COUNT = 256;


    /**
     * This is only used on error declaration.
     * TODO: Find the SpringBoot inner error message model instead.
     */
    @Data
    public static class ErrorResponse {

        private String timestamp;
        private int status;
        private String error;
        private String message;
        private String path;

    }


    public static final String CODE_200 = "200";
    public static final String CODE_201 = "201";
    public static final String CODE_204 = "204";
    public static final String CODE_400 = "400";
    public static final String CODE_401 = "401";
    public static final String CODE_403 = "403";
    public static final String CODE_404 = "404";
    public static final String CODE_406 = "406";
    public static final String CODE_407 = "407";
    public static final String CODE_409 = "409";
    public static final String CODE_507 = "507";

    /**
     * Validated annotations support translated templates wrapped in curly braces : @Min{value=5, message="{message.xxx}"}
     * Those messages will be properly translated and returned by the SpringBoot engine. Nothing more to do.
     * <p>
     * Yet, wrapping those directly will mark translations as unused.
     * Splitting the braces wrapper and the real "message.xxx", computing those on the fly, will solve everything.
     */
    public static final String JAVAX_MESSAGE_PREFIX = "{";
    public static final String JAVAX_MESSAGE_SUFFIX = "}";

    public static final int NAME_MIN_LENGTH = 2;
    public static final int NAME_MAX_LENGTH = 255;
    public static final String NAME_SIZE_ERROR_MESSAGE = "message.name_field_should_be_between_2_and_255_chars";
    public static final String NAME_SIZE_JAVAX_VALIDATION_ERROR_MESSAGE = JAVAX_MESSAGE_PREFIX + NAME_SIZE_ERROR_MESSAGE + JAVAX_MESSAGE_SUFFIX;

    public static final String NAME_CONTAINS_FORBIDDEN_CHARACTER_ERROR_MESSAGE = "message.name_contains_invalid_char";
    public static final String NAME_CONTAINS_FORBIDDEN_CHARACTER_JAVAX_VALIDATION_ERROR_MESSAGE = JAVAX_MESSAGE_PREFIX
            + NAME_CONTAINS_FORBIDDEN_CHARACTER_ERROR_MESSAGE
            + JAVAX_MESSAGE_SUFFIX;

    public static final int KEY_MIN_LENGTH = 1;
    public static final int KEY_MAX_LENGTH = 128;
    public static final String KEY_SIZE_ERROR_MESSAGE = "message.key_field_should_be_between_1_and_128_chars";
    public static final String KEY_SIZE_JAVAX_VALIDATION_ERROR_MESSAGE = JAVAX_MESSAGE_PREFIX + KEY_SIZE_ERROR_MESSAGE + JAVAX_MESSAGE_SUFFIX;

    public static final int DESCRIPTION_MIN_LENGTH = 3;
    public static final int DESCRIPTION_MAX_LENGTH = 255;
    public static final String DESCRIPTION_SIZE_ERROR_MESSAGE = "message.description_field_should_be_between_3_and_255_chars";
    public static final String DESCRIPTION_SIZE_JAVAX_VALIDATION_ERROR_MESSAGE = JAVAX_MESSAGE_PREFIX + DESCRIPTION_SIZE_ERROR_MESSAGE + JAVAX_MESSAGE_SUFFIX;

    public static final int ANNOTATION_MAX_LENGTH = 255;
    public static final String ANNOTATION_SIZE_ERROR_MESSAGE = "message.annotation_should_be_under_255_chars";
    public static final String ANNOTATION_SIZE_JAVAX_VALIDATION_ERROR_MESSAGE = JAVAX_MESSAGE_PREFIX + ANNOTATION_SIZE_ERROR_MESSAGE + JAVAX_MESSAGE_SUFFIX;

    public static final String OWNER_LIST_NOT_NULL_ERROR_MESSAGE = "message.owner_list_should_not_be_null";
    public static final String OWNER_LIST_NOT_NULL_JAVAX_VALIDATION_ERROR_MESSAGE =
            JAVAX_MESSAGE_PREFIX + OWNER_LIST_NOT_NULL_ERROR_MESSAGE + JAVAX_MESSAGE_SUFFIX;

    public static final String OWNER_LIST_MAX_SIZE_ERROR_MESSAGE = "message.owner_list_size_should_be_under_256";
    public static final String OWNER_LIST_MAX_SIZE_JAVAX_VALIDATION_ERROR_MESSAGE =
            JAVAX_MESSAGE_PREFIX + OWNER_LIST_MAX_SIZE_ERROR_MESSAGE + JAVAX_MESSAGE_SUFFIX;
}
