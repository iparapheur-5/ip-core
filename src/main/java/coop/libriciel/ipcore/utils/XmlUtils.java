/*
 * iparapheur Core
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package coop.libriciel.ipcore.utils;

import coop.libriciel.ipcore.model.auth.DeskRepresentation;
import coop.libriciel.ipcore.model.auth.User;
import coop.libriciel.ipcore.model.content.DetachedSignature;
import coop.libriciel.ipcore.model.content.Document;
import coop.libriciel.ipcore.model.crypto.SignatureFormat;
import coop.libriciel.ipcore.model.workflow.Folder;
import coop.libriciel.ipcore.model.workflow.Task;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.apache.xml.security.Init;
import org.apache.xml.security.c14n.Canonicalizer;
import org.apache.xml.security.exceptions.XMLSecurityException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.web.server.ResponseStatusException;

import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.*;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.function.Function;
import java.util.stream.Stream;

import static coop.libriciel.ipcore.model.crypto.SignatureFormat.PKCS7;
import static coop.libriciel.ipcore.model.crypto.SignatureFormat.XADES_DETACHED;
import static coop.libriciel.ipcore.model.workflow.Action.READ;
import static coop.libriciel.ipcore.model.workflow.Action.UNDO;
import static coop.libriciel.ipcore.utils.CollectionUtils.distinctByProperty;
import static coop.libriciel.ipcore.utils.TextUtils.*;
import static java.nio.charset.StandardCharsets.UTF_8;
import static javax.xml.XMLConstants.ACCESS_EXTERNAL_DTD;
import static javax.xml.XMLConstants.ACCESS_EXTERNAL_STYLESHEET;
import static javax.xml.transform.OutputKeys.*;
import static org.apache.commons.lang3.StringUtils.EMPTY;
import static org.apache.xml.security.c14n.Canonicalizer.ALGO_ID_C14N_WITH_COMMENTS;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;


@Log4j2
public class XmlUtils {


    public static final String OUTPUTKEYS_INDENT_AMOUNT = "{http://xml.apache.org/xslt}indent-amount";
    private static final String PREMIS = "http://www.loc.gov/premis/v3";
    private static final String XSI = "http://www.w3.org/2001/XMLSchema-instance";


    static {
        Init.init();
    }


    private XmlUtils() {
        throw new IllegalStateException("Utility class");
    }


    public static @NotNull String prettyPrint(@NotNull String xml) throws TransformerException {

        TransformerFactory factory = TransformerFactory.newInstance();
        factory.setAttribute(ACCESS_EXTERNAL_DTD, EMPTY);
        factory.setAttribute(ACCESS_EXTERNAL_STYLESHEET, EMPTY);

        Transformer transformer = factory.newTransformer();
        transformer.setOutputProperty(METHOD, "xml");
        transformer.setOutputProperty(ENCODING, UTF_8.name());
        transformer.setOutputProperty(INDENT, "yes");
        transformer.setOutputProperty(DOCTYPE_PUBLIC, "yes");
        transformer.setOutputProperty(OUTPUTKEYS_INDENT_AMOUNT, "2");

        StreamResult result = new StreamResult(new StringWriter());
        transformer.transform(new StreamSource(new ByteArrayInputStream(xml.getBytes(UTF_8))), result);

        return result.getWriter().toString();
    }


    public static @NotNull String canonicalize(byte[] xmlBytes) throws TransformerException {

        String result;
        try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
            Canonicalizer canonicalizer = Canonicalizer.getInstance(ALGO_ID_C14N_WITH_COMMENTS);
            canonicalizer.canonicalize(xmlBytes, baos, false);
            result = baos.toString(UTF_8);
        } catch (XMLSecurityException | IOException e) {
            throw new TransformerException(e.getLocalizedMessage());
        }

        return result;
    }


    @SuppressWarnings(SONAR_CODE_BLOCKS)
    public static @NotNull ByteArrayOutputStream folderToPremisXml(@NotNull Folder folder,
                                                                   @Nullable SignatureFormat signatureFormat,
                                                                   @NotNull Function<String, InputStream> documentSupplier) throws XMLStreamException {

        ResourceBundle messages = ResourceBundle.getBundle(MESSAGE_BUNDLE, Locale.getDefault());
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        XMLStreamWriter writer = XMLOutputFactory.newInstance().createXMLStreamWriter(baos);

        writer.writeStartDocument(UTF_8.name(), "1.0");

        writer.writeStartElement("premis");
        writer.writeDefaultNamespace(PREMIS);
        writer.writeNamespace("xsi", XSI);
        writer.writeAttribute("version", "3.0");
        writer.writeAttribute(XSI, "schemaLocation", "http://www.loc.gov/premis/v3 http://www.loc.gov/standards/premis/v3/premis.xsd");

        // Folder metadata
        writer.writeComment(" Folder ");

        {
            writer.writeStartElement("object");
            writer.writeAttribute(XSI, "type", "intellectualEntity");

            writer.writeStartElement("objectIdentifier");
            {
                writer.writeStartElement("objectIdentifierType");
                writer.writeCharacters("folderId");
                writer.writeEndElement();

                writer.writeStartElement("objectIdentifierValue");
                writer.writeCharacters(folder.getId());
                writer.writeEndElement();
            }
            writer.writeEndElement();

            if (folder.getType() != null) {

                writer.writeStartElement("significantProperties");
                {
                    writer.writeStartElement("significantPropertiesType");
                    writer.writeCharacters("type");
                    writer.writeEndElement();

                    writer.writeStartElement("significantPropertiesExtension");
                    {
                        writer.writeStartElement("significantPropertiesType");
                        writer.writeCharacters("typeId");
                        writer.writeEndElement();

                        writer.writeStartElement("significantPropertiesValue");
                        writer.writeCharacters(folder.getType().getId());
                        writer.writeEndElement();
                    }
                    writer.writeEndElement();

                    writer.writeStartElement("significantPropertiesExtension");
                    {
                        writer.writeStartElement("significantPropertiesType");
                        writer.writeCharacters("typeName");
                        writer.writeEndElement();

                        writer.writeStartElement("significantPropertiesValue");
                        writer.writeCharacters(folder.getType().getName());
                        writer.writeEndElement();
                    }
                    writer.writeEndElement();
                }
                writer.writeEndElement();
            }

            if (folder.getSubtype() != null) {

                writer.writeStartElement("significantProperties");
                {
                    writer.writeStartElement("significantPropertiesType");
                    writer.writeCharacters("subtype");
                    writer.writeEndElement();

                    writer.writeStartElement("significantPropertiesExtension");
                    {
                        writer.writeStartElement("significantPropertiesType");
                        writer.writeCharacters("subtypeId");
                        writer.writeEndElement();

                        writer.writeStartElement("significantPropertiesValue");
                        writer.writeCharacters(folder.getSubtype().getId());
                        writer.writeEndElement();
                    }
                    writer.writeEndElement();

                    writer.writeStartElement("significantPropertiesExtension");
                    {
                        writer.writeStartElement("significantPropertiesType");
                        writer.writeCharacters("subtypeName");
                        writer.writeEndElement();

                        writer.writeStartElement("significantPropertiesValue");
                        writer.writeCharacters(folder.getSubtype().getName());
                        writer.writeEndElement();
                    }
                    writer.writeEndElement();
                }
                writer.writeEndElement();
            }

            writer.writeStartElement("originalName");
            writer.writeCharacters(folder.getName());
            writer.writeEndElement();

            writer.writeEndElement();
        }

        // Files
        writer.writeComment(" Files ");

        for (Document document : folder.getDocumentList()) {
            writer.writeStartElement("object");
            writer.writeAttribute(XSI, "type", "file");

            writer.writeStartElement("objectIdentifier");
            {
                writer.writeStartElement("objectIdentifierType");
                writer.writeCharacters("documentId");
                writer.writeEndElement();

                writer.writeStartElement("objectIdentifierValue");
                writer.writeCharacters(document.getId());
                writer.writeEndElement();
            }
            writer.writeEndElement();

            writer.writeStartElement("preservationLevel");
            {
                writer.writeStartElement("preservationLevelValue");
                writer.writeCharacters(document.isMainDocument() ? "mainDocument" : "annex");
                writer.writeEndElement();
            }
            writer.writeEndElement();

            writer.writeStartElement("objectCharacteristics");
            {
                writer.writeStartElement("fixity");
                {
                    writer.writeStartElement("messageDigestAlgorithm");
                    writer.writeCharacters(
                            Optional.ofNullable(document.getChecksumAlgorithm())
                                    .orElseThrow(() -> new LocalizedStatusException(INTERNAL_SERVER_ERROR, "Missing digest algorithm"))
                    );
                    writer.writeEndElement();

                    writer.writeStartElement("messageDigest");
                    writer.writeCharacters(
                            Optional.ofNullable(document.getChecksumValue())
                                    .orElseThrow(() -> new LocalizedStatusException(INTERNAL_SERVER_ERROR, "Missing digest value"))
                    );
                    writer.writeEndElement();
                }
                writer.writeEndElement();

                writer.writeStartElement("size");
                writer.writeCharacters(String.valueOf(document.getContentLength()));
                writer.writeEndElement();

                writer.writeStartElement("format");
                writer.writeStartElement("formatDesignation");
                {
                    writer.writeStartElement("formatName");
                    writer.writeCharacters(String.valueOf(document.getMediaType()));
                    writer.writeEndElement();

                    writer.writeStartElement("formatVersion");
                    writer.writeCharacters(String.valueOf(document.getMediaVersion()));
                    writer.writeEndElement();
                }
                writer.writeEndElement();
                writer.writeEndElement();
            }
            writer.writeEndElement();

            writer.writeStartElement("originalName");
            writer.writeCharacters(document.getName());
            writer.writeEndElement();

            if (signatureFormat != null) {

                if (document.getDetachedSignatures().size() > 0) {
                    Stream.of(XADES_DETACHED, PKCS7)
                            .filter(s -> s == folder.getType().getSignatureFormat())
                            .findAny()
                            .orElseThrow(() -> new ResponseStatusException(INTERNAL_SERVER_ERROR, "Signature format not yet implemented : " + signatureFormat));
                }

                for (DetachedSignature detachedSignature : document.getDetachedSignatures()) {

                    writer.writeStartElement("signatureInformation");
                    writer.writeStartElement("signature");
                    {
                        writer.writeStartElement("signatureEncoding");
                        writer.writeCharacters(UTF_8.name());
                        writer.writeEndElement();

                        writer.writeStartElement("signatureMethod");
                        writer.writeCharacters(signatureFormat == XADES_DETACHED ? "XAdES_BASELINE_B" : "CAdES_BASELINE_B");
                        writer.writeEndElement();

                        writer.writeStartElement("signatureValue");
                        writer.writeCData(
                                Optional.ofNullable(documentSupplier.apply(detachedSignature.getId()))
                                        .map(istream -> {
                                            try (InputStream is = istream) {return new String(is.readAllBytes(), UTF_8);} catch (IOException e) {return null;}
                                        })
                                        .orElseThrow(() -> new ResponseStatusException(INTERNAL_SERVER_ERROR, "Missing detached signature"))
                        );
                        writer.writeEndElement();

                        writer.writeStartElement("signatureValidationRules");
                        writer.writeCharacters(signatureFormat == XADES_DETACHED ? "XAdES-1.3.2" : "CAdES");
                        writer.writeEndElement();
                    }
                    writer.writeEndElement();
                    writer.writeEndElement();
                }
            }

            writer.writeEndElement();
        }

        // Events
        writer.writeComment(" Events ");

        List<Task> cleanedTasks = folder.getStepList()
                .stream()
                .filter(task -> !(task.getAction().equalsOneOf(READ, UNDO) && task.getDate() == null))
                // If a reject happened at some point, the upcoming/unvalidated tasks are not relevant
                .filter(task -> StringUtils.isNotEmpty(task.getId()))
                .filter(task -> task.getDate() != null)
                .toList();

        for (Task task : cleanedTasks) {
            writer.writeStartElement("event");

            writer.writeStartElement("eventIdentifier");
            {
                writer.writeStartElement("eventIdentifierType");
                writer.writeCharacters("taskId");
                writer.writeEndElement();

                writer.writeStartElement("eventIdentifierValue");
                writer.writeCharacters(task.getId());
                writer.writeEndElement();
            }
            writer.writeEndElement();

            writer.writeStartElement("eventType");
            writer.writeCharacters(task.getAction().toString());
            writer.writeEndElement();

            writer.writeStartElement("eventDateTime");
            writer.writeCharacters(TextUtils.serializeDate(task.getDate(), ISO8601_DATE_TIME_FORMAT));
            writer.writeEndElement();

            // TODO : Find a way to get the original task's desk, and print the delegation
            // if (!task.getCandidateDesk().contains(task.getPublicAnnotation())) {
            //     w.writeStartElement("eventDetail");
            //     w.writeStartElement("eventDetailInformation");
            //     w.writeCharacters(String.format("Par suppléance de : %s", task.getDesk().getName());
            //     w.writeEndElement();
            //     w.writeEndElement();
            // }

            if (StringUtils.isNotEmpty(task.getPublicAnnotation())) {
                writer.writeStartElement("eventOutcomeInformation");
                writer.writeStartElement("eventOutcomeDetail");
                writer.writeStartElement("eventOutcomeDetailNote");
                writer.writeCharacters(task.getPublicAnnotation());
                writer.writeEndElement();
                writer.writeEndElement();
                writer.writeEndElement();
            }

            writer.writeStartElement("linkingAgentIdentifier");
            {
                writer.writeStartElement("linkingAgentIdentifierType");
                writer.writeCharacters("userId");
                writer.writeEndElement();

                writer.writeStartElement("linkingAgentIdentifierValue");
                writer.writeCharacters(task.getUser().getId());
                writer.writeEndElement();

                writer.writeStartElement("linkingAgentRole");
                writer.writeCharacters(
                        task.getDesks().stream()
                                .findFirst()
                                .map(DeskRepresentation::getName)
                                .orElse(messages.getString("message.deleted_desk"))
                );
                writer.writeEndElement();
            }
            writer.writeEndElement();

            writer.writeEndElement();
        }

        // Users
        writer.writeComment(" Users ");

        List<User> userSet = cleanedTasks
                .stream()
                .map(Task::getUser)
                .filter(distinctByProperty(User::getId))
                .toList();

        for (User user : userSet) {
            writer.writeStartElement("agent");

            writer.writeStartElement("agentIdentifier");
            {
                writer.writeStartElement("agentIdentifierType");
                writer.writeCharacters("userId");
                writer.writeEndElement();

                writer.writeStartElement("agentIdentifierValue");
                writer.writeCharacters(user.getId());
                writer.writeEndElement();
            }
            writer.writeEndElement();

            writer.writeStartElement("agentName");
            writer.writeCharacters(Optional.of(user)
                    .filter(u -> StringUtils.isNotEmpty(u.getFirstName()))
                    .filter(u -> StringUtils.isNotEmpty(u.getLastName()))
                    .map(u -> String.format("%s %s", user.getFirstName(), user.getLastName()))
                    .or(() -> Optional.ofNullable(user.getUserName()))
                    .orElse(user.getId())
            );
            writer.writeEndElement();

            writer.writeEndElement();
        }

        //

        writer.writeEndElement();

        writer.writeEndDocument();
        writer.close();

        return baos;
    }


}
