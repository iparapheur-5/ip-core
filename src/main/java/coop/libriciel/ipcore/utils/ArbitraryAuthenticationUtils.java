/*
 * iparapheur Core
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.ipcore.utils;

import org.keycloak.KeycloakPrincipal;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.Collection;


public class ArbitraryAuthenticationUtils {

    private ArbitraryAuthenticationUtils() {
    }


    public static void clearAuthentication() {
        SecurityContextHolder.getContext().setAuthentication(null);
    }


    public static void configureAuthentication(String username, String role) {
        Collection<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList(role);
        KeycloakPrincipal principal = new KeycloakPrincipal(username, null);
        Authentication authentication = new UsernamePasswordAuthenticationToken(
                principal,
                role,
                authorities
        );
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }

}
