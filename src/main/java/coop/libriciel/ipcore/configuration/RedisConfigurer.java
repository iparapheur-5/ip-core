/*
 * iparapheur Core
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.ipcore.configuration;

import coop.libriciel.ipcore.controller.WorkflowController;
import coop.libriciel.ipcore.services.auth.AuthServiceInterface;
import coop.libriciel.ipcore.services.database.SubtypeRepository;
import coop.libriciel.ipcore.services.externalsignature.ExternalSignatureInterface;
import coop.libriciel.ipcore.services.ipng.IpngServiceInterface;
import coop.libriciel.ipcore.services.mail.NotificationServiceInterface;
import coop.libriciel.ipcore.services.mail.PostponedNotificationCacheRepository;
import coop.libriciel.ipcore.services.redis.RedisMessageListenerService;
import coop.libriciel.ipcore.services.securemail.SecureMailServiceInterface;
import coop.libriciel.ipcore.services.workflow.WorkflowServiceInterface;
import io.lettuce.core.RedisClient;
import io.lettuce.core.api.StatefulRedisConnection;
import io.lettuce.core.api.sync.RedisStreamCommands;
import lombok.extern.log4j.Log4j2;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.connection.RedisStandaloneConfiguration;
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.listener.ChannelTopic;
import org.springframework.data.redis.listener.RedisMessageListenerContainer;
import org.springframework.data.redis.listener.adapter.MessageListenerAdapter;
import org.springframework.data.redis.repository.configuration.EnableRedisRepositories;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import org.springframework.transaction.PlatformTransactionManager;

import static coop.libriciel.ipcore.utils.TextUtils.INTERNAL_PREFIX;


@Log4j2
@Configuration
@EnableRedisRepositories(basePackageClasses = {PostponedNotificationCacheRepository.class})
@ConditionalOnProperty(name = "spring.cache.type", havingValue = "REDIS")
public class RedisConfigurer {


    public static final String TASK_HOOK_QUEUE = INTERNAL_PREFIX + "task_hook_trigger";
    public static final String EXTERNAL_SIGNATURE_CONNECTOR_NOTIFICATION_QUEUE = INTERNAL_PREFIX + "external_signature_connector_notification_queue";
    public static final String PASTELL_CONNECTOR_NOTIFICATION_QUEUE = INTERNAL_PREFIX + "pastell_connector_notification_queue";


    private @Value("${spring.redis.host}") String hostName;


    // <editor-fold desc="Beans">


    private final PlatformTransactionManager platformTransactionManager;
    private final ExternalSignatureInterface externalSignatureService;
    private final SecureMailServiceInterface secureMailService;
    private final AuthServiceInterface authService;


    public RedisConfigurer(PlatformTransactionManager platformTransactionManager,
                           @NotNull ExternalSignatureInterface externalSignatureService,
                           @NotNull SecureMailServiceInterface secureMailService,
                           AuthServiceInterface authService) {
        this.platformTransactionManager = platformTransactionManager;
        this.externalSignatureService = externalSignatureService;
        this.secureMailService = secureMailService;
        this.authService = authService;
    }


    // </editor-fold desc="Beans">


    @Bean
    LettuceConnectionFactory redisConnectionFactory() {
        RedisStandaloneConfiguration redisStandaloneConfiguration = new RedisStandaloneConfiguration();

        redisStandaloneConfiguration.setHostName(this.hostName);

        return new LettuceConnectionFactory(redisStandaloneConfiguration);
    }


    @Bean
    public RedisTemplate<Object, Object> redisTemplate() {
        RedisTemplate<Object, Object> redisTemplate = new RedisTemplate<>();
        redisTemplate.setConnectionFactory(redisConnectionFactory());
        redisTemplate.setKeySerializer(new StringRedisSerializer());
        return redisTemplate;
    }


    @Bean
    RedisMessageListenerContainer redisContainer(RedisConnectionFactory redisConnectionFactory,
                                                 @Autowired NotificationServiceInterface notificationController,
                                                 @Autowired SubtypeRepository subtypeRepository,
                                                 @Autowired WorkflowServiceInterface workflowService,
                                                 @Autowired WorkflowController workflowController) {

        RedisMessageListenerContainer container = new RedisMessageListenerContainer();
        container.setConnectionFactory(redisConnectionFactory);

        container.addMessageListener(
                new MessageListenerAdapter(
                        new RedisMessageListenerService(
                                notificationController,
                                subtypeRepository,
                                workflowService,
                                workflowController,
                                platformTransactionManager,
                                authService)
                ),
                new ChannelTopic(TASK_HOOK_QUEUE)
        );

        container.addMessageListener(
                new MessageListenerAdapter(externalSignatureService),
                new ChannelTopic(EXTERNAL_SIGNATURE_CONNECTOR_NOTIFICATION_QUEUE));

        container.addMessageListener(
                new MessageListenerAdapter(secureMailService),
                new ChannelTopic(PASTELL_CONNECTOR_NOTIFICATION_QUEUE));

        return container;
    }


    @Bean
    @ConditionalOnProperty(name = IpngServiceInterface.PREFERENCES_PROVIDER_KEY, havingValue = "ipng")
    RedisStreamCommands<String, String> redisSyncCommand() {
        RedisClient client = RedisClient.create("redis://" + this.hostName);
        StatefulRedisConnection<String, String> connection = client.connect();
        return connection.sync();
    }

}
