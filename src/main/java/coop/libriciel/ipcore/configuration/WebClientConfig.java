/*
 * iparapheur Core
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.ipcore.configuration;

import coop.libriciel.ipcore.utils.ForcedExceptionsConnector;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.netty.resources.ConnectionProvider;

import java.time.Duration;


@Configuration
public class WebClientConfig {

    private static final Integer maxConnections = 256;


    @Bean
    public ConnectionProvider connectionPool() {
        /*
         * Quoting doc on default connection provider's maxConections :
         * "Default max connections. Fallback to 2 * available number of processors (but with a minimum value of 16)"
         * This is probably too light for our use cases so we should be able to raise it
         */
        return ConnectionProvider.builder("fixedPool")
                .maxConnections(maxConnections)
                .pendingAcquireTimeout(Duration.ofSeconds(20))
                .build();
    }


    @Bean
    public ForcedExceptionsConnector forcedExceptionsConnector() {
        return new ForcedExceptionsConnector(connectionPool());
    }


    @Bean
    public WebClient.Builder sharedWebClientBuilder() {
        return WebClient.builder().clientConnector(new ForcedExceptionsConnector(connectionPool()));
    }

}
