/*
 * iparapheur Core
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.ipcore.configuration;

import coop.libriciel.ipcore.services.auth.AuthServiceInterface;
import coop.libriciel.ipcore.services.database.*;
import coop.libriciel.ipcore.services.permission.PermissionServiceInterface;
import coop.libriciel.ipcore.services.resolvers.*;
import coop.libriciel.ipcore.services.secret.SecretServiceInterface;
import coop.libriciel.ipcore.services.workflow.WorkflowServiceInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.web.config.EnableSpringDataWebSupport;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.List;


@Configuration
@EnableSpringDataWebSupport
public class WebConfig implements WebMvcConfigurer {


    // <editor-fold desc="Beans">

    private final PermissionServiceInterface permissionService;
    private final AuthServiceInterface authService;
    private final TenantRepository tenantRepository;
    private final TypeRepository typeRepository;
    private final SubtypeRepository subtypeRepository;
    private final LayerRepository layerRepository;
    private final SecretServiceInterface secretService;
    private final MetadataRepository metadataRepository;
    private final WorkflowServiceInterface workflowService;
    private final ExternalSignatureConfigRepository externalSignatureConfigRepository;


    @Autowired
    public WebConfig(PermissionServiceInterface permissionService, AuthServiceInterface authService,
                     TenantRepository tenantRepository,
                     TypeRepository typeRepository,
                     SubtypeRepository subtypeRepository,
                     LayerRepository layerRepository,
                     SecretServiceInterface secretService,
                     MetadataRepository metadataRepository,
                     WorkflowServiceInterface workflowService,
                     ExternalSignatureConfigRepository externalSignatureConfigRepository) {
        this.permissionService = permissionService;
        this.authService = authService;
        this.tenantRepository = tenantRepository;
        this.typeRepository = typeRepository;
        this.subtypeRepository = subtypeRepository;
        this.layerRepository = layerRepository;
        this.secretService = secretService;
        this.metadataRepository = metadataRepository;
        this.workflowService = workflowService;
        this.externalSignatureConfigRepository = externalSignatureConfigRepository;
    }


    // </editor-fold desc="Beans">


    @Override
    public void addArgumentResolvers(List<HandlerMethodArgumentResolver> argumentResolvers) {
        argumentResolvers.add(new CurrentUserResolver(authService));
        argumentResolvers.add(new ExternalSignatureConfigResolver(externalSignatureConfigRepository));
        argumentResolvers.add(new DeskResolver(authService));
        argumentResolvers.add(new TaskResolver(workflowService));
        argumentResolvers.add(new FolderResolver(permissionService, workflowService));
        argumentResolvers.add(new TenantResolver(tenantRepository));
        argumentResolvers.add(new TypeResolver(typeRepository));
        argumentResolvers.add(new SubtypeResolver(subtypeRepository));
        argumentResolvers.add(new LayerResolver(layerRepository));
        argumentResolvers.add(new SealCertificateResolver(secretService));
        argumentResolvers.add(new MetadataResolver(metadataRepository));
        argumentResolvers.add(new UserResolver(authService));
        argumentResolvers.add(new WorkflowDefinitionResolver(workflowService));
    }


}
