/*
 * iparapheur Core
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.ipcore.configuration;

import coop.libriciel.ipcore.model.auth.requests.UserSortBy;
import coop.libriciel.ipcore.model.database.*;
import coop.libriciel.ipcore.model.pdfstamp.LayerSortBy;
import coop.libriciel.ipcore.model.permission.DelegationSortBy;
import coop.libriciel.ipcore.model.workflow.FolderSortBy;
import coop.libriciel.ipcore.model.workflow.WorkflowDefinitionSortBy;
import io.swagger.v3.core.converter.AnnotatedType;
import io.swagger.v3.core.converter.ModelConverters;
import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.ExternalDocumentation;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.Paths;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import io.swagger.v3.oas.models.media.Schema;
import io.swagger.v3.oas.models.security.*;
import lombok.extern.log4j.Log4j2;
import org.jooq.tools.StringUtils;
import org.springdoc.core.GroupedOpenApi;
import org.springdoc.core.customizers.OpenApiCustomiser;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import static coop.libriciel.ipcore.IpCoreApplication.*;
import static io.swagger.v3.oas.models.security.SecurityScheme.Type.OAUTH2;
import static org.apache.commons.lang3.StringUtils.EMPTY;


/**
 * Cheers to this example : {@see https://keithtmiller.com/2020/03/18/OpenApi3-Header-setup}
 */
@Log4j2
@Configuration
@ConditionalOnProperty(name = "springdoc.api-docs.enabled", havingValue = "true")
public class SpringdocConfigurer {


    private static final String SPRING_OAUTH_SECURITY_SCHEME_KEY = "spring_oauth";


    private @Value("${keycloak.auth-server-url}") String authServer;
    private @Value("${keycloak.realm}") String realm;
    private @Value("${application.version?:DEVELOP}") String version;
    private @Value("${info.application.name}") String applicationName;
    private @Value("${info.application.author.name}") String authorName;


    @Bean
    public OpenAPI apiInfo() {
        return new OpenAPI()
                .components(new Components().addSecuritySchemes(SPRING_OAUTH_SECURITY_SCHEME_KEY, generateOAuth2SecurityScheme()))
                .security(List.of(new SecurityRequirement().addList(SPRING_OAUTH_SECURITY_SCHEME_KEY)))
                .info(generateLocalInfo())
                .externalDocs(new ExternalDocumentation().url("https://www.libriciel.fr/i-parapheur/"));
    }


    private Info generateLocalInfo() {
        return new Info()
                .title(applicationName)
                .description(
                        """
                        iparapheur v5.x main core application.
                                                        
                        The main link between every sub-services, integrating business code logic.
                        """
                )
                .version(version)
                .license(new License().name("Affero GPL 3.0").url("https://www.gnu.org/licenses/agpl-3.0.en.html"))
                .contact(new Contact().name(authorName).url("https://libriciel.fr").email("iparapheur@libriciel.coop"));
    }


    private SecurityScheme generateOAuth2SecurityScheme() {
        return new SecurityScheme()
                .type(OAUTH2)
                .name("OAuth2 flow")
                .description("""
                             The following "Authorize" button will redirect you to the standard login page, fetch and store locally an authentication token. \\
                             that will be used for every Swagger method until its expiration.

                             You can manually get a token from command line, with the following command:

                             $ curl --location &#92;\\
                                 --request POST '%s/auth/realms/%s/protocol/openid-connect/token' &#92;\\
                                 --header 'Content-Type: application/x-www-form-urlencoded' &#92;\\
                                 --data-urlencode 'password=password' &#92;\\
                                 --data-urlencode 'username=user' &#92;\\
                                 --data-urlencode 'client_id=ipcore-web' &#92;\\
                                 --data-urlencode 'grant_type=password'

                             ... and parse the .access_token path. It can be easily parsed with a :  ... | jq -r '.access_token'
                             """.formatted(authServer, realm)
                )
                .flows(new OAuthFlows().authorizationCode(
                        new OAuthFlow()
                                .authorizationUrl("./auth/realms/%s/protocol/openid-connect/auth".formatted(realm))
                                .tokenUrl("./auth/realms/%s/protocol/openid-connect/token".formatted(realm))
                                .refreshUrl("./auth/realms/%s/protocol/openid-connect/token".formatted(realm))
                                .scopes(new Scopes())
                ));
    }


    /**
     * The API context path should not be in the server variable.
     *
     * @param environment
     * @return
     * @see <a href="https://github.com/springdoc/springdoc-openapi/issues/1459">The issue that talk about this fix</a>
     */
    public OpenApiCustomiser contextPathCustomiser(Environment environment) {
        String contextPath = environment.getProperty("server.servlet.context-path", "/");
        return openApi -> {

            // For everything Core-related, we have a context-path, the /api.
            // In the OpenApi v3 specs, this context-path is automatically added to the root server URL.
            // Yet, we don't want that. It messes the Oauth2 tokenUrl.

            if (!StringUtils.equals("/", contextPath)) {

                // Strip context path from the "server"
                openApi.getServers().forEach(server -> server.setUrl(server.getUrl().replace(contextPath, EMPTY)));

                // Add context path to each "path"
                Paths openApiPaths = openApi.getPaths();
                HashSet<String> pathsToDelete = new HashSet<>();
                Paths tempOpenApiPaths = new Paths();

                // Do everything in temp objects to not mess up iteration
                openApiPaths.forEach((key, value) -> {
                    if (!key.startsWith(contextPath)) {
                        tempOpenApiPaths.put(contextPath + key, value);
                        pathsToDelete.add(key);
                    }
                });

                pathsToDelete.forEach(openApiPaths::remove);
                openApiPaths.putAll(tempOpenApiPaths);
            }
        };
    }


    /**
     * Two things are fixed here:
     * - Manually add some un-picked enums.
     * - Sort Schemas alphabetically.
     *
     * @return
     * @see <a href="https://github.com/springdoc/springdoc-openapi/issues/741#issuecomment-1165401132">The issue that talk about this fix</a>
     */
    @SuppressWarnings("rawtypes")
    public OpenApiCustomiser schemasCustomizer() {
        return openApi -> {

            // Sort Schemas alphabetically
            // This should definitely be a native option, like any other YAML parameter.
            // But for some reason it is not

            Map<String, Schema> schemas = openApi.getComponents().getSchemas();
            TreeMap<String, Schema> sortedSchemas = new TreeMap<>(schemas);

            // Add manually some schemas

            ModelConverters converterInstance = ModelConverters.getInstance();
            List<Class> schemasToAdd = List.of(
                    DelegationSortBy.class,
                    ExternalSignatureConfigSortBy.class,
                    FolderSortBy.class,
                    InternalMetadata.class,
                    LayerSortBy.class,
                    MetadataSortBy.class,
                    SealCertificateSortBy.class,
                    TenantSortBy.class,
                    TypologySortBy.class,
                    UserSortBy.class,
                    WorkflowDefinitionSortBy.class
            );
            schemasToAdd.forEach(clazz -> sortedSchemas.putAll(converterInstance.resolveAsResolvedSchema(new AnnotatedType(clazz)).referencedSchemas));

            // Rewrite result

            openApi.getComponents().setSchemas(sortedSchemas);
        };
    }


    @Bean
    @ConditionalOnProperty(name = "api-docs.show.internal", havingValue = "true")
    public GroupedOpenApi apiInternal(Environment environment) {
        return GroupedOpenApi.builder()
                .group(API_INTERNAL.replaceAll("/", "_"))
                .pathsToMatch("/%s/**".formatted(API_INTERNAL))
                .addOpenApiCustomiser(contextPathCustomiser(environment))
                .addOpenApiCustomiser(schemasCustomizer())
                .build();
    }


    @Bean
    @ConditionalOnProperty(name = "api-docs.show.standard", havingValue = "true")
    public GroupedOpenApi apiV1(Environment environment) {
        return GroupedOpenApi.builder()
                .group(API_V1)
                .pathsToMatch("/%s/**".formatted(API_V1))
                .addOpenApiCustomiser(contextPathCustomiser(environment))
                .addOpenApiCustomiser(schemasCustomizer())
                .build();
    }


    @Bean
    public GroupedOpenApi apiProvisioningV1(Environment environment) {
        return GroupedOpenApi.builder()
                .group(API_PROVISIONING_V1.replaceAll("/", "_"))
                .pathsToMatch("/%s/**".formatted(API_PROVISIONING_V1))
                .addOpenApiCustomiser(contextPathCustomiser(environment))
                .addOpenApiCustomiser(schemasCustomizer())
                .build();
    }


}
