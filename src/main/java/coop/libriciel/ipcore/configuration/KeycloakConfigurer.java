/*
 * iparapheur Core
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.ipcore.configuration;

import org.keycloak.adapters.springboot.KeycloakSpringBootProperties;
import org.keycloak.adapters.springsecurity.KeycloakSecurityComponents;
import org.keycloak.adapters.springsecurity.authentication.KeycloakAuthenticationProvider;
import org.keycloak.adapters.springsecurity.config.KeycloakWebSecurityConfigurerAdapter;
import org.keycloak.admin.client.KeycloakBuilder;
import org.keycloak.authorization.client.AuthzClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.mapping.SimpleAuthorityMapper;
import org.springframework.security.web.authentication.logout.LogoutFilter;
import org.springframework.security.web.authentication.preauth.x509.X509AuthenticationFilter;
import org.springframework.security.web.authentication.session.NullAuthenticatedSessionStrategy;
import org.springframework.security.web.authentication.session.SessionAuthenticationStrategy;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;
import java.util.stream.Stream;

import static java.util.Collections.singletonList;
import static javax.servlet.http.HttpServletResponse.SC_OK;
import static org.keycloak.OAuth2Constants.CLIENT_CREDENTIALS;
import static org.springframework.http.HttpMethod.*;
import static org.springframework.security.config.http.SessionCreationPolicy.STATELESS;


@Configuration
@EnableWebSecurity
@ComponentScan(basePackageClasses = KeycloakSecurityComponents.class)
@ConditionalOnProperty(name = "keycloak.enabled", havingValue = "true", matchIfMissing = true)
public class KeycloakConfigurer extends KeycloakWebSecurityConfigurerAdapter {


    private @Value("${keycloak.resource}") String clientId;
    private @Value("${keycloak.credentials.secret}") String clientSecret;

    private @Value("${keycloak.auth-server-url}") String authServer;
    private @Value("${keycloak.realm}") String realm;


    /**
     * Defines the session authentication strategy.
     */
    @Bean
    @Override
    protected SessionAuthenticationStrategy sessionAuthenticationStrategy() {
        // required for bearer-only applications.
        return new NullAuthenticatedSessionStrategy();
    }


    /**
     * Registers the KeycloakAuthenticationProvider with the authentication manager.
     */
    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) {
        KeycloakAuthenticationProvider keycloakAuthenticationProvider = keycloakAuthenticationProvider();
        // simple Authority Mapper to avoid ROLE_
        keycloakAuthenticationProvider.setGrantedAuthoritiesMapper(new SimpleAuthorityMapper());
        auth.authenticationProvider(keycloakAuthenticationProvider);
    }


    /**
     * Keycloak specific configuration (filters, ...)
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .sessionManagement()
                // use previously declared bean
                .sessionAuthenticationStrategy(sessionAuthenticationStrategy())

                // keycloak filters for safeguards
                .and()
                .addFilterBefore(keycloakPreAuthActionsFilter(), LogoutFilter.class)
                .addFilterBefore(keycloakAuthenticationProcessingFilter(), X509AuthenticationFilter.class)
                .exceptionHandling()
                .authenticationEntryPoint(authenticationEntryPoint())

                // add cors options
                .and().cors()
                // delegate logout endpoint to spring security

                .and()
                .logout()
                .addLogoutHandler(keycloakLogoutHandler())
                .logoutUrl("/logout")
                // logout handler for API
                .logoutSuccessHandler((HttpServletRequest request, HttpServletResponse response, Authentication authentication) -> response.setStatus(SC_OK))
                .and()
                .apply(new CommonKeycloakAdapter());
    }


    @Bean
    public CorsConfigurationSource corsConfigurationSource() {

        CorsConfiguration configuration = new CorsConfiguration();
        configuration.setAllowedOrigins(singletonList("*"));
        configuration.setAllowedMethods(Stream.of(OPTIONS, GET, POST, PUT, DELETE).map(HttpMethod::name).toList());
        configuration.setAllowedHeaders(Arrays.asList("Access-Control-Allow-Headers", "Access-Control-Allow-Origin", "Authorization"));

        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", configuration);

        return source;
    }


    /**
     * See https://docs.spring.io/spring-security/site/docs/current/reference/htmlsingle/#jc-custom-dsls
     * <ul>
     *     <li>Manage paths safeguards here !You must use this configuration in tests to validate routes safeguards</li>
     *     <li>Use with .and().apply(...) on http dsl</li>
     * </ul>
     */
    public static class CommonKeycloakAdapter extends AbstractHttpConfigurer<CommonKeycloakAdapter, HttpSecurity> {

        @Override
        public void init(HttpSecurity http) throws Exception {
            // any method that adds another configurer
            // must be done in the init method
            http
                    // disable csrf because of API mode
                    .csrf().disable()
                    .sessionManagement()
                    .sessionCreationPolicy(STATELESS)

                    .and()
                    // manage routes safeguards here
                    .authorizeRequests()
                    .antMatchers(OPTIONS).permitAll()
                    .antMatchers("/swagger-ui/**").permitAll()
                    .antMatchers("/v3/api-docs/**").permitAll()
                    .antMatchers("/actuator/**").permitAll()
                    .antMatchers("/**").permitAll()

                    .anyRequest().denyAll();
        }

    }


    @Bean
    public KeycloakBuilder builder() {
        return KeycloakBuilder.builder()
                .realm(realm)
                .serverUrl(authServer)
                .clientId(clientId)
                .clientSecret(clientSecret)
                .grantType(CLIENT_CREDENTIALS);
    }


    /**
     * The default {@link AuthzClient#create()} asks a `keycloak.json` file, that doesn't really exist on SpringBoot.
     * This is a (not-so-bad) workaround to make it SpringBoot-friendly.
     * <p>
     * Cheers to <a href="https://gist.github.com/dasniko/2c64393da0bca89434670908141914c4">this post</a>
     *
     * @param kcProperties
     * @return
     */
    @Bean
    public AuthzClient authzClient(KeycloakSpringBootProperties kcProperties) {

        org.keycloak.authorization.client.Configuration configuration = new org.keycloak.authorization.client.Configuration(
                kcProperties.getAuthServerUrl(),
                kcProperties.getRealm(),
                kcProperties.getResource(),
                kcProperties.getCredentials(),
                null
        );

        return AuthzClient.create(configuration);
    }


}
