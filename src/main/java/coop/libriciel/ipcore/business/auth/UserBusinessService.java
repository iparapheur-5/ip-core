/*
 * iparapheur Core
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.ipcore.business.auth;

import coop.libriciel.ipcore.model.auth.DeskRepresentation;
import coop.libriciel.ipcore.model.auth.User;
import coop.libriciel.ipcore.model.auth.requests.DeskDto;
import coop.libriciel.ipcore.model.auth.requests.UserDto;
import coop.libriciel.ipcore.model.auth.requests.UserRepresentation;
import coop.libriciel.ipcore.model.database.TenantRepresentation;
import coop.libriciel.ipcore.services.auth.AuthServiceInterface;
import coop.libriciel.ipcore.services.database.DatabaseServiceInterface;
import coop.libriciel.ipcore.services.permission.PermissionServiceInterface;
import coop.libriciel.ipcore.utils.LocalizedStatusException;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Stream;

import static coop.libriciel.ipcore.model.auth.UserPrivilege.*;
import static java.util.Comparator.*;
import static org.apache.commons.collections4.CollectionUtils.subtract;
import static org.springframework.data.domain.Pageable.unpaged;
import static org.springframework.http.HttpStatus.NOT_ACCEPTABLE;
import static org.springframework.util.CollectionUtils.isEmpty;


@Log4j2
@Service
public class UserBusinessService {


    // <editor-fold desc="Beans">


    private final AuthServiceInterface authService;
    private final DatabaseServiceInterface dbService;
    private final ModelMapper modelMapper;
    private final PermissionServiceInterface permissionService;


    public UserBusinessService(AuthServiceInterface authService,
                               DatabaseServiceInterface dbService,
                               ModelMapper modelMapper,
                               PermissionServiceInterface permissionService) {
        this.authService = authService;
        this.dbService = dbService;
        this.modelMapper = modelMapper;
        this.permissionService = permissionService;
    }


    // </editor-fold desc="Beans">


    public void updateGlobalInnerValues(@Nullable UserDto userDto) {

        if (userDto == null) {
            return;
        }

        List<TenantRepresentation> associatedTenants = dbService
                .listTenantsForUser(userDto.getId(), unpaged(), null, false, false, false)
                .getContent();

        List<TenantRepresentation> administeredTenants = dbService
                .listTenantsForUser(userDto.getId(), unpaged(), null, false, true, false)
                .getContent();

        // Mutable copies, and sort
        associatedTenants = new ArrayList<>(associatedTenants);
        administeredTenants = new ArrayList<>(administeredTenants);
        associatedTenants.sort(comparing(TenantRepresentation::getName, nullsLast(naturalOrder())));
        administeredTenants.sort(comparing(TenantRepresentation::getName, nullsLast(naturalOrder())));

        userDto.setAssociatedTenants(associatedTenants);
        userDto.setAdministeredTenants(administeredTenants);

        userDto.setAssociatedTenantIds(associatedTenants.stream().map(TenantRepresentation::getId).toList());
        userDto.setAdministeredTenantIds(administeredTenants.stream().map(TenantRepresentation::getId).toList());
    }


    public void updateInnerValues(@Nullable DeskDto deskDto) {

        if (deskDto == null) {
            return;
        }

        // Fetch real models

        Set<String> referencedUserIds = new HashSet<>();
        Optional.ofNullable(deskDto.getOwnerIds()).ifPresent(referencedUserIds::addAll);
        Optional.ofNullable(deskDto.getSupervisorIds()).ifPresent(referencedUserIds::addAll);
        Optional.ofNullable(deskDto.getDelegationManagerIds()).ifPresent(referencedUserIds::addAll);

        Map<String, UserRepresentation> userNamesMap = authService.getUsersByIds(referencedUserIds).stream()
                .map(user -> modelMapper.map(user, UserRepresentation.class))
                .collect(HashMap::new, (map, user) -> map.put(user.getId(), user), HashMap::putAll);

        Optional.ofNullable(deskDto.getOwnerIds())
                .map(userIds -> userIds.stream()
                        .map(userNamesMap::get)
                        .sorted(comparing(UserRepresentation::getUserName, nullsLast(naturalOrder())))
                        .toList()
                )
                .ifPresent(deskDto::setOwners);

        Optional.ofNullable(deskDto.getSupervisorIds())
                .map(userIds -> userIds.stream()
                        .map(userNamesMap::get)
                        .sorted(comparing(UserRepresentation::getUserName, nullsLast(naturalOrder())))
                        .toList()
                )
                .ifPresent(deskDto::setSupervisors);

        Optional.ofNullable(deskDto.getDelegationManagerIds())
                .map(userIds -> userIds.stream()
                        .map(userNamesMap::get)
                        .sorted(comparing(UserRepresentation::getUserName, nullsLast(naturalOrder())))
                        .toList()
                )
                .ifPresent(deskDto::setDelegationManagers);
    }


    public void updateTenantInnerValues(@NotNull String tenantId, @Nullable UserDto userDto) {

        if (userDto == null) {
            return;
        }

        updateGlobalInnerValues(userDto);

        List<DeskRepresentation> administeredDesks = permissionService.getAdministeredDesks(userDto.getId(), tenantId);
        List<DeskRepresentation> associatedDesks = new ArrayList<>(authService.getDesksFromUser(userDto.getId(), unpaged(), null).getContent());
        List<DeskRepresentation> delegationManagedDesks = permissionService.getDelegationManagedDesks(userDto.getId(), tenantId);
        List<DeskRepresentation> supervisedDesks = permissionService.getSupervisedDesks(userDto.getId(), tenantId);
        associatedDesks.removeIf(desk -> !StringUtils.equals(desk.getTenantId(), tenantId));

        // Retrieve names and sort

        List<DeskRepresentation> innerDesks = Stream.of(administeredDesks, associatedDesks, supervisedDesks, delegationManagedDesks)
                .filter(Objects::nonNull)
                .flatMap(Collection::stream)
                .toList();

        authService.updateDeskNames(innerDesks);

        administeredDesks.sort(comparing(DeskRepresentation::getName, nullsLast(naturalOrder())));
        associatedDesks.sort(comparing(DeskRepresentation::getName, nullsLast(naturalOrder())));
        delegationManagedDesks.sort(comparing(DeskRepresentation::getName, nullsLast(naturalOrder())));
        supervisedDesks.sort(comparing(DeskRepresentation::getName, nullsLast(naturalOrder())));

        // TODO: remove the set<xxx>Ids
        userDto.setAdministeredDesks(administeredDesks);
        userDto.setAssociatedDesks(associatedDesks);
        userDto.setDelegationManagedDesks(delegationManagedDesks);
        userDto.setSupervisedDesks(supervisedDesks);

        userDto.setAdministeredDeskIds(administeredDesks.stream().map(DeskRepresentation::getId).toList());
        userDto.setAssociatedDeskIds(associatedDesks.stream().map(DeskRepresentation::getId).toList());
        userDto.setDelegationManagedDeskIds(delegationManagedDesks.stream().map(DeskRepresentation::getId).toList());
        userDto.setSupervisedDeskIds(supervisedDesks.stream().map(DeskRepresentation::getId).toList());
    }


    public void computeGlobalLevelDifferencesAndUpdateUserRolesAndPermissions(User existingUser, UserDto requestUserDto) {

        String userId = existingUser.getId();

        // Integrity checks

        boolean isSuperAdmin = requestUserDto.getPrivilege() == SUPER_ADMIN;
        boolean isTenantLess = isEmpty(requestUserDto.getAssociatedTenantIds()) && isEmpty(requestUserDto.getAdministeredTenantIds());
        if (isTenantLess && !isSuperAdmin) {
            throw new LocalizedStatusException(NOT_ACCEPTABLE, "message.a_non_super_admin_should_be_associated_with_at_least_one_tenant");
        }

        // Refresh every user sub-permissions, to ease the following diff computing.

        UserDto existingUserDto = modelMapper.map(existingUser, UserDto.class);
        updateGlobalInnerValues(existingUserDto);

        // Compute differences

        Collection<String> administeredTenantIdsToAdd = subtract(requestUserDto.getAdministeredTenantIds(), existingUserDto.getAdministeredTenantIds());
        Collection<String> administeredTenantIdsToRemove = subtract(existingUserDto.getAdministeredTenantIds(), requestUserDto.getAdministeredTenantIds());

        log.trace("updateUser requestUserDto.getAdministeredTenantIds:{}", requestUserDto.getAdministeredTenantIds());
        log.trace("updateUser existingUserDto.getAdministeredTenantIds:{}", existingUserDto.getAdministeredTenantIds());
        log.trace("updateUser administeredTenantIdsToAdd:{}", administeredTenantIdsToAdd);
        log.trace("updateUser administeredTenantIdsToRemove:{}", administeredTenantIdsToRemove);

        administeredTenantIdsToAdd.forEach(tenantId -> authService.addUserToTenant(existingUser, tenantId, TENANT_ADMIN));
        administeredTenantIdsToRemove.forEach(tenantId -> authService.addUserToTenant(existingUser, tenantId, NONE));

        Collection<String> tenantIdsToAdd = subtract(requestUserDto.getAssociatedTenantIds(), existingUserDto.getAssociatedTenantIds());
        Collection<String> tenantIdsToRemove = subtract(existingUserDto.getAssociatedTenantIds(), requestUserDto.getAssociatedTenantIds());

        log.trace("updateUser requestUserDto.getAssociatedTenantIds:{}", requestUserDto.getAssociatedTenantIds());
        log.trace("updateUser existingUserDto.getAssociatedTenantIds:{}", existingUserDto.getAssociatedTenantIds());
        log.trace("updateUser tenantIdsToAdd:{}", tenantIdsToAdd);
        log.trace("updateUser tenantIdsToRemove:{}", tenantIdsToRemove);

        tenantIdsToAdd.forEach(tenantId -> authService.addUserToTenant(existingUser, tenantId, NONE));
        tenantIdsToRemove.forEach(tenantId -> authService.removeUserFromTenant(userId, tenantId));
    }


    public void computeTenantLevelDifferencesAndUpdateUserRolesAndPermissions(@NotNull String tenantId, User existingUser, UserDto request) {

        if (request.getSupervisedDeskIds() != null) {

            List<String> originalSupervisedDeskIds = permissionService.getSupervisedDesks(existingUser.getId(), tenantId).stream()
                    .map(DeskRepresentation::getId)
                    .toList();

            originalSupervisedDeskIds.stream()
                    .filter(origId -> request.getSupervisedDeskIds().stream().noneMatch(requestId -> StringUtils.equals(origId, requestId)))
                    .forEach(deskId -> permissionService.removeSupervisorsFromDesk(tenantId, deskId, List.of(existingUser.getId())));

            request.getSupervisedDeskIds().stream()
                    .filter(requestId -> originalSupervisedDeskIds.stream().noneMatch(origId -> StringUtils.equals(requestId, origId)))
                    .forEach(deskId -> permissionService.addSupervisorsToDesk(tenantId, deskId, List.of(existingUser.getId())));
        }

        if (request.getPrivilege() != FUNCTIONAL_ADMIN) {
            log.debug("remove all administered desks");
            permissionService.removeAllAdministeredDesks(tenantId, existingUser.getId());
        } else if (request.getAdministeredDeskIds() != null) {

            List<String> originalAdministeredDeskIds = permissionService.getAdministeredDesks(existingUser.getId(), tenantId).stream()
                    .map(DeskRepresentation::getId)
                    .toList();

            originalAdministeredDeskIds.stream()
                    .filter(origId -> request.getAdministeredDeskIds().stream().noneMatch(requestId -> StringUtils.equals(origId, requestId)))
                    .forEach(deskId -> permissionService.removeFunctionalAdmin(tenantId, deskId, existingUser.getId()));

            request.getAdministeredDeskIds().stream()
                    .filter(requestId -> originalAdministeredDeskIds.stream().noneMatch(origId -> StringUtils.equals(requestId, origId)))
                    .forEach(deskId -> permissionService.setFunctionalAdmin(tenantId, deskId, existingUser.getId()));
        }

        if (request.getDelegationManagedDeskIds() != null) {

            List<String> originalDelegationManagerIds = permissionService.getDelegationManagedDesks(existingUser.getId(), tenantId).stream()
                    .map(DeskRepresentation::getId)
                    .toList();

            originalDelegationManagerIds.stream()
                    .filter(origId -> request.getDelegationManagedDeskIds().stream().noneMatch(requestId -> StringUtils.equals(origId, requestId)))
                    .forEach(deskId -> permissionService.removeDelegationManagersFromDesk(tenantId, deskId, List.of(existingUser.getId())));

            request.getDelegationManagedDeskIds().stream()
                    .filter(requestId -> originalDelegationManagerIds.stream().noneMatch(origId -> StringUtils.equals(requestId, origId)))
                    .forEach(deskId -> permissionService.addDelegationManagersToDesk(tenantId, deskId, List.of(existingUser.getId())));
        }

        if (request.getAssociatedDeskIds() != null) {

            List<String> originalAssociatedDeskIds = authService.getDesksFromUser(existingUser.getId(), unpaged(), null)
                    .filter(desk -> StringUtils.equals(tenantId, desk.getTenantId()))
                    .map(DeskRepresentation::getId)
                    .toList();

            originalAssociatedDeskIds.stream()
                    .filter(origId -> request.getAssociatedDeskIds().stream().noneMatch(requestId -> StringUtils.equals(origId, requestId)))
                    .forEach(deskId -> authService.removeUsersFromDesk(deskId, List.of(existingUser.getId())));

            request.getAssociatedDeskIds().stream()
                    .filter(requestId -> originalAssociatedDeskIds.stream().noneMatch(origId -> StringUtils.equals(requestId, origId)))
                    .forEach(deskId -> authService.addUsersToDesk(deskId, List.of(existingUser.getId())));
        }
    }


}
