/*
 * iparapheur Core
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.ipcore.business.auth;

import coop.libriciel.ipcore.model.auth.Desk;
import coop.libriciel.ipcore.model.auth.DeskRepresentation;
import coop.libriciel.ipcore.model.auth.User;
import coop.libriciel.ipcore.model.auth.requests.DeskDto;
import coop.libriciel.ipcore.model.database.Subtype;
import coop.libriciel.ipcore.model.database.Type;
import coop.libriciel.ipcore.model.database.TypologyEntity;
import coop.libriciel.ipcore.model.database.TypologyRepresentation;
import coop.libriciel.ipcore.model.database.requests.SubtypeDto;
import coop.libriciel.ipcore.model.permission.DelegationDto;
import coop.libriciel.ipcore.model.websockets.DeskCount;
import coop.libriciel.ipcore.model.workflow.DelegationRule;
import coop.libriciel.ipcore.services.auth.AuthServiceInterface;
import coop.libriciel.ipcore.services.database.SubtypeRepository;
import coop.libriciel.ipcore.services.database.TypeRepository;
import coop.libriciel.ipcore.services.permission.PermissionServiceInterface;
import coop.libriciel.ipcore.services.workflow.WorkflowServiceInterface;
import coop.libriciel.ipcore.utils.LocalizedStatusException;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Stream;

import static coop.libriciel.ipcore.model.workflow.State.*;
import static coop.libriciel.ipcore.services.permission.PermissionServiceInterface.*;
import static coop.libriciel.ipcore.services.workflow.WorkflowServiceInterface.META_SUBTYPE_ID;
import static coop.libriciel.ipcore.services.workflow.WorkflowServiceInterface.META_TYPE_ID;
import static coop.libriciel.ipcore.utils.ApiUtils.MAX_USER_PER_DESKS_COUNT;
import static coop.libriciel.ipcore.utils.RequestUtils.hasChangesBetween;
import static java.util.Collections.*;
import static java.util.Comparator.*;
import static java.util.stream.Collectors.toMap;
import static java.util.stream.Collectors.toSet;
import static org.apache.commons.collections4.CollectionUtils.subtract;
import static org.springframework.data.domain.Pageable.unpaged;
import static org.springframework.http.HttpStatus.*;


@Log4j2
@Service
public class DeskBusinessService {


    // <editor-fold desc="Beans">


    private final AuthServiceInterface authService;
    private final ModelMapper modelMapper;
    private final PermissionServiceInterface permissionService;
    private final SubtypeRepository subtypeRepository;
    private final TypeRepository typeRepository;
    private final WorkflowServiceInterface workflowService;


    public DeskBusinessService(AuthServiceInterface authService,
                               ModelMapper modelMapper,
                               PermissionServiceInterface permissionService,
                               SubtypeRepository subtypeRepository,
                               TypeRepository typeRepository,
                               WorkflowServiceInterface workflowService) {
        this.authService = authService;
        this.modelMapper = modelMapper;
        this.permissionService = permissionService;
        this.subtypeRepository = subtypeRepository;
        this.typeRepository = typeRepository;
        this.workflowService = workflowService;
    }


    // </editor-fold desc="Beans">


    public void updateInnerValues(@NotNull String tenantId, @Nullable Collection<DelegationDto> delegationDtoList) {

        if (CollectionUtils.isEmpty(delegationDtoList)) {
            return;
        }

        // Fetch real models

        Set<String> deskIds = Stream
                .concat(delegationDtoList.stream().map(DelegationDto::getDelegatingDeskId),
                        delegationDtoList.stream().map(DelegationDto::getSubstituteDeskId))
                .collect(toSet());

        Map<String, String> deskNames = authService.getDeskNames(deskIds);

        Set<String> typeIds = delegationDtoList.stream().map(DelegationDto::getTypeId).collect(toSet());
        Map<String, Type> typeMap = typeRepository
                .findAllByTenant_IdAndIdIn(tenantId, typeIds, unpaged()).getContent().stream()
                .collect(toMap(
                        TypologyEntity::getId,
                        type -> type
                ));

        Set<String> subtypeIds = delegationDtoList.stream().map(DelegationDto::getSubtypeId).collect(toSet());
        Map<String, Subtype> subtypeMap = subtypeRepository
                .findAllByTenant_IdAndIdIn(tenantId, subtypeIds, unpaged()).getContent().stream()
                .collect(toMap(
                        TypologyEntity::getId,
                        subtype -> subtype
                ));

        // Compute missing values and send it back

        delegationDtoList.forEach(delegationDto -> {

            String delegatingDeskId = delegationDto.getDelegatingDeskId();
            delegationDto.setDelegatingDesk(new DeskRepresentation(delegatingDeskId, deskNames.get(delegatingDeskId)));

            String substituteDeskId = delegationDto.getSubstituteDeskId();
            delegationDto.setSubstituteDesk(new DeskRepresentation(substituteDeskId, deskNames.get(substituteDeskId)));

            Optional.ofNullable(delegationDto.getTypeId())
                    .ifPresent(typeId -> {
                        Type currentType = typeMap.get(typeId);
                        delegationDto.setType(new TypologyRepresentation(currentType.getId(), currentType.getName(), null, null));
                    });

            Optional.ofNullable(delegationDto.getSubtypeId())
                    .ifPresent(subtypeId -> {
                        Subtype currentSubtype = subtypeMap.get(subtypeId);
                        Type currentType = currentSubtype.getParentType();
                        delegationDto.setSubtype(new TypologyRepresentation(currentSubtype.getId(), currentSubtype.getName(), currentType.getId(), null));
                        delegationDto.setType(new TypologyRepresentation(currentType.getId(), currentType.getName(), null, null));
                    });
        });
    }


    public void updateInnerValues(@Nullable SubtypeDto subtype) {

        if (subtype == null) {
            return;
        }

        // Fetch real models

        List<DeskRepresentation> referencedDesks = Stream
                .concat(Optional.ofNullable(subtype.getCreationPermittedDesks()).orElse(emptyList()).stream(),
                        Optional.ofNullable(subtype.getFilterableByDesks()).orElse(emptyList()).stream())
                .toList();

        Set<String> referencedDeskId = referencedDesks.stream().map(DeskRepresentation::getId).collect(toSet());
        Map<String, String> deskNames = authService.getDeskNames(referencedDeskId);
        referencedDesks.forEach(desk -> desk.setName(deskNames.get(desk.getId())));

        Optional.ofNullable(subtype.getCreationPermittedDesks())
                .ifPresent(desks -> desks.sort(comparing(DeskRepresentation::getName, nullsLast(naturalOrder()))));
        Optional.ofNullable(subtype.getFilterableByDesks())
                .ifPresent(desks -> desks.sort(comparing(DeskRepresentation::getName, nullsLast(naturalOrder()))));
    }


    public void updateInnerValues(@Nullable DeskDto deskDto) {

        if (deskDto == null) {
            return;
        }

        // Fetch real models

        Set<String> referencedDeskIds = Stream
                .concat(Optional.ofNullable(deskDto.getAssociatedDeskIds()).orElse(emptyList()).stream(),
                        Stream.empty())
                .collect(toSet());

        Map<String, String> deskNames = authService.getDeskNames(referencedDeskIds);

        Optional.ofNullable(deskDto.getAssociatedDeskIds())
                .map(idList -> idList.stream()
                        // We're using the original desk tenant here, since we cannot associate a tenant with another tenant's desk
                        // TODO: maybe we should loose that tenantId in the DeskRepresentation.
                        .map(id -> new DeskRepresentation(id, deskNames.get(id), deskDto.getTenantId(), null))
                        .sorted(comparing(DeskRepresentation::getName, nullsLast(naturalOrder())))
                        .toList()
                )
                .ifPresent(deskDto::setAssociatedDesks);
    }


    /**
     * If the same desk has duplicate entries in partial delegations,
     * we may want to simplify these cases.
     */
    public void factorizeDelegations(Map<String, Set<DelegationRule>> delegationRulesDeskMap) {

        List<String> referencedSubtypesId = delegationRulesDeskMap.values().stream()
                .flatMap(Collection::stream)
                .filter(rule -> StringUtils.equals(rule.getMetadataKey(), META_SUBTYPE_ID))
                .map(DelegationRule::getMetadataValue)
                .toList();

        Map<String, String> subtypesAndParentTypeMap = subtypeRepository.findAllByIdIn(referencedSubtypesId, unpaged())
                .getContent()
                .stream()
                .collect(toMap(
                        TypologyEntity::getId,
                        subtype -> subtype.getParentType().getId()
                ));

        delegationRulesDeskMap.values().stream()
                .filter(CollectionUtils::isNotEmpty)
                .forEach(delegationRules -> {

                    // If we have a full-desk delegation, and any partial one on the same target :
                    // We can remove the partial ones

                    Set<String> fullDeskIdDelegation = delegationRules.stream()
                            .filter(rule -> StringUtils.isEmpty(rule.getMetadataKey()))
                            .map(DelegationRule::getDelegatingGroup)
                            .collect(toSet());

                    delegationRules.removeIf(rule -> {
                        boolean isPartialDelegation = StringUtils.isNotEmpty(rule.getMetadataKey());
                        boolean isFullDeskDelegationAlreadyExists = fullDeskIdDelegation.contains(rule.getDelegatingGroup());
                        return isPartialDelegation && isFullDeskDelegationAlreadyExists;
                    });

                    // A partial delegation on a type, and another partial one on one of its subtype :
                    // The type-one is enough.

                    Map<String, Set<String>> fullTypeIdDelegationMap = new HashMap<>();
                    delegationRules.stream()
                            .filter(rule -> StringUtils.equals(rule.getMetadataKey(), META_TYPE_ID))
                            .forEach(rule -> {
                                String targetDeskId = rule.getDelegatingGroup();
                                fullTypeIdDelegationMap.computeIfAbsent(targetDeskId, k -> new HashSet<>());
                                fullTypeIdDelegationMap.get(targetDeskId).add(rule.getMetadataValue());
                            });

                    delegationRules.removeIf(rule -> {
                        String targetDeskId = rule.getDelegatingGroup();
                        String parentTypeId = subtypesAndParentTypeMap.get(rule.getMetadataValue());
                        boolean isSubtypeDelegation = StringUtils.equals(rule.getMetadataKey(), META_SUBTYPE_ID);
                        boolean isFullTypeDelegationAlreadyExists = fullTypeIdDelegationMap.getOrDefault(targetDeskId, emptySet()).contains(parentTypeId);
                        return isSubtypeDelegation && isFullTypeDelegationAlreadyExists;
                    });
                });
    }


    public void updateDeskIntegrityChecks(@NotNull Desk existingDesk, @NotNull DeskDto request, @NotNull String tenantId) {

        if (StringUtils.isNotEmpty(request.getParentDeskId())) {
            Optional.of(request.getParentDeskId())
                    .map(i -> authService.findDeskByIdNoException(tenantId, i))
                    .orElseThrow(() -> new LocalizedStatusException(NOT_FOUND, "message.unknown_parent_desk_id"));
        }

        if (!StringUtils.equals(existingDesk.getName(), request.getName())) {
            // If the name has been updated, we check it the new one don't collide an already existing one.
            authService.searchDesks(tenantId, unpaged(), emptyList(), request.getName(), false)
                    .getContent()
                    .stream()
                    .filter(d -> StringUtils.equals(d.getName(), request.getName()))
                    .findFirst()
                    .ifPresent(d -> {throw new LocalizedStatusException(CONFLICT, "message.already_existing_desk_name");});
        }

        authService.findDeskByShortName(tenantId, request.getShortName()).stream()
                // Avoid fake conflict with the currently modified desk
                .filter(d -> !StringUtils.equals(d.getId(), existingDesk.getId()))
                .findFirst()
                .ifPresent(d -> {throw new LocalizedStatusException(CONFLICT, "message.already_existing_desk_short_name");});
    }


    public void computeDifferencesAndUpdateDesk(Desk existingDesk, DeskDto request, String tenantId) {

        this.updateDeskUsers(request, tenantId, existingDesk.getId());
        this.updateDeskAssociatedDesks(request, tenantId, existingDesk.getId());
        this.updateDeskFilterableMetadata(request, tenantId, existingDesk.getId());
        this.updateDeskSupervisors(request, tenantId, existingDesk.getId());
        this.updateDeskDelegations(request, tenantId, existingDesk.getId());

        // Actual edit
        authService.editDesk(tenantId, existingDesk, request.getName(), request.getShortName(), request.getDescription(), request.getParentDeskId());

        // Permissions

        Desk dummyDesk = modelMapper.map(request, Desk.class);
        dummyDesk.setId(existingDesk.getId());
        if (hasChangesBetween(existingDesk, dummyDesk, Desk::isActionAllowed, Desk::isArchivingAllowed, Desk::isChainAllowed, Desk::isFolderCreationAllowed)) {
            permissionService.editDeskPermission(tenantId, dummyDesk);
        }

    }


    void updateDeskUsers(DeskDto request, String tenantId, String existingDeskId) {
        Set<String> existingUserIds = authService.listUsersFromDesk(tenantId, existingDeskId, 0, MAX_USER_PER_DESKS_COUNT)
                .getData()
                .stream()
                .map(User::getId)
                .collect(toSet());
        Collection<String> userIdsToAdd = subtract(request.getOwnerIds(), existingUserIds);
        Collection<String> userIdsToRemove = subtract(existingUserIds, request.getOwnerIds());

        log.trace("updateDeskUsers request.getOwnerUserIdsList:{}", request.getOwnerIds());
        log.trace("updateDeskUsers existingUserIds:{}", existingUserIds);
        log.trace("updateDeskUsers userIdsToAdd:{}", userIdsToAdd);
        log.trace("updateDeskUsers userIdsToRemove:{}", userIdsToRemove);

        if ((existingUserIds.size() + userIdsToAdd.size() - userIdsToRemove.size()) >= MAX_USER_PER_DESKS_COUNT) {
            throw new LocalizedStatusException(INSUFFICIENT_STORAGE, "message.already_n_users_on_this_desk_maximum_reached", MAX_USER_PER_DESKS_COUNT);
        }

        authService.addUsersToDesk(existingDeskId, userIdsToAdd);
        authService.removeUsersFromDesk(existingDeskId, userIdsToRemove);
    }


    void updateDeskAssociatedDesks(DeskDto request, String tenantId, String existingDeskId) {

        Collection<String> existingAssociatedDeskIds = permissionService.getAssociatedDesks(tenantId, existingDeskId);
        Collection<String> associatedDeskIdsToAdd = subtract(request.getAssociatedDeskIds(), existingAssociatedDeskIds);
        Collection<String> associatedDeskIdsToRemove = subtract(existingAssociatedDeskIds, request.getAssociatedDeskIds());

        log.trace("updateDeskAssociatedDesks request.getAssociatedDeskIdsList:{}", request.getAssociatedDeskIds());
        log.trace("updateDeskAssociatedDesks existingAssociatedDeskIds:{}", existingAssociatedDeskIds);
        log.trace("updateDeskAssociatedDesks associatedDeskIdsToAdd:{}", associatedDeskIdsToAdd);
        log.trace("updateDeskAssociatedDesks associatedDeskIdsToRemove:{}", associatedDeskIdsToRemove);

        if ((existingAssociatedDeskIds.size() + associatedDeskIdsToAdd.size() - associatedDeskIdsToRemove.size()) >= MAX_ASSOCIATED_DESK_PER_DESK) {
            throw new LocalizedStatusException(INSUFFICIENT_STORAGE, "message.already_n_associated_desks_maximum_reached", MAX_ASSOCIATED_DESK_PER_DESK);
        }

        permissionService.associateDesks(tenantId, existingDeskId, associatedDeskIdsToAdd);
        permissionService.removeAssociatedDesks(tenantId, existingDeskId, associatedDeskIdsToRemove);
    }


    void updateDeskFilterableMetadata(DeskDto request, String tenantId, String existingDeskId) {
        Set<String> existingFilterableMetadataIds = new HashSet<>(permissionService.getFilterableMetadataIds(tenantId, existingDeskId));
        Collection<String> filterableMetadataIdsToAdd = subtract(request.getFilterableMetadataIds(), existingFilterableMetadataIds);
        Collection<String> filterableMetadataIdsToRemove = subtract(existingFilterableMetadataIds, request.getFilterableMetadataIds());

        log.trace("updateDeskFilterableMetadata request.getFilterableMetadataIds:{}", request.getFilterableMetadataIds());
        log.trace("updateDeskFilterableMetadata existingFilterableMetadataIds:{}", existingFilterableMetadataIds);
        log.trace("updateDeskFilterableMetadata filterableMetadataIdsToAdd:{}", filterableMetadataIdsToAdd);
        log.trace("updateDeskFilterableMetadata filterableMetadataIdsToRemove:{}", filterableMetadataIdsToRemove);

        if ((existingFilterableMetadataIds.size() + filterableMetadataIdsToAdd.size() - filterableMetadataIdsToRemove.size()) >= MAX_ASSOCIATED_DESK_PER_DESK) {
            throw new LocalizedStatusException(INSUFFICIENT_STORAGE, "message.already_n_associated_desks_maximum_reached", MAX_ASSOCIATED_DESK_PER_DESK);
        }

        permissionService.addFilterableMetadata(tenantId, existingDeskId, filterableMetadataIdsToAdd);
        permissionService.removeFilterableMetadata(tenantId, existingDeskId, filterableMetadataIdsToRemove);
    }


    void updateDeskSupervisors(DeskDto request, String tenantId, String existingDeskId) {
        Set<String> existingSupervisorIds = new HashSet<>(permissionService.getSupervisorsFromDesk(tenantId, existingDeskId));
        Collection<String> supervisorIdsToAdd = subtract(request.getSupervisorIds(), existingSupervisorIds);
        Collection<String> supervisorIdsToRemove = subtract(existingSupervisorIds, request.getSupervisorIds());

        log.trace("updateDeskSupervisors request.getSupervisorIdsList:{}", request.getSupervisorIds());
        log.trace("updateDeskSupervisors existingSupervisorIds:{}", existingSupervisorIds);
        log.trace("updateDeskSupervisors supervisorIdsToAdd:{}", supervisorIdsToAdd);
        log.trace("updateDeskSupervisors supervisorIdsToRemove:{}", supervisorIdsToRemove);

        if ((existingSupervisorIds.size() + supervisorIdsToAdd.size() - supervisorIdsToRemove.size()) >= MAX_SUPERVISOR_PER_DESK) {
            throw new LocalizedStatusException(INSUFFICIENT_STORAGE, "message.already_n_supervisors_maximum_reached", MAX_SUPERVISOR_PER_DESK);
        }

        permissionService.addSupervisorsToDesk(tenantId, existingDeskId, supervisorIdsToAdd);
        permissionService.removeSupervisorsFromDesk(tenantId, existingDeskId, supervisorIdsToRemove);
    }


    void updateDeskDelegations(DeskDto request, String tenantId, String existingDeskId) {
        Set<String> existingDelegationManagerIds = new HashSet<>(permissionService.getDelegationManagerOfDesk(tenantId, existingDeskId));
        Collection<String> delegationManagerIdsToAdd = subtract(request.getDelegationManagerIds(), existingDelegationManagerIds);
        Collection<String> delegationManagerIdsToRemove = subtract(existingDelegationManagerIds, request.getDelegationManagerIds());

        log.trace("updateDeskDelegations request.getDelegationManagerIdsList:{}", request.getDelegationManagerIds());
        log.trace("updateDeskDelegations existingDelegationManagerIds:{}", existingDelegationManagerIds);
        log.trace("updateDeskDelegations delegationManagerIdsToAdd:{}", delegationManagerIdsToAdd);
        log.trace("updateDeskDelegations delegationManagerIdsToRemove:{}", delegationManagerIdsToRemove);

        if ((existingDelegationManagerIds.size() + delegationManagerIdsToAdd.size() - delegationManagerIdsToRemove.size()) >= MAX_DELEGATION_MANAGER_PER_DESK) {
            throw new LocalizedStatusException(INSUFFICIENT_STORAGE, "message.already_n_delegations_managers_maximum_reached", MAX_DELEGATION_MANAGER_PER_DESK);
        }

        permissionService.addDelegationManagersToDesk(tenantId, existingDeskId, delegationManagerIdsToAdd);
        permissionService.removeDelegationManagersFromDesk(tenantId, existingDeskId, delegationManagerIdsToRemove);
    }


    public DeskCount getDeskCount(Desk desk) {
        Map<DelegationRule, Integer> deskCountMap = workflowService.countFolders(singleton(desk.getId()), desk.getDelegationRules());
        List<DeskCount> delegatingDeskCounts = desk.getDelegatingDesks()
                .stream()
                .map(delegatingDesk -> {
                    Map<DelegationRule, Integer> delegatingDeskCountMap = workflowService.countFolders(singleton(delegatingDesk.getId()),
                            delegatingDesk.getDelegationRules());
                    return new DeskCount(
                            delegatingDesk.getTenantId(),
                            delegatingDesk.getId(),
                            delegatingDeskCountMap.getOrDefault(new DelegationRule(delegatingDesk.getId(), DRAFT, null, null), 0),
                            delegatingDeskCountMap.getOrDefault(new DelegationRule(delegatingDesk.getId(), REJECTED, null, null), 0),
                            delegatingDeskCountMap.getOrDefault(new DelegationRule(delegatingDesk.getId(), PENDING, null, null), 0),
                            delegatingDeskCountMap.getOrDefault(new DelegationRule(delegatingDesk.getId(), FINISHED, null, null), 0),
                            delegatingDeskCountMap.getOrDefault(new DelegationRule(delegatingDesk.getId(), LATE, null, null), 0),
                            emptyList()
                    );
                }).toList();

        return new DeskCount(
                desk.getTenantId(),
                desk.getId(),
                deskCountMap.getOrDefault(new DelegationRule(desk.getId(), DRAFT, null, null), 0),
                deskCountMap.getOrDefault(new DelegationRule(desk.getId(), REJECTED, null, null), 0),
                deskCountMap.getOrDefault(new DelegationRule(desk.getId(), PENDING, null, null), 0),
                deskCountMap.getOrDefault(new DelegationRule(desk.getId(), FINISHED, null, null), 0),
                deskCountMap.getOrDefault(new DelegationRule(desk.getId(), LATE, null, null), 0),
                delegatingDeskCounts
        );
    }


    public void populateCountsAndDelegations(Map<String, Desk> deskIdNameMap, List<Desk> userDeskList) {
        Map<String, Set<DelegationRule>> delegations = permissionService.getActiveDelegations(
                userDeskList.stream().map(Desk::getId).collect(toSet())
        );

        factorizeDelegations(delegations);
        log.debug("getDesks delegations:{}", delegations);
        userDeskList.stream()
                .filter(d -> delegations.containsKey(d.getId()))
                .forEach(d -> d.getDelegationRules().addAll(delegations.get(d.getId())));


        Map<DelegationRule, Integer> deskCountMap = workflowService.countFolders(
                deskIdNameMap.keySet(),
                delegations.values().stream().flatMap(Collection::stream).toList()
        );

        log.trace("getDesks deskCountMap:{}", deskCountMap);
        userDeskList.forEach(d -> populateCountsAndSubDesks(d, deskCountMap));
    }


    private void populateCountsAndSubDesks(@NotNull Desk desk, @NotNull Map<DelegationRule, Integer> countMap) {

        // Adding appropriate SubDesks in the delegatingDesks parameter

        Map<String, Desk> desks = new HashMap<>();

        desk.getDelegationRules()
                .stream()
                .filter(r -> !desks.containsKey(r.getDelegatingGroup()))
                .forEach(r -> desks.put(r.getDelegatingGroup(), Desk.builder().id(r.getDelegatingGroup()).build()));

        desk.setDelegatingDesks(new ArrayList<>(desks.values()));

        // Init every Desks involved, with counts to 0

        desks.values().forEach(d -> {
            d.setDraftFoldersCount(0);
            d.setLateFoldersCount(0);
            d.setPendingFoldersCount(0);
            d.setFinishedFoldersCount(0);
            d.setRejectedFoldersCount(0);
        });

        // Parsing results starts

        desk.setDraftFoldersCount(countMap.getOrDefault(new DelegationRule(desk.getId(), DRAFT, null, null), 0));
        desk.setLateFoldersCount(countMap.getOrDefault(new DelegationRule(desk.getId(), LATE, null, null), 0));
        desk.setPendingFoldersCount(countMap.getOrDefault(new DelegationRule(desk.getId(), PENDING, null, null), 0));
        desk.setFinishedFoldersCount(countMap.getOrDefault(new DelegationRule(desk.getId(), FINISHED, null, null), 0));
        desk.setRejectedFoldersCount(countMap.getOrDefault(new DelegationRule(desk.getId(), REJECTED, null, null), 0));

        // Summing retrieved results may be necessary,
        // in case multiple metadata-based delegations are set on the same desk

        desk.getDelegationRules().forEach(r -> {
            Desk currentDesk = desks.get(r.getDelegatingGroup());

            currentDesk.setDraftFoldersCount(currentDesk.getDraftFoldersCount()
                    + countMap.getOrDefault(new DelegationRule(currentDesk.getId(), DRAFT, r.getMetadataKey(), r.getMetadataValue()), 0));

            currentDesk.setLateFoldersCount(currentDesk.getLateFoldersCount()
                    + countMap.getOrDefault(new DelegationRule(currentDesk.getId(), LATE, r.getMetadataKey(), r.getMetadataValue()), 0));

            currentDesk.setPendingFoldersCount(currentDesk.getPendingFoldersCount()
                    + countMap.getOrDefault(new DelegationRule(currentDesk.getId(), PENDING, r.getMetadataKey(), r.getMetadataValue()), 0));

            currentDesk.setFinishedFoldersCount(currentDesk.getFinishedFoldersCount()
                    + countMap.getOrDefault(new DelegationRule(currentDesk.getId(), FINISHED, r.getMetadataKey(), r.getMetadataValue()), 0));

            currentDesk.setRejectedFoldersCount(currentDesk.getRejectedFoldersCount()
                    + countMap.getOrDefault(new DelegationRule(currentDesk.getId(), REJECTED, r.getMetadataKey(), r.getMetadataValue()), 0));
        });
    }


}
