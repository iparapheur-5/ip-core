/*
 * iparapheur Core
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.ipcore.business.workflow;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.itextpdf.html2pdf.ConverterProperties;
import com.itextpdf.html2pdf.HtmlConverter;
import coop.libriciel.ipcore.model.auth.DeskRepresentation;
import coop.libriciel.ipcore.model.auth.User;
import coop.libriciel.ipcore.model.content.Document;
import coop.libriciel.ipcore.model.content.DocumentBuffer;
import coop.libriciel.ipcore.model.content.SignatureProof;
import coop.libriciel.ipcore.model.crypto.SignatureFormat;
import coop.libriciel.ipcore.model.database.*;
import coop.libriciel.ipcore.model.ipng.IpngFolderProofs;
import coop.libriciel.ipcore.model.ipng.IpngProofWrap;
import coop.libriciel.ipcore.model.pdfstamp.Stamp;
import coop.libriciel.ipcore.model.workflow.Action;
import coop.libriciel.ipcore.model.workflow.Folder;
import coop.libriciel.ipcore.model.workflow.Task;
import coop.libriciel.ipcore.services.auth.AuthServiceInterface;
import coop.libriciel.ipcore.services.content.ContentServiceInterface;
import coop.libriciel.ipcore.services.crypto.CryptoServiceInterface;
import coop.libriciel.ipcore.services.database.MetadataRepository;
import coop.libriciel.ipcore.services.database.SubtypeLayerRepository;
import coop.libriciel.ipcore.services.database.TypologyService;
import coop.libriciel.ipcore.services.ipng.IpngServiceInterface;
import coop.libriciel.ipcore.services.pdfstamp.PdfStampServiceInterface;
import coop.libriciel.ipcore.services.workflow.WorkflowServiceInterface;
import coop.libriciel.ipcore.utils.*;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.pdfbox.io.MemoryUsageSetting;
import org.apache.pdfbox.multipdf.PDFMergerUtility;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;
import org.springframework.util.StreamUtils;
import org.xml.sax.SAXException;

import javax.servlet.http.HttpServletResponse;
import javax.xml.stream.XMLStreamException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.MessageFormat;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Stream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import static coop.libriciel.ipcore.model.database.InternalMetadata.Constants.*;
import static coop.libriciel.ipcore.model.database.SubtypeLayerAssociation.*;
import static coop.libriciel.ipcore.model.database.TemplateType.DOCKET;
import static coop.libriciel.ipcore.model.pdfstamp.StampType.METADATA;
import static coop.libriciel.ipcore.model.workflow.Action.*;
import static coop.libriciel.ipcore.model.workflow.State.CURRENT;
import static coop.libriciel.ipcore.model.workflow.State.VALIDATED;
import static coop.libriciel.ipcore.services.content.AlfrescoService.PREMIS_NODE_FILE_NAME;
import static coop.libriciel.ipcore.services.workflow.WorkflowServiceInterface.*;
import static coop.libriciel.ipcore.utils.TextUtils.INTERNAL_PREFIX;
import static coop.libriciel.ipcore.utils.TextUtils.MESSAGE_BUNDLE;
import static io.micrometer.core.instrument.util.StringUtils.isNotEmpty;
import static java.lang.Integer.MAX_VALUE;
import static java.lang.System.currentTimeMillis;
import static java.nio.charset.StandardCharsets.UTF_8;
import static java.util.Arrays.asList;
import static java.util.Collections.*;
import static java.util.Comparator.*;
import static javax.xml.XMLConstants.*;
import static org.apache.commons.codec.digest.MessageDigestAlgorithms.SHA_256;
import static org.apache.commons.lang3.StringUtils.EMPTY;
import static org.apache.pdfbox.multipdf.PDFMergerUtility.DocumentMergeMode.OPTIMIZE_RESOURCES_MODE;
import static org.springframework.http.HttpHeaders.CONTENT_DISPOSITION;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;
import static org.springframework.http.HttpStatus.NOT_FOUND;
import static org.springframework.http.MediaType.APPLICATION_OCTET_STREAM_VALUE;
import static org.springframework.http.MediaType.APPLICATION_PDF;
import static org.springframework.util.ResourceUtils.CLASSPATH_URL_PREFIX;


@Log4j2
@Service
public class FolderBusinessService {

    public static final String TMP_PRINTDOC_PREFIX = "ip_print_doc_";
    public static final String TMP_PRINTDOC_SUFFIX = ".pdf";

    public static final String TMP_IPNG_PROOFS_PREFIX = "ip_ipng_proofs_";
    public static final String TMP_IPNG_PROOFS_SUFFIX = ".json";

    public static final int PDF_MERGER_MAX_MEMORY = 1024 * 1024 * 120;

    private static final String INTERNAL_FOLDER_FIELD = INTERNAL_PREFIX + "folder";
    private static final String INTERNAL_RESOURCE_BUNDLE_FIELD = INTERNAL_PREFIX + "resource_bundle";
    private static final String INTERNAL_RESOURCE_CRYPTO_UTILS_FIELD = INTERNAL_PREFIX + "crypto_utils";


    private @Value(CLASSPATH_URL_PREFIX + "templates/docket.ftl") Resource docketResource;
    private @Value(CLASSPATH_URL_PREFIX + "premis3.0.xsd") Resource premisXsd;


    private final AuthServiceInterface authService;
    private final Configuration configuration;
    private final ContentServiceInterface contentService;
    private final CryptoServiceInterface cryptoService;
    private final IpngServiceInterface ipngService;
    private final MetadataRepository metadataRepository;
    private final PdfStampServiceInterface pdfStampService;
    private final SubtypeLayerRepository subtypeLayerRepository;
    private final TypologyService typologyService;
    private final WorkflowServiceInterface workflowService;


    public FolderBusinessService(AuthServiceInterface authService,
                                 Configuration configuration,
                                 ContentServiceInterface contentService,
                                 CryptoServiceInterface cryptoService,
                                 IpngServiceInterface ipngService,
                                 MetadataRepository metadataRepository,
                                 PdfStampServiceInterface pdfStampService,
                                 SubtypeLayerRepository subtypeLayerRepository,
                                 TypologyService typologyService,
                                 WorkflowServiceInterface workflowService) {
        this.authService = authService;
        this.configuration = configuration;
        this.contentService = contentService;
        this.cryptoService = cryptoService;
        this.ipngService = ipngService;
        this.metadataRepository = metadataRepository;
        this.pdfStampService = pdfStampService;
        this.subtypeLayerRepository = subtypeLayerRepository;
        this.typologyService = typologyService;
        this.workflowService = workflowService;
    }


    public void downloadFolderAsZip(HttpServletResponse response, Tenant tenant, Folder folder, InputStream premisInputStream) {

        response.setContentType(APPLICATION_OCTET_STREAM_VALUE);

        String safeFolderName = FileUtils.cleanFileSystemForbiddenCharacters(folder.getName());
        response.setHeader(CONTENT_DISPOSITION, String.format("attachment;filename=%s_%s.zip", safeFolderName, folder.getContentId()));
        Folder folderWithHistory = Optional.ofNullable(workflowService.getFolder(folder.getId(), tenant.getId()))
                .orElseThrow(() -> new LocalizedStatusException(NOT_FOUND, "message.unknown_folder_id"));

        // To uncomment if we want to use the full print doc
        //   folderWithHistory.setDocumentList(folder.getDocumentList());
        //   Set<String> annexeIds = folder.getDocumentList().stream()
        //        .filter(doc -> !doc.isMainDocument())
        //        .map(Document::getId)
        //        .collect(Collectors.toSet());

        folderWithHistory.getMetadata().remove("action", "i_Parapheur_internal_archive");
        Path printDocTempFile = generatePrintDocAsTempFile(tenant, folderWithHistory, true, emptySet());

        List<Document> documentsToAdd = new ArrayList<>(folder.getDocumentList());

        if (folder.getSubtype() != null && !folder.getSubtype().isAnnexeIncluded()) {
            documentsToAdd.removeIf(document -> !document.isMainDocument() && document.getClass() != SignatureProof.class);
        }

        try (ZipOutputStream zippedOut = new ZipOutputStream(response.getOutputStream());
             InputStream premisInputStreamAutoCloseableInput = premisInputStream) {

            for (Document document : documentsToAdd) {
                log.debug("getArchiveAsZip documentName:{} nodeId:{}", document.getName(), document.getId());

                String safeDocumentName = FileUtils.cleanFileSystemForbiddenCharacters(document.getName());
                String innerFolderPath = String.format(
                        "%s%s",
                        document.isMainDocument() ? "Documents principaux/" : "Annexes/",
                        safeDocumentName
                );

                addZipEntry(zippedOut, innerFolderPath, document.getId());
            }

            // Premis file

            ZipEntry premisEntry = new ZipEntry(PREMIS_NODE_FILE_NAME);
            premisEntry.setTime(currentTimeMillis());
            zippedOut.putNextEntry(premisEntry);
            StreamUtils.copy(premisInputStreamAutoCloseableInput, zippedOut);
            zippedOut.closeEntry();

            // Print doc

            ResourceBundle messageResourceBundle = ResourceBundle.getBundle(MESSAGE_BUNDLE, Locale.getDefault());
            // to replace by 'printable_pdf_name' if in the end we want the full print doc
            String printDocFileNameTemplate = messageResourceBundle.getString("message.pdf_docket_name");
            addZipEntry(zippedOut, premisEntry, printDocTempFile, printDocFileNameTemplate, safeFolderName);

            // IPNG proofs

            if (isNotEmpty(folder.getMetadata().get(META_IPNG_PROOF_SENT_ID))) {
                Path ipngProofTempFile = createIpngProofFileForFolder(folder);
                String ipngProofFileNameTemplate = messageResourceBundle.getString("message.ipng_proof_name");
                addZipEntry(zippedOut, premisEntry, ipngProofTempFile, ipngProofFileNameTemplate, safeFolderName);
            }

            zippedOut.finish();

        } catch (IOException e) {
            throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, e, "message.error_building_zip_file");
        }
    }


    private void addZipEntry(ZipOutputStream zippedOut, String zipEntryName, String documentId) {
        ZipEntry entry = new ZipEntry(zipEntryName);
        entry.setTime(currentTimeMillis());
        try (InputStream documentinputStream = contentService.retrievePipedDocument(documentId)) {
            zippedOut.putNextEntry(entry);
            StreamUtils.copy(documentinputStream, zippedOut);
            zippedOut.closeEntry();
        } catch (IOException e) {
            throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, e, "message.error_building_zip_file");
        }
    }


    private void addZipEntry(ZipOutputStream zippedOut, ZipEntry entry, Path path, String template, String folderName) {
        String printDocFileName = MessageFormat.format(template, folderName);

        ZipEntry printDocEntry = new ZipEntry(printDocFileName);
        entry.setTime(currentTimeMillis());

        try {
            zippedOut.putNextEntry(printDocEntry);
            InputStream printDocInputStream = Files.newInputStream(path);
            StreamUtils.copy(printDocInputStream, zippedOut);
            printDocInputStream.close();
            zippedOut.closeEntry();
        } catch (IOException e) {
            throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, e, "message.error_building_zip_file");
        }
    }


    public Task prepareFinalTaskForPremisFile(List<Task> stepList, User currentUser) {

        Task finalTask = stepList
                .stream()
                .filter(a -> a.getState() == CURRENT)
                .filter(a -> asList(ARCHIVE, DELETE).contains(a.getAction()))
                .findFirst()
                .orElse(null);

        if (finalTask == null) {
            List<Task> validatedSteps = stepList
                    .stream()
                    .filter(a -> a.getState() == VALIDATED)
                    .filter(a -> a.getAction() != READ)
                    .toList();

            if (validatedSteps.isEmpty()) {
                throw new LocalizedStatusException(NOT_FOUND, "message.unknown_task_id");
            }

            finalTask = validatedSteps.get(validatedSteps.size() - 1);
        }

        if (finalTask.getDate() == null) {
            finalTask.setDate(new Date());
        }

        if (finalTask.getUser() == null) {
            finalTask.setUser(currentUser);
        }

        return finalTask;
    }


    public void prepareChecksumDataForPremisFile(List<Document> documents) {
        documents
                .stream()
                .filter(d -> StringUtils.isEmpty(d.getChecksumValue()))
                // Retrieving the full document checksum
                .forEach(d -> {
                    try (InputStream ios = contentService.retrievePipedDocument(d.getId())) {
                        d.setChecksumAlgorithm(SHA_256);
                        d.setChecksumValue(DigestUtils.sha256Hex(ios).toUpperCase());
                    } catch (IOException e) {
                        throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, e, "message.error_computing_document_checksum");
                    }
                });
    }


    public String generatePremisContent(Folder folder) {
        SignatureFormat signatureFormat = cryptoService.getSignatureFormat(folder.getType(), folder.getDocumentList());

        try (ByteArrayOutputStream premisOutputStream = XmlUtils.folderToPremisXml(folder, signatureFormat, contentService::retrievePipedDocument)) {

            // TODO : Stream this, instead of putting it in memory.
            // Low priority, though. Since it should never be a very large String.
            // We should manage to get the HTTP outputStream from the WebClient, and write in it directly.
            String premis = premisOutputStream.toString(UTF_8);
            premis = XmlUtils.prettyPrint(premis);

            // Validation

            SchemaFactory schemaFactory = SchemaFactory.newInstance(W3C_XML_SCHEMA_NS_URI);
            schemaFactory.setProperty(ACCESS_EXTERNAL_DTD, EMPTY);
            schemaFactory.setProperty(ACCESS_EXTERNAL_SCHEMA, EMPTY);
            javax.xml.validation.Schema schema = schemaFactory.newSchema(new StreamSource(premisXsd.getInputStream()));
            Validator validator = schema.newValidator();
            validator.validate(new StreamSource(new StringReader(premis)));

            return premis;

        } catch (IOException | XMLStreamException | TransformerException | SAXException e) {
            throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, e, "message.error_generating_PREMIS");
        }
    }


    @NotNull
    public Path generatePrintDocAsTempFile(@NotNull Tenant tenant, Folder folder, boolean includeDocket, Set<String> finalAnnexesIds) {

        // Retrieving target document Ids

        List<String> printableMainDocIds = folder.getDocumentList().stream()
                .filter(Document::isMainDocument)
                .sorted(comparing(Document::getIndex, nullsLast(naturalOrder())))
                .map(d -> d.getMediaType().isCompatibleWith(APPLICATION_PDF) ? d.getId() : d.getPdfVisualId())
                .filter(StringUtils::isNotEmpty)
                .toList();

        List<String> printableAnnexesDocIds = folder.getDocumentList().stream()
                .filter(d -> !d.isMainDocument())
                .filter(d -> finalAnnexesIds.contains(d.getId()))
                .sorted(comparing(Document::getIndex, nullsLast(naturalOrder())))
                .map(d -> d.getMediaType().isCompatibleWith(APPLICATION_PDF) ? d.getId() : d.getPdfVisualId())
                .filter(StringUtils::isNotEmpty)
                .toList();

        // Stamp retrieving

        List<SubtypeLayer> layers = subtypeLayerRepository
                .findBySubtype_Id(folder.getSubtype().getId(), PageRequest.of(0, MAX_VALUE))
                .getContent();

        List<Stamp> mainDocStampList = layers.stream()
                .filter(sl -> asList(ALL, MAIN_DOCUMENT).contains(sl.getAssociation()))
                .flatMap(sl -> sl.getLayer().getStampList().stream())
                .toList();

        List<Stamp> annexeStampList = layers.stream()
                .filter(sl -> asList(ALL, ANNEXE).contains(sl.getAssociation()))
                .flatMap(sl -> sl.getLayer().getStampList().stream())
                .toList();

        Stream.concat(mainDocStampList.stream(), annexeStampList.stream())
                .forEach(stamp -> computeStampValue(tenant.getId(), folder, stamp));

        // Building result

        AtomicReference<Path> prevTempFile = new AtomicReference<>();
        AtomicReference<Path> newTempFile = new AtomicReference<>();
        try (ByteArrayInputStream docketPdfInputStream = includeDocket ? generateDocketFromFolder(tenant, folder) : null) {

            if (docketPdfInputStream != null) {
                mergeStreamToPdf(prevTempFile, newTempFile, docketPdfInputStream);
            }

            Stream.concat(printableMainDocIds.stream(), printableAnnexesDocIds.stream())
                    .map(id -> {
                        DocumentBuffer docBuff = contentService.retrieveContent(id);
                        if (printableMainDocIds.contains(id)) {docBuff.setMainDocument(true);}
                        return docBuff;
                    })
                    .map(doc -> {
                        if (doc.isMainDocument() && CollectionUtils.isNotEmpty(mainDocStampList)) {
                            return pdfStampService.createStamp(doc, mainDocStampList, contentService::retrieveContent);
                        } else if (!doc.isMainDocument() && CollectionUtils.isNotEmpty(annexeStampList)) {
                            return pdfStampService.createStamp(doc, annexeStampList, contentService::retrieveContent);
                        } else {
                            return doc;
                        }
                    })
                    .map(b -> {
                        try {
                            return RequestUtils.bufferToInputStream(b);
                        } catch (IOException e) {
                            throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.cannot_reach_content_service");
                        }
                    })
                    .forEach(sourceInputStream -> {
                        try (InputStream src = sourceInputStream) {
                            mergeStreamToPdf(prevTempFile, newTempFile, src);
                        } catch (IOException e) {
                            log.debug("error creating intermediate merged pdf file : ", e);
                            throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.cannot_reach_content_service");
                        }
                    });

        } catch (IOException e) {
            log.debug("Error merging pdf : ", e);
            throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, e, "message.error_merging_pdf");
        }
        return prevTempFile.get();
    }


    public @NotNull ByteArrayInputStream generateDocketFromFolder(Tenant tenant, Folder folder) {
        authService.updateFolderReferencedDeskNames(singletonList(folder));
        authService.updateFolderReferencedUserNames(singletonList(folder));
        typologyService.updateTypology(singletonList(folder));
        folder.getStepList().removeIf(task -> Action.isSecondary(task.getAction()));

        // FIXME: Those 2 should have been cleaned earlier
        folder.getMetadata().entrySet().removeIf(entry -> StringUtils.startsWith(entry.getKey(), INTERNAL_PREFIX));
        folder.getMetadata().entrySet().removeIf(entry -> StringUtils.startsWith(entry.getKey(), "workflow_internal_"));

        try (InputStream docketTemplateInputStream = tenant.getCustomTemplates().containsKey(DOCKET)
                                                     ? RequestUtils.bufferToInputStream(contentService.getCustomTemplate(tenant, DOCKET))
                                                     : docketResource.getInputStream()) {
            return generateDocketPdfInputStream(folder, docketTemplateInputStream);
        } catch (IOException exception) {
            throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, exception, "message.error_creating_docket");
        }
    }


    private void computeStampValue(String tenantId, Folder folder, Stamp stamp) {
        log.debug("computeStampValue type:{}", stamp.getType());

        // Default cases

        if (stamp.getComputedValue() != null) {
            return;
        }

        if (stamp.getType() != METADATA) {
            return;
        }

        // Compute the internal metadata values

        String metadataId = stamp.getValue();
        log.debug("metadataId:{}", metadataId);

        Metadata metadataDefinition = metadataRepository
                .findByIdAndTenant_IdOrTenantLess(metadataId, tenantId)
                .orElseThrow(() -> new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.unknown_metadata_id"));

        Optional<Task> lastVisa = Optional.ofNullable(FolderUtils.getLastPerformedTask(folder, Set.of(START, VISA, SEAL, SECOND_OPINION, SIGNATURE)));
        Optional<Task> lastSignature = Optional.ofNullable(FolderUtils.getLastPerformedTask(folder, Set.of(SIGNATURE)));
        Optional<Folder> optionalFolder = Optional.ofNullable(folder);
        Optional<String> computedValue = switch (metadataDefinition.getKey()) {
            case FOLDER_NAME_KEY -> optionalFolder.map(Folder::getName);
            case FOLDER_TYPE_KEY -> optionalFolder.map(Folder::getType).map(Type::getName);
            case FOLDER_SUBTYPE_KEY -> optionalFolder.map(Folder::getSubtype).map(Subtype::getName);
            case LAST_VISA_DESK_KEY -> lastVisa.map(Task::getDesks).orElse(emptyList()).stream().findFirst().map(DeskRepresentation::getName);
            case LAST_VISA_USER_KEY -> lastVisa.map(Task::getUser).map(User::getUserName);
            case LAST_VISA_DATE_KEY -> lastVisa.map(Task::getDate).map(TextUtils::prettyPrintDateTime);
            case LAST_SIGNATURE_DESK_KEY -> lastSignature.map(Task::getDate).map(TextUtils::prettyPrintDateTime);
            case LAST_SIGNATURE_USER_KEY -> lastSignature.map(Task::getUser).map(User::getUserName);
            case LAST_SIGNATURE_DATE_KEY -> lastSignature.map(Task::getDate).map(TextUtils::prettyPrintDateTime);
            case LAST_SIGNATURE_DELEGATED_BY_KEY -> lastSignature.map(Task::getDelegatedByDesk).map(DeskRepresentation::getName);
            case LAST_SIGNATURE_HASH_KEY -> Optional.of(EMPTY); // FIXME
            default -> optionalFolder.map(Folder::getMetadata).map(metaList -> metaList.get(metadataDefinition.getKey()));
        };

        stamp.setComputedValue(computedValue.orElse(EMPTY));
    }


    public ByteArrayInputStream generateDocketPdfInputStream(@NotNull Folder folder, @NotNull InputStream docketResourceInputStream) {

        Map<String, Object> fields = new HashMap<>();
        fields.put(INTERNAL_FOLDER_FIELD, folder);
        fields.put(INTERNAL_RESOURCE_BUNDLE_FIELD, ResourceBundle.getBundle(MESSAGE_BUNDLE, Locale.getDefault()));
        // We can only pass objects in here. Not classes.
        // Either we create a lambda for every method, or we instantiate the class...
        // The second one makes less code.
        //noinspection InstantiationOfUtilityClass
        fields.put(INTERNAL_RESOURCE_CRYPTO_UTILS_FIELD, new CryptoUtils());

        try (ByteArrayOutputStream fos = new ByteArrayOutputStream()) {

            Template templateBody = new Template(
                    docketResource.getFilename(),
                    new InputStreamReader(docketResourceInputStream),
                    configuration
            );

            String docket = FreeMarkerTemplateUtils.processTemplateIntoString(templateBody, fields);
            ConverterProperties converterProperties = new ConverterProperties();
            converterProperties.setBaseUri("classpath:/");

            HtmlConverter.convertToPdf(docket, fos, converterProperties);
            return new ByteArrayInputStream(fos.toByteArray());

        } catch (IOException | TemplateException e) {
            throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, e, "message.error_creating_docket");
        }
    }


    public void mergeStreamToPdf(AtomicReference<Path> existingTempFile,
                                 AtomicReference<Path> targetTempFile,
                                 InputStream sourceInputStream) throws IOException {
        targetTempFile.set(Files.createTempFile(TMP_PRINTDOC_PREFIX, TMP_PRINTDOC_SUFFIX));

        try (OutputStream targetOs = Files.newOutputStream(targetTempFile.get())) {

            PDFMergerUtility pdfMerger = new PDFMergerUtility();
            pdfMerger.setDocumentMergeMode(OPTIMIZE_RESOURCES_MODE);
            if (existingTempFile.get() != null) {
                Path toRemove = Path.of(existingTempFile.get().toUri());
                try (InputStream existingIs = Files.newInputStream(existingTempFile.get())) {
                    pdfMerger.addSource(existingIs);
                    pdfMerger.addSource(sourceInputStream);
                    pdfMerger.setDestinationStream(targetOs);
                    pdfMerger.mergeDocuments(MemoryUsageSetting.setupMixed(PDF_MERGER_MAX_MEMORY));
                } finally {
                    Files.delete(toRemove);
                }
            } else {
                pdfMerger.addSource(sourceInputStream);
                pdfMerger.setDestinationStream(targetOs);
                pdfMerger.mergeDocuments(MemoryUsageSetting.setupMixed(PDF_MERGER_MAX_MEMORY));
            }
            existingTempFile.set(targetTempFile.get());
            sourceInputStream.close();
        }
    }


    @NotNull
    public List<IpngFolderProofs> gatherIpngProofsForFolder(Folder folder) {
        Map<String, String> metaMap = folder.getMetadata();
        if (StringUtils.isEmpty(metaMap.get(META_IPNG_PROOF_SENT_ID))) {
            throw new LocalizedStatusException(NOT_FOUND, "message.no_ipng_proof_on_folder");
        }

        // TODO handle multiple IPNG tasks - we don't handle with the current metadata system
        List<IpngFolderProofs> result = new ArrayList<>();

        IpngFolderProofs proofs = IpngFolderProofs.builder()
                .receiverDbxId(metaMap.get(META_IPNG_RECIPIENT_DBX_ID))
                .receiverEntityId(metaMap.get(META_IPNG_RECIPIENT_ENTITY_ID))
                .proofSentId(metaMap.get(META_IPNG_PROOF_SENT_ID))
                .proofReceiptId(metaMap.get(META_IPNG_PROOF_RECEIPT_ID))
                .proofResponseId(metaMap.get(META_IPNG_RESPONSE_ID))
                .build();


        try {
            IpngProofWrap sentProof = ipngService.getProof(proofs.getProofSentId());
            proofs.setProofSent(sentProof);
        } catch (IpInternalException e) {
            log.debug("Error retrieving sent proof for the folder : ", e);
            throw new LocalizedStatusException(NOT_FOUND, "message.no_ipng_send_proof_on_folder");
        }

        try {
            IpngProofWrap proofReceipt = ipngService.getProof(proofs.getProofReceiptId());
            proofs.setProofReceipt(proofReceipt);
        } catch (IpInternalException e) {
            log.debug("Error retrieving proof receipt for the folder : ", e);
            throw new LocalizedStatusException(NOT_FOUND, "message.no_ipng_receipt_proof_on_folder");
        }

        try {
            IpngProofWrap responseProof = ipngService.getProof(proofs.getProofResponseId());
            proofs.setProofResponse(responseProof);
        } catch (IpInternalException e) {
            log.debug("Error retrieving response proof for folder : ", e);
            throw new LocalizedStatusException(NOT_FOUND, "message.no_ipng_response_proof_on_folder");
        }

        result.add(proofs);
        return result;
    }


    @NotNull
    public Path createIpngProofFileForFolder(Folder folder) throws IOException {

        List<IpngFolderProofs> proofList = this.gatherIpngProofsForFolder(folder);
        Path ipngProofsTempFile = Files.createTempFile(TMP_IPNG_PROOFS_PREFIX, TMP_IPNG_PROOFS_SUFFIX);
        try (OutputStream os = Files.newOutputStream(ipngProofsTempFile)) {
            ObjectMapper om = new ObjectMapper();
            om.writeValue(os, proofList);
        }
        return ipngProofsTempFile;
    }

}
