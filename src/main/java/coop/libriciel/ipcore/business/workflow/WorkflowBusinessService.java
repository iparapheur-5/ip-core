/*
 * iparapheur Core
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.ipcore.business.workflow;

import coop.libriciel.ipcore.controller.DeskController;
import coop.libriciel.ipcore.model.auth.Desk;
import coop.libriciel.ipcore.model.auth.DeskRepresentation;
import coop.libriciel.ipcore.model.content.Document;
import coop.libriciel.ipcore.model.content.DocumentBuffer;
import coop.libriciel.ipcore.model.content.CreateFolderRequest;
import coop.libriciel.ipcore.model.database.*;
import coop.libriciel.ipcore.model.workflow.Folder;
import coop.libriciel.ipcore.model.workflow.Task;
import coop.libriciel.ipcore.model.workflow.WorkflowDefinition;
import coop.libriciel.ipcore.model.workflow.WorkflowDefinitionDto;
import coop.libriciel.ipcore.model.workflow.requests.SimpleTaskParams;
import coop.libriciel.ipcore.model.workflow.requests.StartWorkflowResponse;
import coop.libriciel.ipcore.services.auth.AuthServiceInterface;
import coop.libriciel.ipcore.services.content.ContentServiceInterface;
import coop.libriciel.ipcore.services.database.TypologyService;
import coop.libriciel.ipcore.services.groovy.GroovyResultCatcher;
import coop.libriciel.ipcore.services.groovy.GroovyService;
import coop.libriciel.ipcore.services.stats.StatsServiceInterface;
import coop.libriciel.ipcore.services.workflow.WorkflowServiceInterface;
import coop.libriciel.ipcore.utils.LocalizedStatusException;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.regex.Matcher;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static coop.libriciel.ipcore.model.crypto.SignatureFormat.PKCS7;
import static coop.libriciel.ipcore.model.crypto.SignatureFormat.XADES_DETACHED;
import static coop.libriciel.ipcore.model.workflow.Action.*;
import static coop.libriciel.ipcore.model.workflow.State.CURRENT;
import static coop.libriciel.ipcore.model.workflow.State.UPCOMING;
import static coop.libriciel.ipcore.model.workflow.WorkflowDefinition.*;
import static coop.libriciel.ipcore.services.workflow.WorkflowServiceInterface.*;
import static coop.libriciel.ipcore.utils.TextUtils.RESERVED_PREFIX;
import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static java.util.stream.Collectors.toSet;
import static org.springframework.http.HttpStatus.*;


@Log4j2
@Service
public class WorkflowBusinessService {


    // <editor-fold desc="Beans">


    private final AuthServiceInterface authService;
    private final ContentServiceInterface contentService;
    private final GroovyService groovyService;
    private final StatsServiceInterface statsService;
    private final TypologyService typologyService;
    private final WorkflowServiceInterface workflowService;
    private final ModelMapper modelMapper;


    public WorkflowBusinessService(AuthServiceInterface authService,
                                   ContentServiceInterface contentService,
                                   GroovyService groovyService,
                                   StatsServiceInterface statsService,
                                   TypologyService typologyService,
                                   WorkflowServiceInterface workflowService, ModelMapper modelMapper) {
        this.authService = authService;
        this.contentService = contentService;
        this.groovyService = groovyService;
        this.statsService = statsService;
        this.typologyService = typologyService;
        this.workflowService = workflowService;
        this.modelMapper = modelMapper;
    }


    // </editor-fold desc="Beans">


    /**
     * @throws LocalizedStatusException if all the actor desks of the next step have been deleted, or do not have any owner.
     */
    public void checkForNullOrEmptyDesksInNextStep(String tenantId, Folder folder) {
        log.debug("checkForNullDesksInNextStep");

        List<Desk> populatedDesks = new ArrayList<>();
        Task nextTask = folder.getStepList()
                .stream()
                .filter(step -> step.getState().equals(UPCOMING))
                .findFirst()
                .orElseThrow(() -> new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.cannot_fetch_next_step_desks"));

        nextTask.getDesks().stream()
                .map(desk -> authService.findDeskByIdNoException(tenantId, desk.getId()))
                .filter(Objects::nonNull)
                .forEach(populatedDesks::add);

        // TODO if ParallelType == AND && one null or more -> throw exception
        if (CollectionUtils.isEmpty(populatedDesks)) {
            throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.cannot_fetch_next_step_desks");
        }

        boolean noUserInAnyDesk = populatedDesks.stream()
                .map(desk -> authService.listUsersFromDesk(tenantId, desk.getId(), 1, 1))
                .allMatch(usersInDesk -> usersInDesk.getData().size() == 0);

        if (noUserInAnyDesk) {
            throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.no_user_in_next_step_desks");
        }
    }


    public @NotNull StartWorkflowResponse doStartWorkflow(String tenantId,
                                                          Folder folder,
                                                          String deskId,
                                                          SimpleTaskParams simpleTaskParams) {

        if (asList(XADES_DETACHED, PKCS7).contains(folder.getType().getSignatureFormat())) {

            long signatureStepsCount = folder.getStepList().stream().filter(s -> asList(SIGNATURE, EXTERNAL_SIGNATURE, SEAL).contains(s.getAction())).count();
            long documentCount = folder.getDocumentList().stream().filter(Document::isMainDocument).count();
            long signaturesCount = signatureStepsCount * documentCount;

            if (signaturesCount > DeskController.MAX_DETACHED_SIGNATURES_GENERATED_PER_FOLDER) {
                throw new LocalizedStatusException(
                        INSUFFICIENT_STORAGE,
                        "message.this_workflow_will_generates_n_detached_signatures_for_a_limit_of_m",
                        signaturesCount, DeskController.MAX_DETACHED_SIGNATURES_GENERATED_PER_FOLDER
                );
            }
        }

        // Start

        Task startTask = folder.getStepList()
                .stream()
                .filter(t -> t.getAction() == START)
                .filter(t -> t.getState() == CURRENT)
                .filter(t -> t.getDate() == null)
                .findFirst()
                .orElseThrow(() -> new LocalizedStatusException(CONFLICT, "message.cannot_start_this_folder"));

        checkForNullOrEmptyDesksInNextStep(tenantId, folder);

        // TODO this should be either calculated at Folder construction time, or in a shared utils, not ad-hoc code
        boolean hasCreationWorkflow = folder.getStepList().stream()
                .filter(t -> t.getAction() != START)
                .filter(t -> t.getState() == UPCOMING)
                .filter(t -> t.getDate() == null)
                .findFirst()
                .map(Task::getWorkflowIndex)
                .filter(workflowIdx -> workflowIdx < 2).isPresent();
        if (!hasCreationWorkflow) {
            typologyService.verifyFolderMetadataForValidationWorkflow(tenantId, folder);
        }

        // Pre-start checks

        // FIXME : Evaluate an overridden workflow definition id ?

        // Starting workflow

        workflowService.performTask(START, startTask, deskId, folder, null, null, null, null, simpleTaskParams);
        // FIXME : statsService.registerSimpleAction(tenant, START, deskId, folderId);

        log.debug("Folder started {}", folder);
        return new StartWorkflowResponse(folder.getId());
    }


    public WorkflowDefinitionDto computeWorkflowDefinitionFromScriptResult(@NotNull Optional<GroovyResultCatcher> scriptResultWrapped,
                                                                           @NotNull String tenantId,
                                                                           @NotNull String deskId) {
        Map<Integer, String> variableDesksIdsMap = new HashMap<>();

        if (scriptResultWrapped.isEmpty() || StringUtils.isEmpty(scriptResultWrapped.get().getWorkflowDefinitionId())) return null;

        GroovyResultCatcher scriptResult = scriptResultWrapped.get();

        WorkflowDefinition workflowDefinition = workflowService
                .getWorkflowDefinitionByKey(tenantId, scriptResult.getWorkflowDefinitionId(), null)
                .orElseThrow(() -> new LocalizedStatusException(NOT_FOUND, "message.unknown_workflow_definition_key"));

        log.debug("evaluateWorkflowSelectionScript overriddenWorkflow:{}", workflowDefinition.getName());

        groovyService.updateVariableDeskMapFromScriptResult(tenantId, scriptResult, workflowDefinition, variableDesksIdsMap);

        // maybe all this, but like, in workflowService?
        workflowService.substitutePlaceholdersToIndexedPlaceholders(workflowDefinition);
        Map<String, String> placeholdersMapping = workflowService.computePlaceholderDesksConcreteValues(
                workflowDefinition,
                deskId,
                variableDesksIdsMap
        );
        List<DeskRepresentation> workflowDesks = workflowDefinition.getSteps().stream()
                .flatMap(s -> s.getValidatingDesks().stream())
                .collect(Collectors.toList());
        workflowDesks.add(workflowDefinition.getFinalDesk());

        workflowDesks.forEach(d -> workflowService.mapIndexedPlaceholdersToActualDesk(d, deskId, placeholdersMapping));
        authService.updateDeskNames(workflowDesks);

        return modelMapper.map(workflowDefinition, WorkflowDefinitionDto.class);
    }


    public @NotNull Folder prepareDraftFromParams(Tenant tenant,
                                                  Desk desk,
                                                  CreateFolderRequest draftFolderParams,
                                                  String folderName,
                                                  @NotNull Map<Integer, String> variableDeskIds) {

        Folder result = new Folder();
        result.setName(folderName);
        result.setLegacyId(draftFolderParams.getLegacyId());
        result.setDueDate(draftFolderParams.getDueDate());
        result.setVisibility(draftFolderParams.getVisibility());
        result.setOriginDesk(new DeskRepresentation(desk.getId(), desk.getName()));
        result.setType(Optional.ofNullable(draftFolderParams.getTypeId()).map(i -> Type.builder().id(i).build()).orElse(null));
        result.setSubtype(Optional.ofNullable(draftFolderParams.getSubtypeId()).map(i -> Subtype.builder().id(i).build()).orElse(null));
        result.setMetadata(new HashMap<>());

        // Setup Typology

        typologyService.updateTypology(singletonList(result));

        if (result.getSubtype() != null) {
            result.setCreationWorkflowDefinitionKey(result.getSubtype().getCreationWorkflowId());
            result.setValidationWorkflowDefinitionKey(result.getSubtype().getValidationWorkflowId());
        }

        // Integrity checks

        checkAndCopyMetadataFromParams(draftFolderParams, result);

        groovyService.updateWorkflowDataFromScript(tenant.getId(), result, variableDeskIds);
        return result;
    }


    public void checkAndCopyMetadataFromParams(CreateFolderRequest draftFolderParams, Folder result) {
        if (result.getSubtype().getSubtypeMetadataList().stream().anyMatch(SubtypeMetadata::isMandatory)
                && MapUtils.isEmpty(draftFolderParams.getMetadata())) {
            log.debug("checkAndCopyMetadataFromParams - A mandatory metadata was not filled, abort");
            throw new LocalizedStatusException(NOT_ACCEPTABLE, "message.missing_mandatory_metadata");
        }

        if (MapUtils.isNotEmpty(draftFolderParams.getMetadata())) {

            List<SubtypeMetadata> subList = result.getSubtype().getSubtypeMetadataList();
            boolean everyKeyWasFoundInSubtype = draftFolderParams.getMetadata()
                    .keySet()
                    .stream()
                    .filter(key -> !key.startsWith(RESERVED_PREFIX))
                    .allMatch(key -> subList
                            .stream()
                            .anyMatch(subtypeMetadata -> StringUtils.equals(subtypeMetadata.getMetadata().getKey(), key))
                    );

            if (!everyKeyWasFoundInSubtype) {
                log.debug("createDraftFolder - A metadata passed does not belong to the subtype, abort");
                throw new LocalizedStatusException(NOT_ACCEPTABLE, "message.metadata_not_in_subtype");
            }

            typologyService.verifyMetadataForDraftOrCreation(draftFolderParams.getMetadata(), subList);

            result.setMetadata(draftFolderParams.getMetadata());
        }
    }


    public Folder createDraftFolderFromInternalTask(@NotNull String deskId,
                                                    @NotNull String folderName,
                                                    @NotNull Tenant tenant,
                                                    @NotNull List<DocumentBuffer> documentList,
                                                    @NotNull CreateFolderRequest draftFolderParams) {


        Desk desk = Desk.builder().id(deskId).build();

        Map<Integer, String> variableDesksIdsMap = new HashMap<>();
        if (draftFolderParams.getVariableDesksIds() != null) {
            draftFolderParams.getVariableDesksIds().forEach((key, value) -> variableDesksIdsMap.put(Integer.parseInt(key), value));
        }

        Folder result = prepareDraftFromParams(tenant, desk, draftFolderParams, folderName, variableDesksIdsMap);

        // Creating Folder

        String contentId = contentService.createFolder(tenant);
        result.setContentId(contentId);

        for (DocumentBuffer doc : documentList) {
            String documentId = contentService.createDocument(tenant.getId(), result, doc);
            doc.setId(documentId);
            result.getDocumentList().add(doc);
        }

        // Upload to Flowable

        log.info("type:{} subtype:{} creationW:{} validationW:{}", result.getType(), result.getSubtype(),
                result.getCreationWorkflowDefinitionKey(), result.getValidationWorkflowDefinitionKey());

        String createdFolderId = workflowService.createDraftWorkflow(tenant.getId(), result, variableDesksIdsMap).getId();
        result.setId(createdFolderId);

        // Sending back result

        log.debug("CreateFolder folder:{}", result);
        statsService.registerFolderAction(tenant, CREATE, result, result.getOriginDesk(), null);
        return result;
    }


    public WorkflowDefinition retrieveDesksNames(@NotNull Tenant tenant, @NotNull WorkflowDefinition workflowDefinition) {
        populateDesksAndValidators(workflowDefinition);
        return workflowDefinition;
    }


    private void populateDesksAndValidators(WorkflowDefinition workflowDefinition) {

        Set<String> referencedDeskIds = workflowDefinition.getSteps().stream()
                .flatMap(d -> Stream.concat(
                        Optional.ofNullable(d.getValidatingDesks()).orElse(emptyList()).stream(),
                        Optional.ofNullable(d.getNotifiedDesks()).orElse(emptyList()).stream()
                ))
                .filter(Objects::nonNull)
                .map(DeskRepresentation::getId)
                .filter(StringUtils::isNotEmpty)
                .filter(id -> !isVariableDeskIdPlaceholder(id))
                .collect(toSet());

        Optional.ofNullable(workflowDefinition.getFinalDesk())
                .map(DeskRepresentation::getId)
                .filter(StringUtils::isNotEmpty)
                .filter(id -> !isVariableDeskIdPlaceholder(id))
                .ifPresent(referencedDeskIds::add);

        Map<String, String> referencedDeskInfos = authService.getDeskNames(referencedDeskIds);

        workflowDefinition.getSteps().stream()
                .flatMap(d -> Stream.concat(
                        Optional.ofNullable(d.getValidatingDesks()).orElse(emptyList()).stream(),
                        Optional.ofNullable(d.getNotifiedDesks()).orElse(emptyList()).stream()
                ))
                .filter(Objects::nonNull)
                .filter(entity -> StringUtils.isNotEmpty(entity.getId()))
                .filter(entity -> !replaceVariablePlaceholdersByInternalConstant(entity))
                .forEach(entity -> entity.setName(referencedDeskInfos.get(entity.getId())));

        Optional.ofNullable(workflowDefinition.getFinalDesk())
                .filter(entity -> StringUtils.isNotEmpty(entity.getId()))
                .filter(entity -> !replaceVariablePlaceholdersByInternalConstant(entity))
                .ifPresent(entity -> entity.setName(referencedDeskInfos.get(entity.getId())));
    }


    public @Nullable WorkflowDefinition getWorkflowDefinitionByKey(@NotNull Tenant tenant, @NotNull String key, @Nullable String originDeskId) {

        WorkflowDefinition workflowDefinition = workflowService
                .getWorkflowDefinitionByKey(tenant.getId(), key, originDeskId)
                .orElseThrow(() -> new LocalizedStatusException(NOT_FOUND, "message.unknown_workflow_definition_id"));

        populateDesksAndValidators(workflowDefinition);
        log.debug("getWorkflowDefinitionByKey {}", workflowDefinition);

        return workflowDefinition;
    }


    private boolean replaceVariablePlaceholdersByInternalConstant(DeskRepresentation e) {

        Matcher matcher = META_EMITTER_PATTERN.matcher(e.getId());
        if (matcher.matches()) {
            e.setId(EMITTER_ID_PLACEHOLDER);
            return true;
        }

        matcher = META_BOSSOF_PATTERN.matcher(e.getId());
        if (matcher.matches()) {
            e.setId(BOSS_OF_ID_PLACEHOLDER);
            return true;
        }

        matcher = META_VARIABLEDESK_PATTERN.matcher(e.getId());
        if (matcher.matches()) {
            e.setId(VARIABLE_DESK_ID_PLACEHOLDER);
            return true;
        }

        return false;
    }


    private boolean isVariableDeskIdPlaceholder(String id) {

        if (META_EMITTER_PATTERN.matcher(id).matches()) {
            return true;
        }

        if (META_BOSSOF_PATTERN.matcher(id).matches()) {
            return true;
        }

        if (META_VARIABLEDESK_PATTERN.matcher(id).matches()) {
            return true;
        }

        return false;
    }


}
