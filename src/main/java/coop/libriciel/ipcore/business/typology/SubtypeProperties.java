/*
 * iparapheur Core
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.ipcore.business.typology;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.ConstructorBinding;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.validation.annotation.Validated;


@Data
@Validated
@AllArgsConstructor
@ConstructorBinding
@EnableConfigurationProperties
@ConfigurationProperties(prefix = "services.business.subtype")
public class SubtypeProperties {


    /**
     * We do not want 64kb scripts anymore, so we forbid those by default.
     * Nothing good was ever made through a big script. They always brings pain and drama when its maintainer leaves.
     * Yet, for retro-compatibility issues, these can be fully unlocked on demand, through a (painful) parameter.
     */
    private boolean unlockSelectionScriptFullSize;


}
