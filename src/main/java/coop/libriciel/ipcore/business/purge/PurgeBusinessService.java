/*
 * iparapheur Core
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.ipcore.business.purge;

import coop.libriciel.ipcore.model.database.Tenant;
import coop.libriciel.ipcore.model.workflow.Folder;
import coop.libriciel.ipcore.services.content.ContentServiceInterface;
import coop.libriciel.ipcore.services.database.TenantRepository;
import coop.libriciel.ipcore.services.workflow.WorkflowServiceInterface;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.validation.Valid;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;

import static org.springframework.data.domain.Pageable.unpaged;


@Log4j2
@Service
public class PurgeBusinessService {

    private final WorkflowServiceInterface workflowService;
    private final ContentServiceInterface contentService;
    private final TenantRepository tenantRepository;
    private final PurgeProperties properties;


    @Autowired
    public PurgeBusinessService(@Valid PurgeProperties properties,
                                WorkflowServiceInterface workflowService,
                                ContentServiceInterface contentService,
                                TenantRepository tenantRepository) {
        this.workflowService = workflowService;
        this.tenantRepository = tenantRepository;
        this.properties = properties;
        this.contentService = contentService;
    }


    @Scheduled(cron = "${services.business.purge.frequency:-}")
    public void purge() {
        log.info("Starting global purge of {} or more days old archived folders.", properties.getDaysBeforeDelete());

        this.tenantRepository
                .findAll(unpaged())
                .get()
                .forEach(tenant -> {
                    List<? extends Folder> foldersToDelete = this.getOldArchives(tenant.getId());

                    log.info(
                            "Found {} {} or more days old archived folders on tenant {}.",
                            foldersToDelete.size(),
                            properties.getDaysBeforeDelete(),
                            tenant.getId()
                    );

                    foldersToDelete.forEach(folder -> {
                        try {
                            contentService.deleteFolder(folder);
                            workflowService.deleteArchive(folder.getId());
                        } catch (Exception e) {
                            log.error(
                                    "An error occurred during the purge of the folder {}, error detail : {}",
                                    folder.getId(),
                                    e.getMessage()
                            );
                        }
                    });
                });
    }


    private List<? extends Folder> getOldArchives(String tenantId) {
        return this.workflowService
                .getArchives(
                        tenantId,
                        unpaged(),
                        LocalDateTime.now().minusDays(properties.getDaysBeforeDelete()).atZone(ZoneId.systemDefault()).toInstant().toEpochMilli()
                )
                .getData();
    }
}
