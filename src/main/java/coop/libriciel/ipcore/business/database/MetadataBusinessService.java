/*
 * iparapheur Core
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.ipcore.business.database;

import coop.libriciel.ipcore.model.auth.requests.DeskDto;
import coop.libriciel.ipcore.model.database.MetadataRepresentation;
import coop.libriciel.ipcore.services.database.MetadataRepository;
import lombok.extern.log4j.Log4j2;
import org.jetbrains.annotations.Nullable;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.Optional;

import static java.util.Comparator.naturalOrder;
import static java.util.Comparator.nullsLast;
import static org.springframework.data.domain.Pageable.unpaged;


@Log4j2
@Service
public class MetadataBusinessService {


    public static final Comparator<MetadataRepresentation> METADATA_REPRESENTATION_NAME_COMPARATOR = Comparator
            .comparing(MetadataRepresentation::getName, nullsLast(naturalOrder()))
            .thenComparing(MetadataRepresentation::getKey, nullsLast(naturalOrder()));

    public static final Comparator<MetadataRepresentation> METADATA_REPRESENTATION_INDEX_COMPARATOR = Comparator
            .comparing(MetadataRepresentation::getIndex, nullsLast(naturalOrder()))
            .thenComparing(MetadataRepresentation::getName, nullsLast(naturalOrder()))
            .thenComparing(MetadataRepresentation::getKey, nullsLast(naturalOrder()));

    // <editor-fold desc="Beans">


    private final MetadataRepository metadataRepository;
    private final ModelMapper modelMapper;


    public MetadataBusinessService(MetadataRepository metadataRepository,
                                   ModelMapper modelMapper) {
        this.metadataRepository = metadataRepository;
        this.modelMapper = modelMapper;
    }


    // </editor-fold desc="Beans">


    public void updateInnerValues(@Nullable DeskDto deskDto, Comparator<MetadataRepresentation> comparator) {

        if (deskDto == null) {
            return;
        }

        // Fetch real models

        Optional.ofNullable(deskDto.getFilterableMetadataIds())
                .map(ids -> metadataRepository
                        .findAllByTenant_IdAndIdIn(deskDto.getTenantId(), ids, unpaged())
                        .getContent()
                )
                .map(list -> list.stream()
                        .map(metadata -> modelMapper.map(metadata, MetadataRepresentation.class))
                        .sorted(comparator)
                        .toList()
                )
                .ifPresent(deskDto::setFilterableMetadata);
    }


}
