/*
 * iparapheur Core
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.ipcore.model.mail;


import coop.libriciel.ipcore.model.auth.DeskRepresentation;
import coop.libriciel.ipcore.model.auth.User;
import coop.libriciel.ipcore.model.database.Tenant;
import coop.libriciel.ipcore.model.workflow.Action;
import coop.libriciel.ipcore.model.workflow.Folder;
import coop.libriciel.ipcore.model.workflow.NotificationType;
import coop.libriciel.ipcore.model.workflow.Task;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.text.StringEscapeUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.*;

import static coop.libriciel.ipcore.utils.TextUtils.MESSAGE_BUNDLE;
import static java.util.stream.Collectors.toMap;


/**
 * This class is usually given to the FreeTemplate engine.
 * Every fields here will be used in the FTL placeholders.
 * <p>
 * We don't want to give the entire {@link Folder} or {@link Task} instance to the FreeTemplate engine.
 * Some fields and Ids should stay internal, to prevent users to find and use those accidentally.
 * <p>
 * That's why we have these limited fields here.
 */
@Data
@NoArgsConstructor
public class MailContent {


    private String tenantId;
    private String tenantName;

    private String folderId;
    private String folderName;
    private Map<String, String> metadata;

    private Action taskAction;
    private String taskActionString;
    private String taskActionPastString;

    private String username;
    private String deskId;
    private String deskName;
    private Date date;
    private String message;
    private String publicAnnotation;
    private NotificationType notificationType;


    @Builder
    public MailContent(@Nullable Tenant tenant,
                       @NotNull Folder folder,
                       @Nullable Task task,
                       @Nullable DeskRepresentation desk,
                       @Nullable User user,
                       @Nullable NotificationType notificationType) {

        if (tenant != null) {
            this.tenantId = tenant.getId();
            this.tenantName = StringEscapeUtils.escapeHtml4(tenant.getName());
        }

        this.folderId = folder.getId();
        this.folderName = StringEscapeUtils.escapeHtml4(folder.getName());

        this.deskId = Optional.ofNullable(desk).map(DeskRepresentation::getId).orElse(null);
        this.deskName = Optional.ofNullable(desk).map(DeskRepresentation::getName).orElse(null);

        this.username = Optional.ofNullable(user).map(User::getUserName).orElse(null);
        this.notificationType = notificationType;

        this.metadata = folder.getMetadata().entrySet().stream()
                .collect(toMap(
                        e -> StringEscapeUtils.escapeHtml4(e.getKey()),
                        e -> StringEscapeUtils.escapeHtml4(e.getValue())
                ));

        ResourceBundle resourceBundle = ResourceBundle.getBundle(MESSAGE_BUNDLE, Locale.getDefault());
        if (task != null) {

            this.metadata.putAll(task.getMetadata());
            this.taskAction = task.getAction();
            this.taskActionString = resourceBundle.getString(task.getAction().getMessageKey());
            this.taskActionPastString = resourceBundle.getString(task.getAction().getPastMessageKey());
            this.publicAnnotation = task.getPublicAnnotation();
            this.date = task.getDate();

            task.getDesks().stream()
                    .findFirst()
                    .map(DeskRepresentation::getId)
                    .ifPresent(i -> this.deskId = i);
        }
    }


}
