/*
 * iparapheur Core
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.ipcore.model.permission;


import coop.libriciel.ipcore.model.auth.DeskRepresentation;
import coop.libriciel.ipcore.model.database.TypologyRepresentation;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

import static io.swagger.v3.oas.annotations.media.Schema.AccessMode.READ_ONLY;
import static io.swagger.v3.oas.annotations.media.Schema.AccessMode.WRITE_ONLY;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DelegationDto {


    public static final String API_DOC_ID_VALUE = "Delegation Id";
    public static final String API_PATH = "delegationId";
    public static final String API_DOC = "target substitute desk, optional type/subtype; and schedule containing Dates and actions to perform.";


    @Schema(accessMode = READ_ONLY)
    private String id;

    @Schema(accessMode = WRITE_ONLY)
    private String substituteDeskId;

    @Schema(accessMode = READ_ONLY)
    private DeskRepresentation substituteDesk;

    @Schema(accessMode = WRITE_ONLY)
    private String delegatingDeskId;

    @Schema(accessMode = READ_ONLY)
    private DeskRepresentation delegatingDesk;

    @Schema(accessMode = WRITE_ONLY)
    private String typeId;

    @Schema(nullable = true, accessMode = READ_ONLY)
    private TypologyRepresentation type;

    @Schema(nullable = true, accessMode = WRITE_ONLY)
    private String subtypeId;

    @Schema(nullable = true, accessMode = READ_ONLY)
    private TypologyRepresentation subtype;

    @Schema(nullable = true)
    private Date start;

    @Schema(nullable = true)
    private Date end;

}
