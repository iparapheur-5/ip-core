/*
 * iparapheur Core
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.ipcore.model.permission.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;
import java.util.Set;

import static java.util.Collections.emptyMap;


@Data
@NoArgsConstructor
public class KeycloakPermissionRequest {


    @Data
    @AllArgsConstructor
    public static class Resource {

        private String type;
        private Set<KeycloakPermissionScope> scopes;

    }


    @Data
    @NoArgsConstructor
    static class Context {

        private Map<String, String> attributes = emptyMap();

    }


    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Set<Resource> resources;

    /**
     * The Keycloak API is kinda ambiguous here. It actually asks for names, in an "id" parameter...
     * We renamed the inner object, to avoid ambiguity,
     * using a JsonProperty to fix the request.
     */
    private @JsonProperty("roleIds") Set<String> roleNames;

    private String userId;
    private Context context = new Context(); // Not used, but mandatory
    private boolean entitlements = false;


}
