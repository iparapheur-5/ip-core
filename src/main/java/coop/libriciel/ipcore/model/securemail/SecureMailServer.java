/*
 * iparapheur Core
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.ipcore.model.securemail;

import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SecureMailServer {
    public static final String API_PATH = "serverId";

    private Long id;

    private String name;

    private String url;

    private String sslPublicKey;

    private String login;

    private String password;

    private int apiLevel = -1;

    private int entity = -1;

    private String type;

    private String tenantId;
}
