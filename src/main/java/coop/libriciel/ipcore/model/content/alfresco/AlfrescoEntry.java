/*
 * iparapheur Core
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package coop.libriciel.ipcore.model.content.alfresco;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.jetbrains.annotations.NotNull;

/**
 * Alfresco over-wraps every object in an `entry` field, like this :
 * <code>{ "entry": { `the_actual_object` } }</code>
 * <p>
 * Instead of un-wrapping every field, every object, we just define a generic wrapper class,
 * to allow a native code-less parsing with Lombok.
 */
@Data
@Log4j2
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class AlfrescoEntry<T> {

    private @NotNull T entry;

}
