/*
 * iparapheur Core
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.ipcore.model.content;

import com.alfresco.client.api.core.model.Node;
import com.alfresco.client.api.core.model.NodeChildAssociation;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import coop.libriciel.ipcore.model.content.alfresco.AlfrescoNode;
import coop.libriciel.ipcore.model.crypto.PdfSignaturePosition;
import coop.libriciel.ipcore.model.pdfstamp.SignaturePlacement;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.jetbrains.annotations.NotNull;
import org.springframework.http.MediaType;
import org.springframework.web.multipart.MultipartFile;

import java.util.*;

import static coop.libriciel.ipcore.model.content.alfresco.AlfrescoProperties.*;


@Data
@Log4j2
@Builder
@AllArgsConstructor
// FIXME : This is the real inner object, temporarily named DTO to clean the API.
//  We shall create a real inner object, with the same parameters, and split the logic.
@Schema(name = "DocumentDto")
@JsonIgnoreProperties("mainDocument")
public class Document {

    public static final String API_DOC_ID_VALUE = "Document Id";


    private String id;
    private String name;
    private int index;
    private long contentLength;
    private MediaType mediaType;
    private @JsonIgnore Float mediaVersion;
    private @JsonProperty("deletable") boolean deletable = true;
    private @JsonProperty("isMainDocument") boolean isMainDocument;
    private String pdfVisualId;
    private @JsonIgnore String checksumAlgorithm;
    private @JsonIgnore String checksumValue;
    private @Builder.Default List<SignaturePlacement> signaturePlacementAnnotations = new ArrayList<>();
    private @Builder.Default Map<Integer, PdfSignaturePosition> signatureTags = new HashMap<>();
    private @Builder.Default Map<Integer, PdfSignaturePosition> sealTags = new HashMap<>();
    private @Builder.Default List<DetachedSignature> detachedSignatures = new ArrayList<>();


    public Document() {
        signaturePlacementAnnotations = new ArrayList<>();
        signatureTags = new HashMap<>();
        sealTags = new HashMap<>();
        detachedSignatures = new ArrayList<>();
    }


    public Document(@NotNull MultipartFile multipartFile, boolean isMainDocument, int index) {

        this.name = multipartFile.getOriginalFilename();
        this.index = index;
        this.isMainDocument = isMainDocument;
        this.contentLength = multipartFile.getSize();

        this.mediaType = Optional.ofNullable(multipartFile.getContentType())
                .map(MediaType::parseMediaType)
                .orElse(null); // TODO : A fallback guessed with the file extension ?

    }


    /**
     * Seems to be a duplicate, but the OpenApi library generates two unrelated objects.
     * So... 2 constructors.
     *
     * @param alfrescoNode
     */
    public Document(@NotNull Node alfrescoNode) {
        id = alfrescoNode.getId();
        name = alfrescoNode.getName();
        contentLength = alfrescoNode.getContent().getSizeInBytes();
        mediaType = MediaType.parseMediaType(alfrescoNode.getContent().getMimeType());

        //noinspection unchecked
        Map<String, Object> properties = (Map<String, Object>) alfrescoNode.getProperties();
        parseAlfrescoInnerProperties(properties);
    }


    /**
     * Seems to be a duplicate, but the OpenApi library generates two unrelated objects.
     * So... 2 constructors.
     *
     * @param alfrescoNode
     */
    public Document(@NotNull NodeChildAssociation alfrescoNode) {
        id = alfrescoNode.getId();
        name = alfrescoNode.getName();
        contentLength = alfrescoNode.getContent().getSizeInBytes();
        mediaType = MediaType.parseMediaType(alfrescoNode.getContent().getMimeType());

        //noinspection unchecked
        Map<String, Object> properties = (Map<String, Object>) alfrescoNode.getProperties();
        parseAlfrescoInnerProperties(properties);
    }


    /**
     * The Alfresco lib parses the inner "property" variable as a generic {@link LinkedHashMap} class.
     * It is then cast-able to a {@link Map}.
     * <p>
     * It is easier to parse it that way, than to fix the Alfresco's Swagger models on-the-fly.
     *
     * @param alfrescoProperties
     */
    private void parseAlfrescoInnerProperties(@NotNull Map<String, Object> alfrescoProperties) {
        ObjectMapper objectMapper = new ObjectMapper();

        deletable = MapUtils.getBoolean(alfrescoProperties, PROPERTY_DELETABLE, false);
        index = MapUtils.getInteger(alfrescoProperties, PROPERTY_INDEX, 0);
        isMainDocument = MapUtils.getBoolean(alfrescoProperties, PROPERTY_IS_MAIN_DOCUMENT, false);
        checksumAlgorithm = MapUtils.getString(alfrescoProperties, PROPERTY_CHECKSUM_ALGORITHM, null);
        checksumValue = MapUtils.getString(alfrescoProperties, PROPERTY_CHECKSUM_VALUE, null);
        mediaVersion = MapUtils.getFloat(alfrescoProperties, PROPERTY_MEDIA_VERSION, null);

        signaturePlacementAnnotations = new ArrayList<>();
        String annotationsSerialized = MapUtils.getString(alfrescoProperties, PROPERTY_ANNOTATIONS, null);
        if (StringUtils.isNotEmpty(annotationsSerialized)) {
            try {
                signaturePlacementAnnotations = objectMapper.readValue(annotationsSerialized, new TypeReference<>() {});
                // objectMapper.readValue can return null ; and we can't leave this field null.
                if (signaturePlacementAnnotations == null) {
                    signaturePlacementAnnotations = new ArrayList<>();
                }
            } catch (JsonProcessingException e) {
                log.warn("Cannot parse the manual-signature-positioning from Alfresco. Will use `null`, and a default position will be used later.", e);
            }
        }

        signatureTags = new HashMap<>();
        String signatureTagsSerialized = MapUtils.getString(alfrescoProperties, PROPERTY_SIGNATURE_TAGS, null);
        if (StringUtils.isNotEmpty(signatureTagsSerialized)) {
            try {
                signatureTags = objectMapper.readValue(signatureTagsSerialized, new TypeReference<>() {});
            } catch (JsonProcessingException e) {
                log.warn("Cannot parse the signature-tag-positioning from Alfresco. Will use `null`, and a default position will be used later.", e);
            }
        }

        sealTags = new HashMap<>();
        String sealTagsSerialized = MapUtils.getString(alfrescoProperties, PROPERTY_SEAL_TAGS, null);
        if (StringUtils.isNotEmpty(sealTagsSerialized)) {
            try {
                sealTags = objectMapper.readValue(sealTagsSerialized, new TypeReference<>() {});
            } catch (JsonProcessingException e) {
                log.warn("Cannot parse the seal-tag-positioning from Alfresco. Will use `null`, and a default position will be used later.", e);
            }
        }
    }


    public Document(@NotNull AlfrescoNode alfrescoNode) {
        ObjectMapper objectMapper = new ObjectMapper();
        id = alfrescoNode.getId();
        name = alfrescoNode.getName();
        deletable = Optional.ofNullable(alfrescoNode.getProperties().getDeletable()).orElse(false);
        index = alfrescoNode.getProperties().getIndex();
        contentLength = alfrescoNode.getContent().getSizeInBytes();
        isMainDocument = Optional.ofNullable(alfrescoNode.getProperties().getIsMainDocument()).orElse(false);
        checksumAlgorithm = alfrescoNode.getProperties().getChecksumAlgorithm();
        checksumValue = alfrescoNode.getProperties().getChecksumValue();
        mediaType = MediaType.parseMediaType(alfrescoNode.getContent().getMimeType());
        signaturePlacementAnnotations = new ArrayList<>();

        if (Objects.nonNull(alfrescoNode.getProperties().getAnnotations())) {
            try {
                signaturePlacementAnnotations = objectMapper.readValue(alfrescoNode.getProperties().getAnnotations(), new TypeReference<>() {});
                // objectMapper.readValue can return null ; and we can't leave this field null.
                if (signaturePlacementAnnotations == null) {
                    signaturePlacementAnnotations = new ArrayList<>();
                }
            } catch (JsonProcessingException e) {
                log.warn("Cannot parse the manual-signature-positioning from Alfresco. Will use `null`, and a default position will be used later.", e);
            }
        }

        signatureTags = new HashMap<>();
        if (Objects.nonNull(alfrescoNode.getProperties().getSignatureTags())) {
            try {
                signatureTags = objectMapper.readValue(alfrescoNode.getProperties().getSignatureTags(), new TypeReference<>() {});
            } catch (JsonProcessingException e) {
                log.warn("Cannot parse the signature-tag-positioning from Alfresco. Will use `null`, and a default position will be used later.", e);
            }
        }

        sealTags = new HashMap<>();
        if (Objects.nonNull(alfrescoNode.getProperties().getSealTags())) {
            try {
                sealTags = objectMapper.readValue(alfrescoNode.getProperties().getSealTags(), new TypeReference<>() {});
            } catch (JsonProcessingException e) {
                log.warn("Cannot parse the seal-tag-positioning from Alfresco. Will use `null`, and a default position will be used later.", e);
            }
        }

        mediaVersion = Optional
                .ofNullable(alfrescoNode.getProperties().getMediaVersion())
                .filter(NumberUtils::isParsable)
                .map(Float::parseFloat)
                .orElse(null);
    }


}
