/*
 * iparapheur Core
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package coop.libriciel.ipcore.model.content;

import com.alfresco.client.api.core.model.NodeChildAssociation;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.collections4.MapUtils;
import org.springframework.http.MediaType;

import java.util.Map;

import static coop.libriciel.ipcore.model.content.alfresco.AlfrescoProperties.PROPERTY_TARGET_DOCUMENT_ID;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DetachedSignature {


    private String id;
    private String name;
    private long contentLength;
    private MediaType mediaType;
    private String targetDocumentId;
    private String targetTaskId;


    public DetachedSignature(NodeChildAssociation alfrescoNode) {
        id = alfrescoNode.getId();
        name = alfrescoNode.getName();
        contentLength = alfrescoNode.getContent().getSizeInBytes();
        mediaType = MediaType.parseMediaType(alfrescoNode.getContent().getMimeType());

        //noinspection unchecked
        Map<String, Object> properties = (Map<String, Object>) alfrescoNode.getProperties();
        targetDocumentId = MapUtils.getString(properties, PROPERTY_TARGET_DOCUMENT_ID);
    }


}
