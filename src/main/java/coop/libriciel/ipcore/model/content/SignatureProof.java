/*
 * iparapheur Core
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package coop.libriciel.ipcore.model.content;

import com.alfresco.client.api.core.model.NodeChildAssociation;
import lombok.*;
import org.apache.commons.collections4.MapUtils;

import java.util.Map;

import static coop.libriciel.ipcore.model.content.alfresco.AlfrescoProperties.*;


@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@AllArgsConstructor
// TODO create superType Content, document and signatureProof will extend it
public class SignatureProof extends Document {


    private String proofTargetTaskId;


    public SignatureProof(NodeChildAssociation alfrescoNode) {
        super(alfrescoNode);
        Map<String, Object> properties = (Map<String, Object>) alfrescoNode.getProperties();
        this.proofTargetTaskId = MapUtils.getString(properties, PROPERTY_TASK_ID);
    }


}
