/*
 * iparapheur Core
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.ipcore.model.crypto;

import io.swagger.v3.oas.annotations.media.Schema;


@Schema(enumAsRef = true)
public enum SignatureFormat {


    PKCS7,
    PADES,
    PES_V2,
    XADES_DETACHED,
    AUTO;


    public boolean isEnvelopedSignature() {
        return switch (this) {
            case PKCS7, XADES_DETACHED -> false;
            case PADES, PES_V2 -> true;
            default -> throw new RuntimeException("The signature format should have been evaluated before the isEnvelopedSignature call");
        };
    }


}
