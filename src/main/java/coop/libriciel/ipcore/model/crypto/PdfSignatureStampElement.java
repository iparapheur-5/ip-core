/*
 * iparapheur Core
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.ipcore.model.crypto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.apache.pdfbox.pdmodel.font.PDType1Font;

import javax.validation.constraints.Positive;
import javax.validation.constraints.PositiveOrZero;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class PdfSignatureStampElement {

    public enum Type {
        TEXT,
        IMAGE
    }


    @Getter
    @AllArgsConstructor
    public enum Font {

        TIMES_ROMAN(PDType1Font.TIMES_ROMAN),
        TIMES_BOLD(PDType1Font.TIMES_BOLD),
        TIMES_ITALIC(PDType1Font.TIMES_ITALIC),
        TIMES_BOLD_ITALIC(PDType1Font.TIMES_BOLD_ITALIC),

        HELVETICA(PDType1Font.HELVETICA),
        HELVETICA_BOLD(PDType1Font.HELVETICA_BOLD),
        HELVETICA_OBLIQUE(PDType1Font.HELVETICA_OBLIQUE),
        HELVETICA_BOLD_OBLIQUE(PDType1Font.HELVETICA_BOLD_OBLIQUE),

        COURIER(PDType1Font.COURIER),
        COURIER_BOLD(PDType1Font.COURIER_BOLD),
        COURIER_OBLIQUE(PDType1Font.COURIER_OBLIQUE),
        COURIER_BOLD_OBLIQUE(PDType1Font.COURIER_BOLD_OBLIQUE);

        private final PDType1Font pdType1Font;
    }


    private Type type;
    private String value;

    private Font font;

    @PositiveOrZero(message = "{message.font_size_should_be_positive}")
    private Integer fontSize;

    private String colorCode;

    @PositiveOrZero(message = "{message.x_coordinate_should_be_positive}")
    private Float x;

    @PositiveOrZero(message = "{message.y_coordinate_should_be_positive}")
    private Float y;

    @Positive(message = "{message.width_should_be_positive}")
    private Float width;

    @Positive(message = "{message.height_should_be_positive}")
    private Float height;

}
