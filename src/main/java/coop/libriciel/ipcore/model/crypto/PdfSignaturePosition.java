/*
 * iparapheur Core
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.ipcore.model.crypto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.validation.constraints.PositiveOrZero;
import java.io.Serial;
import java.io.Serializable;

import static coop.libriciel.ipcore.model.crypto.PdfSignaturePosition.Origin.BOTTOM_LEFT;


@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class PdfSignaturePosition implements Serializable {


    public enum Origin {BOTTOM_LEFT, CENTER}


    /**
     * Changes on this class should increase the UID,
     * and make sure the de-serialization is retro-compatible.
     */
    private static final @Serial long serialVersionUID = 1L;


    @PositiveOrZero(message = "{message.x_coordinate_should_be_positive}")
    private float x;

    @PositiveOrZero(message = "{message.y_coordinate_should_be_positive}")
    private float y;

    private int page = 1;

    private @JsonIgnore Origin origin = BOTTOM_LEFT;

}
