/*
 * iparapheur Core
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.ipcore.model.auth.requests;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import coop.libriciel.ipcore.model.auth.DeskRepresentation;
import coop.libriciel.ipcore.model.database.MetadataRepresentation;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

import static coop.libriciel.ipcore.utils.ApiUtils.*;
import static io.swagger.v3.oas.annotations.media.Schema.AccessMode;


@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class DeskDto extends DeskRepresentation {

    @Size(min = NAME_MIN_LENGTH, max = NAME_MAX_LENGTH, message = NAME_SIZE_JAVAX_VALIDATION_ERROR_MESSAGE)
    @NotNull(message = NAME_SIZE_JAVAX_VALIDATION_ERROR_MESSAGE)
    private String shortName;

    private String description;

    private boolean folderCreationAllowed = true;
    private boolean actionAllowed = true;
    private boolean archivingAllowed = true;
    private boolean chainAllowed = true;

    @JsonProperty(access = Access.WRITE_ONLY)
    @Schema(accessMode = AccessMode.WRITE_ONLY)
    private String parentDeskId;
    @JsonProperty(access = Access.READ_ONLY)
    @Schema(accessMode = AccessMode.READ_ONLY)
    private DeskRepresentation parentDesk;

    @JsonProperty(access = Access.WRITE_ONLY)
    @Schema(accessMode = AccessMode.WRITE_ONLY)
    @Size(max = MAX_USER_PER_DESKS_COUNT, message = OWNER_LIST_MAX_SIZE_JAVAX_VALIDATION_ERROR_MESSAGE)
    @NotNull(message = OWNER_LIST_NOT_NULL_JAVAX_VALIDATION_ERROR_MESSAGE)
    private List<String> ownerIds;

    @JsonProperty(access = Access.READ_ONLY)
    @Schema(accessMode = AccessMode.READ_ONLY)
    private List<UserRepresentation> owners;

    @JsonProperty(access = Access.WRITE_ONLY)
    @Schema(accessMode = AccessMode.WRITE_ONLY)
    private List<String> associatedDeskIds;

    @JsonProperty(access = Access.READ_ONLY)
    @Schema(accessMode = AccessMode.READ_ONLY)
    private List<DeskRepresentation> associatedDesks;

    @JsonProperty(access = Access.WRITE_ONLY)
    @Schema(accessMode = AccessMode.WRITE_ONLY)
    private List<String> filterableMetadataIds;

    @JsonProperty(access = Access.READ_ONLY)
    @Schema(accessMode = AccessMode.READ_ONLY)
    private List<MetadataRepresentation> filterableMetadata;

    @JsonProperty(access = Access.WRITE_ONLY)
    @Schema(accessMode = AccessMode.WRITE_ONLY)
    private List<String> supervisorIds;

    @JsonProperty(access = Access.READ_ONLY)
    @Schema(accessMode = AccessMode.READ_ONLY)
    private List<UserRepresentation> supervisors;

    @JsonProperty(access = Access.WRITE_ONLY)
    @Schema(accessMode = AccessMode.WRITE_ONLY)
    private List<String> delegationManagerIds;

    @JsonProperty(access = Access.READ_ONLY)
    @Schema(accessMode = AccessMode.READ_ONLY)
    private List<UserRepresentation> delegationManagers;

}
