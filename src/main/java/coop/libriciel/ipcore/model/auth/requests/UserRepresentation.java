/*
 * iparapheur Core
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.ipcore.model.auth.requests;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import coop.libriciel.ipcore.model.auth.UserPrivilege;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.media.Schema.AccessMode;
import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import static coop.libriciel.ipcore.utils.ApiUtils.JAVAX_MESSAGE_PREFIX;
import static coop.libriciel.ipcore.utils.ApiUtils.JAVAX_MESSAGE_SUFFIX;


@Data
public class UserRepresentation {


    public static final String API_PATH = "userId";
    public static final String API_DOC_ID_VALUE = "User id";

    /**
     * Keycloak actually accepts every field up to 255 chars,
     * but on the username, we want a margin for the tenant name.
     * ... And for every other field, we want a regular size anyway.
     */

    private static final int USERNAME_MIN_LENGTH = 1;
    private static final int USERNAME_MAX_LENGTH = 128;
    private static final String USERNAME_SIZE_ERROR_MESSAGE = "message.username_should_be_between_2_and_128_chars";
    private static final String USERNAME_JAVAX_VALIDATION_ERROR_MESSAGE = JAVAX_MESSAGE_PREFIX + USERNAME_SIZE_ERROR_MESSAGE + JAVAX_MESSAGE_SUFFIX;

    private static final int FIRST_NAME_MAX_LENGTH = 128;
    private static final String FIRST_NAME_SIZE_ERROR_MESSAGE = "message.first_name_should_be_under_128_chars";
    private static final String FIRST_NAME_JAVAX_VALIDATION_ERROR_MESSAGE = JAVAX_MESSAGE_PREFIX + FIRST_NAME_SIZE_ERROR_MESSAGE + JAVAX_MESSAGE_SUFFIX;

    private static final int LAST_NAME_MAX_LENGTH = 128;
    private static final String LAST_NAME_SIZE_ERROR_MESSAGE = "message.last_name_should_be_under_128_chars";
    private static final String LAST_NAME_JAVAX_VALIDATION_ERROR_MESSAGE = JAVAX_MESSAGE_PREFIX + LAST_NAME_SIZE_ERROR_MESSAGE + JAVAX_MESSAGE_SUFFIX;

    private static final String EMAIL_VALIDATION_ERROR_MESSAGE = "message.email_is_invalid";
    private static final String EMAIL_JAVAX_VALIDATION_ERROR_MESSAGE = JAVAX_MESSAGE_PREFIX + EMAIL_VALIDATION_ERROR_MESSAGE + JAVAX_MESSAGE_SUFFIX;

    private static final String PRIVILEGE_MISSING_ERROR_MESSAGE = "message.privilege_is_missing";
    private static final String PRIVILEGE_JAVAX_VALIDATION_ERROR_MESSAGE = JAVAX_MESSAGE_PREFIX + PRIVILEGE_MISSING_ERROR_MESSAGE + JAVAX_MESSAGE_SUFFIX;


    @JsonProperty(access = Access.READ_ONLY)
    @Schema(accessMode = AccessMode.READ_ONLY)
    private String id;

    @Size(min = USERNAME_MIN_LENGTH, max = USERNAME_MAX_LENGTH, message = USERNAME_JAVAX_VALIDATION_ERROR_MESSAGE)
    @NotNull(message = USERNAME_JAVAX_VALIDATION_ERROR_MESSAGE)
    private String userName;

    @Size(max = FIRST_NAME_MAX_LENGTH, message = FIRST_NAME_JAVAX_VALIDATION_ERROR_MESSAGE)
    @NotNull(message = FIRST_NAME_JAVAX_VALIDATION_ERROR_MESSAGE)
    private String firstName;

    @Size(max = LAST_NAME_MAX_LENGTH, message = LAST_NAME_JAVAX_VALIDATION_ERROR_MESSAGE)
    @NotNull(message = LAST_NAME_JAVAX_VALIDATION_ERROR_MESSAGE)
    private String lastName;

    @Email(message = EMAIL_JAVAX_VALIDATION_ERROR_MESSAGE)
    @NotNull(message = EMAIL_JAVAX_VALIDATION_ERROR_MESSAGE)
    private String email;

    @NotNull(message = PRIVILEGE_JAVAX_VALIDATION_ERROR_MESSAGE)
    private UserPrivilege privilege;

    @JsonProperty(access = Access.READ_ONLY)
    @Schema(accessMode = AccessMode.READ_ONLY)
    private Boolean isLdapSynchronized;


}
