/*
 * iparapheur Core
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.ipcore.model.workflow;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import coop.libriciel.ipcore.model.auth.DeskRepresentation;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Arrays;
import java.util.List;


@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class WorkflowDefinition {


    public static final String API_ID_DOC_VALUE = "Workflow definition id";
    public static final String API_ID_PATH = "workflowDefinitionId";
    public static final String API_KEY_DOC_VALUE = "Workflow definition key";
    public static final String API_KEY_PATH = "workflowDefinitionKey";
    public static final String EMITTER_ID_PLACEHOLDER = "##EMITTER##";
    public static final String VARIABLE_DESK_ID_PLACEHOLDER = "##VARIABLE_DESK##";
    public static final String BOSS_OF_ID_PLACEHOLDER = "##BOSS_OF##";
    public static final List<String> GENERIC_VALIDATORS_IDS_PLACEHOLDERS = Arrays.asList(
            EMITTER_ID_PLACEHOLDER,
            VARIABLE_DESK_ID_PLACEHOLDER,
            BOSS_OF_ID_PLACEHOLDER
    );


    private String id;
    private String key;
    private String name;
    private Integer version = 0;
    private Boolean isSuspended = false;
    private String deploymentId;
    private long usageCount;
    private List<StepDefinition> steps;

    private DeskRepresentation finalDesk = new DeskRepresentation(EMITTER_ID_PLACEHOLDER);
    private List<DeskRepresentation> finalNotifiedDesks;


    public WorkflowDefinition(@NotNull String id,
                              @NotNull String key,
                              @NotNull String name,
                              @Nullable List<StepDefinition> steps,
                              @Nullable DeskRepresentation finalDesk,
                              boolean isSuspended,
                              @Nullable Integer version,
                              @NotNull String deploymentId,
                              long usageCount) {
        this.id = id;
        this.key = key;
        this.name = name;
        this.version = version;
        this.isSuspended = isSuspended;
        this.deploymentId = deploymentId;
        this.usageCount = usageCount;
        this.steps = steps;
        this.finalDesk = finalDesk;
    }


}
