/*
 * iparapheur Core
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package coop.libriciel.ipcore.model.workflow.ipWorkflow;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import coop.libriciel.ipcore.model.auth.DeskRepresentation;
import coop.libriciel.ipcore.model.auth.User;
import coop.libriciel.ipcore.model.workflow.Action;
import coop.libriciel.ipcore.model.workflow.FolderVisibility;
import coop.libriciel.ipcore.model.workflow.State;
import coop.libriciel.ipcore.model.workflow.Task;
import coop.libriciel.ipcore.utils.TextUtils;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.ObjectUtils;
import org.jetbrains.annotations.NotNull;
import org.jooq.Record;

import java.util.*;

import static coop.libriciel.ipcore.model.workflow.Action.*;
import static coop.libriciel.ipcore.model.workflow.State.*;
import static coop.libriciel.ipcore.services.database.PostgresService.*;
import static coop.libriciel.ipcore.services.workflow.IpWorkflowService.*;
import static coop.libriciel.ipcore.utils.CollectionUtils.parseMetadata;
import static coop.libriciel.ipcore.utils.CollectionUtils.popValue;
import static coop.libriciel.ipcore.utils.TextUtils.INTERNAL_PREFIX;
import static coop.libriciel.ipcore.utils.TextUtils.ISO8601_DATE_TIME_FORMAT;
import static java.lang.Boolean.TRUE;
import static java.util.Collections.emptyList;


@Data
@Log4j2
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@JsonIgnoreProperties(ignoreUnknown = true)
public class IpWorkflowTask extends Task {

    private static final String WORKFLOW_INTERNAL = "workflow_internal_";
    private static final String META_ORIGIN_DESK_ID = WORKFLOW_INTERNAL + "origin_group_id";
    private static final String META_FINAL_DESK_ID = "final_group_id";

    // Flowable API JSON variables

    private static final String API_ID = "id";
    private static final String API_INSTANCE_ID = "instanceId";
    private static final String API_START_TIME = "draftCreationDate";
    private static final String API_INSTANCE_NAME = "instanceName";
    private static final String API_INSTANCE_LEGACY_ID = "legacyId";
    private static final String API_INSTANCE_VISIBILITY = "visibility";
    private static final String API_CREATE_TIME = "createTime";
    private static final String API_END_TIME = "endTime";
    private static final String API_DUE_DATE = "dueDate";
    private static final String API_STATE = "state";
    private static final String API_EXPECTED_ACTION = "expectedAction";
    private static final String API_PERFORMED_ACTION = "performedAction";
    private static final String API_PENDING = "pending";
    private static final String API_VARIABLES = "variables";
    private static final String API_CANDIDATE_GROUPS = "candidateGroups";
    private static final String API_NOTIFIED_GROUPS = "notifiedGroups";
    private static final String API_ASSIGNEE = "assignee";
    private static final String API_DELEGATED_BY_GROUP_ID = "delegatedByGroupId";

    public static final String API_VAR_KEY_PUBLIC_ANNOTATION = INTERNAL_PREFIX + "public_annotation";
    public static final String API_VAR_KEY_PRIVATE_ANNOTATION = INTERNAL_PREFIX + "private_annotation";
    public static final String API_VAR_KEY_CURRENT_CANDIDATE_GROUPS = "current_candidate_groups";


    private @JsonIgnore Boolean isPending = null;
    private @JsonIgnore String folderName;
    private @JsonIgnore String legacyId;
    private @JsonIgnore FolderVisibility visibility;
    private @JsonIgnore String folderId;
    private @JsonIgnore String folderTypeId;
    private @JsonIgnore String folderSubtypeId;
    private @JsonIgnore String folderOriginDeskId;
    private @JsonIgnore String folderFinalDeskId;
    private @JsonIgnore Date folderDueDate;

    private Action performedAction = null;
    private String externalSignatureProcedureId = null;


    @JsonProperty(API_ID)
    private void parseId(String id) {this.id = id;}


    @JsonProperty(API_INSTANCE_NAME)
    private void parseFolderName(String folderName) {this.folderName = folderName;}


    @JsonProperty(API_INSTANCE_LEGACY_ID)
    private void parseLegacyId(String legacyId) {this.legacyId = legacyId;}


    @JsonProperty(API_INSTANCE_VISIBILITY)
    private void parseVisibility(FolderVisibility visibility) {this.visibility = visibility;}


    @JsonProperty(API_INSTANCE_ID)
    private void parseFolderId(String folderId) {this.folderId = folderId;}


    @JsonProperty(API_START_TIME)
    @JsonAlias(API_CREATE_TIME)
    private void parseBeginDate(Date beginDate) {this.beginDate = beginDate;}


    @JsonProperty(API_END_TIME)
    private void parseEndDate(Date date) {this.date = date;}


    @JsonProperty(API_DUE_DATE)
    private void parseDueDate(Date date) {this.folderDueDate = date;}


    @JsonProperty(API_STATE)
    private void parseState(State state) {
        this.state = ObjectUtils.firstNonNull(state, this.state);
        computeState();
    }


    @JsonProperty(API_EXPECTED_ACTION)
    private void parseCurrentAction(Action action) {
        this.action = action;
        computeState();
    }


    @JsonProperty(API_PERFORMED_ACTION)
    private void parsePerformedAction(Action action) {
        this.performedAction = action;
        computeState();
    }


    @JsonProperty(API_PENDING)
    private void parsePending(boolean pending) {
        this.isPending = pending;
        computeState();
    }


    @JsonProperty(API_CANDIDATE_GROUPS)
    private void parseCurrentCandidateGroups(List<String> candidateGroupsId) {
        this.desks = candidateGroupsId
                .stream()
                .map(DeskRepresentation::new)
                .toList();
    }


    @JsonProperty(API_NOTIFIED_GROUPS)
    private void parseNotifiedGroups(List<String> notifiedGroupsId) {
        this.notifiedDesks = Optional.ofNullable(notifiedGroupsId)
                .orElse(emptyList())
                .stream()
                .map(DeskRepresentation::new)
                .toList();
    }


    @JsonProperty(API_ASSIGNEE)
    private void parseAssignee(String assigneeId) {
        this.user = Optional.ofNullable(assigneeId)
                .map(i -> User.builder().id(assigneeId).build())
                .orElse(null);
    }


    @JsonProperty(API_DELEGATED_BY_GROUP_ID)
    private void parseDelegatedByGroupId(String groupId) {
        this.delegatedByDesk = Optional.ofNullable(groupId)
                .map(DeskRepresentation::new)
                .orElse(null);
    }


    @JsonProperty(API_VARIABLES)
    public void parseNestedVariables(Map<String, String> variables) {
        publicAnnotation = popValue(variables, METADATA_PUBLIC_ANNOTATION);
        privateAnnotation = popValue(variables, METADATA_PRIVATE_ANNOTATION);
        draftCreationDate = TextUtils.deserializeDate(popValue(variables, METADATA_DRAFT_CREATION_DATE), ISO8601_DATE_TIME_FORMAT);
        folderTypeId = popValue(variables, META_TYPE_ID);
        folderSubtypeId = popValue(variables, META_SUBTYPE_ID);
        folderOriginDeskId = popValue(variables, META_ORIGIN_DESK_ID);
        folderFinalDeskId = popValue(variables, META_FINAL_DESK_ID);
        publicCertificateBase64 = popValue(variables, METADATA_PUBLIC_CERTIFICATE_BASE64);
        externalSignatureProcedureId = popValue(variables, METADATA_TRANSACTION_ID);
        metadata = variables;
    }


    void computeState() {

        // Simple cases

        if (isPending == TRUE) {
            this.state = PENDING;
            return;
        }

        if (ObjectUtils.anyNull(action, performedAction)) {
            // We'll compute it later, once every parameter will be set
            return;
        }

        if (action == READ) {
            this.state = VALIDATED;
            return;
        }

        if ((action == performedAction) || (action == SIGNATURE && performedAction == PAPER_SIGNATURE)) {
            this.state = VALIDATED;
        }

        this.state = switch (performedAction) {
            case TRANSFER -> TRANSFERRED;
            case ASK_SECOND_OPINION -> SECONDED;
            case BYPASS -> BYPASSED;
            case REJECT -> REJECTED;
            default -> this.state;
        };
    }


    public IpWorkflowTask(@NotNull Record record) {

        id = record.get(SQL_TASK_ID, String.class);
        folderDueDate = record.get(SQL_DUE_DATE, Date.class);
        beginDate = record.get(SQL_BEGIN_DATE, Date.class);
        folderId = record.get(SQL_INSTANCE_ID, String.class);
        folderName = record.get(SQL_INSTANCE_NAME, String.class);

        action = Optional.ofNullable(record.get(SQL_TASK_NAME, String.class))
                .map(Action::fromString)
                .orElse(VISA);

        desks = Optional.ofNullable(record.get(SQL_CANDIDATE_GROUP, String.class))
                .map(DeskRepresentation::new)
                .map(Collections::singletonList)
                .orElse(emptyList());

        metadata = parseMetadata(record.get(SQL_VARIABLES, String[][].class));
        parseNestedVariables(metadata);
    }


}
