/*
 * iparapheur Core
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.ipcore.model.workflow;

import com.fasterxml.jackson.annotation.JsonEnumDefaultValue;
import io.swagger.v3.oas.annotations.media.Schema;


@Schema(enumAsRef = true)
public enum FolderSortBy {
    // TODO not working correctly workflow-side at the moment
    VALIDATION_START_DATE,
    @JsonEnumDefaultValue CREATION_DATE,
    STILL_SINCE_DATE,
    LATE_DATE,
    END_DATE,
    TASK_ID,
    FOLDER_ID,
    FOLDER_NAME,
    TYPE_ID,
    TYPE_NAME,
    SUBTYPE_ID,
    SUBTYPE_NAME,
    ACTION_TYPE;


    public static class Constants {

        public static final String FOLDER_NAME_VALUE = "FOLDER_NAME";
        public static final String API_DOC_SORT_BY_VALUES = "Sorting properties are restricted to the FolderSortBy enum values";

    }


}
