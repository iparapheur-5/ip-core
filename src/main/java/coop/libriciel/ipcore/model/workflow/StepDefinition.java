/*
 * iparapheur Core
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package coop.libriciel.ipcore.model.workflow;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import coop.libriciel.ipcore.model.auth.DeskRepresentation;
import coop.libriciel.ipcore.model.database.Metadata;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static coop.libriciel.ipcore.model.workflow.StepDefinitionParallelType.OR;
import static java.util.Collections.emptyList;


@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class StepDefinition {


    private String id = "id_" + UUID.randomUUID();
    private Action type;
    private List<DeskRepresentation> validatingDesks;
    private List<DeskRepresentation> notifiedDesks;
    private List<Metadata> mandatoryValidationMetadata;
    private List<Metadata> mandatoryRejectionMetadata;

    private StepDefinitionParallelType parallelType = OR;


    @JsonProperty("validators")
    public void parseValidators(@Nullable List<String> validatorsIds) {
        validatingDesks = Optional.ofNullable(validatorsIds)
                .orElse(emptyList())
                .stream()
                .map(id -> new DeskRepresentation(id, null))
                .toList();
    }


    @JsonProperty("notifiedGroups")
    public void parseNotifiedDesks(@Nullable List<String> notifiedDeskIds) {
        notifiedDesks = Optional.ofNullable(notifiedDeskIds)
                .orElse(emptyList())
                .stream()
                .map(id -> new DeskRepresentation(id, null))
                .toList();
    }


    @JsonProperty("mandatoryValidationMetadataIds")
    public void parseMandatoryValidationMetadata(@Nullable List<String> mandatoryValidationMetadataIds) {
        mandatoryValidationMetadata = Optional.ofNullable(mandatoryValidationMetadataIds)
                .orElse(emptyList())
                .stream()
                .map(id -> Metadata.builder().id(id).build())
                .toList();
    }


    @JsonProperty("mandatoryRejectionMetadataIds")
    public void parseMandatoryRejectionMetadata(@Nullable List<String> mandatoryRejectionMetadataIds) {
        mandatoryRejectionMetadata = Optional.ofNullable(mandatoryRejectionMetadataIds)
                .orElse(emptyList())
                .stream()
                .map(id -> Metadata.builder().id(id).build())
                .toList();
    }


}
