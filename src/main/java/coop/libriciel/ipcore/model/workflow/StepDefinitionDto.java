/*
 * iparapheur Core
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.ipcore.model.workflow;

import coop.libriciel.ipcore.model.auth.DeskRepresentation;
import coop.libriciel.ipcore.model.database.MetadataDto;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

import static io.swagger.v3.oas.annotations.media.Schema.AccessMode.READ_ONLY;
import static io.swagger.v3.oas.annotations.media.Schema.AccessMode.WRITE_ONLY;


@Data
@NoArgsConstructor
public class StepDefinitionDto {


    @Schema(accessMode = READ_ONLY)
    private String id;

    private Action type;

    @Schema(accessMode = WRITE_ONLY)
    private List<String> validatingDeskIds;

    @Schema(accessMode = READ_ONLY)
    private List<DeskRepresentation> validatingDesks;

    @Schema(accessMode = WRITE_ONLY)
    private List<String> notifiedDeskIds;

    @Schema(accessMode = READ_ONLY)
    private List<DeskRepresentation> notifiedDesks;

    /**
     * Validation/Rejection Metadata will be used directly.
     * We'll prefer the real DTO over the Representation ones.
     * These lists won't be that long, and having the restricted values already will prevent many requests.
     */

    @Schema(accessMode = WRITE_ONLY)
    private List<String> mandatoryValidationMetadataIds;

    @Schema(accessMode = READ_ONLY)
    private List<MetadataDto> mandatoryValidationMetadata;

    @Schema(accessMode = WRITE_ONLY)
    private List<String> mandatoryRejectionMetadataIds;

    @Schema(accessMode = READ_ONLY)
    private List<MetadataDto> mandatoryRejectionMetadata;

    private StepDefinitionParallelType parallelType;


}
