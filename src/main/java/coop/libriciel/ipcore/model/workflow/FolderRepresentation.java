/*
 * iparapheur Core
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.ipcore.model.workflow;

import coop.libriciel.ipcore.model.auth.DeskRepresentation;
import coop.libriciel.ipcore.model.database.requests.SubtypeDto;
import coop.libriciel.ipcore.model.database.requests.TypeDto;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.Map;

import static coop.libriciel.ipcore.utils.ApiUtils.*;
import static coop.libriciel.ipcore.utils.TextUtils.NO_FORBIDDEN_CHARACTERS_REGEXP;
import static io.swagger.v3.oas.annotations.media.Schema.AccessMode.READ_ONLY;
import static io.swagger.v3.oas.annotations.media.Schema.AccessMode.WRITE_ONLY;


/**
 * This class represents everything that could be used in a folder list.
 */
@Data
@NoArgsConstructor
public class FolderRepresentation {


    @Schema(accessMode = READ_ONLY)
    protected String id;

    @Size(min = NAME_MIN_LENGTH, max = NAME_MAX_LENGTH, message = NAME_SIZE_JAVAX_VALIDATION_ERROR_MESSAGE)
    @NotNull(message = NAME_SIZE_JAVAX_VALIDATION_ERROR_MESSAGE)
    @Pattern(regexp = NO_FORBIDDEN_CHARACTERS_REGEXP, message = NAME_CONTAINS_FORBIDDEN_CHARACTER_JAVAX_VALIDATION_ERROR_MESSAGE)
    protected String name;

    @Schema(nullable = true)
    protected Date dueDate;

    @Schema(accessMode = READ_ONLY)
    protected Date draftCreationDate;

    @Schema(accessMode = WRITE_ONLY)
    protected String typeId;

    @Schema(accessMode = READ_ONLY)
    protected TypeDto type;

    @Schema(accessMode = WRITE_ONLY)
    protected String subtypeId;

    @Schema(accessMode = READ_ONLY)
    protected SubtypeDto subtype;

    @Schema(accessMode = READ_ONLY)
    protected DeskRepresentation originDesk;

    @Schema(accessMode = READ_ONLY)
    protected DeskRepresentation finalDesk;

    @Schema(accessMode = READ_ONLY)
    protected boolean isReadByCurrentUser = false;

    protected Map<String, String> metadata;

}
