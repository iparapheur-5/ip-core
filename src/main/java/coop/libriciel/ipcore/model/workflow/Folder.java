
/*
 * iparapheur Core
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package coop.libriciel.ipcore.model.workflow;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import coop.libriciel.ipcore.model.auth.DeskRepresentation;
import coop.libriciel.ipcore.model.content.Document;
import coop.libriciel.ipcore.model.content.SignatureProof;
import coop.libriciel.ipcore.model.database.Subtype;
import coop.libriciel.ipcore.model.database.Type;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Builder.Default;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Transient;
import java.util.*;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class Folder {


    public static final String API_DOC_ID_VALUE = "Folder id";
    public static final String API_PATH = "folderId";

    protected String id;
    protected String name;
    protected @Default Map<String, String> metadata = new HashMap<>();

    protected Type type;
    protected Subtype subtype;
    protected String creationWorkflowDefinitionKey;
    protected String validationWorkflowDefinitionKey;
    protected String legacyId;
    protected @Default List<Task> stepList = new ArrayList<>();
    protected DeskRepresentation originDesk;
    protected DeskRepresentation finalDesk;

    protected @JsonIgnore String contentId;
    protected @JsonIgnore String premisNodeId;
    protected @Default List<Document> documentList = new ArrayList<>();
    protected @Default List<SignatureProof> signatureProofs = new ArrayList<>();

    protected Date dueDate;
    protected FolderVisibility visibility;
    protected Date draftCreationDate;
    protected Date sentToArchivesDate;

    protected @Builder.Default State state = null;

    protected @Default Set<String> readByUserIds = new HashSet<>();
    protected @Default @Transient boolean isReadByCurrentUser = false;


}
