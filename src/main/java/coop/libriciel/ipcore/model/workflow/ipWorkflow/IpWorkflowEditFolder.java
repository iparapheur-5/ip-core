/*
 * iparapheur Core
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.ipcore.model.workflow.ipWorkflow;

import coop.libriciel.ipcore.model.workflow.FolderDto;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;

import static coop.libriciel.ipcore.services.workflow.WorkflowServiceInterface.*;
import static coop.libriciel.ipcore.utils.TextUtils.ISO8601_DATE_TIME_FORMAT;


@Data
@NoArgsConstructor
public class IpWorkflowEditFolder {


    private String name;
    private Map<String, String> metadata = new HashMap<>();


    public IpWorkflowEditFolder(@NotNull FolderDto request) {
        this.name = request.getName();

        if (MapUtils.isNotEmpty(request.getMetadata())) {
            metadata.putAll(request.getMetadata());
        }

        Optional.ofNullable(request.getTypeId())
                .filter(StringUtils::isNotEmpty)
                .ifPresent(typeId -> metadata.put(META_TYPE_ID, typeId));

        Optional.ofNullable(request.getSubtypeId())
                .filter(StringUtils::isNotEmpty)
                .ifPresent(subtypeId -> metadata.put(META_SUBTYPE_ID, subtypeId));

        Optional.ofNullable(request.getDueDate())
                .ifPresent(dueDate -> metadata.put(META_DUE_DATE, new SimpleDateFormat(ISO8601_DATE_TIME_FORMAT, Locale.getDefault()).format(dueDate)));
    }


}
