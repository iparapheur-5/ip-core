/*
 * iparapheur Core
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package coop.libriciel.ipcore.model.workflow;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import coop.libriciel.ipcore.model.auth.DeskRepresentation;
import coop.libriciel.ipcore.model.auth.User;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.*;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;
import static coop.libriciel.ipcore.model.workflow.Action.VISA;
import static coop.libriciel.ipcore.model.workflow.State.PENDING;
import static coop.libriciel.ipcore.model.workflow.State.UPCOMING;


@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Task {

    public static final String API_PATH = "taskId";
    public static final String API_DOC_ID_VALUE = "Task id";


    @Builder()
    public static Task newTask(String id,
                               Map<String, String> metadata,
                               Action action,
                               Action performedAction,
                               ExternalState externalState,
                               State state,
                               List<DeskRepresentation> desks,
                               User user,
                               Set<String> readBy,
                               String publicCertificateBase64,
                               String externalSignatureProcedureId,
                               Date beginDate,
                               Date date,
                               Date draftCreationDate,
                               String publicAnnotation,
                               String privateAnnotation,
                               List<DeskRepresentation> notifiedDesks,
                               Long workflowIndex,
                               Long stepIndex,
                               List<String> mandatoryValidationMetadata,
                               List<String> mandatoryRejectionMetadata) {

        metadata = metadata != null ? metadata : new HashMap<>();
        action = action != null ? action : VISA;
        state = state != null ? state : PENDING;

        return new Task(
                id,
                metadata,
                action,
                performedAction,
                externalState,
                state,
                desks,
                null,
                user,
                readBy,
                publicCertificateBase64,
                externalSignatureProcedureId,
                beginDate,
                date,
                draftCreationDate,
                publicAnnotation,
                privateAnnotation,
                notifiedDesks,
                workflowIndex,
                stepIndex,
                mandatoryValidationMetadata,
                mandatoryRejectionMetadata
        );
    }


    @Schema(enumAsRef = true)
    public enum ExternalState {
        FORM,
        ACTIVE,
        SIGNED,
        REFUSED,
        EXPIRED,
        CREATED,
        IN_REDACTION,
        DELETED,
        SENT,
        SENT_AGAIN,
        RECEIVED_PARTIALLY,
        RECEIVED,
        NOT_RECEIVED,
        ERROR
    }


    protected String id;
    protected Map<String, String> metadata = new HashMap<>();

    protected Action action = VISA;
    protected Action performedAction = null;
    protected ExternalState externalState = null;
    protected State state = UPCOMING;

    protected List<DeskRepresentation> desks;
    protected DeskRepresentation delegatedByDesk;
    protected User user;
    protected Set<String> readByUserIds;
    protected String publicCertificateBase64;

    protected String externalSignatureProcedureId;
    protected Date beginDate;
    protected Date date;
    protected Date draftCreationDate;

    protected String publicAnnotation;
    protected String privateAnnotation;
    protected List<DeskRepresentation> notifiedDesks;

    protected Long workflowIndex;
    protected Long stepIndex;

    protected List<String> mandatoryValidationMetadata = new ArrayList<>();
    protected List<String> mandatoryRejectionMetadata = new ArrayList<>();

}
