/*
 * iparapheur Core
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.ipcore.model.pdfstamp;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.PositiveOrZero;

import static coop.libriciel.ipcore.model.pdfstamp.StampTextColor.BLACK;


@Data
@NoArgsConstructor
public class StampDto {


    public static final String API_DOC_ID_VALUE = "Stamp id";
    public static final String API_PATH = "stampId";


    private String id;
    private int page = 0;

    @PositiveOrZero(message = "{message.x_coordinate_should_be_positive}")
    private int x;

    @PositiveOrZero(message = "{message.y_coordinate_should_be_positive}")
    private int y;

    private Integer signatureRank;
    private boolean afterSignature = false;
    private StampType type;
    private String value;

    @PositiveOrZero(message = "{message.font_size_should_be_positive}")
    private Integer fontSize;
    private StampTextColor textColor = BLACK;

}
