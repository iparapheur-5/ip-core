/*
 * iparapheur Core
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.ipcore.model.pdfstamp;

import coop.libriciel.ipcore.model.database.SubtypeLayer;
import coop.libriciel.ipcore.model.database.Tenant;
import lombok.*;
import org.jetbrains.annotations.NotNull;

import javax.persistence.*;
import java.util.List;

import static coop.libriciel.ipcore.utils.TextUtils.UUID_STRING_SIZE;
import static javax.persistence.CascadeType.REMOVE;
import static javax.persistence.FetchType.EAGER;
import static javax.persistence.FetchType.LAZY;


@Setter
@Getter
@Entity
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Table(name = Layer.TABLE_NAME,
        uniqueConstraints = {
                @UniqueConstraint(
                        name = Layer.COLUMN_TENANT_ID + "_" + Layer.COLUMN_NAME + "_unique",
                        columnNames = {Layer.COLUMN_TENANT_ID, Layer.COLUMN_NAME}
                )
        }
)
public class Layer {


    public static final String TABLE_NAME = "layer";

    public static final String COLUMN_ID = "id";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_TENANT_ID = "tenant_id";
    public static final String COLUMN_STAMP_LIST = "stamp_list";
    public static final String COLUMN_STAMP_LIST_ORDER_INDEX = "order_index";


    @Column(name = COLUMN_ID, length = UUID_STRING_SIZE)
    private @Id String id;

    @Column(name = COLUMN_NAME)
    private String name;

    @ToString.Exclude
    @ManyToOne(fetch = EAGER)
    @JoinColumn(name = COLUMN_TENANT_ID, foreignKey = @ForeignKey(name = "fk_" + COLUMN_TENANT_ID))
    private Tenant tenant;

    @ToString.Exclude
    @Column(name = COLUMN_STAMP_LIST)
    @OneToMany(fetch = EAGER, mappedBy = "parentLayer", cascade = REMOVE, orphanRemoval = true)
    private List<Stamp> stampList;

    @ToString.Exclude
    @OneToMany(fetch = LAZY, mappedBy = "layer", cascade = REMOVE, orphanRemoval = true)
    private List<SubtypeLayer> subtypeLayerList;


    public Layer(@NotNull String id) {
        this.id = id;
    }


}
