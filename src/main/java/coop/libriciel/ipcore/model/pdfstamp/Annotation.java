/*
 * iparapheur Core
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.ipcore.model.pdfstamp;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import static coop.libriciel.ipcore.model.pdfstamp.Annotation.Origin.TOP_RIGHT;
import static io.swagger.v3.oas.annotations.media.Schema.AccessMode.READ_ONLY;


@Getter
@Setter
@MappedSuperclass
public abstract class Annotation {


    public enum Origin {TOP_RIGHT, BOTTOM_LEFT}


    public static final String COLUMN_ID = "id";

    protected static final String COLUMN_PAGE = "page";
    protected static final String COLUMN_X = "x";
    protected static final String COLUMN_Y = "y";
    protected static final String COLUMN_WIDTH = "width";
    protected static final String COLUMN_HEIGHT = "height";
    protected static final String COLUMN_PAGE_ROTATION = "page_rotation";
    protected static final String COLUMN_RECTANGLE_ORIGIN = "rectangle_origin";


    @Schema(accessMode = READ_ONLY)
    @Column(name = COLUMN_ID)
    protected @Id String id;

    @Schema(example = "1")
    @Column(name = COLUMN_PAGE)
    protected int page = -1;

    @Schema(example = "15")
    @Column(name = COLUMN_WIDTH)
    protected int width;

    @Schema(example = "15")
    @Column(name = COLUMN_HEIGHT)
    protected int height;

    @Column(name = COLUMN_X)
    protected int x;

    @Column(name = COLUMN_Y)
    protected int y;

    @Schema(example = "0")
    @Column(name = COLUMN_PAGE_ROTATION)
    protected int pageRotation = 0;

    @Schema(example = "TOP_RIGHT")
    @Column(name = COLUMN_RECTANGLE_ORIGIN)
    protected Origin rectangleOrigin = TOP_RIGHT;


}
