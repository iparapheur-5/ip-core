/*
 * iparapheur Core
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.ipcore.model.pdfstamp;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import javax.persistence.*;

import static coop.libriciel.ipcore.model.pdfstamp.StampTextColor.BLACK;
import static io.swagger.v3.oas.annotations.media.Schema.AccessMode.READ_ONLY;
import static javax.persistence.FetchType.EAGER;


@Getter
@Setter
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = Stamp.TABLE_NAME)
public class Stamp extends Annotation {


    public static final String TABLE_NAME = "stamp";

    public static final String COLUMN_INDEX = "index";
    private static final String COLUMN_TYPE = "type";
    private static final String COLUMN_PARENT = "layer_id";


    @JsonIgnore
    @ToString.Exclude
    @ManyToOne(fetch = EAGER)
    @Schema(accessMode = READ_ONLY)
    @JoinColumn(name = COLUMN_PARENT, foreignKey = @ForeignKey(name = "fk_" + COLUMN_PARENT))
    protected Layer parentLayer;

    protected Integer signatureRank;
    protected boolean afterSignature = false;
    protected @Column(name = COLUMN_TYPE) StampType type;
    protected String value;
    protected Integer fontSize;
    protected StampTextColor textColor = BLACK;

    @Transient
    @JsonIgnore
    protected String computedValue;

    // TODO make the @OrderColumn annotation works
    @JsonIgnore
    @Schema(accessMode = READ_ONLY)
    protected @Column(name = COLUMN_INDEX) Integer index = 0;


}
