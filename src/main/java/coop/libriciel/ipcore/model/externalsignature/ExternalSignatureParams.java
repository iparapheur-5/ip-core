/*
 * iparapheur Core
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package coop.libriciel.ipcore.model.externalsignature;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import coop.libriciel.ipcore.model.crypto.PdfSignaturePosition;
import coop.libriciel.ipcore.model.workflow.requests.SimpleTaskParams;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static coop.libriciel.ipcore.utils.TextUtils.RESERVED_PREFIX;
import static io.swagger.v3.oas.annotations.media.Schema.AccessMode.READ_ONLY;


@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@EqualsAndHashCode(callSuper = true)
public class ExternalSignatureParams extends SimpleTaskParams {

    private static final String EXTERNAL_SIGNATURE_METADATA_KEY = RESERVED_PREFIX + "ext_sig_";
    public static final String METADATA_EXTERNAL_SIGNATURE_KEY_MAIL = EXTERNAL_SIGNATURE_METADATA_KEY + "mail";
    public static final String METADATA_EXTERNAL_SIGNATURE_KEY_PHONE = EXTERNAL_SIGNATURE_METADATA_KEY + "phone";
    public static final String METADATA_EXTERNAL_SIGNATURE_KEY_FIRSTNAME = EXTERNAL_SIGNATURE_METADATA_KEY + "firstname";
    public static final String METADATA_EXTERNAL_SIGNATURE_KEY_LASTNAME = EXTERNAL_SIGNATURE_METADATA_KEY + "lastname";


    private String name;
    private String folderId;
    private List<SignRequestMember> members = new ArrayList<>();
    private Map<String, PdfSignaturePosition> documentIdsToSignaturePlacementMapping;

    @Schema(accessMode = READ_ONLY)
    private List<ExternalSignatureFileWrapper> fileWrappers;

}
