/*
 * iparapheur Core
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.ipcore.model.database;


import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;


@Getter
@Schema(enumAsRef = true)
public enum ExternalSignatureProvider {


    YOUSIGN,
    UNIVERSIGN,
    DOCAGE;


    @Converter(autoApply = true)
    public static class ExternalSignatureServiceNameConverter implements AttributeConverter<ExternalSignatureProvider, String> {


        @Override
        public String convertToDatabaseColumn(ExternalSignatureProvider category) {
            if (category == null) {
                return null;
            }
            return category.toString();
        }


        @Override
        public ExternalSignatureProvider convertToEntityAttribute(String name) {
            if (name == null) return null;

            return switch (name) {
                case "YOUSIGN", "yousign" -> YOUSIGN;
                case "UNIVERSIGN", "universign" -> UNIVERSIGN;
                case "DOCAGE", "docage" -> DOCAGE;
                default -> null;
            };
        }
    }


}