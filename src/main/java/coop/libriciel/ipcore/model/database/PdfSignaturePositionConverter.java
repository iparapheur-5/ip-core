/*
 * iparapheur Core
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.ipcore.model.database;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import coop.libriciel.ipcore.model.crypto.PdfSignaturePosition;
import lombok.extern.log4j.Log4j2;
import org.jooq.tools.StringUtils;
import org.springframework.web.server.ResponseStatusException;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;


@Log4j2
@Converter
public class PdfSignaturePositionConverter implements AttributeConverter<PdfSignaturePosition, String> {


    @Override
    public String convertToDatabaseColumn(PdfSignaturePosition attribute) {

        if (attribute == null) {
            return null;
        }

        try {
            return new ObjectMapper().writeValueAsString(attribute);
        } catch (JsonProcessingException exception) {
            throw new ResponseStatusException(
                    INTERNAL_SERVER_ERROR,
                    "PdfSignaturePositionConverter cannot write PdfSignaturePosition. This should not happen.",
                    exception
            );
        }
    }


    @Override
    public PdfSignaturePosition convertToEntityAttribute(String dbData) {

        if (StringUtils.isEmpty(dbData)) {
            return null;
        }

        try {
            return new ObjectMapper().readValue(dbData, PdfSignaturePosition.class);
        } catch (JsonProcessingException exception) {
            throw new ResponseStatusException(
                    INTERNAL_SERVER_ERROR,
                    "PdfSignaturePositionConverter cannot read PdfSignaturePosition. The model may have changed in a non-retro-compatible way",
                    exception
            );
        }
    }


}
