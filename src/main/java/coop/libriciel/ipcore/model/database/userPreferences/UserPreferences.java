/*
 * iparapheur Core
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.ipcore.model.database.userPreferences;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.Hibernate;
import org.hibernate.annotations.Fetch;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static coop.libriciel.ipcore.model.database.userPreferences.FolderFilter.COLUMN_ID;
import static coop.libriciel.ipcore.utils.TextUtils.UUID_STRING_SIZE;
import static javax.persistence.EnumType.STRING;
import static javax.persistence.FetchType.EAGER;
import static org.hibernate.annotations.FetchMode.SUBSELECT;


@Getter
@Setter
@Entity
@ToString
@RequiredArgsConstructor
@Table(name = UserPreferences.TABLE_NAME)
public class UserPreferences {

    public static final String TABLE_NAME = "user_preferences";
    public static final String COLUMN_USER_ID = "user_id";
    public static final String COLUMN_SHOW_DELEGATIONS = "show_delegations";
    public static final String COLUMN_SHOW_ADMIN_IDS = "show_admin_ids";
    public static final String COLUMN_TASK_VIEW_DEFAULT_PAGE_SIZE = "task_view_default_page_size";
    public static final String COLUMN_TASK_VIEW_DEFAULT_SORT_BY = "task_view_default_sort_by";
    public static final String COLUMN_TASK_VIEW_DEFAULT_ASC = "task_view_default_asc";
    public static final String COLUMN_ARCHIVE_VIEW_DEFAULT_PAGE_SIZE = "archive_view_default_page_size";
    public static final String TABLE_TASK_VIEW_COLUMNS_NAME = TABLE_NAME + "_task_view_columns";
    public static final String TABLE_ARCHIVES_COLUMNS_NAME = TABLE_NAME + "_archive_view_columns";
    public static final String TABLE_FAVORITE_DESKS = TABLE_NAME + "_favorite_desk_ids";
    public static final String TABLE_FOLDER_VIEW_BLOCKS = TABLE_NAME + "_folder_view_blocks";


    @Column(name = COLUMN_USER_ID, length = UUID_STRING_SIZE)
    private @Id String userId;

    @Column(name = COLUMN_SHOW_DELEGATIONS)
    private Boolean showDelegations = true;

    @Column(name = COLUMN_TASK_VIEW_DEFAULT_PAGE_SIZE)
    private Integer taskViewDefaultPageSize = 10;

    @Column(name = COLUMN_ARCHIVE_VIEW_DEFAULT_PAGE_SIZE)
    private Integer archiveViewDefaultPageSize = 10;

    @Enumerated(STRING)
    @Column(name = COLUMN_TASK_VIEW_DEFAULT_SORT_BY)
    private TaskViewColumn taskViewDefaultSortBy = TaskViewColumn.FOLDER_NAME;

    @Column(name = COLUMN_SHOW_ADMIN_IDS)
    private Boolean showAdminIds = false;

    @Column(name = COLUMN_TASK_VIEW_DEFAULT_ASC)
    private Boolean taskViewDefaultAsc = true;

    @OneToOne
    @JoinColumn(name = COLUMN_ID, foreignKey = @ForeignKey(name = "fk_" + COLUMN_ID))
    private FolderFilter currentFilter = new FolderFilter();

    @ElementCollection(fetch = EAGER, targetClass = FolderViewBlock.class)
    @Fetch(value = SUBSELECT)
    @Enumerated(STRING)
    @CollectionTable(
            name = TABLE_FOLDER_VIEW_BLOCKS,
            joinColumns = @JoinColumn(name = COLUMN_USER_ID),
            foreignKey = @ForeignKey(name = "fk_" + COLUMN_USER_ID)
    )
    private List<FolderViewBlock> folderViewBlockList;

    @ElementCollection(fetch = EAGER, targetClass = TaskViewColumn.class)
    @Fetch(value = SUBSELECT)
    @Enumerated(STRING)
    @CollectionTable(
            name = TABLE_TASK_VIEW_COLUMNS_NAME,
            joinColumns = @JoinColumn(name = COLUMN_USER_ID),
            foreignKey = @ForeignKey(name = "fk_" + COLUMN_USER_ID)
    )
    private List<TaskViewColumn> taskViewColumnList;

    @ElementCollection(fetch = EAGER, targetClass = ArchiveViewColumn.class)
    @Fetch(value = SUBSELECT)
    @Enumerated(STRING)
    @CollectionTable(
            name = TABLE_ARCHIVES_COLUMNS_NAME,
            joinColumns = @JoinColumn(name = COLUMN_USER_ID),
            foreignKey = @ForeignKey(name = "fk_" + COLUMN_USER_ID)
    )
    private List<ArchiveViewColumn> archiveViewColumnList;

    @ElementCollection(fetch = EAGER, targetClass = String.class)
    @Fetch(value = SUBSELECT)
    @CollectionTable(
            name = TABLE_FAVORITE_DESKS,
            joinColumns = @JoinColumn(name = COLUMN_USER_ID),
            foreignKey = @ForeignKey(name = "fk_" + COLUMN_USER_ID)
    )
    private List<String> favoriteDeskIdList = new ArrayList<>();


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        UserPreferences that = (UserPreferences) o;
        return userId != null && Objects.equals(userId, that.userId);
    }


    @Override
    public int hashCode() {
        return getClass().hashCode();
    }


}
