/*
 * iparapheur Core
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.ipcore.model.database;

import com.fasterxml.jackson.annotation.JsonIgnore;
import coop.libriciel.ipcore.model.pdfstamp.Layer;
import lombok.*;
import org.jetbrains.annotations.Nullable;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static coop.libriciel.ipcore.utils.ApiUtils.NAME_MAX_LENGTH;
import static coop.libriciel.ipcore.utils.TextUtils.UUID_STRING_SIZE;
import static javax.persistence.CascadeType.REMOVE;
import static javax.persistence.FetchType.EAGER;
import static javax.persistence.FetchType.LAZY;


@Getter
@Setter
@Entity
@ToString
@NoArgsConstructor
@Table(name = Tenant.TABLE_NAME)
public class Tenant {


    public static final String API_DOC_ID_VALUE = "Tenant id";
    public static final String API_PATH = "tenantId";

    public static final String COLUMN_ID = "id";
    public static final String COLUMN_NAME = "name";
    public static final String TABLE_NAME = "tenant";
    public static final String COLUMN_INDEX = "index";
    public static final String COLUMN_CONTENT_ID = "content_id";
    public static final String COLUMN_STATS_ID = "stats_id";

    public static final String TABLE_CUSTOM_TEMPLATES_NAME = TABLE_NAME + "_custom_templates";
    public static final String COLUMN_TENANT_ID = "tenant_id";

    @Column(name = COLUMN_ID, length = UUID_STRING_SIZE)
    protected @Id String id;

    @SuppressWarnings("DefaultAnnotationParam")
    @Column(name = COLUMN_NAME, length = NAME_MAX_LENGTH)
    protected String name;

    @JsonIgnore
    @Column(name = COLUMN_INDEX)
    private Long index = null;

    @JsonIgnore
    @Column(name = COLUMN_CONTENT_ID, length = UUID_STRING_SIZE)
    private String contentId = null;

    @JsonIgnore
    @Column(name = COLUMN_STATS_ID)
    private String statsId = null;

    /**
     * JPA Buddy doesn't support this kind of external connexion yet.
     * We'll have to wait for it, and manage sql diffs manually until then.
     *
     * @see <a href="https://youtrack.haulmont.com/issue/JPAB-806">JPA Buddy's tracked bug</a>
     */
    @ToString.Exclude
    @ElementCollection(fetch = EAGER)
    @CollectionTable(
            name = TABLE_CUSTOM_TEMPLATES_NAME,
            joinColumns = @JoinColumn(name = COLUMN_TENANT_ID),
            foreignKey = @ForeignKey(name = "fk_" + COLUMN_TENANT_ID)
    )
    private Map<TemplateType, String> customTemplates = new HashMap<>();

    /**
     * The following OneToMany fields are not columns in the Tenant table.
     * The relation is set in the child Table, with their {@link TypologyEntity#COLUMN_TENANT_ID} columns or equivalents.
     * The child-to-parent relationship is forced with the {@link OneToMany#mappedBy} annotation parameter.
     * <p>
     * Yet, the link has to be made to allow the cascade deletion.
     * That's why those lists are here, and in {@link FetchType#LAZY} mode.
     */

    @JsonIgnore
    @ToString.Exclude
    @OneToMany(fetch = LAZY, mappedBy = "tenant", cascade = REMOVE, orphanRemoval = true)
    private List<Type> typeList = new ArrayList<>();

    @JsonIgnore
    @ToString.Exclude
    @OneToMany(fetch = LAZY, mappedBy = "tenant", cascade = REMOVE, orphanRemoval = true)
    private List<ExternalSignatureConfig> externalSignatureConfigList = new ArrayList<>();

    @JsonIgnore
    @ToString.Exclude
    @OneToMany(fetch = LAZY, mappedBy = "tenant", cascade = REMOVE, orphanRemoval = true)
    private List<Metadata> metadataList = new ArrayList<>();

    @JsonIgnore
    @ToString.Exclude
    @OneToMany(fetch = LAZY, mappedBy = "tenant", cascade = REMOVE, orphanRemoval = true)
    private List<Layer> layerList = new ArrayList<>();


    @Builder
    public Tenant(@Nullable String id, @Nullable String name, @Nullable Long index, @Nullable String contentId, @Nullable String statsId) {
        this.id = id;
        this.name = name;
        this.index = index;
        this.contentId = contentId;
        this.statsId = statsId;
    }


}
