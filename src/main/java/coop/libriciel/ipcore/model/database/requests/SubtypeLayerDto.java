/*
 * iparapheur Core
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.ipcore.model.database.requests;

import com.fasterxml.jackson.annotation.JsonIgnore;
import coop.libriciel.ipcore.model.database.SubtypeLayer;
import coop.libriciel.ipcore.model.database.SubtypeLayerAssociation;
import coop.libriciel.ipcore.model.pdfstamp.LayerRepresentation;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.NoArgsConstructor;

import static io.swagger.v3.oas.annotations.media.Schema.AccessMode.READ_ONLY;
import static io.swagger.v3.oas.annotations.media.Schema.AccessMode.WRITE_ONLY;


@Data
@NoArgsConstructor
public class SubtypeLayerDto {


    /**
     * It shall not be given, in any request, but we still want this parameter internally.
     * The {@link SubtypeLayer.CompositeId} object conversion will be way easier with it.
     */
    private @JsonIgnore String subtypeId;

    @Schema(accessMode = WRITE_ONLY)
    private String layerId;

    @Schema(accessMode = READ_ONLY)
    private LayerRepresentation layer;

    private SubtypeLayerAssociation association;


    @JsonIgnore
    public SubtypeLayer.CompositeId getCompositeId() {
        return new SubtypeLayer.CompositeId(subtypeId, layerId);
    }


}
