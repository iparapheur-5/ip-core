/*
 * iparapheur Core
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.ipcore.model.database;


import com.fasterxml.jackson.annotation.JsonEnumDefaultValue;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Getter;


@Getter
@AllArgsConstructor
@Schema(enumAsRef = true)
public enum MetadataSortBy {


    @JsonEnumDefaultValue
    NAME(Metadata.COLUMN_NAME),
    ID(Metadata.COLUMN_ID),
    KEY(Metadata.COLUMN_KEY),
    INDEX(Metadata.COLUMN_INDEX);


    private final String columnName;


    public static class Constants {

        public static final String NAME_VALUE = "NAME";
        public static final String API_DOC_SORT_BY_VALUES = "Sorting properties are restricted to the MetadataSortBy enum values";

    }

}