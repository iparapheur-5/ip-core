/*
 * iparapheur Core
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.ipcore.model.database.userPreferences;

import io.swagger.v3.oas.annotations.media.Schema;

import java.util.List;


@Schema(enumAsRef = true)
public enum FolderViewBlock {


    ACTIONS,
    ANNEXE_DOCUMENTS,
    DRAFT,
    MAIN_DOCUMENTS,
    METADATA,
    PRIVATE_ANNOTATION,
    PUBLIC_ANNOTATION,
    WORKFLOW;


    public static class Constants {

        public static final List<FolderViewBlock> DEFAULT_ORDER =
                List.of(ACTIONS, DRAFT, METADATA, WORKFLOW, MAIN_DOCUMENTS, ANNEXE_DOCUMENTS, PUBLIC_ANNOTATION, PRIVATE_ANNOTATION);

    }


}
