/*
 * iparapheur Core
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.ipcore.model.database;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static coop.libriciel.ipcore.utils.TextUtils.UUID_STRING_SIZE;
import static javax.persistence.CascadeType.DETACH;
import static javax.persistence.FetchType.EAGER;
import static javax.persistence.FetchType.LAZY;
import static javax.persistence.InheritanceType.TABLE_PER_CLASS;


@Entity
@Getter
@Setter
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
@SqlResultSetMapping(
        name = ExternalSignatureConfig.SQL_MAPPING_NAME,
        classes = {
                @ConstructorResult(targetClass = ExternalSignatureConfig.class, columns = {
                        @ColumnResult(name = ExternalSignatureConfig.COLUMN_ID, type = String.class),
                        @ColumnResult(name = ExternalSignatureConfig.COLUMN_SERVICE_NAME, type = String.class),
                        @ColumnResult(name = ExternalSignatureConfig.COLUMN_NAME, type = String.class),
                        @ColumnResult(name = ExternalSignatureConfig.COLUMN_URL, type = String.class),
                        @ColumnResult(name = ExternalSignatureConfig.COLUMN_TOKEN, type = String.class),
                        @ColumnResult(name = ExternalSignatureConfig.COLUMN_PASSWORD, type = String.class),
                        @ColumnResult(name = ExternalSignatureConfig.COLUMN_LOGIN, type = String.class),
                        @ColumnResult(name = ExternalSignatureConfig.COLUMN_TENANT_ID, type = String.class),
                })
        }
)
@Inheritance(strategy = TABLE_PER_CLASS)
public class ExternalSignatureConfig {


    public static final String API_PATH = "configId";

    public static final String SQL_MAPPING_NAME = "ExternalSignatureConfig";

    // FIXME update database column names to uppercase
    public static final String COLUMN_ID = "id";
    public static final String COLUMN_SERVICE_NAME = "service_name";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_URL = "url";
    public static final String COLUMN_TRANSACTION_IDS = "transaction_ids";
    public static final String COLUMN_TOKEN = "token";
    public static final String COLUMN_PASSWORD = "password";
    public static final String COLUMN_LOGIN = "login";
    public static final String COLUMN_TENANT_ID = "tenant_id";

    public static final String TABLE_TRANSACTION_IDS_NAME = "external_signature_config_transaction_ids";
    public static final String COLUMN_EXTERNAL_SIGNATURE_CONFIG_ID = "external_signature_config_id";


    @Column(name = COLUMN_ID, length = UUID_STRING_SIZE)
    private @Id String id;

    @Column(name = COLUMN_NAME)
    private String name;

    @Column(name = COLUMN_SERVICE_NAME)
    private ExternalSignatureProvider serviceName;

    @Column(name = COLUMN_URL)
    private String url;

    @Column(name = COLUMN_TOKEN)
    private String token;

    @Column(name = COLUMN_PASSWORD)
    private String password;

    @Column(name = COLUMN_LOGIN)
    private String login;

    @ElementCollection(fetch = EAGER, targetClass = String.class)
    @CollectionTable(
            name = TABLE_TRANSACTION_IDS_NAME,
            joinColumns = @JoinColumn(name = COLUMN_EXTERNAL_SIGNATURE_CONFIG_ID),
            foreignKey = @ForeignKey(name = "fk_" + COLUMN_EXTERNAL_SIGNATURE_CONFIG_ID)
    )
    private Set<String> transactionIds = new HashSet<>();

    @JsonIgnore
    @ToString.Exclude
    @OneToMany(fetch = LAZY, mappedBy = "externalSignatureConfig", cascade = DETACH)
    private List<Subtype> subtypes = new ArrayList<>();

    @JsonIgnore
    @ToString.Exclude
    @ManyToOne(fetch = EAGER)
    @JoinColumn(name = COLUMN_TENANT_ID, foreignKey = @ForeignKey(name = "fk_" + COLUMN_TENANT_ID))
    private Tenant tenant;


}
