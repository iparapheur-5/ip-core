/*
 * iparapheur Core
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.ipcore.controller;

import coop.libriciel.ipcore.business.typology.TypologyBusinessService;
import coop.libriciel.ipcore.model.auth.Desk;
import coop.libriciel.ipcore.model.auth.DeskRepresentation;
import coop.libriciel.ipcore.model.database.*;
import coop.libriciel.ipcore.model.database.requests.SubtypeDto;
import coop.libriciel.ipcore.model.database.requests.SubtypeRepresentation;
import coop.libriciel.ipcore.model.database.requests.TypeDto;
import coop.libriciel.ipcore.model.database.requests.TypeRepresentation;
import coop.libriciel.ipcore.services.database.SubtypeRepository;
import coop.libriciel.ipcore.services.database.TypeRepository;
import coop.libriciel.ipcore.services.permission.PermissionServiceInterface;
import coop.libriciel.ipcore.services.resolvers.DeskResolver.DeskResolved;
import coop.libriciel.ipcore.services.resolvers.SubtypeResolver.SubtypeResolved;
import coop.libriciel.ipcore.services.resolvers.TenantResolver.TenantResolved;
import coop.libriciel.ipcore.services.resolvers.TypeResolver.TypeResolved;
import coop.libriciel.ipcore.utils.ApiUtils;
import coop.libriciel.ipcore.utils.PaginatedList;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.log4j.Log4j2;
import org.modelmapper.ModelMapper;
import org.springdoc.api.annotations.ParameterObject;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import java.util.Comparator;
import java.util.List;

import static coop.libriciel.ipcore.IpCoreApplication.API_V1;
import static coop.libriciel.ipcore.IpCoreApplication.HIDE_UNSTABLE_API;
import static coop.libriciel.ipcore.model.database.SubtypeMetadata.SUBTYPE_METADATA_COMPARATOR;
import static coop.libriciel.ipcore.utils.ApiUtils.*;
import static coop.libriciel.ipcore.utils.PaginatedList.*;
import static java.util.Comparator.naturalOrder;
import static java.util.Comparator.nullsFirst;
import static org.springframework.data.domain.Pageable.unpaged;
import static org.springframework.data.domain.Sort.Direction.ASC;
import static org.springframework.http.HttpStatus.OK;


@Log4j2
@RestController
@Tag(name = "typology", description = "Types and Subtypes access for a regular logged user")
@RequestMapping(API_V1 + "/tenant/{tenantId}")
public class TypologyController {


    // <editor-fold desc="Beans">

    private final ModelMapper modelMapper;
    private final PermissionServiceInterface permissionService;
    private final SubtypeRepository subtypeRepository;
    private final TypeRepository typeRepository;
    private final TypologyBusinessService typologyBusinessService;


    public TypologyController(ModelMapper modelMapper,
                              PermissionServiceInterface permissionService,
                              SubtypeRepository subtypeRepository,
                              TypeRepository typeRepository, TypologyBusinessService typologyBusinessService) {
        this.modelMapper = modelMapper;
        this.permissionService = permissionService;
        this.subtypeRepository = subtypeRepository;
        this.typeRepository = typeRepository;
        this.typologyBusinessService = typologyBusinessService;
    }


    // </editor-fold desc="Beans">


    @GetMapping("desk/{deskId}/types")
    @Operation(summary = "Get type list")
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ApiUtils.ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public Page<TypeRepresentation> getTypes(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                             @PathVariable(name = Tenant.API_PATH) String tenantId,
                                             @TenantResolved Tenant tenant,
                                             @Parameter(description = DeskRepresentation.API_DOC_ID_VALUE)
                                             @PathVariable(name = DeskRepresentation.API_PATH) String deskId,
                                             @DeskResolved Desk desk,
                                             @ParameterObject Pageable pageable) {

        log.debug("getTypes deskId:{} page:{} pageSize:{}", deskId, pageable.getPageNumber(), pageable.getPageSize());

        PageRequest typeSorting = PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(), ASC, TypologyEntity.COLUMN_NAME, TypologyEntity.COLUMN_ID);

        return typeRepository.findAllByTenantId(tenantId, typeSorting)
                .map(type -> modelMapper.map(type, TypeRepresentation.class));
    }


    @GetMapping("desk/{deskId}/types/creation-allowed")
    @Operation(hidden = HIDE_UNSTABLE_API)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "A JSON object containing pagination infos and data (a page of Type objects which this desk has creation permission for)"),
            @ApiResponse(responseCode = "401", description = "The user is not logged, or its session has expired"),
            @ApiResponse(responseCode = "403", description = "Authenticated user doesn't have desk's permission, or the desk doesn't exist"),
            @ApiResponse(responseCode = "404", description = "The given deskId does not exist")
    })
    public PaginatedList<TypeRepresentation> getCreationAllowedTypes(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                                                     @PathVariable(name = Tenant.API_PATH) String tenantId,
                                                                     @TenantResolved Tenant tenant,
                                                                     @Parameter(description = DeskRepresentation.API_DOC_ID_VALUE)
                                                                     @PathVariable(name = DeskRepresentation.API_PATH) String deskId,
                                                                     @DeskResolved Desk desk,
                                                                     @Parameter(description = API_DOC_PAGE)
                                                                     @RequestParam(required = false, defaultValue = API_DOC_PAGE_DEFAULT) int page,
                                                                     @Parameter(description = API_DOC_PAGE_SIZE)
                                                                     @RequestParam(required = false, defaultValue = API_DOC_PAGE_SIZE_DEFAULT) int pageSize) {

        log.debug("getCreationAllowedTypes deskId:{} page:{} pageSize:{}", deskId, page, pageSize);

        // Permission fetch
        // FIXME remove modelMapper when all internal actions are mapped to Desk in controllers
        List<String> subtypesIdsAllowed = permissionService.getAllowedSubtypeIds(tenantId, deskId);
        // TODO : we probably can work with the id only through the typeRepository,
        //  that would be a proper way than doing this manual pagination
        // Request
        List<Type> typesAllowed = subtypeRepository.findAllByTenant_IdAndIdIn(tenant.getId(), subtypesIdsAllowed, unpaged()).getContent().stream()
                .map(Subtype::getParentType)
                .distinct()
                .toList();

        // Sending back result

        return new PaginatedList<>(
                typesAllowed.stream()
                        .map(type -> modelMapper.map(type, TypeRepresentation.class))
                        .sorted(Comparator.comparing(TypeRepresentation::getName, nullsFirst(naturalOrder())))
                        .skip((long) page * pageSize)
                        .limit(pageSize)
                        .toList(),
                page,
                pageSize,
                typesAllowed.size()
        );
    }


    @GetMapping("typology/type/{typeId}")
    @Operation(summary = "Get a type with every informations set")
    @ResponseStatus(OK)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public TypeDto getType(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                           @PathVariable(name = Tenant.API_PATH) String tenantId,
                           @Parameter(description = Type.API_DOC_ID_VALUE)
                           @PathVariable(name = Type.API_PATH) String typeId,
                           @TypeResolved Type type) {
        return modelMapper.map(type, TypeDto.class);
    }


    @GetMapping("desk/{deskId}/types/{typeId}/subtypes")
    @Operation(summary = "Get subtype list")
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ApiUtils.ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public Page<SubtypeRepresentation> getSubtypes(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                                   @PathVariable(name = Tenant.API_PATH) String tenantId,
                                                   @Parameter(description = DeskRepresentation.API_DOC_ID_VALUE)
                                                   @PathVariable(name = DeskRepresentation.API_PATH) String deskId,
                                                   @DeskResolved Desk desk,
                                                   @Parameter(description = Type.API_DOC_ID_VALUE)
                                                   @PathVariable(name = Type.API_PATH) String typeId,
                                                   @TypeResolved Type type,
                                                   @ParameterObject Pageable pageable) {

        log.debug("getSubtypes deskId:{} page:{} pageSize:{}", deskId, pageable.getPageNumber(), pageable.getPageSize());

        PageRequest subtypeSorting = PageRequest.of(
                pageable.getPageNumber(),
                pageable.getPageSize(),
                ASC,
                TypologyEntity.COLUMN_NAME,
                TypologyEntity.COLUMN_ID
        );
        Page<Subtype> subtypes = subtypeRepository.findAllByTenant_IdAndParentType_Id(type.getTenant().getId(), type.getId(), subtypeSorting);
        subtypes.forEach(typologyBusinessService::setMaxMainDocuments);

        return subtypes.map(subtype -> modelMapper.map(subtype, SubtypeRepresentation.class));
    }


    @GetMapping("desk/{deskId}/types/{typeId}/subtypes/creation-allowed")
    @Operation(hidden = HIDE_UNSTABLE_API)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "A JSON object containing pagination infos and data (a page of Subtype objects for which this desk has creation permission)"),
            @ApiResponse(responseCode = "401", description = "The user is not logged, or its session has expired"),
            @ApiResponse(responseCode = "403", description = "Authenticated user doesn't have desk's permission, or the desk doesn't exist"),
            @ApiResponse(responseCode = "404", description = "The given Type Id doesn't exist")
    })
    public PaginatedList<SubtypeRepresentation> getCreationAllowedSubtypes(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                                                           @PathVariable(name = Tenant.API_PATH) String tenantId,
                                                                           @Parameter(description = DeskRepresentation.API_DOC_ID_VALUE)
                                                                           @PathVariable(name = DeskRepresentation.API_PATH) String deskId,
                                                                           @DeskResolved Desk desk,
                                                                           @Parameter(description = Type.API_DOC_ID_VALUE, required = true)
                                                                           @PathVariable(name = Type.API_PATH) String typeId,
                                                                           @TypeResolved Type type,
                                                                           @Parameter(description = API_DOC_PAGE)
                                                                           @RequestParam(required = false, defaultValue = API_DOC_PAGE_DEFAULT) int page,
                                                                           @Parameter(description = API_DOC_PAGE_SIZE)
                                                                           @RequestParam(required = false, defaultValue = API_DOC_PAGE_SIZE_DEFAULT) int pageSize) {

        log.debug("getCreationAllowedSubtypes deskId:{} page:{} pageSize:{}", deskId, page, pageSize);

        // Permission fetch
        // FIXME remove modelMapper when all internal actions are mapped to Desk in controllers
        List<String> subtypesAllowed = permissionService.getAllowedSubtypeIds(tenantId, deskId);
        // Actual fetch
        PageRequest subtypeSorting = PageRequest.of(page, pageSize, ASC, TypologyEntity.COLUMN_NAME, TypologyEntity.COLUMN_ID);
        Page<Subtype> subtypesLinked = subtypeRepository
                .findAllByTenant_IdAndParentType_IdAndIdIn(type.getTenant().getId(), type.getId(), subtypesAllowed, subtypeSorting);

        subtypesLinked.forEach(typologyBusinessService::setMaxMainDocuments);

        // Sending back result

        return new PaginatedList<>(
                subtypesLinked.getContent().stream().map(subtype -> modelMapper.map(subtype, SubtypeRepresentation.class)).toList(),
                page,
                pageSize,
                subtypesLinked.getTotalElements()
        );
    }


    @GetMapping("typology/type/{typeId}/subtype/{subtypeId}")
    @Operation(summary = "Get a subtype")
    @ResponseStatus(OK)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public SubtypeDto getSubtype(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                 @PathVariable(name = Tenant.API_PATH) String tenantId,
                                 @Parameter(description = Tenant.API_DOC_ID_VALUE)
                                 @PathVariable(name = Tenant.API_PATH) Tenant tenant,
                                 @Parameter(description = Type.API_DOC_ID_VALUE)
                                 @PathVariable(name = Type.API_PATH) String typeId,
                                 @PathVariable(name = Subtype.API_PATH) String subtypeId,
                                 @SubtypeResolved Subtype subtype) {

        typologyBusinessService.setMaxMainDocuments(subtype);
        subtype.getSubtypeMetadataList().sort(SUBTYPE_METADATA_COMPARATOR);

        return modelMapper.map(subtype, SubtypeDto.class);
    }


}
