/*
 * iparapheur Core
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.ipcore.controller;

import coop.libriciel.ipcore.business.auth.DeskBusinessService;
import coop.libriciel.ipcore.business.workflow.FolderBusinessService;
import coop.libriciel.ipcore.model.auth.Desk;
import coop.libriciel.ipcore.model.auth.DeskRepresentation;
import coop.libriciel.ipcore.model.auth.User;
import coop.libriciel.ipcore.model.auth.requests.DeskDto;
import coop.libriciel.ipcore.model.content.Document;
import coop.libriciel.ipcore.model.content.DocumentBuffer;
import coop.libriciel.ipcore.model.crypto.SignatureFormat;
import coop.libriciel.ipcore.model.database.Metadata;
import coop.libriciel.ipcore.model.database.MetadataRepresentation;
import coop.libriciel.ipcore.model.database.Tenant;
import coop.libriciel.ipcore.model.permission.DelegationDto;
import coop.libriciel.ipcore.model.permission.DelegationSortBy;
import coop.libriciel.ipcore.model.websockets.DeskCount;
import coop.libriciel.ipcore.model.workflow.*;
import coop.libriciel.ipcore.services.auth.AuthServiceInterface;
import coop.libriciel.ipcore.services.content.ContentServiceInterface;
import coop.libriciel.ipcore.services.crypto.CryptoServiceInterface;
import coop.libriciel.ipcore.services.database.MetadataRepository;
import coop.libriciel.ipcore.services.database.TypologyService;
import coop.libriciel.ipcore.services.permission.PermissionServiceInterface;
import coop.libriciel.ipcore.services.resolvers.CurrentUserResolver.CurrentUserResolved;
import coop.libriciel.ipcore.services.resolvers.DeskResolver.DeskResolved;
import coop.libriciel.ipcore.services.resolvers.FolderResolver.FolderResolved;
import coop.libriciel.ipcore.services.resolvers.TenantResolver.TenantResolved;
import coop.libriciel.ipcore.services.stats.StatsServiceInterface;
import coop.libriciel.ipcore.services.stats.StatsServiceInterface.GraphPeriod;
import coop.libriciel.ipcore.services.stats.StatsServiceInterface.GraphType;
import coop.libriciel.ipcore.services.workflow.WorkflowServiceInterface;
import coop.libriciel.ipcore.utils.*;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.jetbrains.annotations.NotNull;
import org.modelmapper.ModelMapper;
import org.springdoc.api.annotations.ParameterObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.reactive.function.client.WebClient;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.*;

import static coop.libriciel.ipcore.IpCoreApplication.*;
import static coop.libriciel.ipcore.model.auth.DeskRepresentation.DEFAULT_SORT_NAME;
import static coop.libriciel.ipcore.model.permission.DelegationSortBy.Constants.SUBSTITUTE_DESK_VALUE;
import static coop.libriciel.ipcore.model.permission.DelegationSortBy.generateDelegationDtoComparator;
import static coop.libriciel.ipcore.model.stats.StatsCategory.DESK;
import static coop.libriciel.ipcore.model.workflow.Action.*;
import static coop.libriciel.ipcore.model.workflow.State.CURRENT;
import static coop.libriciel.ipcore.services.resolvers.FolderResolver.FolderResolved.Permission.WORKFLOW_DESK;
import static coop.libriciel.ipcore.services.workflow.WorkflowServiceInterface.META_PREMIS_NODE_ID;
import static coop.libriciel.ipcore.utils.ApiUtils.*;
import static java.lang.Integer.MAX_VALUE;
import static java.util.Arrays.asList;
import static java.util.Collections.*;
import static java.util.Comparator.*;
import static java.util.Locale.ROOT;
import static org.springframework.data.domain.Sort.Direction.ASC;
import static org.springframework.http.HttpStatus.*;
import static org.springframework.http.MediaType.*;


@Log4j2
@RestController
@RequestMapping
@Tag(name = "desk", description = "Desk access for a regular logged user")
public class DeskController {


    /**
     * On a 30 main docs basis : (30 x 5) original detached signatures + 300 detached signatures
     * Maximum of 450 nodes in Alfresco already, just for the signature files.
     * That's good enough.
     */
    public static final int MAX_DETACHED_SIGNATURES_PER_DOCUMENT_ON_STARTUP = 5;
    public static final int MAX_DETACHED_SIGNATURES_GENERATED_PER_FOLDER = 300;


    // <editor-fold desc="Beans">


    private final AuthServiceInterface authService;
    private final ContentServiceInterface contentService;
    private final CryptoServiceInterface cryptoService;
    private final DeskBusinessService deskBusinessService;
    private final FolderBusinessService folderBusinessService;
    private final MetadataRepository metadataRepository;
    private final ModelMapper modelMapper;
    private final PermissionServiceInterface permissionService;
    private final StatsServiceInterface statsService;
    private final TypologyService typologyService;
    private final WorkflowServiceInterface workflowService;


    @Autowired
    public DeskController(AuthServiceInterface authService,
                          ContentServiceInterface contentService,
                          CryptoServiceInterface cryptoService,
                          DeskBusinessService deskBusinessService,
                          FolderBusinessService folderBusinessService,
                          MetadataRepository metadataRepository,
                          ModelMapper modelMapper,
                          PermissionServiceInterface permissionService,
                          StatsServiceInterface statsService,
                          TypologyService typologyService,
                          WorkflowServiceInterface workflowService) {
        this.authService = authService;
        this.contentService = contentService;
        this.cryptoService = cryptoService;
        this.deskBusinessService = deskBusinessService;
        this.folderBusinessService = folderBusinessService;
        this.metadataRepository = metadataRepository;
        this.modelMapper = modelMapper;
        this.permissionService = permissionService;
        this.statsService = statsService;
        this.typologyService = typologyService;
        this.workflowService = workflowService;
    }


    // </editor-fold desc="Beans">


    @InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(State.class, new EnumLowercaseConverter<>(State.class));
    }


    @GetMapping(API_V1 + "/tenant/{tenantId}/desk/{deskId}")
    @Operation(summary = "Get the target desk and its data")
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public DeskDto getDesk(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                           @PathVariable(name = Tenant.API_PATH) String tenantId,
                           @TenantResolved Tenant tenant,
                           @Parameter(description = DeskRepresentation.API_DOC_ID_VALUE)
                           @PathVariable(name = DeskRepresentation.API_PATH) String deskId,
                           @DeskResolved Desk desk) {

        log.debug("getDesk deskId:{} ", deskId);

        boolean userIsAdmin = KeycloakSecurityUtils.isSuperAdmin() || KeycloakSecurityUtils.currentUserIsTenantAdmin(tenantId);
        boolean userHasViewingRight = permissionService.currentUserHasDirectViewingRightOnSomeDeskIn(tenantId, singleton(deskId), null, null);
        if (!userIsAdmin && !userHasViewingRight) {
            throw new LocalizedStatusException(FORBIDDEN, "message.you_can_t_view_this_desk");
        }

        // Missing infos

        Set<String> deskIdSet = singleton(desk.getId());

        Map<String, Set<DelegationRule>> delegations = permissionService.getActiveDelegations(deskIdSet);
        deskBusinessService.factorizeDelegations(delegations);
        desk.getDelegationRules().addAll(MapUtils.getObject(delegations, desk.getId(), emptySet()));

        Map<DelegationRule, Integer> deskCountMap = workflowService.countFolders(deskIdSet, desk.getDelegationRules());
        log.debug("getDesks deskCountMap:{}", deskCountMap);

        boolean userHasActionRight = permissionService.currentUserHasFolderActionRightOnSomeDeskIn(tenantId, deskIdSet, null, null);
        boolean hasArchiveActionPermission = permissionService.currentUserHasArchivingRightOnSomeDeskIn(tenantId, deskIdSet, null, null);
        boolean hasChainActionPermission = permissionService.currentUserHasChainingRightOnSomeDeskIn(tenantId, deskIdSet, null, null);
        desk.setActionAllowed(desk.isActionAllowed() && userHasActionRight);
        desk.setArchivingAllowed(hasArchiveActionPermission);
        desk.setChainAllowed(hasChainActionPermission);

        return modelMapper.map(desk, DeskDto.class);
    }


    @GetMapping(API_V1 + "/tenant/{tenantId}/desk/{deskId}/folderCount")
    @Operation(summary = "Get the target desk folder count")
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public DeskCount getDeskFolderCount(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                        @PathVariable(name = Tenant.API_PATH) String tenantId,
                                        @TenantResolved Tenant tenant,
                                        @Parameter(description = DeskRepresentation.API_DOC_ID_VALUE)
                                        @PathVariable(name = DeskRepresentation.API_PATH) String deskId,
                                        @DeskResolved Desk desk) {

        log.info("getDeskCount :{} ", deskId);

        deskBusinessService.populateCountsAndDelegations(singletonMap(desk.getId(), desk), singletonList(desk));

        return deskBusinessService.getDeskCount(desk);
    }


    @GetMapping(API_INTERNAL + "/tenant/{tenantId}/desk/{deskId}/associatedDesks")
    @Operation(summary = "Get the target desk's associated ones")
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public Page<DeskRepresentation> getAssociatedDesks(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                                       @PathVariable(name = Tenant.API_PATH) String tenantId,
                                                       @TenantResolved Tenant tenant,
                                                       @Parameter(description = DeskRepresentation.API_DOC_ID_VALUE)
                                                       @PathVariable(name = DeskRepresentation.API_PATH) String deskId,
                                                       @DeskResolved Desk desk,
                                                       @PageableDefault(sort = DEFAULT_SORT_NAME)
                                                       @ParameterObject Pageable pageable,
                                                       @Parameter(description = "Searching for a specific desk name")
                                                       @RequestParam(required = false) String searchTerm) {

        this.getAssociatedDesksIntegrityChecks(tenant.getId(), desk.getId());

        List<DeskRepresentation> associatedDesks = permissionService.getAssociatedDesks(tenant.getId(), desk.getId())
                .stream().map(DeskRepresentation::new)
                .toList();

        authService.updateDeskNames(associatedDesks);

        int total = associatedDesks.size();

        if (StringUtils.isNotEmpty(searchTerm)) {
            associatedDesks = associatedDesks.stream()
                    .filter(d -> d.getName().toUpperCase(ROOT).contains(searchTerm.toUpperCase(ROOT)))
                    .toList();
        }

        List<DeskRepresentation> paginatedResult = associatedDesks
                .stream()
                .sorted(comparing(DeskRepresentation::getName, nullsLast(naturalOrder())))
                .skip((long) pageable.getPageNumber() * pageable.getPageSize())
                .limit(pageable.getPageSize())
                .toList();

        return new PageImpl<>(paginatedResult, pageable, total);
    }


    @GetMapping(API_V1 + "/tenant/{tenantId}/desk/{deskId}/filterableMetadata")
    @PreAuthorize("""
                  hasAnyRole(
                    'tenant_' + #tenantId + '_desk_' + #deskId,
                    'tenant_' + #tenantId + '_functional_admin',
                    'tenant_' + #tenantId + '_admin',
                    'admin'
                  )
                  """)
    @Operation(summary = "Get the target desk's filterable metadata", hidden = HIDE_UNSTABLE_API)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "A JSON object containing data and pagination infos"),
            @ApiResponse(responseCode = "401", description = "The user is not logged, or its session has expired"),
            @ApiResponse(responseCode = "403", description = "Authenticated user doesn't have desk's permission, or the desk doesn't exist"),
            @ApiResponse(responseCode = "404", description = "The given Tenant/Desk Id doesn't exist")
    })
    public Page<MetadataRepresentation> getFilterableMetadata(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                                              @PathVariable(name = Tenant.API_PATH) String tenantId,
                                                              @TenantResolved Tenant tenant,
                                                              @Parameter(description = DeskRepresentation.API_DOC_ID_VALUE)
                                                              @PathVariable(name = DeskRepresentation.API_PATH) String deskId,
                                                              @DeskResolved Desk desk) {

        log.debug("getFilterableMetadata tenantId:{} deskId:{}", tenant.getId(), deskId);

        // Fetch metadata

        PageRequest pageSort = PageRequest.of(0, MAX_VALUE, ASC, Metadata.COLUMN_INDEX, Metadata.COLUMN_NAME, Metadata.COLUMN_KEY, Metadata.COLUMN_ID);
        List<String> metadataIds = permissionService.getFilterableMetadataIds(tenantId, deskId);
        Page<MetadataRepresentation> result = metadataRepository.findAllByTenant_IdAndIdIn(tenant.getId(), metadataIds, pageSort);

        log.trace("getFilterableMetadata ids:{} result:{}", metadataIds, result);
        return result;
    }


    @PutMapping(API_V1 + "/tenant/{tenantId}/desk/{deskId}/finished/{folderId}")
    @Operation(summary = "Archive", description = "Archive the given Folder.", hidden = HIDE_UNSTABLE_API)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Action performed"),
            @ApiResponse(responseCode = "401", description = "The user is not logged, or its session has expired"),
            @ApiResponse(responseCode = "403", description = "Authenticated user doesn't have desk's role of given desk.\n(You might want to set it in Keycloak)"),
            @ApiResponse(responseCode = "404", description = "The given folderId or deskId does not exist"),
            @ApiResponse(responseCode = "409", description = "The given folder is not archivable yet")
    })
    public void archiveFolder(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                              @PathVariable(name = Tenant.API_PATH) String tenantId,
                              @TenantResolved Tenant tenant,
                              @Parameter(description = DeskRepresentation.API_DOC_ID_VALUE, required = true)
                              @PathVariable String deskId,
                              @Parameter(description = Folder.API_DOC_ID_VALUE)
                              @PathVariable(name = Folder.API_PATH) String folderId,
                              @FolderResolved(permission = WORKFLOW_DESK) Folder folder,
                              @CurrentUserResolved User currentUser) {

        log.info("requestArchive folderId:{}", folderId);

        // Integrity check

        if (folder.getStepList().stream()
                .filter(t -> asList(DELETE, ARCHIVE).contains(t.getAction()))
                .noneMatch(t -> t.getState() == CURRENT)) {
            throw new LocalizedStatusException(CONFLICT, "message.this_workflow_is_not_finished_yet");
        }

        boolean hasArchiveActionPermission = permissionService.currentUserHasArchivingRightOnSomeDeskIn(tenantId, singleton(deskId), null, null);

        if (!StringUtils.equals(deskId, folder.getFinalDesk().getId()) || !hasArchiveActionPermission) {
            throw new LocalizedStatusException(NOT_ACCEPTABLE, "message.you_can_t_archive_this_folder_from_this_desk");
        }

        log.debug("requestArchive folderId:{} contentId:{}", folder.getId(), folder.getContentId());

        // Setting up some dummy archive's Task action data, locally, prior to the real Archive action.
        // We need those for the PREMIS generation, yet we want to perform the real task only if everything went well.
        // In case of error, the actual workflow won't be locked.

        Task finalTask = folderBusinessService.prepareFinalTaskForPremisFile(folder.getStepList(), currentUser);

        // Compute missing document infos

        List<Document> documentList = contentService.getDocumentList(folder.getContentId());
        folder.setDocumentList(documentList);

        typologyService.updateTypology(singletonList(folder));

        // It should not be allowed (it is hidden in the UI) to archive when the type/subtype is missing
        // Nevertheless, we cannot be sure about webservices, it IS handled down the road and I don't like NPEs so...
        if (folder.getType() != null) {
            SignatureFormat signatureFormat = cryptoService.getSignatureFormat(folder.getType(), folder.getDocumentList());
            folder.getType().setSignatureFormat(signatureFormat);
        }

        folder.getDocumentList()
                .stream()
                .filter(d -> d.getMediaVersion() == null)
                .filter(d -> d.getMediaType() != null)
                // Retrieving PDF version
                .forEach(d -> {
                    switch (d.getMediaType().toString()) {
                        case APPLICATION_PDF_VALUE -> {
                            try (InputStream ios = contentService.retrievePipedDocument(d.getId());
                                 PDDocument doc = PDDocument.load(ios)) {
                                d.setMediaType(APPLICATION_PDF);
                                d.setMediaVersion(doc.getVersion());
                            } catch (IOException e) {
                                throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, e, "message.error_retrieving_PDF_version");
                            }
                        }
                        case APPLICATION_XML_VALUE, TEXT_XML_VALUE -> d.setMediaVersion(1.0F); // TODO : parse real XML version
                    }
                });

        folderBusinessService.prepareChecksumDataForPremisFile(folder.getDocumentList());

        // Actual Premis generation

        String premisContent = folderBusinessService.generatePremisContent(folder);
        String premisNodeId = contentService.createPremisDocument(folder, premisContent);

        FolderDto editRequest = new FolderDto();
        editRequest.setMetadata(Map.of(META_PREMIS_NODE_ID, premisNodeId));
        workflowService.editFolder(folder.getId(), editRequest);

        folder.setPremisNodeId(premisNodeId);

        // Actually perform the workflow task, once everything went well.

        workflowService.performTask(finalTask, ARCHIVE, folder, deskId, null, null, null);

        // Stats

        Long timeToCompleteInHours = statsService.computeTimeToCompleteInHours(finalTask);
        statsService.registerFolderAction(tenant, ARCHIVE, folder, folder.getFinalDesk(), timeToCompleteInHours);
    }


    @GetMapping(API_V1 + "/tenant/{tenantId}/desk/{deskId}/stats")
    @Operation(summary = "Stats", description = "Get the chart", hidden = HIDE_UNSTABLE_API)
    @ResponseStatus(OK)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Action performed"),
            @ApiResponse(responseCode = "401", description = "The user is not logged, or its session has expired"),
            @ApiResponse(responseCode = "403", description = "Authenticated user doesn't have desk's role of given desk.\n(You might want to set it in Keycloak)"),
            @ApiResponse(responseCode = "404", description = "The given folderId or deskId does not exist")
    })
    public void getStatsChart(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                              @PathVariable(name = Tenant.API_PATH) String tenantId,
                              @TenantResolved Tenant tenant,
                              @Parameter(description = DeskRepresentation.API_DOC_ID_VALUE)
                              @PathVariable(name = DeskRepresentation.API_PATH) String deskId,
                              @DeskResolved Desk desk,
                              @RequestParam String startDate,
                              @RequestParam String endDate,
                              @RequestParam int width,
                              @RequestParam int height,
                              @RequestParam(required = false, defaultValue = "LINES") GraphType graphType,
                              @RequestParam(required = false, defaultValue = "DAY") GraphPeriod periodicity,
                              @NotNull HttpServletResponse response) {
        log.debug("getStatsChart deskId:{}", deskId);

        // Retrieve image

        URI imageUrl;
        try {
            imageUrl = new URI(statsService.getActionChartUrl(tenant, deskId, startDate, endDate, width, height, graphType, periodicity));
        } catch (URISyntaxException e) {
            throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, e, "message.cannot_reach_stats_service");
        }

        log.debug("Image URL = " + imageUrl);

        DocumentBuffer result = new DocumentBuffer();
        response.setStatus(OK.value());
        result.setContentFlux(WebClient.builder().clientConnector(new ForcedExceptionsConnector()).build()
                .get().uri(imageUrl)
                .accept(APPLICATION_OCTET_STREAM)
                .exchangeToFlux(rep -> RequestUtils.clientResponseToDataBufferFlux(rep, result, "statsChart"))
                // Result
                .doOnError(e -> {throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, e, "message.cannot_reach_stats_service");})
        );

        RequestUtils.writeBufferToResponse(result, response);
    }


    // <editor-fold desc="Delegations CRUDL">


    @PostMapping(API_V1 + "/tenant/{tenantId}/desk/{deskId}/delegation")
    @Operation(summary = "Create a new delegation (active or planned) from target desk")
//    @PreAuthorize("hasAnyRole('admin', 'tenant_' + #tenantId + '_admin')")
    @ResponseStatus(CREATED)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_201),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public void createDelegation(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                 @PathVariable(name = Tenant.API_PATH) String tenantId,
                                 @TenantResolved Tenant tenant,
                                 @Parameter(description = DeskRepresentation.API_DOC_ID_VALUE)
                                 @PathVariable(name = DeskRepresentation.API_PATH) String deskId,
                                 @Parameter(description = DelegationDto.API_DOC, required = true)
                                 @RequestBody DelegationDto delegationDto) {

        log.info("createDelegation delegating:{} substitute:{}", deskId, delegationDto.getSubstituteDeskId());
        log.info("                 type:{} subtype:{}", delegationDto.getTypeId(), delegationDto.getSubtypeId());

        if (StringUtils.equals(deskId, delegationDto.getSubstituteDeskId())) {
            throw new LocalizedStatusException(NOT_ACCEPTABLE, "message.redundant_delegation_target");
        }

        permissionService.addDelegation(
                tenant.getId(),
                delegationDto.getSubstituteDeskId(),
                deskId,
                delegationDto.getTypeId(),
                delegationDto.getSubtypeId(),
                delegationDto.getStart(),
                delegationDto.getEnd()
        );

        statsService.registerAdminAction(tenant, DESK, UPDATE, deskId);
    }


    @DeleteMapping(API_V1 + "/tenant/{tenantId}/desk/{deskId}/delegation/{delegationId}")
    @Operation(summary = "Remove an active or planned delegation from target Desk")
//    @PreAuthorize("hasAnyRole('admin', 'tenant_' + #tenantId + '_admin')")
    @ResponseStatus(NO_CONTENT)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_204),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public void deleteDelegation(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                 @PathVariable(name = Tenant.API_PATH) String tenantId,
                                 @TenantResolved Tenant tenant,
                                 @Parameter(description = DeskRepresentation.API_DOC_ID_VALUE)
                                 @PathVariable(name = DeskRepresentation.API_PATH) String deskId,
                                 @Parameter(description = DelegationDto.API_DOC_ID_VALUE)
                                 @PathVariable(name = DelegationDto.API_PATH) String delegationId,
                                 @Parameter(description = DelegationDto.API_DOC, required = true)
                                 @RequestBody DelegationDto delegationDto) {

        log.info("deleteDelegation tenantId:{} deskId:{} substituteDeskId:{}", tenantId, deskId, delegationDto.getSubstituteDesk());

        permissionService.deleteDelegation(
                delegationId,
                tenant.getId(),
                delegationDto.getSubstituteDeskId(),
                deskId,
                delegationDto.getTypeId(),
                delegationDto.getSubtypeId(),
                delegationDto.getStart(),
                delegationDto.getEnd()
        );

        statsService.registerAdminAction(tenant, DESK, UPDATE, deskId);
    }


    @GetMapping(API_V1 + "/tenant/{tenantId}/desk/{deskId}/delegation")
    @Operation(summary = "List delegations (active and planned) for the given delegating desk", description = DelegationSortBy.Constants.API_DOC_SORT_BY_VALUES)
//    @PreAuthorize("hasAnyRole('admin', 'tenant_' + #tenantId + '_admin')")
//    @PreAuthorize("hasRole('" + DESK_ROLE_PREFIX + "' + #deskId)")
    @ResponseStatus(OK)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_400, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public Page<DelegationDto> listDelegations(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                               @PathVariable(name = Tenant.API_PATH) String tenantId,
                                               @TenantResolved Tenant tenant,
                                               @Parameter(description = DeskRepresentation.API_DOC_ID_VALUE)
                                               @PathVariable(name = DeskRepresentation.API_PATH) String deskId,
                                               @DeskResolved Desk desk,
                                               @PageableDefault(sort = SUBSTITUTE_DESK_VALUE)
                                               @ParameterObject Pageable pageable) {

        log.debug("listDelegations tenantId:{} deskId:{}", tenantId, deskId);

        List<DelegationDto> delegationsDtoList = permissionService.getDelegations(tenant.getId(), deskId);
        delegationsDtoList.forEach(dto -> dto.setDelegatingDeskId(deskId)); // TODO : Fix this hardcoded fix
        deskBusinessService.updateInnerValues(tenantId, delegationsDtoList);

        // Manual sort

        boolean asc = pageable.getSort().stream().findFirst().map(order -> order.getDirection() == ASC).orElse(true);
        String sortBySerialized = pageable.getSort().stream().findFirst().map(Sort.Order::getProperty).orElse(null);
        DelegationSortBy sortBy = Optional.ofNullable(sortBySerialized)
                .map(DelegationSortBy::valueOf)
                .orElseThrow(() -> new LocalizedStatusException(BAD_REQUEST, "message.unknown_sort_by_enum_value_s", sortBySerialized));

        Comparator<DelegationDto> comparator = generateDelegationDtoComparator(sortBy, asc);

        PageImpl<DelegationDto> result = new PageImpl<>(
                delegationsDtoList.stream()
                        .sorted(comparator)
                        .skip(pageable.getOffset())
                        .limit(pageable.getPageSize())
                        .toList(),
                pageable,
                delegationsDtoList.size()
        );

        log.debug("listDelegations result:{}", result);
        return result;
    }


    // </editor-fold desc="Delegations CRUDL">


    private void getAssociatedDesksIntegrityChecks(String tenantId, String deskId) {
        boolean userIsAdmin = KeycloakSecurityUtils.isSuperAdmin() || KeycloakSecurityUtils.currentUserIsTenantAdmin(tenantId);
        String userId = KeycloakSecurityUtils.getCurrentSessionUserId();

        boolean userIsDelegationManager = permissionService.getDelegationManagedDesks(userId, tenantId)
                .stream()
                .map(DeskRepresentation::getId)
                .anyMatch(id -> StringUtils.equals(deskId, id));

        boolean userIsFunctionalAdmin = permissionService.getAdministeredDesks(userId, tenantId)
                .stream()
                .map(DeskRepresentation::getId)
                .anyMatch(id -> StringUtils.equals(deskId, id));

        boolean userIsDeskOwner = KeycloakSecurityUtils.getCurrentUserDeskIds().contains(deskId);

        if (!userIsAdmin && !userIsDelegationManager && !userIsFunctionalAdmin && !userIsDeskOwner) {
            throw new LocalizedStatusException(FORBIDDEN, "message.you_can_t_view_this_desk");
        }
    }


}
