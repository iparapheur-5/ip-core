/*
 * iparapheur Core
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package coop.libriciel.ipcore.controller.admin;

import coop.libriciel.ipcore.business.workflow.WorkflowBusinessService;
import coop.libriciel.ipcore.model.auth.DeskRepresentation;
import coop.libriciel.ipcore.model.database.Subtype;
import coop.libriciel.ipcore.model.database.Tenant;
import coop.libriciel.ipcore.model.workflow.*;
import coop.libriciel.ipcore.services.auth.AuthServiceInterface;
import coop.libriciel.ipcore.services.database.SubtypeRepository;
import coop.libriciel.ipcore.services.resolvers.TenantResolver.TenantResolved;
import coop.libriciel.ipcore.services.resolvers.WorkflowDefinitionResolver.WorkflowDefinitionResolved;
import coop.libriciel.ipcore.services.stats.StatsServiceInterface;
import coop.libriciel.ipcore.services.workflow.WorkflowServiceInterface;
import coop.libriciel.ipcore.utils.LocalizedStatusException;
import coop.libriciel.ipcore.utils.RequestUtils;
import coop.libriciel.ipcore.utils.XmlUtils;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.modelmapper.ModelMapper;
import org.springdoc.api.annotations.ParameterObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.sax.SAXResult;
import javax.xml.transform.sax.SAXTransformerFactory;
import javax.xml.transform.sax.TransformerHandler;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.*;
import java.util.stream.IntStream;

import static coop.libriciel.ipcore.IpCoreApplication.API_V1;
import static coop.libriciel.ipcore.IpCoreApplication.HIDE_UNSTABLE_API;
import static coop.libriciel.ipcore.model.stats.StatsCategory.WORKFLOW;
import static coop.libriciel.ipcore.model.workflow.Action.*;
import static coop.libriciel.ipcore.model.workflow.WorkflowDefinition.BOSS_OF_ID_PLACEHOLDER;
import static coop.libriciel.ipcore.model.workflow.WorkflowDefinition.GENERIC_VALIDATORS_IDS_PLACEHOLDERS;
import static coop.libriciel.ipcore.utils.ApiUtils.*;
import static coop.libriciel.ipcore.utils.XmlUtils.OUTPUTKEYS_INDENT_AMOUNT;
import static java.nio.charset.StandardCharsets.UTF_8;
import static java.util.stream.Collectors.toSet;
import static javax.xml.XMLConstants.*;
import static javax.xml.transform.OutputKeys.*;
import static org.apache.commons.lang3.StringUtils.EMPTY;
import static org.springframework.data.domain.Pageable.unpaged;
import static org.springframework.http.HttpStatus.*;


@Log4j2
@RestController
@RequestMapping
@Tag(name = "admin-workflow-definition", description = "Reserved operations on workflow definitions")
public class AdminWorkflowDefinitionController {


    // <editor-fold desc="Beans">


    private final AuthServiceInterface authService;
    private final ModelMapper modelMapper;
    private final StatsServiceInterface statsService;
    private final SubtypeRepository subtypeRepository;
    private final WorkflowBusinessService workflowBusinessService;
    private final WorkflowServiceInterface workflowService;


    @Autowired
    public AdminWorkflowDefinitionController(AuthServiceInterface authService,
                                             ModelMapper modelMapper,
                                             StatsServiceInterface statsService,
                                             SubtypeRepository subtypeRepository,
                                             WorkflowServiceInterface workflowService,
                                             WorkflowBusinessService workflowBusinessService) {
        this.authService = authService;
        this.modelMapper = modelMapper;
        this.statsService = statsService;
        this.subtypeRepository = subtypeRepository;
        this.workflowService = workflowService;
        this.workflowBusinessService = workflowBusinessService;
    }


    // </editor-fold desc="Beans">


    // <editor-fold desc="Utils">


    static @NotNull String patchBpmn(ClassLoader loader, InputStream bpmnInputStream) throws TransformerException {

        SAXTransformerFactory stf = (SAXTransformerFactory) TransformerFactory.newInstance();
        stf.setAttribute(ACCESS_EXTERNAL_DTD, EMPTY);
        stf.setAttribute(ACCESS_EXTERNAL_STYLESHEET, EMPTY);
        stf.setFeature(FEATURE_SECURE_PROCESSING, true);

        TransformerHandler th1 = stf.newTransformerHandler(new StreamSource(loader.getResourceAsStream("bpmn/patches/add_flowable_xmlns.xslt")));
        TransformerHandler th2 = stf.newTransformerHandler(new StreamSource(loader.getResourceAsStream("bpmn/patches/set_process_executable.xslt")));
        TransformerHandler th3 = stf.newTransformerHandler(new StreamSource(loader.getResourceAsStream("bpmn/patches/callActivity_attributes.xslt")));
        TransformerHandler th4 = stf.newTransformerHandler(new StreamSource(loader.getResourceAsStream("bpmn/patches/callActivity_candidates.xslt")));
        TransformerHandler th5 = stf.newTransformerHandler(new StreamSource(loader.getResourceAsStream("bpmn/patches/callActivity_undo.xslt")));
        TransformerHandler th6 = stf.newTransformerHandler(new StreamSource(loader.getResourceAsStream("bpmn/patches/one_liner.xslt")));

        // There are some differences between OracleJDK and OpenJDK results.
        // Removing new lines (with the one_liner.xslt, and re-indenting it makes the results unit-tests friendly.

        TransformerHandler th7 = stf.newTransformerHandler();
        th7.getTransformer().setOutputProperty(METHOD, "xml");
        th7.getTransformer().setOutputProperty(ENCODING, UTF_8.name());
        th7.getTransformer().setOutputProperty(INDENT, "yes");
        th7.getTransformer().setOutputProperty(OUTPUTKEYS_INDENT_AMOUNT, "2");

        // Building the chain of transformers, and feeding the inputStream

        ByteArrayOutputStream baos = new ByteArrayOutputStream();

        th1.setResult(new SAXResult(th2));
        th2.setResult(new SAXResult(th3));
        th3.setResult(new SAXResult(th4));
        th4.setResult(new SAXResult(th5));
        th5.setResult(new SAXResult(th6));
        th6.setResult(new SAXResult(th7));
        th7.setResult(new StreamResult(baos));

        stf.newTransformer().transform(new StreamSource(bpmnInputStream), new SAXResult(th1));

        // There are some differences between OracleJDK and OpenJDK results.
        // Canonicalization makes the results unit-tests friendly.
        String canonicalized = XmlUtils.canonicalize(baos.toByteArray());

        // Deleting line feeds, from https://bpmn.io/
        canonicalized = StringUtils.replace(canonicalized, "&#xA;\"", "\"");

        return canonicalized;
    }


    static boolean isParallelStep(@Nullable StepDefinition stepDefinition) {
        return Optional.ofNullable(stepDefinition)
                .filter(s -> CollectionUtils.isNotEmpty(s.getValidatingDesks()))
                .filter(s -> s.getValidatingDesks().size() > 1)
                .isPresent();
    }


    static void checkDefinitionIntegrity(WorkflowDefinition definition) {

        // Nullity checks

        if (definition.getSteps() == null) {
            throw new LocalizedStatusException(NOT_ACCEPTABLE, "message.steps_cannot_be_null");
        }

        if (definition.getSteps().stream().anyMatch(Objects::isNull)) {
            throw new LocalizedStatusException(NOT_ACCEPTABLE, "message.steps_cannot_be_null");
        }

        if (definition.getSteps().stream().map(StepDefinition::getValidatingDesks).anyMatch(Objects::isNull)) {
            throw new LocalizedStatusException(NOT_ACCEPTABLE, "message.validator_ids_cannot_be_null");
        }

        if (definition.getSteps().stream().flatMap(s -> s.getValidatingDesks().stream()).anyMatch(Objects::isNull)) {
            throw new LocalizedStatusException(NOT_ACCEPTABLE, "message.validator_ids_cannot_be_null");
        }

        if (definition.getSteps().stream().flatMap(s -> s.getValidatingDesks().stream()).map(DeskRepresentation::getId).anyMatch(StringUtils::isEmpty)) {
            throw new LocalizedStatusException(NOT_ACCEPTABLE, "message.validator_ids_cannot_be_null");
        }

        // Disabling variable desks in parallel steps (for now)

        definition.getSteps().stream()
                .filter(AdminWorkflowDefinitionController::isParallelStep)
                .map(s -> s.getValidatingDesks().stream().map(DeskRepresentation::getId).toList())
                .filter(s -> GENERIC_VALIDATORS_IDS_PLACEHOLDERS.stream().anyMatch(s::contains))
                .findAny()
                .ifPresent(s -> {throw new LocalizedStatusException(NOT_ACCEPTABLE, "message.parallel_steps_cannot_contain_variable_desks");});

        // Cannot have a BOSS OF step after a parallel one

        IntStream.range(1, definition.getSteps().size())
                .filter(i -> definition.getSteps().get(i).getValidatingDesks().stream()
                        .map(DeskRepresentation::getId)
                        .anyMatch(id -> StringUtils.equals(BOSS_OF_ID_PLACEHOLDER, id))
                )
                .filter(i -> isParallelStep(definition.getSteps().get(i - 1)))
                .findAny()
                .ifPresent(s -> {throw new LocalizedStatusException(NOT_ACCEPTABLE, "message.boss_of_generic_step_cannot_be_after_a_parallel_one");});
    }


    void verifyEachTargetDeskExistence(@RequestBody WorkflowDefinition definition) {

        Set<DeskRepresentation> targetDesks = new HashSet<>();
        targetDesks.add(definition.getFinalDesk());
        targetDesks.addAll(definition.getFinalNotifiedDesks());
        targetDesks.addAll(definition.getSteps().stream().flatMap(s -> s.getValidatingDesks().stream()).collect(toSet()));
        targetDesks.addAll(definition.getSteps().stream().flatMap(s -> s.getNotifiedDesks().stream()).collect(toSet()));

        Set<String> targetDeskIds = targetDesks.stream()
                .map(DeskRepresentation::getId)
                .filter(deskId -> !GENERIC_VALIDATORS_IDS_PLACEHOLDERS.contains(deskId))
                .collect(toSet());

        Map<String, String> existingDeskNames = authService.getDeskNames(targetDeskIds);

        targetDeskIds.stream()
                .filter(i -> existingDeskNames.get(i) == null)
                .findFirst()
                .ifPresent(i -> {throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.s_is_not_a_known_desk_id_or_name", i);});

        // Since we have those, we can update them...

        targetDesks.forEach(deskRepresentation -> deskRepresentation.setName(existingDeskNames.get(deskRepresentation.getId())));
    }


    // </editor-fold desc="Utils">


    // <editor-fold desc="WorkflowDefinition CRUDL">


//    @PostMapping(API_V1 + "/admin/tenant/{tenantId}")
//    @Operation(summary = "Create a workflow definition")
//    @PreAuthorize("hasAnyRole('admin', 'tenant_' + #tenantId + '_admin')")
//    @ResponseStatus(CREATED)
//    @ApiResponses(value = {
//            @ApiResponse(responseCode = "201", description = "Created"),
//            @ApiResponse(responseCode = "401", description = "The user is not logged, or its session has expired"),
//            @ApiResponse(responseCode = "403", description = "Authenticated user doesn't have admin role.\n(You might want to set it in Keycloak)"),
//            @ApiResponse(responseCode = "404", description = "(At least) one of the desk called in the BPMN doesn't exist")
//    })
//    public void createWorkflowDefinition(@RequestParam MultipartFile bpmnXmlFile,
//                                         @RequestParam(required = false) String workflowName) {
//
//        // Patch the given XML to Flowable parameters
//
//        String bpmn;
//        try {
//            bpmn = IOUtils.toString(bpmnXmlFile.getInputStream(), UTF_8);
//            log.debug("BPMN definition :\n{}", bpmn);
//        } catch (IOException e) {
//            log.error("Error on WorkflowDefinition creation", e);
//            return;
//        }
//
//        // Retrieving desk names and IDs
//
//        Set<String> desks = new HashSet<>();
//        Matcher m = Pattern
//                .compile("<.*?callActivity.*?name=\"(?:visa|signature) (.*?)\"")
//                .matcher(bpmn);
//
//        while (m.find()) {
//            desks.add(m.group(1));
//        }
//
//        // Mapping to desks Ids from Keycloak
//
//        log.info("BPMN desks to patch:" + desks);
//
//        for (String desk : desks) {
//
//            String deskId = Optional.ofNullable(authService.findDeskById(desk))
//                    .or(() -> Optional.ofNullable(authService.findDeskByName(desk)))
//                    .orElseThrow(() -> new ResponseStatusException(NOT_FOUND, String.format("The desk named '%s' is neither a deskId nor a deskName", desk)))
//                    .getId();
//
//            bpmn = bpmn.replaceAll(
//                    "(<.*?callActivity.*?name=\")(visa|signature) " + desk + "\"",
//                    "$1$2 " + deskId + "\""
//            );
//
//            log.debug("Replacing {} with {} done", desk, deskId);
//        }
//
//        log.info("BPMN after patch :\n" + bpmn);
//
//        // Upload to Flowable
//
//        String workflowDefinitionName = ObjectUtils.firstNonNull(workflowName, bpmnXmlFile.getOriginalFilename(), bpmnXmlFile.getName());
//        workflowService.createWorkflowDefinition(bpmn, workflowDefinitionName);
//        statsService.registerAdminAction(WORKFLOW, CREATE, workflowDefinitionName);
//    }


    @PostMapping(API_V1 + "/admin/tenant/{tenantId}/workflowDefinition")
    @Operation(summary = "Create a workflow definition")
    @PreAuthorize("hasAnyRole('admin', 'tenant_' + #tenantId + '_admin')")
    @ResponseStatus(CREATED)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_400, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_409, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public WorkflowDefinitionDto createWorkflowDefinition(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                                          @PathVariable(name = Tenant.API_PATH) String tenantId,
                                                          @TenantResolved Tenant tenant,
                                                          @RequestBody @Valid WorkflowDefinitionDto definition) {

        log.info("createWorkflowDefinition id:{} key:{} name:{}", definition.getId(), definition.getKey(), definition.getName());
        WorkflowDefinition workflowDefinition = modelMapper.map(definition, WorkflowDefinition.class);

        // Integrity check

        checkDefinitionIntegrity(workflowDefinition);
        verifyEachTargetDeskExistence(workflowDefinition);

        if (workflowService.listWorkflowDefinitions(tenant.getId(), unpaged(), workflowDefinition.getName())
                .getContent()
                .stream()
                .anyMatch(w -> StringUtils.equals(w.getName(), definition.getName()))) {
            throw new LocalizedStatusException(CONFLICT, "message.this_workflow_name_already_exists");
        }

        if (workflowService.getWorkflowDefinitionByKey(tenant.getId(), workflowDefinition.getKey(), null).isPresent()) {
            throw new LocalizedStatusException(CONFLICT, "message.this_workflow_key_already_exists");
        }

        if (workflowDefinition.getSteps().stream()
                .filter(s -> List.of(SECURE_MAIL, IPNG, EXTERNAL_SIGNATURE).contains(s.getType()))
                .anyMatch(AdminWorkflowDefinitionController::isParallelStep)) {
            throw new LocalizedStatusException(NOT_ACCEPTABLE, "message.external_actions_cannot_be_parallel");
        }

        // Actual save

        workflowService.createWorkflowDefinition(tenant.getId(), workflowDefinition);
        statsService.registerAdminAction(tenant, WORKFLOW, CREATE, workflowDefinition.getKey());
        return modelMapper.map(workflowDefinition, WorkflowDefinitionDto.class);
    }


    @GetMapping(API_V1 + "/admin/tenant/{tenantId}/workflowDefinition/{workflowDefinitionId}")
    @Operation(summary = "Get an existing workflow definition by id, with every information set")
    @PreAuthorize("hasAnyRole('admin', 'tenant_' + #tenantId + '_admin')")
    @ResponseStatus(OK)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public WorkflowDefinitionDto getWorkflowDefinitionById(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                                           @PathVariable(name = Tenant.API_PATH) String tenantId,
                                                           @TenantResolved Tenant tenant,
                                                           @Parameter(description = WorkflowDefinition.API_ID_DOC_VALUE)
                                                           @PathVariable(name = WorkflowDefinition.API_ID_PATH) String workflowDefinitionId,
                                                           @WorkflowDefinitionResolved WorkflowDefinition workflowDefinition) {
        log.debug("getWorkflowDefinitionById tenantId:{} id:{}", tenantId, workflowDefinitionId);
        workflowDefinition = workflowBusinessService.retrieveDesksNames(tenant, workflowDefinition);
        log.debug("getWorkflowDefinitionById finalDesk:{} finalNotifiedDesks:{}",
                workflowDefinition.getFinalDesk(),
                workflowDefinition.getFinalNotifiedDesks());
        return modelMapper.map(workflowDefinition, WorkflowDefinitionDto.class);
    }


    @GetMapping(API_V1 + "/admin/tenant/{tenantId}/workflowDefinitionByKey/{key}")
    @Operation(summary = "Get an existing workflow definition by key, with every information set")
    @PreAuthorize("hasAnyRole('admin', 'tenant_' + #tenantId + '_admin')")
    @ResponseStatus(OK)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public WorkflowDefinitionDto getWorkflowDefinitionByKey(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                                            @PathVariable(name = Tenant.API_PATH) String tenantId,
                                                            @TenantResolved Tenant tenant,
                                                            @PathVariable String key) {
        log.debug("getWorkflowDefinitionByKey tenantId:{} key:{}", tenantId, key);
        WorkflowDefinition workflowDefinition = workflowBusinessService.getWorkflowDefinitionByKey(tenant, key, null);
        return modelMapper.map(workflowDefinition, WorkflowDefinitionDto.class);
    }


    @PutMapping(API_V1 + "/admin/tenant/{tenantId}/workflowDefinition/{workflowDefinitionId}")
    @Operation(summary = "Update an existing workflow definition")
    @PreAuthorize("hasAnyRole('admin', 'tenant_' + #tenantId + '_admin')")
    @ResponseStatus(OK)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public WorkflowDefinitionDto updateWorkflowDefinition(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                                          @PathVariable(name = Tenant.API_PATH) String tenantId,
                                                          @TenantResolved Tenant tenant,
                                                          @Parameter(description = WorkflowDefinition.API_ID_DOC_VALUE)
                                                          @PathVariable(name = WorkflowDefinition.API_ID_PATH) String workflowDefinitionId,
                                                          @WorkflowDefinitionResolved WorkflowDefinition existingWorkflowDefinition,
                                                          @RequestBody @Valid WorkflowDefinitionDto definition) {

        log.info("updateWorkflowDefinition id:{} def:{}", workflowDefinitionId, definition);
        WorkflowDefinition updatedWorkflowDefinition = modelMapper.map(definition, WorkflowDefinition.class);

        // Integrity check

        checkDefinitionIntegrity(updatedWorkflowDefinition);
        verifyEachTargetDeskExistence(updatedWorkflowDefinition);

        if (!StringUtils.equals(existingWorkflowDefinition.getKey(), definition.getKey())) {
            throw new LocalizedStatusException(NOT_ACCEPTABLE, "message.workflow_keys_are_immutable");
        }

        if (updatedWorkflowDefinition.getSteps().stream()
                .filter(s -> List.of(SECURE_MAIL, IPNG, EXTERNAL_SIGNATURE).contains(s.getType()))
                .anyMatch(AdminWorkflowDefinitionController::isParallelStep)) {
            throw new LocalizedStatusException(NOT_ACCEPTABLE, "message.external_actions_cannot_be_parallel");
        }

        // Actual save

        workflowService.updateWorkflowDefinition(tenant.getId(), workflowDefinitionId, updatedWorkflowDefinition);
        statsService.registerAdminAction(tenant, WORKFLOW, UPDATE, updatedWorkflowDefinition.getKey());

        return modelMapper.map(updatedWorkflowDefinition, WorkflowDefinitionDto.class);
    }


    @DeleteMapping(API_V1 + "/admin/tenant/{tenantId}/workflowDefinition/{workflowDefinitionKey}")
    @Operation(summary = "Delete a workflow definition")
    @PreAuthorize("hasAnyRole('admin', 'tenant_' + #tenantId + '_admin')")
    @ResponseStatus(NO_CONTENT)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_204),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public void deleteWorkflowDefinition(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                         @PathVariable(name = Tenant.API_PATH) String tenantId,
                                         @TenantResolved Tenant tenant,
                                         @Parameter(description = WorkflowDefinition.API_KEY_DOC_VALUE)
                                         @PathVariable(name = WorkflowDefinition.API_KEY_PATH) String workflowDefinitionKey) {

        log.info("deleteWorkflowDefinition tenant:{} key:{}", tenant.getId(), workflowDefinitionKey);

        WorkflowDefinition workflowDefinition = workflowService
                .getWorkflowDefinitionByKey(tenant.getId(), workflowDefinitionKey, null)
                .orElseThrow(() -> new LocalizedStatusException(NOT_FOUND, "message.unknown_workflow_definition_key"));

        List<Subtype> subtypeUsingDef = subtypeRepository.getSubtypesReferencingWorkflowKey(tenant, workflowDefinitionKey);
        if (CollectionUtils.isNotEmpty(subtypeUsingDef)) {
            throw new LocalizedStatusException(CONFLICT, "message.cannot_delete_a_workflow_definition_referenced_by_subtypes");
        }

        workflowService.deleteWorkflowDefinition(workflowDefinition.getDeploymentId());

        // Deleting previous versions too

        // TODO : find a one-request method for all of those
        // The Flowable versioning starts at 1, we follow the same pattern here.
        IntStream.range(1, workflowDefinition.getVersion())
                .mapToObj(i -> workflowService.getWorkflowDefinitionByKey(tenant.getId(), workflowDefinitionKey, null))
                .filter(Optional::isPresent)
                .map(Optional::get)
                .map(WorkflowDefinition::getDeploymentId)
                .forEach(workflowService::deleteWorkflowDefinition);

        statsService.registerAdminAction(tenant, WORKFLOW, DELETE, workflowDefinition.getKey());
    }


    @GetMapping(API_V1 + "/admin/tenant/{tenantId}/workflowDefinition")
    @Operation(summary = "List workflow definitions", description = WorkflowDefinitionSortBy.Constants.API_DOC_SORT_BY_VALUES)
    @PreAuthorize("hasAnyRole('admin', 'tenant_' + #tenantId + '_admin')")
    @ResponseStatus(OK)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_400, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public Page<WorkflowDefinitionRepresentation> listWorkflowDefinitions(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                                                          @PathVariable(name = Tenant.API_PATH) String tenantId,
                                                                          @TenantResolved Tenant tenant,
                                                                          @PageableDefault(sort = WorkflowDefinitionSortBy.Constants.NAME_VALUE)
                                                                          @ParameterObject Pageable pageable,
                                                                          @Parameter(description = "Searching for a specific workflow name")
                                                                          @RequestParam(required = false) String searchTerm) {

        log.info("getWorkflowDefinitions page:{} pageSize:{} searchTerm:{}", pageable.getPageNumber(), pageable.getPageSize(), searchTerm);
        Pageable innerPageable = RequestUtils.convertSortedPageable(pageable, WorkflowDefinitionSortBy.class, WorkflowDefinitionSortBy::name);
        return workflowService.listWorkflowDefinitions(tenant.getId(), innerPageable, searchTerm);
    }


    // </editor-fold desc="WorkflowDefinition CRUDL">


    @GetMapping(API_V1 + "/admin/tenant/{tenantId}/workflowDefinitionByKey/{key}/referencingSubtypes")
    @Operation(summary = "Get an existing workflow definition", hidden = HIDE_UNSTABLE_API)
    @PreAuthorize("hasAnyRole('admin', 'tenant_' + #tenantId + '_admin')")
    @ResponseStatus(OK)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "403", description = "Authenticated user doesn't have admin role.\n(You might want to set it in Keycloak)"),
            @ApiResponse(responseCode = "404", description = "The workflow definition doesn't exist")
    })
    public List<Subtype> getSubtypesReferencingWorkflowDefinitionKey(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                                                     @PathVariable(name = Tenant.API_PATH) String tenantId,
                                                                     @TenantResolved Tenant tenant,
                                                                     @PathVariable String key) {

        // Throws 404 if not found
        workflowBusinessService.getWorkflowDefinitionByKey(tenant, key, null);
        return subtypeRepository.getSubtypesReferencingWorkflowKey(tenant, key);
    }


}
