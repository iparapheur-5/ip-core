/*
 * iparapheur Core
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.ipcore.controller.admin;

import coop.libriciel.ipcore.business.workflow.FolderBusinessService;
import coop.libriciel.ipcore.model.auth.User;
import coop.libriciel.ipcore.model.database.Tenant;
import coop.libriciel.ipcore.model.workflow.Folder;
import coop.libriciel.ipcore.model.workflow.FolderRepresentation;
import coop.libriciel.ipcore.model.workflow.FolderSortBy;
import coop.libriciel.ipcore.services.auth.AuthServiceInterface;
import coop.libriciel.ipcore.services.content.ContentServiceInterface;
import coop.libriciel.ipcore.services.database.TypologyService;
import coop.libriciel.ipcore.services.resolvers.CurrentUserResolver.CurrentUserResolved;
import coop.libriciel.ipcore.services.resolvers.FolderResolver.FolderResolved;
import coop.libriciel.ipcore.services.resolvers.TenantResolver.TenantResolved;
import coop.libriciel.ipcore.services.stats.StatsServiceInterface;
import coop.libriciel.ipcore.services.workflow.WorkflowServiceInterface;
import coop.libriciel.ipcore.utils.PaginatedList;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.log4j.Log4j2;
import org.jetbrains.annotations.NotNull;
import org.modelmapper.ModelMapper;
import org.springdoc.api.annotations.ParameterObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.Optional;

import static coop.libriciel.ipcore.IpCoreApplication.API_V1;
import static coop.libriciel.ipcore.model.workflow.Action.DELETE;
import static coop.libriciel.ipcore.utils.ApiUtils.*;
import static java.util.Collections.singletonList;
import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static org.springframework.http.HttpStatus.NO_CONTENT;
import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.MediaType.APPLICATION_OCTET_STREAM_VALUE;


@Log4j2
@RestController
@PreAuthorize("hasAnyRole('admin', 'tenant_' + #tenantId + '_admin')")
@RequestMapping(API_V1 + "/tenant/{tenantId}/archive")
@Tag(name = "admin-trash-bin", description = "Reserved operations on trash-bin")
public class AdminTrashBinController {


    // <editor-fold desc="Beans">


    private final AuthServiceInterface authService;
    private final ContentServiceInterface contentService;
    private final FolderBusinessService folderBusinessService;
    private final ModelMapper modelMapper;
    private final StatsServiceInterface statsService;
    private final TypologyService typologyService;
    private final WorkflowServiceInterface workflowService;


    @Autowired
    public AdminTrashBinController(AuthServiceInterface authService,
                                   ContentServiceInterface contentService,
                                   FolderBusinessService folderBusinessService,
                                   ModelMapper modelMapper,
                                   StatsServiceInterface statsService,
                                   TypologyService typologyService,
                                   WorkflowServiceInterface workflowService) {
        this.authService = authService;
        this.contentService = contentService;
        this.folderBusinessService = folderBusinessService;
        this.modelMapper = modelMapper;
        this.statsService = statsService;
        this.typologyService = typologyService;
        this.workflowService = workflowService;
    }


    // </editor-fold desc="Beans">


    @GetMapping("{folderId}/zip")
    @Operation(summary = "Get every file as ZIP, with a PREMIS-formatted summary")
    @ResponseStatus(OK)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200, content = @Content(mediaType = APPLICATION_OCTET_STREAM_VALUE)),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public void downloadTrashBinFolderZip(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                          @PathVariable(name = Tenant.API_PATH) String tenantId,
                                          @TenantResolved Tenant tenant,
                                          @Parameter(description = Folder.API_DOC_ID_VALUE)
                                          @PathVariable(name = Folder.API_PATH) String folderId,
                                          @FolderResolved Folder folder,
                                          @CurrentUserResolved User currentUser,
                                          @NotNull HttpServletResponse response) {

        log.debug("downloadDeletedFolderZip tenantId:{} folderId:{}", tenant.getId(), folder.getId());

        // Update inner values

        folder.setDocumentList(contentService.getDocumentList(folder.getContentId()));
        typologyService.updateTypology(singletonList(folder));

        // Trigger the client download

        folderBusinessService.downloadFolderAsZip(response, tenant, folder, contentService.retrievePipedDocument(folder.getPremisNodeId()));
    }


    @DeleteMapping("{folderId}")
    @Operation(summary = "Permanently delete a folder")
    @ResponseStatus(NO_CONTENT)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_204),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public void deleteTrashBinFolder(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                     @PathVariable(name = Tenant.API_PATH) String tenantId,
                                     @TenantResolved Tenant tenant,
                                     @Parameter(description = Folder.API_DOC_ID_VALUE)
                                     @PathVariable(name = Folder.API_PATH) String folderId,
                                     @FolderResolved Folder folder) {

        log.info("deleteTrashBinFolder folderId:{}", folderId);

        // Actual delete

        contentService.deleteFolder(folder);
        workflowService.deleteArchive(folder.getId());

        // Building stats result

        Long timeToCompleteInHours = Optional.ofNullable(folder.getSentToArchivesDate())
                .map(endTime -> MILLISECONDS.toHours(new Date().getTime() - endTime.getTime()))
                .orElse(null);

        statsService.registerFolderAction(tenant, DELETE, folder, folder.getFinalDesk(), timeToCompleteInHours);
    }


    @GetMapping
    @Operation(summary = "List deleted folders", description = FolderSortBy.Constants.API_DOC_SORT_BY_VALUES)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public Page<FolderRepresentation> listTrashBinFolders(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                                          @PathVariable(name = Tenant.API_PATH) String tenantId,
                                                          @TenantResolved Tenant tenant,
                                                          @PageableDefault(sort = FolderSortBy.Constants.FOLDER_NAME_VALUE)
                                                          @ParameterObject Pageable pageable) {

        log.debug("listTrashBinFolders tenant:{} page:{} pageSize:{}", tenant.getId(), pageable.getPageNumber(), pageable.getPageSize());
        PaginatedList<? extends Folder> result = workflowService.getArchives(tenant.getId(), pageable, null);

        // Compute missing values

        typologyService.updateTypology(result.getData());
        authService.updateFolderReferencedDeskNames(result.getData());

        // Send back results

        log.debug("listTrashBinFolders result:{}", result.getData().size());
        return new PageImpl<>(result.getData(), pageable, result.getTotal())
                .map(folder -> modelMapper.map(folder, FolderRepresentation.class));
    }


}
