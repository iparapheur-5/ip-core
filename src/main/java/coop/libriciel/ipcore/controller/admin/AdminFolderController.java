/*
 * iparapheur Core
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.ipcore.controller.admin;

import coop.libriciel.ipcore.model.auth.DeskRepresentation;
import coop.libriciel.ipcore.model.database.Subtype;
import coop.libriciel.ipcore.model.database.Tenant;
import coop.libriciel.ipcore.model.database.Type;
import coop.libriciel.ipcore.model.database.userPreferences.FolderFilter;
import coop.libriciel.ipcore.model.workflow.Folder;
import coop.libriciel.ipcore.model.workflow.FolderSortBy;
import coop.libriciel.ipcore.model.workflow.State;
import coop.libriciel.ipcore.model.workflow.Task;
import coop.libriciel.ipcore.services.content.ContentServiceInterface;
import coop.libriciel.ipcore.services.database.TypologyService;
import coop.libriciel.ipcore.services.permission.PermissionServiceInterface;
import coop.libriciel.ipcore.services.resolvers.FolderResolver.FolderResolved;
import coop.libriciel.ipcore.services.resolvers.TenantResolver.TenantResolved;
import coop.libriciel.ipcore.services.stats.StatsServiceInterface;
import coop.libriciel.ipcore.services.workflow.WorkflowServiceInterface;
import coop.libriciel.ipcore.utils.KeycloakSecurityUtils;
import coop.libriciel.ipcore.utils.LocalizedStatusException;
import coop.libriciel.ipcore.utils.PaginatedList;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.*;

import static coop.libriciel.ipcore.IpCoreApplication.API_V1;
import static coop.libriciel.ipcore.IpCoreApplication.HIDE_UNSTABLE_API;
import static coop.libriciel.ipcore.model.database.userPreferences.FolderFilter.*;
import static coop.libriciel.ipcore.model.workflow.Action.ARCHIVE;
import static coop.libriciel.ipcore.model.workflow.Action.DELETE;
import static coop.libriciel.ipcore.model.workflow.FolderSortBy.Constants.FOLDER_NAME_VALUE;
import static coop.libriciel.ipcore.model.workflow.State.PENDING;
import static coop.libriciel.ipcore.services.resolvers.FolderResolver.FolderResolved.Permission.IGNORE;
import static coop.libriciel.ipcore.utils.ApiUtils.*;
import static coop.libriciel.ipcore.utils.PaginatedList.*;
import static java.util.stream.Collectors.toSet;
import static org.springframework.http.HttpStatus.FORBIDDEN;
import static org.springframework.http.HttpStatus.NO_CONTENT;


@Log4j2
@RestController
@RequestMapping(API_V1 + "/admin/tenant/{tenantId}/folder")
@Tag(name = "admin-folder", description = "Reserved operations on folder")
public class AdminFolderController {


    // <editor-fold desc="Beans">


    private final PermissionServiceInterface permissionService;
    private final WorkflowServiceInterface workflowService;
    private final ContentServiceInterface contentService;
    private final TypologyService typologyService;
    private final StatsServiceInterface statsService;


    @Autowired
    public AdminFolderController(PermissionServiceInterface permissionService,
                                 WorkflowServiceInterface workflowService,
                                 ContentServiceInterface contentService,
                                 StatsServiceInterface statsService,
                                 TypologyService typologyService) {
        this.permissionService = permissionService;
        this.workflowService = workflowService;
        this.contentService = contentService;
        this.statsService = statsService;
        this.typologyService = typologyService;
    }


    // </editor-fold desc="Beans">


    @GetMapping
    @Operation(summary = "List folders", hidden = HIDE_UNSTABLE_API)
    @PreAuthorize("hasAnyRole('admin', 'tenant_' + #tenantId + '_admin', 'tenant_' + #tenantId + '_functional_admin')")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "401", description = "The user is not logged, or its session has expired"),
            @ApiResponse(responseCode = "403", description = "Authenticated user doesn't have admin role.\n(You might want to set it in Keycloak)"),
            @ApiResponse(responseCode = "404", description = "The given tenantId or folderId does not exist")
    })
    public PaginatedList<? extends Folder> listFoldersAsAdmin(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                                              @PathVariable(name = Tenant.API_PATH) String tenantId,
                                                              @TenantResolved Tenant tenant,
                                                              @Parameter(description = API_DOC_PAGE)
                                                              @RequestParam(defaultValue = API_DOC_PAGE_DEFAULT) int page,
                                                              @Parameter(description = API_DOC_PAGE_SIZE)
                                                              @RequestParam(defaultValue = API_DOC_PAGE_SIZE_DEFAULT) int pageSize,
                                                              @Parameter(description = API_DOC_SORT_BY)
                                                              @RequestParam(required = false, defaultValue = FOLDER_NAME_VALUE) FolderSortBy sortBy,
                                                              // FILTER_PARAMS START
                                                              @Parameter(description = API_DOC_TYPE_NAME)
                                                              @RequestParam(required = false) String typeId,
                                                              @Parameter(description = API_DOC_SUBTYPE_NAME)
                                                              @RequestParam(required = false) String subtypeId,
                                                              @Parameter(description = API_DOC_FROM)
                                                              @RequestParam(required = false) Long from,
                                                              @Parameter(description = API_DOC_TO)
                                                              @RequestParam(required = false) Long to,
                                                              @Parameter(description = API_DOC_SEARCH_DATA)
                                                              @RequestParam(required = false) String searchData,
                                                              @Parameter(description = API_DOC_LEGACY_ID)
                                                              @RequestParam(required = false) String legacyId,
                                                              // FILTER_PARAMS END
                                                              @Parameter(description = API_DOC_ASC)
                                                              @RequestParam(required = false, defaultValue = API_DOC_ASC_DEFAULT) boolean asc,
                                                              @Parameter(description = DeskRepresentation.API_DOC_ID_VALUE)
                                                              @RequestParam(required = false) String deskId,
                                                              @Parameter(description = "Searching for a specific folder name")
                                                              @RequestParam(required = false) String searchTerm,
                                                              @Parameter(description = "Filtering folders started before the given threshold")
                                                              @RequestParam(required = false) Long emitBeforeTime,
                                                              @Parameter(description = "Filtering folders that are still since given threshold")
                                                              @RequestParam(required = false) Long stillSinceTime,
                                                              @Parameter(description = "Filtering folders with given state")
                                                              @RequestParam(required = false) State state) {

        log.info("listFolders tenant:{} page:{} pageSize:{} state:{}", tenant.getName(), page, pageSize, state);

        PaginatedList<? extends Folder> result;
        FolderFilter filter = new FolderFilter(
                Optional.ofNullable(typeId).map(Type::new).orElse(null),
                Optional.ofNullable(subtypeId).map(Subtype::new).orElse(null),
                searchData,
                legacyId
        );

        if (KeycloakSecurityUtils.isSuperAdmin() || KeycloakSecurityUtils.currentUserIsTenantAdmin(tenantId)) {
            result = workflowService.listFolders(
                    tenant.getId(),
                    page,
                    pageSize,
                    sortBy,
                    filter,
                    asc,
                    deskId,
                    searchTerm,
                    emitBeforeTime,
                    stillSinceTime,
                    state
            );
        } else {
            // We rely on pre-authorize to assert that the user is a functional admin
            String userId = KeycloakSecurityUtils.getCurrentSessionUserId();
            List<String> administeredDeskIds =
                    permissionService.getAdministeredDesks(userId, tenantId)
                            .stream()
                            .map(DeskRepresentation::getId)
                            .toList();
            List<String> finalTargetDeskIds = administeredDeskIds;

            if (!StringUtils.isEmpty(deskId)) {
                if (administeredDeskIds.stream().noneMatch(dId -> StringUtils.equals(dId, deskId))) {
                    throw new LocalizedStatusException(FORBIDDEN, "message.you_can_t_view_this_desk");
                }

                finalTargetDeskIds = Collections.singletonList(deskId);
            }

            result = workflowService.listFoldersForDesks(
                    tenantId,
                    page,
                    pageSize,
                    sortBy,
                    filter,
                    asc,
                    finalTargetDeskIds,
                    searchTerm,
                    emitBeforeTime,
                    stillSinceTime,
                    state);
        }

        typologyService.updateTypology(result.getData());

        log.info("listFolders result:{}", result.getData().size());
        return result;
    }


    @DeleteMapping("{folderId}")
    @Operation(summary = "Delete folder")
    @PreAuthorize("hasAnyRole('admin', 'tenant_' + #tenantId + '_admin', 'tenant_' + #tenantId + '_functional_admin')")
    @ResponseStatus(NO_CONTENT)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_204),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public void deleteFolderAsAdmin(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                    @PathVariable(name = Tenant.API_PATH) String tenantId,
                                    @TenantResolved Tenant tenant,
                                    @Parameter(description = Folder.API_DOC_ID_VALUE)
                                    @PathVariable(name = Folder.API_PATH) String folderId,
                                    @FolderResolved(permission = IGNORE, withHistory = false) Folder folder) {

        log.info("deleteFolderAsAdmin id:{}", folderId);

        boolean isCurrentUserAdmin = KeycloakSecurityUtils.isSuperAdmin() || KeycloakSecurityUtils.currentUserIsTenantAdmin(tenantId);

        // Checking if the current user is a functional admin of any involved desk
        if (!isCurrentUserAdmin) {

            // We rely on pre-authorize to assert that the user is a functional admin
            String userId = KeycloakSecurityUtils.getCurrentSessionUserId();
            Set<String> administeredDeskIds = permissionService
                    .getAdministeredDesks(userId, tenantId)
                    .stream()
                    .map(DeskRepresentation::getId)
                    .collect(toSet());

            Set<String> currentlyInvolvedDeskIds = folder.getStepList().stream()
                    .filter(task -> task.getState() == PENDING)
                    .map(Task::getDesks)
                    .flatMap(Collection::stream)
                    .map(DeskRepresentation::getId)
                    .collect(toSet());

            // If nothing is remaining in the intersection of the two,
            // the current user is not a functional admin of any involved desk
            currentlyInvolvedDeskIds.retainAll(administeredDeskIds);
            if (currentlyInvolvedDeskIds.size() == 0) {
                throw new LocalizedStatusException(FORBIDDEN, "message.you_can_t_administrate_this_desk");
            }
        }

        contentService.deleteFolder(folder);
        workflowService.deleteWorkflow(folder.getId());

        // Building stats result

        Long timeToCompleteInHours = folder.getStepList()
                .stream()
                .filter(t -> (t.getAction() == ARCHIVE) || (t.getAction() == DELETE))
                .findFirst()
                .map(statsService::computeTimeToCompleteInHours)
                .orElse(null);

        statsService.registerFolderAction(tenant, DELETE, folder, folder.getFinalDesk(), timeToCompleteInHours);
    }


}
