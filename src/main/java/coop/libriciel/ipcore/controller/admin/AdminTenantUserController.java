/*
 * iparapheur Core
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.ipcore.controller.admin;

import coop.libriciel.ipcore.business.auth.UserBusinessService;
import coop.libriciel.ipcore.model.auth.DeskRepresentation;
import coop.libriciel.ipcore.model.auth.User;
import coop.libriciel.ipcore.model.auth.UserPrivilege;
import coop.libriciel.ipcore.model.auth.requests.UserDto;
import coop.libriciel.ipcore.model.auth.requests.UserRepresentation;
import coop.libriciel.ipcore.model.auth.requests.UserSortBy;
import coop.libriciel.ipcore.model.content.DocumentBuffer;
import coop.libriciel.ipcore.model.database.Tenant;
import coop.libriciel.ipcore.model.database.TenantRepresentation;
import coop.libriciel.ipcore.model.database.requests.StringResult;
import coop.libriciel.ipcore.services.auth.AuthServiceInterface;
import coop.libriciel.ipcore.services.content.ContentServiceInterface;
import coop.libriciel.ipcore.services.database.DatabaseServiceInterface;
import coop.libriciel.ipcore.services.database.UserPreferencesRepository;
import coop.libriciel.ipcore.services.permission.PermissionServiceInterface;
import coop.libriciel.ipcore.services.resolvers.CurrentUserResolver.CurrentUserResolved;
import coop.libriciel.ipcore.services.resolvers.TenantResolver.TenantResolved;
import coop.libriciel.ipcore.services.resolvers.UserResolver.UserResolved;
import coop.libriciel.ipcore.services.stats.StatsServiceInterface;
import coop.libriciel.ipcore.utils.ApiUtils;
import coop.libriciel.ipcore.utils.LocalizedStatusException;
import coop.libriciel.ipcore.utils.RequestUtils;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.modelmapper.ModelMapper;
import org.springdoc.api.annotations.ParameterObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.buffer.DefaultDataBufferFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static coop.libriciel.ipcore.IpCoreApplication.*;
import static coop.libriciel.ipcore.model.auth.UserPrivilege.*;
import static coop.libriciel.ipcore.model.auth.requests.UserSortBy.Constants.LAST_NAME_VALUE;
import static coop.libriciel.ipcore.model.permission.DelegationSortBy.Constants.SUBSTITUTE_DESK_VALUE;
import static coop.libriciel.ipcore.model.stats.StatsCategory.USER;
import static coop.libriciel.ipcore.model.workflow.Action.*;
import static coop.libriciel.ipcore.services.content.ContentServiceInterface.FILE_TRANSFER_BUFFER_SIZE;
import static coop.libriciel.ipcore.services.resolvers.UserResolver.UserResolved.Scope.TENANT;
import static coop.libriciel.ipcore.utils.ApiUtils.*;
import static java.util.Collections.singletonList;
import static java.util.Comparator.*;
import static java.util.stream.Collectors.toSet;
import static org.apache.commons.lang3.StringUtils.EMPTY;
import static org.apache.commons.lang3.StringUtils.isNotEmpty;
import static org.apache.tomcat.util.http.fileupload.FileUploadBase.MULTIPART_FORM_DATA;
import static org.springframework.core.io.buffer.DataBufferUtils.readInputStream;
import static org.springframework.data.domain.Pageable.unpaged;
import static org.springframework.data.domain.Sort.Direction.ASC;
import static org.springframework.http.HttpStatus.*;


@Log4j2
@RestController
@Tag(name = "admin-tenant-user", description = "Reserved operations on tenant users")
public class AdminTenantUserController {


    public static final int MAX_USERS_COUNT = 500000;


    private final AuthServiceInterface authService;
    private final ContentServiceInterface contentService;
    private final DatabaseServiceInterface dbService;
    private final ModelMapper modelMapper;
    private final PermissionServiceInterface permissionService;
    private final StatsServiceInterface statsService;
    private final UserBusinessService userBusinessService;
    private final UserPreferencesRepository userPreferencesRepository;


    // <editor-fold desc="Beans">


    @Autowired
    public AdminTenantUserController(AuthServiceInterface authService,
                                     ContentServiceInterface contentService,
                                     DatabaseServiceInterface dbService,
                                     ModelMapper modelMapper,
                                     PermissionServiceInterface permissionService,
                                     StatsServiceInterface statsService,
                                     UserBusinessService userBusinessService,
                                     UserPreferencesRepository userPreferencesRepository) {
        this.authService = authService;
        this.contentService = contentService;
        this.dbService = dbService;
        this.modelMapper = modelMapper;
        this.permissionService = permissionService;
        this.statsService = statsService;
        this.userBusinessService = userBusinessService;
        this.userPreferencesRepository = userPreferencesRepository;
    }


    // </editor-fold desc="Beans">


    // <editor-fold desc="User CRUDL">


    @PostMapping(API_PROVISIONING_V1 + "/admin/tenant/{" + Tenant.API_PATH + "}/user")
    @Operation(summary = "Create a new user")
    @PreAuthorize("hasAnyRole('admin', 'tenant_' + #tenantId + '_admin')")
    @ResponseStatus(CREATED)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_201),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_409, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_507, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public UserDto createUser(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                              @PathVariable(name = Tenant.API_PATH) String tenantId,
                              @TenantResolved Tenant tenant,
                              @Parameter(name = "body", description = "Every field is mandatory")
                              @RequestBody @Valid UserDto request,
                              @CurrentUserResolved User currentUser) {

        log.info("createUser tenantId:{}", tenant.getId());
        log.debug("createUser request:{}", request);

        Page<User> userList = authService.listTenantUsers(tenant.getId(), PageRequest.of(0, 1, ASC, LAST_NAME_VALUE), null);
        if (userList.getTotalElements() >= MAX_USERS_COUNT) {
            throw new LocalizedStatusException(INSUFFICIENT_STORAGE, "message.already_n_users_maximum_reached", userList.getTotalElements());
        }

        // Integrity check

        User requestDummyUser = modelMapper.map(request, User.class);
        authService.checkUserCrudPrivileges(currentUser, requestDummyUser, request);

        // Computing permissions

        if (request.getPrivilege() == null) {
            request.setPrivilege(NONE);
        }

        if (request.getPrivilege() == FUNCTIONAL_ADMIN && CollectionUtils.isEmpty(request.getAdministeredDeskIds())) {
            request.setPrivilege(NONE);
        }

        if (request.getPrivilege() == TENANT_ADMIN) {
            request.setAdministeredTenantIds(singletonList(tenant.getId()));
        }

        // Actual save

        String createdUserId = authService.createUser(tenant, request);
        request.setId(createdUserId);

        if (request.getPrivilege() == FUNCTIONAL_ADMIN) {
            request.getAdministeredDeskIds().forEach(deskId -> permissionService.setFunctionalAdmin(tenantId, deskId, createdUserId));
        }

        statsService.registerAdminAction(tenant, USER, CREATE, createdUserId);
        return request;
    }


    @GetMapping(API_PROVISIONING_V1 + "/admin/tenant/{" + Tenant.API_PATH + "}/user/{userId}")
    @Operation(summary = "Get a full user description")
    @PreAuthorize("hasAnyRole('admin', 'tenant_' + #tenantId + '_admin')")
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public UserDto getUser(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                           @PathVariable(name = Tenant.API_PATH) String tenantId,
                           @Parameter(description = UserRepresentation.API_DOC_ID_VALUE)
                           @PathVariable(name = UserRepresentation.API_PATH) String userId,
                           @UserResolved(scope = TENANT) User user) {
        log.debug("getUser result:{}", user);
        UserDto userDto = modelMapper.map(user, UserDto.class);
        userBusinessService.updateTenantInnerValues(tenantId, userDto);
        return userDto;
    }


    @PutMapping(API_PROVISIONING_V1 + "/admin/tenant/{" + Tenant.API_PATH + "}/user/{userId}")
    @Operation(summary = "Edit a user")
    @PreAuthorize("hasAnyRole('admin', 'tenant_' + #tenantId + '_admin')")
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_406, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public UserDto updateUser(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                              @PathVariable(name = Tenant.API_PATH) String tenantId,
                              @TenantResolved Tenant tenant,
                              @Parameter(description = UserRepresentation.API_DOC_ID_VALUE)
                              @PathVariable(name = UserRepresentation.API_PATH) String existingUserId,
                              @UserResolved(scope = TENANT) User existingUser,
                              @RequestBody @Valid UserDto request,
                              @CurrentUserResolved User currentUser) {

        log.info("updateUser userId:{}", existingUser.getId());
        log.debug("updateUser request:{}", request);

        // Integrity check

        authService.checkUserCrudPrivileges(currentUser, existingUser, request);

        // Computing permission changes

        List<String> originalAdministeredTenantIds = dbService
                .listTenantsForUser(existingUser.getId(), unpaged(), null, false, true, false)
                .map(TenantRepresentation::getId)
                .getContent();

        List<String> newAdministeredTenantIds = new ArrayList<>(originalAdministeredTenantIds);
        if (request.getPrivilege() == TENANT_ADMIN) {
            newAdministeredTenantIds.add(tenant.getId());
        } else {
            // target privilege is not tenant admin, we remove current tenant if it was there
            newAdministeredTenantIds.removeIf(tId -> StringUtils.equals(tId, tenant.getId()));
        }
        request.setAdministeredTenantIds(newAdministeredTenantIds);

        log.debug("updateUser - after setup - request.getAdministeredTenantIds:{}", request.getAdministeredTenantIds());

        if (request.getPrivilege() == FUNCTIONAL_ADMIN && CollectionUtils.isEmpty(request.getAdministeredDeskIds())) {
            log.debug("Trying to setup a functional admin without any administered desk");
            throw new ResponseStatusException(NOT_ACCEPTABLE, "Impossible d'être administrateur fonctionnel sans bureau à administrer");
        }

        // Actual save

        UserPrivilege newPrivilege = request.getPrivilege();
        authService.updateUser(modelMapper, existingUser, request, originalAdministeredTenantIds, tenant.getId());
        userBusinessService.computeTenantLevelDifferencesAndUpdateUserRolesAndPermissions(tenantId, existingUser, request);

        // Result

        statsService.registerAdminAction(tenant, USER, UPDATE, existingUser.getId());
        return request;
    }


    @DeleteMapping(API_PROVISIONING_V1 + "/admin/tenant/{" + Tenant.API_PATH + "}/user/{userId}")
    @Operation(summary = "Remove the given user from the tenant, deleting it if it was the last tenant linked")
    @PreAuthorize("hasAnyRole('admin', 'tenant_' + #tenantId + '_admin')")
    @ResponseStatus(NO_CONTENT)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_204),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_406, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public void removeUser(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                           @PathVariable(name = Tenant.API_PATH) String tenantId,
                           @TenantResolved Tenant tenant,
                           @Parameter(description = UserRepresentation.API_DOC_ID_VALUE)
                           @PathVariable(name = UserRepresentation.API_PATH) String targetUserId,
                           @UserResolved(scope = TENANT) User targetUser,
                           @CurrentUserResolved User currentUser) {

        log.info("removeUserFromTenant tenantId:{} userId:{}", tenant.getId(), targetUser.getId());
        // Integrity check

        authService.checkUserCrudPrivileges(currentUser, targetUser, null);

        if (StringUtils.equals(targetUser.getId(), currentUser.getId())) {
            throw new LocalizedStatusException(NOT_ACCEPTABLE, "message.cannot_delete_yourself");
        }

        boolean isSuperAdminUser = targetUser.getPrivilege() == SUPER_ADMIN;
        boolean isLastTenantForUser = dbService
                .listTenantsForUser(targetUser.getId(), PageRequest.of(0, 1), null, false, false, false)
                .getTotalPages() <= 1;

        // Actual delete

        permissionService.removeAllAdministeredDesks(tenant.getId(), targetUser.getId());
        permissionService.removeAllSupervisedDesks(tenant.getId(), targetUser.getId());
        permissionService.removeAllDelegationManagedDesks(tenant.getId(), targetUser.getId());


        if (isLastTenantForUser && !isSuperAdminUser) {
            log.info("Delete user id:{} contentNodeId:{}", targetUser.getId(), targetUser.getContentNodeId());

            Optional.ofNullable(targetUser.getContentNodeId())
                    .filter(StringUtils::isNotEmpty)
                    .ifPresent(contentService::deleteUserData);

            // TODO : A single call to deleteByUserId, but we have this error if we try:
            //  No EntityManager with actual transaction available for current thread
            userPreferencesRepository.getByUserId(targetUser.getId())
                    .ifPresent(userPreferencesRepository::delete);

            authService.deleteUser(targetUser.getId());
        } else {
            log.info("Unlink user id:{}", targetUser.getId());
            authService.removeUserFromTenant(targetUser.getId(), tenant.getId());
        }

        statsService.registerAdminAction(tenant, USER, DELETE, targetUser.getId());
    }


    @GetMapping(API_PROVISIONING_V1 + "/admin/tenant/{" + Tenant.API_PATH + "}/user")
    @Operation(summary = "List all users associated with the tenant", description = UserSortBy.Constants.API_DOC_SORT_BY_VALUES)
    @PreAuthorize("hasAnyRole('admin', 'tenant_' + #tenantId + '_admin', 'tenant_' + #tenantId + '_functional_admin')")
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_400, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public Page<UserRepresentation> listTenantUsers(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                                    @PathVariable(name = Tenant.API_PATH) String tenantId,
                                                    @TenantResolved Tenant tenant,
                                                    @PageableDefault(sort = LAST_NAME_VALUE)
                                                    @ParameterObject Pageable pageable,
                                                    @Parameter(description = "Searching for a specific term within `username`, `firstName`, `lastName` or `email` fields.")
                                                    @RequestParam(required = false) String searchTerm,
                                                    @Parameter(description = "Retrieved users will have an `isChecked` boolean, if the user has given desk's permissions.")
                                                    @RequestParam(required = false) String deskId) {

        log.debug("listTenantUsersAsAdmin tenantId:{}", tenant.getId());
        Page<User> result = authService.listTenantUsers(tenant.getId(), pageable, searchTerm);

        // Desk status (if defined)

        if (isNotEmpty(deskId)) {

            Optional.ofNullable(authService.findDeskByIdNoException(tenant.getId(), deskId))
                    .orElseThrow(() -> new LocalizedStatusException(NOT_FOUND, "message.unknown_desk_id"));

            authService.refreshUsersDeskStatus(result.getContent(), deskId);

        } else {

            Set<String> userIds = result.getContent().stream().map(User::getId).collect(toSet());
            Set<String> admins = authService.filterUsersWithPrivilege(userIds, SUPER_ADMIN, tenant.getId());
            Set<String> tenantAdmins = authService.filterUsersWithPrivilege(userIds, TENANT_ADMIN, tenant.getId());
            Set<String> functionalAdmins = authService.filterUsersWithPrivilege(userIds, FUNCTIONAL_ADMIN, tenant.getId());

            result.getContent().forEach(u -> u.setPrivilege(NONE));

            result.getContent().stream()
                    .filter(u -> functionalAdmins.contains(u.getId()))
                    .forEach(u -> u.setPrivilege(FUNCTIONAL_ADMIN));

            result.getContent().stream()
                    .filter(u -> tenantAdmins.contains(u.getId()))
                    .forEach(u -> u.setPrivilege(TENANT_ADMIN));

            result.getContent().stream()
                    .filter(u -> admins.contains(u.getId()))
                    .forEach(u -> u.setPrivilege(SUPER_ADMIN));
        }

        // Sending back result

        log.debug("List users result:{}", result.getContent().size());
        return result.map(user -> modelMapper.map(user, UserRepresentation.class));
    }


    // </editor-fold desc="User CRUDL">


    @GetMapping(API_V1 + "/admin/tenant/{" + Tenant.API_PATH + "}/user/{userId}/desks")
    @Operation(summary = "Get user's desks")
    @PreAuthorize("hasAnyRole('admin', 'tenant_' + #tenantId + '_admin')")
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ApiUtils.ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public Page<DeskRepresentation> getUserDesks(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                                 @PathVariable(name = Tenant.API_PATH) String tenantId,
                                                 @Parameter(description = UserRepresentation.API_DOC_ID_VALUE)
                                                 @PathVariable(name = UserRepresentation.API_PATH) String userId,
                                                 @UserResolved(scope = TENANT) User user,
                                                 @PageableDefault(sort = SUBSTITUTE_DESK_VALUE)
                                                 @ParameterObject Pageable pageable,
                                                 @Parameter(description = "Searching for a specific desk name")
                                                 @RequestParam(required = false) String searchTerm) {

        return authService.getDesksFromUser(user.getId(), pageable, searchTerm);
    }


    @GetMapping(API_V1 + "/admin/tenant/{" + Tenant.API_PATH + "}/user/{userId}/administeredDesks")
    @Operation(summary = "Get desks administered by the given user")
    @PreAuthorize("hasAnyRole('admin', 'tenant_' + #tenantId + '_admin')")
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ApiUtils.ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public Page<DeskRepresentation> getAdministeredDesks(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                                         @PathVariable(name = Tenant.API_PATH) String tenantId,
                                                         @TenantResolved Tenant tenant,
                                                         @Parameter(description = UserRepresentation.API_DOC_ID_VALUE)
                                                         @PathVariable(name = UserRepresentation.API_PATH) String userId,
                                                         @UserResolved(scope = TENANT) User user,
                                                         @PageableDefault(sort = SUBSTITUTE_DESK_VALUE)
                                                         @ParameterObject Pageable pageable) {

        List<DeskRepresentation> administeredDesks = permissionService.getAdministeredDesks(user.getId(), tenant.getId());
        int total = administeredDesks.size();

        List<DeskRepresentation> paginatedAdministeredDesks = administeredDesks.stream()
                .sorted(comparing(DeskRepresentation::getName, nullsLast(naturalOrder())))
                .skip((long) pageable.getPageNumber() * pageable.getPageSize())
                .limit(pageable.getPageSize())
                .toList();

        authService.updateDeskNames(paginatedAdministeredDesks);
        log.debug("getAdministeredDesks {}", paginatedAdministeredDesks);

        return new PageImpl<>(paginatedAdministeredDesks, pageable, total);
    }


    @GetMapping(API_V1 + "/admin/tenant/{" + Tenant.API_PATH + "}/user/{userId}/supervisedDesks")
    @Operation(summary = "Get desks supervised by the given user")
    @PreAuthorize("hasAnyRole('admin', 'tenant_' + #tenantId + '_admin')")
    @ResponseStatus(OK)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ApiUtils.ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public Page<DeskRepresentation> getSupervisedDesks(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                                       @PathVariable(name = Tenant.API_PATH) String tenantId,
                                                       @Parameter(description = UserRepresentation.API_DOC_ID_VALUE)
                                                       @PathVariable(name = UserRepresentation.API_PATH) String userId,
                                                       @UserResolved(scope = TENANT) User user,
                                                       @PageableDefault(sort = SUBSTITUTE_DESK_VALUE)
                                                       @ParameterObject Pageable pageable) {

        // Fetching data

        List<DeskRepresentation> supervisedDesks = permissionService.getSupervisedDesks(user.getId(), tenantId);
        int total = supervisedDesks.size();

        List<DeskRepresentation> paginatedSupervisedDesks = supervisedDesks.stream()
                .sorted(comparing(DeskRepresentation::getName, nullsLast(naturalOrder())))
                .skip((long) pageable.getPageNumber() * pageable.getPageSize())
                .limit(pageable.getPageSize())
                .toList();

        authService.updateDeskNames(paginatedSupervisedDesks);
        log.debug("getSupervisedDesks {}", paginatedSupervisedDesks);

        return new PageImpl<>(paginatedSupervisedDesks, pageable, total);
    }


    @GetMapping(API_V1 + "/admin/tenant/{" + Tenant.API_PATH + "}/user/{userId}/delegationManagedDesks")
    @Operation(summary = "Get desks with delegation managed by the given user")
    @PreAuthorize("hasAnyRole('admin', 'tenant_' + #tenantId + '_admin')")
    @ResponseStatus(OK)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ApiUtils.ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public Page<DeskRepresentation> getDelegationManagedDesks(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                                              @PathVariable(name = Tenant.API_PATH) String tenantId,
                                                              @Parameter(description = UserRepresentation.API_DOC_ID_VALUE)
                                                              @PathVariable(name = UserRepresentation.API_PATH) String userId,
                                                              @UserResolved(scope = TENANT) User user,
                                                              @PageableDefault(sort = SUBSTITUTE_DESK_VALUE)
                                                              @ParameterObject Pageable pageable) {

        // Fetching data

        List<DeskRepresentation> delegationManagedDesks = permissionService.getDelegationManagedDesks(user.getId(), tenantId);
        int total = delegationManagedDesks.size();

        List<DeskRepresentation> paginatedDelegationManagedDesks = delegationManagedDesks.stream()
                .sorted(comparing(DeskRepresentation::getName, nullsLast(naturalOrder())))
                .skip((long) pageable.getPageNumber() * pageable.getPageSize())
                .limit(pageable.getPageSize())
                .toList();

        authService.updateDeskNames(paginatedDelegationManagedDesks);
        log.debug("getDelegationManagedDesks {}", paginatedDelegationManagedDesks);

        return new PageImpl<>(paginatedDelegationManagedDesks, pageable, total);
    }


    // <editor-fold desc="Signature image CRUD">


    @PostMapping(value = API_V1 + "/admin/tenant/{" + Tenant.API_PATH + "}/user/{userId}/signatureImage", consumes = MULTIPART_FORM_DATA)
    @Operation(summary = "Create user's signature image", hidden = HIDE_UNSTABLE_API)
    @PreAuthorize("hasAnyRole('admin', 'tenant_' + #tenantId + '_admin')")
    @ResponseStatus(CREATED)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Created"),
            @ApiResponse(responseCode = "401", description = "The user is not logged, or its session has expired"),
            @ApiResponse(responseCode = "403", description = "Authenticated user doesn't have admin's role.\n(You might want to set it in Keycloak)"),
            @ApiResponse(responseCode = "404", description = "The given userId or tenantId does not exist"),
            @ApiResponse(responseCode = "409", description = "This user already have a signature image set. You should use the PUT method")
    })
    public StringResult setSignatureImage(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                          @PathVariable(name = Tenant.API_PATH) String tenantId,
                                          @Parameter(hidden = true)
                                          @TenantResolved Tenant tenant,
                                          @Parameter(description = UserRepresentation.API_DOC_ID_VALUE)
                                          @PathVariable(name = UserRepresentation.API_PATH) String userId,
                                          @UserResolved(scope = TENANT) User targetUser,
                                          @CurrentUserResolved User currentUser,
                                          @Parameter(description = "File")
                                          @RequestPart MultipartFile file) {

        log.info("setSignatureImage tenantId:{} userId:{} imageSize:{} imageName:{}", tenant.getId(), targetUser.getId(), file.getSize(), file.getName());

        // Integrity check

        authService.checkUserCrudPrivileges(currentUser, targetUser, null);

        if (StringUtils.isNotEmpty(targetUser.getSignatureImageContentId())) {
            throw new LocalizedStatusException(CONFLICT, "message.user_already_have_a_signature_image_set");
        }

        // Init User content data if needed

        if (StringUtils.isEmpty(targetUser.getContentNodeId())) {

            int contentGroupIndex = Optional
                    .ofNullable(targetUser.getContentGroupIndex())
                    .orElseGet(dbService::getUserDataGroupIndexAvailable);

            String userDataContentNodeId = contentService.createUserData(targetUser.getId(), contentGroupIndex);
            authService.updateUserInternalMetadata(targetUser.getId(), contentGroupIndex, userDataContentNodeId, null);

            targetUser.setContentGroupIndex(contentGroupIndex);
            targetUser.setContentNodeId(userDataContentNodeId);
        }

        // Updating data

        String imageSignatureNodeId;
        try (InputStream body = file.getInputStream()) {
            DocumentBuffer imageBuffer = new DocumentBuffer();
            imageBuffer.setName(file.getName());
            imageBuffer.setContentFlux(readInputStream(() -> body, new DefaultDataBufferFactory(), FILE_TRANSFER_BUFFER_SIZE));
            imageSignatureNodeId = contentService.createUserSignatureImage(targetUser.getContentNodeId(), imageBuffer);
        } catch (IOException e) {
            throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, e, "message.error_reading_file");
        }

        authService.updateUserInternalMetadata(targetUser.getId(), null, null, imageSignatureNodeId);
        statsService.registerAdminAction(tenant, USER, UPDATE, targetUser.getId());
        return new StringResult(imageSignatureNodeId);
    }


    @GetMapping(API_V1 + "/admin/tenant/{" + Tenant.API_PATH + "}/user/{userId}/signatureImage")
    @Operation(summary = "Get user's signature image", hidden = HIDE_UNSTABLE_API)
    @PreAuthorize("hasAnyRole('admin', 'tenant_' + #tenantId + '_admin')")
    @ResponseStatus(OK)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "401", description = "The user is not logged, or its session has expired"),
            @ApiResponse(responseCode = "403", description = "Authenticated user doesn't have admin's role.\n(You might want to set it in Keycloak)"),
            @ApiResponse(responseCode = "404", description = "The given userId does not exist, or the user does not have any signature image set")
    })
    public void getSignatureImage(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                  @PathVariable(name = Tenant.API_PATH) String tenantId,
                                  @TenantResolved Tenant tenant,
                                  @Parameter(description = UserRepresentation.API_DOC_ID_VALUE)
                                  @PathVariable(name = UserRepresentation.API_PATH) String userId,
                                  @UserResolved(scope = TENANT) User targetUser,
                                  HttpServletResponse response) {

        log.info("getSignatureImage tenantId:{} userId:{}", tenant.getId(), targetUser.getId());

        // Integrity check

        if (StringUtils.isEmpty(targetUser.getSignatureImageContentId())) {
            throw new LocalizedStatusException(NOT_FOUND, "message.no_signature_for_the_given_user_id");
        }

        // Image fetch

        DocumentBuffer documentBuffer = contentService.getSignatureImage(targetUser.getSignatureImageContentId());
        log.debug("getSignatureImage retrieve from Alfresco buffer imageContentId:{} buffer:{}", targetUser.getSignatureImageContentId(), documentBuffer);

        // Build Response

        RequestUtils.writeBufferToResponse(documentBuffer, response);
    }


    @PutMapping(API_V1 + "/admin/tenant/{" + Tenant.API_PATH + "}/user/{userId}/signatureImage")
    @Operation(summary = "Replace user's signature image", hidden = HIDE_UNSTABLE_API)
    @PreAuthorize("hasAnyRole('admin', 'tenant_' + #tenantId + '_admin')")
    @ResponseStatus(OK)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Created"),
            @ApiResponse(responseCode = "401", description = "The user is not logged, or its session has expired"),
            @ApiResponse(responseCode = "403", description = "Authenticated user doesn't have admin's role.\n(You might want to set it in Keycloak)"),
            @ApiResponse(responseCode = "404", description = "The given userId or tenantId does not exist, or the user doesn't have any signature image to update.")
    })
    public void updateSignatureImage(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                     @PathVariable(name = Tenant.API_PATH) String tenantId,
                                     @TenantResolved Tenant tenant,
                                     @Parameter(description = UserRepresentation.API_DOC_ID_VALUE)
                                     @PathVariable(name = UserRepresentation.API_PATH) String userId,
                                     @UserResolved(scope = TENANT) User targetUser,
                                     @CurrentUserResolved User currentUser,
                                     @Parameter(description = "File")
                                     @RequestPart MultipartFile file) {

        log.info("updateSignatureImage tenantId:{} userId:{} imageSize:{} imageName:{}", tenant.getId(), targetUser.getId(), file.getSize(), file.getName());

        // Integrity check

        authService.checkUserCrudPrivileges(currentUser, targetUser, null);

        String imageNodeId = Optional.ofNullable(targetUser.getSignatureImageContentId())
                .filter(StringUtils::isNotEmpty)
                .orElseThrow(() -> new LocalizedStatusException(NOT_FOUND, "message.no_signature_for_the_given_user_id"));

        // Updating data

        try (InputStream body = file.getInputStream()) {
            DocumentBuffer imageBuffer = new DocumentBuffer();
            imageBuffer.setName(file.getOriginalFilename());
            imageBuffer.setContentFlux(readInputStream(() -> body, new DefaultDataBufferFactory(), FILE_TRANSFER_BUFFER_SIZE));
            contentService.updateSignatureImage(imageNodeId, imageBuffer);
        } catch (IOException e) {
            throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, e, "message.error_reading_file");
        }

        statsService.registerAdminAction(tenant, USER, UPDATE, targetUser.getId());
    }


    @DeleteMapping(API_V1 + "/admin/tenant/{" + Tenant.API_PATH + "}/user/{userId}/signatureImage")
    @Operation(summary = "Delete user's signature image", hidden = HIDE_UNSTABLE_API)
    @PreAuthorize("hasAnyRole('admin', 'tenant_' + #tenantId + '_admin')")
    @ResponseStatus(NO_CONTENT)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "No content"),
            @ApiResponse(responseCode = "401", description = "The user is not logged, or its session has expired"),
            @ApiResponse(responseCode = "403", description = "Authenticated user doesn't have admin's role.\n(You might want to set it in Keycloak)"),
            @ApiResponse(responseCode = "404", description = "The given userId does not exist")
    })
    public void deleteSignatureImage(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                     @PathVariable(name = Tenant.API_PATH) String tenantId,
                                     @TenantResolved Tenant tenant,
                                     @Parameter(description = UserRepresentation.API_DOC_ID_VALUE)
                                     @PathVariable(name = UserRepresentation.API_PATH) String userId,
                                     @UserResolved(scope = TENANT) User targetUser,
                                     @CurrentUserResolved User currentUser) {

        log.info("deleteSignatureImage tenantId:{} userId:{}", tenant.getId(), targetUser.getId());

        // Integrity check

        authService.checkUserCrudPrivileges(currentUser, targetUser, null);

        String imageNodeId = Optional.ofNullable(targetUser.getSignatureImageContentId())
                .filter(StringUtils::isNotEmpty)
                .orElseThrow(() -> new LocalizedStatusException(NOT_FOUND, "message.no_signature_for_the_given_user_id"));

        // Actual delete

        contentService.deleteSignatureImage(imageNodeId);
        authService.updateUserInternalMetadata(targetUser.getId(), null, null, EMPTY);
        statsService.registerAdminAction(tenant, USER, UPDATE, targetUser.getId());
    }


    // </editor-fold desc="Signature image CRUD">


}
