/*
 * iparapheur Core
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.ipcore.controller.admin;

import com.google.common.collect.ImmutableSet;
import coop.libriciel.ipcore.model.content.DocumentBuffer;
import coop.libriciel.ipcore.model.crypto.SealCertificate;
import coop.libriciel.ipcore.model.crypto.SealCertificateRepresentation;
import coop.libriciel.ipcore.model.database.IdCount;
import coop.libriciel.ipcore.model.database.SealCertificateSortBy;
import coop.libriciel.ipcore.model.database.Subtype;
import coop.libriciel.ipcore.model.database.Tenant;
import coop.libriciel.ipcore.model.database.requests.StringResult;
import coop.libriciel.ipcore.model.database.requests.SubtypeDto;
import coop.libriciel.ipcore.model.database.requests.SubtypeRepresentation;
import coop.libriciel.ipcore.services.content.ContentServiceInterface;
import coop.libriciel.ipcore.services.database.SubtypeRepository;
import coop.libriciel.ipcore.services.resolvers.SealCertificateResolver.SealCertificateResolved;
import coop.libriciel.ipcore.services.resolvers.TenantResolver.TenantResolved;
import coop.libriciel.ipcore.services.secret.SecretServiceInterface;
import coop.libriciel.ipcore.services.stats.StatsServiceInterface;
import coop.libriciel.ipcore.utils.LocalizedStatusException;
import coop.libriciel.ipcore.utils.PaginatedList;
import coop.libriciel.ipcore.utils.RequestUtils;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.bouncycastle.asn1.x500.RDN;
import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.asn1.x500.style.IETFUtils;
import org.bouncycastle.cert.jcajce.JcaX509CertificateHolder;
import org.modelmapper.ModelMapper;
import org.springdoc.api.annotations.ParameterObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.buffer.DefaultDataBufferFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.io.InputStream;
import java.security.*;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.*;

import static coop.libriciel.ipcore.IpCoreApplication.*;
import static coop.libriciel.ipcore.model.stats.StatsCategory.SEAL_CERTIFICATE;
import static coop.libriciel.ipcore.model.workflow.Action.*;
import static coop.libriciel.ipcore.services.content.ContentServiceInterface.FILE_TRANSFER_BUFFER_SIZE;
import static coop.libriciel.ipcore.services.secret.SecretServiceInterface.MAX_SEAL_CERTIFICATES_PER_TENANT;
import static coop.libriciel.ipcore.utils.ApiUtils.*;
import static coop.libriciel.ipcore.utils.PaginatedList.*;
import static java.util.stream.Collectors.toMap;
import static java.util.stream.Collectors.toSet;
import static org.apache.tomcat.util.http.fileupload.FileUploadBase.MULTIPART_FORM_DATA;
import static org.bouncycastle.asn1.x500.style.BCStyle.CN;
import static org.springframework.core.io.buffer.DataBufferUtils.readInputStream;
import static org.springframework.data.domain.Sort.Direction.ASC;
import static org.springframework.http.HttpStatus.*;


@Log4j2
@RestController
@RequestMapping
@Tag(name = "admin-seal-certificate", description = "Reserved operations on seal certificates")
public class AdminSealCertificateController {


    // <editor-fold desc="Beans">


    private final ContentServiceInterface contentService;
    private final SecretServiceInterface secretService;
    private final StatsServiceInterface statsService;
    private final SubtypeRepository subtypeRepository;
    private final ModelMapper modelMapper;


    @Autowired
    public AdminSealCertificateController(ContentServiceInterface contentService,
                                          SecretServiceInterface secretService,
                                          StatsServiceInterface statsService,
                                          SubtypeRepository subtypeRepository,
                                          ModelMapper modelMapper) {
        this.contentService = contentService;
        this.secretService = secretService;
        this.statsService = statsService;
        this.subtypeRepository = subtypeRepository;
        this.modelMapper = modelMapper;
    }


    // </editor-fold desc="Beans">


    // <editor-fold desc="Seal certificate CRUDL">


    @PostMapping(value = API_V1 + "/admin/tenant/{tenantId}/sealCertificate", consumes = MULTIPART_FORM_DATA)
    @Operation(summary = "Create a seal certificate")
    @PreAuthorize("hasAnyRole('admin', 'tenant_' + #tenantId + '_admin')")
    @ResponseStatus(CREATED)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_201),
            @ApiResponse(responseCode = CODE_400, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_507, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public SealCertificate createSealCertificate(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                                 @PathVariable(name = Tenant.API_PATH) String tenantId,
                                                 @Parameter(hidden = true)
                                                 @TenantResolved Tenant tenant,
                                                 @Parameter(description = "password", required = true)
                                                 @RequestPart String password,
                                                 @Parameter(description = "file", required = true)
                                                 @RequestPart MultipartFile file) {

        log.info("createSealCertificate");

        // Integrity check

        Page<SealCertificateRepresentation> existing = secretService.getSealCertificateList(tenant.getId(), PageRequest.of(0, 1));
        if (existing.getTotalElements() >= MAX_SEAL_CERTIFICATES_PER_TENANT) {
            throw new LocalizedStatusException(INSUFFICIENT_STORAGE, "message.already_n_seal_certificates_maximum_reached", MAX_SEAL_CERTIFICATES_PER_TENANT);
        }

        // Parsing p12 data

        SealCertificate sealCertificate = new SealCertificate();

        try (InputStream p12InputStream = file.getInputStream()) {

            KeyStore ks = KeyStore.getInstance("PKCS12");
            ks.load(p12InputStream, password.toCharArray());

            Enumeration<String> aliasEnum = ks.aliases();

            PrivateKey privateKey = null;
            X509Certificate certificate = null;

            while (aliasEnum.hasMoreElements()) {
                String keyName = aliasEnum.nextElement();
                privateKey = (PrivateKey) ks.getKey(keyName, password.toCharArray());
                certificate = (X509Certificate) ks.getCertificate(keyName);
            }

            if (privateKey == null) {
                throw new CertificateException("Could not read private key from given file");
            }
            if (certificate == null) {
                throw new CertificateException("Could not read certificate from given file");
            }

            JcaX509CertificateHolder certificateHolder = new JcaX509CertificateHolder(certificate);
            X500Name x500name = certificateHolder.getSubject();
            RDN cn = x500name.getRDNs(CN)[0];

            sealCertificate.setId(UUID.randomUUID().toString());
            sealCertificate.setName(IETFUtils.valueToString(cn.getFirst().getValue()));
            sealCertificate.setExpirationDate(certificateHolder.getNotAfter());
            sealCertificate.setPublicCertificateBase64(Base64.getEncoder().encodeToString(certificate.getEncoded()));
            sealCertificate.setPrivateKeyBase64(Base64.getEncoder().encodeToString(privateKey.getEncoded()));

        } catch (UnrecoverableKeyException e) {
            // PrivateKey password error
            throw new LocalizedStatusException(BAD_REQUEST, "message.certificate_bad_password");
        } catch (IOException | CertificateException | NoSuchAlgorithmException | KeyStoreException e) {
            // KeyStore password error. It throws an IOException.
            // that we cannot differentiate from an InputStream one. We have to parse the message...
            if (StringUtils.contains(e.getMessage(), "password was incorrect")) {
                throw new LocalizedStatusException(BAD_REQUEST, "message.certificate_bad_password");
            }
            throw new LocalizedStatusException(BAD_REQUEST, "message.could_not_read_certificate_content");
        }

        // Actual registering

        Optional.of(sealCertificate)
                .filter(c -> StringUtils.isNotEmpty(c.getId()))
                .filter(c -> StringUtils.isNotEmpty(c.getName()))
                .filter(c -> StringUtils.isNotEmpty(c.getPublicCertificateBase64()))
                .filter(c -> StringUtils.isNotEmpty(c.getPrivateKeyBase64()))
                .filter(c -> Objects.nonNull(c.getExpirationDate()))
                .orElseThrow(() -> new LocalizedStatusException(BAD_REQUEST, "message.could_not_read_certificate_content"));

        secretService.storeSealCertificate(tenant.getId(), sealCertificate);
        statsService.registerAdminAction(tenant, SEAL_CERTIFICATE, CREATE, sealCertificate.getId());

        return sealCertificate;
    }


    @GetMapping(API_V1 + "/admin/tenant/{tenantId}/sealCertificate/{sealCertificateId}")
    @Operation(summary = "Get a seal certificate with every information set")
    @PreAuthorize("hasAnyRole('admin', 'tenant_' + #tenantId + '_admin')")
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_400, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public SealCertificate getSealCertificate(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                              @PathVariable(name = Tenant.API_PATH) String tenantId,
                                              @TenantResolved Tenant tenant,
                                              @Parameter(description = SealCertificate.API_DOC_ID_VALUE)
                                              @PathVariable(name = SealCertificate.API_PATH) String sealCertificateId,
                                              @SealCertificateResolved SealCertificate sealCertificate) {

        log.debug("getSealCertificate tenantId:{} sealCertificateId:{}", tenant.getId(), sealCertificate.getId());

        // Computing transient values

        long usageCount = subtypeRepository.fetchCountBySealCertificateIdIn(ImmutableSet.of(sealCertificateId)).stream()
                .findAny().map(IdCount::getCount).orElse(0L);

        sealCertificate.setUsageCount(usageCount);

        // Actual registering

        return sealCertificate;
    }


    @PutMapping(API_V1 + "/admin/tenant/{tenantId}/sealCertificate/{sealCertificateId}")
    @Operation(summary = "Edit a seal certificate")
    @PreAuthorize("hasAnyRole('admin', 'tenant_' + #tenantId + '_admin')")
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public SealCertificate updateSealCertificate(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                                 @PathVariable(name = Tenant.API_PATH) String tenantId,
                                                 @TenantResolved Tenant tenant,
                                                 @Parameter(description = SealCertificate.API_DOC_ID_VALUE)
                                                 @PathVariable(name = SealCertificate.API_PATH) String sealCertificateId,
                                                 @SealCertificateResolved SealCertificate sealCertificate,
                                                 @RequestBody @Valid SealCertificate updatedSealCertificate) {

        log.debug("updateSealCertificate id:{} tenant:{}", sealCertificate.getId(), tenant.getId());

        // Update given model with hidden, redacted and immuable values

        updatedSealCertificate.setId(sealCertificate.getId());
        updatedSealCertificate.setPublicCertificateBase64(sealCertificate.getPublicCertificateBase64());
        updatedSealCertificate.setPrivateKeyBase64(sealCertificate.getPrivateKeyBase64());
        updatedSealCertificate.setExpirationDate(sealCertificate.getExpirationDate());
        updatedSealCertificate.setSignatureImageContentId(sealCertificate.getSignatureImageContentId());

        // Actual store

        secretService.storeSealCertificate(tenant.getId(), updatedSealCertificate);
        statsService.registerAdminAction(tenant, SEAL_CERTIFICATE, UPDATE, updatedSealCertificate.getId());
        return updatedSealCertificate;
    }


    @DeleteMapping(API_V1 + "/admin/tenant/{tenantId}/sealCertificate/{sealCertificateId}")
    @Operation(summary = "Delete a stored seal certificate")
    @PreAuthorize("hasAnyRole('admin', 'tenant_' + #tenantId + '_admin')")
    @ResponseStatus(NO_CONTENT)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_204),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_406, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public void deleteSealCertificate(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                      @PathVariable(name = Tenant.API_PATH) String tenantId,
                                      @TenantResolved Tenant tenant,
                                      @Parameter(description = SealCertificate.API_DOC_ID_VALUE)
                                      @PathVariable(name = SealCertificate.API_PATH) String sealCertificateId,
                                      @SealCertificateResolved SealCertificate sealCertificate) {

        log.info("deleteSealCertificate metadata tenantId:{} sealCertificateId:{}", tenant.getId(), sealCertificate.getId());

        // Integrity check

        long usageCount = subtypeRepository.fetchCountBySealCertificateIdIn(Set.of(sealCertificate.getId()))
                .stream()
                .findAny()
                .map(IdCount::getCount)
                .orElse(0L);

        if (usageCount > 0) {
            throw new LocalizedStatusException(NOT_ACCEPTABLE, "message.this_seal_certificate_is_still_linked_to_a_subtype");
        }

        // Actual delete

        secretService.deleteSealCertificate(tenant.getId(), sealCertificateId);
        statsService.registerAdminAction(tenant, SEAL_CERTIFICATE, DELETE, sealCertificateId);
    }


    @GetMapping(API_V1 + "/admin/tenant/{tenantId}/sealCertificate")
    @Operation(summary = "List seal certificates", description = SealCertificateSortBy.Constants.API_DOC_SORT_BY_VALUES)
    @PreAuthorize("hasAnyRole('admin', 'tenant_' + #tenantId + '_admin')")
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_400, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public Page<SealCertificateRepresentation> listSealCertificateAsAdmin(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                                                          @PathVariable(name = Tenant.API_PATH) String tenantId,
                                                                          @TenantResolved Tenant tenant,
                                                                          @PageableDefault(sort = SealCertificateSortBy.Constants.NAME_VALUE)
                                                                          @ParameterObject Pageable pageable) {

        log.info("listSealCertificate page:{} pageSize:{}", pageable.getPageNumber(), pageable.getPageSize());

        Pageable innerPageable = RequestUtils.convertSortedPageable(pageable, SealCertificateSortBy.class, SealCertificateSortBy::getFieldName);
        Page<SealCertificateRepresentation> result = secretService.getSealCertificateList(tenant.getId(), innerPageable);

        // Computing transient values

        Set<String> certIds = result.getContent().stream().map(SealCertificateRepresentation::getId).collect(toSet());
        Map<String, Long> idCounts = subtypeRepository.fetchCountBySealCertificateIdIn(certIds)
                .stream()
                .collect(toMap(IdCount::getId, IdCount::getCount));

        result.getContent().forEach(s -> s.setUsageCount(idCounts.getOrDefault(s.getId(), 0L)));

        // Sending back result

        return result;
    }


    // </editor-fold desc="Seal certificate CRUDL">


    @GetMapping(API_INTERNAL + "/admin/tenant/{tenantId}/sealCertificate/{sealCertificateId}/subtypeUsage")
    @Operation(summary = "List subtypes using the given certificate")
    @PreAuthorize("hasAnyRole('admin', 'tenant_' + #tenantId + '_admin')")
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public Page<SubtypeRepresentation> listSubtypeUsage(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                                        @PathVariable(name = Tenant.API_PATH) String tenantId,
                                                        @Parameter(description = SealCertificate.API_DOC_ID_VALUE)
                                                        @PathVariable(name = SealCertificate.API_PATH) String sealCertificateId,
                                                        @SealCertificateResolved SealCertificate sealCertificate,
                                                        @ParameterObject Pageable pageable) {

        log.debug("listSealCertificate page:{} pageSize:{}", pageable.getPageNumber(), pageable.getPageSize());

        return subtypeRepository.findByTenant_IdAndSealCertificateId(tenantId, sealCertificate.getId(), pageable)
                .map(subtype -> modelMapper.map(subtype, SubtypeRepresentation.class));

    }


    // <editor-fold desc="Signature image CRUD">


    @PostMapping(value = API_V1 + "/admin/tenant/{tenantId}/sealCertificate/{sealCertificateId}/signatureImage", consumes = MULTIPART_FORM_DATA)
    @Operation(summary = "Create seal's signature image", hidden = HIDE_UNSTABLE_API)
    @PreAuthorize("hasAnyRole('admin', 'tenant_' + #tenantId + '_admin')")
    @ResponseStatus(CREATED)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Created"),
            @ApiResponse(responseCode = "401", description = "The user is not logged, or its session has expired"),
            @ApiResponse(responseCode = "403", description = "Authenticated user doesn't have admin's role.\n(You might want to set it in Keycloak)"),
            @ApiResponse(responseCode = "404", description = "The given sealCertificateId or tenantId does not exist"),
            @ApiResponse(responseCode = "409", description = "This seal certificate already have a signature image set. You should use the PUT method")
    })
    public StringResult setSealSignatureImage(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                              @PathVariable(name = Tenant.API_PATH) String tenantId,
                                              @Parameter(hidden = true)
                                              @TenantResolved Tenant tenant,
                                              @Parameter(description = SealCertificate.API_DOC_ID_VALUE)
                                              @PathVariable(name = SealCertificate.API_PATH) String sealCertificateId,
                                              @Parameter(hidden = true)
                                              @SealCertificateResolved SealCertificate sealCertificate,
                                              @Parameter(description = "File")
                                              @RequestPart MultipartFile file) {

        log.info("setSealSignatureImage sealCertificateId:{}", sealCertificateId);

        // Integrity check

        if (StringUtils.isNotEmpty(sealCertificate.getSignatureImageContentId())) {
            throw new LocalizedStatusException(CONFLICT, "message.seal_certificate_already_have_a_signature_image_set");
        }

        // Updating data

        String imageSignatureNodeId;
        try (InputStream body = file.getInputStream()) {
            DocumentBuffer imageBuffer = new DocumentBuffer();
            imageBuffer.setName(file.getOriginalFilename());
            imageBuffer.setContentFlux(readInputStream(() -> body, new DefaultDataBufferFactory(), FILE_TRANSFER_BUFFER_SIZE));
            imageSignatureNodeId = contentService.createSealCertificateSignatureImage(tenant, sealCertificate.getId(), imageBuffer);
        } catch (IOException e) {
            throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, e, "message.error_reading_file");
        }

        sealCertificate.setSignatureImageContentId(imageSignatureNodeId);
        secretService.storeSealCertificate(tenant.getId(), sealCertificate);
        statsService.registerAdminAction(tenant, SEAL_CERTIFICATE, UPDATE, sealCertificate.getId());
        return new StringResult(imageSignatureNodeId);
    }


    @GetMapping(API_V1 + "/admin/tenant/{tenantId}/sealCertificate/{sealCertificateId}/signatureImage")
    @Operation(summary = "Get seal's signature image", hidden = HIDE_UNSTABLE_API)
    @PreAuthorize("hasAnyRole('admin', 'tenant_' + #tenantId + '_admin')")
    @ResponseStatus(OK)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "403", description = "Authenticated user doesn't have admin's role.\n(You might want to set it in Keycloak)"),
            @ApiResponse(responseCode = "404", description = "The current seal certificate does not exist, or the user does not have any signature image set")
    })
    public void getSealSignatureImage(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                      @PathVariable(name = Tenant.API_PATH) String tenantId,
                                      @Parameter(hidden = true)
                                      @TenantResolved Tenant tenant,
                                      @Parameter(description = SealCertificate.API_DOC_ID_VALUE)
                                      @PathVariable(name = SealCertificate.API_PATH) String sealCertificateId,
                                      @Parameter(hidden = true)
                                      @SealCertificateResolved SealCertificate sealCertificate,
                                      HttpServletResponse response) {

        // Integrity check

        if (StringUtils.isEmpty(sealCertificate.getSignatureImageContentId())) {
            throw new LocalizedStatusException(NOT_FOUND, "message.no_signature_for_the_given_seal_certificate_id");
        }

        // Image fetch

        DocumentBuffer documentBuffer = contentService.getSignatureImage(sealCertificate.getSignatureImageContentId());
        log.debug("getSealSignatureImage imageContentId:{} buffer:{}", sealCertificate.getSignatureImageContentId(), documentBuffer);
        RequestUtils.writeBufferToResponse(documentBuffer, response);
    }


    @PutMapping(value = API_V1 + "/admin/tenant/{tenantId}/sealCertificate/{sealCertificateId}/signatureImage", consumes = MULTIPART_FORM_DATA)
    @Operation(summary = "Replace seal's signature image", hidden = HIDE_UNSTABLE_API)
    @PreAuthorize("hasAnyRole('admin', 'tenant_' + #tenantId + '_admin')")
    @ResponseStatus(OK)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Created"),
            @ApiResponse(responseCode = "401", description = "The user is not logged, or its session has expired"),
            @ApiResponse(responseCode = "403", description = "Authenticated user doesn't have admin's role.\n(You might want to set it in Keycloak)"),
            @ApiResponse(responseCode = "404", description = "The given sealCertificateId does not exist, or the user doesn't have any signature image.")
    })
    public void updateSealSignatureImage(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                         @PathVariable(name = Tenant.API_PATH) String tenantId,
                                         @TenantResolved Tenant tenant,
                                         @Parameter(description = SealCertificate.API_DOC_ID_VALUE)
                                         @PathVariable(name = SealCertificate.API_PATH) String sealCertificateId,
                                         @SealCertificateResolved SealCertificate sealCertificate,
                                         @Parameter(description = "File")
                                         @RequestPart MultipartFile file) {

        // Integrity check

        if (StringUtils.isEmpty(sealCertificate.getSignatureImageContentId())) {
            throw new LocalizedStatusException(NOT_FOUND, "message.no_signature_for_the_given_seal_certificate_id");
        }

        // Updating data

        try (InputStream body = file.getInputStream()) {
            DocumentBuffer imageBuffer = new DocumentBuffer();
            imageBuffer.setName(file.getOriginalFilename());
            imageBuffer.setContentFlux(readInputStream(() -> body, new DefaultDataBufferFactory(), FILE_TRANSFER_BUFFER_SIZE));
            contentService.updateSignatureImage(sealCertificate.getSignatureImageContentId(), imageBuffer);
        } catch (IOException e) {
            throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, e, "message.error_reading_file");
        }

        statsService.registerAdminAction(tenant, SEAL_CERTIFICATE, UPDATE, sealCertificate.getId());
    }


    @DeleteMapping(API_V1 + "/admin/tenant/{tenantId}/sealCertificate/{sealCertificateId}/signatureImage")
    @Operation(summary = "Delete user's signature image", hidden = HIDE_UNSTABLE_API)
    @PreAuthorize("hasAnyRole('admin', 'tenant_' + #tenantId + '_admin')")
    @ResponseStatus(NO_CONTENT)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "No content"),
            @ApiResponse(responseCode = "401", description = "The user is not logged, or its session has expired"),
            @ApiResponse(responseCode = "403", description = "Authenticated user doesn't have admin's role.\n(You might want to set it in Keycloak)"),
            @ApiResponse(responseCode = "404", description = "The given sealCertificateId does not exist, or the user doesn't have any signature image.")
    })
    public void deleteSealSignatureImage(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                         @PathVariable(name = Tenant.API_PATH) String tenantId,
                                         @TenantResolved Tenant tenant,
                                         @Parameter(description = SealCertificate.API_DOC_ID_VALUE)
                                         @PathVariable(name = SealCertificate.API_PATH) String sealCertificateId,
                                         @SealCertificateResolved SealCertificate sealCertificate) {

        // Integrity check

        if (StringUtils.isEmpty(sealCertificate.getSignatureImageContentId())) {
            throw new LocalizedStatusException(NOT_FOUND, "message.no_signature_for_the_given_seal_certificate_id");
        }

        // Actual delete

        contentService.deleteSignatureImage(sealCertificate.getSignatureImageContentId());

        sealCertificate.setSignatureImageContentId(null);
        secretService.storeSealCertificate(tenant.getId(), sealCertificate);
        statsService.registerAdminAction(tenant, SEAL_CERTIFICATE, UPDATE, sealCertificate.getId());
    }


    // </editor-fold desc="Signature image CRUD">


}
