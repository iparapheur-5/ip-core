/*
 * iparapheur Core
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.ipcore.controller.admin;

import coop.libriciel.ipcore.business.auth.UserBusinessService;
import coop.libriciel.ipcore.model.auth.User;
import coop.libriciel.ipcore.model.auth.requests.UserDto;
import coop.libriciel.ipcore.model.auth.requests.UserRepresentation;
import coop.libriciel.ipcore.model.auth.requests.UserSortBy;
import coop.libriciel.ipcore.model.content.DocumentBuffer;
import coop.libriciel.ipcore.model.database.TenantRepresentation;
import coop.libriciel.ipcore.model.database.requests.StringResult;
import coop.libriciel.ipcore.services.auth.AuthServiceInterface;
import coop.libriciel.ipcore.services.content.ContentServiceInterface;
import coop.libriciel.ipcore.services.database.DatabaseServiceInterface;
import coop.libriciel.ipcore.services.database.UserPreferencesRepository;
import coop.libriciel.ipcore.services.resolvers.CurrentUserResolver.CurrentUserResolved;
import coop.libriciel.ipcore.services.resolvers.UserResolver.UserResolved;
import coop.libriciel.ipcore.services.stats.StatsServiceInterface;
import coop.libriciel.ipcore.utils.LocalizedStatusException;
import coop.libriciel.ipcore.utils.RequestUtils;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.modelmapper.ModelMapper;
import org.springdoc.api.annotations.ParameterObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.buffer.DefaultDataBufferFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static coop.libriciel.ipcore.IpCoreApplication.*;
import static coop.libriciel.ipcore.model.auth.UserPrivilege.*;
import static coop.libriciel.ipcore.model.auth.requests.UserSortBy.Constants.LAST_NAME_VALUE;
import static coop.libriciel.ipcore.services.auth.AuthServiceInterface.ROOT_ADMIN_USERNAME;
import static coop.libriciel.ipcore.services.auth.AuthServiceInterface.SERVICE_ACCOUNT_USERNAME;
import static coop.libriciel.ipcore.services.content.ContentServiceInterface.FILE_TRANSFER_BUFFER_SIZE;
import static coop.libriciel.ipcore.services.resolvers.UserResolver.UserResolved.Scope.GLOBAL;
import static coop.libriciel.ipcore.utils.ApiUtils.*;
import static java.util.stream.Collectors.toSet;
import static org.apache.commons.lang3.StringUtils.EMPTY;
import static org.apache.tomcat.util.http.fileupload.FileUploadBase.MULTIPART_FORM_DATA;
import static org.springframework.core.io.buffer.DataBufferUtils.readInputStream;
import static org.springframework.data.domain.Pageable.unpaged;
import static org.springframework.http.HttpStatus.*;


@Log4j2
@RestController
@PreAuthorize("hasRole('admin')")
@Tag(name = "admin-all-users", description = "Reserved operations on users")
public class AdminAllUserController {


    private final AuthServiceInterface authService;
    private final ContentServiceInterface contentService;
    private final DatabaseServiceInterface dbService;
    private final ModelMapper modelMapper;
    private final StatsServiceInterface statsService;
    private final UserBusinessService userBusinessService;
    private final UserPreferencesRepository userPreferencesRepository;


    // <editor-fold desc="Beans">


    @Autowired
    public AdminAllUserController(AuthServiceInterface authService,
                                  ContentServiceInterface contentService,
                                  DatabaseServiceInterface dbService,
                                  ModelMapper modelMapper,
                                  StatsServiceInterface statsService,
                                  UserBusinessService userBusinessService,
                                  UserPreferencesRepository userPreferencesRepository) {
        this.authService = authService;
        this.contentService = contentService;
        this.dbService = dbService;
        this.modelMapper = modelMapper;
        this.statsService = statsService;
        this.userBusinessService = userBusinessService;
        this.userPreferencesRepository = userPreferencesRepository;
    }


    // </editor-fold desc="Beans">


    // TODO: move this to an internal controller, we do not want to support it
    @GetMapping(API_V1 + "/admin/user/count")
    @Operation(summary = "Count logged in users")
    @ResponseStatus(OK)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public int countLoggedInUsers() {
        return this.authService.countLoggedInUsers();
    }


    // <editor-fold desc="User CRUDL">


    @GetMapping(API_PROVISIONING_V1 + "/admin/user/{userId}")
    @Operation(summary = "Get a full user representation")
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_400, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public UserDto getUserAsSuperAdmin(@Parameter(description = UserRepresentation.API_DOC_ID_VALUE)
                                       @PathVariable(name = UserRepresentation.API_PATH) String userId,
                                       @UserResolved(scope = GLOBAL) User user) {

        log.debug("getUser id:{}", userId);

        UserDto result = modelMapper.map(user, UserDto.class);
        userBusinessService.updateGlobalInnerValues(result);

        return result;
    }


    @PutMapping(API_PROVISIONING_V1 + "/admin/user/{userId}")
    @Operation(summary = "Edit a user")
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_400, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public UserDto updateUserAsSuperAdmin(@Parameter(description = UserRepresentation.API_DOC_ID_VALUE)
                                          @PathVariable(name = UserRepresentation.API_PATH) String userId,
                                          @UserResolved(scope = GLOBAL) User existingUser,
                                          @RequestBody UserDto request,
                                          @CurrentUserResolved User currentUser) {

        log.info("updateUser userId:{}", userId);
        log.debug("updateUser request:{}", request);

        // Integrity check

        authService.checkUserCrudPrivileges(currentUser, existingUser, request);

        // Registering

        Page<TenantRepresentation> administeredTenants = dbService
                .listTenantsForUser(existingUser.getId(), unpaged(), null, false, true, false);

        List<String> administeredTenantIds = administeredTenants.getContent().stream()
                .map(TenantRepresentation::getId)
                .toList();

        userBusinessService.computeGlobalLevelDifferencesAndUpdateUserRolesAndPermissions(existingUser, request);
        authService.updateUser(modelMapper, existingUser, request, administeredTenantIds);

        // TODO: check the nullable tenant compatibility and uncomment:
        //  statsService.registerAdminAction(null, USER, UPDATE, userId);

        return request;
    }


    @DeleteMapping(API_PROVISIONING_V1 + "/admin/user/{userId}")
    @Operation(summary = "Delete a user")
    @ResponseStatus(NO_CONTENT)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_204),
            @ApiResponse(responseCode = CODE_400, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public void deleteUserAsSuperAdmin(@Parameter(description = UserRepresentation.API_DOC_ID_VALUE)
                                       @PathVariable(name = UserRepresentation.API_PATH) String userId,
                                       @UserResolved(scope = GLOBAL) User targetUser,
                                       @CurrentUserResolved User currentUser) {

        log.info("deleteUser userId:{}", userId);

        // Integrity check

        if (StringUtils.equals(targetUser.getId(), currentUser.getId())) {
            throw new LocalizedStatusException(NOT_ACCEPTABLE, "message.cannot_delete_yourself");
        }

        boolean isTargetSuperAdmin = targetUser.getPrivilege() == SUPER_ADMIN;
        boolean hasOnlyOneSuperAdmin = authService.getSuperAdminsList(2).size() <= 1;
        if (isTargetSuperAdmin && hasOnlyOneSuperAdmin) {
            throw new LocalizedStatusException(NOT_ACCEPTABLE, "message.cannot_delete_the_last_admin");
        }

        boolean isServiceAccount = StringUtils.equals(targetUser.getUserName(), SERVICE_ACCOUNT_USERNAME);
        boolean isRootAdminUser = StringUtils.equals(targetUser.getUserName(), ROOT_ADMIN_USERNAME);
        if (isServiceAccount || isRootAdminUser) {
            throw new LocalizedStatusException(NOT_ACCEPTABLE, "message.cannot_touch_an_internal_user");
        }

        // Actual delete

        log.debug("deleteUser userId:{} contentNodeId:{}", userId, targetUser.getContentNodeId());
        authService.deleteUser(userId);
        userPreferencesRepository.deleteByUserId(userId);

        Optional.ofNullable(targetUser.getContentNodeId())
                .ifPresent(contentService::deleteUserData);

        // TODO: check the nullable tenant compatibility and uncomment:
        //  statsService.registerAdminAction(null, USER, DELETE, userId);
    }


    @GetMapping(API_PROVISIONING_V1 + "/admin/user")
    @Operation(summary = "List all users on the instance", description = UserSortBy.Constants.API_DOC_SORT_BY_VALUES)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_400, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public Page<UserRepresentation> listUsersAsSuperAdmin(@PageableDefault(sort = LAST_NAME_VALUE)
                                                          @ParameterObject Pageable pageable,
                                                          @Parameter(description = "Searching for a specific term within `username`, `firstName`, `lastName` or `email` fields.")
                                                          @RequestParam(required = false) String searchTerm) {

        log.debug("listAllUsers pageable:{}", pageable);
        Page<User> result = authService.listUsers(pageable, searchTerm);
        log.debug("List users result:{}", result.getSize());

        // Update status

        Set<String> userIds = result.getContent().stream().map(User::getId).collect(toSet());
        Set<String> admins = authService.filterUsersWithPrivilege(userIds, SUPER_ADMIN, null);
        Set<String> tenantAdmins = authService.filterUsersWithPrivilege(userIds, TENANT_ADMIN, null);
        Set<String> functionalAdmins = authService.filterUsersWithPrivilege(userIds, FUNCTIONAL_ADMIN, null);

        result.getContent().forEach(u -> u.setPrivilege(NONE));

        result.getContent().stream()
                .filter(u -> functionalAdmins.contains(u.getId()))
                .forEach(u -> u.setPrivilege(FUNCTIONAL_ADMIN));

        result.getContent().stream()
                .filter(u -> tenantAdmins.contains(u.getId()))
                .forEach(u -> u.setPrivilege(TENANT_ADMIN));

        result.getContent().stream()
                .filter(u -> admins.contains(u.getId()))
                .forEach(u -> u.setPrivilege(SUPER_ADMIN));

        // Sending back result

        return result.map(user -> modelMapper.map(user, UserRepresentation.class));
    }


    // </editor-fold desc="User CRUDL">


    // <editor-fold desc="Signature image CRUD">


    @PostMapping(value = API_V1 + "/admin/user/{userId}/signatureImage", consumes = MULTIPART_FORM_DATA)
    @Operation(summary = "Create user's signature image", hidden = HIDE_UNSTABLE_API)
    @ResponseStatus(CREATED)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Created"),
            @ApiResponse(responseCode = "401", description = "The user is not logged, or its session has expired"),
            @ApiResponse(responseCode = "403", description = "Authenticated user doesn't have admin's role.\n(You might want to set it in Keycloak)"),
            @ApiResponse(responseCode = "404", description = "The given userId or tenantId does not exist"),
            @ApiResponse(responseCode = "409", description = "This user already have a signature image set. You should use the PUT method")
    })
    public StringResult setSignatureImage(@Parameter(description = UserRepresentation.API_DOC_ID_VALUE)
                                          @PathVariable(name = UserRepresentation.API_PATH) String userId,
                                          @UserResolved(scope = GLOBAL) User user,
                                          @Parameter(description = "File")
                                          @RequestPart MultipartFile file) {

        log.info("setSignatureImage userId:{} imageSize:{} imageName:{}", userId, file.getSize(), file.getName());

        if (StringUtils.isNotEmpty(user.getSignatureImageContentId())) {
            throw new LocalizedStatusException(CONFLICT, "message.user_already_have_a_signature_image_set");
        }

        // Init User content data if needed

        if (StringUtils.isEmpty(user.getContentNodeId())) {

            int contentGroupIndex = Optional
                    .ofNullable(user.getContentGroupIndex())
                    .orElseGet(dbService::getUserDataGroupIndexAvailable);

            String userDataContentNodeId = contentService.createUserData(user.getId(), contentGroupIndex);
            authService.updateUserInternalMetadata(userId, contentGroupIndex, userDataContentNodeId, null);

            user.setContentGroupIndex(contentGroupIndex);
            user.setContentNodeId(userDataContentNodeId);
        }

        // Updating data

        String imageSignatureNodeId;
        try (InputStream body = file.getInputStream()) {
            DocumentBuffer imageBuffer = new DocumentBuffer();
            imageBuffer.setName(file.getOriginalFilename());
            imageBuffer.setContentFlux(readInputStream(() -> body, new DefaultDataBufferFactory(), FILE_TRANSFER_BUFFER_SIZE));
            imageSignatureNodeId = contentService.createUserSignatureImage(user.getContentNodeId(), imageBuffer);
        } catch (IOException e) {
            throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, e, "message.error_reading_file");
        }

        authService.updateUserInternalMetadata(userId, null, null, imageSignatureNodeId);
        return new StringResult(imageSignatureNodeId);
    }


    @GetMapping(API_V1 + "/admin/user/{userId}/signatureImage")
    @Operation(summary = "Get user's signature image", hidden = HIDE_UNSTABLE_API)
    @ResponseStatus(OK)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "401", description = "The user is not logged, or its session has expired"),
            @ApiResponse(responseCode = "403", description = "Authenticated user doesn't have admin's role.\n(You might want to set it in Keycloak)"),
            @ApiResponse(responseCode = "404", description = "The given userId does not exist, or the user does not have any signature image set")
    })
    public void getSignatureImage(@Parameter(description = UserRepresentation.API_DOC_ID_VALUE)
                                  @PathVariable(name = UserRepresentation.API_PATH) String userId,
                                  @UserResolved(scope = GLOBAL) User user,
                                  HttpServletResponse response) {

        log.info("getSignatureImage userId:{}", userId);

        // Integrity check

        if (StringUtils.isEmpty(user.getSignatureImageContentId())) {
            throw new LocalizedStatusException(NOT_FOUND, "message.no_signature_for_the_given_user_id");
        }

        // Image fetch

        DocumentBuffer documentBuffer = contentService.getSignatureImage(user.getSignatureImageContentId());
        log.debug("getSignatureImage retrieve from Alfresco buffer imageContentId:{} buffer:{}", user.getSignatureImageContentId(), documentBuffer);

        // Build Response

        RequestUtils.writeBufferToResponse(documentBuffer, response);
    }


    @PutMapping(API_V1 + "/admin/user/{userId}/signatureImage")
    @Operation(summary = "Replace user's signature image", hidden = HIDE_UNSTABLE_API)
    @ResponseStatus(OK)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Created"),
            @ApiResponse(responseCode = "401", description = "The user is not logged, or its session has expired"),
            @ApiResponse(responseCode = "403", description = "Authenticated user doesn't have admin's role.\n(You might want to set it in Keycloak)"),
            @ApiResponse(responseCode = "404", description = "The given userId or tenantId does not exist, or the user doesn't have any signature image to update.")
    })
    public void updateSignatureImage(@Parameter(description = UserRepresentation.API_DOC_ID_VALUE)
                                     @PathVariable(name = UserRepresentation.API_PATH) String userId,
                                     @UserResolved(scope = GLOBAL) User user,
                                     @Parameter(description = "File")
                                     @RequestPart MultipartFile file) {

        log.info("updateSignatureImage userId:{} imageSize:{} imageName:{}", userId, file.getSize(), file.getName());

        String imageNodeId = Optional.ofNullable(user.getSignatureImageContentId())
                .filter(StringUtils::isNotEmpty)
                .orElseThrow(() -> new LocalizedStatusException(NOT_FOUND, "message.no_signature_for_the_given_user_id"));

        // Updating data

        try (InputStream body = file.getInputStream()) {
            DocumentBuffer imageBuffer = new DocumentBuffer();
            imageBuffer.setName(file.getOriginalFilename());
            imageBuffer.setContentFlux(readInputStream(() -> body, new DefaultDataBufferFactory(), FILE_TRANSFER_BUFFER_SIZE));
            contentService.updateSignatureImage(imageNodeId, imageBuffer);
        } catch (IOException e) {
            throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, e, "message.error_reading_file");
        }
    }


    @DeleteMapping(API_V1 + "/admin/user/{userId}/signatureImage")
    @Operation(summary = "Delete user's signature image", hidden = HIDE_UNSTABLE_API)
    @ResponseStatus(NO_CONTENT)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "No content"),
            @ApiResponse(responseCode = "401", description = "The user is not logged, or its session has expired"),
            @ApiResponse(responseCode = "403", description = "Authenticated user doesn't have admin's role.\n(You might want to set it in Keycloak)"),
            @ApiResponse(responseCode = "404", description = "The given userId does not exist")
    })
    public void deleteSignatureImage(@Parameter(description = UserRepresentation.API_DOC_ID_VALUE)
                                     @PathVariable(name = UserRepresentation.API_PATH) String userId,
                                     @UserResolved(scope = GLOBAL) User user) {

        log.info("deleteSignatureImage userId:{}", userId);

        String imageNodeId = Optional.ofNullable(user.getSignatureImageContentId())
                .filter(StringUtils::isNotEmpty)
                .orElseThrow(() -> new LocalizedStatusException(NOT_FOUND, "message.no_signature_for_the_given_user_id"));

        contentService.deleteSignatureImage(imageNodeId);
        authService.updateUserInternalMetadata(userId, null, null, EMPTY);
    }


    // </editor-fold desc="Signature image CRUD">


}
