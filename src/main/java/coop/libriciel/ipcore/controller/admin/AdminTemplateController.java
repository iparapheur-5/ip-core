/*
 * iparapheur Core
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.ipcore.controller.admin;

import coop.libriciel.ipcore.business.workflow.FolderBusinessService;
import coop.libriciel.ipcore.model.auth.DeskRepresentation;
import coop.libriciel.ipcore.model.auth.User;
import coop.libriciel.ipcore.model.content.DocumentBuffer;
import coop.libriciel.ipcore.model.database.Subtype;
import coop.libriciel.ipcore.model.database.TemplateType;
import coop.libriciel.ipcore.model.database.Tenant;
import coop.libriciel.ipcore.model.database.Type;
import coop.libriciel.ipcore.model.database.requests.TemplateTestRequest;
import coop.libriciel.ipcore.model.mail.MailContent;
import coop.libriciel.ipcore.model.mail.MailTarget;
import coop.libriciel.ipcore.model.workflow.Folder;
import coop.libriciel.ipcore.model.workflow.Task;
import coop.libriciel.ipcore.services.content.ContentServiceInterface;
import coop.libriciel.ipcore.services.database.TenantRepository;
import coop.libriciel.ipcore.services.mail.NotificationServiceInterface;
import coop.libriciel.ipcore.services.resolvers.TenantResolver.TenantResolved;
import coop.libriciel.ipcore.services.stats.StatsServiceInterface;
import coop.libriciel.ipcore.utils.LocalizedStatusException;
import coop.libriciel.ipcore.utils.RequestUtils;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static coop.libriciel.ipcore.IpCoreApplication.API_V1;
import static coop.libriciel.ipcore.model.crypto.SignatureFormat.PADES;
import static coop.libriciel.ipcore.model.database.Type.SignatureProtocol.ACTES;
import static coop.libriciel.ipcore.model.stats.StatsCategory.TEMPLATE;
import static coop.libriciel.ipcore.model.workflow.Action.*;
import static coop.libriciel.ipcore.utils.ApiUtils.*;
import static coop.libriciel.ipcore.utils.CryptoUtils.EXAMPLE_PUBLIC_CERTIFICATE_BASE64;
import static java.nio.charset.StandardCharsets.UTF_8;
import static java.util.Collections.singletonList;
import static org.springframework.http.HttpHeaders.CONTENT_DISPOSITION;
import static org.springframework.http.HttpHeaders.CONTENT_TYPE;
import static org.springframework.http.HttpStatus.*;
import static org.springframework.http.MediaType.APPLICATION_PDF_VALUE;
import static org.springframework.http.MediaType.TEXT_PLAIN_VALUE;
import static org.springframework.util.ResourceUtils.CLASSPATH_URL_PREFIX;


@Log4j2
@RestController
@RequestMapping
@Tag(name = "admin-template", description = "Reserved operations on templates")
public class AdminTemplateController {


    public static final String TEMPLATE_PATH = CLASSPATH_URL_PREFIX + "templates/";


    private @Value(TEMPLATE_PATH + "mail_notification_single.ftl") Resource mailNotificationSingleTemplateResource;
    private @Value(TEMPLATE_PATH + "mail_notification_digest.ftl") Resource mailNotificationDigestTemplateResource;
    private @Value(TEMPLATE_PATH + "mail_action_send.ftl") Resource mailActionSendTemplateResource;
    private @Value(TEMPLATE_PATH + "docket.ftl") Resource docketTemplateResource;


    // <editor-fold desc="Beans">


    private final ContentServiceInterface contentService;
    private final NotificationServiceInterface notificationService;
    private final TenantRepository tenantRepository;
    private final StatsServiceInterface statsService;
    private final FolderBusinessService folderBusinessService;


    @Autowired
    public AdminTemplateController(ContentServiceInterface contentService,
                                   NotificationServiceInterface notificationService,
                                   TenantRepository tenantRepository,
                                   FolderBusinessService folderBusinessService,
                                   StatsServiceInterface statsService) {
        this.contentService = contentService;
        this.notificationService = notificationService;
        this.tenantRepository = tenantRepository;
        this.statsService = statsService;
        this.folderBusinessService = folderBusinessService;
    }


    // </editor-fold desc="Beans">


    @GetMapping(API_V1 + "/admin/templates/{templateType}")
    @Operation(summary = "Get the server default template")
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200, content = @Content(mediaType = TEXT_PLAIN_VALUE)),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public void getDefaultTemplate(@PathVariable TemplateType templateType,
                                   HttpServletResponse response) {

        log.info("getDefaultTemplate {}", templateType);

        // We can compute it later, but it seems clearer here.

        Resource templateResource = switch (templateType) {
            case MAIL_NOTIFICATION_SINGLE -> mailNotificationSingleTemplateResource;
            case MAIL_NOTIFICATION_DIGEST -> mailNotificationDigestTemplateResource;
            case MAIL_ACTION_SEND -> mailActionSendTemplateResource;
            case DOCKET -> docketTemplateResource;
        };

        // Computing the actual response

        response.setHeader(CONTENT_TYPE, TEXT_PLAIN_VALUE);
        try (InputStream is = templateResource.getInputStream();
             OutputStream os = response.getOutputStream()) {
            IOUtils.copy(is, os);
        } catch (IOException e) {
            throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.error_reading_file");
        }
    }


    @PostMapping(API_V1 + "/admin/tenant/{tenantId}/testMailTemplate")
    @Operation(summary = "Test the given mail template")
    @PreAuthorize("hasAnyRole('admin', 'tenant_' + #tenantId + '_admin')")
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public void testMailTemplate(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                 @PathVariable(name = Tenant.API_PATH) String tenantId,
                                 @Parameter(description = "body", required = true)
                                 @RequestBody TemplateTestRequest templateTestRequest) {

        log.info("testMailTemplate mail:{} templateSize:{}", templateTestRequest.getMail(), templateTestRequest.getTemplate().length());

        // Dummy values
        // TODO : externalize in resources

        String dummyMessage = "Message";

        User dummyUser = User.builder()
                .id("user_id").userName("username_01")
                .firstName("Prénom").lastName("Nom")
                .build();

        DeskRepresentation dummyDesk = new DeskRepresentation("desk_01", "Bureau 01");

        MailTarget dummyMailTarget = MailTarget.builder()
                .to(singletonList(templateTestRequest.getMail()))
                .build();

        Task dummyTask = Task.builder()
                .action(SIGNATURE)
                .date(new Date())
                .desks(singletonList(dummyDesk))
                .build();

        MailContent dummyMailContent = MailContent.builder()
                .tenant(Tenant.builder().id("tenant_id").name("Tenant 01").build())
                .folder(Folder.builder().id("folder_id").name("Dossier 01").build())
                .desk(dummyDesk)
                .task(dummyTask)
                .build();

        // Sending mail

        Resource tempResource = new ByteArrayResource(templateTestRequest.getTemplate().getBytes(), "file");
        notificationService.mailFolder(
                singletonList(dummyMailTarget),
                tempResource,
                singletonList(dummyMailContent),
                dummyUser,
                "Objet du mail",
                dummyMessage,
                null,
                null
        );
    }


    @PostMapping(API_V1 + "/admin/tenant/{tenantId}/testDocketPdfTemplate")
    @Operation(summary = "Test the given docket PDF template")
    @PreAuthorize("hasAnyRole('admin', 'tenant_' + #tenantId + '_admin')")
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200, content = @Content(mediaType = APPLICATION_PDF_VALUE)),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public void testPdfTemplate(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                @PathVariable(name = Tenant.API_PATH) String tenantId,
                                @Parameter(description = "body", required = true)
                                @RequestBody TemplateTestRequest templateTestRequest,
                                HttpServletResponse response) {

        log.info("testPdfTemplate mail:{} templateSize:{}", templateTestRequest.getMail(), templateTestRequest.getTemplate().length());

        // Dummy values
        // TODO : externalize in resources

        User dummyUser = User.builder()
                .id("user_id").userName("username_01")
                .firstName("Prénom").lastName("Nom")
                .build();

        DeskRepresentation dummyDesk = new DeskRepresentation("desk_id", "Bureau 01");

        Folder dummyFolder = new Folder();
        dummyFolder.setId("folder_id");
        dummyFolder.setName("Dossier 01");
        dummyFolder.setMetadata(Map.of(
                "Métadonnée 01", "Valeur 01",
                "Métadonnée 02", "Valeur 02"
        ));
        dummyFolder.setType(Type.builder()
                .id("type01").name("Type 01").description("Type 01 description")
                .isSignatureVisible(true).signatureFormat(PADES).protocol(ACTES)
                .signatureLocation("Montpellier").signatureZipCode("34000")
                .build());
        dummyFolder.setSubtype(Subtype.builder()
                .id("subtype01").name("Sous-type 01").description("Sous-type 01 description")
                .build());
        dummyFolder.setStepList(List.of(
                Task.builder()
                        .action(START).date(new Date(1609502400000L))
                        .desks(List.of(dummyDesk)).user(dummyUser)
                        .build(),
                Task.builder()
                        .action(VISA).date(new Date(1643803200000L))
                        .desks(List.of(dummyDesk)).user(dummyUser)
                        .build(),
                Task.builder()
                        .action(SIGNATURE).date(new Date(1677844800000L))
                        .publicCertificateBase64(EXAMPLE_PUBLIC_CERTIFICATE_BASE64)
                        .desks(List.of(dummyDesk)).user(dummyUser)
                        .build(),
                Task.builder()
                        .action(ARCHIVE)
                        .desks(List.of(dummyDesk)).user(dummyUser)
                        .build()
        ));

        // Generate actual PDF and send back value

        try (ByteArrayInputStream templateInputStream = new ByteArrayInputStream(templateTestRequest.getTemplate().getBytes(UTF_8));
             ByteArrayInputStream pdfInputStream = folderBusinessService.generateDocketPdfInputStream(dummyFolder, templateInputStream);
             OutputStream responseOutputStream = response.getOutputStream()) {

            response.setHeader(CONTENT_TYPE, APPLICATION_PDF_VALUE);
            response.setHeader(CONTENT_DISPOSITION, "attachment;filename=Bordereau.pdf");
            response.setContentLengthLong(pdfInputStream.available());
            IOUtils.copyLarge(pdfInputStream, responseOutputStream);

        } catch (IOException exception) {
            throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, exception, "message.error_creating_docket");
        }
    }


    // <editor-fold desc="Custom template CRUD">


    @PostMapping(API_V1 + "/admin/tenant/{tenantId}/templates/{templateType}")
    @Operation(summary = "Create a custom template")
    @PreAuthorize("hasAnyRole('admin', 'tenant_' + #tenantId + '_admin')")
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public void createCustomTemplate(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                     @PathVariable(name = Tenant.API_PATH) String tenantId,
                                     @TenantResolved Tenant tenant,
                                     @PathVariable TemplateType templateType,
                                     @Parameter(description = "Template", content = @Content(mediaType = TEXT_PLAIN_VALUE))
                                     @RequestBody String template) {

        log.info("createCustomTemplate tenantId:{} template:{}", tenant.getId(), template.length());

        // Integrity check

        if (tenant.getCustomTemplates().containsKey(templateType)) {
            throw new LocalizedStatusException(CONFLICT, "message.tenant_already_have_this_template_set");
        }

        // Store and save

        log.info("createCustomTemplate contentId:{}", tenant.getContentId());
        String nodeId = contentService.createCustomTemplate(tenant, templateType, template);

        tenant.getCustomTemplates().put(templateType, nodeId);
        tenantRepository.save(tenant);
        statsService.registerAdminAction(tenant, TEMPLATE, CREATE, templateType.name());
    }


    @GetMapping(API_V1 + "/admin/tenant/{tenantId}/templates/{templateType}")
    @Operation(summary = "Get a custom template")
    @PreAuthorize("hasAnyRole('admin', 'tenant_' + #tenantId + '_admin')")
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200, content = @Content(mediaType = TEXT_PLAIN_VALUE)),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public void getCustomTemplate(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                  @PathVariable(name = Tenant.API_PATH) String tenantId,
                                  @TenantResolved Tenant tenant,
                                  @PathVariable TemplateType templateType,
                                  HttpServletResponse response) {

        log.debug("getCustomTemplate {}", templateType);

        // Computing the actual response

        DocumentBuffer templateBuffer = Optional.ofNullable(tenant.getCustomTemplates())
                .map(m -> m.get(templateType))
                .filter(StringUtils::isNotEmpty)
                .map(i -> contentService.getCustomTemplate(tenant, templateType))
                .orElseThrow(() -> new LocalizedStatusException(NOT_FOUND, "message.missing_template"));

        response.setHeader(CONTENT_TYPE, TEXT_PLAIN_VALUE);
        RequestUtils.writeBufferToResponse(templateBuffer, response);
    }


    @PutMapping(API_V1 + "/admin/tenant/{tenantId}/templates/{templateType}")
    @Operation(summary = "Update a custom template")
    @PreAuthorize("hasAnyRole('admin', 'tenant_' + #tenantId + '_admin')")
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public void updateCustomTemplate(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                     @PathVariable(name = Tenant.API_PATH) String tenantId,
                                     @TenantResolved Tenant tenant,
                                     @PathVariable TemplateType templateType,
                                     @Parameter(description = "Template", content = @Content(mediaType = TEXT_PLAIN_VALUE))
                                     @RequestBody String template) {

        log.info("updateCustomTemplate tenantId:{} template:{}", tenant.getId(), template.length());

        // Integrity check

        if (!tenant.getCustomTemplates().containsKey(templateType)) {
            throw new LocalizedStatusException(NOT_FOUND, "message.missing_template");
        }

        // Storing

        contentService.editCustomTemplate(tenant, templateType, template);
        statsService.registerAdminAction(tenant, TEMPLATE, UPDATE, templateType.name());
    }


    @DeleteMapping(API_V1 + "/admin/tenant/{tenantId}/templates/{templateType}")
    @Operation(summary = "Delete a custom template")
    @PreAuthorize("hasAnyRole('admin', 'tenant_' + #tenantId + '_admin')")
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public void deleteCustomTemplate(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                     @PathVariable(name = Tenant.API_PATH) String tenantId,
                                     @TenantResolved Tenant tenant,
                                     @PathVariable TemplateType templateType) {

        log.info("deleteCustomTemplate tenantId:{} template:{}", tenant.getId(), templateType);

        // Integrity check

        if (!tenant.getCustomTemplates().containsKey(templateType)) {
            throw new LocalizedStatusException(NOT_FOUND, "message.missing_template");
        }

        // Delete and save

        contentService.deleteCustomTemplate(tenant, templateType);

        tenant.getCustomTemplates().remove(templateType);
        tenantRepository.save(tenant);
        statsService.registerAdminAction(tenant, TEMPLATE, DELETE, templateType.name());
    }


    // </editor-fold desc="Template CRUD">


}
