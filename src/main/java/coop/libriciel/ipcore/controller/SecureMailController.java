/*
 * iparapheur Core
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.ipcore.controller;

import coop.libriciel.ipcore.model.database.Tenant;
import coop.libriciel.ipcore.model.securemail.MailParams;
import coop.libriciel.ipcore.model.securemail.SecureMailDocument;
import coop.libriciel.ipcore.model.workflow.Folder;
import coop.libriciel.ipcore.model.workflow.Task;
import coop.libriciel.ipcore.services.resolvers.FolderResolver.FolderResolved;
import coop.libriciel.ipcore.services.resolvers.TenantResolver.TenantResolved;
import coop.libriciel.ipcore.services.securemail.SecureMailServiceInterface;
import coop.libriciel.ipcore.services.workflow.WorkflowServiceInterface;
import coop.libriciel.ipcore.utils.LocalizedStatusException;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;

import static coop.libriciel.ipcore.IpCoreApplication.API_V1;
import static coop.libriciel.ipcore.IpCoreApplication.HIDE_UNSTABLE_API;
import static coop.libriciel.ipcore.services.workflow.IpWorkflowService.METADATA_PASTELL_DOCUMENT_ID;
import static java.util.Optional.ofNullable;
import static org.springframework.http.HttpStatus.*;


@Log4j2
@RestController
@RequestMapping(API_V1 + "/tenant/{tenantId}/secureMail/server")
public class SecureMailController {

    private static final int MAX_SECURE_MAIL_SERVER_PER_TENANT_COUNT = 50;

    private final SecureMailServiceInterface secureMailService;
    private final WorkflowServiceInterface workflowService;


    @Autowired
    public SecureMailController(SecureMailServiceInterface secureMailService, WorkflowServiceInterface workflowService) {
        this.secureMailService = secureMailService;
        this.workflowService = workflowService;
    }


    @PostMapping("send")
    @Operation(hidden = HIDE_UNSTABLE_API)
    @ResponseStatus(OK)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "401", description = "The user is not logged, or its session has expired"),
            @ApiResponse(responseCode = "403", description = "Authenticated user doesn't have admin role.\n(You might want to set it in Keycloak)"),
            @ApiResponse(responseCode = "404", description = "The given tenantId does not exist")
    })
    public void createAndSendSecureMail(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                        @PathVariable(name = Tenant.API_PATH) String tenantId,
                                        @TenantResolved Tenant tenant,
                                        @RequestBody Task task,
                                        @RequestBody String folderId,
                                        @RequestBody @Valid MailParams mailParams) {

        Folder folder = ofNullable(workflowService.getFolder(folderId, tenant.getId()))
                .orElseThrow(() -> new LocalizedStatusException(NOT_FOUND, "message.unknown_folder_id"));

        this.secureMailService.createAndSendSecureMail(tenant, folder, mailParams, new ArrayList<>());
    }


    @GetMapping("{serverId}/folder/{folderId}/document")
    @Operation(summary = "Get server", hidden = HIDE_UNSTABLE_API)
    @PreAuthorize("hasAnyRole('admin', 'tenant_' + #tenantId + '_admin')")
    @ResponseStatus(OK)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "401", description = "The user is not logged, or its session has expired"),
            @ApiResponse(responseCode = "403", description = "Authenticated user doesn't have admin role.\n(You might want to set it in Keycloak)"),
            @ApiResponse(responseCode = "404", description = "The given tenantId does not exist")
    })
    public SecureMailDocument findCurrentFolderPastellDocument(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                                               @PathVariable(name = Tenant.API_PATH) String tenantId,
                                                               @PathVariable(name = Folder.API_PATH) String folderId,
                                                               @FolderResolved Folder folder,
                                                               @PathVariable Long serverId) {

        String lastDocumentId = null;

        for (Task step : folder.getStepList()) {
            if (step.getMetadata().containsKey(METADATA_PASTELL_DOCUMENT_ID)) {
                lastDocumentId = step.getMetadata().get(METADATA_PASTELL_DOCUMENT_ID);
            }
        }

        if (StringUtils.isEmpty(lastDocumentId)) throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.pastell_document_metadata_not_found");

        return this.secureMailService.findDocument(serverId.toString(), lastDocumentId);
    }
}
