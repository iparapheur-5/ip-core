/*
 * iparapheur Core
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.ipcore.controller;

import coop.libriciel.ipcore.business.workflow.FolderBusinessService;
import coop.libriciel.ipcore.business.workflow.WorkflowBusinessService;
import coop.libriciel.ipcore.model.auth.Desk;
import coop.libriciel.ipcore.model.auth.DeskRepresentation;
import coop.libriciel.ipcore.model.auth.User;
import coop.libriciel.ipcore.model.content.Document;
import coop.libriciel.ipcore.model.content.DocumentBuffer;
import coop.libriciel.ipcore.model.crypto.FolderDataToSignHolder;
import coop.libriciel.ipcore.model.crypto.SignatureFormat;
import coop.libriciel.ipcore.model.database.*;
import coop.libriciel.ipcore.model.database.requests.StringResult;
import coop.libriciel.ipcore.model.externalsignature.Status;
import coop.libriciel.ipcore.model.ipng.IpngFolderProofs;
import coop.libriciel.ipcore.model.ipng.PendingIpngFolder;
import coop.libriciel.ipcore.model.ipng.PendingIpngFolderKey;
import coop.libriciel.ipcore.model.mail.MailContent;
import coop.libriciel.ipcore.model.mail.MailTarget;
import coop.libriciel.ipcore.model.securemail.MailParams;
import coop.libriciel.ipcore.model.workflow.*;
import coop.libriciel.ipcore.services.auth.AuthServiceInterface;
import coop.libriciel.ipcore.services.content.ContentServiceInterface;
import coop.libriciel.ipcore.services.crypto.CryptoServiceInterface;
import coop.libriciel.ipcore.services.database.SubtypeRepository;
import coop.libriciel.ipcore.services.database.TypeRepository;
import coop.libriciel.ipcore.services.database.TypologyService;
import coop.libriciel.ipcore.services.externalsignature.ExternalSignatureInterface;
import coop.libriciel.ipcore.services.groovy.GroovyResultCatcher;
import coop.libriciel.ipcore.services.groovy.GroovyService;
import coop.libriciel.ipcore.services.ipng.PendingIpngFolderRepository;
import coop.libriciel.ipcore.services.mail.NotificationServiceInterface;
import coop.libriciel.ipcore.services.resolvers.CurrentUserResolver.CurrentUserResolved;
import coop.libriciel.ipcore.services.resolvers.FolderResolver.FolderResolved;
import coop.libriciel.ipcore.services.resolvers.TenantResolver.TenantResolved;
import coop.libriciel.ipcore.services.securemail.SecureMailServiceInterface;
import coop.libriciel.ipcore.services.stats.StatsServiceInterface;
import coop.libriciel.ipcore.services.workflow.WorkflowServiceInterface;
import coop.libriciel.ipcore.utils.*;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.jetbrains.annotations.NotNull;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.reactive.function.client.WebClientResponseException;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.MessageFormat;
import java.util.*;

import static coop.libriciel.ipcore.IpCoreApplication.API_V1;
import static coop.libriciel.ipcore.IpCoreApplication.HIDE_UNSTABLE_API;
import static coop.libriciel.ipcore.model.database.TemplateType.MAIL_ACTION_SEND;
import static coop.libriciel.ipcore.model.workflow.Action.*;
import static coop.libriciel.ipcore.model.workflow.State.*;
import static coop.libriciel.ipcore.model.workflow.Task.ExternalState.ERROR;
import static coop.libriciel.ipcore.model.workflow.Task.ExternalState.FORM;
import static coop.libriciel.ipcore.services.mail.NotificationServiceInterface.MAX_MAIL_ATTACHMENT_SIZE;
import static coop.libriciel.ipcore.services.resolvers.FolderResolver.FolderResolved.Permission.CURRENT_DESK;
import static coop.libriciel.ipcore.services.resolvers.FolderResolver.FolderResolved.Permission.WORKFLOW_DESK;
import static coop.libriciel.ipcore.services.workflow.IpWorkflowService.*;
import static coop.libriciel.ipcore.utils.ApiUtils.*;
import static coop.libriciel.ipcore.utils.TextUtils.MESSAGE_BUNDLE;
import static java.nio.charset.StandardCharsets.UTF_8;
import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static java.util.stream.Collectors.toSet;
import static org.apache.commons.lang3.StringUtils.isNotEmpty;
import static org.springframework.http.HttpHeaders.CONTENT_DISPOSITION;
import static org.springframework.http.HttpHeaders.CONTENT_TYPE;
import static org.springframework.http.HttpStatus.*;
import static org.springframework.http.MediaType.*;
import static org.springframework.util.ResourceUtils.CLASSPATH_URL_PREFIX;


@Log4j2
@RestController
@RequestMapping(API_V1 + "/tenant/{tenantId}/folder")
@Tag(name = "folder", description = "Folder access for a regular logged user")
public class FolderController {


    private @Value(CLASSPATH_URL_PREFIX + "templates/mail_action_send.ftl") Resource mailActionSendTemplateResource;


    // <editor-fold desc="Beans">


    private final AuthServiceInterface authService;
    private final ContentServiceInterface contentService;
    private final CryptoServiceInterface cryptoService;
    private final ExternalSignatureInterface externalSignatureService;
    private final FolderBusinessService folderBusinessService;
    private final ModelMapper modelMapper;
    private final NotificationServiceInterface notificationService;
    private final PendingIpngFolderRepository pendingIpngFolderRepository;
    private final SecureMailServiceInterface secureMailService;
    private final StatsServiceInterface statsService;
    private final SubtypeRepository subtypeRepository;
    private final TypeRepository typeRepository;
    private final TypologyService typologyService;
    private final WorkflowServiceInterface workflowService;
    private final GroovyService groovyService;
    private final WorkflowBusinessService workflowBusinessService;


    @Autowired
    public FolderController(AuthServiceInterface authService,
                            ContentServiceInterface contentService,
                            CryptoServiceInterface cryptoService,
                            ExternalSignatureInterface externalSignatureService,
                            FolderBusinessService folderBusinessService,
                            ModelMapper modelMapper,
                            NotificationServiceInterface notificationService,
                            PendingIpngFolderRepository pendingIpngFolderRepository,
                            SecureMailServiceInterface secureMailService,
                            StatsServiceInterface statsService,
                            SubtypeRepository subtypeRepository,
                            TypeRepository typeRepository,
                            TypologyService typologyService,
                            WorkflowServiceInterface workflowService, GroovyService groovyService, WorkflowBusinessService workflowBusinessService) {
        this.authService = authService;
        this.contentService = contentService;
        this.cryptoService = cryptoService;
        this.externalSignatureService = externalSignatureService;
        this.folderBusinessService = folderBusinessService;
        this.modelMapper = modelMapper;
        this.notificationService = notificationService;
        this.pendingIpngFolderRepository = pendingIpngFolderRepository;
        this.secureMailService = secureMailService;
        this.statsService = statsService;
        this.subtypeRepository = subtypeRepository;
        this.typeRepository = typeRepository;
        this.typologyService = typologyService;
        this.workflowService = workflowService;
        this.groovyService = groovyService;
        this.workflowBusinessService = workflowBusinessService;
    }


    // </editor-fold desc="Beans">


    @GetMapping("{folderId}/dataToSign")
    @Operation(hidden = HIDE_UNSTABLE_API)
    public List<FolderDataToSignHolder> getDataToSign(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                                      @PathVariable(name = Tenant.API_PATH) String tenantId,
                                                      @TenantResolved Tenant tenant,
                                                      @Parameter(description = Folder.API_DOC_ID_VALUE)
                                                      @PathVariable(name = Folder.API_PATH) String folderId,
                                                      @FolderResolved(permission = CURRENT_DESK) Folder folder,
                                                      @RequestParam String certBase64,
                                                      @Parameter(description = DeskRepresentation.API_DOC_ID_VALUE)
                                                      @RequestParam String deskId,
                                                      @CurrentUserResolved User user) {

        log.debug("getDataToSign folderId:{}, name: {}, publicKeyBase64:{} originDeskId:{}", folderId, folder.getName(), certBase64, deskId);

        // Integrity check

        typologyService.updateTypology(singletonList(folder));
        folder.setDocumentList(contentService.getDocumentList(folder.getContentId()));

        Desk desk = authService.findDeskByIdNoException(tenant.getId(), deskId);

        if (desk == null) {
            throw new LocalizedStatusException(NOT_FOUND, "message.unknown_desk_id");
        }

        // Building

        long signatureDateTime = new Date().getTime();
        String signatureImageBase64 = Optional.ofNullable(user.getSignatureImageContentId())
                .filter(StringUtils::isNotEmpty)
                .map(contentService::retrieveContentAsBase64)
                .orElse(null);

        folder.setType(typeRepository.findById(folder.getType().getId())
                .orElseThrow(() -> new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.unknown_type_id")));

        SignatureFormat signatureFormat = cryptoService.getSignatureFormat(folder.getType(), folder.getDocumentList());

        List<FolderDataToSignHolder> result = folder.getDocumentList()
                .stream()
                .filter(Document::isMainDocument)
                .map(d -> {
                    // TODO : a proper constructor, to transfer parent params ?
                    DocumentBuffer documentBuffer = contentService.retrieveContent(d.getId());
                    documentBuffer.setId(d.getId());
                    documentBuffer.setName(d.getName());
                    documentBuffer.setSignaturePlacementAnnotations(d.getSignaturePlacementAnnotations());
                    documentBuffer.setSignaturePlacementAnnotations(d.getSignaturePlacementAnnotations());
                    documentBuffer.setSignatureTags(d.getSignatureTags());
                    documentBuffer.setSealTags(d.getSealTags());
                    documentBuffer.setDetachedSignatures(d.getDetachedSignatures());
                    return documentBuffer;
                })
                .map(d -> cryptoService.getDataToSign(
                        tenant,
                        desk,
                        folder,
                        user,
                        d,
                        signatureFormat,
                        CryptoUtils.getPosition(folder, d, SIGNATURE),
                        certBase64,
                        user.getUserName(),
                        signatureDateTime,
                        false,
                        signatureImageBase64,
                        null
                ))
                .peek(data -> {if (data == null) throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.cannot_reach_crypto_service");})
                .map(dataToSignHolder -> new FolderDataToSignHolder(dataToSignHolder, folderId))
                .toList();

        log.debug("getDataToSign (folder {}) prepared:{}", folder.getName(), result);
        return result;
    }


    // <editor-fold desc="Folder CRUDL">


    @GetMapping("{folderId}")
    @Operation(summary = "Get the current folder and its data")
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public FolderDto getFolder(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                               @PathVariable(name = Tenant.API_PATH) String tenantId,
                               @TenantResolved Tenant tenant,
                               @Parameter(description = Folder.API_DOC_ID_VALUE)
                               @PathVariable(name = Folder.API_PATH) String folderId,
                               @RequestParam(required = false) String asDeskId,
                               @RequestParam(required = false, defaultValue = "false") boolean doNotMarkAsRead,
                               @FolderResolved(permission = WORKFLOW_DESK) Folder folder,
                               @CurrentUserResolved User currentUser) {

        log.debug("getFolder tenantId:{} id:{}", tenantId, folderId);

        // Retrieve inner values

        authService.updateFolderReferencedDeskNames(singletonList(folder));
        authService.updateFolderReferencedUserNames(singletonList(folder));
        typologyService.updateTypology(singletonList(folder));

        // Retrieve Status info on external Signature

        folder.getStepList()
                .stream()
                .filter(t -> Action.isExternal(t.getAction()))
                .filter(t -> FolderUtils.isActive(t.getState()))
                .forEach(t -> putExternalStateInMetadata(tenantId, folder, t));

        Set<Task> currentActiveTasks = folder.getStepList()
                .stream()
                .filter(t -> !Action.isExternal(t.getAction()))
                .filter(t -> !Action.isSecondary(t.getAction()))
                .filter(t -> FolderUtils.isActive(t.getState()))
                .collect(toSet());

        // We'll mark the folder as read only if the current user had accessed the folder from one of the current active desks.
        // If anyone has accessed the folder from the previous desk, we won't do anything.

        if (!doNotMarkAsRead && currentActiveTasks.stream()
                .map(Task::getDesks)
                .flatMap(Collection::stream)
                .map(DeskRepresentation::getId)
                .anyMatch(id -> StringUtils.equals(asDeskId, id))) {

            markAsRead(folder, currentUser);
            // Undo task has been revoked, we can simply filter out these.
            folder.getStepList().removeIf(t -> t.getAction() == UNDO);
        }

        folder.getStepList().removeIf(task -> task.getAction() == READ);

        // Retrieve documents infos

        List<Document> documentList = contentService.getDocumentList(folder.getContentId());
        log.debug("getFolder documentList:{}", documentList);
        folder.setDocumentList(documentList);

        // Add missing values

        if (folder.getType() != null) {
            folder.getType().setSignatureFormat(cryptoService.getSignatureFormat(folder.getType(), folder.getDocumentList()));
        }

        // Sending back result

        log.debug("getFolder result:{}", folder);
        return modelMapper.map(folder, FolderDto.class);
    }


    @PutMapping("{folderId}")
    @Operation(summary = "Edit folder parameters")
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public FolderDto updateFolder(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                  @PathVariable(name = Tenant.API_PATH) String tenantId,
                                  @Parameter(description = Folder.API_DOC_ID_VALUE)
                                  @PathVariable(value = Folder.API_PATH) String folderId,
                                  @FolderResolved(permission = CURRENT_DESK) Folder existingFolder,
                                  @RequestBody @Valid FolderDto updatedFolder) {

        log.info("updateFolder id:{} name:{} typology:{}/{}", folderId, updatedFolder.getName(), updatedFolder.getTypeId(), updatedFolder.getSubtypeId());

        // Integrity check

        Folder dummyUpdatedFolder = modelMapper.map(updatedFolder, Folder.class);

        typologyService.updateTypology(List.of(existingFolder, dummyUpdatedFolder));
        List<Document> documentList = contentService.getDocumentList(existingFolder.getContentId());


        if (existingFolder.getType() != null) {
            SignatureFormat oldSignatureFormat = cryptoService.getSignatureFormat(existingFolder.getType(), documentList);
            existingFolder.getType().setSignatureFormat(oldSignatureFormat);
        }

        SignatureFormat newSignatureFormat = cryptoService.getSignatureFormat(dummyUpdatedFolder.getType(), documentList);
        dummyUpdatedFolder.getType().setSignatureFormat(newSignatureFormat);

        CryptoUtils.checkSignatureFormat(newSignatureFormat, documentList, null);

        if (existingFolder.getType() != null) {
            CryptoUtils.checkTypeReplacementCompatibility(existingFolder.getType(), dummyUpdatedFolder.getType());
        }

        if (existingFolder.getSubtype() != null) {
            CryptoUtils.checkSubtypeReplacementCompatibility(existingFolder.getSubtype(), dummyUpdatedFolder.getSubtype());
        }

        // Actual edit

        workflowService.editFolder(folderId, updatedFolder);
        return modelMapper.map(dummyUpdatedFolder, FolderDto.class);
    }


    @GetMapping("{folderId}/zip")
    @Operation(summary = "Get every file as ZIP, with a PREMIS-formatted summary")
    @ResponseStatus(OK)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200, content = @Content(mediaType = APPLICATION_OCTET_STREAM_VALUE)),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public void downloadFolderZip(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                  @PathVariable(name = Tenant.API_PATH) String tenantId,
                                  @TenantResolved Tenant tenant,
                                  @Parameter(description = Folder.API_DOC_ID_VALUE)
                                  @PathVariable(name = Folder.API_PATH) String folderId,
                                  @FolderResolved(permission = CURRENT_DESK) Folder folder,
                                  @CurrentUserResolved User currentUser,
                                  @NotNull HttpServletResponse response) {

        log.debug("downloadFolderZip tenantId:{} folderId:{}", tenant.getId(), folder.getId());

        // Update inner values

        folder.setDocumentList(contentService.getDocumentList(folder.getContentId()));
        typologyService.updateTypology(singletonList(folder));

        // Generate an (in-memory) temp Premis file

        folderBusinessService.prepareFinalTaskForPremisFile(folder.getStepList(), currentUser);
        folderBusinessService.prepareChecksumDataForPremisFile(folder.getDocumentList());
        String premisContent = folderBusinessService.generatePremisContent(folder);

        // Trigger the client download

        folderBusinessService.downloadFolderAsZip(response, tenant, folder, new ByteArrayInputStream(premisContent.getBytes(UTF_8)));
    }


    @DeleteMapping("{folderId}")
    @Operation(
            summary = "Delete folder",
            description = """
                          Deletes the given folder.
                          Note that current pending folders cannot be deleted here. Only drafts/rejected/finished ones.
                          To delete a currently pending folder, you need to go through the admin's access point.
                          """
    )
    @ResponseStatus(NO_CONTENT)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_204),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public void deleteFolder(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                             @PathVariable(name = Tenant.API_PATH) String tenantId,
                             @TenantResolved Tenant tenant,
                             @Parameter(description = Folder.API_DOC_ID_VALUE)
                             @PathVariable(name = Folder.API_PATH) String folderId,
                             @FolderResolved(permission = CURRENT_DESK) Folder folder) {

        log.info("deleteFolder {}", folder.getId());

        // Integrity check

        if (folder.getState() == PENDING) {
            throw new LocalizedStatusException(CONFLICT, "message.this_workflow_is_still_in_a_pending_state");
        }

        // Deleting

        contentService.deleteFolder(folder);

        Optional.ofNullable(folder.getId())
                .ifPresent(workflowService::deleteWorkflow);

        Long timeToCompleteInHours = folder.getStepList()
                .stream()
                .filter(t -> (t.getAction() == ARCHIVE) || (t.getAction() == DELETE))
                .findFirst()
                .map(statsService::computeTimeToCompleteInHours)
                .orElse(null);

        statsService.registerFolderAction(tenant, DELETE, folder, folder.getOriginDesk(), timeToCompleteInHours);
    }


    // </editor-fold desc="Folder CRUDL">


    @GetMapping("{folderId}/ipngProofs")
    @Operation(summary = "Get the current folder and its data", hidden = HIDE_UNSTABLE_API)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "401", description = "The user is not logged, or its session has expired"),
            @ApiResponse(responseCode = "403", description = "Authenticated user doesn't have desk's role.\n(You might want to set it in Keycloak)"),
            @ApiResponse(responseCode = "404", description = "The given folderId does not exist")
    })
    public List<IpngFolderProofs> getFolderIpngProofs(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                                      @PathVariable(name = Tenant.API_PATH) String tenantId,
                                                      @TenantResolved Tenant tenant,
                                                      @Parameter(description = Folder.API_DOC_ID_VALUE)
                                                      @PathVariable(name = Folder.API_PATH) String folderId,
//                                                      @RequestParam(required = false) String asDeskId,
                                                      @FolderResolved(permission = WORKFLOW_DESK) Folder folder,
                                                      @CurrentUserResolved User currentUser) {

        return folderBusinessService.gatherIpngProofsForFolder(folder);
    }


    @GetMapping("{folderId}/ipngProofsFile")
    @Operation(summary = "Get the current folder and its data", hidden = HIDE_UNSTABLE_API)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "401", description = "The user is not logged, or its session has expired"),
            @ApiResponse(responseCode = "403", description = "Authenticated user doesn't have desk's role.\n(You might want to set it in Keycloak)"),
            @ApiResponse(responseCode = "404", description = "The given folderId does not exist")
    })
    public void getFolderIpngProofsFile(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                        @PathVariable(name = Tenant.API_PATH) String tenantId,
                                        @TenantResolved Tenant tenant,
                                        @Parameter(description = Folder.API_DOC_ID_VALUE)
                                        @PathVariable(name = Folder.API_PATH) String folderId,
                                        @FolderResolved(permission = WORKFLOW_DESK) Folder folder,
                                        @CurrentUserResolved User currentUser,
                                        HttpServletResponse response) throws IOException {


        Path tempFileResult = folderBusinessService.createIpngProofFileForFolder(folder);
        try (InputStream is = Files.newInputStream(tempFileResult);
             OutputStream os = response.getOutputStream()) {
            response.setHeader(CONTENT_TYPE, APPLICATION_JSON_VALUE);
            IOUtils.copy(is, os);
        }
    }


    /**
     * @param folder      with a {@link Folder#getStepList()} properly available
     * @param currentUser
     */
    private void markAsRead(@NotNull Folder folder, @NotNull User currentUser) {
        log.debug("markAsRead folderId:{} currentUser:{}", folder.getId(), currentUser.getUserName());
        List<Task> readTasks = workflowService.getReadTasks(folder);

        Pair<Long, Long> currentIndex = folder.getStepList().stream()
                .filter(t -> t.getState() == CURRENT)
                .map(t -> Pair.of(t.getWorkflowIndex(), t.getStepIndex()))
                .findFirst()
                .orElse(Pair.of(null, null));

        boolean hasAlreadyBeenReadThisStep = workflowService.hasAlreadyBeenReadAtIndex(currentIndex, currentUser, readTasks);
        log.debug("markAsRead hasAlreadyBeenReadThisStep:{}", hasAlreadyBeenReadThisStep);
        if (!hasAlreadyBeenReadThisStep) {
            readTasks.stream()
                    .filter(t -> t.getDate() == null)
                    .findFirst()
                    .ifPresent(t -> workflowService.performTask(t, READ, folder, null, null, null, null));
        }
    }


    private void putExternalStateInMetadata(String tenantId, Folder folder, Task task) {

        if (folder.getSubtype() == null) {
            return;
        }

        switch (task.getAction()) {
            case EXTERNAL_SIGNATURE -> {
                ExternalSignatureConfig extSignConfig = folder.getSubtype().getExternalSignatureConfig();
                String transactionId = task.getExternalSignatureProcedureId();
                if (transactionId != null && extSignConfig != null) {
                    Status status = externalSignatureService.getProcedureStatus(extSignConfig, transactionId);
                    task.getMetadata().put(METADATA_STATUS, status.name());
                } else if (extSignConfig != null) {
                    task.getMetadata().put(METADATA_STATUS, FORM.name());
                } else {
                    task.getMetadata().put(METADATA_STATUS, ERROR.name());
                }
            }
            case SECURE_MAIL -> {
                if (Objects.nonNull(folder.getSubtype().getSecureMailServerId())
                        && Objects.nonNull(task.getMetadata().get(METADATA_PASTELL_DOCUMENT_ID))) {
                    String lastAction;
                    try {
                        lastAction = secureMailService.findDocument(
                                folder.getSubtype().getSecureMailServerId().toString(),
                                task.getMetadata().get(METADATA_PASTELL_DOCUMENT_ID)
                        ).getLastAction();
                    } catch (WebClientResponseException e) {
                        lastAction = "ERROR";
                    }

                    task.getMetadata().put(METADATA_STATUS, lastAction);

                }
            }
            case IPNG -> {
                String ipngBusinessId = task.getMetadata().get(METADATA_IPNG_BUSINESS_ID);
                if (ipngBusinessId != null) {
                    Optional<PendingIpngFolder> pendingIpngOpt = this.pendingIpngFolderRepository.findById(new PendingIpngFolderKey(ipngBusinessId, tenantId));
                    pendingIpngOpt.ifPresent(pendingIpngData -> {
                        Task.ExternalState extState = switch (pendingIpngData.getState()) {
                            case SENT -> Task.ExternalState.CREATED;
                            case IN_NETWORK -> Task.ExternalState.SENT;
                            case RECEIVED -> Task.ExternalState.RECEIVED;
                            default -> Task.ExternalState.ERROR;
                        };
                        task.getMetadata().put(METADATA_STATUS, extState.name());
                    });
                }
            }
        }

        setTaskState(task);
    }


    @GetMapping("{folderId}/historyTasks")
    @Operation(summary = "Get the current folder and its data", hidden = HIDE_UNSTABLE_API)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public List<Task> getFolderHistoryTasks(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                            @PathVariable(name = Tenant.API_PATH) String tenantId,
                                            @Parameter(description = Folder.API_DOC_ID_VALUE)
                                            @PathVariable(name = Folder.API_PATH) String folderId,
                                            @FolderResolved(permission = WORKFLOW_DESK) Folder folder) {

        log.debug("getFolderHistoryTasks folderId:{}", folder.getId());

        List<Task> tasks = workflowService.getHistoricTasks(folderId);
        authService.updateTaskReferencedDeskNames(tasks);
        authService.updateTaskReferencedUserNames(tasks);
        parseChainStepsForHistory(tasks);

        log.debug("getFolderHistoryTasks result:{}", tasks);
        return tasks;
    }


    @PostMapping("{folderId}/mail")
    @Operation(summary = "Send given folder by mail")
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_400, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public void sendFolder(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                           @PathVariable(name = Tenant.API_PATH) String tenantId,
                           @TenantResolved Tenant tenant,
                           @Parameter(description = Folder.API_DOC_ID_VALUE)
                           @PathVariable(name = Folder.API_PATH) String folderId,
                           @FolderResolved(permission = WORKFLOW_DESK) Folder folder,
                           @RequestBody @Valid MailParams params,
                           @CurrentUserResolved User currentUser) {

        log.info("sendFolder id:{} mail:{}", folderId, params);

        // Prepare mail

        MailTarget mailTarget = new MailTarget(params);

        Resource mailTemplateResource = Optional.ofNullable(tenant.getCustomTemplates())
                .filter(t -> t.containsKey(MAIL_ACTION_SEND))
                .map(i -> contentService.getCustomTemplate(tenant, MAIL_ACTION_SEND))
                .filter(d -> d.getContentLength() > 0)
                .map(RequestUtils::writeBufferToString)
                .map(s -> (Resource) new ByteArrayResource(s.getBytes(UTF_8)))
                .orElse(mailActionSendTemplateResource);

        Desk desk = folder
                .getStepList()
                .stream()
                .filter(s -> s.getState().equals(PENDING))
                .findFirst()
                .map(Task::getDesks)
                // FIXME we should not do a "findfirst" here,
                //  we should instead have a param from the request (like "asDesk" in getFolder)
                .orElse(emptyList()).stream().findFirst().map(DeskRepresentation::getId)
                .map(deskId -> Desk
                        .builder()
                        .id(deskId)
                        .build()
                )
                .orElse(null);

        folder.setDocumentList(contentService.getDocumentList(folder.getContentId()));
        typologyService.updateTypology(singletonList(folder));

        MailContent mailContent = new MailContent(
                tenant,
                folder,
                null,
                // FIXME remove modelMapper when all internal actions are mapped to Desk in controllers
                modelMapper.map(desk, DeskRepresentation.class),
                null,
                null
        );

        Set<String> annexesToIncludeIds =
                folder.getSubtype().isAnnexeIncluded()
                ? folder.getDocumentList().stream()
                        .filter(d -> !d.isMainDocument())
                        .map(Document::getId)
                        .collect(toSet())
                : Collections.emptySet();

        Path printDocTempFile = folderBusinessService.generatePrintDocAsTempFile(tenant, folder, params.isIncludeDocket(), annexesToIncludeIds);
        String targetAttachmentName = "%s.pdf".formatted(FileUtils.cleanFileSystemForbiddenCharacters(folder.getName()));

        try (InputStream printDocInputStream = Files.newInputStream(printDocTempFile)) {
            long printDocSize = Files.size(printDocTempFile);
            if (printDocSize > MAX_MAIL_ATTACHMENT_SIZE) {
                log.warn("Cannot send folder by mail, the print document is too heavy for folder {}, size : {}K ", folder.getId(), printDocSize / 1024);
                log.debug("tenant name : {}, folder name : {}", tenant.getName(), folder.getName());
                throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.cannot_send_mail_file_too_big");
            }

            notificationService.mailFolder(
                    singletonList(mailTarget),
                    mailTemplateResource,
                    singletonList(mailContent),
                    currentUser,
                    params.getObject(),
                    params.getMessage(),
                    printDocInputStream,
                    targetAttachmentName
            );
        } catch (IOException e) {
            log.error("An error occurred while reading generated print document to send by mail for Tenant : {}, Folder : {}", tenant.getId(), folder.getId());
            log.debug("tenant name : {}, folder name : {}, exception details : ", tenant.getName(), folder.getName(), e);
            throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.error_reading_file");
        } finally {
            try {
                Files.delete(printDocTempFile);
            } catch (IOException e) {
                log.error("Error deleting temporary print document at path : {}", printDocTempFile.toString());
                log.debug("Error detail : ", e);
            }
        }
    }


    @GetMapping("{folderId}/print")
    @Operation(summary = "Get the PDF printable version of a folder")
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200, content = @Content(mediaType = APPLICATION_PDF_VALUE)),
            @ApiResponse(responseCode = CODE_400, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public void printFolder(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                            @PathVariable(name = Tenant.API_PATH) String tenantId,
                            @TenantResolved Tenant tenant,
                            @Parameter(description = Folder.API_DOC_ID_VALUE)
                            @PathVariable(name = Folder.API_PATH) String folderId,
                            @FolderResolved(permission = WORKFLOW_DESK) Folder folder,
                            @RequestParam(required = false, defaultValue = "true") boolean includeDocket,
                            @RequestParam(required = false) List<String> annexesIds,
                            HttpServletResponse response) {

        log.info("printFolder id:{} includeDocket:{} annexesIds:{}", folderId, includeDocket, annexesIds);

        // Fetch missing infos

        folder.setDocumentList(contentService.getDocumentList(folder.getContentId()));
        typologyService.updateTypology(singletonList(folder));

        // Integrity check

        Set<String> existingAnnexesIds = folder.getDocumentList()
                .stream()
                .filter(d -> !d.isMainDocument())
                .map(Document::getId)
                .collect(toSet());

        Set<String> finalAnnexesIds = new HashSet<>(Optional.ofNullable(annexesIds).orElse(emptyList()));
        if (!CollectionUtils.containsAll(existingAnnexesIds, finalAnnexesIds)) {
            throw new LocalizedStatusException(NOT_FOUND, "message.unknown_document_id");
        }

        // Generate document

        Path printDocTempFile = folderBusinessService.generatePrintDocAsTempFile(tenant, folder, includeDocket, finalAnnexesIds);

        ResourceBundle messageResourceBundle = ResourceBundle.getBundle(MESSAGE_BUNDLE, Locale.getDefault());
        String resultFileNameTemplate = messageResourceBundle.getString("message.printable_pdf_name");
        String resultFileName = MessageFormat.format(resultFileNameTemplate, folder.getName());

        response.setHeader(CONTENT_DISPOSITION, "attachment; filename=" + URLEncoder.encode(resultFileName, UTF_8));
        response.setHeader(CONTENT_TYPE, APPLICATION_PDF.toString());

        log.debug("printFolder - merged the doc into temp file : {}", printDocTempFile.toString());
        try (InputStream is = Files.newInputStream(printDocTempFile)) {
            is.transferTo(response.getOutputStream());
            response.flushBuffer();
        } catch (IOException e) {
            throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, e, "message.error_merging_pdf");
        } finally {
            try {
                Files.delete(printDocTempFile);
            } catch (IOException e) {
                log.error("Error deleting temporary print document at path : {}", printDocTempFile.toString());
                log.debug("Error detail : ", e);
            }
        }
    }


    @PutMapping("{folderId}/metadata/{metadataId}")
    @Operation(summary = "Update folder's metadata value", hidden = HIDE_UNSTABLE_API)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "400", description = "The given value does not match the subtype values restrictions"),
            @ApiResponse(responseCode = "401", description = "The user is not logged, or its session has expired"),
            @ApiResponse(responseCode = "403", description = "Authenticated user doesn't have desk's role.\n(You might want to set it in Keycloak)"),
            @ApiResponse(responseCode = "404", description = "The given folderId/metadataId does not exist")
    })
    public void setMetadata(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                            @PathVariable(name = Tenant.API_PATH) String tenantId,
                            @Parameter(description = Folder.API_DOC_ID_VALUE)
                            @PathVariable(name = Folder.API_PATH) String folderId,
                            @FolderResolved(permission = CURRENT_DESK) Folder folder,
                            @Parameter(description = Metadata.API_DOC_ID_VALUE)
                            @PathVariable(name = Metadata.API_PATH) String metadataId,
                            @RequestBody StringResult stringRequest) {

        log.info("setMetadata folderId:{} metadataId:{} value:{}", folderId, metadataId, stringRequest.getValue());

        // Integrity check

        Metadata metadata = Optional.ofNullable(folder.getSubtype())
                .map(TypologyEntity::getId)
                .flatMap(subtypeRepository::findById)
                .map(Subtype::getSubtypeMetadataList)
                .orElse(emptyList())
                .stream()
                .map(SubtypeMetadata::getMetadata)
                .filter(m -> StringUtils.equals(metadataId, m.getId()))
                .findFirst()
                .orElseThrow(() -> new LocalizedStatusException(NOT_FOUND, "message.unknown_metadata_id"));

        if (CollectionUtils.isNotEmpty(metadata.getRestrictedValues())
                && metadata.getRestrictedValues().stream().noneMatch(s -> StringUtils.equals(s, stringRequest.getValue()))) {
            throw new LocalizedStatusException(BAD_REQUEST, "message.metadata_given_value_is_not_in_restricted_values");
        }

        // Actual edit

        FolderDto editRequest = new FolderDto();
        editRequest.setMetadata(Map.of(metadata.getKey(), stringRequest.getValue()));
        editRequest.setTypeId(folder.getType().getId());
        editRequest.setSubtypeId(folder.getSubtype().getId());
        editRequest.setName(folder.getName());
        workflowService.editFolder(folder.getId(), editRequest);
        this.editWorkflowDefinitionFromScript(folder, metadata, stringRequest, tenantId);

    }


    private void editWorkflowDefinitionFromScript(Folder folder, Metadata editedMetadata, StringResult stringRequest, String tenantId) {
        if (folder.getState() != DRAFT) return;

        typologyService.updateTypology(singletonList(folder));
        folder.setMetadata(Map.of(editedMetadata.getKey(), stringRequest.getValue()));
        Optional<GroovyResultCatcher> scriptResultWrapped = groovyService.computeSelectionScriptForFolder(tenantId, folder);

        WorkflowDefinitionDto newWorkflowDefinition = workflowBusinessService.computeWorkflowDefinitionFromScriptResult(
                scriptResultWrapped,
                tenantId,
                folder.getOriginDesk().getId()
        );

        workflowService.editFolderValidationWorkflow(folder.getId(), newWorkflowDefinition);
    }


    private void setTaskState(Task task) {
        if (Objects.nonNull(task.getMetadata())
                && task.getMetadata().containsKey(METADATA_STATUS)
                && isNotEmpty(task.getMetadata().get(METADATA_STATUS))) {
            task.setExternalState(FolderUtils.mapTaskStatus(task.getMetadata().get(METADATA_STATUS)));
        }
    }


    private void parseChainStepsForHistory(List<Task> tasks) {
        tasks.forEach(task -> {
            if (task.getPerformedAction() != null && task.getPerformedAction().equals(CHAIN)) {
                task.setState(VALIDATED);
                task.setAction(CHAIN);
            }
        });
    }


}
