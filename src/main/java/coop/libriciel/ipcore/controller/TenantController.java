/*
 * iparapheur Core
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.ipcore.controller;

import coop.libriciel.ipcore.model.auth.User;
import coop.libriciel.ipcore.model.database.Tenant;
import coop.libriciel.ipcore.model.database.TenantRepresentation;
import coop.libriciel.ipcore.model.database.TenantSortBy;
import coop.libriciel.ipcore.model.database.requests.TenantDto;
import coop.libriciel.ipcore.services.database.DatabaseServiceInterface;
import coop.libriciel.ipcore.services.database.TenantRepository;
import coop.libriciel.ipcore.services.resolvers.TenantResolver.TenantResolved;
import coop.libriciel.ipcore.utils.KeycloakSecurityUtils;
import coop.libriciel.ipcore.utils.LocalizedStatusException;
import coop.libriciel.ipcore.utils.RequestUtils;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.log4j.Log4j2;
import org.keycloak.KeycloakPrincipal;
import org.keycloak.KeycloakSecurityContext;
import org.modelmapper.ModelMapper;
import org.springdoc.api.annotations.ParameterObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

import static coop.libriciel.ipcore.IpCoreApplication.API_V1;
import static coop.libriciel.ipcore.model.database.TenantSortBy.Constants.NAME_VALUE;
import static coop.libriciel.ipcore.services.auth.KeycloakService.SUPER_ADMIN_ROLE_NAME;
import static coop.libriciel.ipcore.utils.ApiUtils.*;
import static org.springframework.http.HttpStatus.FORBIDDEN;


@Log4j2
@RestController
@RequestMapping(API_V1 + "/tenant")
@Tag(name = "tenant", description = "Tenant access for a regular logged user")
public class TenantController {


    // <editor-fold desc="Beans">


    private final DatabaseServiceInterface dbService;
    private final ModelMapper modelMapper;
    private final TenantRepository tenantRepository;


    @Autowired
    public TenantController(TenantRepository tenantRepository,
                            ModelMapper modelMapper,
                            DatabaseServiceInterface dbService) {
        this.dbService = dbService;
        this.modelMapper = modelMapper;
        this.tenantRepository = tenantRepository;
    }


    // </editor-fold desc="Beans">


    @GetMapping("{tenantId}")
    @Operation(summary = "Get a tenant with every information set")
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public TenantDto getTenant(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                               @PathVariable(name = Tenant.API_PATH) String tenantId,
                               @TenantResolved Tenant tenant) {

        log.debug("getTenant id:{} name:{}", tenant.getId(), tenant.getName());

        Set<String> accessibleTenants = KeycloakSecurityUtils.getCurrentUserTenantIds();
        boolean isNotLinkedToTenant = !accessibleTenants.contains(tenant.getId());
        boolean isNotSuperAdmin = !KeycloakSecurityUtils.isSuperAdmin();
        if (isNotLinkedToTenant && isNotSuperAdmin) {
            throw new LocalizedStatusException(FORBIDDEN, "message.you_can_t_view_this_tenant");
        }

        return modelMapper.map(tenant, TenantDto.class);
    }


    @GetMapping
    @Operation(summary = "List tenants associated with current user", description = TenantSortBy.Constants.API_DOC_SORT_BY_VALUES)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_400, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public Page<TenantRepresentation> listTenantsForUser(@PageableDefault(sort = NAME_VALUE)
                                                         @ParameterObject Pageable pageable,
                                                         @Parameter(description = "filtering the ones as administrator")
                                                         @RequestParam(defaultValue = "false") boolean withAdminRights) {

        log.debug("listTenants page:{} pageSize:{} withAdminRights:{}", pageable.getPageNumber(), pageable.getOffset(), withAdminRights);

        KeycloakPrincipal<KeycloakSecurityContext> principal = KeycloakSecurityUtils.getLoggedInPrincipal();
        boolean isSuperAdmin = principal.getKeycloakSecurityContext().getToken().getRealmAccess().isUserInRole(SUPER_ADMIN_ROLE_NAME);
        String userId = principal.toString();
        User currentUser = User.builder().id(userId).build();

        log.debug("listTenantsForUser userId:{} isSuperAdmin:{}", userId, isSuperAdmin);

        // Retrieving tenant ids from service

        Pageable innerPageable = RequestUtils.convertSortedPageable(pageable, TenantSortBy.class, TenantSortBy::getColumnName);
        // Special case : When the current user is a super-admin, and asks for the tenants that he can administrate,
        // we'll just return everything. No need to check anything else here.
        Page<TenantRepresentation> result = (withAdminRights && isSuperAdmin)
                                            ? tenantRepository.findAll(innerPageable).map(tenant -> modelMapper.map(tenant, TenantRepresentation.class))
                                            : dbService.listTenantsForUser(currentUser.getId(), innerPageable, null, false, withAdminRights, true);

        // Sending back result

        log.info("listTenantsForUser result:{}", result.getTotalElements());
        return result;
    }


}
