/*
 * iparapheur Core
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.ipcore.controller.websockets;

import coop.libriciel.ipcore.business.auth.DeskBusinessService;
import coop.libriciel.ipcore.model.auth.Desk;
import coop.libriciel.ipcore.model.websockets.DeskCount;
import coop.libriciel.ipcore.services.auth.AuthServiceInterface;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;


@Controller
public class WebSocketController {

    private final AuthServiceInterface authService;
    private final SimpMessagingTemplate simpMessagingTemplate;
    private final DeskBusinessService deskBusinessService;


    public WebSocketController(AuthServiceInterface authService,
                               SimpMessagingTemplate simpMessagingTemplate,
                               DeskBusinessService deskBusinessService) {
        this.authService = authService;
        this.simpMessagingTemplate = simpMessagingTemplate;
        this.deskBusinessService = deskBusinessService;
    }


    @MessageMapping("/websocket/tenant/{tenantId}/desk/{deskId}/folder/notify")
    public void notifyAllOfFolderAction(@DestinationVariable String tenantId,
                                        @DestinationVariable String deskId) {


        Desk desk = authService.findDeskByIdNoException(tenantId, deskId);

        if (desk == null) return;

        DeskCount deskCountNotification = deskBusinessService.getDeskCount(desk);


        simpMessagingTemplate.convertAndSend("/websocket/tenant/" + tenantId + "/desk/" + deskId + "/folder/watch", deskCountNotification);
    }


}
