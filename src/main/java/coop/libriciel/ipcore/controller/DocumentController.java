/*
 * iparapheur Core
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.ipcore.controller;

import coop.libriciel.ipcore.model.auth.DeskRepresentation;
import coop.libriciel.ipcore.model.auth.User;
import coop.libriciel.ipcore.model.content.DetachedSignature;
import coop.libriciel.ipcore.model.content.Document;
import coop.libriciel.ipcore.model.content.DocumentBuffer;
import coop.libriciel.ipcore.model.crypto.SignatureFormat;
import coop.libriciel.ipcore.model.database.Tenant;
import coop.libriciel.ipcore.model.pdfstamp.SignaturePlacement;
import coop.libriciel.ipcore.model.pdfstamp.StickyNote;
import coop.libriciel.ipcore.model.workflow.Folder;
import coop.libriciel.ipcore.model.workflow.Task;
import coop.libriciel.ipcore.services.auth.AuthServiceInterface;
import coop.libriciel.ipcore.services.content.ContentServiceInterface;
import coop.libriciel.ipcore.services.crypto.CryptoServiceInterface;
import coop.libriciel.ipcore.services.database.TypologyService;
import coop.libriciel.ipcore.services.pdfstamp.PdfStampServiceInterface;
import coop.libriciel.ipcore.services.pesviewer.PesViewerServiceInterface;
import coop.libriciel.ipcore.services.resolvers.CurrentUserResolver.CurrentUserResolved;
import coop.libriciel.ipcore.services.resolvers.FolderResolver.FolderResolved;
import coop.libriciel.ipcore.services.resolvers.TenantResolver.TenantResolved;
import coop.libriciel.ipcore.services.transformation.TransformationServiceInterface;
import coop.libriciel.ipcore.services.workflow.WorkflowServiceInterface;
import coop.libriciel.ipcore.utils.FolderUtils;
import coop.libriciel.ipcore.utils.LocalizedStatusException;
import coop.libriciel.ipcore.utils.RequestUtils;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.buffer.DefaultDataBufferFactory;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.util.stream.IntStream;

import static coop.libriciel.ipcore.IpCoreApplication.API_V1;
import static coop.libriciel.ipcore.IpCoreApplication.HIDE_UNSTABLE_API;
import static coop.libriciel.ipcore.controller.DeskController.MAX_DETACHED_SIGNATURES_PER_DOCUMENT_ON_STARTUP;
import static coop.libriciel.ipcore.model.crypto.SignatureFormat.AUTO;
import static coop.libriciel.ipcore.model.workflow.Action.START;
import static coop.libriciel.ipcore.services.content.ContentServiceInterface.FILE_TRANSFER_BUFFER_SIZE;
import static coop.libriciel.ipcore.services.resolvers.FolderResolver.FolderResolved.Permission.CURRENT_DESK;
import static coop.libriciel.ipcore.services.resolvers.FolderResolver.FolderResolved.Permission.WORKFLOW_DESK;
import static coop.libriciel.ipcore.utils.CryptoUtils.checkIfDetachedSignatureAllowed;
import static coop.libriciel.ipcore.utils.CryptoUtils.checkSignatureFormat;
import static java.util.Collections.singletonList;
import static java.util.Optional.ofNullable;
import static org.apache.tomcat.util.http.fileupload.FileUploadBase.MULTIPART_FORM_DATA;
import static org.springframework.core.io.buffer.DataBufferUtils.readInputStream;
import static org.springframework.http.HttpStatus.*;


@Log4j2
@RestController
@RequestMapping(API_V1 + "/tenant/{tenantId}/folder/{folderId}/document")
public class DocumentController {


    public static final int MAX_MAIN_DOCUMENTS = 30;
    public static final int MAX_DOCUMENTS = 100;


    // <editor-fold desc="Beans">


    private final ContentServiceInterface contentService;
    private final AuthServiceInterface authService;
    private final TransformationServiceInterface transformationService;
    private final WorkflowServiceInterface workflowService;
    private final PdfStampServiceInterface pdfStampService;
    private final PesViewerServiceInterface pesViewerService;
    private final TypologyService typologyService;
    private final CryptoServiceInterface cryptoService;


    @Autowired
    public DocumentController(ContentServiceInterface contentService,
                              AuthServiceInterface authService,
                              TransformationServiceInterface transformationService,
                              WorkflowServiceInterface workflowService,
                              PdfStampServiceInterface pdfStampService,
                              PesViewerServiceInterface pesViewerService,
                              CryptoServiceInterface cryptoService,
                              TypologyService typologyService) {
        this.contentService = contentService;
        this.authService = authService;
        this.transformationService = transformationService;
        this.workflowService = workflowService;
        this.pesViewerService = pesViewerService;
        this.pdfStampService = pdfStampService;
        this.cryptoService = cryptoService;
        this.typologyService = typologyService;
    }


    // </editor-fold desc="Beans">


    // <editor-fold desc="Document CRUD">


    @PostMapping(consumes = MULTIPART_FORM_DATA)
    @Operation(summary = "Adding a document to given Folder", hidden = HIDE_UNSTABLE_API)
    @ResponseStatus(CREATED)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Created"),
            @ApiResponse(responseCode = "401", description = "The user is not logged, or its session has expired"),
            @ApiResponse(responseCode = "403", description = "Authenticated user doesn't have desk's role.\n(You might want to set it in Keycloak)"),
            @ApiResponse(responseCode = "404", description = "The given folderId does not exist"),
            @ApiResponse(responseCode = "507", description = "The document limit ("
                    + MAX_DOCUMENTS
                    + ") or the main document limit ("
                    + MAX_MAIN_DOCUMENTS
                    + ") "
                    +
                    "has been reached.\nConsider deleting some.")
    })
    public void addDocument(@Parameter(description = Tenant.API_DOC_ID_VALUE, required = true)
                            @PathVariable(name = Tenant.API_PATH) String tenantId,
                            @Parameter(hidden = true)
                            @TenantResolved Tenant tenant,
                            @Parameter(description = Folder.API_DOC_ID_VALUE)
                            @PathVariable(name = Folder.API_PATH) String folderId,
                            @FolderResolved(permission = CURRENT_DESK) Folder folder,
                            @Parameter(description = "Is a Main Document")
                            @RequestParam(defaultValue = "true", required = false) boolean isMainDocument,
                            @Parameter(description = "Document index (note that main documents and annexes have separate ones)")
                            @RequestParam(defaultValue = "0", required = false) int index,
                            @Parameter(description = "File")
                            @RequestPart MultipartFile file) {

        log.info("Add document document:{} size:{} name:{}", folderId, file.getSize(), file.getOriginalFilename());

        // Prepare dummy values, to ease tests

        Document currentDocument = new Document(file, isMainDocument, index);
        folder.setDocumentList(new ArrayList<>(contentService.getDocumentList(folder.getContentId())));
        folder.getDocumentList().add(currentDocument);

        typologyService.updateTypology(singletonList(folder));
        SignatureFormat signatureFormat = cryptoService.getSignatureFormat(folder.getType(), folder.getDocumentList());

        // Integrity checks

        checkSignatureFormat(signatureFormat, folder.getDocumentList(), null);

        boolean hasBeenStarted = FolderUtils.hasBeenStarted(folder);
        if (isMainDocument && hasBeenStarted) {
            throw new ResponseStatusException(CONFLICT, "message.main_documents_cannot_be_edited_in_a_started_folder");
        }

        if (folder.getDocumentList().size() >= MAX_DOCUMENTS) {
            throw new LocalizedStatusException(INSUFFICIENT_STORAGE, "message.documents_count_reached_the_limit_of_n", MAX_DOCUMENTS);
        }

        long mainDocumentSize = folder.getDocumentList().stream().filter(Objects::nonNull).filter(Document::isMainDocument).count();
        int maxMainDocuments = folder.getSubtype().getMaxMainDocuments();
        if (isMainDocument && (mainDocumentSize > maxMainDocuments)) {
            throw new LocalizedStatusException(
                    INSUFFICIENT_STORAGE,
                    (maxMainDocuments > 1) ? "message.main_documents_count_reached_the_limit_of_n" : "message.cannot_add_multiple_documents_to_this_typology",
                    maxMainDocuments
            );
        }

        // Actual save

        contentService.storeMultipartList(tenant.getId(), folder, List.of(file), isMainDocument, null, transformationService::toPdf);
    }


    @GetMapping("{documentId}")
    @Operation(summary = "Retrieve the PDF/XML/... document", hidden = HIDE_UNSTABLE_API)
    @ResponseStatus(OK)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "401", description = "The user is not logged, or its session has expired"),
            @ApiResponse(responseCode = "403", description = "Authenticated user doesn't have desk's role.\n(You might want to set it in Keycloak)"),
            @ApiResponse(responseCode = "404", description = "The given folderId/documentId does not exist")
    })
    public void getDocument(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                            @PathVariable(name = Tenant.API_PATH) String tenantId,
                            @TenantResolved Tenant tenant,
                            @Parameter(description = Folder.API_DOC_ID_VALUE)
                            @PathVariable(name = Folder.API_PATH) String folderId,
                            @FolderResolved Folder folder,
                            @Parameter(description = Document.API_DOC_ID_VALUE)
                            @PathVariable String documentId,
                            HttpServletResponse response) {
        log.info("Download folderId:{} documentId:{}", folder.getId(), documentId);
        getContent(tenant, folderId, documentId, false, response);
    }


    @PutMapping("{documentId}")
    @Operation(summary = "Replace the PDF/XML/... document", hidden = HIDE_UNSTABLE_API)
    @ResponseStatus(OK)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "401", description = "The user is not logged, or its session has expired"),
            @ApiResponse(responseCode = "403", description = "Authenticated user doesn't have desk's role.\n(You might want to set it in Keycloak)"),
            @ApiResponse(responseCode = "404", description = "The given folderId/documentId does not exist")
    })
    public void updateDocument(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                               @PathVariable(name = Tenant.API_PATH) String tenantId,
                               @TenantResolved Tenant tenant,
                               @Parameter(description = Folder.API_DOC_ID_VALUE)
                               @PathVariable(name = Folder.API_PATH) String folderId,
                               @FolderResolved Folder folder,
                               @Parameter(description = Document.API_DOC_ID_VALUE)
                               @PathVariable String documentId,
                               @Parameter(description = "File")
                               @RequestPart MultipartFile file) {

        log.info("Download folderId:{} documentId:{}", folder.getId(), documentId);

        // Integrity check

        List<Document> documentList = contentService.getDocumentList(folder.getContentId());
        folder.setDocumentList(documentList);

        Document existingDocument = folder.getDocumentList().stream()
                .filter(d -> StringUtils.equals(d.getId(), documentId))
                .findAny()
                .orElseThrow(() -> new LocalizedStatusException(NOT_FOUND, "message.unknown_document_id"));

        // Prepare dummy values, to ease tests

        Document newDocument = new Document(file, existingDocument.isMainDocument(), existingDocument.getIndex());
        folder.getDocumentList().removeIf(document -> StringUtils.equals(document.getId(), existingDocument.getId()));
        folder.getDocumentList().add(newDocument);

        typologyService.updateTypology(singletonList(folder));
        SignatureFormat signatureFormat = cryptoService.getSignatureFormat(folder.getType(), folder.getDocumentList());

        // Integrity checks

        checkSignatureFormat(signatureFormat, folder.getDocumentList(), null);

        boolean hasBeenStarted = FolderUtils.hasBeenStarted(folder);
        boolean isMainDocument = existingDocument.isMainDocument();
        if (isMainDocument && hasBeenStarted) {
            throw new ResponseStatusException(CONFLICT, "message.main_documents_cannot_be_edited_in_a_started_folder");
        }

        // Actual save

        existingDocument.getDetachedSignatures().stream()
                .map(DetachedSignature::getId)
                .filter(StringUtils::isNotEmpty)
                .forEach(contentService::deleteDocument);

        Optional.ofNullable(existingDocument.getPdfVisualId())
                .filter(StringUtils::isNotEmpty)
                .ifPresent(contentService::deleteDocument);

        contentService.storeMultipartList(tenant.getId(), folder, List.of(file), isMainDocument, singletonList(documentId), transformationService::toPdf);
    }


    @DeleteMapping("{documentId}")
    @Operation(summary = "Remove the document from the folder, and delete the copy stored", hidden = HIDE_UNSTABLE_API)
    @ResponseStatus(OK)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "401", description = "The user is not logged, or its session has expired"),
            @ApiResponse(responseCode = "403", description = "Authenticated user doesn't have desk's role.\n(You might want to set it in Keycloak)"),
            @ApiResponse(responseCode = "404", description = "The given folderId/documentId does not exist")
    })
    public void deleteDocument(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                               @PathVariable(name = Tenant.API_PATH) String tenantId,
                               @Parameter(description = Folder.API_DOC_ID_VALUE)
                               @PathVariable(name = Folder.API_PATH) String folderId,
                               @FolderResolved(permission = CURRENT_DESK) Folder folder,
                               @Parameter(description = Document.API_DOC_ID_VALUE)
                               @PathVariable String documentId) {

        log.info("Delete folderId:{} documentId:{}", folder.getId(), documentId);

        // Integrity check

        List<Document> documentList = contentService.getDocumentList(folder.getContentId());
        folder.setDocumentList(documentList);

        Document document = folder.getDocumentList().stream()
                .filter(d -> StringUtils.equals(d.getId(), documentId))
                .findAny()
                .orElseThrow(() -> new LocalizedStatusException(NOT_FOUND, "message.unknown_document_id"));

        boolean hasBeenStarted = FolderUtils.hasBeenStarted(folder);
        boolean isMainDocument = document.isMainDocument();
        if (isMainDocument && hasBeenStarted) {
            throw new ResponseStatusException(CONFLICT, "message.main_documents_cannot_be_edited_in_a_started_folder");
        }

        long mainDocCount = folder.getDocumentList().stream()
                .filter(Document::isMainDocument)
                .count();
        if (isMainDocument && mainDocCount == 1) {
            throw new ResponseStatusException(CONFLICT, "message.main_documents_cannot_be_edited_in_a_started_folder");
        }

        document.getDetachedSignatures()
                .forEach(s -> contentService.deleteDocument(s.getId()));

        contentService.deleteDocument(documentId);
    }


    // </editor-fold desc="Document CRUD">


    @GetMapping("{documentId}/pdfVisual")
    @Operation(summary = "Retrieve the PDF/XML/... document", hidden = HIDE_UNSTABLE_API)
    @ResponseStatus(OK)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "401", description = "The user is not logged, or its session has expired"),
            @ApiResponse(responseCode = "403", description = "Authenticated user doesn't have desk's role.\n(You might want to set it in Keycloak)"),
            @ApiResponse(responseCode = "404", description = "The given folderId/documentId does not exist")
    })
    public void getPdfVisual(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                             @PathVariable(name = Tenant.API_PATH) String tenantId,
                             @TenantResolved Tenant tenant,
                             @Parameter(description = Folder.API_DOC_ID_VALUE)
                             @PathVariable(name = Folder.API_PATH) String folderId,
                             @FolderResolved Folder folder,
                             @Parameter(description = Document.API_DOC_ID_VALUE)
                             @PathVariable String documentId,
                             HttpServletResponse response) {
        log.info("Download pdfVisual folderId:{} documentId:{}", folder.getId(), documentId);
        getContent(tenant, folder.getId(), documentId, true, response);
    }


    public void getContent(@NotNull Tenant tenant,
                           @NotNull String folderId,
                           @NotNull String documentId,
                           boolean pdfVisual,
                           HttpServletResponse response) {

        Folder folder = ofNullable(workflowService.getFolder(folderId, tenant.getId()))
                .orElseThrow(() -> new LocalizedStatusException(NOT_FOUND, "message.unknown_folder_id"));

        Document document = contentService.getDocumentList(folder.getContentId())
                .stream()
                .filter(Objects::nonNull)
                .filter(d -> StringUtils.equals(d.getId(), documentId))
                .findFirst()
                .orElseThrow(() -> new LocalizedStatusException(NOT_FOUND, "message.unknown_document_id"));

        log.debug("getContent id:{} pdfVisual:{}", document.getId(), document.getPdfVisualId());

        if (pdfVisual && StringUtils.isEmpty(document.getPdfVisualId())) {
            throw new LocalizedStatusException(NOT_FOUND, "message.unknown_pdf_visual_for_this_document");
        }

        // Retrieve the proper document

        DocumentBuffer documentBuffer = contentService.retrieveContent(pdfVisual ? document.getPdfVisualId() : document.getId());
        log.debug("getDocument retrieve from Alfresco buffer currentDocumentId:{} id:{} buffer:{}",
                document.getId(), document.getId(), documentBuffer);

        // Build Response

        RequestUtils.writeBufferToResponse(documentBuffer, response);
    }


    @GetMapping("{documentId}/pes-viewer")
    @Operation(summary = "Getting the root HTML page returned by the PES-viewer", hidden = HIDE_UNSTABLE_API)
    @ResponseStatus(OK)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "401", description = "The user is not logged, or its session has expired"),
            @ApiResponse(responseCode = "403", description = "Authenticated user doesn't have desk's role.\n(You might want to set it in Keycloak)"),
            @ApiResponse(responseCode = "404", description = "The given folderId/documentId does not exist")
    })
    public void getPesViewerContent(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                    @PathVariable(name = Tenant.API_PATH) String tenantId,
                                    @Parameter(description = Folder.API_DOC_ID_VALUE)
                                    @PathVariable(name = Folder.API_PATH) String folderId,
                                    @FolderResolved(permission = WORKFLOW_DESK) Folder folder,
                                    @Parameter(description = Document.API_DOC_ID_VALUE)
                                    @PathVariable String documentId,
                                    HttpServletResponse response) {

        log.info("getPesViewerContent folderId:{} documentId:{}", folder.getId(), documentId);

        List<Document> documentList = contentService.getDocumentList(folder.getContentId());
        folder.setDocumentList(documentList);

        Document document = folder.getDocumentList().stream()
                .filter(Objects::nonNull)
                .filter(d -> StringUtils.equals(d.getId(), documentId))
                .findFirst()
                .orElseThrow(() -> new LocalizedStatusException(NOT_FOUND, "message.unknown_document_id"));

        DocumentBuffer documentBuffer = contentService.retrieveContent(documentId);
        documentBuffer.setName(document.getName());
        log.debug("getPesViewerContent retrieve from Alfresco buffer id:{} buffer:{}", documentId, documentBuffer);
        pesViewerService.prepare(documentBuffer, response);
    }


    // <editor-fold desc="Detached signature CRUD">


    @PostMapping(value = "{documentId}/detachedSignature", consumes = MULTIPART_FORM_DATA)
    @Operation(summary = "Adding a detached signature for the given Document", hidden = HIDE_UNSTABLE_API)
    @ResponseStatus(CREATED)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Created"),
            @ApiResponse(responseCode = "401", description = "The user is not logged, or its session has expired"),
            @ApiResponse(responseCode = "403", description = "Authenticated user doesn't have desk's role.\n(You might want to set it in Keycloak)"),
            @ApiResponse(responseCode = "404", description = "The given folderId/deskId/documentId does not exist"),
            @ApiResponse(responseCode = "406", description = "Cannot update detached signatures of pending folders")
    })
    public void addDetachedSignature(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                     @PathVariable(name = Tenant.API_PATH) String tenantId,
                                     @Parameter(hidden = true)
                                     @TenantResolved Tenant tenant,
                                     @Parameter(description = Folder.API_DOC_ID_VALUE)
                                     @PathVariable(name = Folder.API_PATH) String folderId,
                                     @FolderResolved(permission = CURRENT_DESK) Folder folder,
                                     @Parameter(description = Document.API_DOC_ID_VALUE)
                                     @PathVariable String documentId,
                                     @Parameter(description = DeskRepresentation.API_DOC_ID_VALUE)
                                     @RequestPart String deskId,
                                     @CurrentUserResolved User currentUser,
                                     @Parameter(description = "File")
                                     @RequestPart MultipartFile file) {

        log.info("setDetachedSignature folderId:{} documentId:{}", folder.getId(), documentId);

        // Integrity check

        Optional.ofNullable(authService.findDeskByIdNoException(tenant.getId(), deskId))
                .orElseThrow(() -> new LocalizedStatusException(NOT_FOUND, "message.unknown_desk_id"));

        if (FolderUtils.hasBeenStarted(folder)) {
            throw new LocalizedStatusException(NOT_ACCEPTABLE, "message.cannot_update_detached_signatures_of_a_pending_folder");
        }
        typologyService.updateTypology(singletonList(folder));

        if (folder.getType() == null) {
            throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.cannot_add_detached_signatures_on_a_folder_with_a_deleted_type");
        }

        folder.setDocumentList(contentService.getDocumentList(folder.getContentId()));

        if (!folder.getType().getSignatureFormat().equals(AUTO)) {
            SignatureFormat signatureFormat = cryptoService.getSignatureFormat(folder.getType(), folder.getDocumentList());
            checkIfDetachedSignatureAllowed(signatureFormat, true);
        }

        Document targetDocument = folder.getDocumentList().stream()
                .filter(d -> StringUtils.equals(d.getId(), documentId))
                .findAny()
                .orElseThrow(() -> new LocalizedStatusException(NOT_FOUND, "message.unknown_document_id"));

        int taskIndex = IntStream.range(0, folder.getStepList().size())
                .filter(i -> FolderUtils.isActive(folder.getStepList().get(i).getState()))
                .findFirst()
                .orElseThrow(() -> new LocalizedStatusException(NOT_FOUND, "message.unknown_task_id"));

        Task currentTask = folder.getStepList().get(taskIndex);
        currentTask.setStepIndex((long) taskIndex);

        long existingSignaturesCount = targetDocument.getDetachedSignatures().size();
        if (existingSignaturesCount >= MAX_DETACHED_SIGNATURES_PER_DOCUMENT_ON_STARTUP) {
            throw new LocalizedStatusException(
                    INSUFFICIENT_STORAGE,
                    "message.already_n_detached_signatures_for_a_limit_of_m",
                    existingSignaturesCount, MAX_DETACHED_SIGNATURES_PER_DOCUMENT_ON_STARTUP
            );
        }

        currentTask.setUser(currentUser);

        // Registering things

        DocumentBuffer documentBuffer = new DocumentBuffer(file, false, -1);

        try (InputStream body = file.getInputStream()) {
            documentBuffer.setContentFlux(readInputStream(() -> body, new DefaultDataBufferFactory(), FILE_TRANSFER_BUFFER_SIZE));
            String detachedSignatureDocumentId = contentService.createDetachedSignature(folder.getContentId(), targetDocument, documentBuffer, currentTask);
            documentBuffer.setId(detachedSignatureDocumentId);
        } catch (IOException e) {
            throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, e, "message.error_reading_file");
        }
    }


    @GetMapping("{documentId}/detachedSignature/{detachedSignatureId}")
    @Operation(summary = "Getting the detachedSignature of the given Document", hidden = HIDE_UNSTABLE_API)
    @ResponseStatus(OK)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "401", description = "The user is not logged, or its session has expired"),
            @ApiResponse(responseCode = "403", description = "Authenticated user doesn't have desk's role.\n(You might want to set it in Keycloak)"),
            @ApiResponse(responseCode = "404", description = "The given folderId/documentId/detachedSignatureId does not exist")
    })
    public void getDetachedSignature(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                     @PathVariable(name = Tenant.API_PATH) String tenantId,
                                     @Parameter(description = Folder.API_DOC_ID_VALUE)
                                     @PathVariable(name = Folder.API_PATH) String folderId,
                                     @FolderResolved(permission = WORKFLOW_DESK) Folder folder,
                                     @Parameter(description = Document.API_DOC_ID_VALUE)
                                     @PathVariable String documentId,
                                     @Parameter(description = "Detached signature Id")
                                     @PathVariable String detachedSignatureId,
                                     HttpServletResponse response) {

        log.info("getDetachedSignature folderId:{} documentId:{} detachedSignatureId:{}", folderId, documentId, detachedSignatureId);

        // Integrity check

        folder.setDocumentList(contentService.getDocumentList(folder.getContentId()));

        Document document = folder.getDocumentList()
                .stream()
                .filter(d -> StringUtils.equals(documentId, d.getId()))
                .findAny()
                .orElseThrow(() -> new LocalizedStatusException(NOT_FOUND, "message.unknown_document_id"));

        DetachedSignature detachedSignature = document.getDetachedSignatures()
                .stream()
                .filter(s -> StringUtils.equals(detachedSignatureId, s.getId()))
                .findAny()
                .orElseThrow(() -> new LocalizedStatusException(NOT_FOUND, "message.unknown_detached_signature_id"));

        // Actual retrieve

        DocumentBuffer documentBuffer = contentService.retrieveContent(detachedSignature.getId());
        RequestUtils.writeBufferToResponse(documentBuffer, response);
    }


    @DeleteMapping("{documentId}/detachedSignature/{detachedSignatureId}")
    @Operation(summary = "Delete the detachedSignature of the given Document", hidden = HIDE_UNSTABLE_API)
    @ResponseStatus(NO_CONTENT)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Deleted"),
            @ApiResponse(responseCode = "401", description = "The user is not logged, or its session has expired"),
            @ApiResponse(responseCode = "403", description = "Authenticated user doesn't have desk's role.\n(You might want to set it in Keycloak)"),
            @ApiResponse(responseCode = "404", description = "The given folderId/documentId/detachedSignatureId does not exist"),
            @ApiResponse(responseCode = "406", description = "Cannot update detached signatures of pending folders")
    })
    public void deleteDetachedSignature(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                        @PathVariable(name = Tenant.API_PATH) String tenantId,
                                        @Parameter(description = Folder.API_DOC_ID_VALUE)
                                        @PathVariable(name = Folder.API_PATH) String folderId,
                                        @FolderResolved(permission = CURRENT_DESK) Folder folder,
                                        @Parameter(description = Document.API_DOC_ID_VALUE)
                                        @PathVariable String documentId,
                                        @Parameter(description = "Detached signature Id")
                                        @PathVariable String detachedSignatureId) {

        log.info("deleteDetachedSignature folderId:{} documentId:{} detachedSignatureId:{}", folderId, documentId, detachedSignatureId);

        // Integrity check

        folder.getStepList().stream()
                .filter(t -> t.getAction() == START)
                .filter(t -> t.getDate() == null)
                .findFirst()
                .orElseThrow(() -> new LocalizedStatusException(NOT_ACCEPTABLE, "message.cannot_update_detached_signatures_of_a_pending_folder"));

        folder.setDocumentList(contentService.getDocumentList(folder.getContentId()));

        Document document = folder.getDocumentList()
                .stream()
                .filter(d -> StringUtils.equals(documentId, d.getId()))
                .findAny()
                .orElseThrow(() -> new LocalizedStatusException(NOT_FOUND, "message.unknown_document_id"));

        DetachedSignature detachedSignature = document.getDetachedSignatures()
                .stream()
                .filter(s -> StringUtils.equals(detachedSignatureId, s.getId()))
                .findAny()
                .orElseThrow(() -> new LocalizedStatusException(NOT_FOUND, "message.unknown_detached_signature_id"));

        // Actual delete

        contentService.deleteDocument(detachedSignature.getId());
    }


    // </editor-fold desc="Detached signature CRUD">


    @PostMapping("{documentId}/annotations")
    @Operation(summary = "Creates a PDF annotation", hidden = HIDE_UNSTABLE_API)
    @ResponseStatus(CREATED)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Created"),
            @ApiResponse(responseCode = "401", description = "The user is not logged, or its session has expired"),
            @ApiResponse(responseCode = "403", description = "Authenticated user doesn't have desk's role.\n(You might want to set it in Keycloak)"),
            @ApiResponse(responseCode = "404", description = "The given folderId/documentId does not exist")
    })
    public void createAnnotation(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                 @PathVariable(name = Tenant.API_PATH) String tenantId,
                                 @Parameter(description = Folder.API_DOC_ID_VALUE)
                                 @PathVariable(name = Folder.API_PATH) String folderId,
                                 @FolderResolved(permission = CURRENT_DESK) Folder folder,
                                 @Parameter(description = Document.API_DOC_ID_VALUE)
                                 @PathVariable String documentId,
                                 @Parameter(description = "body")
                                 @RequestBody StickyNote stickyNote) {

        // Integrity check

        folder.setDocumentList(contentService.getDocumentList(folder.getContentId()));

        log.debug("docList : {}", folder.getDocumentList());
        log.debug("docId   : {}", documentId);
        log.debug("foldId  : {}", folderId);

        Document document = folder.getDocumentList()
                .stream()
                .filter(d -> StringUtils.equals(documentId, d.getId()))
                .findAny()
                .orElseThrow(() -> new LocalizedStatusException(NOT_FOUND, "message.unknown_document_id"));

        // Actual stamp

        DocumentBuffer documentBuffer = contentService.retrieveContent(document.getId());
        DocumentBuffer documentAnnotatedBuffer = pdfStampService.createAnnotation(documentBuffer, stickyNote);

        log.debug("createAnnotation pdfSize:{} patchedPdfSize:{}", documentBuffer.getContentLength(), documentAnnotatedBuffer.getContentLength());
        contentService.updateDocument(documentId, documentAnnotatedBuffer);
    }


    @GetMapping("{documentId}/signaturePlacement")
    @Operation(summary = "Get all signaturePlacement Annotations of a document", hidden = HIDE_UNSTABLE_API)
    @ResponseStatus(CREATED)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Created"),
            @ApiResponse(responseCode = "401", description = "The user is not logged, or its session has expired"),
            @ApiResponse(responseCode = "403", description = "Authenticated user doesn't have desk's role.\n(You might want to set it in Keycloak)"),
            @ApiResponse(responseCode = "404", description = "The given folderId/documentId does not exist")
    })
    public List<SignaturePlacement> getSignaturePlacementAnnotations(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                                                     @PathVariable(name = Tenant.API_PATH) String tenantId,
                                                                     @Parameter(description = Folder.API_DOC_ID_VALUE)
                                                                     @PathVariable(name = Folder.API_PATH) String folderId,
                                                                     @FolderResolved(permission = WORKFLOW_DESK) Folder folder,
                                                                     @Parameter(description = Document.API_DOC_ID_VALUE)
                                                                     @PathVariable String documentId) {

        folder.setDocumentList(contentService.getDocumentList(folder.getContentId()));

        log.debug("docList : {}", folder.getDocumentList());
        log.debug("docId   : {}", documentId);
        log.debug("foldId  : {}", folderId);

        Document document = folder.getDocumentList()
                .stream()
                .filter(d -> StringUtils.equals(documentId, d.getId()))
                .findAny()
                .orElseThrow(() -> new LocalizedStatusException(NOT_FOUND, "message.unknown_document_id"));

        return document.getSignaturePlacementAnnotations();
    }


    @PostMapping("{documentId}/signaturePlacement")
    @Operation(summary = "Creates a signature placement annotation", hidden = HIDE_UNSTABLE_API)
    @ResponseStatus(CREATED)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Created"),
            @ApiResponse(responseCode = "401", description = "The user is not logged, or its session has expired"),
            @ApiResponse(responseCode = "403", description = "Authenticated user doesn't have desk's role.\n(You might want to set it in Keycloak)"),
            @ApiResponse(responseCode = "404", description = "The given folderId/documentId does not exist")
    })
    public void createSignaturePlacementAnnotation(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                                   @PathVariable(name = Tenant.API_PATH) String tenantId,
                                                   @Parameter(description = Folder.API_DOC_ID_VALUE)
                                                   @PathVariable(name = Folder.API_PATH) String folderId,
                                                   @FolderResolved(permission = CURRENT_DESK) Folder folder,
                                                   @Parameter(description = Document.API_DOC_ID_VALUE)
                                                   @PathVariable String documentId,
                                                   @Parameter(description = "body")
                                                   @RequestBody SignaturePlacement signaturePlacement) {

        folder.setDocumentList(contentService.getDocumentList(folder.getContentId()));

        log.debug("docList : {}", folder.getDocumentList());
        log.debug("docId   : {}", documentId);
        log.debug("foldId  : {}", folderId);

        Document document = folder.getDocumentList()
                .stream()
                .filter(d -> StringUtils.equals(documentId, d.getId()))
                .findAny()
                .orElseThrow(() -> new LocalizedStatusException(NOT_FOUND, "message.unknown_document_id"));

        /* FIXME multiple signature placement annotation
         *  if (Objects.isNull(document.getSignaturePlacementAnnotations())) {
         *      document.setSignaturePlacementAnnotations(new ArrayList<>());
         *  }
         */

        if (StringUtils.isEmpty(signaturePlacement.getId())) {

            /* FIXME multiple signature placement annotation
             *  document.getSignaturePlacementAnnotations().removeIf(s -> s.getSignatureNumber() == signaturePlacement.getSignatureNumber());
             */
            document.setSignaturePlacementAnnotations(new ArrayList<>());
            signaturePlacement.setId(UUID.randomUUID().toString());
            document.getSignaturePlacementAnnotations().add(signaturePlacement);
        } else {
            document.getSignaturePlacementAnnotations().forEach(s -> {
                if (s.getId().equals(signaturePlacement.getId())) {
                    s.setSignatureNumber(signaturePlacement.getSignatureNumber());
                }
            });
        }
        contentService.updateDocumentAlfrescoProperties(document);
    }


    @DeleteMapping("{documentId}/annotations/{annotationId}")
    @Operation(summary = "Deletes the given PDF annotation", hidden = HIDE_UNSTABLE_API)
    @ResponseStatus(NO_CONTENT)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Deleted"),
            @ApiResponse(responseCode = "401", description = "The user is not logged, or its session has expired"),
            @ApiResponse(responseCode = "403", description = "Authenticated user doesn't have desk's role.\n(You might want to set it in Keycloak)"),
            @ApiResponse(responseCode = "404", description = "The given folderId/documentId/AnnotationId does not exist")
    })
    public void deleteAnnotation(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                 @PathVariable(name = Tenant.API_PATH) String tenantId,
                                 @Parameter(description = Folder.API_DOC_ID_VALUE)
                                 @PathVariable(name = Folder.API_PATH) String folderId,
                                 @FolderResolved(permission = CURRENT_DESK) Folder folder,
                                 @Parameter(description = Document.API_DOC_ID_VALUE)
                                 @PathVariable String documentId,
                                 @Parameter(description = "Annotation Id")
                                 @PathVariable String annotationId) {

        // Integrity check

        folder.setDocumentList(contentService.getDocumentList(folder.getContentId()));

        Document document = folder.getDocumentList()
                .stream()
                .filter(d -> StringUtils.equals(documentId, d.getId()))
                .findFirst()
                .orElseThrow(() -> new LocalizedStatusException(NOT_FOUND, "message.unknown_document_id"));

        // Actual stamp

        DocumentBuffer documentBuffer = contentService.retrieveContent(document.getId());
        boolean signaturePlacementRemoved = document.getSignaturePlacementAnnotations()
                .removeIf(signaturePlacement -> signaturePlacement.getId().equals(annotationId));
        if (signaturePlacementRemoved) {
            contentService.updateDocumentAlfrescoProperties(document);
        } else {
            DocumentBuffer documentPatchedBuffer = pdfStampService.deleteComment(documentBuffer, annotationId);
            log.debug("deleteAnnotation pdfSize:{} patchedPdfSize:{}", documentBuffer.getContentLength(), documentPatchedBuffer.getContentLength());
            contentService.updateDocument(documentId, documentPatchedBuffer);
        }
    }


}
