/*
 * iparapheur Core
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.ipcore.controller;

import coop.libriciel.crypto.model.DataToSign;
import coop.libriciel.crypto.model.DataToSignHolder;
import coop.libriciel.ipcore.business.workflow.WorkflowBusinessService;
import coop.libriciel.ipcore.model.auth.Desk;
import coop.libriciel.ipcore.model.auth.DeskRepresentation;
import coop.libriciel.ipcore.model.auth.User;
import coop.libriciel.ipcore.model.content.CreateFolderRequest;
import coop.libriciel.ipcore.model.content.Document;
import coop.libriciel.ipcore.model.content.DocumentBuffer;
import coop.libriciel.ipcore.model.crypto.DocumentDataToSignHolder;
import coop.libriciel.ipcore.model.crypto.PdfSignaturePosition;
import coop.libriciel.ipcore.model.crypto.SealCertificate;
import coop.libriciel.ipcore.model.crypto.SignatureFormat;
import coop.libriciel.ipcore.model.database.ExternalSignatureConfig;
import coop.libriciel.ipcore.model.database.Subtype;
import coop.libriciel.ipcore.model.database.Tenant;
import coop.libriciel.ipcore.model.database.Type;
import coop.libriciel.ipcore.model.database.userPreferences.FolderFilter;
import coop.libriciel.ipcore.model.database.userPreferences.FolderFilterDto;
import coop.libriciel.ipcore.model.externalsignature.ExternalSignatureParams;
import coop.libriciel.ipcore.model.externalsignature.Status;
import coop.libriciel.ipcore.model.ipng.*;
import coop.libriciel.ipcore.model.securemail.MailParams;
import coop.libriciel.ipcore.model.securemail.SecureMailDocument;
import coop.libriciel.ipcore.model.workflow.*;
import coop.libriciel.ipcore.model.workflow.requests.SignatureTaskParams;
import coop.libriciel.ipcore.model.workflow.requests.SimpleTaskParams;
import coop.libriciel.ipcore.model.workflow.requests.StartWorkflowResponse;
import coop.libriciel.ipcore.model.workflow.requests.TransferTaskParams;
import coop.libriciel.ipcore.services.auth.AuthServiceInterface;
import coop.libriciel.ipcore.services.content.ContentServiceInterface;
import coop.libriciel.ipcore.services.crypto.CryptoServiceInterface;
import coop.libriciel.ipcore.services.database.DatabaseServiceInterface;
import coop.libriciel.ipcore.services.database.TypeRepository;
import coop.libriciel.ipcore.services.database.TypologyService;
import coop.libriciel.ipcore.services.externalsignature.ExternalSignatureInterface;
import coop.libriciel.ipcore.services.filetransfer.FileTransferServiceInterface;
import coop.libriciel.ipcore.services.groovy.GroovyResultCatcher;
import coop.libriciel.ipcore.services.groovy.GroovyService;
import coop.libriciel.ipcore.services.ipng.IpngBindingsInterface;
import coop.libriciel.ipcore.services.ipng.IpngServiceInterface;
import coop.libriciel.ipcore.services.permission.PermissionServiceInterface;
import coop.libriciel.ipcore.services.resolvers.CurrentUserResolver.CurrentUserResolved;
import coop.libriciel.ipcore.services.resolvers.DeskResolver.DeskResolved;
import coop.libriciel.ipcore.services.resolvers.ExternalSignatureConfigResolver.ExternalSignatureConfigResolved;
import coop.libriciel.ipcore.services.resolvers.FolderResolver.FolderResolved;
import coop.libriciel.ipcore.services.resolvers.TaskResolver.TaskResolved;
import coop.libriciel.ipcore.services.resolvers.TenantResolver.TenantResolved;
import coop.libriciel.ipcore.services.secret.SecretServiceInterface;
import coop.libriciel.ipcore.services.securemail.SecureMailServiceInterface;
import coop.libriciel.ipcore.services.stats.StatsServiceInterface;
import coop.libriciel.ipcore.services.transformation.TransformationServiceInterface;
import coop.libriciel.ipcore.services.workflow.WorkflowServiceInterface;
import coop.libriciel.ipcore.utils.*;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.buffer.DefaultDataBufferFactory;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.util.*;
import java.util.stream.IntStream;

import static coop.libriciel.ipcore.IpCoreApplication.API_V1;
import static coop.libriciel.ipcore.IpCoreApplication.HIDE_UNSTABLE_API;
import static coop.libriciel.ipcore.controller.DocumentController.MAX_DOCUMENTS;
import static coop.libriciel.ipcore.model.crypto.SignatureFormat.*;
import static coop.libriciel.ipcore.model.workflow.Action.*;
import static coop.libriciel.ipcore.model.workflow.FolderSortBy.CREATION_DATE;
import static coop.libriciel.ipcore.model.workflow.State.*;
import static coop.libriciel.ipcore.services.content.ContentServiceInterface.FILE_TRANSFER_BUFFER_SIZE;
import static coop.libriciel.ipcore.services.resolvers.FolderResolver.FolderResolved.Permission.CURRENT_DESK;
import static coop.libriciel.ipcore.services.resolvers.FolderResolver.FolderResolved.Permission.WORKFLOW_DESK;
import static coop.libriciel.ipcore.services.workflow.IpWorkflowService.*;
import static coop.libriciel.ipcore.utils.ApiUtils.*;
import static coop.libriciel.ipcore.utils.CryptoUtils.SIGNATURE_PLACEHOLDER_CUSTOM_SIGNATURE_FIELD;
import static coop.libriciel.ipcore.utils.CryptoUtils.sha256Sign;
import static coop.libriciel.ipcore.utils.PaginatedList.*;
import static coop.libriciel.ipcore.utils.TextUtils.firstNotEmpty;
import static coop.libriciel.ipcore.utils.UserUtils.AUTOMATIC_TASK_USER_ID;
import static java.util.Collections.*;
import static java.util.Optional.ofNullable;
import static java.util.UUID.randomUUID;
import static java.util.stream.Collectors.*;
import static org.apache.commons.collections4.MapUtils.isNotEmpty;
import static org.apache.commons.io.FilenameUtils.removeExtension;
import static org.apache.commons.lang3.StringUtils.length;
import static org.apache.tomcat.util.http.fileupload.FileUploadBase.MULTIPART_FORM_DATA;
import static org.springframework.core.io.buffer.DataBufferUtils.readInputStream;
import static org.springframework.http.HttpStatus.*;
import static org.springframework.http.MediaType.*;


@Log4j2
@RestController
@RequestMapping(API_V1 + "/tenant/{tenantId}/")
@Tag(name = "workflow", description = "Workflow access for a regular logged user")
public class WorkflowController {


    // <editor-fold desc="Beans">


    private final AuthServiceInterface authService;
    private final ContentServiceInterface contentService;
    private final CryptoServiceInterface cryptoService;
    private final DatabaseServiceInterface dbService;
    private final ExternalSignatureInterface externalSignatureService;
    private final FileTransferServiceInterface fileTransferService;
    private final GroovyService groovyService;
    private final IpngBindingsInterface ipngBindings;
    private final IpngServiceInterface ipngService;
    private final ModelMapper modelMapper;
    private final PermissionServiceInterface permissionService;
    private final SecretServiceInterface secretService;
    private final SecureMailServiceInterface secureMailService;
    private final StatsServiceInterface statsService;
    private final TransformationServiceInterface transformationService;
    private final TypeRepository typeRepository;
    private final TypologyService typologyService;
    private final WorkflowBusinessService workflowBusinessService;
    private final WorkflowServiceInterface workflowService;


    @Autowired
    public WorkflowController(AuthServiceInterface authService,
                              ContentServiceInterface contentService,
                              CryptoServiceInterface cryptoService,
                              DatabaseServiceInterface dbService,
                              ExternalSignatureInterface externalSignatureService,
                              FileTransferServiceInterface fileTransferService,
                              GroovyService groovyService,
                              IpngBindingsInterface ipngBindings,
                              IpngServiceInterface ipngService,
                              ModelMapper modelMapper,
                              PermissionServiceInterface permissionService,
                              SecretServiceInterface secretService,
                              TransformationServiceInterface transformationService,
                              TypeRepository typeRepository,
                              TypologyService typologyService,
                              SecureMailServiceInterface secureMailService,
                              StatsServiceInterface statsService,
                              WorkflowBusinessService workflowBusinessService,
                              WorkflowServiceInterface workflowService) {
        this.authService = authService;
        this.contentService = contentService;
        this.cryptoService = cryptoService;
        this.dbService = dbService;
        this.externalSignatureService = externalSignatureService;
        this.fileTransferService = fileTransferService;
        this.groovyService = groovyService;
        this.ipngBindings = ipngBindings;
        this.ipngService = ipngService;
        this.modelMapper = modelMapper;
        this.permissionService = permissionService;
        this.secretService = secretService;
        this.secureMailService = secureMailService;
        this.statsService = statsService;
        this.transformationService = transformationService;
        this.typologyService = typologyService;
        this.typeRepository = typeRepository;
        this.workflowBusinessService = workflowBusinessService;
        this.workflowService = workflowService;
    }


    // </editor-fold desc="Beans">


    // <editor-fold desc="Requests">


    @PostMapping(value = "desk/{deskId}/evaluate-workflow-selection-script")
    // @PreAuthorize("hasRole('" + DESK_ROLE_PREFIX + "' + #deskId)")
    @Operation(summary = "Returns the evaluated workflow definition, using current parent desks, and given metadata on a possible selection script", hidden = HIDE_UNSTABLE_API)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_400, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public WorkflowDefinitionDto evaluateWorkflowSelectionScript(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                                                 @PathVariable(name = Tenant.API_PATH) String tenantId,
                                                                 @TenantResolved Tenant tenant,
                                                                 @Parameter(description = DeskRepresentation.API_DOC_ID_VALUE)
                                                                 @PathVariable(name = DeskRepresentation.API_PATH) String deskId,
                                                                 @DeskResolved Desk desk,
                                                                 @Parameter(description = "Params")
                                                                 @RequestBody CreateFolderRequest draftFolderParams) {

        log.debug("evaluateWorkflowSelectionScript deskId:{} name:{} typeId:{} subtypeId:{}",
                desk.getId(), draftFolderParams.getName(), draftFolderParams.getTypeId(), draftFolderParams.getSubtypeId());

        Folder mockFolder = new Folder();
        mockFolder.setOriginDesk(new DeskRepresentation(desk.getId(), desk.getName()));
        mockFolder.setMetadata(new HashMap<>());
        mockFolder.setType(ofNullable(draftFolderParams.getTypeId()).map(i -> Type.builder().id(i).build()).orElse(null));
        mockFolder.setSubtype(ofNullable(draftFolderParams.getSubtypeId()).map(i -> Subtype.builder().id(i).build()).orElse(null));

        typologyService.updateTypology(singletonList(mockFolder));
        workflowBusinessService.checkAndCopyMetadataFromParams(draftFolderParams, mockFolder);

        Optional<GroovyResultCatcher> scriptResultWrapped = groovyService.computeSelectionScriptForFolder(tenantId, mockFolder);

        // FIXME : return result on regular non-scripted subtype too
        return workflowBusinessService.computeWorkflowDefinitionFromScriptResult(scriptResultWrapped, tenantId, desk.getId());
    }


    @PostMapping(value = "desk/{deskId}/draft", consumes = MULTIPART_FORM_DATA)
    // @PreAuthorize("hasRole('" + DESK_ROLE_PREFIX + "' + #deskId)")
    @Operation(summary = "Create a draft folder")
    @ResponseStatus(CREATED)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_201),
            @ApiResponse(responseCode = CODE_400, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public FolderRepresentation createDraftFolder(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                                  @PathVariable(name = Tenant.API_PATH) String tenantId,
                                                  @TenantResolved Tenant tenant,
                                                  @Parameter(description = DeskRepresentation.API_DOC_ID_VALUE)
                                                  @PathVariable(name = DeskRepresentation.API_PATH) String deskId,
                                                  @DeskResolved Desk desk,
                                                  @Parameter(description = "Main files")
                                                  @RequestPart List<MultipartFile> mainFiles,
                                                  @Parameter(description = "Annexe files")
                                                  @RequestPart(required = false) List<MultipartFile> annexeFiles,
                                                  @Parameter(description = "Detached signature files")
                                                  @RequestPart(required = false) List<MultipartFile> detachedSignatures,
                                                  @Parameter(description = "Parameters")
                                                  @RequestPart @Valid CreateFolderRequest createFolderRequest) {

        log.debug("createDraftFolder deskId:{} name:{} typeId:{} subtypeId:{}",
                desk.getId(), createFolderRequest.getName(), createFolderRequest.getTypeId(), createFolderRequest.getSubtypeId());

        if (CollectionUtils.isEmpty(mainFiles)) {
            throw new LocalizedStatusException(NOT_FOUND, "message.cannot_create_folder_without_main_document");
        }

        String folderName = firstNotEmpty(createFolderRequest.getName(), removeExtension(mainFiles.get(0).getOriginalFilename()));

        // We must test again the validity here despite parameter validation, the main file name could have been used
        if (TextUtils.containsForbiddenChar(folderName)) {
            throw new LocalizedStatusException(NOT_ACCEPTABLE, "message.name_contains_invalid_char");
        }

        Map<Integer, String> variableDesksIdsMap = new HashMap<>();
        if (Objects.nonNull(createFolderRequest.getVariableDesksIds())) {
            createFolderRequest.getVariableDesksIds().forEach((key, value) -> variableDesksIdsMap.put(Integer.parseInt(key), value));
        }

        Folder result = workflowBusinessService.prepareDraftFromParams(tenant, desk, createFolderRequest, folderName, variableDesksIdsMap);

        if ((CollectionUtils.size(mainFiles) + CollectionUtils.size(annexeFiles)) >= MAX_DOCUMENTS) {
            throw new LocalizedStatusException(INSUFFICIENT_STORAGE, "message.documents_count_reached_the_limit_of_n", MAX_DOCUMENTS);
        }

        int maxMainDocuments = ofNullable(result.getSubtype())
                .map(Subtype::getMaxMainDocuments)
                .orElse(1);

        if (CollectionUtils.size(mainFiles) > maxMainDocuments) {
            throw new LocalizedStatusException(INSUFFICIENT_STORAGE, "message.main_documents_count_reached_the_limit_of_n", maxMainDocuments);
        }

        if (result.getType() != null) {

            List<Document> dummyMainFiles = IntStream
                    .range(0, CollectionUtils.size(mainFiles))
                    .mapToObj(i -> new Document(mainFiles.get(i), true, i))
                    .toList();

            List<Document> dummyAnnexes = ofNullable(annexeFiles)
                    .filter(CollectionUtils::isNotEmpty)
                    .map(a -> IntStream.range(0, CollectionUtils.size(a))
                            .mapToObj(i -> new Document(annexeFiles.get(i), false, i))
                            .toList()
                    )
                    .orElse(emptyList());

            result.setDocumentList(new ArrayList<>());
            result.getDocumentList().addAll(dummyMainFiles);
            result.getDocumentList().addAll(dummyAnnexes);

            if (!result.getType().getSignatureFormat().equals(AUTO)) {
                CryptoUtils.checkSignatureFormat(result.getType().getSignatureFormat(), result.getDocumentList(), detachedSignatures);
            }
        }

        // Creating Folder

        String contentId = contentService.createFolder(tenant);
        result.setContentId(contentId);

        contentService.storeMultipartList(tenantId, result, mainFiles, true, null, transformationService::toPdf);
        contentService.storeMultipartList(tenantId, result, annexeFiles, false, null, transformationService::toPdf);

        if (isNotEmpty(createFolderRequest.getDetachedSignaturesMapping())) {

            // TODO user createDocument to get documents ids.
            List<Document> documents = contentService.getDocumentList(contentId);

            createFolderRequest
                    .getDetachedSignaturesMapping()
                    .forEach((documentName, detachedSignatureNames) -> {
                        Document document = documents.stream().filter(d -> d.getName().equals(documentName)).findFirst()
                                .orElseThrow(() -> new LocalizedStatusException(BAD_REQUEST, "message.document_mapping_error"));

                        List<MultipartFile> detachedSignaturesToCreate = getDetachedSignatureListForDocument(detachedSignatureNames, detachedSignatures);

                        detachedSignaturesToCreate.forEach(detachedSignatureToCreate -> createDetachedSignatureForDocument(
                                contentId,
                                document,
                                detachedSignatureToCreate));
                    });

        }

        // Upload to Flowable

        log.info("type:{} subtype:{} creationW:{} validationW:{}", result.getType(), result.getSubtype(),
                result.getCreationWorkflowDefinitionKey(), result.getValidationWorkflowDefinitionKey());


        String createdFolderId = workflowService
                .createDraftWorkflow(tenant.getId(), result, variableDesksIdsMap)
                .getId();

        result.setId(createdFolderId);

        // Sending back result

        log.debug("CreateFolder folder:{}", result);
        statsService.registerFolderAction(tenant, CREATE, result, desk, null);

        return modelMapper.map(result, FolderRepresentation.class);
    }


    @PutMapping("desk/{deskId}/draft/{folderId}")
    @Operation(summary = "Start", description = "Start the given Folder")
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_201),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_407, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public StartWorkflowResponse startWorkflow(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                               @PathVariable(name = Tenant.API_PATH) String tenantId,
                                               @TenantResolved Tenant tenant,
                                               @Parameter(description = DeskRepresentation.API_DOC_ID_VALUE)
                                               @PathVariable String deskId,
                                               @Parameter(description = Folder.API_DOC_ID_VALUE)
                                               @PathVariable(name = Folder.API_PATH) String folderId,
                                               @FolderResolved(permission = WORKFLOW_DESK) Folder folder,
                                               @RequestBody @Valid SimpleTaskParams simpleTaskParams) {

        log.info("startWorkflow deskId:{} folderId:{}", deskId, folderId);

        // Integrity check

        if (folder.getStepList().stream()
                .filter(s -> s.getAction() == START)
                .anyMatch(s -> s.getState() != CURRENT)) {
            throw new LocalizedStatusException(CONFLICT, "message.this_workflow_has_already_been_started");
        }

        if (!StringUtils.equals(deskId, folder.getOriginDesk().getId())) {
            throw new LocalizedStatusException(BAD_REQUEST, "message.you_can_t_start_this_folder_from_this_desk");
        }

        StartWorkflowResponse startWorkflowResponse = workflowBusinessService.doStartWorkflow(tenantId, folder, deskId, simpleTaskParams);
        this.disableDeleteCapabilityOnCurrentDocuments(folder);
        return startWorkflowResponse;
    }


    @PutMapping("desk/{deskId}/folder/{folderId}/task/{taskId}/visa")
    @Operation(summary = "Visa", description = "Perform a straightforward Visa on a Task")
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_400, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public void visa(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                     @PathVariable(name = Tenant.API_PATH) String tenantId,
                     @TenantResolved Tenant tenant,
                     @Parameter(description = DeskRepresentation.API_DOC_ID_VALUE)
                     @PathVariable(name = DeskRepresentation.API_PATH) String deskId,
                     @DeskResolved Desk desk,
                     @Parameter(description = Folder.API_DOC_ID_VALUE)
                     @PathVariable(name = Folder.API_PATH) String folderId,
                     @FolderResolved Folder folder,
                     @Parameter(description = Task.API_DOC_ID_VALUE)
                     @PathVariable(name = Task.API_PATH) String taskId,
                     @TaskResolved Task task,
                     @Parameter(name = SimpleTaskParams.API_NAME, description = SimpleTaskParams.API_VALUE, required = true)
                     @RequestBody(required = false) SimpleTaskParams performTaskRequest) {
        performSimpleAction(tenant, desk, folder, task, VISA, performTaskRequest, null);
    }


    @PutMapping("desk/{deskId}/folder/{folderId}/task/{taskId}/second_opinion")
    @Operation(summary = "Second opinion", description = "Perform a straightforward Second opinion on a Task")
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_400, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public void secondOpinion(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                              @PathVariable(name = Tenant.API_PATH) String tenantId,
                              @TenantResolved Tenant tenant,
                              @Parameter(description = DeskRepresentation.API_DOC_ID_VALUE)
                              @PathVariable(name = DeskRepresentation.API_PATH) String deskId,
                              @DeskResolved Desk desk,
                              @Parameter(description = Folder.API_DOC_ID_VALUE)
                              @PathVariable(name = Folder.API_PATH) String folderId,
                              @FolderResolved Folder folder,
                              @Parameter(description = Task.API_DOC_ID_VALUE)
                              @PathVariable(name = Task.API_PATH) String taskId,
                              @TaskResolved Task task,
                              @Parameter(name = SimpleTaskParams.API_NAME, description = SimpleTaskParams.API_VALUE, required = true)
                              @RequestBody(required = false) SimpleTaskParams performTaskRequest) {
        performSimpleAction(tenant, desk, folder, task, SECOND_OPINION, performTaskRequest, null);
    }


    @PutMapping("desk/{deskId}/folder/{folderId}/task/{taskId}/external_signature")
    @Operation(summary = "External signature", description = "Perform an external signature")
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_400, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public void requestExternalSignatureProcedure(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                                  @PathVariable(name = Tenant.API_PATH) String tenantId,
                                                  @TenantResolved Tenant tenant,
                                                  @Parameter(description = DeskRepresentation.API_DOC_ID_VALUE)
                                                  @PathVariable(name = DeskRepresentation.API_PATH) String deskId,
                                                  @DeskResolved Desk desk,
                                                  @Parameter(description = Folder.API_DOC_ID_VALUE)
                                                  @PathVariable(name = Folder.API_PATH) String folderId,
                                                  @FolderResolved(permission = CURRENT_DESK) Folder folder,
                                                  @Parameter(description = Task.API_DOC_ID_VALUE)
                                                  @PathVariable(name = Task.API_PATH) String taskId,
                                                  @TaskResolved Task task,
                                                  @Parameter(description = "body", required = true)
                                                  @CurrentUserResolved User user,
                                                  @RequestBody @Valid ExternalSignatureParams externalSignatureParams) {

        if (task.getAction() != EXTERNAL_SIGNATURE) {
            throw new LocalizedStatusException(BAD_REQUEST, "message.you_can_t_perform_a_x_action_on_a_y_task", EXTERNAL_SIGNATURE, task.getAction());
        }

        performExternalSignature(tenant, desk, folder, task, user, externalSignatureParams);
    }


    @PutMapping("desk/{deskId}/folder/{folderId}/task/{taskId}/config/{configId}/external_signature/force")
    @Operation(summary = "External signature", description = "Perform an external signature")
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_400, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public void finalizeExternalSignature(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                          @PathVariable(name = Tenant.API_PATH) String tenantId,
                                          @TenantResolved Tenant tenant,
                                          @Parameter(description = DeskRepresentation.API_DOC_ID_VALUE)
                                          @PathVariable(name = DeskRepresentation.API_PATH) String deskId,
                                          @DeskResolved Desk desk,
                                          @Parameter(description = Folder.API_DOC_ID_VALUE)
                                          @PathVariable(name = Folder.API_PATH) String folderId,
                                          @FolderResolved(permission = CURRENT_DESK) Folder folder,
                                          @Parameter(description = Task.API_DOC_ID_VALUE)
                                          @PathVariable(name = Task.API_PATH) String taskId,
                                          @TaskResolved Task task,
                                          @CurrentUserResolved User user,
                                          @PathVariable(name = ExternalSignatureConfig.API_PATH) String externalSignatureConfigId,
                                          @ExternalSignatureConfigResolved ExternalSignatureConfig externalSignatureConfig) {

        if (task.getAction() != EXTERNAL_SIGNATURE) {
            throw new LocalizedStatusException(BAD_REQUEST, "message.you_can_t_perform_a_x_action_on_a_y_task", EXTERNAL_SIGNATURE, task.getAction());
        }

        Status status = this.externalSignatureService.getProcedureStatus(externalSignatureConfig, task.getExternalSignatureProcedureId());
        if (status != Status.SIGNED) {
            throw new LocalizedStatusException(BAD_REQUEST, "message.you_can_t_force_an_unsigned_external_signature_task");
        }

        try {
            this.externalSignatureService.updateFilesAndPerformTask(externalSignatureConfig, user, task.getExternalSignatureProcedureId());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

    }


    @PutMapping("desk/{deskId}/folder/{folderId}/task/{taskId}/ipng")
    @Operation(summary = "ipng", description = "Perform an ipng request", hidden = HIDE_UNSTABLE_API)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Action performed"),
            @ApiResponse(responseCode = "400", description = "Wrong action attempt"),
            @ApiResponse(responseCode = "401", description = "The user is not logged, or its session has expired"),
            @ApiResponse(responseCode = "403", description = "Authenticated user doesn't have desk's role of given task.\n(You might want to set it in Keycloak)"),
            @ApiResponse(responseCode = "404", description = "The given taskId does not exist")
    })
    public void requestIpng(@RequestHeader("Authorization") String token,
                            @Parameter(description = Tenant.API_DOC_ID_VALUE)
                            @PathVariable(name = Tenant.API_PATH) String tenantId,
                            @TenantResolved Tenant tenant,
                            @Parameter(description = DeskRepresentation.API_DOC_ID_VALUE)
                            @PathVariable(name = DeskRepresentation.API_PATH) String deskId,
                            @DeskResolved Desk desk,
                            @Parameter(description = Folder.API_DOC_ID_VALUE)
                            @PathVariable(name = Folder.API_PATH) String folderId,
                            @FolderResolved(permission = CURRENT_DESK) Folder folder,
                            @Parameter(description = Task.API_DOC_ID_VALUE)
                            @PathVariable(name = Task.API_PATH) String taskId,
                            @TaskResolved Task task,
                            @Parameter(description = "body", required = true)
                            @RequestBody IpngParams ipngParams) {

        log.info("requestIpng - ipngParams : {}", ipngParams);

        if (task.getAction() != IPNG) {
            throw new LocalizedStatusException(BAD_REQUEST, "message.you_can_t_perform_a_x_action_on_a_y_task", IPNG, task.getAction());
        }

        performIpng(tenant, desk, folder, task, ipngParams, token);
    }


    @PutMapping("desk/{deskId}/folder/{folderId}/task/{taskId}/secure_mail")
    @Operation(summary = "Secure mail", description = "Perform a mail sec")
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_400, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public void requestSecureMail(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                  @PathVariable(name = Tenant.API_PATH) String tenantId,
                                  @TenantResolved Tenant tenant,
                                  @Parameter(description = DeskRepresentation.API_DOC_ID_VALUE)
                                  @PathVariable(name = DeskRepresentation.API_PATH) String deskId,
                                  @DeskResolved Desk desk,
                                  @Parameter(description = Folder.API_DOC_ID_VALUE)
                                  @PathVariable(name = Folder.API_PATH) String folderId,
                                  @FolderResolved(permission = CURRENT_DESK) Folder folder,
                                  @Parameter(description = Task.API_DOC_ID_VALUE)
                                  @PathVariable(name = Task.API_PATH) String taskId,
                                  @TaskResolved Task task,
                                  @CurrentUserResolved User user,
                                  @Parameter(description = "body", required = true)
                                  @RequestBody @Valid MailParams mailParams) {

        if (task.getAction() != SECURE_MAIL) {
            throw new LocalizedStatusException(BAD_REQUEST, "message.you_can_t_perform_a_x_action_on_a_y_task", SECURE_MAIL, task.getAction());
        }

        performSecureMail(tenant, desk, folder, task, user, mailParams);
    }


    @PutMapping("desk/{deskId}/folder/{folderId}/task/{taskId}/reject")
    @Operation(summary = "Reject", description = "Perform a straightforward Reject on a Task")
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_400, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public void reject(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                       @PathVariable(name = Tenant.API_PATH) String tenantId,
                       @TenantResolved Tenant tenant,
                       @Parameter(description = DeskRepresentation.API_DOC_ID_VALUE)
                       @PathVariable(name = DeskRepresentation.API_PATH) String deskId,
                       @DeskResolved Desk desk,
                       @Parameter(description = Folder.API_DOC_ID_VALUE)
                       @PathVariable(name = Folder.API_PATH) String folderId,
                       @FolderResolved Folder folder,
                       @Parameter(description = Task.API_DOC_ID_VALUE)
                       @PathVariable(name = Task.API_PATH) String taskId,
                       @TaskResolved Task task,
                       @Parameter(name = SimpleTaskParams.API_NAME, description = SimpleTaskParams.API_VALUE, required = true)
                       @RequestBody @Valid SimpleTaskParams performTaskRequest) {

        if (length(performTaskRequest.getPublicAnnotation()) < 3) {
            throw new LocalizedStatusException(BAD_REQUEST, "message.rejecting_a_tasks_need_a_public_annotation_longer_than_3_chars");
        }

        String userId = KeycloakSecurityUtils.getCurrentSessionUserId();
        log.debug("getFolder current user:{}", userId);

        handleExternalTasks(folder, task);
        performSimpleAction(tenant, desk, folder, task, REJECT, performTaskRequest, null);
    }


    @PutMapping("desk/{deskId}/folder/{folderId}/task/{taskId}/bypass")
    @Operation(summary = "Bypass", description = "Force an external task stuck in a waiting state.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_400, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public void bypass(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                       @PathVariable(name = Tenant.API_PATH) String tenantId,
                       @TenantResolved Tenant tenant,
                       @Parameter(description = DeskRepresentation.API_DOC_ID_VALUE)
                       @PathVariable(name = DeskRepresentation.API_PATH) String deskId,
                       @DeskResolved Desk desk,
                       @Parameter(description = Folder.API_DOC_ID_VALUE)
                       @PathVariable(name = Folder.API_PATH) String folderId,
                       @FolderResolved Folder folder,
                       @Parameter(description = Task.API_DOC_ID_VALUE)
                       @PathVariable(name = Task.API_PATH) String taskId,
                       @TaskResolved Task task,
                       @Parameter(name = SimpleTaskParams.API_NAME, description = SimpleTaskParams.API_VALUE, required = true)
                       @RequestBody @Valid SimpleTaskParams performTaskRequest) {

        String userId = KeycloakSecurityUtils.getCurrentSessionUserId();
        log.debug("getFolder current user:{}", userId);

        handleExternalTasks(folder, task);
        performSimpleAction(tenant, desk, folder, task, BYPASS, performTaskRequest, null);
    }


    @PutMapping("desk/{deskId}/folder/{folderId}/task/{taskId}/transfer")
    @Operation(
            summary = "Transfer",
            description = """
                          Transfers the task to another desk.
                          This action adds a step to the workflow history.
                          """
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_400, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public void transfer(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                         @PathVariable(name = Tenant.API_PATH) String tenantId,
                         @TenantResolved Tenant tenant,
                         @Parameter(description = DeskRepresentation.API_DOC_ID_VALUE)
                         @PathVariable(name = DeskRepresentation.API_PATH) String deskId,
                         @DeskResolved Desk desk,
                         @Parameter(description = Folder.API_DOC_ID_VALUE)
                         @PathVariable(name = Folder.API_PATH) String folderId,
                         @FolderResolved Folder folder,
                         @Parameter(description = Task.API_DOC_ID_VALUE)
                         @PathVariable(name = Task.API_PATH) String taskId,
                         @TaskResolved Task task,
                         @Parameter(
                                 name = "body",
                                 description = "Target desk ID that will have to perform the action.",
                                 required = true)
                         @RequestBody @Valid TransferTaskParams transferTaskParams) {
        performSimpleAction(tenant, desk, folder, task, TRANSFER, transferTaskParams, transferTaskParams.getTargetDeskId());
    }


    @PutMapping("desk/{deskId}/folder/{folderId}/task/{taskId}/ask_second_opinion")
    @Operation(
            summary = "Ask second opinion",
            description = """
                          Adds a simple visa to another desk.
                          This action adds a step to the workflow history.

                          Once this second opinion is performed, the task will return to the original desk, leaving the final choice to the original user.
                          """
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_400, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public void askSecondOpinion(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                 @PathVariable(name = Tenant.API_PATH) String tenantId,
                                 @TenantResolved Tenant tenant,
                                 @Parameter(description = DeskRepresentation.API_DOC_ID_VALUE)
                                 @PathVariable(name = DeskRepresentation.API_PATH) String deskId,
                                 @DeskResolved Desk desk,
                                 @Parameter(description = Folder.API_DOC_ID_VALUE)
                                 @PathVariable(name = Folder.API_PATH) String folderId,
                                 @FolderResolved Folder folder,
                                 @Parameter(description = Task.API_DOC_ID_VALUE)
                                 @PathVariable(name = Task.API_PATH) String taskId,
                                 @TaskResolved Task task,
                                 @Parameter(
                                         name = "body",
                                         description = "Target desk ID that will have to perform the action.",
                                         required = true)
                                 @RequestBody @Valid TransferTaskParams transferTaskParams) {
        performSimpleAction(tenant, desk, folder, task, ASK_SECOND_OPINION, transferTaskParams, transferTaskParams.getTargetDeskId());
    }


    @PutMapping("desk/{deskId}/folder/{folderId}/task/{taskId}/undo")
    @Operation(summary = "Undo", description = "Rollback the action to the previous state.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_400, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public void undo(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                     @PathVariable(name = Tenant.API_PATH) String tenantId,
                     @TenantResolved Tenant tenant,
                     @Parameter(description = DeskRepresentation.API_DOC_ID_VALUE)
                     @PathVariable(name = DeskRepresentation.API_PATH) String deskId,
                     @DeskResolved Desk desk,
                     @Parameter(description = Folder.API_DOC_ID_VALUE)
                     @PathVariable(name = Folder.API_PATH) String folderId,
                     @FolderResolved(permission = CURRENT_DESK) Folder folder,
                     @Parameter(description = Task.API_DOC_ID_VALUE)
                     @PathVariable(name = Task.API_PATH) String taskId,
                     @TaskResolved Task task,
                     @Parameter(name = SimpleTaskParams.API_NAME, description = SimpleTaskParams.API_VALUE, required = true)
                     @RequestBody @Valid SimpleTaskParams performTaskRequest) {

        String userId = KeycloakSecurityUtils.getCurrentSessionUserId();
        log.debug("getFolder current user:{}", userId);

        handleExternalTasks(folder, task);
        performSimpleAction(tenant, desk, folder, task, UNDO, performTaskRequest, null);
    }


    @PutMapping("desk/{deskId}/folder/{folderId}/task/{taskId}/recycle")
    @Operation(summary = "Recycle", description = "Returns a rejected folder in its draft task.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_400, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public void recycle(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                        @PathVariable(name = Tenant.API_PATH) String tenantId,
                        @TenantResolved Tenant tenant,
                        @Parameter(description = DeskRepresentation.API_DOC_ID_VALUE)
                        @PathVariable(name = DeskRepresentation.API_PATH) String deskId,
                        @DeskResolved Desk desk,
                        @Parameter(description = Folder.API_DOC_ID_VALUE)
                        @PathVariable(name = Folder.API_PATH) String folderId,
                        @FolderResolved Folder folder,
                        @Parameter(description = Task.API_DOC_ID_VALUE)
                        @PathVariable(name = Task.API_PATH) String taskId,
                        @TaskResolved Task task,
                        @Parameter(name = SimpleTaskParams.API_NAME, description = SimpleTaskParams.API_VALUE, required = true)
                        @RequestBody @Valid SimpleTaskParams simpleTaskParams) {

        // Integrity checks

        if (task.getAction() != DELETE) {
            throw new LocalizedStatusException(BAD_REQUEST, "message.you_can_t_perform_a_x_action_on_a_y_task", RECYCLE, task.getAction());
        }

        // Action

        performSimpleAction(tenant, desk, folder, task, RECYCLE, simpleTaskParams, null);
    }


    @PutMapping("desk/{deskId}/folder/{folderId}/task/{taskId}/seal")
    @Operation(summary = "Seal", description = "Perform a Seal on a Task.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_400, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public void seal(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                     @PathVariable(name = Tenant.API_PATH) String tenantId,
                     @TenantResolved Tenant tenant,
                     @Parameter(description = DeskRepresentation.API_DOC_ID_VALUE)
                     @PathVariable(name = DeskRepresentation.API_PATH) String deskId,
                     @DeskResolved Desk desk,
                     @Parameter(description = Folder.API_DOC_ID_VALUE)
                     @PathVariable(name = Folder.API_PATH) String folderId,
                     @FolderResolved(permission = CURRENT_DESK) Folder folder,
                     @Parameter(description = Task.API_DOC_ID_VALUE)
                     @PathVariable(name = Task.API_PATH) String taskId,
                     @TaskResolved Task task,
                     @CurrentUserResolved User user,
                     @Parameter(name = SimpleTaskParams.API_NAME, description = SimpleTaskParams.API_VALUE, required = true)
                     @RequestBody @Valid SimpleTaskParams simpleTaskParams) {

        if (task.getAction() != SEAL) {
            throw new LocalizedStatusException(BAD_REQUEST, "message.you_can_t_perform_a_x_action_on_a_y_task", SEAL, task.getAction());
        }

        performSeal(tenant, desk, folder, task, user, simpleTaskParams);
    }


    @PutMapping("desk/{deskId}/folder/{folderId}/task/{taskId}/paper_signature")
    @Operation(summary = "Paper signature", description = "Bypass the current Signature Task with a straightforward Visa.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_400, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_406, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public void paperSignature(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                               @PathVariable(name = Tenant.API_PATH) String tenantId,
                               @TenantResolved Tenant tenant,
                               @Parameter(description = DeskRepresentation.API_DOC_ID_VALUE)
                               @PathVariable(name = DeskRepresentation.API_PATH) String deskId,
                               @DeskResolved Desk desk,
                               @Parameter(description = Folder.API_DOC_ID_VALUE)
                               @PathVariable(name = Folder.API_PATH) String folderId,
                               @FolderResolved(permission = CURRENT_DESK) Folder folder,
                               @Parameter(description = Task.API_DOC_ID_VALUE)
                               @PathVariable(name = Task.API_PATH) String taskId,
                               @TaskResolved Task task,
                               @Parameter(name = SimpleTaskParams.API_NAME, description = SimpleTaskParams.API_VALUE, required = true)
                               @RequestBody @Valid SimpleTaskParams simpleTaskParams) {

        // Integrity check

        if (task.getAction() != SIGNATURE) {
            throw new LocalizedStatusException(BAD_REQUEST, "message.you_can_t_perform_a_x_action_on_a_y_task", PAPER_SIGNATURE, task.getAction());
        }

        typologyService.updateTypology(singletonList(folder));

        if (folder.getSubtype().isDigitalSignatureMandatory()) {
            throw new ResponseStatusException(NOT_ACCEPTABLE, "Subtype parameters forbid paper-signing this folder");
        }

        // Signature

        performSimpleAction(tenant, desk, folder, task, PAPER_SIGNATURE, simpleTaskParams, null);
    }


    @PutMapping("desk/{deskId}/folder/{folderId}/task/{taskId}/sign")
    @Operation(summary = "Signature", description = "Perform a straightforward Signature on a Task.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_400, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_406, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public void signature(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                          @PathVariable(name = Tenant.API_PATH) String tenantId,
                          @TenantResolved Tenant tenant,
                          @Parameter(description = DeskRepresentation.API_DOC_ID_VALUE)
                          @PathVariable(name = DeskRepresentation.API_PATH) String deskId,
                          @DeskResolved Desk desk,
                          @Parameter(description = Folder.API_DOC_ID_VALUE)
                          @PathVariable(name = Folder.API_PATH) String folderId,
                          @FolderResolved(permission = CURRENT_DESK) Folder folder,
                          @Parameter(description = Task.API_DOC_ID_VALUE)
                          @PathVariable(name = Task.API_PATH) String taskId,
                          @TaskResolved Task task,
                          @Parameter(description = """
                                                   The publicAnnotation is visible to any users, and will be stored in task history.

                                                   The privateAnnotation will only be sent to the workflow's next user.
                                                   It won't be archived anywhere.

                                                   Every field should be properly set.
                                                   """)
                          @RequestBody @Valid SignatureTaskParams signatureTaskParams,
                          @CurrentUserResolved User currentUser) {

        // Integrity check

        typologyService.updateTypology(singletonList(folder));
        folder.setDocumentList(contentService.getDocumentList(folder.getContentId()));

        checkActionAllowedWithDetachedSignatures(folder, SIGNATURE);

        if (task.getAction() != SIGNATURE) {
            throw new LocalizedStatusException(BAD_REQUEST, "message.you_can_t_perform_a_x_action_on_a_y_task", SIGNATURE, task.getAction());
        }

        List<Task> readTasks = workflowService.getReadTasks(folder);
        boolean isReadingMandatory = folder.getSubtype().isReadingMandatory();
        Pair<Long, Long> currentIndex = Pair.of(task.getWorkflowIndex(), task.getStepIndex());
        boolean isAlreadyReadByUser = workflowService.hasAlreadyBeenReadAtIndex(currentIndex, currentUser, readTasks);
        if (isReadingMandatory && !isAlreadyReadByUser) {
            throw new LocalizedStatusException(NOT_ACCEPTABLE, "message.you_have_to_read_this_folder_before_signing_it");
        }

        // Signature

        performSignature(tenant, desk, task, folder, currentUser, signatureTaskParams);
    }


    @PutMapping("desk/{deskId}/folder/{folderId}/task/{taskId}/chain")
    @Operation(summary = "Chain", description = "Perform a Chain on a Task.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_400, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public void chain(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                      @PathVariable(name = Tenant.API_PATH) String tenantId,
                      @TenantResolved Tenant tenant,
                      @Parameter(description = DeskRepresentation.API_DOC_ID_VALUE)
                      @PathVariable(name = DeskRepresentation.API_PATH) String deskId,
                      @DeskResolved Desk desk,
                      @Parameter(description = Folder.API_DOC_ID_VALUE)
                      @PathVariable(name = Folder.API_PATH) String folderId,
                      @FolderResolved(permission = CURRENT_DESK) Folder existingFolder,
                      @Parameter(description = Task.API_DOC_ID_VALUE)
                      @PathVariable(name = Task.API_PATH) String taskId,
                      @TaskResolved Task task,
                      @Parameter(name = "body", description = "subtypeId field is mandatory.")
                      @RequestBody @Valid CreateFolderRequest draftFolderParams) {

        // Integrity check

        if (task.getAction() != ARCHIVE) {
            throw new LocalizedStatusException(NOT_ACCEPTABLE, "message.you_can_t_perform_a_x_action_on_a_y_task", CHAIN, task.getAction());
        }

        boolean hasChainActionPermission = permissionService.currentUserHasChainingRightOnSomeDeskIn(tenantId, singleton(deskId), null, null);
        if (!hasChainActionPermission) {
            throw new LocalizedStatusException(NOT_ACCEPTABLE, "message.you_can_t_chain_from_this_desk");
        }

        Folder dummyUpdatedFolder = modelMapper.map(draftFolderParams, Folder.class);
        dummyUpdatedFolder.setType(ofNullable(draftFolderParams.getTypeId()).map(i -> Type.builder().id(i).build()).orElse(null));
        dummyUpdatedFolder.setSubtype(ofNullable(draftFolderParams.getSubtypeId()).map(i -> Subtype.builder().id(i).build()).orElse(null));

        typologyService.updateTypology(List.of(existingFolder, dummyUpdatedFolder));

        List<Document> documentList = contentService.getDocumentList(existingFolder.getContentId());

        SignatureFormat oldSignatureFormat = cryptoService.getSignatureFormat(existingFolder.getType(), documentList);
        existingFolder.getType().setSignatureFormat(oldSignatureFormat);
        SignatureFormat newSignatureFormat = cryptoService.getSignatureFormat(dummyUpdatedFolder.getType(), documentList);
        dummyUpdatedFolder.getType().setSignatureFormat(newSignatureFormat);

        CryptoUtils.checkSignatureFormat(newSignatureFormat, documentList, null);
        CryptoUtils.checkTypeReplacementCompatibility(existingFolder.getType(), dummyUpdatedFolder.getType());
        CryptoUtils.checkSubtypeReplacementCompatibility(existingFolder.getSubtype(), dummyUpdatedFolder.getSubtype());

        // Chain
        handleVariableDesksMetadata(
                dummyUpdatedFolder,
                desk,
                draftFolderParams.getVariableDesksIds(),
                tenant.getId()
        );

        workflowService.editFolder(folderId, modelMapper.map(dummyUpdatedFolder, FolderDto.class));

        workflowService.performTask(
                CHAIN,
                task,
                desk.getId(),
                existingFolder,
                null,
                dummyUpdatedFolder.getValidationWorkflowDefinitionKey(),
                desk.getId(), // On a chain action, the current desk becomes the new emitter. That's a part of the feature.
                ofNullable(dummyUpdatedFolder.getFinalDesk()).map(DeskRepresentation::getId).orElse(null),
                null
        );

        Long timeToCompleteInHours = statsService.computeTimeToCompleteInHours(task);
        statsService.registerFolderAction(tenant, CHAIN, existingFolder, desk, timeToCompleteInHours);
    }


    private void handleVariableDesksMetadata(Folder dummyUpdatedFolder,
                                             Desk desk,
                                             Map<String, String> variableDesksIds,
                                             String tenantId) {

        WorkflowDefinition validationWorkflowDefinition = workflowService.getWorkflowDefinitionByKey(
                        tenantId,
                        dummyUpdatedFolder.getSubtype().getValidationWorkflowId(),
                        desk.getId()
                )
                .orElseThrow(() -> new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.unknown_workflow_definition_id"));


        Map<Integer, String> variableDesksIdsMap = new HashMap<>();
        if (variableDesksIds != null) {
            variableDesksIds.forEach((key, value) -> variableDesksIdsMap.put(Integer.parseInt(key), value));
        }

        groovyService.updateWorkflowDataFromScript(tenantId, dummyUpdatedFolder, variableDesksIdsMap);

        dummyUpdatedFolder.setFinalDesk(validationWorkflowDefinition.getFinalDesk());

        Map<String, String> additionalMetadata = workflowService.computePlaceholderDesksConcreteValues(
                validationWorkflowDefinition,
                desk.getId(),
                variableDesksIdsMap
        );
        workflowService.mapIndexedPlaceholdersToActualDesk(dummyUpdatedFolder.getFinalDesk(), desk.getId(), additionalMetadata);

        dummyUpdatedFolder.getStepList()
                .stream()
                .map(Task::getDesks)
                .flatMap(Collection::stream)
                .forEach(d -> workflowService.mapIndexedPlaceholdersToActualDesk(d, desk.getId(), additionalMetadata));


        dummyUpdatedFolder.getMetadata().putAll(additionalMetadata);
    }


    // </editor-fold desc="Requests">


    // <editor-fold desc="Common check">


    private void triggerPostTaskActions(String tenantId, Desk desk, Folder folder, @NotNull Action performedAction) {
        this.triggerIpngPostAction(tenantId, desk, folder);
        if (!performedAction.equals(READ)) {
            this.disableDeleteCapabilityOnCurrentDocuments(folder);
        }
    }


    private void triggerIpngPostAction(String tenantId, Desk desk, Folder folder) {
        folder = workflowService.getFolder(folder.getId(), tenantId);
        if (folder == null) {
            log.warn("Could not retrieve folder for post-execution action");
            return;
        }

        // We filter first on whether an IPNG metadata is present, as this should be both less frequent and faster to test
        if (folder.getMetadata().containsKey(METADATA_KEY_IPNG_RETURN_DESKBOX_ID)
                && folder.getMetadata().containsKey(METADATA_KEY_IPNG_RETURN_BUSINESS_ID)) {
            // calculate it is over

            List<Action> endActions = List.of(ARCHIVE, CHAIN, UNDO);
            List<Task> remainingNonEndSteps = folder.getStepList().stream()
                    .filter(t -> t.getState() == UPCOMING)
                    .filter(t -> !endActions.contains(t.getAction()))
                    .toList();

            if (remainingNonEndSteps.size() == 0) {

                // send back trough IPNG
                prepareAndSendIpngResponse(tenantId, desk.getId(), folder);
            }
        }
    }


    private void disableDeleteCapabilityOnCurrentDocuments(Folder folder) {
        this.contentService.getDocumentList(folder.getContentId())
                .stream()
                .filter(document -> !document.isMainDocument())
                .forEach(document -> {
                    document.setDeletable(false);
                    this.contentService.updateDocumentAlfrescoProperties(document);
                });
    }


    private void assertCurrentUserCanPerformTask(@NotNull String tenantId,
                                                 @NotNull Task task,
                                                 @Nullable User user,
                                                 @Nullable String typeId,
                                                 @Nullable String subtypeId,
                                                 @Nullable String deskId) {

        List<String> taskDeskIds = task.getDesks().stream().map(DeskRepresentation::getId).toList();
        boolean delegationIncompatible = !permissionService.currentUserHasDelegationOnSomeDeskIn(
                tenantId,
                Set.copyOf(taskDeskIds),
                null,
                null
        );

        if (!taskDeskIds.contains(deskId) && delegationIncompatible) {
            throw new LocalizedStatusException(BAD_REQUEST, "message.current_desk_not_consistent_with_task_desk_or_delegation");
        }

        if (user != null && user.getId().equals(AUTOMATIC_TASK_USER_ID)) {
            log.info("Automatic user, bypassing permission check.");
            return;
        }

        Set<String> candidateDeskIds = task.getDesks().stream()
                .map(DeskRepresentation::getId)
                .collect(toSet());

        boolean canPerformActionOnFolderAsDesk = permissionService.currentUserHasAdminRightsOnTenant(tenantId) ||
                permissionService.currentUserHasFolderActionRightOnSomeDeskIn(tenantId, candidateDeskIds, typeId, subtypeId);

        if (!canPerformActionOnFolderAsDesk) {
            log.info("assertCurrentUserCanPerformTask : failed to perform task {}, insufficient rights", task);
            throw new LocalizedStatusException(FORBIDDEN, "message.you_can_t_perform_an_action_on_this_folder");
        }
    }


    // </editor-fold desc="Common check">


    private void prepareAndSendIpngResponse(String tenantId, String deskId, Folder folder) {
        log.info("sending back folder to original sender through ipng");
        IpngProof proof = new IpngProof();
        String uuid = randomUUID().toString();
        proof.setId(this.ipngService.buildProofId(uuid, folder.getName().trim()));

        proof.setBusinessId(folder.getMetadata().get(METADATA_KEY_IPNG_RETURN_BUSINESS_ID));
        proof.setReceiverDeskboxId(folder.getMetadata().get(METADATA_KEY_IPNG_RETURN_DESKBOX_ID));
        proof.setSenderDeskboxId(folder.getMetadata().get(METADATA_KEY_IPNG_SOURCE_DESKBOX_ID));
        proof.setProofType(IpngProof.ProofType.RESPONSE);

        // TODO Maybe it is here that we should find the appropriate credentials to use with IPNG.
        String token = "";
        populateIpngResponseProofFromFolder(token, tenantId, folder, proof);

        try {
            this.sendIpngProof(token, folder, proof, proof.getId());
        } catch (NoSuchAlgorithmException | IOException e) {
            log.error("could not send proof " + proof + " back to ipng", e);
        }
    }


    /**
     * Factorized task for every simple action (VISA, REJECT...)
     * Every task except SIGNATURE, SEAL, or EXTERNAL_SIGNATURE, basically.
     *
     * @param task             Should exists, and target the relevant action
     * @param simpleTaskParams Contains step metadata, public & private annotations
     * @param targetDesk       Only relevant on {@link Action#TRANSFER} and {@link Action#ASK_SECOND_OPINION}.
     */
    private void performSimpleAction(@NotNull Tenant tenant,
                                     @NotNull Desk desk,
                                     @NotNull Folder folder,
                                     @NotNull Task task,
                                     @NotNull Action action,
                                     @NotNull SimpleTaskParams simpleTaskParams,
                                     @Nullable String targetDesk) {

        if (!action.equalsOneOf(REJECT, READ, CHAIN, UNDO, TRANSFER, RECYCLE, ASK_SECOND_OPINION)) {
            workflowBusinessService.checkForNullOrEmptyDesksInNextStep(tenant.getId(), folder);
        }

        // TODO : Maybe we should do this check directly in Workflow, to avoid this request.
        boolean isNotSecondaryAction = !List.of(REJECT, BYPASS, UNDO, TRANSFER, ASK_SECOND_OPINION).contains(action);
        boolean isNotTheSameAction = (task.getAction() != action);
        boolean isAnAcceptableSignature = (task.getAction() == SIGNATURE) && (action == PAPER_SIGNATURE);
        boolean isAnAcceptableRecycle = (task.getAction() == DELETE) && (action == RECYCLE);
        if (isNotSecondaryAction && isNotTheSameAction && !isAnAcceptableSignature && !isAnAcceptableRecycle) {
            throw new LocalizedStatusException(BAD_REQUEST, "message.you_can_t_perform_a_x_action_on_a_y_task", action, task.getAction());
        }

        assertCurrentUserCanPerformTask(tenant.getId(), task, null, folder.getType().getId(), folder.getSubtype().getId(), desk.getId());

        // Verify metadata

        if (!List.of(REJECT, UNDO, ARCHIVE, RECYCLE).contains(action)) {
            typologyService.verifyFolderMetadataForValidationWorkflow(tenant.getId(), folder);
        }

        // Perform task

        workflowService.performTask(task, action, folder, desk.getId(), simpleTaskParams, targetDesk, null);

        Long timeToCompleteInHours = statsService.computeTimeToCompleteInHours(task);
        statsService.registerFolderAction(tenant, action, folder, desk, timeToCompleteInHours);

        triggerPostTaskActions(tenant.getId(), desk, folder, action);
        log.info("PerformAction taskId:{} action:{}", task.getId(), task.getAction());
    }


    // @PreAuthorize("hasRole(#task.candidateGroup)")
    private void performExternalSignature(@NotNull Tenant tenant,
                                          @NotNull Desk desk,
                                          @NotNull Folder folder,
                                          @NotNull Task task,
                                          @NotNull User user,
                                          @NotNull ExternalSignatureParams externalSignatureParams) {
        workflowBusinessService.checkForNullOrEmptyDesksInNextStep(tenant.getId(), folder);

        assertCurrentUserCanPerformTask(tenant.getId(), task, user, folder.getType().getId(), folder.getSubtype().getId(), desk.getId());

        folder.setDocumentList(contentService.getDocumentList(folder.getContentId()));

        if (StringUtils.isEmpty(folder.getSubtype().getId())) {
            throw new LocalizedStatusException(BAD_REQUEST, "message.unknown_subtype_id");
        }

        checkActionAllowedWithDetachedSignatures(folder, EXTERNAL_SIGNATURE);

        List<Document> documentList = contentService.getDocumentList(folder.getContentId()).stream()
                .filter(Document::isMainDocument)
                .filter(document -> document.getMediaType().equals(APPLICATION_PDF))
                .toList();

        String procedureId = externalSignatureService.createProcedure(folder, externalSignatureParams, documentList, folder.getSubtype().getId());
        workflowService.performExternalSignature(task, user, folder, externalSignatureParams, procedureId);

        Long timeToCompleteInHours = statsService.computeTimeToCompleteInHours(task);
        statsService.registerFolderAction(tenant, EXTERNAL_SIGNATURE, folder, desk, timeToCompleteInHours);

        this.triggerPostTaskActions(tenant.getId(), desk, folder, EXTERNAL_SIGNATURE);
    }


    // @PreAuthorize("hasRole(#task.candidateGroup)")
    private void performIpng(@NotNull Tenant tenant,
                             @NotNull Desk desk,
                             @NotNull Folder folder,
                             @NotNull Task task,
                             @NotNull IpngParams ipngParams,
                             String token) {

        workflowBusinessService.checkForNullOrEmptyDesksInNextStep(tenant.getId(), folder);

        if (StringUtils.isEmpty(folder.getSubtype().getId())) {
            throw new LocalizedStatusException(BAD_REQUEST, "message.unknown_subtype_id");
        }
        log.info("performIpng - ipngParams : {}", ipngParams);

        IpngProof proof = ipngParams.getIpngProof();
        populateIpngProofFromFolder(token, tenant.getId(), folder, proof);

        String senderDeskboxId = this.ipngBindings.getMappedDeskboxIdForDesk(tenant.getId(), desk.getId());
        log.info("performIpng - senderDeskboxId : {}", senderDeskboxId);
        proof.setSenderDeskboxId(senderDeskboxId);

        // Send files

        String cleanFolderName = folder.getName().strip();
        String proofRandomUid = randomUUID().toString();
        String proofId = this.ipngService.buildProofId(proofRandomUid, cleanFolderName);
        log.warn("generated file name = {}", proofId);
        proof.setId(proofId);


        String businessRandomUid = randomUUID().toString();
        proof.setBusinessId(this.ipngService.buildBusinessId(businessRandomUid, cleanFolderName));

        String userId = KeycloakSecurityUtils.getCurrentSessionUserId();

        this.workflowService.setFolderWaitingForIpngResponse(proof, tenant.getId(), folder);

        try {
            sendIpngProof(token, folder, proof, proofId);

            workflowService.performIpng(task, folder, proof.getBusinessId(), userId, ipngParams);
            Long timeToCompleteInHours = statsService.computeTimeToCompleteInHours(task);
            statsService.registerFolderAction(tenant, IPNG, folder, desk, timeToCompleteInHours);

        } catch (NoSuchAlgorithmException e) {
            log.debug("An unlikely exception occurred while trying to transfer the folder {}, aborting IPNG post", proofId);
            throw new ResponseStatusException(INTERNAL_SERVER_ERROR, "Error building or sending folder for transfer");
        } catch (IOException e) {
            throw new ResponseStatusException(INTERNAL_SERVER_ERROR, "Error building folder for transfer");
        }
    }


    private void populateIpngResponseProofFromFolder(String token, String tenantId, Folder folder, IpngProof proof) {
        IpngTypology ipngFullTypo = this.ipngService.getLatestTypology(token);
        IpngMetadataList ipngFullMeta = this.ipngService.getLatestMetadataList(token);

        String allIpngMetadata = folder.getMetadata().get(METADATA_KEY_IPNG_RETURN_METADATA_IDS);
        List<IpngMetadata> proofMetadataList = new ArrayList<>();
        if (!StringUtils.isEmpty(allIpngMetadata)) {
            List<String> metadataKeys = Arrays.asList(allIpngMetadata.split(IPNG_METADATA_KEYS_SEPARATOR));

            metadataKeys.forEach(key -> {
                ipngFullMeta.getMetadatas().stream()
                        .filter(ipngMetadata -> ipngMetadata.getKey().equals(key))
                        .findFirst()
                        .ifPresent(proofMetadataList::add);
            });
        }
        proof.setMetadatas(proofMetadataList);

        String ipngTypeKey = folder.getMetadata().getOrDefault(METADATA_KEY_IPNG_RETURN_TYPE_ID, "000-default");
        proof.setType(ipngFullTypo.getTypes()
                .stream()
                .filter(ipngType -> StringUtils.equals(ipngType.getKey(), ipngTypeKey))
                .findAny()
                .orElseThrow(() -> new ResponseStatusException(INTERNAL_SERVER_ERROR, "Error retrieving IPNG type"))
        );
    }


    private void populateIpngProofFromFolder(String token, String tenantId, Folder folder, IpngProof proof) {
        IpngTypology ipngFullTypo = this.ipngService.getLatestTypology(token);
        IpngMetadataList ipngFullMeta = this.ipngService.getLatestMetadataList(token);

        log.info("folder metadatas : {}", folder.getMetadata());

        List<IpngMetadata> proofMetadata = new ArrayList<>();
        folder.getMetadata().keySet().stream()
                .filter(Objects::nonNull)
                .forEach(tenantKey -> {
                    String ipngKey = this.ipngBindings.getIpngMetadataKeyForOutgoingTenantMetadata(tenantId, tenantKey);
                    ipngFullMeta.getMetadatas().stream().filter(m -> m.getKey().equals(ipngKey))
                            .findFirst().ifPresent(proofMetadata::add);
                });

        proof.setMetadatas(proofMetadata);

        Subtype subtype = folder.getSubtype();
        log.info("::populateIpngProofFromFolder - current folder subtype : {}", subtype);

        final String mappedType = ofNullable(subtype)
                .map(s -> this.ipngBindings.getOutgoingIpngTypeIdForLocalType(tenantId, s.getId()))
                .orElse("000-default");
        proof.setType(ipngFullTypo.getTypes()
                .stream()
                .filter(ipngType -> StringUtils.equals(ipngType.getKey(), mappedType))
                .findAny()
                .orElseThrow(() -> new ResponseStatusException(INTERNAL_SERVER_ERROR, "Error retrievin IPNG type"))
        );
    }


    public void sendIpngProof(String token, Folder folder, IpngProof proof, String targetName) throws NoSuchAlgorithmException, IOException {
        String folderHash = this.fileTransferService.postFolder(folder, targetName);

        proof.setHash(folderHash);
        // Send data to IPNG
        this.ipngService.postProof(proof, token);
        log.info("Proof sent to IPNG network API, waiting for result");
    }


    // @PreAuthorize("hasRole(#task.candidateGroup)")
    private void performSecureMail(@NotNull Tenant tenant,
                                   @NotNull Desk desk,
                                   @NotNull Folder folder,
                                   @NotNull Task task,
                                   @NotNull User user,
                                   @NotNull MailParams mailParams) {
        workflowBusinessService.checkForNullOrEmptyDesksInNextStep(tenant.getId(), folder);

        assertCurrentUserCanPerformTask(tenant.getId(), task, null, folder.getType().getId(), folder.getSubtype().getId(), desk.getId());

        if (StringUtils.isEmpty(folder.getSubtype().getId())) {
            throw new LocalizedStatusException(BAD_REQUEST, "message.unknown_subtype_id");
        }

        List<Document> documentList = contentService.getDocumentList(folder.getContentId());
        SecureMailDocument pastellDocument = secureMailService.createAndSendSecureMail(tenant, folder, mailParams, documentList);
        workflowService.performSecureMail(task, user, folder, mailParams, pastellDocument.getId());
        Long timeToCompleteInHours = statsService.computeTimeToCompleteInHours(task);
        statsService.registerFolderAction(tenant, SECURE_MAIL, folder, desk, timeToCompleteInHours);

        this.triggerPostTaskActions(tenant.getId(), desk, folder, SECURE_MAIL);
    }


    // @PreAuthorize("hasRole(#task.candidateGroup)")
    private void performSignature(@NotNull Tenant tenant,
                                  @NotNull Desk desk,
                                  @NotNull Task task,
                                  @NotNull Folder folder,
                                  @NotNull User user,
                                  SignatureTaskParams signatureTaskParams) {
        workflowBusinessService.checkForNullOrEmptyDesksInNextStep(tenant.getId(), folder);
        assertCurrentUserCanPerformTask(tenant.getId(), task, user, folder.getType().getId(), folder.getSubtype().getId(), desk.getId());

        if (StringUtils.isNotEmpty(signatureTaskParams.getCustomSignatureField())) {
            List<String> keysToRemove = folder.getMetadata()
                    .keySet()
                    .stream()
                    // FIXME all metadata with the workflow_internal prefix should not be visible here
                    .filter(key -> key.contains("workflow_internal_"))
                    .toList();

            keysToRemove.forEach(key -> folder.getMetadata().remove(key));
            folder.getMetadata().put(SIGNATURE_PLACEHOLDER_CUSTOM_SIGNATURE_FIELD, signatureTaskParams.getCustomSignatureField());
            workflowService.editFolder(folder.getId(), modelMapper.map(folder, FolderDto.class));
        }

        List<DocumentDataToSignHolder> dataToSignHolderList = signatureTaskParams.getDataToSignHolderList();
        String certBase64 = signatureTaskParams.getCertificateBase64();

        List<Document> mainDocumentList = folder.getDocumentList().stream().filter(Document::isMainDocument).toList();

        String userSignatureBase64 = ofNullable(user.getSignatureImageContentId())
                .filter(StringUtils::isNotEmpty)
                .map(contentService::retrieveContentAsBase64)
                .orElse(null);

        // We don't want to partially sign a folder.
        // We'll check for every document signature, first.
        Set<String> mainDocumentIdSet = mainDocumentList.stream().map(Document::getId).collect(toSet());
        Set<String> signedDocumentIdSet = dataToSignHolderList.stream().map(DocumentDataToSignHolder::getDocumentId).collect(toSet());
        if (!CollectionUtils.isEqualCollection(mainDocumentIdSet, signedDocumentIdSet)) {
            log.warn("The number of signatures does not match the number of documents");
            throw new LocalizedStatusException(BAD_REQUEST, "message.error_missing_signature");
        }

        folder.setType(this.typeRepository.findById(folder.getType().getId())
                .orElseThrow(() -> new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.unknown_type_id")));

        SignatureFormat signatureFormat = cryptoService.getSignatureFormat(folder.getType(), folder.getDocumentList());

        mainDocumentList.forEach(d -> {

            DataToSignHolder dataToSignHolder = dataToSignHolderList.stream()
                    .filter(p -> StringUtils.equals(p.getDocumentId(), d.getId()))
                    .findFirst()
                    .orElseThrow(() -> new LocalizedStatusException(BAD_REQUEST, "message.error_missing_signature"));

            // We have to reset the stream here, the document has to be parsed once for the digest,
            // and a second time for the signature injection. Large document streams aren't (and should not) be resettable.
            // Special case : PKCS7 doesn't need the document on signature generation.
            DocumentBuffer docBuffer = (signatureFormat != PKCS7) ? contentService.retrieveContent(d.getId()) : null;

            DocumentBuffer signedDocumentBuffer = cryptoService.signDocument(
                    tenant,
                    desk,
                    folder,
                    user,
                    dataToSignHolder,
                    signatureFormat,
                    CryptoUtils.getPosition(folder, d, SIGNATURE),
                    certBase64,
                    user.getUserName(),
                    false,
                    docBuffer,
                    userSignatureBase64,
                    signatureTaskParams.getCustomSignatureField()
            );
            signedDocumentBuffer.setId(d.getId());
            signedDocumentBuffer.setName(d.getName());

            if (signatureFormat.isEnvelopedSignature()) {
                contentService.updateDocument(d.getId(), signedDocumentBuffer);
            } else {
                task.setUser(user);
                // Hardcoded fix.
                // TODO : Parse it from Crypto
                signedDocumentBuffer.setMediaType(signatureFormat == XADES_DETACHED ? APPLICATION_XML : APPLICATION_PDF);
                contentService.createDetachedSignature(folder.getContentId(), d, signedDocumentBuffer, task);
            }
        });

        contentService.removeFirstSignaturePlacementAnnotation(mainDocumentList);
        workflowService.performTask(task, SIGNATURE, folder, desk.getId(), signatureTaskParams, null, certBase64);
        Long timeToCompleteInHours = statsService.computeTimeToCompleteInHours(task);
        statsService.registerFolderAction(tenant, SIGNATURE, folder, desk, timeToCompleteInHours);

        log.info("PerformAction taskId:{}", task.getId());
        triggerPostTaskActions(tenant.getId(), desk, folder, SIGNATURE);
    }


    // @PreAuthorize("hasRole(#task.candidateGroup)")
    private void performSeal(@NotNull Tenant tenant,
                             @NotNull Desk desk,
                             @NotNull Folder folder,
                             @NotNull Task task,
                             @NotNull User currentUser,
                             @Nullable SimpleTaskParams simpleTaskParams) {

        workflowBusinessService.checkForNullOrEmptyDesksInNextStep(tenant.getId(), folder);

        log.info("performSeal folderId:{}", folder.getId());

        assertCurrentUserCanPerformTask(tenant.getId(), task, currentUser, folder.getType().getId(), folder.getSubtype().getId(), desk.getId());

        // Integrity check

        List<Document> documentList = Optional.of(contentService.getDocumentList(folder.getContentId()))
                .filter(CollectionUtils::isNotEmpty)
                .orElseThrow(() -> new LocalizedStatusException(NOT_FOUND, "message.unknown_document_id"));

        folder.setType(typeRepository.findById(folder.getType().getId())
                .orElseThrow(() -> new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.unknown_type_id")));

        folder.setDocumentList(documentList);

        checkActionAllowedWithDetachedSignatures(folder, SEAL);

        typologyService.updateTypology(singletonList(folder));

        SealCertificate sealCert = ofNullable(folder.getSubtype().getSealCertificateId())
                .map(id -> secretService.getSealCertificate(tenant.getId(), id))
                .orElseThrow(() -> new LocalizedStatusException(NOT_FOUND, "message.unknown_seal_certificate_id"));

        String signatureImageBase64 = ofNullable(sealCert.getSignatureImageContentId())
                .filter(StringUtils::isNotEmpty)
                .map(contentService::retrieveContentAsBase64)
                .orElse(null);

        PrivateKey privateKey = CryptoUtils.getPrivateKey(sealCert);
        if (sealCert.getExpirationDate().before(new Date())) {
            throw new LocalizedStatusException(NOT_ACCEPTABLE, "message.expired_seal_certificate");
        }

        // Actual Seal

        long signatureDateTime = new Date().getTime();
        SignatureFormat signatureFormat = cryptoService.getSignatureFormat(folder.getType(), folder.getDocumentList());

        documentList.stream()
                .filter(Document::isMainDocument)
                .forEach(d -> {

                    DocumentBuffer documentBuffer = contentService.retrieveContent(d.getId());
                    documentBuffer.setName(d.getName());
                    documentBuffer.setId(d.getId());

                    PdfSignaturePosition pdfSigPosition = CryptoUtils.getPosition(folder, d, SEAL);

                    DataToSignHolder dataToSignHolder = cryptoService.getDataToSign(
                            tenant,
                            desk,
                            folder,
                            currentUser,
                            documentBuffer,
                            signatureFormat,
                            pdfSigPosition,
                            sealCert.getPublicCertificateBase64(),
                            sealCert.getName(),
                            signatureDateTime,
                            true,
                            signatureImageBase64,
                            null
                    );

                    for (DataToSign dataToSign : dataToSignHolder.getDataToSignList()) {
                        String stringToSignBase64 = dataToSign.getDataToSignBase64();
                        byte[] bytesToSign = Base64.getDecoder().decode(stringToSignBase64);
                        dataToSign.setSignatureValue(sha256Sign(bytesToSign, privateKey));
                    }

                    // We have to reset the stream here, the document has to be parsed once for the digest,
                    // and a second time for the signature injection. Large document streams aren't (and should not) be resettable.
                    // Special case : PKCS7 doesn't need the document on signature generation.
                    documentBuffer = (signatureFormat != PKCS7) ? contentService.retrieveContent(d.getId()) : null;

                    DocumentBuffer signedDocumentBuffer = cryptoService.signDocument(
                            tenant,
                            desk,
                            folder,
                            currentUser,
                            dataToSignHolder,
                            signatureFormat,
                            pdfSigPosition,
                            sealCert.getPublicCertificateBase64(),
                            sealCert.getName(),
                            true,
                            documentBuffer,
                            signatureImageBase64,
                            ofNullable(simpleTaskParams).map(SimpleTaskParams::getCustomSignatureField).orElse(null)
                    );

                    signedDocumentBuffer.setId(d.getId());

                    if (signatureFormat.isEnvelopedSignature()) {
                        contentService.updateDocument(d.getId(), signedDocumentBuffer, false);
                    } else {
                        task.setUser(currentUser);
                        // Hardcoded fix.
                        // TODO : Parse it from Crypto
                        signedDocumentBuffer.setMediaType(signatureFormat == XADES_DETACHED ? APPLICATION_XML : APPLICATION_OCTET_STREAM);
                        contentService.createDetachedSignature(folder.getContentId(), d, signedDocumentBuffer, task);
                    }
                });

        workflowService.performTask(task, SEAL, folder, desk.getId(), simpleTaskParams, null, sealCert.getPublicCertificateBase64());
        contentService.removeFirstSignaturePlacementAnnotation(documentList);
        Long timeToCompleteInHours = statsService.computeTimeToCompleteInHours(task);
        statsService.registerFolderAction(tenant, SEAL, folder, desk, timeToCompleteInHours);

        log.info("PerformAction taskId:{}", task.getId());
        triggerPostTaskActions(tenant.getId(), desk, folder, SEAL);
    }


    @PostMapping("desk/{deskId}/search/{state}")
    @Operation(
            summary = "Query folders",
            description = """
                          Returns every folders of the given state, on the the given desk.
                                                    
                          - DRAFT (yellow pill)     : Non-started folders
                          - PENDING (blue pill)     : Every pending folders on current desk
                          - LATE (red pill)         : Same as PENDING, but only late ones
                          - DELEGATED (purple pill) : Every pending folders that can be acted upon by delegation
                          - FINISHED (green pill)   : Every finished workflow, ready to export
                          - REJECTED (black pill)   : Every rejected workflow, ready to delete
                          - RETRIEVABLE             : Available UNDO to perform
                          """,
            hidden = HIDE_UNSTABLE_API
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "A JSON object containing data and pagination infos"),
            @ApiResponse(responseCode = "401", description = "The user is not logged, or its session has expired"),
            @ApiResponse(responseCode = "403", description = "Authenticated user doesn't have desk's role.\n(You might want to set it in Keycloak)"),
            @ApiResponse(responseCode = "404", description = "The given deskId does not exist")
    })
    // @PreAuthorize("hasRole('" + DESK_ROLE_PREFIX + "' + #deskId)")
    public PaginatedList<? extends Folder> listFolders(@Parameter(description = Tenant.API_DOC_ID_VALUE, required = true)
                                                       @PathVariable(name = Tenant.API_PATH) String tenantId,
                                                       @Parameter(description = DeskRepresentation.API_DOC_ID_VALUE)
                                                       @PathVariable(name = DeskRepresentation.API_PATH) String deskId,
                                                       @PathVariable State state,
                                                       @DeskResolved Desk desk,
                                                       @RequestBody FolderFilterDto folderFilterDto,
                                                       @Parameter(description = API_DOC_SORT_BY)
                                                       @RequestParam(required = false) FolderSortBy sortBy,
                                                       @Parameter(description = API_DOC_ASC)
                                                       @RequestParam(required = false, defaultValue = API_DOC_ASC_DEFAULT) boolean asc,
                                                       @Parameter(description = API_DOC_PAGE)
                                                       @RequestParam(required = false, defaultValue = API_DOC_PAGE_DEFAULT) int page,
                                                       @Parameter(description = API_DOC_PAGE_SIZE)
                                                       @RequestParam(required = false, defaultValue = API_DOC_PAGE_SIZE_DEFAULT) int pageSize) {

        log.debug("listFolders desk:{} state:{}", desk.getName(), state);

        FolderFilter folderFilter = modelMapper.map(folderFilterDto, FolderFilter.class);
        // Permission request

        ArrayList<DelegationRule> delegationRules = new ArrayList<>();
        Map<String, Desk> delegatingRoles = new HashMap<>();
        if (state == DELEGATED) {
            Optional.of(permissionService.getActiveDelegations(Set.of(desk.getId())))
                    .map(a -> a.get(desk.getId()))
                    .filter(CollectionUtils::isNotEmpty)
                    .map(ArrayList::new)
                    .ifPresent(desk::setDelegationRules);

            ofNullable(desk.getDelegatingDesks())
                    .orElse(emptyList())
                    .forEach(d -> delegatingRoles.put(d.getId(), d));

            ofNullable(desk.getDelegationRules())
                    .ifPresent(delegationRules::addAll);

            delegationRules.addAll(
                    delegatingRoles.keySet().stream()
                            .map(d -> new DelegationRule(d, null, null, null))
                            .toList()
            );
        }

        // Request

        sortBy = ofNullable(sortBy).orElse(CREATION_DATE);
        PaginatedList<? extends Folder> folders = switch (sortBy) {

            // Special case : Sorting by Typology needs a cross-DB request
            // The total is still retrieved from the Workflow service, though.
            case TYPE_NAME, SUBTYPE_NAME -> new PaginatedList<>(
                    dbService.getFoldersSortedByTypologyName(desk, state, sortBy, asc, page, pageSize),
                    page,
                    pageSize,
                    () -> workflowService.countFolders(desk.getId(), state)
            );

            // Regular case :
            // We go through the Workflow service
            default -> workflowService.listFoldersByState(desk.getId(), state, delegationRules, sortBy, folderFilter, asc, page, pageSize);
        };

        // Setup proper values

        typologyService.updateTypology(folders.getData());
        authService.updateFolderReferencedDeskNames(folders.getData());

        String currentUserId = KeycloakSecurityUtils.getCurrentSessionUserId();
        folders.getData()
                .stream()
                .filter(folder -> CollectionUtils.isNotEmpty(folder.getReadByUserIds()))
                .filter(folder -> folder.getReadByUserIds().contains(currentUserId))
                .forEach(folder -> folder.setReadByCurrentUser(true));

        // Sending back the result

        log.debug("listTasks deskId:{} list:{}", desk.getId(), folders.getData().stream().map(Folder::getName).toList());
        return folders;
    }


    @GetMapping("desk/{deskId}/workflowDefinition/{key}")
    @Operation(hidden = HIDE_UNSTABLE_API)
    public WorkflowDefinition getWorkflowDefinitionByKey(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                                         @PathVariable(name = Tenant.API_PATH) String tenantId,
                                                         @TenantResolved Tenant tenant,
                                                         @Parameter(description = DeskRepresentation.API_DOC_ID_VALUE)
                                                         @PathVariable(name = DeskRepresentation.API_PATH) String deskId,
                                                         @DeskResolved Desk desk,
                                                         @PathVariable String key) {

        log.info("getWorkflowDefinitionByKey key:{}", key);
        return workflowBusinessService.getWorkflowDefinitionByKey(tenant, key, desk.getId());
    }


    private void handleExternalTasks(Folder folder, Task task) {
        if (folder.getStepList().stream().map(Task::getAction).anyMatch(action -> action.equalsOneOf(EXTERNAL_SIGNATURE, SECURE_MAIL))) {
            this.secureMailService.rejectExternalProcedure(folder, task);
            this.externalSignatureService.rejectExternalProcedure(folder, task);

            task.getMetadata().put(METADATA_STATUS, "");
            task.getMetadata().put(METADATA_TRANSACTION_ID, "");
            task.getMetadata().put(METADATA_PASTELL_DOCUMENT_ID, "");
        }
    }


    void checkActionAllowedWithDetachedSignatures(@NotNull Folder folder, @NotNull Action action) {

        boolean containsDetachedSignatures = folder.getDocumentList()
                .stream()
                .filter(Document::isMainDocument)
                .map(Document::getDetachedSignatures)
                .anyMatch(CollectionUtils::isNotEmpty);

        // Default case, nothing else to check

        if (!containsDetachedSignatures) {
            return;
        }

        // Most external signature services won't do anything but PAdES.
        // So we'll prevent signature invalidation, and forbid that action for now.

        if (action == EXTERNAL_SIGNATURE) {
            throw new LocalizedStatusException(BAD_REQUEST, "message.you_can_t_perform_this_action_with_detached_signatures");
        }

        // Regular case check

        boolean isCryptographicSignatureAction = action.equalsOneOf(SEAL, SIGNATURE);
        boolean isEnvelopedSignature = cryptoService
                .getSignatureFormat(folder.getType(), folder.getDocumentList())
                .isEnvelopedSignature();

        if (isEnvelopedSignature && isCryptographicSignatureAction) {
            throw new LocalizedStatusException(BAD_REQUEST, "message.you_can_t_perform_this_action_with_detached_signatures");
        }
    }


    private List<MultipartFile> getDetachedSignatureListForDocument(List<String> detachedSignatureNames, List<MultipartFile> detachedSignatures) {
        checkDetachedSignatureNames(detachedSignatureNames, detachedSignatures);

        return detachedSignatures.stream()
                .filter(detachedSignature -> detachedSignature.getOriginalFilename() != null
                        && detachedSignatureNames.contains(detachedSignature.getOriginalFilename()))
                .toList();
    }


    private void checkDetachedSignatureNames(List<String> detachedSignatureNames, List<MultipartFile> detachedSignatures) {
        Map<String, MultipartFile> multipartFileNames = detachedSignatures
                .stream()
                .collect(toMap(MultipartFile::getOriginalFilename, m -> m));

        if (detachedSignatureNames.stream().anyMatch(name -> !multipartFileNames.containsKey(name))) {
            throw new LocalizedStatusException(BAD_REQUEST, "message.detached_signature_mapping_error");
        }
    }


    private void createDetachedSignatureForDocument(String folderContentId, Document document, MultipartFile detachedSignature) {
        DocumentBuffer documentBuffer = new DocumentBuffer(detachedSignature, false, -1);

        documentBuffer.setContentFlux(readInputStream(detachedSignature::getInputStream, new DefaultDataBufferFactory(), FILE_TRANSFER_BUFFER_SIZE));

        contentService.createDetachedSignature(folderContentId, document, documentBuffer, null);
    }

}

